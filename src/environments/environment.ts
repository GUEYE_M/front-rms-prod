// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SERVER_API_URL: 'https://rmsapi-old.azurewebsites.net/rms-api/',
  SERVER_API_AUTH: 'https://rmsapi-old.azurewebsites.net/',
  HOST_SERVEUR: 'https://reseau-medical.com',
  HOST_SERVEUR_ADMIN: 'https://reseau-medical.com/RMS-Admin',
  URL_ANNEXE_CONTRAT: 'http://rms-api/annexe_contrat.pdf',
  SENDIN_BLUE_URL: 'https://api.sendinblue.com/v3/',
  SENDIN_BLUE_API_KEY: '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',
  IMAGES_API_URL: 'https://rmsapi-old.azurewebsites.net/',
  publicKey: 'BHxZIZMIQklE4luEV7PD9bDP8A0Q8RwrsFP9fZ11UCzi8aEn5pK0g-EF9r-YT9IhFREcoT6LG7k5hDSs86hQXK',
  privateKey: '3Y5p4LDbz_58N_Y0DSLbIVrABCU4DLU_B3QbHB9gz4g',
  urlWordPress: 'https://rms-wp.azurewebsites.net',
  urlLogoRms: '../../../assets/logo.png',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
