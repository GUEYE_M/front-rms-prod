import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DossierService} from '../../shared/services/interim/dossier.service';
import {ActivatedRoute} from '@angular/router';
import {IMAGES_API_URL} from '../../shared/utils/images_api_url';

@Component({
  selector: 'app-dossier-candidature-med',
  templateUrl: './dossier-candidature-med.component.html',
  styleUrls: ['./dossier-candidature-med.component.css']
})
export class DossierCandidatureMedComponent implements OnInit {

  private fileUrl = IMAGES_API_URL;
  imgFiledefault = this.fileUrl + 'images/addfile.jpg';
  fileDifferentImage = this.fileUrl + 'images/fichier.png';
  dossierInterim: any;
  infosPerso: any = [];
  diplomesUrls = [];
  idDossier: number;
  nomInterim: string;


  displayedColumns: string[] = ['id', 'libelle', 'action'];
  dataSource: MatTableDataSource<any[]>;

  // private paginator: MatPaginator;
  // private sort: MatSort;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;


  constructor(private dossierService: DossierService,
              private activatedRoute: ActivatedRoute) {
    this.idDossier =  this.activatedRoute.snapshot.params.params;

    this.dataSource = new MatTableDataSource([]);

    this.dossierService.findDossierCandidat(this.idDossier).subscribe((response) => {
      if (response['dossier']) {
        this.dossierInterim = response['dossier'];

        this.infosPerso = Object.keys(this.dossierInterim);
        this.nomInterim = response['nom'];
      }
      if (response['diplome']) {
        this.dataSource = new MatTableDataSource(JSON.parse(response['diplome']));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.diplomesUrls = JSON.parse(response['diplomeUrls']);

      }
    });
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  dowloadDossier() {
    if (this.dataSource.data) {
      const thiss = this;
    }
  }

}
