import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DossierCandidatureMedComponent } from './dossier-candidature-med.component';

describe('DossierCandidatureMedComponent', () => {
  let component: DossierCandidatureMedComponent;
  let fixture: ComponentFixture<DossierCandidatureMedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DossierCandidatureMedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DossierCandidatureMedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
