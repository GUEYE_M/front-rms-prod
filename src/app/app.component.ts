import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { SwUpdate } from '@angular/service-worker';
import { version } from 'punycode';
import { environment } from 'src/environments/environment';
import { WebSocketService } from 'src/shared/services/web-socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'rms-new';
  constructor(private swUpdate: SwUpdate) {
    if (environment.production) {
      this.swUpdate.available.subscribe(
        (version) => {
          if (version) {
            this.swUpdate.activateUpdate().then(
              () => {
                window.location.reload()
              }
            )
          }
        }
      );
      this.swUpdate.checkForUpdate();
    }

  }
  ngOnInit(): void {

    AOS.init();
  }
}
