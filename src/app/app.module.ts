import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, LOCALE_ID, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/modules/shared/shared.module';
import { SiteContainerComponent } from './site-container/site-container.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ErpModule } from '../shared/modules/erp/erp.module';
import { OffresModule } from '../shared/modules/offres/offres.module';
import { WrapperComponent } from './site-container/wrapper/wrapper.component';
import {
	AuthServiceConfig,
	FacebookLoginProvider,
	GoogleLoginProvider,
	LinkedinLoginProvider
} from 'angular5-social-auth';
import { EspaceMembreModule } from '../shared/modules/espace-membre/espace-membre.module';
import { AuthInterceptor } from '../shared/interceptors/auth.interceptor';
import { BlockUIModule } from 'ng-block-ui';
import { SpecialiteListesResolver } from '../shared/resolver/specialite_listes.resolve';
import { UsersErpResolve } from '../shared/resolver/usersErp.resolve';
import { InterimResolve } from '../shared/resolver/interim.resolve';
import { InterimSingleResolve } from '../shared/resolver/InterimSingle.resolve';
import { TitleCasePipe, registerLocaleData } from '@angular/common';
import { ClientResolveService } from '../shared/resolver/ClientResolve.resolve';
import { AllCandidaturesResolver } from '../shared/resolver/allCandidatures.resolver';
import { MissionMedecinResolve } from '../shared/resolver/missionMedecin.resolve';
import { ClientSingleResolve } from '../shared/resolver/ClientSingle.resolve';
import { FicheSpecialiteResolve } from '../shared/resolver/ficheSpecialite.resolve';
import { MissionsResolve } from '../shared/resolver/missions.resolve';
import { ListesPlanningResolve } from '../shared/resolver/listesPlanning.resolve';
import { VacationClientResolve } from '../shared/resolver/vacationClient.resolve';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

import localeFr from '@angular/common/locales/fr';
import { DossierCandidatureMedComponent } from './dossier-candidature-med/dossier-candidature-med.component';
import { LiensUtilesComponent } from './site-container/inc/liens-utiles/liens-utiles.component';
import { FaqModalComponent } from './site-container/inc/faq-modal/faq-modal.component';
import { PageExterneComponent } from './site-container/inc/page-externe/page-externe.component';
import { CarouselSiteComponent } from './site-container/inc/carousel-site/carousel-site.component';
import { FaqSiteComponent } from './site-container/inc/faq-site/faq-site.component';
import { ServiceSiteComponent } from './site-container/inc/service-site/service-site.component';
import { NavbarSiteComponent } from './site-container/inc/navbar-site/navbar-site.component';
import { FooterSiteComponent } from './site-container/inc/footer-site/footer-site.component';
import { FourOrFourComponent } from './exceptions/four-or-four/four-or-four.component';
import { OffreSiteResolver } from '../shared/resolver/offreSite.resolver';
import { WrapperInterimComponent } from './site-container/wrapper-interim/wrapper-interim.component';
import { WrapperClientComponent } from './site-container/wrapper-client/wrapper-client.component';
import { AuthGuard } from '../shared/guard/auth.guard';
import { AuthCommercialGuard } from '../shared/guard/authCommercial.guard';
import { AuthGuardManager } from '../shared/guard/auth.guardManager';
import { AuthSuperAdminGuard } from '../shared/guard/authSuperAdmin.guard';
import { AuthGuardCandidat } from '../shared/guard/auth.guardCandidat';
import { AuthGuardRecruteur } from '../shared/guard/auth.guardRecruteur';
import { SingleInterimComponent } from '../shared/modules/erp/erp-container/modules/interim/single-interim/single-interim.component';
import { environment } from '../environments/environment';
import { NgDatePikerService } from 'src/shared/services/ngDatePiker.service';
import { HistoriquesService } from 'src/shared/services/erp/historiques.service';



registerLocaleData(localeFr, 'fr-FR');

export function getAuthServiceConfigs() {
	const config = new AuthServiceConfig([
		{
			id: FacebookLoginProvider.PROVIDER_ID,
			provider: new FacebookLoginProvider('385574452043000')
		},
		{
			id: GoogleLoginProvider.PROVIDER_ID,
			provider: new GoogleLoginProvider(
				'453388316288-30teqjcm29jeq2hs4q3212mbteul92h3.apps.googleusercontent.com'
			)
		},
		{
			id: LinkedinLoginProvider.PROVIDER_ID,
			provider: new LinkedinLoginProvider('86340tilnxwnjt')
		}
	]);
	return config;
}

@NgModule({
	declarations: [
		AppComponent,
		SiteContainerComponent,
		WrapperComponent,
		DossierCandidatureMedComponent,
		LiensUtilesComponent,
		FaqModalComponent,
		PageExterneComponent,
		CarouselSiteComponent,
		FaqSiteComponent,
		ServiceSiteComponent,
		NavbarSiteComponent,
		FooterSiteComponent,
		FourOrFourComponent,
		WrapperInterimComponent,
		WrapperClientComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		SharedModule,
		ErpModule,
		OffresModule,
		EspaceMembreModule,
		HttpClientModule,
		NgxDaterangepickerMd.forRoot(),
		BlockUIModule.forRoot(),
		ServiceWorkerModule.register('/ngsw-worker.js', {
			enabled: environment.production
		})
	],
	providers: [
		TitleCasePipe,
		SpecialiteListesResolver,
		ClientResolveService,
		NgDatePikerService,
		HistoriquesService,
		UsersErpResolve,
		InterimResolve,
		MissionMedecinResolve,
		InterimSingleResolve,
		AllCandidaturesResolver,
		ClientSingleResolve,
		FicheSpecialiteResolve,
		MissionsResolve,
		OffreSiteResolver,
		ListesPlanningResolve,
		VacationClientResolve,
		{
			provide: AuthServiceConfig,
			useFactory: getAuthServiceConfigs
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		},
		{ provide: LOCALE_ID, useValue: 'fr-FR' },
		AuthGuard,
		AuthGuardCandidat,
		AuthGuardRecruteur,
		AuthCommercialGuard,
		AuthGuardManager,
		AuthSuperAdminGuard
	],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
	entryComponents: [ LiensUtilesComponent, FaqModalComponent ],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
