import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {SiteContainerComponent} from './site-container/site-container.component';
import {WrapperComponent} from './site-container/wrapper/wrapper.component';
import {DossierCandidatureMedComponent} from './dossier-candidature-med/dossier-candidature-med.component';
import {PageExterneComponent} from './site-container/inc/page-externe/page-externe.component';
import {UpdatePasswordComponent} from '../shared/modules/espace-membre/update-password/update-password.component';
import {CheckMailComponent} from '../shared/modules/espace-membre/check-mail/check-mail.component';
import {VerifieTokenUrlComponent} from '../shared/modules/espace-membre/verifie-token-url/verifie-token-url.component';
import {ForgetPasswordComponent} from '../shared/modules/espace-membre/forget-password/forget-password.component';
import {ActivationCompteComponent} from '../shared/modules/espace-membre/activation-compte/activation-compte.component';
import {FourOrFourComponent} from './exceptions/four-or-four/four-or-four.component';
import {WrapperInterimComponent} from './site-container/wrapper-interim/wrapper-interim.component';
import {WrapperClientComponent} from './site-container/wrapper-client/wrapper-client.component';
import {AuthGuard} from '../shared/guard/auth.guard';
import {SingleInterimComponent} from '../shared/modules/erp/erp-container/modules/interim/single-interim/single-interim.component';
import {BienvenueMyRMSComponentComponent} from '../shared/modules/erp/erp-container/modules/interim/bienvenue-my-rmscomponent/bienvenue-my-rmscomponent.component';


const routes: Routes = [
  {
    path: '',
    component: SiteContainerComponent,
    children:
      [
        {
          path: '',
          loadChildren: '../shared/modules/espace-membre/espace-membre.module#EspaceMembreModule'
        },
        {
          path: 'offres',
          loadChildren: '../shared/modules/offres/offres.module#OffresModule'
        },
        {path: 'blog', loadChildren: () => import('./blog/blog.module').then(m => m.BlogModule)},
        {
          path: 'espace-membre',
          loadChildren: '../shared/modules/espace-membre/espace-membre.module#EspaceMembreModule'
        },
        {path: 'membre/activation-compte/:token', component: ActivationCompteComponent},
        {path: 'membre/mot-de-passe-oublie', component: ForgetPasswordComponent},
        {path: 'membre/mot-de-passe-oublie/:params', component: ForgetPasswordComponent},
        {path: 'membre/verifie-token/:params', component: VerifieTokenUrlComponent},
        {path: 'membre/check-email', component: CheckMailComponent},
        {path: 'membre/nouveau/mot-de-passe-oublie', component: UpdatePasswordComponent},
        {path: 'rgpd', component: PageExterneComponent},
        {path: 'wrapper-interim', component: WrapperInterimComponent},
        {path: 'wrapper-client', component: WrapperClientComponent},
        {path: 'mention-legal', component: PageExterneComponent},
        {path: 'a-propos', component: PageExterneComponent},
      ],
  },
  {
    path: 'RMS-Admin',
    loadChildren: '../shared/modules/erp/erp.module#ErpModule', canActivate: [AuthGuard]
  },
  {path: 'dossier-candidat/:params', component: DossierCandidatureMedComponent},
  {path: '404', component: FourOrFourComponent},
  {path: '**', redirectTo: '404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
