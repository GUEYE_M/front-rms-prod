import { Component, OnInit } from '@angular/core';
import {SERVER_API_AUTH, SERVER_API_URL} from '../../../shared/utils/server_api_url';
import {UtilsService} from '../../../shared/services/utils.service';
import {HttpClient} from '@angular/common/http';
import {retry} from 'rxjs/operators';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})
export class WrapperComponent implements OnInit {
  private resourceUrl = SERVER_API_AUTH;
  stats: any;
  constructor(private utilsService: UtilsService, private http: HttpClient) {
  }

  ngOnInit() {
    this.utilsService.onRevientEnHaut();

    this.http.get<any[]>(this.resourceUrl + 'stats').pipe(
      retry(3), // retry a failed request up to 3 times
    ).subscribe(
      (stats: any) => {
        this.stats = stats;
      }
    );
  }
}
