import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-service-site',
  templateUrl: './service-site.component.html',
  styleUrls: ['./service-site.component.css']
})
export class ServiceSiteComponent implements OnInit {
  @Input() stats?: any;
  constructor() { }

  ngOnInit() {
  }

}
