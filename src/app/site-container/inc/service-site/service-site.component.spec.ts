import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceSiteComponent } from './service-site.component';

describe('ServiceSiteComponent', () => {
  let component: ServiceSiteComponent;
  let fixture: ComponentFixture<ServiceSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
