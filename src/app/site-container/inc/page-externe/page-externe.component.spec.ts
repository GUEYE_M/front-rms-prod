import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExterneComponent } from './page-externe.component';

describe('PageExterneComponent', () => {
  let component: PageExterneComponent;
  let fixture: ComponentFixture<PageExterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageExterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageExterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
