import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UtilsService} from "../../../../shared/services/utils.service";

@Component({
  selector: 'app-page-externe',
  templateUrl: './page-externe.component.html',
  styleUrls: ['./page-externe.component.css']
})
export class PageExterneComponent implements OnInit {

  rgpd = false;
  mentionLegale = false;
  qui_sommes_nous = false;
  constructor(
      private activated_route: ActivatedRoute,
      private utile : UtilsService
  ) { }

  ngOnInit() {
    this.utile.onRevientEnHaut();
    if(this.activated_route.routeConfig.path === 'rgpd') {
        this.rgpd = true;
        this.mentionLegale = false;
        this.qui_sommes_nous = false;
    }
    if(this.activated_route.routeConfig.path === 'mention-legal') {
        this.rgpd = false;
        this.mentionLegale = true;
        this.qui_sommes_nous = true;
    }
    if(this.activated_route.routeConfig.path === 'a-propos'){
        this.rgpd = false;
        this.mentionLegale = false;
        this.qui_sommes_nous = true;
    }

  }

}
