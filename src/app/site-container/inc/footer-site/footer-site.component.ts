import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer-site',
  templateUrl: './footer-site.component.html',
  styleUrls: ['./footer-site.component.css']
})
export class FooterSiteComponent implements OnInit {
   years = new Date()
  constructor() {
   
  }

  ngOnInit() {
    // fff
  }

  onTopFunction() {

    window.scroll(0, 0);

  }

}

//window.onscroll = function() {scrollFunction()};


function scrollFunction() {

  if (document.body.scrollTop > 350 || document.documentElement.scrollTop > 350) {

    document.getElementById('myBtn').style.display = 'block';

  } else {

    document.getElementById('myBtn').style.display = 'none';

  }

}
