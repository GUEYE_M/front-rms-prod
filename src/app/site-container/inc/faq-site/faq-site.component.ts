import {Component, OnInit} from '@angular/core';
import {FaqModalComponent} from '../faq-modal/faq-modal.component';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-faq-site',
  templateUrl: './faq-site.component.html',
  styleUrls: ['./faq-site.component.css']
})
export class FaqSiteComponent implements OnInit {
  dialogRef: MatDialogRef<FaqModalComponent>;

  constructor(
    private dialog: MatDialog,
  ) {
  }

  ngOnInit() {
  }

  onConsulter() {
    (1542);
    this.dialogRef = this.dialog.open(FaqModalComponent, {
      width: '800px',
      height: '650px',
      data: '',
      panelClass: 'myapp-no-padding-dialog'
    });
  }

}
