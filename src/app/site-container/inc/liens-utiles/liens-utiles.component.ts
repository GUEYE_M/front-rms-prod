import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-liens-utiles',
  templateUrl: './liens-utiles.component.html',
  styleUrls: ['./liens-utiles.component.css']
})
export class LiensUtilesComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<LiensUtilesComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
