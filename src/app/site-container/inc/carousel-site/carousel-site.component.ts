import { Component, OnInit } from '@angular/core';
declare var $: any;
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../../../shared/services/user.service';
import {NotificationService} from '../../../../shared/services/notification.service';
import {RolesService} from '../../../../shared/services/roles.service';
import {OffresService} from '../../../../shared/services/recruteur/offres.service';
@Component({
  selector: 'app-carousel-site',
  templateUrl: './carousel-site.component.html',
  styleUrls: ['./carousel-site.component.css']
})
export class CarouselSiteComponent implements OnInit {

  interimaire: any;
  interimaire_id = 0;
  nombre_offres = 0;
  form: FormGroup;

  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              private router: Router,
              private notifService: NotificationService,
              private roleService: RolesService,
              private offresService: OffresService) {
  }

  ngOnInit() {
    // dd
    localStorage.removeItem('filterOnCarousel');
    this.initForm();
    let i = 2;
    let interimaire_id = 0;
    this.interimaire = this.userService.getCurrentUser();
    if (this.interimaire && this.roleService.isCandidatUnique(this.interimaire.roles)) {
      interimaire_id = this.interimaire.interim.id;
    }
    // this.offresService.get_nombre_offres(interimaire_id).subscribe(
    //   (next) => {
    //     this.nombre_offres = next;
    //   }
    // );
  }
  initForm() {
    this.form = this.formBuilder.group({
      specialite: [''],
      departement: [''],
      mois: [''],
    });
  }
  onSubmitForm() {
    localStorage.setItem('filterOnCarousel', JSON.stringify(this.form.getRawValue()));
    this.router.navigateByUrl('/offres');
  }

}
