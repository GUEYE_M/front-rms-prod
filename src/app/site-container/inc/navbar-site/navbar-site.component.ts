import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {LiensUtilesComponent} from '../liens-utiles/liens-utiles.component';
import {JwtToken} from '../../../../shared/models/jwt-token.model';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {NewAuthService} from '../../../../shared/services/Auth/newAuth.service';
import {AuthNormalService} from '../../../../shared/services/authNormal.service';
import {UserService} from '../../../../shared/services/user.service';
import {NotificationService} from '../../../../shared/services/notification.service';
import {JwtService} from '../../../../shared/services/jwt..service';
import {HOST_SERVEUR} from '../../../../shared/utils/server_api_url';

@Component({
  selector: 'app-navbar-site',
  templateUrl: './navbar-site.component.html',
  styleUrls: ['./navbar-site.component.css']
})
export class NavbarSiteComponent implements OnInit {
  dialogRef: MatDialogRef<LiensUtilesComponent>;
  public jwtToken: JwtToken;
  public token: string;
  public subscription: Subscription;
  public currentUser: any;
  private hostServeur = HOST_SERVEUR;


  constructor(
    private router: Router,
    private newAuthService: NewAuthService,
    private activatedRoute: ActivatedRoute,
    private authNormalService: AuthNormalService,
    private userService: UserService,
    private notifService: NotificationService,
    private jwtService: JwtService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.subscription = this.jwtService.jwtToken.subscribe((jwtToken: JwtToken) => {
      this.jwtToken = jwtToken;
      this.currentUser = jwtToken.user;
    });
    // si l'url contient un fragment on scroll la page jusquau fragment
    this.activatedRoute.fragment.subscribe(f => {
      if (f) {
        let x = document.querySelector('#' + f);
        if (x) {
          x.scrollIntoView();
        }
      }
    });
  }

  public logout(): void {
    this.newAuthService.logout();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  monCompte() {
    if (this.currentUser) {
      this.authNormalService.redirectProfile(this.currentUser.roles);
    }
  }

  lien(item) {
    // if (item === 'RMS-Admin') {
    //   window.location.href = this.hostServeur + item;
    // } else {
    //   this.router.navigate(['/' + item]);
    // }
    this.router.navigate(['/'+item]);
  }

  onLiensUtiles() {
    this.dialogRef = this.dialog.open(LiensUtilesComponent, {
      width: '800px',
      height: '650px',
      data: '',
      panelClass: 'myapp-no-padding-dialog'
    });
  }

}
