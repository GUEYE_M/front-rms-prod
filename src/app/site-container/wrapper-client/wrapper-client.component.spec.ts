import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapperClientComponent } from './wrapper-client.component';

describe('WrapperClientComponent', () => {
  let component: WrapperClientComponent;
  let fixture: ComponentFixture<WrapperClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrapperClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
