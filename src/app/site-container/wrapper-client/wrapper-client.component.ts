import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/shared/services/utils.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Specialite } from 'src/shared/models/Specialite.model';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';
moment.locale('fr', localization);
import { LOCALE } from 'src/shared/utils/date_filter_config';
import { NotificationService } from 'src/shared/services/notification.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-wrapper-client',
  templateUrl: './wrapper-client.component.html',
  styleUrls: ['./wrapper-client.component.css']
})
export class WrapperClientComponent implements OnInit {
  devisForm: FormGroup;
  locale = LOCALE;
  listSpecialites: [] = [];
  constructor(private utile: UtilsService, private fb: FormBuilder, private specialiteService: SpecialiteService,
    private notificationService: NotificationService, private datePipe: DatePipe, ) { }

  ngOnInit() {
    this.utile.onRevientEnHaut();

    this.initform();

    this.specialiteService.list(null, true).subscribe(
      (response) => {
        if (response) {
          this.listSpecialites = response.data;
        }
      }
    );
  }


  initform() {
    this.devisForm = this.fb.group(
      {
        nom_etablissement: ['', [Validators.required]],
        nom: ['', [Validators.required]],
        telephone: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        service: ['', [Validators.required]],
        periode: ['' , [Validators.required]],
        date_debut: [],
        date_fin: []
      }
    );
  }

  onDevis() {

    this.notificationService.blockUiStart();

    this.devisForm.get('date_debut').setValue(this.datePipe.transform(this.devisForm.get('periode').value.startDate, 'yyyy-MM-dd'));
    this.devisForm.get('date_fin').setValue(this.datePipe.transform(this.devisForm.get('periode').value.endDate, 'yyyy-MM-dd'));


    this.utile.demandeDevis(this.devisForm.value).subscribe(
      (response: any) => {
        this.notificationService.blockUiStop();
        if (response.erreur) {

          this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
        } else {
         // this.devisForm.reset();
          this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
        }
      }
    );
  }

}
