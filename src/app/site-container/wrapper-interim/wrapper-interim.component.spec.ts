import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapperInterimComponent } from './wrapper-interim.component';

describe('WrapperInterimComponent', () => {
  let component: WrapperInterimComponent;
  let fixture: ComponentFixture<WrapperInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrapperInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
