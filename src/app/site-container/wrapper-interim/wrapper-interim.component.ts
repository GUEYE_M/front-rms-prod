import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/shared/services/utils.service';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-wrapper-interim',
  templateUrl: './wrapper-interim.component.html',
  styleUrls: ['./wrapper-interim.component.css']
})
export class WrapperInterimComponent implements OnInit {
  form: FormGroup;
  constructor(
    private utile: UtilsService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.initForm();
    this.utile.onRevientEnHaut();
  }

  initForm() {
    this.form = this.formBuilder.group({
      specialite: [''],
      departement: [''],
      mois: [''],
    });
  }

  onSubmitForm() {
    localStorage.setItem('filterOnCarousel', JSON.stringify(this.form.getRawValue()));
    this.router.navigateByUrl('/offres');
  }

}
