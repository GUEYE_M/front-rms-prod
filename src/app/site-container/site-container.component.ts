import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-site-container',
  templateUrl: './site-container.component.html',
  styleUrls: ['./site-container.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SiteContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
