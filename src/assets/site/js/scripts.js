var captachaConnexionCandidat = function(response) {
    if (response){
     document.getElementById('_submit').removeAttribute("disabled");
    }

};

var captachRegistrationCandidat = function(response) {
    if (response || document.getElementById('medecinform').value===true){
        document.getElementById('btn-verification-candidat').removeAttribute("disabled");
    }

};
var widgetId1;
var widgetId2;
var onloadCallback = function() {
    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
    var widgetId1;
    var widgetId2;
    if ( $('#connexionCandidat').length ) {
        grecaptcha.render('connexionCandidat',{
            'sitekey' : '6Le1qFgUAAAAAIQNQshDQU6aN2rSOMxO4emmAd4s',
            'callback' : captachaConnexionCandidat,
            'theme' : 'light'
        });
    }
    if ( $('#registrationcandidat').length ) {
        grecaptcha.render('registrationcandidat', {
            'sitekey' : '6Le1qFgUAAAAAIQNQshDQU6aN2rSOMxO4emmAd4s',
            'callback' : captachRegistrationCandidat,
            'theme' : 'light'
        });
    }
    if ( $('#registrationclient').length ) {
        grecaptcha.render('registrationclient', {
            'sitekey' : '6Le1qFgUAAAAAIQNQshDQU6aN2rSOMxO4emmAd4s',
            'callback' : verifyCallback,
            'theme' : 'light'
        });
    }
};

// ==============================================================

//     script pour les panel du bloc FAQ

// =================================================================

$(document).on('click', '.panel-heading span.clickable', function (e) {

    var $this = $(this);

    if (!$this.hasClass('panel-collapsed')) {

        $this.parents('.panel').find('.panel-body').slideUp();

        $this.addClass('panel-collapsed');

        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');

    } else {

        $this.parents('.panel').find('.panel-body').slideDown();

        $this.removeClass('panel-collapsed');

        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');

    }

});

$(document).on('click', '.panel div.clickable', function (e) {

    var $this = $(this);

    if (!$this.hasClass('panel-collapsed')) {

        $this.parents('.panel').find('.panel-body').slideUp();

        $this.addClass('panel-collapsed');

        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');

    } else {

        $this.parents('.panel').find('.panel-body').slideDown();

        $this.removeClass('panel-collapsed');

        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');

    }

});

$(document).ready(function () {
  $('#bootstrap-touch-slider').bsTouchSlider();

  $('.panel-heading span.clickable').click();

    $('.panel div.clickable').click();


    // Pour fermer les messages flash du parametrage du compte
    $(".close").click(function(){
        $("#myAlertSuccess").alert("close");
    });

    $(".close").click(function(){
        $("#myAlertDanger").alert("close");
    });

  (function ($) {
    $.fn.countTo = function (options) {
      options = options || {};

      return $(this).each(function () {
        // set options for current element
        var settings = $.extend({}, $.fn.countTo.defaults, {
          from:            $(this).data('from'),
          to:              $(this).data('to'),
          speed:           $(this).data('speed'),
          refreshInterval: $(this).data('refresh-interval'),
          decimals:        $(this).data('decimals')
        }, options);

        // how many times to update the value, and how much to increment the value on each update
        var loops = Math.ceil(settings.speed / settings.refreshInterval),
          increment = (settings.to - settings.from) / loops;

        // references & variables that will change with each update
        var self = this,
          $self = $(this),
          loopCount = 0,
          value = settings.from,
          data = $self.data('countTo') || {};

        $self.data('countTo', data);

        // if an existing interval can be found, clear it first
        if (data.interval) {
          clearInterval(data.interval);
        }
        data.interval = setInterval(updateTimer, settings.refreshInterval);

        // initialize the element with the starting value
        render(value);

        function updateTimer() {
          value += increment;
          loopCount++;

          render(value);

          if (typeof(settings.onUpdate) == 'function') {
            settings.onUpdate.call(self, value);
          }

          if (loopCount >= loops) {
            // remove the interval
            $self.removeData('countTo');
            clearInterval(data.interval);
            value = settings.to;

            if (typeof(settings.onComplete) == 'function') {
              settings.onComplete.call(self, value);
            }
          }
        }

        function render(value) {
          var formattedValue = settings.formatter.call(self, value, settings);
          $self.html(formattedValue);
        }
      });
    };

    $.fn.countTo.defaults = {
      from: 0,               // the number the element should start at
      to: 0,                 // the number the element should end at
      speed: 1000,           // how long it should take to count between the target numbers
      refreshInterval: 100,  // how often the element should be updated
      decimals: 0,           // the number of decimal places to show
      formatter: formatter,  // handler for formatting the value before rendering
      onUpdate: null,        // callback method for every time the element is updated
      onComplete: null       // callback method for when the element finishes updating
    };

    function formatter(value, settings) {
      return value.toFixed(settings.decimals);
    }
  }(jQuery));

  jQuery(function ($) {
    // custom formatting example
    $('.count-number').data('countToOptions', {
      formatter: function (value, options) {
        return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
      }
    });

    // start all the timers
    $('.timer').each(count);

    function count(options) {
      var $this = $(this);
      options = $.extend({}, options || {}, $this.data('countToOptions') || {});
      $this.countTo(options);
    }
  });

});


// *******************************script du bloc google maps******************************

$('.map-container')

    .click(function(){

        $(this).find('iframe').addClass('clicked')})

    .mouseleave(function(){

        $(this).find('iframe').removeClass('clicked')});



$(".add_diplome").click(function () {
    $(".div_add_diplome").append( "<br><form class=\"form-inline ajout_diplome\">\n" +
        "  <div class=\"form-group col-lg-4 col-md-4 col-sm-4 col-xs-12\">\n" +
        "    <input type=\"text\" class=\"form-control\" id=\"nomdiplome\" placeholder=\"Nom du diplome\">\n" +
        "  </div>\n" +
        "  <div class=\"form-group col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n" +
        "    <input type=\"file\" class=\"form-control\">\n" +
        "  </div>\n"+
        "  <div class=\"form-group col-lg-2 col-md-2 col-sm-2 col-xs-12\">" +
        "    <button type=\"button\" class=\"col-lg-6 col-md-2 col-sm-2 col-xs-12 btn btn-success remove_diplome\" style='margin-right: 1px'><i class=\"glyphicon glyphicon-ok\"></i></button>\n" +
        "    <button type=\"button\" class=\"col-lg-5 col-md-2 col-sm-2 col-xs-12 btn btn-danger\"><i class=\"glyphicon glyphicon-remove\"></i></button>\n"+
        "  </div>\n"+
        "</form>" +
        "<br>"+
        "<hr width='100%'>" )
});

$(".remove_diplome").click(function () {
    
});



