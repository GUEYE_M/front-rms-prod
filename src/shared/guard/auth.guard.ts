import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {JwtToken} from '../models/jwt-token.model';
import {map} from 'rxjs/internal/operators';
import {JwtService} from '../services/jwt..service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private jwtService: JwtService,
    ) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> {
        return this.jwtService.jwtToken.pipe(
            map( (jwtToken: JwtToken) => {
                if (jwtToken.isAuthenticated  && jwtToken.roles.includes('ROLE_ADMIN')) {
                    return true;
                } else {
                    this.router.navigate(['']);
                    return false;
                }
            })
        );
    }
}


