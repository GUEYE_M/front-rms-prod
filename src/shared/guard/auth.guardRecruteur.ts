import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {JwtToken} from '../models/jwt-token.model';
import {map} from 'rxjs/internal/operators';
import {AuthNormalService} from '../services/authNormal.service';
import {JwtService} from '../services/jwt..service';
import {RolesService} from '../services/roles.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardRecruteur implements CanActivate {
  constructor(
        private router: Router,
        private authNormalService: AuthNormalService,
        private jwtService: JwtService,
        private rolesService: RolesService,
  ) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.jwtService.jwtToken.pipe(
      map((jwtToken: JwtToken) => {
          if (jwtToken.isAuthenticated && this.rolesService.isRecruteur(jwtToken.roles)) {
            return true;
          }
        this.router.navigate(['/RMS-Admin']);
          return false;
      })
    );
  }
}


