import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {JwtToken} from '../models/jwt-token.model';
import {map} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {JwtService} from '../services/jwt..service';
import {RolesService} from '../services/roles.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardSuperviseur implements CanActivate {
  constructor(
        private router: Router,
        private jwtService: JwtService,
        private rolesService: RolesService,

  ) {}
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
      return this.jwtService.jwtToken.pipe(
        map((jwtToken: JwtToken) => {
          if (jwtToken.isAuthenticated && this.rolesService.isSuperviseur(jwtToken.roles)) {
            return true;
          }
          this.router.navigate(['/RMS-Admin']);
          return false;
        })
      );
    }
}


