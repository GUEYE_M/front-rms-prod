import { LocaleConfig } from 'ngx-daterangepicker-material';

import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

export const LOCALE : LocaleConfig = {
    applyLabel: 'Appliquer',
    customRangeLabel: ' - ',
    clearLabel: 'Annuler',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  }
;
