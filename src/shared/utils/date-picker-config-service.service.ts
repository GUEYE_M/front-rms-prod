import { Injectable, ViewChild } from '@angular/core';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DatePickerConfigServiceService {



 public  options:NgxDrpOptions;
 public  presets:Array<PresetItem> = [];
  constructor() { 
    

    this.datepickerConfig();

   
  }



  
  datepickerConfig()
  {
    const today = new Date();
    const fromMin = new Date(today.getFullYear(), today.getMonth()-2, 1);
    const fromMax = new Date(today.getFullYear(), today.getMonth()+1, 0);
    const toMin = new Date(today.getFullYear(), today.getMonth()-1, 1);
    const toMax = new Date(today.getFullYear(), today.getMonth()+2, 0);
  
    // const resetRange = {fromDate: today, toDate: today};
    // this.dateRangePicker.resetDates(resetRange);
  
    this.setupPresets();
    this.options = {
                    presets: this.presets,
                    format: 'dd-MM-yyyy',
                    range: {fromDate:today, toDate: today},
                    applyLabel: "OK",
                    calendarOverlayConfig: {
                      shouldCloseOnBackdropClick: false,
                      hasBackdrop: false
                    },

                     cancelLabel: "Annuler",
                     startDatePrefix:"Debut",
                     endDatePrefix:"Fin",
                     placeholder:"Rechercher",
                     animation:true
                    // excludeWeekends:true,
                    // fromMinMax: {fromDate:fromMin, toDate:fromMax},
                    // toMinMax: {fromDate:toMin, toDate:toMax}
                  };
  }


  setupPresets() {
  
    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    }
    
    const today = new Date() 
    const yesterday = backDate(1);
    const minus7 = backDate(7)
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth()+1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth()-1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);
    
    this.presets =  [
      {presetLabel: "Hier", range:{ fromDate:yesterday, toDate:today }},
      {presetLabel: "Les 7 derniers jours", range:{ fromDate: minus7, toDate:today }},
      {presetLabel: "Les 30 derniers jours", range:{ fromDate: minus30, toDate:today }},
      {presetLabel: "Ce mois", range:{ fromDate: currMonthStart, toDate:currMonthEnd }},
      {presetLabel: "Le mois dernier", range:{ fromDate: lastMonthStart, toDate:lastMonthEnd }}
    ]
  }

  

}
