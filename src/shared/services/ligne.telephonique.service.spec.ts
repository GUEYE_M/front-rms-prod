import { TestBed, inject } from '@angular/core/testing';

import { LigneTelephoniqueService } from './ligne.telephonique.service';

describe('Ligne.TelephoniqueService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LigneTelephoniqueService]
    });
  });

  it('should be created', inject([LigneTelephoniqueService], (service: LigneTelephoniqueService) => {
    expect(service).toBeTruthy();
  }));
});
