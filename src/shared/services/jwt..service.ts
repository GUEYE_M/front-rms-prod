import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {JwtToken} from '../models/jwt-token.model';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  public jwtToken: BehaviorSubject<JwtToken> = new BehaviorSubject({
    isAuthenticated: null,
    roles: null,
    user: null,
  });
  constructor() {
    this.initToken();
  }


  private initToken(): void {
    const user = localStorage.getItem('rms-user');
    if (user) {
      this.jwtToken.next({
        isAuthenticated: true,
        roles: JSON.parse(user).roles,
        user: JSON.parse(user),
      });
    } else {
      this.jwtToken.next({
        isAuthenticated: false,
        roles: null,
        user: null,
      });
    }
  }
}
