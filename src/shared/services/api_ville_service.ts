import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Departement } from '../models/departement.model';
import { Commune } from '../models/commune.model';
import { API_ALL_VILLE_FR, APPI_ADDRESSE, APPI_ADDRESSE2 } from '../utils/api_gouv_ville_fr';
import { BehaviorSubject, Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { SERVER_API_URL, URL_COUNTRIES } from '../utils/server_api_url';


@Injectable({
  providedIn: 'root'
})
export class ApiVilleService {
  private resourVille = API_ALL_VILLE_FR;
  private resourceAdresse = APPI_ADDRESSE;
  private resourceAdresse2 = APPI_ADDRESSE2;
  private api_url = SERVER_API_URL

  listDeptSubject: BehaviorSubject<any[]> = new BehaviorSubject([null]);
  listCommuneBydepartement: BehaviorSubject<any[]> = new BehaviorSubject([null])


  constructor(private http: HttpClient) {
    // this.listdepartement();
  }
  allDepartementFr() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "access-control-allow-headers": "X-Requested-With", 'access-control-allow-origin': '*'
    });
    return this.http.get<any[]>(this.resourVille);
  }

  listdepartement() {
    this.http.get<Departement[]>(this.resourVille).subscribe(

      (response) => {
        // (response);
        this.listDeptSubject.next(response);
      }, (error) => {

      });
  }
  departementByCommune() {
    this.http.get<any[]>(this.resourVille + '/18/communes').subscribe(
      (response) => {
        this.listCommuneBydepartement.next(response);
      }
    )
  }

  findbyCommune(commune: string) {

    let data = {
      commune: commune
    };

    return this.http.post<Commune[]>(this.api_url + 'search-commune', data).pipe(
      retry(3)
    );
  }

  getCountriesByName(name:string){

   return  this.http.get<[]>(URL_COUNTRIES+'name/'+name).pipe(retry(3));
    
  }

  findVilleByVilleAndAdresse(addresse, dept) {
    return this.http.get<Commune[]>(this.resourVille + addresse + '&fields=departement,region,population');
  }
  findbyCodePostal(code: string): Observable<HttpResponse<Commune[]>> {
    return this.http.get<Commune[]>(this.resourVille + '/communes?codePostal=' + code + '&fields=departement,region,population', { observe: 'response' });
  }

  findAllRegion() {
    return this.http.get<any[]>(this.resourVille + '/regions');
  }


  listCommunes() {
    return this.http.get<any[]>(this.resourVille + '/communes');
  }

}
