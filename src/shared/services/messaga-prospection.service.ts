import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';
import { retry } from 'rxjs/operators';
import { SERVER_API_URL } from '../utils/server_api_url';
@Injectable({
	providedIn: 'root'
})
export class MessagaProspectionService {
	messageProspection: BehaviorSubject<any[]> = new BehaviorSubject([]);
	typeProspectionSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
	public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
	public closemodaleAddMsg: BehaviorSubject<boolean> = new BehaviorSubject(false);
	ProspectSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
	list_message_by_prospect: any[] = [];

	list_type: Array<any> = [];
	list_message: any[];
	private resourceUrl = SERVER_API_URL + 'message-prospection/';
	constructor(private http: HttpClient) {}

	emitMessage() {
		this.messageProspection.next(this.list_message);
	}

	emitMessageByProspect() {
		this.ProspectSubject.next(this.list_message_by_prospect);
	}

	emitType() {
		this.typeProspectionSubject.next(this.list_type);
	}

	list() {
		// this.showLoadClient.next(false);
		return this.http.get<any[]>(this.resourceUrl + 'list').pipe(retry(3)).subscribe((response: any) => {
			this.list_message = response;
			this.emitMessage();
		});
	}

	listMessageByProspect(data: any) {
		// this.showLoadClient.next(false);
		return this.http.post<any[]>(this.resourceUrl, data).pipe(retry(3));
	}

	create(message: any) {
		return this.http.post<any>(this.resourceUrl + 'new', message).pipe(retry(3));
	}
	update(id: number, message: any) {
		return this.http.post<any>(this.resourceUrl + id + '/update', message).pipe(retry(3));
	}

	find(id: number) {
		return this.http.get(this.resourceUrl + 'show/' + id).pipe(retry(3));
	}

	listTypeProspection(data: any = null) {
		return this.http.post<any[]>(this.resourceUrl + 'type/list', data).pipe(retry(3));
	}

	newTypeProspection(type: any) {
		return this.http.post<any>(this.resourceUrl + 'type/new', type).pipe(retry(3));
	}
	findTypeProspectionById(id: number) {
		return this.http.get<any>(this.resourceUrl + id + '/type').pipe(retry(3));
	}
	updateTypeProspection(id: number, type: any) {
		return this.http.post<any>(this.resourceUrl + 'type/update/' + id, type).pipe(retry(3));
	}

	allProspectionInterim(data: any) {
		return this.http.post<any>(this.resourceUrl + 'message-prospection-interims', data).pipe(retry(3));
	}

	displayFilterNameForClient() {
		let element = [
			{
				name: 'date',
				label: 'Date'
			},
			{
				name: 'etablissement',
				label: 'Nom Etablissement'
			},

			{
				name: 'nom',
				label: 'Nom Recruteur'
			},
			{
				name: 'prenom',
				label: 'Prenom Recruteur'
			},
			{
				name: 'type',
				label: 'Type'
			},

			{
				name: 'rappeler',
				label: 'A Rappeler'
			},

			{
				name: 'auteur',
				label: 'Auteur'
			}
		];

		return element;
	}
	displayFilterNameForInterim() {
		let element = [
			{
				name: 'date',
				label: 'Date'
			},

			{
				name: 'nom',
				label: 'Nom'
			},
			{
				name: 'prenom',
				label: 'Prenom'
			},
			{
				name: 'type',
				label: 'Type'
			},

			{
				name: 'rappeler',
				label: 'A Rappeler'
			},

			{
				name: 'auteur',
				label: 'Auteur'
			}
		];

		return element;
	}
	displayFilterTypeName() {
		let element = [
			{
				name: 'type',
				label: 'Type message'
			}
		];

		return element;
	}
}
