import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import {SERVER_API_URL} from '../utils/server_api_url';


@Injectable({
  providedIn: 'root'
})
export class LigneTelephoniqueService {
  ligneSubject :  BehaviorSubject<any[]> = new BehaviorSubject([]);
  typeligneSubject :  BehaviorSubject<any[]> = new BehaviorSubject([]);
  ligne_list:any;
  ligne_type:any;
  public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public closemodaletype: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private resourceUrl = SERVER_API_URL +'ligne-telephonique/';
  constructor(private http: HttpClient) {

  }


  emitLigne()
  {
    this.ligneSubject.next(this.ligne_list)
  }
  emitTypeLigne()
  {
    this.typeligneSubject.next(this.ligne_type)
  }
  list(data:any=null)
  {

    return this.http.post<any[]>(this.resourceUrl+'list',data).pipe
    (
    retry(3)
    )
  }
  edit(ligne: any) {
    return this.http.post<any>(this.resourceUrl+'edit', ligne).pipe(
      retry(3)
    );
  }



find(id: number){
return this.http.get(this.resourceUrl+id).pipe(
    retry(3),
)
}

listtype(data:any=null)
{

  return this.http.post<any[]>(this.resourceUrl+'list/type',data).pipe
  (
  retry(3)
  )
}

findByIdLigneType(id: number){
  return this.http.get(this.resourceUrl+id+'/type').pipe(
      retry(3),
  )
  }

  editLigneType(ligne: any) {
    return this.http.post<any>(this.resourceUrl+'type/edit', ligne).pipe(
      retry(3)
    );
  }

  getIntemLigneBySpecialite(ligne:any)
  {
    return this.http.post<any>(this.resourceUrl+'interim', ligne).pipe(
      retry(3)
    );
  }



}
