import { Injectable } from '@angular/core';
import { retry } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from 'src/shared/utils/server_api_url';

@Injectable({
  providedIn: 'root'
})
export class ClientMajorationService {


  private resourceUrl = SERVER_API_URL +'majoration/';

  constructor(private http: HttpClient) { }


  find(id: number){
    return this.http.get(this.resourceUrl+id).pipe(
        retry(3),
    )
    }
    
    list(data:any=null)
    {
    
      return this.http.post<any[]>(this.resourceUrl+'list',data).pipe
      (
      retry(3)
      )
    }
    
    findByClient(id_client: number){
      return this.http.get(this.resourceUrl+'client/'+id_client).pipe(
          retry(3),
      )
      }

      edit(data: any) {
        return this.http.post<any>(this.resourceUrl+'edit', data).pipe(
          retry(3)
        );
      }
}

