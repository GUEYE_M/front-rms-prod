import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject, Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Vacation} from '../../models/recruteur/Vacation.model';
import {SERVER_API_URL} from '../../utils/server_api_url';
import {retry} from 'rxjs/operators';

type EntityResponseType = HttpResponse<Vacation>;

@Injectable({
    providedIn: 'root'
})
export class VacationClientService {
    listVacation: any[];
    list_type_vaacation: any[];
    public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public closemodaleType: BehaviorSubject<boolean> = new BehaviorSubject(false);
    vacationSubjet: BehaviorSubject<any[]> = new BehaviorSubject([null]);
    typevacationSubjet: BehaviorSubject<any[]> = new BehaviorSubject([]);
    private resourceUrl = SERVER_API_URL + 'typedegarde/';

    constructor(private http: HttpClient) {
    }

    emitsVactions() {
        this.vacationSubjet.next(this.listVacation);
    }

    emitTypeVacation() {
        this.typevacationSubjet.next(this.list_type_vaacation)
    }
    allVacations(data:any= null){
        return this.http.post<any[]>(this.resourceUrl + 'list',data).pipe(
            retry(3), // retry a failed request up to 3 times
        );

    }

    // allVacations() {
    //     this.http.get<any[]>(this.resourceUrl + 'list').pipe(retry(3))
    //     // .subscribe(
    //
    //     // (response) => {
    //     //   this.listVacation = response;
    //     //   this.emitsVactions();
    //     // }, (error) => {
    //     // (error.message)
    //     // } );
    // }

    create(vacation: any) {
        return this.http.post<Vacation>(this.resourceUrl + 'new', vacation).pipe
        (
            retry(3)
        );
    }


    update(id: number, vacation: Vacation): Observable<EntityResponseType> {
        return this.http.put<Vacation>(this.resourceUrl + id + '/update', vacation, {observe: 'response'});
    }

    find(id: number) {
        return this.http.get<Vacation>(this.resourceUrl + id).pipe(
            retry(3)
        )
    }

    findTypeVaction(id: number) {
        return this.http.get<any>(this.resourceUrl + 'type-vacation/' + id).pipe(
            retry(3)
        )
    }

    editTypeVacation(type: any) {
        return this.http.post<any>(this.resourceUrl + 'typ-vacation/new', type).pipe
        (
            retry(3)
        );
    }

    listtype(data:any=null) {

        return this.http.post<any[]>(this.resourceUrl + 'type-vacation/list',data).pipe
        (
            retry(3)

        )
    }

    displayedColumnsName()
    {

      let element = [

        {
          name:"specialite",
          label:"Specialite"
        },
        {
          name:"etablissement",
          label:"Nom Etablissement"
        },
        {
          name:"vacation",
          label:"Type Vacation"
        },

        {
          name:"heure_debut",
          label:"Heure Debut"
        },
        {
          name:"heure_fin",
          label:"Heure Fin"
        },
        {
          name:"annee",
          label:"Annee"
        },
        {
          name:"salaire_net",
          label:"Salaire Net"
        },

        {
          name:"tarif",
          label:"Tarif"
        },


      ]

      return element;
    }




}
