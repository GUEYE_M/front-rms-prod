import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'src/shared/utils/server_api_url';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HebergementServiceService {

  public resourceUrl = SERVER_API_URL ; // 


  //FicheClient de type behaviorSubject

  constructor(
      private http: HttpClient
    
  ) {
  }
  // hebergement client
  getHebergementByClient(id_client){
    return this.http.get(this.resourceUrl +'hebergement/client/'+ id_client).pipe(
        retry(3), 
    )
  }
  find(id){
    return this.http.get(this.resourceUrl + 'hebergement/show/'+ id).pipe(
        retry(3), 
    )
  }

  archived(data:any){
    return this.http.post<any>(this.resourceUrl + 'hebergement/archived/',data).pipe(
        retry(3), 
    )
  }
  // creation et Mis a jours Hebergement
  update(data) {
      return this.http.post<any>(this.resourceUrl + 'heberhement-edit/',data).pipe(
        retry(3)
      );
  }
 // all hebergement 
  list(data:any) {
    return this.http.post<any>(this.resourceUrl + 'hebergements',data).pipe(
      retry(3)
    );
}
displayedColumnsName()
{
   
  let element = [
   
   
    {
      name:"etablissement",
      label:"Nom Etablissement"
    },
    {
      name:"lieu",
      label:"Lieu"
    },
  
    {
      name:"etoile",
      label:"Etoile"
    },
    {
      name:"telephone",
      label:"Telephone"
    },
  
    
    
  ]

  return element;
}
}
