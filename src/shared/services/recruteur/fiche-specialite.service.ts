import { Injectable } from '@angular/core';
import {FicheSpeciliate} from '../../models/recruteur/FicheSpeciliate.model';
import {HttpClient, HttpResponse} from '@angular/common/http';

import { Subject, Observable, BehaviorSubject, of } from 'rxjs';
import {SERVER_API_URL} from '../../utils/server_api_url';
import {ClientService} from './client.service';
import {retry} from 'rxjs/operators';
import {createRequestOption} from '../../utils/request-util';



type EntityResponseType = HttpResponse<FicheSpeciliate>;
type EntityArrayResponseType = HttpResponse<FicheSpeciliate[]>;

@Injectable({
  providedIn: 'root'
})
export class FicheSpecialiteService {
  private resourceUrl = SERVER_API_URL + 'fiche-specialite/';
  messageErrors: string;
  ficheSpecialiteOnClient:FicheSpeciliate[] = [];
   closemodale: BehaviorSubject<boolean> = new BehaviorSubject(null);
  listFicheSpe: any[]=[];

  ficheSpeSubject: BehaviorSubject<FicheSpeciliate[]> = new BehaviorSubject([]);
  public ficheSpeByClientSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

  oneficheSpe = new Subject<FicheSpeciliate[]>();
  constructor(private http: HttpClient,
              private clientService: ClientService) {
  this.closemodale.next(false);

   }

   emitsFicheSpeByClient() {
    this.ficheSpeByClientSubject.next(this.ficheSpecialiteOnClient);
  }

  emitsFicheSpecialite() {
    this.ficheSpeSubject.next(this.listFicheSpe);
  }

  // Recuperation des fiches specialites du client
  getFicheSpecialiteDistinct(idClient: number): Observable<FicheSpeciliate[]>  {
    return this.http.get<FicheSpeciliate[]>(this.resourceUrl + 'listDistinct/'+idClient).pipe(
        retry(3)
    );
  }

  create(ficheSpecialite: FicheSpeciliate){
    // return this.http.post<FicheSpeciliate>(this.resourceUrl+'new', ficheSpecialite, { observe: 'response' });
    return this.http.post<FicheSpeciliate>(this.resourceUrl + 'new', ficheSpecialite).pipe(
      retry(3)
    );
  }


update(id : number,ficheSpecialite: FicheSpeciliate) {
    return this.http.post<FicheSpeciliate>(this.resourceUrl+id+'/update',ficheSpecialite).pipe
    (
      retry(3)
    )

}

find(id: number) {
  return this.http.get<FicheSpeciliate>(this.resourceUrl+id);

}

query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<FicheSpeciliate[]>(this.resourceUrl, { params: options, observe: 'response' });
}

delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
}


  allFicheSpecialite(data:any= null) {
      return this.http.post(this.resourceUrl +'list',data).pipe(
          retry(3), // retry a failed request up to 3 times
      );
    }
    ficheSpecialiteByClient(data:any)
    {

      return this.http.post<FicheSpeciliate[]>(this.resourceUrl + 'client', data).pipe(
        retry(3)
      )



    }
    findAll(){
      return this.http.get<FicheSpeciliate[]>(this.resourceUrl+'list',{ observe: 'response' });
    }

  displayFilterName(profil=false)
  {
    let element =
    [
      {
          name:"etablissement",
          label:"Nom Etablissement"
      },

      {
          name:"specialite",
          label:"Specilaite"
      },
      {
          name:"nombre_lit",
          label:"Nombre lit"
      },
      {
          name:"nombre_passage_an",
          label:"Passage par annee"
      },

      {
          name:"nombre_medecin_nuit",
          label:"Medecin nuit"
      },

       {
          name:"nombre_medecin_jour",
          label:"Medecin jour"
      },
      {
          name:"coef_tarification",
          label:"Coef.Tarification"
      },

      {
          name:"coef_saison",
          label:"Coef.Saison"
      },


];

  if(profil)
  {
    element.slice(0,1)
  }

return element
  }


    // OffreByFicheSpecialite(id:number) {
    //   return new Promise(
    //     (resolve, reject) => {
    //       this.http.get<FicheSpeciliate>(this.resourceUrl+'qualifiSpecialite/'+specialite).subscribe(
    //         (respone)=>{
    //          resolve(respone);
    //         },(error)=>{
    //           reject(error);
    //         }
    //       )
    //     }
    //   );
    //   }

    getTypeGardeByFicheSpecialiteId(id:number)
    {

      return this.http.get<FicheSpeciliate[]>(this.resourceUrl+ 'vacation/'+id).pipe(
        retry(3)
      )



    }
}

