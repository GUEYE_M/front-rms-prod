import { TestBed } from '@angular/core/testing';

import { ClientMajorationService } from './client-majoration.service';

describe('ClientMajorationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientMajorationService = TestBed.get(ClientMajorationService);
    expect(service).toBeTruthy();
  });
});
