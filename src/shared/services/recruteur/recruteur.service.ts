import {Injectable} from '@angular/core';
import {Recruteur} from '../../models/recruteur/Recruteur.model';
import {Subject, BehaviorSubject, pipe} from 'rxjs';

import {HttpClient, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs';
import {Client} from '../../models/recruteur/Client.model';
import {retry} from 'rxjs/operators';
import {error} from 'util';
import {NotificationService} from '../notification.service';
import {SERVER_API_URL} from '../../utils/server_api_url';
import {API_ALL_VILLE_FR} from '../../utils/api_gouv_ville_fr';
import {createRequestOption} from '../../utils/request-util';
import {MatDialogRef} from '@angular/material';
import {ModalListeMissionForCandidaturesValidationComponent} from '../../modules/erp/erp-container/modules/client-wrapper/client/modal-client/modal-liste-mission-for-candidatures-validation/modal-liste-mission-for-candidatures-validation.component';
import {ListRecruteurModalComponent} from '../../modules/erp/erp-container/modules/client-wrapper/client/modal-client/list-recruteur-modal/list-recruteur-modal.component';

type EntityResponseType = HttpResponse<Recruteur>;
type EntityArrayResponseType = HttpResponse<Recruteur[]>;


@Injectable({
  providedIn: 'root'
})
export class RecruteurService {
  dialogRef: MatDialogRef<ListRecruteurModalComponent>;
  listRecruteur: any[];
  recruteurSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
  messageErrors: string;
  private resourceUrl = SERVER_API_URL + 'recruteur/';
  private api_all_ville = API_ALL_VILLE_FR;

  constructor(private http: HttpClient, private notificationService: NotificationService) {

  }

  emitRecruteur() {
    this.recruteurSubject.next(this.listRecruteur);
  }

  allRecruteur(data: any = null) {


    return this.http.post<Recruteur[]>(this.resourceUrl + 'list', data).pipe(
      retry(3),
    );


  }

  create(recruteur: Recruteur) {
    return this.http.post<Recruteur>(this.resourceUrl + 'new', recruteur).pipe(
      retry(3)
    );

  }

  villes() {
    return this.http.get(this.api_all_ville).subscribe(
      (resp) => {
        //   (resp);
      }
    );
  }


  update(id: number, recruteur: Recruteur) {
    return this.http.post<Recruteur>(this.resourceUrl + id, recruteur).pipe(
      retry(3)
    );

  }

  find(id: number) {
    return this.http.get<any>(this.resourceUrl + id).pipe(
      retry(3)
    );
  }


  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Recruteur[]>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<EntityResponseType> {
    return this.http.delete<Recruteur>(this.resourceUrl + 'delete/' + id, {observe: 'response'});
  }

  showUpdatedItem(newItem) {
    let updateItem = this.listRecruteur.find(this.findIndexToUpdate, newItem.id);

    let index = this.listRecruteur.indexOf(updateItem);
    this.listRecruteur[index] = newItem;

  }

  findIndexToUpdate(newItem) {
    return newItem.id === this;

  }

  getClientForRecruteur(id: number) {
    return this.http.get<any[]>(this.resourceUrl + 'client/' + id);
  }

  removeRecruteur(recruteur: Recruteur) {

    const recruteurIndexToRemove = this.listRecruteur.findIndex(
      (recruteurEl) => {
        if (recruteurEl === recruteur) {
          return true;
        }
      }
    );
    this.listRecruteur.splice(recruteurIndexToRemove, 1);
    this.delete(recruteur.id).subscribe();
    this.emitRecruteur();
  }

  recruteursInactifs() {
    return this.http.get(`${this.resourceUrl}recruteurs-inactifs`).pipe(
      retry(3)
    );
  }


  updateStatut(data: any) {
    return this.http.post<Recruteur>(this.resourceUrl + 'update-statut', data).pipe(
      retry(3)
    );


  }


  displayedColumnsName() {
    let element = [
      {
        name: 'fonction',
        label: 'Fonction'
      },
      {
        name: 'nom',
        label: 'Nom'
      },
      {
        name: 'prenom',
        label: 'Prenom'
      },
      {
        name: 'etablissement',
        label: 'Nom Etablissement'
      },
      {
        name: 'telephone',
        label: 'Telephone'
      },
      {
        name: 'email',
        label: 'E-Mail'
      },
      {
        name: 'activer',
        label: 'Activer'
      },
      {
        name: 'ville',
        label: 'Ville'
      },

      {
        name: 'adresse',
        label: 'Adresse'
      }
    ];

    return element;
  }
}
