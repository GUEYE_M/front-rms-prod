import { Injectable } from '@angular/core';
import {TypeDeGarde} from '../../models/recruteur/TypeDeGarde.model';
import {HttpClient, HttpResponse} from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';
import { retry } from 'rxjs/operators';
import {SERVER_API_URL} from '../../utils/server_api_url';
import {createRequestOption} from '../../utils/request-util';
type EntityResponseType = HttpResponse<TypeDeGarde>;
type EntityArrayResponseType = HttpResponse<TypeDeGarde[]>;

@Injectable({
  providedIn: 'root'
})
export class TypeDeGardeService {

  public resourceUrl = SERVER_API_URL ;
  //TypeGarde de type behaviorSubject
  public typeDeGardeSubject: BehaviorSubject<TypeDeGarde[]> = new BehaviorSubject(null);

  constructor(
      private http: HttpClient,
  ) {
  }
  // Recuperation des fiches client
  getTypeDeGarde(): Observable<TypeDeGarde[]>  {
    return this.http.get<TypeDeGarde[]>(this.resourceUrl + 'typedegarde/list').pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

create(typeDeGarde: TypeDeGarde): Observable<EntityResponseType> {
    return this.http.post<TypeDeGarde>(this.resourceUrl, typeDeGarde, { observe: 'response' });
  }

update(typeDeGarde: TypeDeGarde): Observable<EntityResponseType> {
    return this.http.put<TypeDeGarde>(this.resourceUrl, typeDeGarde, { observe: 'response' });
}

find(id: number): Observable<EntityResponseType> {
    return this.http.get<TypeDeGarde>(`${this.resourceUrl}/${id}`, { observe: 'response' });
}

query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<TypeDeGarde[]>(this.resourceUrl, { params: options, observe: 'response' });
}

delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
}
}
