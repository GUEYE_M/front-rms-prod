
import { Injectable } from '@angular/core';
import {FicheClient} from '../../models/recruteur/FicheClient.model';
import {BehaviorSubject, Observable} from 'rxjs/index';
import {HttpClient , HttpHeaders, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {SERVER_API_URL} from '../../utils/server_api_url';
import {retry} from 'rxjs/operators';
type EntityResponseType = HttpResponse<FicheClient>;



@Injectable({
  providedIn: 'root'
})
export class FicheClientService {
  public resourceUrl = SERVER_API_URL ; // 'fiche-client';
  //FicheClient de type behaviorSubject
  public ficheClientSubject: BehaviorSubject<any> = new BehaviorSubject([]);
  public ifFicheClientEmpty: boolean = false;
  constructor(
      private http: HttpClient,
      private router: Router
  ) {
  }
  // Recuperation des fiches client
  getFicheClient(id_client){
    return this.http.get(this.resourceUrl + 'fiche-client/'+ id_client).pipe(
        retry(3), // retry a failed request up to 3 times
    );
  }
  // //Creation d'une fiche de client
 create(ficheClient: FicheClient): Observable<EntityResponseType> {
    return this.http.post<FicheClient>(this.resourceUrl + 'fiche-client', ficheClient, { observe: 'response' });
  }
  // //Mis a jours d'une fiche de client
  update(ficheClient: FicheClient, id: number): Observable<EntityResponseType> {
      return this.http.post<FicheClient>(this.resourceUrl + 'fiche-client/'+id, JSON.stringify(ficheClient), { observe: 'response' });
  }
}
