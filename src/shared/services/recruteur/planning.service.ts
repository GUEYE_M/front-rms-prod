import { Injectable } from '@angular/core';
import { Planning } from '../../models/recruteur/Planning.model';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { retry } from 'rxjs/operators';
import { ClientService } from './client.service';
import { SERVER_API_URL } from '../../utils/server_api_url';
import { createRequestOption } from '../../utils/request-util';
import * as moment from 'moment';

type EntityResponseType = HttpResponse<Planning>;
type EntityArrayResponseType = HttpResponse<Planning[]>;

@Injectable({
	providedIn: 'root'
})
export class PlanningService {
	checkConfrimSubject: BehaviorSubject<any> = new BehaviorSubject(null);
	newDatesOnMissionViaDatePiker = [];
	hiddenListeMissions = true;
	showMission = false;
	showCreateOnMissionOrList = false;
	showNewCreate = false;
	messageErrors: string;
	title = 'Gestion des Missions';
	public planningsSubject: BehaviorSubject<any> = new BehaviorSubject([]);
	public newDatesOnMissionViaDatePikerSubject: BehaviorSubject<any> = new BehaviorSubject([]);
	public missionChoisieSubject: BehaviorSubject<Planning[]> = new BehaviorSubject([]);
	public AllplanningsForMissionChoiceSubject = new BehaviorSubject([]);
	public typedeGardeForNewPeriodeClientSubject: BehaviorSubject<any> = new BehaviorSubject([]);
	public ficheSpecialiteForNewPeriodeClientSubject: BehaviorSubject<any> = new BehaviorSubject([]);
	public showAlertSubject: BehaviorSubject<any> = new BehaviorSubject(null);
	private resourceUrl = SERVER_API_URL;

	constructor(private http: HttpClient, private clientService: ClientService) {}

	// Recuperation des fiches client
	getplannings(id_client) {
		return this.http.get<Planning[]>(this.resourceUrl + 'plannings/' + id_client).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	getAllPlannings(data: any = null) {
		return this.http.post<Planning[]>(this.resourceUrl + 'plannings/all', data).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	changeStatutPlanningOnHaveAnyCandidature(data: any) {
		return this.http.post<any>(this.resourceUrl + 'plannings/changeStatut', { data: JSON.stringify(data) }).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	create(planning: any): Observable<EntityResponseType> {
		return this.http.post<any>(this.resourceUrl + 'offre/new', planning, {
			observe: 'response'
		});
	}

	createMissionByPeriode(planning: any) {
		return this.http.post(this.resourceUrl + 'offre/newPlanningByPeriode', JSON.stringify(planning));
	}

	createPeiodeOnMission(planning: any, idClient, auteur, mail) {
		return this.http.post(this.resourceUrl + 'plannings/new/newPlanningByPeriodeOnMission', {
			planning,
			idClient,
			auteur,
			mail
		});
	}

	createByMission(planning, auteur) {
		return this.http.post<any>(this.resourceUrl + 'plannings/create', {
			planning,
			auteur
		});
	}

	update(planning: any, idClient, auteur, mail, id_mission) {
		return this.http.post<Planning>(this.resourceUrl + 'planning/update', {
			planning,
			mail,
			auteur,
			idClient,
			id_mission
		});
	}

	find(id: number): Observable<EntityResponseType> {
		return this.http.get<Planning>(`${this.resourceUrl}/${id}`, {
			observe: 'response'
		});
	}

	query(req?: any): Observable<EntityArrayResponseType> {
		const options = createRequestOption(req);
		return this.http.get<Planning[]>(this.resourceUrl, {
			params: options,
			observe: 'response'
		});
	}

	delete(id: number): Observable<HttpResponse<any>> {
		return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
			observe: 'response'
		});
	}

	getReformatPlanningForMission(planningForMission) {
		let planningsTmpErp = [];
		const planningsTmpErpCopie = of(planningForMission);
		Object.entries(planningForMission);
		this.planningsSubject.next(Object.values(planningForMission));
		const highlightDaysErp = [];
		let yy = 0;
		const currentDate = moment();
		// const currentDate = new Date(moment().format('DD-MM-YYYY')).getTime() / 1000;
		Object.values(planningForMission).forEach((element: any) => {
			element;
			// const date_element = new Date(element[0].dateFr).getTime() / 1000;
			const date_element = moment(element[0].date);

			if (currentDate.isAfter(date_element)) {
				yy = yy + 1;
				let cpt = true;
				let p = 0;
				let ex = 0;
				if (element.length > 1) {
					element.forEach((item_element) => {
						if (item_element.statut.toLowerCase() === 'pourvue') {
							cpt = true;
							p = p + 1;
						} else {
							ex = ex + 1;
							cpt = false;
						}

						if (cpt === false) {
							highlightDaysErp.push({
								date: new Date(element[0].date),
								css: 'expirerEtPasPourvu',
								selectable: false,
								title: 'expirer !'
							});
						} else {
							highlightDaysErp.push({
								date: new Date(element[0].date),
								css: 'valider',
								selectable: false,
								title: ' Valider !'
							});
						}
					});
				} else if (element.length === 1) {
					if (element[0].statut.toLowerCase() !== 'pourvue') {
						highlightDaysErp.push({
							date: new Date(element[0].date),
							css: 'expirerEtPasPourvu',
							selectable: false,
							title: 'expirer !'
						});
					} else {
						highlightDaysErp.push({
							date: new Date(element[0].date),
							css: 'valider',
							selectable: false,
							title: 'Pourvue !'
						});
					}
				}
			} else {
				let cpt = true;
				let p = 0;
				let ex = 0;
				// si l'element contient plus dune date
				if (element.length > 1) {
					element.forEach((item_element) => {
						if (item_element.statut.toLowerCase() === 'pourvue') {
							cpt = true;
							p = p + 1;
						} else {
							ex = ex + 1;
							cpt = false;
						}
					});
					if (ex > 0 && p > 0) {
						highlightDaysErp.push({
							date: new Date(element[0].date),
							css: 'pourvuEtEnCour',
							selectable: false,
							title: 'valider ' + p + ' en cours ' + ex + ' !'
						});
					} else if (p <= 0) {
						highlightDaysErp.push({
							date: new Date(element[0].date),
							css: 'encours',
							selectable: false,
							title: 'en cours ' + ex + ' !'
						});
					} else {
						highlightDaysErp.push({
							date: new Date(element[0].date),
							css: 'valider',
							selectable: false,
							title: 'Valider !'
						});
					}
				} else if (element.length === 1) {
					if (element[0].statut.toLowerCase() !== 'pourvue') {
						if (element[0].statut.toLowerCase() !== 'a pourvoir') {
							highlightDaysErp.push({
								date: new Date(element[0].date),
								css: 'annuler',
								selectable: false,
								title: 'Annuler !'
							});
						} else {
							highlightDaysErp.push({
								date: new Date(element[0].date),
								css: 'encours',
								selectable: false,
								title: 'A Pourvoir !'
							});
						}
					} else {
						highlightDaysErp.push({
							date: new Date(element[0].date),
							css: 'valider',
							selectable: false,
							title: 'Pourvue !'
						});
					}
				}
			}
		});
		this.AllplanningsForMissionChoiceSubject.next(highlightDaysErp);
	}

	displayFilterName() {
		const element = [
			{
				name: 'date',
				label: 'Date'
			},
			{
				name: 'etablissement',
				label: 'Nom Etablissement'
			},

			{
				name: 'mois',
				label: 'Mois'
			},
			{
				name: 'annee',
				label: 'Annee'
			},
			{
				name: 'specialite',
				label: 'Specialite'
			},
			{
				name: 'qualification',
				label: 'Qualification'
			},
			{
				name: 'heure_debut',
				label: 'Heure Debut'
			},

			{
				name: 'heure_fin',
				label: 'Heure Fin'
			},
			{
				name: 'vacation',
				label: 'Vacation'
			},


			{
				name: 'statut',
				label: 'Statut'
			},
			{
				name: 'tarif',
				label: 'Tarif'
			},

			{
				name: 'salaire',
				label: 'Salaire'
			},
			{
				name: 'departement',
				label: 'Departement'
			},
			{
				name: 'ville',
				label: 'Ville'
			}
		];

		return element;
	}

	// revenir sur la liste des missions
	onResetPlanning() {
		this.showNewCreate = false;
		this.showCreateOnMissionOrList = false;
		this.hiddenListeMissions = true;
		this.showMission = false;

		this.title = 'Gestion des Missions';
	}
}
