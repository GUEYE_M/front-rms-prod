import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import { retry } from 'rxjs/operators';
import { Candidature } from '../../models/interim/Candidature.model';
import { SERVER_API_URL } from '../../utils/server_api_url';
@Injectable({
  providedIn: 'root'
})
export class PaieService {
  private resourceUrl = SERVER_API_URL + 'renumeration-prestation/';
  private resourceUrlContrat = SERVER_API_URL + 'renumeration-contrat/';
  public dataSourceUniqueInterim$: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient) { }

  list(data) {
    return this.http.get<any[]>(this.resourceUrlContrat + 'paie/list', {params: data} ).pipe(
      retry(3),
    );
  }
  statsRemuneration(data) {
    return this.http.get<any[]>(this.resourceUrlContrat + 'paie/stats-remuneration', {params: data} ).pipe(
      retry(3),
    );
  }
  recupData() {
    return this.http.get<any[]>(this.resourceUrlContrat + 'paie/recupData' ).pipe(
      retry(3),
    );
  }
  listOld(data) {

    return this.http.post<any[]>(this.resourceUrl + 'paie-old/list', data).pipe(
      retry(3),
    );
  }
  listLength(size) {
    return this.http.get<any[]>(this.resourceUrl + 'paie-old/list-length', { params: size }).pipe(
      retry(3),
    );
  }
  keys(): Observable<any[]> {
    return this.http.get<any[]>(this.resourceUrl + 'keys').pipe(
      retry(3),
    );
  }
  findInterimAndRefByIdRenumeration(id) {
    return this.http.post<any[]>(this.resourceUrl + 'interimAndRef-by-renumeration/' + id, null).pipe(
      retry(3),
    );
  }
  findByMM(id: number) {
    return this.http.get<any[]>(this.resourceUrl + 'mission-medecin/' + id).pipe(
      retry(3),
    );
  }
  findByPld(id: number) {
    return this.http.get<any[]>(this.resourceUrlContrat + 'contrat-pld/' + id).pipe(
      retry(3),
    );
  }
  updateRenumeration(params) {
    return this.http.post(this.resourceUrl + 'update-renumeration/' + params.id, params.data).pipe();
  }

  updateRenumerationContrat(params) {
    return this.http.post(this.resourceUrlContrat + 'update-renumeration-contrat/' + params.id, params.data).pipe();
  }

  formaterItem(item) {
    switch (item) {
      case 'datePaiementFrais':
        item = 'P.F';
        break;
      case 'totalFrais':
        item = 'Frais';
        break;
      case 'etatFrais':
        item = 'E.F';
        break;
      case 'dateEnreg':
        item = 'Enreg';
        break;
      case 'montantTrain':
        item = 'Train';
        break;
      case 'montantAvion':
        item = 'Avion';
        break;
      case 'montantTaxi':
        item = 'Taxi';
        break;
      case 'montantBus':
        item = 'Bus';
        break;
      case 'montantCovoiturage':
        item = 'Covoiturage';
        break;
      case 'montantParking':
        item = 'Parking';
        break;
      case 'montantLocationVoiture':
        item = 'Location véhicule';
        break;
      case 'montantTicketPeage':
        item = 'Péages';
        break;
      case 'montantHotel':
        item = 'Hébergement';
        break;
      case 'montantRepas':
        item = 'Repas';
        break;
      case 'modePaiement':
        item = 'M. paiement';
        break;
      case 'totalTarif':
        item = 'Tarif';
        break;
      case 'datePaiement':
        item = 'D. paiement';
        break;
      case 'totalPrestation':
        item = 'Presta';
        break;
      case 'etat':
        item = 'État';
        break;


    }
    return item;
  }
}
