import {Injectable} from '@angular/core';
import {Offres} from '../../models/recruteur/Offres.model';
import {HttpClient, HttpResponse} from '@angular/common/http';

import {BehaviorSubject, Observable} from 'rxjs';
import {RolesService} from '../roles.service';
import {SERVER_API_AUTH, SERVER_API_URL} from '../../utils/server_api_url';
import {UserService} from '../user.service';
import {InterimService} from '../interim/interim.service';
import {retry} from 'rxjs/operators';
import {createRequestOption} from '../../utils/request-util';
import {NotificationService} from "../notification.service";

type EntityResponseType = HttpResponse<Offres>;
type EntityArrayResponseType = HttpResponse<Offres[]>;


@Injectable({
  providedIn: 'root'
})

export class OffresService {

  messageErrors: string;
  public offresSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public offresSubjectSingle: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private resourceUrl = SERVER_API_URL;
  private SiteUrl = SERVER_API_AUTH;

  constructor(private http: HttpClient,
              private users_service: UserService,
              private roleService: RolesService,
              private notificationService: NotificationService,
              private interim_service: InterimService) {
    const interimaire: any = this.users_service.getCurrentUser();
    let interim_id = 0;
    if (interimaire && this.roleService.isCandidatUnique(interimaire.roles)) {
      interim_id = interimaire.interim.id;


    } else {
      interim_id = 0;
    }
    this.getJsonOffres(interim_id);
  }

  // Recuperation des fiches client
  getJsonOffres(interim_id) {
    return this.http.get<any[]>(this.SiteUrl + 'json_offres/' + interim_id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  // offre site new version
  getOffreForSite(data) {
    return this.http.post(this.SiteUrl + 'offres-site',data).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // req new version
  getOffresByReference(reference) {
    return this.http.get<any[]>(this.SiteUrl + 'single-offres-site', {params:reference}).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  PostCandidatureSansCompte(data,params) {
    return this.http.post<any[]>(this.SiteUrl + 'candidature-sans-compte', data,{params:params}).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  PostCandidatureMYRMS(data) {
    return this.http.post<any[]>(this.resourceUrl + 'candidatures-my-rms', data).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  // Recuperation du total des offres
  getJsonOffresTotal() {
    return this.http.get<any>(this.SiteUrl + 'json_offresSiteTotal').pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  getCandidaturesMeds(idMission, idInterim) {
    return this.http.get<any>(this.resourceUrl + 'get_candidatures_meds/' + idMission + '/' + idInterim).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  getJsonOffresSite(interimId, debut, size) {
    return this.http.get<any[]>(this.SiteUrl + 'json_offresSite/' + interimId + '/' + debut + '/' + size).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  getJsonOffresSiteListeAffiner(interimId) {
    return this.http.get<any>(this.SiteUrl + 'json_offresSite_liste_affinner/' + interimId).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // Recuperation des fiches client
  get_nombre_offres(interim_id): Observable<any> {
    return this.http.get<any[]>(this.SiteUrl + 'nombre_offres_sites/' + interim_id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
    // }
  }

  // Recuperation des donnee en fonction des filtres sur le carousel
  get_data_on_filter_carousel(interim_id, data): Observable<any> {
    return this.http.get<any[]>(this.SiteUrl + 'search_on_carousel/' + interim_id + '/' + data).pipe(
      retry(3), // retry a failed request up to 3 times
    );
    // }
  }

  // Recuperation des offres via la reference
  getJsonOffresByReference(reference, interim_id) {
    (interim_id);
    return this.http.get<any[]>(this.SiteUrl + 'get_missions_by_reference/' + interim_id + '/' + reference).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // Recuperation des offres similaires
  getJsonOffresSimilaires(specialite, id_mission, interim_id) {
    return this.http.get<any[]>(this.SiteUrl + 'json_offres_similaire/' + specialite + '/' + id_mission + '/' + interim_id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // Recuperation de toutes les specialite des missions validees
  getSpecialite_into_mission_enabled() {
    return this.http.get<any>(this.resourceUrl + 'get_distinct_specialite_for_mission').pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // Recuperation de tous les mois des missions validees
  getMois_into_mission_enabled() {
    return this.http.get<any>(this.resourceUrl + 'get_distinct_mois_for_mission').pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  getCommune_into_mission_enabled() {
    return this.http.get<any>(this.resourceUrl + 'get_distinct_departement_for_mission').pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // Recuperation de tous les mois des missions validees en fonction des filtres
  getOffresByFiltre(filtres, interim_id) {
    (interim_id);
    return this.http.post<any>(this.SiteUrl + 'filtre_offres/' + interim_id, {filtres}).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // Recuperation de tous les mois des missions validees en fonction des filtres
  getOffresByFiltreInput(filtres, interim_id) {
    (interim_id);
    return this.http.post<any>(this.resourceUrl + 'search_input/' + interim_id, {filtres}).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  create(offre: Offres, vacation: any[]) {
    return this.http.post(this.resourceUrl + 'new', {offre, vacation}).subscribe(
      (resp) => {
        (resp);
      }
    );
  }

  addPlanningOffre(plannning: any[]) {
    return this.http.post<Offres>(this.resourceUrl + 'new', plannning).subscribe(
      (resp) => {
        (resp);
      }
    );
  }

  update(offres: Offres): Observable<EntityResponseType> {
    return this.http.put<Offres>(this.resourceUrl, offres, {observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Offres>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Offres[]>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

}
