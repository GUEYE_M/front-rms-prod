import { Injectable } from '@angular/core';
import { Recruteur } from '../../models/recruteur/Recruteur.model';
import { Subject, BehaviorSubject, ReplaySubject } from 'rxjs';

import { HttpClient, HttpParams, HttpResponse, HttpHeaders } from '@angular/common/http';

import { Client } from '../../models/recruteur/Client.model';
import { retry } from 'rxjs/operators';
import { RecruteurClient } from '../../models/recruteur/RecruteurClient.model';
import { AnyKindOfDictionary } from 'lodash';
import { SERVER_API_URL } from '../../utils/server_api_url';

@Injectable({
	providedIn: 'root'
})
export class ClientService {
	currentClient: BehaviorSubject<any> = new BehaviorSubject(null);
	public showLoadClient: BehaviorSubject<any> = new BehaviorSubject(true);
	public allmissionsForClientSubject: BehaviorSubject<any> = new BehaviorSubject(null);
	listClient = [];
	idClient: BehaviorSubject<number> = new BehaviorSubject(null);
	labelTab: BehaviorSubject<number> = new BehaviorSubject(0);
	labelTab_text: BehaviorSubject<string> = new BehaviorSubject('');
	public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
	clientSubject: BehaviorSubject<Client[]> = new BehaviorSubject([]);
	singleClient: BehaviorSubject<any[]> = new BehaviorSubject([]);
	messageErrors: string;

	private resourceUrl = SERVER_API_URL + 'client/';

	constructor(private http: HttpClient) {}

	emitClient() {
		this.clientSubject.next(this.listClient);
	}
	public addIdClientPLDToClientErp(data) {
		return this.http.post(this.resourceUrl + 'add-idPldClient-to-client', data).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}
	list() {
		this.showLoadClient.next(false);
		return this.http.post<Client[]>(this.resourceUrl + 'list', null).pipe(retry(3)).subscribe((response) => {
			this.listClient = response;
			this.emitClient();
		});
	}

	listClients(data: any = null, all = false) {
		return this.http.post<Client[]>(this.resourceUrl + 'list', { data, all }).pipe(retry(3));
	}

	allClient() {
		return this.http.post<Client[]>(this.resourceUrl + 'list', null).pipe(retry(3)).subscribe((clients: any) => {
			this.listClient = clients.data;
		});
	}

	clientsInactifs() {
		return this.http.get(`${this.resourceUrl}clients-inactifs`).pipe(retry(3));
	}

	// create(client: Client) {
	//   return this.http.post<Client>(this.resourceUrl + 'new', client).subscribe(
	//       (resp) => {
	//           (resp);
	//           this.listClient.push(resp)
	//           this.emitClient();

	//       }
	//   );
	// }

	create(client: Client) {
		return this.http.post<Client>(this.resourceUrl + 'new', client).pipe(retry(3));
	}

	update(id: number, client: Client) {
		this.resourceUrl + id + '/update';
		return this.http.post<Client>(this.resourceUrl + id + '/update', client).pipe(retry(3));
	}

	delete(data: any) {
		return this.http.post<Client>(this.resourceUrl + 'delete', data).pipe(retry(3));
	}

	find(data: any) {
		return this.http.post(this.resourceUrl + 'show', data).pipe(retry(3));
	}

	find_new(id: number) {
		return this.http.get<any[]>(this.resourceUrl + 'find/' + id).pipe(retry(3));
	}

	getRecruteurForClient(id: number) {
		return this.http.get<RecruteurClient>(this.resourceUrl + 'recruteur/' + id, { observe: 'body' }).pipe(retry(3));
	}

	getClient(id: number) {
		return this.http.get<any[]>(this.resourceUrl + id, { observe: 'body' }).pipe(retry(3));
	}

	// fonction pour archiver un ou plusieures client(s) sur liste client
	archiverClient = (data: any) => {
		
		return this.http.post<any>(this.resourceUrl + 'archived', data).pipe(retry(3));
	}

	contratsClient(id) {
		return this.http.get<any[]>(this.resourceUrl + 'contrat/' + id).pipe(retry(3));
	}
	candidatureValideeByRef(ref) {
		return this.http.get<any[]>(this.resourceUrl + 'candidature-validee-by-ref/' + ref).pipe(retry(3));
	}

	// query(req?: any): Observable<EntityArrayResponseType> {
	//     const options = createRequestOption(req);
	//     return this.http.get<Recruteur[]>(this.resourceUrl, { params: options, observe: 'response' });
	// }

	// delete(id: number): Observable<HttpResponse<any>> {
	//     return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
	// }

	// allClient() {
	//   this.http.get<Client[]>(this.resourceUrl +'list').subscribe(

	//     (response)=>{
	//     this.listRecruteur = response;
	//       this.emitRecruteur();
	//     },(error)=>
	//     {
	//       this.messageErrors = error;
	//     }
	// }

	//}
	enableClient(clientId, enable) {
		return this.http.post(this.resourceUrl + 'enabled_client/' + clientId, enable);
	}

	public updateIdPld(client) {
		return this.http.post(this.resourceUrl + 'update-id-pldClient', client);
	}

	getClientRecruteurByCandidatureId(id: number) {
		return this.http.get<any[]>(this.resourceUrl + 'recruteur-cand/' + id).pipe(retry(3));
	}

	// cette fonction met  a jout le champ activer du client
	updateEnable(data: any) {
		return this.http.post<Recruteur>(this.resourceUrl + 'update-enable', data).pipe(retry(3));
	}

	getStatistiqueTarifCh(data: any) {
		return this.http.post<any>(this.resourceUrl + 'statistique-tarif-ch-year', data).pipe(retry(3));
	}

	getStatistiquePlanningByYearAndStatut(data: any) {
		return this.http.post<any>(this.resourceUrl + 'statistique-nombre-planning-ch-year', data).pipe(retry(3));
	}

	getChColaborationByyear(data: any) {
		return this.http.post<any>(this.resourceUrl + 'statistique-colaboration-ch-year', data).pipe(retry(3));
	}

	getChOrInterimByRegion(data: any) {
		return this.http.post<any>(this.resourceUrl + 'statistique-by-region', data).pipe(retry(3));
	}

	getCandidatureForMissionByClient(data: any) {
		return this.http.post<any>(this.resourceUrl + 'get-all-Candidature', data).pipe(retry(3));
	}

	getAllMissionByClient(data: any) {
		return this.http.post<any>(this.resourceUrl + 'get-all-Missions-enable', data).pipe(retry(3));
	}
}
