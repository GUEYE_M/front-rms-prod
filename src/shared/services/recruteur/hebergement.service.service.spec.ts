import { TestBed } from '@angular/core/testing';

import { HebergementServiceService } from './hebergement.service.service';

describe('Hebergement.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HebergementServiceService = TestBed.get(HebergementServiceService);
    expect(service).toBeTruthy();
  });
});
