import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Candidature} from '../models/interim/Candidature.model';
import {SERVER_API_URL} from '../utils/server_api_url';
import {BehaviorSubject} from 'rxjs';
import {retry} from 'rxjs/operators';

type EntityResponseType = HttpResponse<Candidature>;
type EntityArrayResponseType = HttpResponse<Candidature[]>;

@Injectable({
  providedIn: 'root'
})
export class OffresServiceAll {
  messageErrors: string;
  public offreErpSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public listeDesOffresSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private resourceUrl = SERVER_API_URL;
  

  constructor(private http: HttpClient) {
  }
  // Recuperation des fiches client
  getJsonOffres(interim_id, data: any = null) {
    if (data) {
      return this.http.post<any[]>(this.resourceUrl + 'json_offres/' + interim_id, data).pipe(
        retry(3),
      );
    } else {
      return this.http.get<any[]>(this.resourceUrl + 'json_offres/' + interim_id).pipe(
        retry(3),
      );
    }
  }


}
