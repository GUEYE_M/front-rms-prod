import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subscription, timer } from 'rxjs/index';
import { JwtToken } from '../models/jwt-token.model';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/User.model';
import { switchMap, tap } from 'rxjs/internal/operators';
import { Router } from '@angular/router';
import { UserService } from './user.service';
import { Recruteur } from '../models/recruteur/Recruteur.model';
import { Interim } from '../models/interim/Interim.model';
import { JwtService } from './jwt..service';
import { HOST_SERVEUR_ADMIN, SERVER_API_URL } from '../utils/server_api_url';

@Injectable({
  providedIn: 'root'
})
export class AuthNormalService {
  public subscription: Subscription;
  public response: string;
  private resourceUrl = SERVER_API_URL;
  private hostServeurAdmin = HOST_SERVEUR_ADMIN;
  constructor(private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private jwtService: JwtService
  ) {
    // this.subscription = this.initTimer();
  }
  // public initTimer() {
  //     return timer(50000,150000).pipe(
  //      switchMap(() => {
  //          if (localStorage.getItem('rms-user')) {
  //              return this.http.get<string>(this.resourceUrl + 'refresh-token').pipe(
  //                  tap((token: any) => {
  //                    let roles = JSON.parse(rep.user).roles ;
  //                    this.jwtService.jwtToken.next({
  //                          isAuthenticated: true,
  //                          roles: roles
  //                      });
  //                      localStorage.setItem('rms-membre', token.jws + 'rms' + token.roles);
  //                  })
  //              );
  //          } else {
  //              this.subscription.unsubscribe();
  //              return of(null);
  //          }
  //      })
  //     ).subscribe(() => {}, erreur => {
  //       this.jwtService.jwtToken.next({
  //             isAuthenticated: false,
  //             roles: null
  //         });
  //         localStorage.removeItem('rms-user');
  //         this.subscription.unsubscribe();
  //       this.router.navigate(['']);
  //
  //     });
  // }

  public refreshUser() {
    return this.http.get<string>(this.resourceUrl + 'current').pipe(
      tap((rep: any) => {
        const user = rep['user'];
        this.jwtService.jwtToken.next({
          isAuthenticated: true,
          roles: JSON.parse(user).roles,
          user: JSON.parse(user)
        });
        localStorage.setItem('rms-user', user);
      })
    );
  }

  public signupRecruteur(recruteur: Recruteur) {
    return this.http.post<string>(this.resourceUrl + 'signup-recruteur', recruteur);
  }
  public signupInterim(user) {
    return this.http.post<string>(this.resourceUrl + 'signup-interim', user);
  }
  public signupInterimErp(interim) {
    return this.http.post<string>(this.resourceUrl + 'signup-interim-erp', interim);
  }
  public logout(): void {
    this.jwtService.jwtToken.next({
      isAuthenticated: false,
      roles: null,
      user: null,
    });
    // location.reload();
    localStorage.removeItem('rms-user');
  }
  public signin(credentials: { username: string, password: string }) {
    return this.http.post(this.resourceUrl + 'signin', credentials).pipe(
      tap((rep: any) => {
        if (rep !== 'erreur' && rep !== 'inactif') {
          let user = rep.user;
          this.jwtService.jwtToken.next({
            isAuthenticated: true,
            roles: JSON.parse(user).roles,
            user: JSON.parse(user)
          });
          localStorage.setItem('rms-user', user);
          this.redirectProfile(JSON.parse(user).roles);
        }
      })
    );
  }
  signinByRS(item: any): Observable<string> {
    return this.http.post<string>(this.resourceUrl + 'signinByRS', item).pipe(
      tap((rep: any) => {
        if (rep !== 'erreur' && rep !== 'inactif') {
          let user = rep.user;
          this.jwtService.jwtToken.next({
            isAuthenticated: true,
            roles: JSON.parse(user).roles,
            user: JSON.parse(user)
          });
          localStorage.setItem('rms-user', user);
          this.redirectProfile(JSON.parse(user).roles);
        }
      })
    );
  }
  endSigninByRS(user): Observable<string> {
    return this.http.post<string>(this.resourceUrl + 'endSigninByRS', user).pipe(
      tap((rep: any) => {
        if (rep !== 'erreur') {
          let user = rep.user;
          this.jwtService.jwtToken.next({
            isAuthenticated: true,
            roles: JSON.parse(user).roles,
            user: JSON.parse(user)
          });
          localStorage.setItem('rms-user', user);
          this.redirectProfile(JSON.parse(user).roles);
        }
      })
    );
  }
  redirectProfile(roles) {
    if (roles.includes('ROLE_ADMIN'))
      // window.location.href = this.hostServeurAdmin;
      this.router.navigate(['RMS-Admin']);
  }
}
