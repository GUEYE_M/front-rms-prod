import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";

import { Observable, BehaviorSubject, Subject } from "rxjs";
import { retry } from "rxjs/operators";
import { Client } from "../../models/recruteur/Client.model";
import { SERVER_API_URL } from "../../utils/server_api_url";

@Injectable({
  providedIn: "root"
})
export class GestionnaireInterneService {
  currentGestionnaireInterne: BehaviorSubject<any> = new BehaviorSubject(null);
  private resourceUrl = SERVER_API_URL + "gestionnaire-interne/";
  constructor(private http: HttpClient) {}

  create(gestionnaireInterne) {
    return this.http.post(this.resourceUrl + "add", gestionnaireInterne);
  }
  find(id: number): Observable<any> {
    return this.http
      .get<any>(`${this.resourceUrl + "show/"}${id}`)
      .pipe(retry(3));
  }
  update(id: number, gestionnaireInterne: any) {
    return this.http
      .post<any>(this.resourceUrl + id + "/update", gestionnaireInterne)
      .pipe(retry(3));
  }
  delete() {}

  list() {
    return this.http.get<any[]>(this.resourceUrl + "list").pipe(retry(3));
  }

  getStatistique(data) {
    return this.http
      .post<any[]>(this.resourceUrl + "statistique", data)
      .pipe(retry(3));
  }

  visibleList(data: any = null) {
    return this.http
      .post<any[]>(this.resourceUrl + "visible-list", data)
      .pipe(retry(3));
  }

  displayFilterName() {
    let element = [
      {
        name: "nom",
        label: "Nom"
      },

      {
        name: "prenom",
        label: "Prenom"
      },

      {
        name: "username",
        label: "Nom Utilisateur"
      },

      {
        name: "email",
        label: "E-mail"
      },

      {
        name: "activer",
        label: "Activer"
      }
    ];

    return element;
  }
}
