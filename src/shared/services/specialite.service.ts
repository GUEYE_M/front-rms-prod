import { Injectable } from '@angular/core';
import { Specialite } from '../models/Specialite.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, BehaviorSubject} from 'rxjs';
import {SERVER_API_URL} from '../utils/server_api_url';
import {retry} from 'rxjs/operators';
import {createRequestOption} from '../utils/request-util';
import {NotificationService} from './notification.service';


type EntityArrayResponseType = HttpResponse<Specialite[]>;



@Injectable({
  providedIn: 'root'
})
export class SpecialiteService {
  detail = false;
  private resourceUrl = SERVER_API_URL + 'specialite/';
  specialites: Specialite[] = [];
  list_specialite: any[];
  specialiteLoader = false;
  specialite_choisi: BehaviorSubject<Specialite> = new BehaviorSubject(null);

  SpecialiteSubject: BehaviorSubject<any> = new BehaviorSubject([]);
  listeSpecialitesForInterimSubject = new BehaviorSubject(null);


  constructor(
    private http: HttpClient,
    private notificatiService: NotificationService
  ) {
    //this.list();

    // this.list().subscribe(
    //    (next) => this.SpecialiteSubject.next(next)
    // );
  }

  emitSpecialite()
  {
    this.SpecialiteSubject.next(this.list_specialite);
  }


  list(data:any=null,all=false) {
    if(all)
    {
      return this.http.get<any>(this.resourceUrl + 'list').pipe(
        retry(3)
    )
    }
    else
    {
      return this.http.post<any>(this.resourceUrl + 'list', data).pipe(
        retry(3)
    )
    }

  }

  getQualifsBySpecialite(id_specialite){
      return this.http.get(this.resourceUrl + id_specialite).pipe(
          retry(3)
      );
  }
  getSpecialiteAndQualifsForInterimHaveNot(id_interim) {
      return this.http.get(this.resourceUrl + 'InterimHaveNot/' + id_interim).pipe(
          retry(3)
      );
  }
  deleteQualifsBySpecialite(id_interim, content){
      return this.http.post(SERVER_API_URL + 'deleteQualifications/'+id_interim, JSON.stringify(content));
  }

  getQualificationBySpecialite(specialite:string) {
    return this.http.get(`${this.resourceUrl + 'qualifiSpecialite'}/${specialite}`);
}
  create(specialite: Specialite) {
    return this.http.post(this.resourceUrl + 'add', JSON.stringify(specialite));
  }

update(specialite: Specialite) {
    const spe = {'id': specialite.id, 'specialite': specialite.specialite, 'nomspecailite': specialite.nomspecialite, 'qualifications': specialite.qualifications};
    return this.http.post(this.resourceUrl + 'update', specialite);
}

find(id: number) {
    return this.http.get(`${this.resourceUrl}/${id}`);
}

getSingleBook(specialite: string) {
  return new Promise(
    (resolve, reject) => {
      this.http.get<Specialite[]>(this.resourceUrl+'qualifiSpecialite/'+specialite).subscribe(
        (respone)=>{
         resolve(respone);
        },(error)=>{
          reject(error);
        }
      )
    }
  );
  }

query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Specialite[]>(this.resourceUrl, { params: options, observe: 'response' });
}

delete(specialite: any){
    return this.http.post(this.resourceUrl+'delete',specialite);
}


displayFilterName()
{

  let element =
  [

    {
      name:"specialite",
      label:"Specialite"
  }


];

return element
}
}
