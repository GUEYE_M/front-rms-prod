import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Candidature } from '../models/interim/Candidature.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { retry } from 'rxjs/internal/operators';
import { SERVER_API_URL } from '../utils/server_api_url';
import { createRequestOption } from '../utils/request-util';
import { MatDialogRef } from '@angular/material';
import { ModalListeMissionForCandidaturesValidationComponent } from '../modules/erp/erp-container/modules/client-wrapper/client/modal-client/modal-liste-mission-for-candidatures-validation/modal-liste-mission-for-candidatures-validation.component';

type EntityResponseType = HttpResponse<Candidature>;
type EntityArrayResponseType = HttpResponse<Candidature[]>;

@Injectable({
  providedIn: 'root'
})
export class CandidatureService {
  dialogRef: MatDialogRef<ModalListeMissionForCandidaturesValidationComponent>;
  idClient = 0;
  messageErrors: string;
  public candidatureInterimSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public allCandidaturesSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public candidaturesByMissionForClientSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public candidaturesByMissionForClientSubject_: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public alertTraitementCandidature = '';
  public loaderTraitementCandidatureServiceSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private resourceUrl = SERVER_API_URL;

  constructor(private http: HttpClient) {
  }

  create_candidature_medecin(enabled, condition_medecin = null, send_mail = true, candidatures, id_missionAndInterim) {
    return this.http.post<Candidature>(this.resourceUrl + 'set_candidature', {
      enabled,
      send_mail,
      condition_medecin,
      candidatures,
      id_missionAndInterim
    });
  }

  create_candidature_medecin_matching(data) {
    return this.http.post<Candidature>(this.resourceUrl + 'set_candidatureMatching', {
      data
    });
  }

  get_candidature_interim(id_interim, mission_id): BehaviorSubject<any[]> {
    this.http.get<any[]>(this.resourceUrl + 'get_candidature_interim/' + id_interim + '/' + mission_id).pipe(
      retry(3), // retry a failed request up to 3 times
    ).subscribe(
      (candidatureInterimSubject: any[]) => {
        this.candidatureInterimSubject.next(candidatureInterimSubject);
      });
    return this.candidatureInterimSubject;
  }

  // recuperation des candidatures des interimaire pour les clients
  get_candidatures_for_clients(id_client): BehaviorSubject<any[]> {
    this.http.get<any[]>(this.resourceUrl + 'getCandidaturesByMissionForClient/' + id_client).pipe(
      retry(3), // retry a failed request up to 3 times
    ).subscribe(
      (candidaturesByMissionForClientSubject: any[]) => {
        this.candidaturesByMissionForClientSubject.next(candidaturesByMissionForClientSubject);
      });
    return this.candidaturesByMissionForClientSubject;
  }

  // traitement de candidatures selectionnees
  set_candidatures_for_clients(condition_med = null, mail = true, listesDateATraiter) {
    return this.http.post(this.resourceUrl + 'traitementCandidature',
      {
        listesDateATraiter,
        condition_med,
        mail
      });
  }

  update(candidature: Candidature): Observable<EntityResponseType> {
    return this.http.put<Candidature>(this.resourceUrl, candidature, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Candidature>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Candidature[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAllCandidaturesValidees(data: any = null) {
    return this.http.post(this.resourceUrl + 'get_all_candidatures_validees', data).pipe(
      retry(3), // retry a failed request up to 3 tim
    );
  }

  getAllCandidatures(data: any = null) {
    return this.http.post(this.resourceUrl + 'get_all_candidatures', data).pipe(
      retry(3), // retry a failed request up to 3 tim
    );
  }

  getContratByClient(id) {
    return this.http.get<any>(this.resourceUrl + 'contrats-by-client/' + id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  getCandidaturesByIdMM(idMission, idInterim) {
    return this.http.get<any>(this.resourceUrl + 'candidatures-by-misssion/' + idMission + '/' + idInterim).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }


  // cette fonction envoie les candidatures interimaires au CH
  activerLesCandidaturesMedecins(listesCandidatures) {
    return this.http.post(this.resourceUrl + 'activer_candidatures_medecins', listesCandidatures).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  // cette fonction met les candidatures en situation non pris en charge
  CandidaturesNonPrisesEnCharge(listesCandidatures) {
    return this.http.post(this.resourceUrl + 'candidatures_non_pris_en_compte', listesCandidatures).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public updateIdPld(candidature) {
    return this.http.post(this.resourceUrl + 'update-candidature-id-pld', candidature).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }
  public addIdContratPldToCandidature(data) {
    return this.http.post(this.resourceUrl + 'add-idPldContrat-to-candidature', data).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public findByIdpl(id) {
    return this.http.get(this.resourceUrl + 'find-by-idpld/' + id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public findCandidaturesByIdContratPld(id) {
    return this.http.get(this.resourceUrl + 'find-by-idContrat-pld/' + id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public findCandidaturesByIdRenum(id) {
    return this.http.get(this.resourceUrl + 'find-by-idRenum/' + id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public findCandidaturesByIdRenumeration(id) {
    return this.http.get(this.resourceUrl + 'find-by-idRenumeration/' + id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public onGenereContratPdf(data) {
    return this.http.post(this.resourceUrl + 'contrat', data).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }

  public onDetailPaie(id) {
    return this.http.get(this.resourceUrl + 'paie-contrat/' + id).pipe(
      retry(3), // retry a failed request up to 3 times
    );
  }


  public getListCandidatureByMissionMedecinID(id) {
    return this.http.post(this.resourceUrl + 'get_all_candidatures_mission_medecin/' + id, null).pipe(
      retry(3),
    );
  }
}
