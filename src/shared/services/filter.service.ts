import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Candidature} from '../models/interim/Candidature.model';
import {filter, retry} from 'rxjs/operators';
import {SERVER_API_URL} from '../utils/server_api_url';
import {from} from 'rxjs';
import {FormControl} from '@angular/forms';

export interface planning_I {
  annee: string;
  date: number;
  dateFormat: string;
  debut: string;
  etablissement: string;
  etablissement_id: number;
  fiche_specialite_id: number;
  fin: string;
  mois: string;
  planning_id: number;
  salaire: string;
  specialite: string;
  statut: string;
  tarif: string
}

type EntityResponseType = HttpResponse<Candidature>;
type EntityArrayResponseType = HttpResponse<Candidature[]>;

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private resourceUrl = SERVER_API_URL;

  constructor(private http: HttpClient) {
  }

  initFormFilter(elements, formFilter) {
    elements.forEach((x) => {
      formFilter.addControl(x, new FormControl());
    });
    const formTmp = Object.keys(formFilter.value);
    formTmp.forEach((y) => {
      const n = elements.includes(y);
      if (!n) {
        formFilter.removeControl(y);
      }
    });
    return formFilter;
  }

  setControlsOnFormFilter(elements, filterValues, formFilter) {
    elements.forEach((x) => {
      const xx = filterValues ? filterValues[x] : null;
      formFilter.addControl(x, new FormControl(xx));
    });
    return formFilter;
  }

  sendFilters(formFilter) {
    const tmpValFilter = Object.keys(formFilter.value);
    let isNull = true;
    tmpValFilter.forEach((x) => {
      if ( x !== 'date' && formFilter.get(x).value === null) {
        isNull = false;
      } else {
        isNull = true;
      }
    });
    return isNull;
  }
}
