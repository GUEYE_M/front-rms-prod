import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  constructor() {}
  isCandidat(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleCandidat = 'ROLE_CANDIDAT';
    if (level >= 1 || roles.includes(roleCandidat)) {
      rep = true;
    }
    return rep;
  }
  isCandidatUnique(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleCandidat = 'ROLE_CANDIDAT';
    if (roles.includes(roleCandidat)) {
      rep = true;
    }
    return rep;
  }
  isRecruteur(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleRecruteur = 'ROLE_RECRUTEUR';
    if (level >= 1 || roles.includes(roleRecruteur)) {
      rep = true;
    }
    return rep;
  }
  isRecruteurUnique(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleRecruteur = 'ROLE_RECRUTEUR';
    if (roles.includes(roleRecruteur)) {
      rep = true;
    }
    return rep;
  }
  isCommercial(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleCommercial = 'ROLE_COMMERCIAL';
    if (level >= 2 || roles.includes(roleCommercial)) {
      rep = true;
    }
    return rep;
  }
  isCommercialUnique(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleCommercial = 'ROLE_COMMERCIAL';
    if (roles.includes(roleCommercial)) {
      rep = true;
    }
    return rep;
  }
  isSuperviseur(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleSuperviseur = 'ROLE_SUPERVISEUR';
    if (level >= 3 || roles.includes(roleSuperviseur)) {
      rep = true;
    }
    return rep;
  }
  isSuperviseurUnique(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleSuperviseur = 'ROLE_SUPERVISEUR';
    if (roles.includes(roleSuperviseur)) {
      rep = true;
    }
    return rep;
  }
  isComptable(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleComptable = 'ROLE_COMPTABLE';
    if (level >= 4 || roles.includes(roleComptable)) {
      rep = true;
    }
    return rep;
  }
  isComptableUnique(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleComptable = 'ROLE_COMPTABLE';
    if (roles.includes(roleComptable)) {
      rep = true;
    }
    return rep;
  }
  isManager(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleManager = 'ROLE_MANAGER';
    if (level >= 4 || roles.includes(roleManager)) {
      rep = true;
    }
    return rep;
  }
  isManagerUnique(roles): boolean {
    const level = this.returnLevel(roles);
    let rep: any = false;
    const roleManager = 'ROLE_MANAGER';
    if (roles.includes(roleManager)) {
      rep = true;
    }
    return rep;
  }
  isSuperAdmin(roles): boolean {
    const level = this.returnLevel(roles);
    let rep = false;
    const roleSuperAdmin = 'ROLE_SUPER_ADMIN';
    if (roles.includes(roleSuperAdmin) && level === 5) {
      rep = true;
    }
    return rep;
  }
  // nous retourne le level de lautorisation en fonction du role lutilisateur connecte
  returnLevel(roles): any {
    let level = 0;
    const roleTab = [
      {'role': 'ROLE_CANDIDAT', 'level': 1},
      {'role': 'ROLE_RECRUTEUR', 'level': 1},
      {'role': 'ROLE_COMMERCIAL', 'level': 2},
      {'role': 'ROLE_SUPERVISEUR', 'level': 3},
      {'role': 'ROLE_COMPTABLE', 'level': 4},
      {'role': 'ROLE_MANAGER', 'level': 4},
      {'role': 'ROLE_SUPER_ADMIN', 'level': 5},
    ];
    roleTab.forEach((element) => {
      if (roles && roles.includes(element['role'])) {
        level = element['level'];
      }
    });
    return level;
  }
}
