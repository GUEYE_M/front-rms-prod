import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { SENDIN_BLUE_API_KEY, SENDIN_BLUE_URL, SERVER_API_URL } from '../utils/server_api_url';

@Injectable({
	providedIn: 'root'
})
export class SendinblueService {
	data_statistique: any[];
	list_mail_date: any[];
	nombre_email_envoyer_par_date: BehaviorSubject<any> = new BehaviorSubject(null);
	limit: string = '12';
	event: any = null;
	email: any = null;
	date_debut = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
	date_fin = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
	//nombre_  :BehaviorSubject<number> = new BehaviorSubject(0);
	statistiqueSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
	logMaiParDateSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

	private resourceUrl = SENDIN_BLUE_URL;
	private ressourceApiUrl = SERVER_API_URL;

	constructor(private http: HttpClient, private datePipe: DatePipe) { }
	emitDataStatistique() {
		this.logMaiParDateSubject.next(this.data_statistique);
	}

	list() {
		// this.showLoadClient.next(false);
		return this.http.get<any[]>(this.resourceUrl + 'transactionnel-statistic').pipe(retry(3));
	}

	create(email: any) {
		return this.http.post<any>(this.resourceUrl + 'new', email).pipe(retry(3));
	}
	update(id: number, message: any) {
		return this.http.post<any>(this.resourceUrl + 'update/' + id, message).pipe(retry(3));
	}

	find(id: number) {
		return this.http.get(this.resourceUrl + id).pipe(retry(3));
	}
	//  cette function les mails de transactions

	sendEmail(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/send-email-transactionnel', data).pipe(retry(3));
	}

	sendSMS(data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			})
		};

		return this.http.post<any>(this.resourceUrl + 'transactionalSMS/sms', data, httpOptions).pipe(retry(3));
	}

	sendEmailPropositionCollaborartion(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/send-email-collaboration-interim', data).pipe(retry(3));
	}

	sendEmailDemandeDisponibilite(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/demande-disponibilite', data).pipe(retry(3));
	}
	sendEmailDemandecoordonneeMobile(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/demande-mobile-phone', data).pipe(retry(3));
	}
	sendEmailichePoste(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/send-email-fiche-poste', data).pipe(retry(1));
	}
	sendMdd(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/send-mail-diffusion', data).pipe(retry(3));
	}
	allEventActivity() {
		return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/events').pipe(retry(3));
	}

	reportActivity() {
		let date_debut = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
		let date_fin = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			}),
			params: new HttpParams().set('startDate', date_debut).set('endDate', date_fin)
		};

		return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/aggregatedReport', httpOptions).pipe(retry(3));
	}

	allemailCampaigns() {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			})
		};
		return this.http.get<any[]>(this.resourceUrl + 'emailCampaigns', httpOptions).pipe(retry(3));
	}

	allemailContacs() {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			})
		};
		return this.http.get<any[]>(this.resourceUrl + 'contacts', httpOptions).pipe(retry(3));
	}

	// return par defautt les mail envoyer par la date d'aujordhui
	reportsMailDay(startDate = null, endDate = null) {
		if (startDate != null && endDate != null) {
			const httpOptions = {
				headers: new HttpHeaders({
					'Content-Type': 'application/json',
					'api-key': SENDIN_BLUE_API_KEY
				}),

				params: new HttpParams().set('startDate', startDate).set('endDate', endDate).set('limit', '1')
			};

			return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/reports', httpOptions).pipe(retry(3));
		} else {
			const httpOptions = {
				headers: new HttpHeaders({
					'Content-Type': 'application/json',
					'api-key': SENDIN_BLUE_API_KEY
				}),

				params: new HttpParams().set('days', '1').set('limit', '1')
			};

			return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/reports', httpOptions).pipe(retry(3));
		}
	}
	// return l'histprique des mails envoyés
	reportsLogDay(startDate: any, endDate: any, offset: any = 0, event: string = '') {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			}),

			params: new HttpParams()
				.set('startDate', startDate)
				.set('endDate', endDate)
				.set('limit', this.limit)
				.set('offset', offset)
		};

		return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/events', httpOptions).pipe(retry(3));
	}

	filterEvent(startDate: any, endDate: any, offset: any = 0, event: string) {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			}),

			params: new HttpParams()
				.set('startDate', startDate)
				.set('endDate', endDate)
				.set('limit', this.limit)
				.set('offset', offset)
				.set('event', event)
		};

		return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/events', httpOptions).pipe(retry(3));
	}

	filterEmail(startDate: any, endDate: any, offset: any = 0, email: string) {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'api-key': SENDIN_BLUE_API_KEY
			}),

			params: new HttpParams().set('limit', this.limit).set('offset', offset).set('email', email)
		};

		return this.http.get<any[]>(this.resourceUrl + 'smtp/statistics/events', httpOptions).pipe(retry(3));
	}

	sendMailIdentifiants(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/identifiants-user', data).pipe(retry(3));
	}

	sendMailDemandeDossier(data: any) {
		return this.http.post<any>(this.ressourceApiUrl + 'sendinblue/demande-dossier', data).pipe(retry(3));
	}
}
