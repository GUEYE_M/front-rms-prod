import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Qualification } from '../models/Qualification.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { SERVER_API_URL } from '../utils/server_api_url';
import { createRequestOption } from '../utils/request-util';

type EntityResponseType = HttpResponse<Qualification>;
type EntityArrayResponseType = HttpResponse<Qualification[]>;

@Injectable({
  providedIn: 'root'
})
export class QualificationService {

  private resourceUrl = SERVER_API_URL;
  messageErrors: string;
  public QualificationSubject: BehaviorSubject<Qualification[]> = new BehaviorSubject(null);
  public QualificationForMedSubject: BehaviorSubject<Qualification[]> = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
  ) {
  }

  // Recuperation de toutes les qualifications
  getQualifications(): BehaviorSubject<Qualification[]> {
    this.http.get(this.resourceUrl + 'qualifications')
      .pipe(
        retry(3), // retry a failed request up to 3 times
      )
      .subscribe(
        (qualification: Qualification[]) => {
          this.QualificationSubject.next(qualification);
        }
      );
    return this.QualificationSubject;
  }
  // Recuperation de toutes les qualifications du medecin
  getQualificationsForMed(id: number) {
    return this.http.get(this.resourceUrl + 'qualif-medecin/' + id)
      .pipe(
        retry(3), // retry a failed request up to 3 times
      );
  }
  // creation de specialites pour le medecin
  create(id: number, qualification: Qualification[], mail = true): Observable<EntityResponseType> {
    return this.http.post<Qualification>(this.resourceUrl + 'addQualifications/' + id, { qualification, mail }, { observe: 'response' });
  }

  update(id: number, qualification): Observable<EntityResponseType> {
    return this.http.post<Qualification>(this.resourceUrl + 'editQualifications/' + id, qualification, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Qualification>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Qualification[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number, qualification: Qualification): Observable<HttpResponse<any>> {
    return this.http.post<Qualification>(this.resourceUrl + 'deleteQualifications/' + id, qualification, { observe: 'response' });
  }
  // creation de qualification
  newQualification(qualification: Qualification[]) {
    return this.http.post(this.resourceUrl + 'qualification/create', qualification);
  }
  // suppression de qualification
  deleteQualification(qualification) {
    return this.http.post(this.resourceUrl + 'qualification/delete', qualification);
  }
  // modification de qualification
  updateQualification(qualification) {
    return this.http.post(this.resourceUrl + 'qualification/update', qualification);
  }
}
