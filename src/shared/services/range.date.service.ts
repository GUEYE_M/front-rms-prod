import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';

@Injectable({
  providedIn: 'root'
})
export class RangeDateService {

  range:Range = {fromDate:new Date(), toDate: new Date()};
  options:NgxDrpOptions;
  presets:Array<PresetItem> = [];
  customRanges = [

  
    {
      text: 'cette année', desc: 'cette année', value: 'thisyear',
      start: moment().startOf('year'),
      end: moment().endOf('year'),
      
    },
    
    {
      text: 'Aujourd\'\hui', desc: 'Aujourd\'\hui', value: 'today',
      start: moment(),
      end: moment(),
    
    },
    {
      text: '3 derniers mois', desc: '3 derniers mois', value: 'last3month',
      start: moment().subtract(3, 'month').startOf('month'),
      end: moment().subtract(1, 'month').endOf('month')
    },
    {
      text: '6 derniers mois', desc: '6 derniers mois', value: 'lastmonth',
      start: moment().subtract(6, 'month').startOf('month'),
      end: moment().subtract(1, 'month').endOf('month')
    },

    {
      text: 'l\'\année dernieres', desc: 'l\'\année dernieres', value: 'lastyear',
      start: moment().subtract(1, 'year').startOf('year'),
      end: moment().subtract(1, 'year').endOf('year')
    }
  ];

  ranges: any = {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  }
  constructor() {

    
    
   }

   datepickerConfig()
   {
     const today = new Date();
     const fromMin = new Date(today.getFullYear(), today.getMonth()-2, 1);
     const fromMax = new Date(today.getFullYear(), today.getMonth()+1, 0);
     const toMin = new Date(today.getFullYear(), today.getMonth()-1, 1);
     const toMax = new Date(today.getFullYear(), today.getMonth()+2, 0);
  
     this.setupPresets();
     this.options = {
                     presets: this.presets,
                     format: 'mediumDate',
                     range: {fromDate:today, toDate: today},
                     applyLabel: "Submit",
                     calendarOverlayConfig: {
                       shouldCloseOnBackdropClick: false,
                       hasBackdrop: false
                     }
                     // cancelLabel: "Cancel",
                     // excludeWeekends:true,
                     // fromMinMax: {fromDate:fromMin, toDate:fromMax},
                     // toMinMax: {fromDate:toMin, toDate:toMax}
                   };
   }
 
   setupPresets() {
 
    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    }
    
    const today = new Date();
    const yesterday = backDate(1);
    const minus7 = backDate(7)
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth()+1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth()-1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);
    
    this.presets =  [
      {presetLabel: "Yesterday", range:{ fromDate:yesterday, toDate:today }},
      {presetLabel: "Last 7 Days", range:{ fromDate: minus7, toDate:today }},
      {presetLabel: "Last 30 Days", range:{ fromDate: minus30, toDate:today }},
      {presetLabel: "This Month", range:{ fromDate: currMonthStart, toDate:currMonthEnd }},
      {presetLabel: "Last Month", range:{ fromDate: lastMonthStart, toDate:lastMonthEnd }}
    ]
  }
}
