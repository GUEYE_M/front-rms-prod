import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';

import { Observable } from 'rxjs';
import {SERVER_API_URL} from '../utils/server_api_url';
import {Adresse} from '../models/Adresse.model';
import {createRequestOption} from '../utils/request-util';
type EntityResponseType = HttpResponse<Adresse>;
type EntityArrayResponseType = HttpResponse<Adresse[]>;

@Injectable({
  providedIn: 'root'
})
export class AddresseService {

  private resourceUrl = SERVER_API_URL + '/adresse';
  messageErrors: string;
  constructor(private http: HttpClient) { }

create(adresse: Adresse): Observable<EntityResponseType> {
    return this.http.post<Adresse>(this.resourceUrl, adresse, { observe: 'response' });
  }

update(adresse: Adresse): Observable<EntityResponseType> {
    return this.http.put<Adresse>(this.resourceUrl, adresse, { observe: 'response' });
}

find(id: number): Observable<EntityResponseType> {
    return this.http.get<Adresse>(`${this.resourceUrl}/${id}`, { observe: 'response' });
}

query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Adresse[]>(this.resourceUrl, { params: options, observe: 'response' });
}

delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
}


}
