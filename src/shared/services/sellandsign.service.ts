import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { retry, map, tap } from 'rxjs/operators';
import { Observable, observable, BehaviorSubject, from } from 'rxjs';
import { contractSelector } from '../models/erp/ContratctSelector.model';
import { SERVER_API_URL } from '../utils/server_api_url';

@Injectable({
	providedIn: 'root'
})
export class SellandsignService {
	constructor(private http: HttpClient) { }
	private api_ressource_url = SERVER_API_URL;
	toke_api = 'RMSPRODU%7CciRgF7qIorbN3%2F4GP%2FrmjsF0tgM21nCvCg8%2F0irZHDg%3D';
	public showLoadContrat: BehaviorSubject<any> = new BehaviorSubject(true);
	public total_contrat: BehaviorSubject<Number> = new BehaviorSubject(0);
	public contrat_encours: BehaviorSubject<Number> = new BehaviorSubject(0);
	public contrat_valider: BehaviorSubject<Number> = new BehaviorSubject(0);
	public contrat_a_valider: BehaviorSubject<Number> = new BehaviorSubject(0);
	public contrat_archiver: BehaviorSubject<Number> = new BehaviorSubject(0);
	public contrat_bientot_expirer: BehaviorSubject<Number> = new BehaviorSubject(0);
	public contrat_abandonnee: BehaviorSubject<Number> = new BehaviorSubject(0);

	// recuper un client sell and sign par son numer
	findContratIdbyIdPldAndApartenace(id_pld, apartenance: number) {
		let data = {
			id_pld: id_pld,
			apartenance: apartenance
		};
		return this.http.post(this.api_ressource_url + 'contrat/show-by-id-pld', data).pipe(retry(3));
	}

	getDocumentPdfSellAndSign(contrat_id: number) {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			})
		};

		return this.http
			.get(
				'https://cloud.sellandsign.com/calinda/hub/selling/do?m=getCurrentDocumentForContract&id=' +
				contrat_id +
				'&j_token=' +
				this.toke_api,
				{ responseType: 'blob' }
			)
			.pipe(retry(3));
	}

	annulerContrat(contrat_id: number) {
		let data = {
			contrat_id: contrat_id
		};
		return this.http.post(this.api_ressource_url + 'annuler-contrat-sell-and-sign', data).pipe(retry(3));
	}

	getDocumentSigné(contrat_id: number): Observable<Blob> {
		return this.http
			.get(
				'https://cloud.sellandsign.com/calinda/hub/selling/do?m=getSignedContract&contract_id=' +
				contrat_id +
				'&j_token=' +
				this.toke_api,
				{ responseType: 'blob' }
			)
			.pipe(retry(3));
	}

	getDossierPreuve(contrat_id: number): Observable<Blob> {
		return this.http
			.get(
				'https://cloud.sellandsign.com/calinda/hub/selling/do?m=getEvidences&contract_id=' +
				contrat_id +
				'&j_token=Zkb3%216%7CxwPRpen0b3fTIRqeYdqxC0udSukv8Dgb9SRLlLlAJ8eU5Nb2t8sdIA%3D%3D',
				{ responseType: 'blob' }
			)
			.pipe(retry(3));
	}

	relancerContrat(data: any) {

		return this.http
			.post(
				'https://cloud.sellandsign.com/calinda/hub/selling/do?m=animateContractorsForContract&j_token=' +
				this.toke_api,
				data
			)
			.pipe(retry(3));
	}

	getListSignataire(data: any) {
		return this.http
			.post(
				'https://cloud.sellandsign.com/calinda/hub/selling//model/contractorstatus/list?action=loadContractorsStatusForContractDashboard&j_token=' +
				this.toke_api,
				data
			)
			.pipe(retry(3));
	}

	getAllContratBd(data: any) {
		return this.http.post<any>(this.api_ressource_url + 'list_contrat', data).pipe(retry(3));
	}

	listContratByStatut(
		statut: number,
		size: number,
		limit: number,
		type: string = '1',
		ville: string = '',
		raison_social: string = '',
		vendor_email: string = '',
		start_date: number = 0,
		end_date: number = 0
	) {
		let contrat = new contractSelector(
			statut,
			'',
			vendor_email,
			raison_social,
			'',
			start_date,
			end_date,
			'',
			ville,
			type,
			'0',
			size,
			limit,
			'',
			{}
		);
		return this.http.post(
			'https://cloud.sellandsign.com/calinda/hub/selling/do?m=getContracts&j_token=Zkb3%216%7CxwPRpen0b3fTIRqeYdqxC0udSukv8Dgb9SRLlLlAJ8eU5Nb2t8sdIA%3D%3D',
			contrat
		);
	}

	// fonction validation contrat
	validationContrat(data: any) {
		return this.http.post(this.api_ressource_url + 'validation-contrat-sell-and-sign', data).pipe(retry(3));
	}

	// fonction qui supprime le contrat sellAndSign sur notre BD
	deleteContrat(data: any) {
		return this.http.post(this.api_ressource_url + 'delete-contrat-sell-and-sign', data).pipe(retry(3));
	}

	public sendContratToSign(data) {
		data;
		return this.http.post(this.api_ressource_url + 'contrat/send-contrat-to-sign', data).pipe(retry(3));
	}

	public getStatutContrat(id: number) {
		const data = {
			contrat: id
		};
		return this.http.post(this.api_ressource_url + 'statut-contrat-sell-and-sign', data).pipe(retry(3));
	}

	custumlist(list: any) {
		const listContrat: any[] = [];
		from(list)
			.pipe(
				map((contrat: any) => {
					const mission = contrat.messageTitle.split('-');
					const filename = contrat.filename.split('.');

					return {
						id: contrat.id,
						status: contrat.status,
						customer: contrat.customer ? contrat.customer : null,
						filename: filename[0],
						mission: mission[2] + '-' + mission[3] + '-' + mission[4] + '-' + mission[5] + '-' + mission[6],
						date: new Date(contrat.date).toLocaleDateString()
					};
				}),
				tap((contrat) => {
					listContrat.push(contrat);
				})
			)
			.subscribe();

		return listContrat;

		//this.length = this.recruteurlist.length;
	}
}
