import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { Interim } from '../../models/interim/Interim.model';
import { retry } from 'rxjs/operators';
import { SERVER_API_URL } from '../../utils/server_api_url';
import { createRequestOption } from '../../utils/request-util';
import { RolesService } from '../roles.service';

type EntityResponseType = HttpResponse<Interim>;
type EntityArrayResponseType = HttpResponse<Interim[]>;

@Injectable({
	providedIn: 'root'
})
export class InterimService {
	labelTab: BehaviorSubject<number> = new BehaviorSubject(0);
	labelTab_text: BehaviorSubject<any> = new BehaviorSubject('default');
	currentInterimaire: BehaviorSubject<any> = new BehaviorSubject(null);
	listes_mois_disponibilite = [];
	messageErrors: string;
	listInterim: any[];
	public interim_choisi: any;
	public showAlertSubjectInterim: BehaviorSubject<any> = new BehaviorSubject(null);
	public loader: BehaviorSubject<any> = new BehaviorSubject(false);
	//Contient l'ensemble des donnees de linterimaire
	interimGlobalDataSubject: BehaviorSubject<any> = new BehaviorSubject([]);
	listesNomSpeciliatesForInterim: BehaviorSubject<any> = new BehaviorSubject([]);
	interimSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
	public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
	public showLoadInterim: BehaviorSubject<any> = new BehaviorSubject(true);
	public resourceUrl = SERVER_API_URL + 'interim/';

	constructor(private http: HttpClient, private roleService: RolesService) { }

	interimsInactifs() {
		return this.http.get(`${this.resourceUrl}interims-inactifs`).pipe(retry(3));
	}

	create(interim: any) {
		return this.http.post<Interim>(this.resourceUrl + 'new', interim).pipe(retry(3));
	}

	acompte(data: any) {
		return this.http.post<Interim>(this.resourceUrl + 'acompte', data).pipe(retry(3));
	}

	missionByInterim(id: any) {
		return this.http.get<Interim>(this.resourceUrl + 'mission-interim/' + id).pipe(retry(3));
	}

	update(id: number, interim: Interim): Observable<EntityResponseType> {
		return this.http.put<Interim>(this.resourceUrl + id + '/update', interim, { observe: 'response' });
	}

	find(id: number) {
		return this.http.get<Interim>(`${this.resourceUrl}${id}`).pipe(retry(3));
	}
	public addIdInterimPldToInterim(data) {
		return this.http.post(this.resourceUrl + 'add-idPldInterim-to-interim', data).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}
	query(req?: any): Observable<EntityArrayResponseType> {
		const options = createRequestOption(req);
		return this.http.get<Interim[]>(this.resourceUrl, { params: options, observe: 'response' });
	}
	delete(interims: any) {
		return this.http.post<Interim[]>(this.resourceUrl + 'delete', interims).pipe(retry(3));
	}

	// fonction pour supprimer un ou plusieures interimaires sur list interim
	deleteInterims(data: any) {
		return this.http.post<any>(this.resourceUrl + 'delete-interim', data).pipe(retry(3));
	}
// fonction pour archiver un ou plusieures interimaires sur list interim
	public archiverInterim = (data: any) => {
		
		return this.http.post<any>(this.resourceUrl + 'archived', data).pipe(retry(3));
	}
	getInterim() {
		return JSON.parse(localStorage.getItem('single-interim'));
	}

	getInterimMission() {
		return JSON.parse(localStorage.getItem('mission-interim'));
	}

	emitInterim() {
		this.interimSubject.next(this.listInterim);
	}

	// recupere la list des interims selon les parametres inscrit et speialite
	getAllInterimByspecialiteAndInscription(data: any) {
		return this.http.post<any[]>(this.resourceUrl + 'interim-by-specialite-and-inscription', data).pipe(retry(3));
	}

	activerOuDesactiveInscription(utilisateur) {
		return this.http.post(this.resourceUrl + 'activer-ou-desactiver-inscription', utilisateur);
	}

	actifOuInactif(utilisateur) {
		return this.http.post(this.resourceUrl + 'actif-ou-inactif', utilisateur);
	}

	getAllInterimCopie(data: any = null) {
		this.showLoadInterim.next(false);
		return this.http.post<Interim[]>(this.resourceUrl + 'list', data).pipe(retry(3));
	}

	getDossierInterim(id_interim) {
		return this.http.get(this.resourceUrl + 'get_etat_dossier/' + id_interim).pipe(retry(3));
	}

	getNomSpecialitesForInterim(id_interim) {
		return this.http.get(this.resourceUrl + 'my_noms_specialite/' + id_interim).pipe(retry(3));
	}

	updateItem(interimaire: any, idInterim: number) {
		return this.http.post(this.resourceUrl + 'updateItem/' + idInterim, interimaire);
	}

	attributSansUnderscore(item) {
		let newItem: string;
		for (let i = 0; i < item.length; i++) {
			if (i !== 0 && item.charAt(i) === '_') {
				newItem += item.charAt(i + 1).toUpperCase();
				i = i + 1;
			} else {
				newItem += item.charAt(i);
			}
		}
		newItem = newItem.substring(9, newItem.length);

		return newItem;
	}

	updateEnable(data: any) {
		return this.http.post<any>(this.resourceUrl + 'update-enable', data).pipe(retry(3));
	}

	getStatistiqueByYear(data: any) {
		return this.http.post<any>(this.resourceUrl + 'statistique-candidature', data).pipe(retry(3));
	}

	getStatistiqueProfil(data: any) {
		return this.http.post<any>(this.resourceUrl + 'statistique-profil', data).pipe(retry(3));
	}

	querySearch(data: any) {
		return this.http.post<any>(this.resourceUrl + 'query', data).pipe(retry(3));
	}

	displayedColumnsName() {
		let element = [
			{
				name: 'civilite',
				label: 'Civilite'
			},
			{
				name: 'specialite',
				label: 'Specialite'
			},
			{
				name: 'nom',
				label: 'Nom'
			},
			{
				name: 'prenom',
				label: 'Prenom'
			},

			{
				name: 'telephone',
				label: 'Telephone'
			},
			{
				name: 'email',
				label: 'E-Mail'
			},
			{
				name: 'actif',
				label: 'Actif'
			},
			{
				name: 'username',
				label: 'Nom Utilisateur'
			},

			{
				name: 'addresse',
				label: 'Adresse'
			},
			{
				name: 'ville',
				label: 'Ville'
			},
			{
				name: 'region',
				label: 'region'
			},
			{
				name: 'departement',
				label: 'Departement'
			},
			{
				name: 'inscription',
				label: 'Inscription'
			},
			{
				name: 'activer',
				label: 'Activer'
			},

			{
				name: 'contact',
				label: 'P.contact'
			},
			{
				name: 'horaire',
				label: 'H.joignabilité'
			},

			{
				name: 'date',
				label: 'Date Enregistrement'
			},

			{
				name: 'auteur',
				label: 'Auteur Enregistrement'
			},
			{
				name: 'dossier',
				label: 'Dossier Valide'
			},
			{
				name: 'source',
				label: 'Source'
			},

			{
				name: 'last_login',
				label: 'date dernière connexion'
			},
			{
				name: 'autre_remarque',
				label: 'Autre Remarque'
			},
			{
				name: 'remarque',
				label: 'Remarque'
			},


			{
				name: 'cible',
				label: 'Cible'
			},
			{
				name: 'chAEviter',
				label: 'CH A Eviter'
			}
		];

		return element;
	}
}
