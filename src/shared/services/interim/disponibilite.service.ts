import { Injectable } from '@angular/core';
import { Disponibilite } from '../../models/interim/Disponibilite.model';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

type EntityResponseType = HttpResponse<Disponibilite>;
type EntityArrayResponseType = HttpResponse<Disponibilite[]>;
import { retry } from 'rxjs/operators';
import { SERVER_API_URL } from '../../utils/server_api_url';
import { stringify } from 'querystring';
import { MatDialogRef } from '@angular/material/dialog';
import { ModalDisponibiliteGestionComponent } from '../../modules/erp/erp-container/modules/interim/modal-interim/modal-disponibilite-gestion/modal-disponibilite-gestion.component';

@Injectable({
	providedIn: 'root'
})
export class DisponibiliteService {
	interimIdDis = 0;
	dialogRef: MatDialogRef<ModalDisponibiliteGestionComponent>;
	public dateDebut: any;
	messageErrors: string;
	alertDisponibilite: any;
	highlightDays = [];
	loaderInterim: BehaviorSubject<Boolean> = new BehaviorSubject(false);
	public disponibiliteSubject = new BehaviorSubject([]);
	public listeDisponibiliteChoisiSubject = new BehaviorSubject([]);
	public listeDisponibiliteOffreMatchingSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
	public listeDisponibilitesForInterimSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
	private resourceUrl = SERVER_API_URL;

	constructor(private http: HttpClient, private router: Router) {}

	// Recuperation des fiches client

	getDisponibilite(idInterim) {
		return this.http.get(this.resourceUrl + 'interim-disponibilite/' + idInterim).pipe(retry(3));
	}

	// Recuperation des matching MyRMS
	getDisponibiliteMatchingMyRMS(interimId, nomSpecialite, moisDisponibilite, moisFr, anneeFr): Observable<any> {
		let data = {
			interim_id: interimId,
			nom_specialite: nomSpecialite,
			mois: moisFr,
			mois_disponibilite: moisDisponibilite,
			annee: anneeFr
		};

		return this.http.post(this.resourceUrl + 'disponibilite/matching', { data }).pipe(retry(3));
	}

	create_periode(disponibilite: any): Observable<EntityResponseType> {
		return this.http.post<Disponibilite>(this.resourceUrl + 'create_disponibilitePeriode', disponibilite, {
			observe: 'response'
		});
	}

	deleteChoiceDisponibilite(id: number, disponibilite: any) {
		return this.http.post(this.resourceUrl + 'deleteChoiceDisponibilite/' + id, disponibilite);
	}

	deleteMoisDisponibilite(id: number, disponibilite: any) {
		return this.http.post(this.resourceUrl + 'deleteMoisDisponibilite/' + id, disponibilite);
	}

	create(disponibilite: any) {
		return this.http.post<Disponibilite>(this.resourceUrl + 'disponibilite', disponibilite);
	}

	update(disponibilite: Disponibilite): Observable<EntityResponseType> {
		return this.http.put<Disponibilite>(this.resourceUrl, disponibilite, { observe: 'response' });
	}

	delete(disponibilite: any): Observable<EntityResponseType> {
		return this.http.post<Disponibilite>(this.resourceUrl + 'disponibilite/sup', disponibilite, {
			observe: 'response'
		});
	}

	getDisponibiliteByYear(year: any) {
		return this.http.post<Disponibilite>(this.resourceUrl + 'disponibilie/year', year);
	}

	list() {
		return this.http.get<Disponibilite>(this.resourceUrl + 'disponibilie/list');
	}

	listAll(data: any) {
		return this.http.post<any>(this.resourceUrl + 'disponibilie/list-all', data);
	}

	displayFilterName() {
		let element = [
			{
				name: 'date',
				label: 'Date'
			},
			{
				name: 'etablissement',
				label: 'Nom Etablissement'
			},

			{
				name: 'mois',
				label: 'Mois'
			},
			{
				name: 'annee',
				label: 'Annee'
			},
			{
				name: 'heure_debut',
				label: 'Heure Debut'
			},

			{
				name: 'heure_fin',
				label: 'Heure Fin'
			},

			{
				name: 'statut',
				label: 'Statut'
			},
			{
				name: 'tarif',
				label: 'Tarif'
			},

			{
				name: 'salaire',
				label: 'Salaire'
			},
			{
				name: 'departement',
				label: 'Departement'
			},
			{
				name: 'ville',
				label: 'Ville'
			}
		];

		return element;
	}
}
