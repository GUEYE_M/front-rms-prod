import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Justificatif } from '../../models/interim/Justificatif.model';
import { retry } from 'rxjs/operators';
import { SERVER_API_URL } from '../../utils/server_api_url';
import { createRequestOption } from '../../utils/request-util';

type EntityResponseType = HttpResponse<Justificatif>;
type EntityArrayResponseType = HttpResponse<Justificatif[]>;

@Injectable({
	providedIn: 'root'
})
export class JustificatifService {
	private resourceUrl = SERVER_API_URL + 'justificatif/';
	messageErrors: string;

	constructor(private http: HttpClient) {}

	create(disponibilite: Justificatif): Observable<EntityResponseType> {
		return this.http.post<Justificatif>(this.resourceUrl, disponibilite, { observe: 'response' });
	}

	creerPiece(tableau) {
		const id = tableau['id'];
		const files = tableau['files'];
		return this.http.post(this.resourceUrl + 'add-piece/' + id, files);
	}
	new(params) {
		const id = params.id;
		const data = params.data;
		return this.http.post<any>(this.resourceUrl + 'new/' + id, data).pipe(retry(3));
	}
	newWithContrat(params) {
		const id = params.id;
		const data = params.data;
		return this.http.post<any>(this.resourceUrl + 'newWithContrat/' + id, data).pipe(retry(3));
	}

	update(params): Observable<EntityResponseType> {
		const id = params.id;
		const data = params.data;
		return this.http.post<any>(this.resourceUrl + 'update/' + id, data).pipe(retry(3));
	}

	find(id: number): Observable<EntityResponseType> {
		return this.http.get<Justificatif>(`${this.resourceUrl}/${id}`, { observe: 'response' });
	}

	findByMM(id) {
		return this.http.get<any[]>(this.resourceUrl + 'by-mission-medecin/' + id).pipe(retry(3));
	}

	findMMByIdRenumeration(id) {
		return this.http.get<any[]>(this.resourceUrl + 'justificatis-by-renumeration/' + id).pipe(retry(3));
	}

	query(req?: any): Observable<EntityArrayResponseType> {
		const options = createRequestOption(req);
		return this.http.get<Justificatif[]>(this.resourceUrl, { params: options, observe: 'response' });
	}

	delete(id: number) {
		return this.http.get<any>(this.resourceUrl + 'delete-piece/' + id).pipe(retry(3));
	}

	relanceJistificatif(data: any) {
		return this.http.post<any>(this.resourceUrl + 'relance-justificatif', data).pipe(retry(3));
	}

	relanceByIdMM(idMM: any) {
		return this.http.get<any>(this.resourceUrl + 'relance-by-idMM/' + idMM).pipe(retry(3));
	}

	valideOrInvalide(data: any) {
		return this.http.post<any>(this.resourceUrl + 'valide-or-invalide-justificatif', data).pipe(retry(3));
	}

	rappelSecurite(data: any) {
		return this.http.post<any>(this.resourceUrl + 'rapel_securite', data).pipe(retry(3));
	}
}
