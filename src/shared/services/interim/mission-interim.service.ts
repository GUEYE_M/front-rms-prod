import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';

import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { retry } from 'rxjs/operators';
import {Interim} from '../../models/interim/Interim.model';
import {SERVER_API_URL} from '../../utils/server_api_url';


@Injectable({
  providedIn: 'root'
})
export class MissionInterimService {
  private resourceUrl = SERVER_API_URL + 'missions/';
  constructor(private http: HttpClient) {}
  getMissionsMed(): Observable<any> {
    return this.http.get<any>(this.resourceUrl + 'list').pipe(
      retry(3)
    );
  }
  find(id: number): Observable<any> {
    return this.http.get<any>(this.resourceUrl + 'show/' + id).pipe(
      retry(3)
    );
  }

  updateFacturationOrRapelSecu(data:any) {
    return this.http.post<any>(this.resourceUrl + 'facture-or-rapelsecu',data).pipe(
      retry(3)
    );
  }


}
