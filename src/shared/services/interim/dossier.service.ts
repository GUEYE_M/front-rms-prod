import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { Dossier } from '../../models/interim/Dossier.model';
import { retry } from 'rxjs/operators';
import { SERVER_API_AUTH, SERVER_API_URL } from '../../utils/server_api_url';
type EntityResponseType = HttpResponse<Dossier>;
type EntityArrayResponseType = HttpResponse<Dossier[]>;

@Injectable({
	providedIn: 'root'
})
export class DossierService {
	private resourceUrl = SERVER_API_URL;
	private resourceUrlNotAuth = SERVER_API_AUTH;
	showDossierSubject = new BehaviorSubject(false);
	messageErrors: string;
	constructor(private http: HttpClient) {}


	creer(tableau: any) {
		return this.http.post(this.resourceUrl + 'dossier-add/' + tableau.id , tableau.dossier);
	}

	validitePiece(tableau) {
		const id = tableau['id'];
		const validite = tableau['validite'];
		return this.http.post(this.resourceUrl + 'dossier-validitePiece/' + id, validite);
	}

	deleteDossier(tableau) {
		const id = tableau['id'];
		const item = tableau['item'];
		return this.http.post(this.resourceUrl + 'dossier-delete/' + id, item);
	}

	find(id: number): Observable<Dossier> {
		return this.http.get<Dossier>(`${this.resourceUrl}dossier/${id}`).pipe(retry(3));
	}

	findDossierCandidat(id): Observable<Dossier> {
		return this.http.get<Dossier>(`${this.resourceUrlNotAuth}dossier-candidat/${id}`).pipe(retry(3));
	}

	creerDiplome(tableau) {
		const id = tableau['id'];
		const diplome = tableau['diplome'];
		return this.http.post(this.resourceUrl + 'diplome-add/' + id, diplome);
	}

	updateDiplome(tableau) {
		const id = tableau['id'];
		const diplome = tableau['diplome'];
		return this.http.post(this.resourceUrl + 'diplome-update/' + id, diplome);
	}
	deleteDiplome(tableau) {
		const id = tableau['id'];
		const item = tableau['item'];
		return this.http.post(this.resourceUrl + 'diplome-delete/' + id, item);
	}

	soumettreDossier(id: number) {
		return this.http.post(this.resourceUrl + 'dossier-soumettre', JSON.stringify(id));
	}
	activer(id: number, mail: boolean) {
		// return this.http.get<Dossier>(`${this.resourceUrl}activer-dossier/${id}`).pipe(
		//   retry(3)
		// );

		return this.http.post(this.resourceUrl + 'activer-dossier/' + id, mail).pipe(retry(3));
	}
	constitutionDossier(data: any) {
		return this.http.post(this.resourceUrl + 'dossier-constitution', data);
	}

	remettreNameDossier(item) {
		switch (item) {
			case "Pièce d'identité":
				item = 'pieceName';
				break;
			case 'Carte vitale':
				item = 'carteVitaleName';
				break;
			case 'Carte grise':
				item = 'carteGriseName';
				break;
			case 'CV':
				item = 'cvName';
				break;
			case "inscription à l'ordre":
				item = 'inscriptionName';
				break;
			case 'Justificatif de domicile':
				item = 'jdomicileName';
				break;
			case 'RIB':
				item = 'ribName';
				break;
		}
		return item;
	}
}
