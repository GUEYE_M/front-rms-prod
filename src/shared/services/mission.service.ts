import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';
import { Mission } from '../models/interim/Mission.model';
import { retry } from 'rxjs/operators';
import { Candidature } from '../models/interim/Candidature.model';
import { SERVER_API_URL } from '../utils/server_api_url';
import { createRequestOption } from '../utils/request-util';

type EntityResponseType = HttpResponse<Mission>;
type EntityArrayResponseType = HttpResponse<Mission[]>;

@Injectable({
	providedIn: 'root'
})
export class MissionService {
	messageErrors: string;
	//Missions de type behaviorSubject
	public MissionsSubject: BehaviorSubject<Mission[]> = new BehaviorSubject([]);
	public MissionsSubjectClient: BehaviorSubject<Mission[]> = new BehaviorSubject([]);
	public MissionsChoisieSubject: BehaviorSubject<any> = new BehaviorSubject(null);
	private resourceUrl = SERVER_API_URL;

	constructor(private http: HttpClient) {}

	// Recuperation de toutes les missions
	getMissions(data: any = null) {
		return this.http.post<any>(this.resourceUrl + 'missions/listes', data).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	// Recuperation des missions activer pour le client
	getMissionsForClient(id_client: number) {
		return this.http.get<Mission[]>(this.resourceUrl + 'listesMissions/' + id_client).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	enableMission(idmission, enable) {
		return this.http.post(this.resourceUrl + 'enabledMission/' + idmission, JSON.stringify(enable));
	}

	//Choisir une mission
	setMissinChoisie(mission: Mission) {
		this.MissionsChoisieSubject.next(mission);
	}

	create(mission: Mission): Observable<EntityResponseType> {
		return this.http.post<Mission>(this.resourceUrl, mission, {
			observe: 'response'
		});
	}

	update(mission: Mission): Observable<EntityResponseType> {
		return this.http.put<Mission>(this.resourceUrl, mission, {
			observe: 'response'
		});
	}

	find(id: number): Observable<EntityResponseType> {
		return this.http.get<Mission>(`${this.resourceUrl}/${id}`, {
			observe: 'response'
		});
	}

	query(req?: any): Observable<EntityArrayResponseType> {
		const options = createRequestOption(req);
		return this.http.get<Mission[]>(this.resourceUrl, {
			params: options,
			observe: 'response'
		});
	}

	delete(id: number): Observable<HttpResponse<any>> {
		return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
			observe: 'response'
		});
	}

	// Recuperation de toutes les missions
	getMissionsByReference(reference: string) {
		return this.http.get<any>(this.resourceUrl + 'mission-by-reference/' + reference).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	create_RH(rh) {
		return this.http.post<any>(this.resourceUrl + 'createRH', rh, {
			observe: 'response'
		});
	}

	getRhByContrat(idContratPld) {
		return this.http.get<any>(this.resourceUrl + 'rhBycontrat/' + idContratPld).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	updateComRH(data) {
		return this.http.post<any>(this.resourceUrl + 'updateRH', data).pipe(
			retry(3) // retry a failed request up to 3 times
		);
	}

	telechargeRH(data) {
		return this.http.get<any>(this.resourceUrl + 'telechargerRH', data).pipe();
	}

	getPlanningByMission(id) {
		return this.http.get<any>(this.resourceUrl + 'mission/get-planning/' + id).pipe();
	}

	displayFilterName() {
		let element = [
			{
				name: 'etablissement',
				label: 'Nom Etablissement'
			},

			{
				name: 'mois',
				label: 'Mois'
			},
			{
				name: 'annee',
				label: 'Annee'
			},
			{
				name: 'reference',
				label: 'Reference'
			},

			{
				name: 'statut',
				label: 'Statut'
			},

			{
				name: 'enable',
				label: 'Publier'
			},

			{
				name: 'specialite',
				label: 'Specialite'
			},
			{
				name: 'qualification',
				label: 'Qualification'
			},
			{
				name: 'ville',
				label: 'Ville'
			},

			{
				name: 'departement',
				label: 'Departement'
			},

			{
				name: 'auteur',
				label: 'Auteur'
			}
		];

		return element;
	}

	getMissionMachingByDispo(data: any) {
		return this.http.post<any>(this.resourceUrl + 'missions/getDispo', { data }).pipe(retry(3));
	}
}
