import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class FlashMessageService {

  constructor(private snackBar:MatSnackBar) { }


  flashMessage(message : string) {
    this.snackBar.open(message,'cancel',
    {
      duration:5000
    }
    )
  }
  
}
