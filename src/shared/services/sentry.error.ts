import { ErrorHandler } from '@angular/core';
import * as Sentry from '@sentry/browser';
import { environment } from 'src/environments/environment';

export class SentryErrorHandler implements ErrorHandler {
  constructor() {
    Sentry.init({
        dsn: 'https://c40b0a7c44cb46fd9c435080cf086332@sentry.io/1886788'
      });
    // if (environment.production) {
    
    // }
  }

  handleError(error: any) {
    if (environment.production) {
      Sentry.captureException(error.originalError || error);
    }
    console.error(error);
  }
}