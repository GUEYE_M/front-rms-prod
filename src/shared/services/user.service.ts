import {Injectable} from '@angular/core';
import {User} from '../models/User.model';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {retry, tap} from 'rxjs/operators';
import {JwtService} from './jwt..service';
import {NotificationService} from './notification.service';
import {SERVER_API_AUTH, SERVER_API_URL} from '../utils/server_api_url';


@Injectable({
    providedIn: 'root'
})
export class UserService {

    public currentUser: any;
    private resourceUrl = SERVER_API_URL;
  private resourceApiAuth = SERVER_API_AUTH;
  PreviewPage$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(private http: HttpClient,
                private router: Router,
                private notificatiService: NotificationService,
                private notificationService: NotificationService,
                private jwtService: JwtService) {
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('rms-user'));
    }

    public parametreCompte(element: any): Observable<string> {
        const key = element['key'];
        const value = element['value'];
        return this.http.post<string>(this.resourceUrl + 'parametre-compte', element).pipe(
            tap((resp: any) => {
                if (resp === '') {
                    this.currentUser.value[key] = value;
                }
            })
        );
    }
  monProfil() {
    return this.http.get<any>(`${this.resourceUrl + 'mon-profil'}`).pipe(
      retry(3)
    );
  }

    public updatePasswordCompte(tableau) {
        const id = tableau['id'];
        const update_password = tableau['update_password'];
        return this.http.post(this.resourceUrl + 'update-password-compte/' + id, update_password);
    }

    public updatePasswordByAdmin(id) {
        return this.http.post(this.resourceUrl + 'update-password-by-admin/' + id, []);
    }
    updatePhotoProfil(tableau) {
        const id = tableau['id'];
        const photo = tableau['photo'];
        return this.http.post(this.resourceUrl + 'update-photo/' + id, photo);
    }

    activerOuDesactive(data) {
        return this.http.post(this.resourceUrl + 'activer-ou-desactiver', data);
    }
    deleteUser(ids) {
        return this.http.post(this.resourceUrl + 'delete-user', ids);
    }

    listUsers(data: any= null) {
      this.notificatiService.blockUiStart();
        return this.http.post<any[]>(this.resourceUrl + 'list-users', data).pipe(
            retry(3),
        );
    }

    public updateIdPld(user) {
        return this.http.post(this.resourceUrl + 'update-id-pld', user);
    }

    redirectProfile(roles) {
        if (roles.includes('ROLE_ADMIN'))
            this.router.navigate(['RMS-Admin']);
    }

    ajouZerot(number): string {
        if (number < 10)
            number = 0 + '' + number;
        return number;
    }



    displayedColumnsName()
{
  let element = [
    {
      name:"fonction",
      label:"Fonction"
    },
    {
      name:"nom",
      label:"Nom"
    },
    {
      name:"prenom",
      label:"Prenom"
    },

    {
      name:"telephone",
      label:"Telephone"
    },
    {
      name:"email",
      label:"E-Mail"
    },
    {
      name:"activer",
      label:"Etat"
    },
    {
      name:"username",
      label:"Nom Utilisateur"
    },

    {
      name:"adresse",
      label:"Adresse"
    }
  ]

  return element;
}

   refractorLabelUser(item) {
      switch (item) {
        case 'civilite':
          item = 'Civilité';
          break;
        case 'dateEnreg':
          item = 'Membre depuis';
          break;
        case 'complement_adresse':
          item = 'Complément adresse';
          break;
        case 'departement':
          item = 'Département';
          break;
        case 'region':
          item = 'Région';
          break;
        case 'num_secu':
          item = 'N. Sécurité sociale';
          break;
        case 'cp':
          item = 'Code postal';
          break;
        case 'id_pays_naissance':
          item = 'Pays de naissance';
          break;
        case 'date_naissance':
          item = 'date de naissance';
          break;
        case 'lieu_naissance':
          item = 'lieu de naissance';
          break;
        case 'id_nationalite':
          item = 'Nationalité';
          break;
        case 'password':
          item = 'Mot de passe';
          break;
        case 'last_login':
          item = 'dernière connexion';
          break;
      }
      return item;
  }
}
