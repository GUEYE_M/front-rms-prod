import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RedirectionSercive{
    labelTab_client : BehaviorSubject<number> = new BehaviorSubject(0);
    labelTab_interim : BehaviorSubject<number> = new BehaviorSubject(0);
}
