import { Injectable } from '@angular/core';
import {retry} from 'rxjs/operators';
import {SERVER_API_URL} from '../utils/server_api_url';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MissionInterimaireService {
  private resourceUrl = SERVER_API_URL;
  constructor(private http: HttpClient) {}

  getMissionsInterims(data) {
    return this.http.get<any>(this.resourceUrl + 'missions/new-list' , {params: data} ).pipe(
        retry(3) // retry a failed request up to 3 times
    );
  }

  getStatsBarMissionsInterims(data) {
    return this.http.get<any>(this.resourceUrl + 'missions/stats-bars' , {params: data} ).pipe(
        retry(3) // retry a failed request up to 3 times
    );
  }
}
