import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router, Route } from '@angular/router';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from '../../utils/server_api_url';
import { retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TacheService {
  public resourceUrl = SERVER_API_URL + 'tache/';

  public tacheDaySubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

  public tacheEnCoursGestionnaireSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

  public tacheGestionnaireSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

  list_type: any;
  list_taches: any[] = []
  list_taches_gestionnaire: any[] = []
  tache_encours_gestionnaire: any[] = [];

  etats: any[] = [
    { name: 'En Cours', id: 0 },
    { name: 'Terminer', id: 1 },
    { name: 'Annuler', id: 2 },
    { name: 'Non Commencée', id: 3 },
    { name: 'Validée', id: 4 },
    { name: 'Perdue ', id: 5 },
  ];

  listTachesSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  typeTecheSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public closemodaletype: BehaviorSubject<boolean> = new BehaviorSubject(false);



  constructor(private http: HttpClient) {

  }




  emitType() {
    this.typeTecheSubject.next(this.list_type)
  }
  emitTaches() {
    this.listTachesSubject.next(this.list_taches)
  }

  emitTachesGestionnaireEncCours() {

    this.tacheEnCoursGestionnaireSubject.next(this.tache_encours_gestionnaire);
  }

  emitTachesGestionnaire() {
    this.tacheGestionnaireSubject.next(this.list_taches_gestionnaire)
  }

  // Recuperation des taches  du jour
  findByTacheDays(): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>(this.resourceUrl + 'days', { observe: 'response' });
  }

  edit(tache: any) {
    return this.http.post<any[]>(this.resourceUrl + 'edit', tache);

  }

  find(id: number) {
    return this.http.get(this.resourceUrl + id).pipe(
      retry(3),
    )
  }

  list(data: any = null) {

    return this.http.post<any[]>(this.resourceUrl + 'list', data).pipe
      (
        retry(3)
      )
  }




  listTypeTache(data: any = null) {
    return this.http.post<any[]>(SERVER_API_URL + 'type-taches/list', data).pipe(
      retry(3),
    )

  }

  editType(type: any) {

    return this.http.post<any>(SERVER_API_URL + 'type-tache/edit', type).pipe(
      retry(3)
    );
  }
  findTypeById(id: number) {
    return this.http.get<any>(SERVER_API_URL + 'type-tache/' + id).pipe(
      retry(3)
    );
  }


  tacheBydate(data: any) {
    return this.http.post<any[]>(this.resourceUrl + 'date-filter', data).pipe(
      retry(3)
    )
  }

  updateAvancement(data: any) {
    {
      return this.http.post<any[]>(this.resourceUrl + 'update-etat', data).pipe(
        retry(3)
      )
    }
  }

  tacheGestionnaire(id: number) {
    return this.http.get(this.resourceUrl + 'gestionnaire-interne/' + id).pipe(
      retry(3),
    ).subscribe(
      (response: any) => {
        this.list_taches_gestionnaire = response.data
        this.emitTachesGestionnaire()
      })
  }



  displayedColumnsName() {
    let element = [
      {
        name: "nom",
        label: "Nom"
      },

      {
        name: "type",
        label: "Type"
      },
      {
        name: "statut",
        label: "Statut"
      },

      {
        name: "heure_debut",
        label: "Heure Debut"
      },
      {
        name: "heure_fin",
        label: "Heure Fin"
      },

      {
        name: "date",
        label: "Date"
      },

    ]

    return element;
  }



}



