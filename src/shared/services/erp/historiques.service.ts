import {Injectable} from '@angular/core';
import {SERVER_API_URL} from '../../utils/server_api_url';
import {HttpClient} from '@angular/common/http';
import {retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HistoriquesService {
  public resourceUrl = SERVER_API_URL;

  constructor(private http: HttpClient) {

  }

  getHistory() {
    return this.http.get(this.resourceUrl + 'historiques').pipe(
      retry(3),
    );
  }

  getHistoryByPeriode(periode) {
    return this.http.post(this.resourceUrl + 'historiques/getHistoryByPeriode', JSON.stringify(periode)).pipe(
      retry(3),
    );
  }

}
