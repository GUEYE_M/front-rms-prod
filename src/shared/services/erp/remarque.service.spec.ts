import { TestBed } from '@angular/core/testing';

import { RemarqueService } from './remarque.service';

describe('RemarqueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemarqueService = TestBed.get(RemarqueService);
    expect(service).toBeTruthy();
  });
});
