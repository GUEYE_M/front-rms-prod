import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {SERVER_API_URL} from '../../utils/server_api_url';

@Injectable({
  providedIn: 'root'
})
export class BoiteEmailService {

  list_boite_email : any = [];
  boiteEmailSubject :  BehaviorSubject<any[]> = new BehaviorSubject([]);
  public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private resourceUrl = SERVER_API_URL +'boite-email/';
  constructor(private http: HttpClient) {
    this.list();


  }
  emitBoiteEmail()
  {
    this.boiteEmailSubject.next(this.list_boite_email)
  }

  list(data:any=null)
    {
     // this.showLoadClient.next(false);
      return this.http.post(this.resourceUrl + 'list',data).pipe
      (
      retry(3)
      )
    }

    create(email: any) {
      return this.http.post<any>(this.resourceUrl+'new', email).pipe(
        retry(3)
      );
    }
    update(id:number,message: any){
      return this.http.post<any>(this.resourceUrl+'update/'+id, message).pipe(
        retry(3)
      )
}

find(id: number){
  return this.http.get(this.resourceUrl+id).pipe(
      retry(3),
  )
}
}
