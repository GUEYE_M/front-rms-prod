import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'src/shared/utils/server_api_url';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RessourceService {

  private resourceUrl = SERVER_API_URL + 'ressource/';
  constructor(private http: HttpClient) {

  }

  list(data: any = null) {

    return this.http.post<any[]>(this.resourceUrl + 'list', data).pipe
      (
        retry(3)
      )
  }
  edit(ligne: any) {
    return this.http.post<any>(this.resourceUrl + 'edit', ligne).pipe(
      retry(3)
    );
  }



  find(id: number) {
    return this.http.get(this.resourceUrl + id).pipe(
      retry(3),
    )
  }
}
