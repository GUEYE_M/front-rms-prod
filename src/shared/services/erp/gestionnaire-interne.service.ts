import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../utils/server_api_url';
import { retry } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class GestionnaireInterneService {
	public resourceUrl = SERVER_API_URL + 'gestionnaire-interne/';
	constructor(private http: HttpClient) {}

	allGestionnaireInterne() {
		return this.http.get<any[]>(this.resourceUrl + 'list').pipe(retry(3));
	}
}
