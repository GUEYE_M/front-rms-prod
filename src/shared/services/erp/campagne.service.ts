import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {SERVER_API_URL} from '../../utils/server_api_url';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampagneService {

  campagneSubjet :  BehaviorSubject<any[]> = new BehaviorSubject([]);
  list_campagne: any[];
  public closemodale: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private resourceUrl = SERVER_API_URL +'campagne/';
  constructor(private http: HttpClient) {
    this.list();
  }

  emitCampagne()
  {
    this.campagneSubjet.next(this.list_campagne)
  }

  list(data:any=null)
  {

    return this.http.post<any[]>(this.resourceUrl+'list',data).pipe
    (
    retry(3)

    )
  }
  edit(ligne: any) {
    return this.http.post<any>(this.resourceUrl+'edit', ligne).pipe(
      retry(3)
    );
  }



find(id: number){
return this.http.get(this.resourceUrl+id).pipe(
    retry(3),
)
}

}
