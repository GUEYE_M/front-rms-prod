import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import { HOST_SERVEUR_ADMIN, SERVER_API_AUTH, SERVER_API_URL } from '../utils/server_api_url';
import { Router } from '@angular/router';
import { LocaleConfig } from 'ngx-daterangepicker-material';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private resourceUrl = SERVER_API_URL;
  private hostServeurAdmin = HOST_SERVEUR_ADMIN;


  logoRms = environment.urlLogoRms;
  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  indicateurs() {
    return this.http.get(this.resourceUrl + 'indicateur-dasboard').pipe(
      retry(3),
    );
  }
  updateIdClientPLD(data) {
    return this.http.post(this.resourceUrl + 'update-id-client-pld', data).pipe();
  }
  updateIdInterimPLD(data) {
    return this.http.post(this.resourceUrl + 'update-id-interim-pld', data).pipe();
  }
  updateIdContratPLD(data) {
    return this.http.post(this.resourceUrl + 'update-id-contrat-pld', data).pipe();
  }

  getMissionByYear(data: any) {
    return this.http.post(this.resourceUrl + 'mission-by-year', data).pipe(
      retry(3),
    );
  }
  getMissionBySpecialite(data: any) {
    return this.http.post(this.resourceUrl + 'mission-by-specialite', data).pipe(
      retry(3),
    );
  }

  getMissionBySpecialiteForYear(data: any) {
    return this.http.post(this.resourceUrl + 'mission-by-specialite-sur-annee', data).pipe(
      retry(3),
    );
  }

  getAllMissionByMois(data) {
    return this.http.post(this.resourceUrl + 'all-mission-by-mois', data).pipe(
      retry(3),
    );
  }

  getNbrPlanningByStatut(data) {
    return this.http.post(this.resourceUrl + 'nbr-planning-by-statut', data).pipe(
      retry(3),
    );
  }

  getAllContries() {
    return this.http.get(SERVER_API_AUTH + 'get-all-contries').pipe(
      retry(3)
    );
  }

  onRevientEnHaut() {
    window.scrollTo(0, 0);
  }

  yearForGraph() {
    return [
      { annee: new Date().getFullYear() },
      { annee: new Date().getFullYear() - 1 },
      { annee: new Date().getFullYear() - 2 },
      { annee: new Date().getFullYear() - 3 },
      { annee: new Date().getFullYear() - 4 },
      { annee: new Date().getFullYear() - 5 },
      { annee: new Date().getFullYear() - 6 },
      { annee: new Date().getFullYear() - 7 },
      { annee: new Date().getFullYear() - 8 },
      { annee: new Date().getFullYear() - 9 },

    ];
  }

  getChAviterForInterim(data) {
    return this.http.post(this.resourceUrl + 'ch-a-eviter-interim', data).pipe(
      retry(3),
    );
  }

  newChAviterForInterim(data) {
    return this.http.post(this.resourceUrl + 'new-ch-a-eviter', data).pipe(
      retry(3),
    );
  }

  retirerChAviterForInterim(id) {
    return this.http.post(this.resourceUrl + 'retirer-ch-a-eviter-interim/' + id, null).pipe(
      retry(3),
    );
  }
  siNullRenderZero(item) {
    if (!item)
      item = 0;
    return item;
  }


  satutContratFr(statut: string) {
    let statut_contrat;
    if (statut === 'ABANDONED') {
      statut_contrat = "Abandonné"
    }
    else if (statut === 'CLOSED') {
      statut_contrat = "Terminé"
    }
    else if (statut === 'ARCHIVED') {
      statut_contrat = "Archivé"
    }
    else if (statut === 'OPEN') {
      statut_contrat = "Envoyé"
    }
    else if (statut === 'SIGNED') {
      statut_contrat = "Signé"
    }

    return statut_contrat

  }

  getMois() {
    return [

      "janvier",
      "février",
      "mars",
      "avril",
      "mai",
      "juin",
      "juillet",
      "août",
      "septembre",
      "octobre",
      "novembre",
      "décembre"
    ];
  }
  redirectGIListe() {
    this.router.navigate(['/RMS-Admin/redirection-admin/', 'GIListe']);
  }
  redirectSpecialitesListe() {
    this.router.navigate(['/RMS-Admin/redirection-admin/', 'specialiteListe']);
  }
  redirectUserListe() {
    this.router.navigate(['/RMS-Admin/redirection-admin/', 'usersListe']);
  }
  redirectRecruteursListe() {
    this.router.navigate(['/RMS-Admin/redirection-admin/', 'recruteursListe']);
  }
  redirectInterimListe() {
    this.router.navigate(['/RMS-Admin/redirection-admin/', 'interimListe']);
  }
  redirectEspace(items) {
    this.router.navigate(['/RMS-Admin/redirection-admin/' + items.espace + '/' + items.id]);
  }
  redirectAdmin() {
    this.router.navigate(['/' + 'RMS-Admin']);
    // window.location.href = this.hostServeurAdmin;
    // (this.hostServeurAdmin)
  }
  convertNumnerMoisToString() {
    let month = []
    month["01"] = "Janvier";
    month["02"] = "Février";
    month["03"] = "Mars";
    month["04"] = "Avril";
    month["05"] = "Mai";
    month["06"] = "Juin";
    month["07"] = "Juillet";
    month["08"] = "Août";
    month["09"] = "septembre";
    month["10"] = "Octobre";
    month["11"] = "Novembre";
    month["12"] = "Décembre";

    return month;
  }


  demandeDevis(data: any) {
    return this.http.post(this.resourceUrl + 'demande-devis', data).pipe(
      retry(3),
    );
  }

  messageNotifMail() {

    return "Envoyer un e-mail de  notification"
  }

  getAllTables() {
    return this.http.get(this.resourceUrl + 'list-table').pipe(
      retry(3),
    );
  }


}



