import {Injectable} from '@angular/core';
import {NgbCalendar, NgbDate, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})

export class NgDatePikerService {
  hoveredDate: NgbDate;

  dateDebut: NgbDate;
  dateFin: NgbDate;

  constructor(
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter
  ) {
    this.dateDebut = calendar.getToday();
    this.dateFin = calendar.getNext(calendar.getToday(), 'd', 10);
  }

  onDateSelection(date: NgbDate) {
    if (!this.dateDebut && !this.dateFin) {
      this.dateDebut = date;
    } else if (this.dateDebut && !this.dateFin && date.after(this.dateDebut)) {
      this.dateFin = date;
    } else {
      this.dateFin = null;
      this.dateDebut = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.dateDebut && !this.dateFin && this.hoveredDate && date.after(this.dateDebut) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.dateDebut) && date.before(this.dateFin);
  }

  isRange(date: NgbDate) {
    return date.equals(this.dateDebut) || date.equals(this.dateFin) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
