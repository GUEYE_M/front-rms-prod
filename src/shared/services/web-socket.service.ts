import { Injectable } from '@angular/core';
import WS from '../../assets/js/gos/gos_web_socket_client.js';

@Injectable({
	providedIn: 'root'
})
export class WebSocketService {
	constructor() {}

	public run() {
		var webSocket = WS.connect('ws://127.0.0.1:8080');

		webSocket.on('socket/connect', function(session) {
			//session is an AutobahnJS WAMP session.
		});

		webSocket.on('socket/disconnect', function(error) {
			//error provides us with some insight into the disconnection: error.reason and error.code
		});
	}
}
