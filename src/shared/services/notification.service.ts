import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SERVER_API_URL, vapidKeys } from "../utils/server_api_url";
import { Ng2IzitoastService } from "ng2-izitoast";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { SwPush } from "@angular/service-worker";
import { DialogConfirmationComponent } from "../modules/erp/erp-container/dialog-confirmation/dialog-confirmation.component";
import { MatDialog } from "@angular/material";

@Injectable({
  providedIn: "root"
})
export class NotificationService {
  readonly publicKey = vapidKeys.publicKey;
  @BlockUI() blockUI: NgBlockUI;
  dialogRef: any;
  private resourceUrl = SERVER_API_URL;

  constructor(
    public iziToast: Ng2IzitoastService,
    private http: HttpClient,
    public dialog: MatDialog,
    private swPush: SwPush
  ) {
    iziToast.settings({
      timeout: 3000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: "flipInX",
      transitionOut: "flipOutX",
      position: "topCenter" // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
    });
  }

  showNotificationSuccess(message: string): void {
    this.iziToast.success({
      title: "Succès !",
      message: message
    });
  }

  showNotificationSuccessCopie(position, message: string): void {
    this.iziToast.settings({
      timeout: 5000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: "flipInX",
      transitionOut: "flipOutX",
      position: position // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center, topCenter
    });
    this.iziToast.success({
      title: "Succès !",
      message: message
    });
  }

  showNotificationSuccessUneMinute(position, message: string): void {
    this.iziToast.settings({
      timeout: 100000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: "flipInX",
      transitionOut: "flipOutX",
      position: position // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center, topCenter
    });
    this.iziToast.success({
      title: "Succès !",
      message: message
    });
  }

  showNotificationEchec(message: string): void {
    this.iziToast.error({
      title: "Erreur",
      message: message
    });
  }

  showNotificationEchecCopie(position, message: string): void {
    this.iziToast.settings({
      timeout: 4000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: "flipInX",
      transitionOut: "flipOutX",
      position: position // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center, topCenter
    });
    this.iziToast.error({
      title: "Erreur",
      message: message
    });
  }

  confirmDialogue(msg) {
    this.iziToast.question({
      theme: "green",
      timeout: 200000,
      title: "",
      overlay: true,
      message: msg,
      position: "center", // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
      progressBarColor: "rgb(0, 255, 184)",
      buttons: [
        [
          "<button>Oui</button>",
          function(instance, toast) {
            instance.hide(
              {
                transitionOut: "fadeOutUp",
                onClosing: function(instance, toast, closedBy) {}
              },
              toast,
              "Oui"
            );
          },
          true
        ], // true to focus
        [
          "<button>Non</button>",
          function(instance, toast) {
            instance.hide(
              {
                transitionOut: "fadeOutUp",
                onClosing: function(instance, toast, closedBy) {}
              },
              toast,
              "Non"
            );
          }
        ]
      ]
    });
  }

  public blockUiStart() {
    this.blockUI.start();
  }

  public blockUiStop() {
    this.blockUI.stop();
  }

  sup() {}

  offerNotifications() {
    this.swPush
      .requestSubscription({
        serverPublicKey: this.publicKey
      })
      .then((sub: PushSubscription) => {
        this.http.post(this.resourceUrl + "notifications", sub).subscribe(
          () => {
            ("sub ok");
          },
          err => err
        );
      })
      .catch(err => console.error("Could not subscribe to notifications", err));
  }

  onConfirm(message = null) {
    this.dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: "550px",
      height: "200px",
      panelClass: "myapp-no-padding-dialog",
      data: message
    });
  }
}
