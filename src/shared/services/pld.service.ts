import { Injectable } from '@angular/core';
import {retry} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ContratPldModel} from '../models/interim/ContratPld.model';
import {PLD_API_URL} from '../utils/pld_api_url';
import {SERVER_API_URL} from '../utils/server_api_url';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PldService {
  private resourceServeur = SERVER_API_URL;
  prmesPldSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  prmesPldByClientSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  caracteristiquesPostePldSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient) {}
  findInterimaire(id) {
    return this.http.get<any>(this.resourceServeur + 'pld/interimaire/' + id).pipe();
  }
  findClient(id) {
    return this.http.get<any>(this.resourceServeur + 'pld/client/' + id).pipe();
  }

  findRH(id) {
    return this.http.get<any>(this.resourceServeur + 'pld/rh/' + id).pipe();
  }
  genereRH(id) {
    const httpOptions = {
      responseType: 'blob' as 'json'
    };
    return this.http.get<any>(this.resourceServeur + 'pld/generer-rh/' + id, httpOptions).pipe(retry(3));
  }
  idClientPldToErp(offset) {
    return this.http.get<any>(this.resourceServeur + 'pld/idClientPld-to-erp/' + offset).pipe();
  }
  idInterimPldToErp(offset) {
    return this.http.get<any>(this.resourceServeur + 'pld/idInterimPld-to-erp/' + offset).pipe();
  }
  creerRH(rh) {
    return this.http.post<any>(this.resourceServeur + 'pld/rh', rh, {observe: 'response'}).pipe(retry(3));
  }

  findContrat(id) {
    return this.http.get<any>(this.resourceServeur + 'pld/contrat/' + id).pipe(retry(3));
  }

  creerContrat(contratPld) {
    return this.http.post<any>(this.resourceServeur + 'pld/createContrat', contratPld, {observe: 'response'}).pipe();
  }
  listPrime(idClient) {
    return this.http.get<any>(this.resourceServeur + 'pld/primes/' + idClient).pipe(retry(3));
  }
  listPrimeAll() {
    return this.http.get<any>(this.resourceServeur + 'pld/primes').pipe(retry(3));
  }
  listCaracteristiquePoste() {
    return this.http.get<any>(this.resourceServeur + 'pld/caracteristiquePoste').pipe(retry(3));
  }
  addInterimaire(idInterim) {
    return this.http.post<any>(this.resourceServeur + 'pld/interim/' + idInterim, {observe: 'response'}).pipe(retry(3));
  }
  addClient(idClient) {
    return this.http.post<any>(this.resourceServeur + 'pld/Client/' + idClient, {observe: 'response'}).pipe(retry(3));
  }
  listCommerciaux() {
    return this.http.get<any>(this.resourceServeur + 'pld/commerciaux').pipe(retry(3));
  }

}
