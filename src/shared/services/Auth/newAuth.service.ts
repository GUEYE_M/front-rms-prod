import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, of, Subscription, timer } from "rxjs/index";
import { HttpClient } from "@angular/common/http";
import { retry, switchMap, tap } from "rxjs/operators";
import { SERVER_API_AUTH } from "../../utils/server_api_url";
import { JwtService } from "../jwt..service";
import { InterimService } from "../interim/interim.service";
import { RolesService } from "../roles.service";
import { NotificationService } from "../notification.service";
import { Recruteur } from "../../models/recruteur/Recruteur.model";
import { UtilsService } from "../utils.service";
@Injectable({
  providedIn: "root",
})
export class NewAuthService {
  public resourceUrl = SERVER_API_AUTH;
  public subscription: Subscription;
  candidatureTmp = false;
  statusDossier: boolean;
  labelTab: any;
  constructor(
    private http: HttpClient,
    private router: Router,
    private jwtService: JwtService,
    private interimService: InterimService,
    private utilsService: UtilsService,
    private roleService: RolesService,
    private notificationService: NotificationService
  ) {
    this.subscription = this.initTimer();
  }
  register(recruteur: Recruteur) {
    return this.http.post<any>(
      this.resourceUrl + "register/",
      JSON.stringify(recruteur)
    );
  }
  login(credentials: { username: string; password: string }) {
    return this.http
      .post(this.resourceUrl + "api/login_check", credentials)
      .pipe(
        tap((response) => {
          const resp = Object(response);
          if (resp.token) {
            localStorage.setItem("rms-token", resp.token);
            localStorage.setItem("rms-refreshToken", resp.refresh_token);
          }
        })
      );
  }
  getUser() {
    return this.http.get(this.resourceUrl + "rms-api/current").pipe(
      tap(
        (response) => {
          if (response) {
            const user = Object(response).user;
            this.jwtService.jwtToken.next({
              isAuthenticated: true,
              roles: JSON.parse(user).roles,
              user: JSON.parse(user),
            });
            localStorage.setItem("rms-user", user);
          }
        },
        (error) => {
          this.logout();
        }
      )
    );
  }
  getUserRedirect() {
    return this.http.get(this.resourceUrl + "rms-api/current").pipe(
      tap(
        (response) => {
          const user = Object(response).user;
          this.jwtService.jwtToken.next({
            isAuthenticated: true,
            roles: JSON.parse(user).roles,
            user: JSON.parse(user),
          });
          localStorage.setItem("rms-user", user);
          if (this.roleService.isCandidatUnique(JSON.parse(user).roles)) {
            if (this.candidatureTmp && this.statusDossier) {
              this.utilsService.redirectAdmin();
            } else if (this.candidatureTmp && !this.statusDossier) {
              this.labelTab = 1;
              this.interimService.labelTab.next(this.labelTab);
              this.router.navigate([
                "RMS-Admin/redirection-admin/",
                "Mes-Rubriques",
              ]);
            } else {
              this.utilsService.redirectAdmin();
            }
          } else {
            this.utilsService.redirectAdmin();
          }
        },
        (error) => {
          this.logout();
        }
      )
    );
  }
  signinByRS(item: any): Observable<string> {
    this.notificationService.blockUiStart();
    return this.http
      .post<string>(this.resourceUrl + "auth/signin-by-rs", item)
      .pipe(
        tap(
          (response) => {
            if (response["data"]) {
              const data = response["data"];
              localStorage.setItem("rms-token", data["token"]);
              localStorage.setItem("rms-refreshToken", data["refresh_token"]);
              this.statusDossier = data["etatDossier"];
              this.getUserRedirect().subscribe(
                (resp) => {
                  this.notificationService.blockUiStop();
                },
                (error1) => {
                  this.notificationService.blockUiStop();
                }
              );
            } else this.notificationService.blockUiStop();
          },
          (error) => {
            this.notificationService.blockUiStop();
          }
        )
      );
  }
  userExiste(data): Observable<string> {
    return this.http
      .post<string>(this.resourceUrl + "auth/user", data)
      .pipe(retry(3));
  }
  refreshToken(credentials: { refresh_token: string }) {
    return this.http
      .post<any>(this.resourceUrl + "rms-api/refresh-token", credentials)
      .pipe(
        retry(3),
        tap(
          (response: any) => {
            if (response && response.token) {
              localStorage.setItem("rms-token", response.token);
              // localStorage.setItem(
              //   "rms-refreshToken",
              //   response["refresh_token"]
              // );
              this.getUser().subscribe();
            }
          },
          (error) => {
            this.logout();
          }
        )
      );
  }
  deleteLocalStorage() {
    localStorage.removeItem("rms-user");
    localStorage.removeItem("rms-token");
    localStorage.removeItem("rms-refreshToken");
  }
  initTimer() {
    return timer(150000, 150000)
      .pipe(
        switchMap(() => {
          if (localStorage.getItem("rms-refreshToken")) {
            let refreshToken = {
              refresh_token: localStorage.getItem("rms-refreshToken"),
            };
            this.refreshToken(refreshToken).subscribe();
          }
          return of(null);
        })
      )
      .subscribe(
        () => {},
        (erreur) => {
          this.logout();
        }
      );
  }
  public forgetPassword(email: string) {
    return this.http
      .post(this.resourceUrl + "auth/forget-password", email)
      .pipe(retry(3));
  }
  public verifieToken(token: string) {
    return this.http
      .post(this.resourceUrl + "auth/verifie-token", token)
      .pipe(retry(3));
  }
  public activationCompte(token: string) {
    return this.http
      .post(this.resourceUrl + "auth/activation-compte", token)
      .pipe(retry(3));
  }
  public confirmationToken(token: any) {
    this.http
      .post<string>(this.resourceUrl + "auth/confirmation-token", token)
      .subscribe((rep) => {
        if (rep === "ok") {
          this.router.navigate(["membre/nouveau/mot-de-passe-oublie"]);
        }
      });
  }
  public updatePassword(data: any) {
    let token = localStorage.getItem("token");
    let tab: any;
    if (token) tab = { token: token, pwd: data };
    else tab = data;
    return this.http.post(this.resourceUrl + "auth/update-password", tab).pipe(
      tap((rep: any) => {
        if (rep === "echec") {
          this.notificationService.showNotificationEchec(
            "Echec de la mise à du mot de passe, veuillez ressayer à nouveau !"
          );
          this.router.navigate(["RMS-Admin/redirection-admin/", "Mon-Profil"]);
        } else {
          this.notificationService.showNotificationSuccess(
            "Vous avez changé avec succès votre mot de passe!"
          );
        }
      })
    );
  }
  public updatePasswordProfil(data: any) {
    let token = localStorage.getItem("token");
    let tab: any;
    if (token) tab = { token: token, pwd: data };
    else tab = data;
    return this.http
      .post(this.resourceUrl + "rms-api/update-password", tab)
      .pipe(
        tap((rep: any) => {
          if (!rep["erreur"]) {
            this.notificationService.showNotificationSuccess(rep["success"]);
            this.router.navigate([
              "RMS-Admin/redirection-admin/",
              "Mon-Profil",
            ]);
          } else {
            this.notificationService.showNotificationEchec(rep["erreur"]);
          }
        })
      );
  }
  logout() {
    this.deleteLocalStorage();
    this.jwtService.jwtToken.next({
      isAuthenticated: false,
      roles: null,
      user: null,
    });
    this.subscription.unsubscribe();
    window.location.href = "/";
    // this.router.navigate(['/']);
  }
}
