import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewAuthService } from 'src/shared/services/Auth/newAuth.service';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
  selector: 'app-verifie-token-url',
  templateUrl: './verifie-token-url.component.html',
  styleUrls: ['./verifie-token-url.component.css']
})
export class VerifieTokenUrlComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private authService: NewAuthService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  ngOnInit() {


    this.authService.verifieToken(this.route.snapshot.paramMap.get("params")).subscribe(
      (response:any)=>{
        if(response.erreur)
        {
          this.notificationService.showNotificationEchec(response.erreur)
          this.router.navigate(['/']);
        }

        else{
          this.router.navigate(['membre/nouveau/mot-de-passe-oublie']);
        }
      }
    );

  }

}
