import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifieTokenUrlComponent } from './verifie-token-url.component';

describe('VerifieTokenUrlComponent', () => {
  let component: VerifieTokenUrlComponent;
  let fixture: ComponentFixture<VerifieTokenUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifieTokenUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifieTokenUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
