import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../models/User.model';
import {NotificationService} from '../../../services/notification.service';
import {NewAuthService} from '../../../services/Auth/newAuth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-modal-inscription',
  templateUrl: './modal-inscription.component.html',
  styleUrls: ['./modal-inscription.component.css']
})
export class ModalInscriptionComponent implements OnInit {
  signupForm: FormGroup;
  matchingPwd: boolean;
  candidatureTmp = [];
  user: User;
  error_register: string;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private newAuthService: NewAuthService,
    private router: Router,
    public dialogRef: MatDialogRef<ModalInscriptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (localStorage.getItem('candidatureTmp')) {
      this.candidatureTmp = JSON.parse(localStorage.getItem('candidatureTmp'));
    }
    this.signupForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]],
      roles: ['', Validators.required],
      civilite: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      candidatureTmp: [''],
      telephone: ['', Validators.required],
      enabled: [false],
      photo: [''],
    });
  }

  MatchingPassword(pwd) {
    (this.signupForm.value);
    if (pwd.name === 'password') {
      if (this.signupForm.value['password'] !== this.signupForm.value['confirm_password']) {
        this.matchingPwd = true;
      } else {
        this.matchingPwd = false;
      }
    }
    if (pwd.name === 'confirmPassword') {
      if (this.signupForm.value['confirm_password'] !== this.signupForm.value['password']) {
        this.matchingPwd = true;
      } else {
        this.matchingPwd = false;
      }
    }
  }

  public submit(): void {
    this.notificationService.blockUiStart();
    let utilisateur: any;
    this.signupForm.patchValue({
        candidatureTmp: this.candidatureTmp
      }
    );
    const signupvalue = this.signupForm.value;
    this.user = new User(
      signupvalue['username'],
      signupvalue['email'],
      signupvalue['password'],
      signupvalue['roles'],
      signupvalue['civilite'],
      signupvalue['nom'],
      signupvalue['prenom'],
      signupvalue['telephone'],
      signupvalue['username'],
      '',
      '',
      '',
      signupvalue['enabled'],
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      0,
      ''
    );
    if (this.user.roles.includes('ROLE_RECRUTEUR')) {
      this.user.roles = ['ROLE_ADMIN', 'ROLE_RECRUTEUR'];
      utilisateur = this.user;
    } else if (this.user.roles.includes('ROLE_CANDIDAT')) {
      this.user.roles = ['ROLE_ADMIN', 'ROLE_CANDIDAT'];
      utilisateur = this.user;
    }

    this.newAuthService.register(utilisateur).subscribe((response) => {
      if (response && !response['erreur']) {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationSuccess('Félicitations ! Votre nouveau compte a été créé avec succès !');
        this.error_register = null;
        this.router.navigate(['espace-membre/check-email'], {queryParams: {email: this.user.email}});
      } else {
        this.notificationService.showNotificationEchecCopie('bottomRight', 'L\'email ou le nom d\'utilisateur est déjà associé à un autre compte ! ');
        this.notificationService.blockUiStop();
      }
    }, err => {
      this.error_register = err.error;
      this.notificationService.blockUiStop();
    });
  }

}
