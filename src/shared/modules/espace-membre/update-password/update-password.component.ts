import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import $ from 'jquery';
import {AuthNormalService} from '../../../services/authNormal.service';
import {UserService} from '../../../services/user.service';
import {NewAuthService} from '../../../services/Auth/newAuth.service';
import {NotificationService} from '../../../services/notification.service';
import {MatPaginator} from '@angular/material';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {
  @ViewChild('reference_pass', {static: false}) refPass: ElementRef;
  @ViewChild('reference_pass_2', {static: false}) refPass2: ElementRef;
  parametre: string;
  updatepassword_table: any;
  sub: any;
  matchingPwd: boolean;
  confirmation_new_password: any;
  passwordupdateForm: FormGroup;

  constructor(private formbuilder: FormBuilder,
              private authNormalService: AuthNormalService,
              private userService: UserService,
              private newAuthService: NewAuthService,
              private router: Router,
              private notificationService: NotificationService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.initForm();
    this.sub = this.route.params.subscribe(params => {
      this.parametre = params['token'];
    });
  }

  initForm() {
    this.passwordupdateForm = this.formbuilder.group(
      {
        new_password: ['', [Validators.required, Validators.minLength(6)]],
        confirmation_new_password: ['', [Validators.required, Validators.minLength(6)]],
      }
    );
  }

  MatchingPassword(pwd) {
    if (pwd.name === 'nouveauPwd') {
      if (pwd.value !== this.passwordupdateForm.value['confirmation_new_password']) {
        this.matchingPwd = true;
      } else {
        this.matchingPwd = false;
      }
    }
    if (pwd.name === 'renouveauPwd') {
      if (pwd.value !== this.passwordupdateForm.value['new_password']) {
        this.matchingPwd = true;
      } else {
        this.matchingPwd = false;
      }
    }
  }

  afficherPwd(referencePass) {
    if (referencePass.nativeElement.type === 'password') {
      referencePass.nativeElement.type = 'text';
    } else {
      referencePass.nativeElement.type = 'password';
    }
  }

  onSubmitForm() {
    this.notificationService.blockUiStart();
    const formValue = this.passwordupdateForm.value;
    const new_password = formValue['new_password'];
    this.newAuthService.updatePassword(new_password).subscribe(() => {
      this.router.navigate(['/']);
      this.notificationService.blockUiStop();
    });
  }
}
