import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {NewAuthService} from '../../../services/Auth/newAuth.service';
import {ActivatedRoute} from '@angular/router';
import {UtilsService} from '../../../services/utils.service';

@Component({
  selector: 'app-check-mail',
  templateUrl: './check-mail.component.html',
  styleUrls: ['./check-mail.component.css']
})
export class CheckMailComponent implements OnInit {
  public token: string;
  email: any;
  constructor(
    private userService: UserService,
    private newAuthService: NewAuthService,
    private activatedRoute: ActivatedRoute,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {
    this.utilsService.onRevientEnHaut();

    if (this.activatedRoute.snapshot.queryParams.email)
      this.email  = this.activatedRoute.snapshot.queryParams.email;

    this.token = localStorage.getItem('token');
  }
  OnUpdatePassword() {
    this.newAuthService.confirmationToken(this.token);
  }

}
