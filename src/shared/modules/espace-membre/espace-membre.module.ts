import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthComponent} from './auth/auth.component';
import {ActivationCompteComponent} from './activation-compte/activation-compte.component';
import {RegistrationSuccessComponent} from './registration-success/registration-success.component';
import {CheckMailComponent} from './check-mail/check-mail.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {ParametreCompteComponent} from './parametre-compte/parametre-compte.component';
import {UpdatePasswordComponent} from './update-password/update-password.component';
import {VerifieTokenUrlComponent} from './verifie-token-url/verifie-token-url.component';
import {EspaceMembreRouting} from './espaceMembre.routing';
import {SharedModule} from '../shared/shared.module';
import { ModalInscriptionComponent } from './modal-inscription/modal-inscription.component';



@NgModule({
  declarations: [
    AuthComponent,
    ActivationCompteComponent,
    RegistrationSuccessComponent,
    CheckMailComponent,
    ForgetPasswordComponent,
    ParametreCompteComponent,
    UpdatePasswordComponent,
    VerifieTokenUrlComponent,
    ModalInscriptionComponent,
  ],
  imports: [
    CommonModule,
    EspaceMembreRouting,
    SharedModule,
  ],
  entryComponents: [
    ModalInscriptionComponent
  ]
})
export class EspaceMembreModule { }
