import {Component, ContentChild, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import $ from 'jquery';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../models/User.model';
import {UserService} from '../../../services/user.service';


@Component({
  selector: 'app-parametre-compte',
  templateUrl: './parametre-compte.component.html',
  styleUrls: ['./parametre-compte.component.css']
})
export class ParametreCompteComponent implements OnInit {
  userItems = [];
  error_username: boolean;
  erreur: string;
  matchingPwd: boolean;
  UpdateUserItem: object;
  updatePwdForm: FormGroup;
  currentUser: BehaviorSubject<User> = new BehaviorSubject( null);
  constructor(
    private userService: UserService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.userService.currentUser.subscribe((response) => {
      if (response) {
        this.currentUser.next(response);
        const tableItems = Object.keys(this.currentUser.value);
        for (let i = 0; i < tableItems.length; i++) {
          const item = tableItems[i];
          if (item !== 'idUser' && item !== 'idInterim' && item !== 'roles' && item !== 'photo')
            this.userItems.push(item);
        }
      }
    });
    this.initUpdatePwdForm();
  }

  initUpdatePwdForm() {
    this.updatePwdForm = this.fb.group({
      ancienPassword: ['', [Validators.required, Validators.minLength(6)]],
      nouveauPassword: ['', [Validators.required, Validators.minLength(6)]],
      renouveauPassword: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  MatchingPassword(pwd) {
    if(pwd.name === 'nouveauPwd') {
      if (pwd.value !== this.updatePwdForm.value['renouveauPassword'])
        this.matchingPwd = true;
      else
        this.matchingPwd = false;
    }
    if(pwd.name === 'renouveauPwd') {
      if (pwd.value !== this.updatePwdForm.value['nouveauPassword'])
        this.matchingPwd = true;
      else
        this.matchingPwd = false;
    }
  }
  OnModifie(element) {
    let valeur = $('#' + element)[0]['outerText'];
    let newcontentselect = ' <form id="' + element + '" class="form-inline">\n' +
      '      <select class="form-control col-10" id="input' + element + '">\n' +
      '        <option value=" ">SÉLECTIONNER</option>\n' +
      '        <option value="Monsieur">Monsieur</option>\n' +
      '        <option value="Madame">Madame</option>\n' +
      '      </select>' +
      '    <a class="input-group-addon col-2" style="background-color:#1dc5a3;color: white" id="valider' + element + '"><i class="glyphicon glyphicon-ok"></i></a>' +
      '  </form>';
    let newContent = ' <p id="' + element + '" class="input-group">\n' +
      '      <input id="input' + element + '" type="text" class="form-control" name="msg" placeholder="' + valeur + '">\n' +
      '      <a class="input-group-addon" style="background-color:#1dc5a3;color: white" id="valider' + element + '"><i class="glyphicon glyphicon-ok"></i></a>\n' +
      '    </p>';
    if(element === 'civilite')
      $('#' + element).replaceWith(newcontentselect);
    else
      $('#' + element).replaceWith(newContent);
    $('#valider' + element).on('click', () => {
      this.Onvalide(element);
    });
  }

  Onvalide(element) {
    let new_saisi = $('#input' + element).val();
      if(!new_saisi)
        new_saisi = $('#input' + element)[0]['placeholder'];
    let newContent = '<p id="' + element + '">\n' + new_saisi + ' &nbsp;&nbsp;&nbsp;\n' +
    '            <span>\n' +
    '                <i class="fa fa-pencil" id="modifie' + element + '"></i>\n' +
    '            </span>\n' +
    '          </p>';
    this.UpdateUserItem = {'key' : element , 'value' : new_saisi};
    this.userService.parametreCompte(this.UpdateUserItem).subscribe(
      (resp) => {
        this.error_username = true;
        $('#' + element).replaceWith(newContent);
        $('#modifie' + element).on('click', () => {
          this.OnModifie(element);
        });
      },
      err => {
        this.error_username = false;
      });
  }

  afficherPwd (pwd, glyphicon) {
    const eyeOpen = '<i class="glyphicon glyphicon-eye-open"  id="' + glyphicon.id + '" #' + glyphicon.id + '> </i>';
    const eyeClose = '<i class="glyphicon glyphicon-eye-close" id="' + glyphicon.id + '" #' + glyphicon.id + '></i>';
    if (pwd.type === 'text') {
      pwd.type = 'password';
      $('#' + glyphicon.id).replaceWith(eyeOpen);
    }
    else {
      pwd.type = 'text';
      $('#' + glyphicon.id).replaceWith(eyeClose);
    }
  }
  UpdatePasswordCompte() {
    const id = this.userService.currentUser.value['idUser'];
    const formValue = this.updatePwdForm.value;
    const password = formValue['ancienPassword'];
    const new_password = formValue['nouveauPassword'];
    const update_password = {'password': password, 'new_password' : new_password };
    const tableau = {'id': id, 'update_password' : update_password };
    this.userService.updatePasswordCompte( tableau ).subscribe((resp) => {
      if(resp === 'erreur')
        this.erreur = 'l\'ancien mot de passe saisi est incorrect!';
      if (resp === 'ok') {
        $('#update_passwordModal')[0].modal('hide');
        this.erreur = null;
      }
    });
  }
}
