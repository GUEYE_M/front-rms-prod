import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-registration-success',
  templateUrl: './registration-success.component.html',
  styleUrls: ['./registration-success.component.css']
})
export class RegistrationSuccessComponent implements OnInit {

  public email: string;
  public nom: string;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.email = this.route.snapshot.queryParams['email'];
    this.nom = this.route.snapshot.queryParams['nom'];
  }

}
