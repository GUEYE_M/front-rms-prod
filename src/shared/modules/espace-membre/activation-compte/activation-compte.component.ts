import { Component, OnInit } from '@angular/core';
import { NewAuthService } from 'src/shared/services/Auth/newAuth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
  selector: 'app-activation-compte',
  templateUrl: './activation-compte.component.html',
  styleUrls: ['./activation-compte.component.css']
})
export class ActivationCompteComponent implements OnInit {
 message = '';
 erreur = false;
 constructor(
   private router: Router,
   private activateRoute: ActivatedRoute,
   private newAuthService: NewAuthService,
   private notificationService: NotificationService
 ) { }
  ngOnInit() {
    this.newAuthService.activationCompte(this.activateRoute.snapshot.paramMap.get('token')).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.notificationService.showNotificationEchec(response.erreur);
          this.router.navigate(['/']);
        } else {
          this.router.navigate(['/espace-membre']);
          this.notificationService.showNotificationSuccess(response.success);
        }
      }
    );
  }

}
