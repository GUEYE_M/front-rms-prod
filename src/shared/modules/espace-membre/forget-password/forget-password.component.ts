import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {NewAuthService} from '../../../services/Auth/newAuth.service';
import {NotificationService} from '../../../services/notification.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  public email: string;
  public status: string;
  private sub: any;
  erreur = false;
  erreurTxt: string;
  passwordForm: FormGroup;
  constructor(private formbuilder: FormBuilder,
              private userService: UserService ,
              private newAuthService: NewAuthService,
              private notificationService: NotificationService,
              private router: Router,
              private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initForm();
    this.sub = this.route.params.subscribe(params => {
      this.status = params['params'];
    });
  }

  initForm() {
    this.passwordForm = this.formbuilder.group(
      {
        email: ['', [Validators.required, Validators.email]]
      }
    );
  }
  onSubmitForm() {
    const formValue = this.passwordForm.value;
    this.notificationService.blockUiStart();
    this.newAuthService.forgetPassword(formValue.email).subscribe(
      (response:any) => {
         if(response.erreur)
         {
          this.erreur = true;
          this.erreurTxt = response['erreur'];
         } else {
            this.erreur = false;
            localStorage.setItem('token', response.data);
           this.router.navigate(['espace-membre/check-email'],{ queryParams: {email: formValue.email}});
          }
        this.notificationService.blockUiStop();
      });
  }

}
