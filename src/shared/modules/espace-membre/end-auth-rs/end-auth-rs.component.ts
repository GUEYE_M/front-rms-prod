import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';
import {AuthService} from 'angular5-social-auth';
import {User} from '../../../models/User.model';
import {AuthNormalService} from '../../../services/authNormal.service';
import {NewAuthService} from '../../../services/Auth/newAuth.service';
import {UtilsService} from '../../../services/utils.service';
import {NotificationService} from '../../../services/notification.service';

@Component({
  selector: 'app-end-auth-rs',
  templateUrl: './end-auth-rs.component.html',
  styleUrls: ['./end-auth-rs.component.css']
})
export class EndAuthRsComponent implements OnInit {


  public hideElement = false;
  public email: string;
  loader = false;
  element_value = 'ROLE_CANDIDAT';
  showElement = false;
  signupForm: FormGroup;
  user: User;
  error_login: string;
  error_register: string;
  error_rs: string;
  username_existe = false;
  bd_echec = 'Impossible d\'etablir une connexion a la base de données';
  user_existe = 'L\' username ou le l\' adresse email est déja associé à un compte!';
  signinForm: FormGroup;
  signinRsForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private authNormalService: AuthNormalService,
    private newAuthService: NewAuthService,
    private router: Router,
    private utilsService: UtilsService,
    private dialog: MatDialog,
    private socialAuthService: AuthService,
    private notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.utilsService.onRevientEnHaut();

    this.signinRsForm = this.fb.group({
      username: [''],
      role: ['', Validators.required],
      civilite: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      telephone: ['', Validators.required],
    });
  }

  public submit(): void {
    this.notificationService.blockUiStart();
    let utilisateur: any;
    const signupvalue = this.signupForm.value;
    this.user = new User(
      signupvalue['username'],
      signupvalue['email'],
      signupvalue['password'],
      signupvalue['roles'],
      signupvalue['civilite'],
      signupvalue['nom'],
      signupvalue['prenom'],
      signupvalue['telephone'],
      signupvalue['username'],
      '',
      '',
      '',
      signupvalue['enabled'],
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      0,
      ''
    );
    if ( this.user.roles.includes( 'ROLE_RECRUTEUR')) {
      this.user.roles = ['ROLE_ADMIN', 'ROLE_RECRUTEUR'];
      utilisateur = this.user;
    }
    else if ( this.user.roles.includes('ROLE_CANDIDAT')) {
      this.user.roles = ['ROLE_ADMIN', 'ROLE_CANDIDAT'];
      utilisateur = this.user;
    }

    this.newAuthService.register(utilisateur).subscribe((response) => {
      if (response && !response['erreur']) {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationSuccessCopie('bottomRight',
          'Félicitations ! Votre nouveau compte a été créé avec succès !');
        this.error_register = null;
        this.router.navigate(['Espace-Membre/inscription'],{ queryParams: {nom: this.user.nom, email: this.user.email}});
      }
      else {
        this.notificationService.showNotificationEchecCopie('bottomRight',
          'L\'email ou le nom d\'utilisateur est déjà associé à un autre compte ! ' );
        this.notificationService.blockUiStop();
      }
    }, err => {
      this.error_register = err.error;
      this.notificationService.blockUiStop();
    });
  }

  public terminer_inscription_rs() {
    if (this.signinRsForm.value['username'] !== '')
      this.data.name = this.signinRsForm.value['username'];
    if ( this.signinRsForm.value['role'] === 'ROLE_RECRUTEUR') {
      this.data.roles = ['ROLE_ADMIN', 'ROLE_RECRUTEUR'];
    } else if ( this.signinRsForm.value['role'] === 'ROLE_MEDICAL') {
      this.data.roles = ['ROLE_ADMIN', 'ROLE_CANDIDAT', 'ROLE_MEDICAL'];
    } else if ( this.signinRsForm.value['role'] === 'ROLE_PARAMEDICAL') {
      this.data.roles = ['ROLE_ADMIN', 'ROLE_CANDIDAT', 'ROLE_PARAMEDICAL'];
    }

    this.data.civilite = this.signinRsForm.value['civilite'];
    this.data.nom = this.signinRsForm.value['nom'];
    this.data.prenom = this.signinRsForm.value['prenom'];
    this.data.telephone = this.signinRsForm.value['telephone'];
    this.newAuthService.signinByRS(this.data).subscribe();
  }

}
