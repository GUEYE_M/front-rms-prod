import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider,
} from "angular5-social-auth";
import { EndAuthRsComponent } from "../end-auth-rs/end-auth-rs.component";
import { User } from "../../../models/User.model";
import { AuthNormalService } from "../../../services/authNormal.service";
import { NewAuthService } from "../../../services/Auth/newAuth.service";
import { UtilsService } from "../../../services/utils.service";
import { NotificationService } from "../../../services/notification.service";
import { ModalInscriptionComponent } from "../modal-inscription/modal-inscription.component";
@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.css"],
})
export class AuthComponent implements OnInit {
  div_connexion = true;
  div_inscription = false;
  hideElement = false;
  email: string;
  loader = false;
  element_value = "ROLE_CANDIDAT";
  showElement = false;
  signupForm: FormGroup;
  user: User;
  candidatureTmp = [];
  error_login: string;
  error_register: string;
  error_rs: string;
  username_existe = false;
  bd_echec = "Impossible d'etablir une connexion a la base de données";
  user_existe =
    "L' username ou le l' adresse email est déja associé à un compte!";
  signinForm: FormGroup;
  signinRsForm: FormGroup;
  dialogRef: MatDialogRef<EndAuthRsComponent>;
  dataRS = {
    email: "",
    name: "",
    provider: "",
    idToken: "",
    id: "",
    image: "",
    roles: [],
    candidatureTmp: [],
    telephone: "",
    civilite: "",
    nom: "",
    prenom: "",
  };
  matchingPwd: boolean;
  constructor(
    private fb: FormBuilder,
    private authNormalService: AuthNormalService,
    private newAuthService: NewAuthService,
    private router: Router,
    private utilsService: UtilsService,
    private dialog: MatDialog,
    private socialAuthService: AuthService,
    private notificationService: NotificationService
  ) {}
  ngOnInit() {
    this.utilsService.onRevientEnHaut();
    if (localStorage.getItem("candidatureTmp")) {
      this.candidatureTmp = JSON.parse(localStorage.getItem("candidatureTmp"));
    }
    this.utilsService.onRevientEnHaut();
    this.signupForm = this.fb.group({
      username: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required]],
      confirm_password: ["", [Validators.required]],
      roles: ["", Validators.required],
      civilite: ["", Validators.required],
      nom: ["", Validators.required],
      prenom: ["", Validators.required],
      candidatureTmp: [""],
      telephone: ["", Validators.required],
      enabled: [false],
      photo: [""],
    });
    this.signinRsForm = this.fb.group({
      username: [""],
      role: ["", Validators.required],
      civilite: ["", Validators.required],
      nom: ["", Validators.required],
      prenom: ["", Validators.required],
      candidatureTmp: [this.candidatureTmp, Validators.required],
      telephone: ["", Validators.required],
    });
    this.signinForm = this.fb.group({
      username: [""],
      password: [""],
      candidatureTmp: [this.candidatureTmp, Validators.required],
    });
  }
  public submit(): void {
    this.notificationService.blockUiStart();
    let utilisateur: any;
    this.signupForm.patchValue({
      candidatureTmp: this.candidatureTmp,
    });
    const signupvalue = this.signupForm.value;
    this.user = new User(
      signupvalue["username"],
      signupvalue["email"],
      signupvalue["password"],
      signupvalue["roles"],
      signupvalue["civilite"],
      signupvalue["nom"],
      signupvalue["prenom"],
      signupvalue["telephone"],
      signupvalue["username"],
      "",
      "",
      "",
      signupvalue["enabled"],
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      0,
      ""
    );
    if (this.user.roles.includes("ROLE_RECRUTEUR")) {
      this.user.roles = ["ROLE_ADMIN", "ROLE_RECRUTEUR"];
      utilisateur = this.user;
    } else if (this.user.roles.includes("ROLE_CANDIDAT")) {
      this.user.roles = ["ROLE_ADMIN", "ROLE_CANDIDAT"];
      utilisateur = this.user;
    }
    this.newAuthService.register(utilisateur).subscribe(
      (response) => {
        if (response && !response["erreur"]) {
          this.notificationService.blockUiStop();
          this.notificationService.showNotificationSuccess(
            "Félicitations ! Votre nouveau compte a été créé avec succès !"
          );
          this.error_register = null;
          this.router.navigate(["espace-membre/check-email"], {
            queryParams: { email: this.user.email },
          });
        } else {
          this.notificationService.showNotificationEchecCopie(
            "bottomRight",
            "L'email ou le nom d'utilisateur est déjà associé à un autre compte ! "
          );
          this.notificationService.blockUiStop();
        }
      },
      (err) => {
        this.error_register = err.error;
        this.notificationService.blockUiStop();
      }
    );
  }
  MatchingPassword(pwd) {
    this.signupForm.value;
    if (pwd.name === "password") {
      if (
        this.signupForm.value["password"] !==
        this.signupForm.value["confirm_password"]
      ) {
        this.matchingPwd = true;
      } else {
        this.matchingPwd = false;
      }
    }
    if (pwd.name === "confirmPassword") {
      if (
        this.signupForm.value["confirm_password"] !==
        this.signupForm.value["password"]
      ) {
        this.matchingPwd = true;
      } else {
        this.matchingPwd = false;
      }
    }
  }
  public connexion(): void {
    this.notificationService.blockUiStart();
    this.newAuthService.login(this.signinForm.value).subscribe(
      (response) => {
        this.newAuthService.getUser().subscribe(
          (resp) => {
            this.utilsService.redirectAdmin();
            this.notificationService.blockUiStop();
          },
          (error) => {
            this.newAuthService.deleteLocalStorage();
            this.notificationService.blockUiStop();
            this.notificationService.showNotificationEchecCopie(
              "bottomRight",
              "impossible d'etablir une connexion, veuillez réssayer !"
            );
          }
        );
      },
      (err) => {
        this.notificationService.showNotificationEchecCopie(
          "bottomRight",
          "<body>" +
          "<ul>" +
          "<li>Le login ou le mot de passe est incorrect !</li>" +
          "<li>Veuillez nous contacter si le problème persiste !</li>" +
          "</ul>" +
          "</body>"
        );
        this.notificationService.blockUiStop();
      }
    );
  }
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then((userData) => {
      this.notificationService.blockUiStart();
      this.dataRS.email = userData.email;
      this.dataRS.candidatureTmp = this.candidatureTmp;
      this.dataRS.name = userData.name;
      this.dataRS.image = userData.image;
      this.dataRS.provider = userData.provider;
      this.dataRS.id = userData.id;
      this.dataRS.idToken = userData.idToken;
      this.newAuthService.userExiste(this.dataRS).subscribe(
        (response) => {
          localStorage.removeItem("candidatureTmp");
          if (response !== "erreur") {
            this.newAuthService.signinByRS(this.dataRS).subscribe();
          } else {
            this.OnUpdateInterimaire();
          }
          this.notificationService.blockUiStop();
        },
        (error) => {
          this.notificationService.blockUiStop();
        }
      );
    });
  }
  OnUpdateInterimaire() {
    this.dialogRef = this.dialog.open(EndAuthRsComponent, {
      width: "650px",
      height: "550px",
      panelClass: "myapp-no-padding-dialog",
      data: this.dataRS,
    });
  }
  showInscriptionModal() {
    const dialogRef = this.dialog.open(ModalInscriptionComponent, {
      width: "800px",
      height: "700px",
      data: {},
    });
  }
  showInscription() {
    this.div_inscription = true;
    this.div_connexion = false;
  }
}
