import {Route, RouterModule} from '@angular/router';
import {AuthComponent} from './auth/auth.component';
import {ActivationCompteComponent} from './activation-compte/activation-compte.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {VerifieTokenUrlComponent} from './verifie-token-url/verifie-token-url.component';
import {CheckMailComponent} from './check-mail/check-mail.component';
import {UpdatePasswordComponent} from './update-password/update-password.component';

const ESPACE_MEMBRE_ROUTES: Route[] = [
  {path: '', component: AuthComponent},
  {path: 'activation-compte/:token', component: ActivationCompteComponent},
  {path: 'mot-de-passe-oublie', component: ForgetPasswordComponent},
  {path: 'mot-de-passe-oublie/:params', component: ForgetPasswordComponent},
  {path: 'verifie-token/:params', component: VerifieTokenUrlComponent},
  {path: 'check-email', component: CheckMailComponent},
  {path: 'nouveau/mot-de-passe-oublie', component: UpdatePasswordComponent},

];

export const EspaceMembreRouting = RouterModule.forChild(ESPACE_MEMBRE_ROUTES);
