import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
  MatChipInputEvent, MatDialog,
  MatDialogRef,
  MatTableDataSource
} from '@angular/material';
import {NotificationService} from '../../../services/notification.service';
import {PldService} from '../../../services/pld.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {CandidatureService} from '../../../services/candidature.service';
import {SelectionModel} from '@angular/cdk/collections';
import {DatePipe} from '@angular/common';
import {InterimService} from '../../../services/interim/interim.service';
import {UtilsService} from '../../../services/utils.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-update-contrat-pld',
  templateUrl: './update-contrat-pld.component.html',
  styleUrls: ['./update-contrat-pld.component.css']
})
export class UpdateContratPldComponent implements OnInit {
  form: FormGroup;
  idClientPld: any;
  caracteristiques = [];
  idCLient: any;
  isUpdateTauxHoraire = false;
  dateDebut: any;
  TabPrimes = [];
  dateFin: any;
  contratPld: any;
  // matchipList ******************
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  primeCtrl = new FormControl();
  filteredPrimes: Observable<string[]>;
  primesDuContrat: string[];
  allPrimes: any;
  idpld: any;
  idMM: any;
  idIterim: any;
  allPrimesPld = [];
// ***************************************//

  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  displayedColumns = [
    'select',
    'number',
    'date',
    'vacation',
    'debut',
    'fin',
    'salaireNet',
    'salaireBrute',
    'tarif',
  ];

  selection = new SelectionModel<any[]>(true, []);

  @ViewChild('fruitInput', {static: false}) fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;



  constructor(
    private notificationService: NotificationService,
    private pldService: PldService,
    private interimService: InterimService,
    private utilsService: UtilsService,
    private dialog: MatDialog,
    public datepipe: DatePipe,
    private candidatureService: CandidatureService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }
  ngOnInit() {
    this.idIterim = this.data.idInterim;
    this.idpld = this.data.idpld;
    this.idMM = this.data.idMM;
    this.idCLient = this.data.idclient;
    this.notificationService.blockUiStart();
    this.getContratPld(this.idpld);
    this.candidatureByIdMission(this.idMM, this.idIterim);

  }

  initForm() {
    this.caracteristiquesPoste();
    this.form = this.fb.group(
      {
        recours: [this.contratPld.TxtRecours,  Validators.required],
        justificatif: ['Recherche candidat pour le poste'],
        salaireReference: [this.contratPld.SalaireReference],
        heureDebut: [this.contratPld.HoraireAMDebut],
        heureFin: [this.contratPld.HoraireAMFin],
        nbJoursEssai: [this.contratPld.NbJoursEssai],
        horaires: [ this.contratPld.Horaires ],
        caracteristiquePoste: [ '' ],
        moyenAcces: [this.contratPld.MoyenAcces]
      });
  }
  caracteristiquesPoste(){
    this.pldService.caracteristiquesPostePldSubject.subscribe((caracteristiques: any) => {
      this.caracteristiques = caracteristiques;
    });
  }
  getContratPld(idpld) {
    this.pldService.findContrat(idpld).subscribe((response) => {
      this.notificationService.blockUiStop();
      response = JSON.parse(response);
      this.contratPld = response;
      this.idClientPld = response.IdClient;
      this.primePld();
      this.tabPrimeOldPld(response.TabPrimes);
      this.initForm();
    },
      error => {
      this.notificationService.blockUiStop();
      });
  }
  primePld(){
    const tabPrimes = [];
    this.pldService.prmesPldByClientSubject.subscribe(
      (response: any) => {
        Object.values(response).forEach((element: any) => {
          tabPrimes.push(element.Designation);
          this.allPrimesPld.push(element);
        });
        this.initPrimes(tabPrimes);
      },
    );
  }
  tabPrimeOldPld(primes) {
    const tabPrimes = [];
    const thiss = this;
    primes.forEach(function(element) {
      tabPrimes.push( element['TxtPrime']);
    });
    this.primesDuContrat = tabPrimes;
  }
  suppIdentifantPrime(item) {
    let response = 'existe pas';
    if (item.includes('0000000'))
      response = item.replace('0000000', '');
    if (item.includes(this.idClientPld))
      response = item.replace(this.idClientPld, '');
    return response;
  }
  candidatureByIdMission(idMM, idInterim) {
    this.notificationService.blockUiStart();
    this.candidatureService.getCandidaturesByIdMM(idMM, idInterim).subscribe((response) => {
      this.notificationService.blockUiStop();
      if (response['data']) {
        this.dataSource = new MatTableDataSource(response['data']);
      }
    },
      error => {
        this.notificationService.blockUiStop();
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  // MatchipList*****************************
  initPrimes(items: string[]) {
    this.filteredPrimes = this.primeCtrl.valueChanges.pipe(
      startWith(null),
      map((prime: string | null) => prime ? this._filter(prime) : items.slice()));
  }
  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        this.primesDuContrat.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }
      this.primeCtrl.setValue(null);
    }
  }

  remove(prim: string): void {
    const index = this.primesDuContrat.indexOf(prim);
    if (index >= 0) {
      this.primesDuContrat.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.primesDuContrat.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.primeCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allPrimes ? this.allPrimes.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0) : null;
  }

  // ****************************************************
  updateTauxHoraire() {
    this.isUpdateTauxHoraire = !this.isUpdateTauxHoraire;
  }

  onSave() {
    this.notificationService.blockUiStart();
    this.TabPrimes = [];
    if (this.selection.selected.length > 0) {
      const thiss = this;
      this.contratPld.TxtJustificatif = this.form.value.justificatif;
      this.contratPld.HoraireAMDebut = this.form.value.heureDebut;
      this.contratPld.HoraireAMFin = this.form.value.heureFin;
      this.contratPld.NbJoursEssai = this.form.value.nbJoursEssai;
      this.contratPld.MoyenAcces = this.form.value.moyenAcces;
      this.contratPld.Horaires = this.form.value.horaires;
      this.contratPld.caracteristiquePoste = 	this.form.value.caracteristiquePoste;

      this.primesDuContrat.forEach(item => {
        this.selection.selected.forEach( function(el, index) {
          const jtc = el['jtc'];
          const candidatureDate = thiss.datepipe.transform(el['date'].date, 'yyyyMMdd');
          if (index === 0) {
            // on initialise la date de debut et la date de fin de la candidature
            thiss.dateDebut = candidatureDate;
            thiss.dateFin = candidatureDate;

          } else {
            // a ce niveau on flitre pour recuperer le dernier jour (candidature) de la mission
            if (thiss.dateFin < candidatureDate)
              thiss.dateFin = candidatureDate;
            // a ce niveau on flitre pour recuperer le premier jour (candidature) de la mission
            if (thiss.dateDebut > candidatureDate)
              thiss.dateDebut = candidatureDate;
          }
          const prime = el['abreviation-vacation'] + ' ' + el['specialite'];
          const modelPrime = {
            IdPrime: '',
            TxtPrime: '',
            LibPrimeComplement: '',
            JTC: '',
            TauxReference: 0,
            TauxPayeImposable: 0,
            TauxPayeNonImposable: 0,
            TauxFacture: 0
          };
          if (thiss.suppIdentifantPrime(item).trim() === prime) {
            let itemPrime: any;
            thiss.allPrimesPld.forEach( element => {
              if (item.trim() === element['Designation'].trim()) {
                itemPrime = element;
              }
            });
            modelPrime.TauxReference = el['txtreference'];
            modelPrime.TauxPayeImposable = el['salaireBrute'];
            modelPrime.TauxFacture = el['tarif'];
            modelPrime.IdPrime = itemPrime['IdPrime'];
            modelPrime.TxtPrime = item;
            modelPrime.JTC = jtc;
            modelPrime.TauxPayeNonImposable = 0;
            thiss.TabPrimes.push(modelPrime);
          }
          if (thiss.suppIdentifantPrime(item).trim() === 'Heures') {
            let itemPrime: any;
            thiss.allPrimesPld.forEach( element => {
              if (item.trim() === element['Designation'].trim()) {
                itemPrime = element;
              }
            });
            modelPrime.IdPrime = itemPrime['IdPrime'];
            modelPrime.TxtPrime = item;
            modelPrime.JTC = jtc;
            thiss.TabPrimes.push(modelPrime);
          }
        });
      });
      this.contratPld.DateDebut = this.dateDebut;
      this.contratPld.DateFin = this.dateFin;
      thiss.TabPrimes = this.removeDuplicates(thiss.TabPrimes);
      this.contratPld.TabPrimes = thiss.TabPrimes;

      const tabIdCandidatures = [];
      this.selection.selected.forEach((element) => {
        tabIdCandidatures.push(element['id']);
      });
      const tabIdCandidaturesForUpdate = {candidatureIds: Array.from(new Set(tabIdCandidatures)), contrat : this.contratPld , primes : Array.from(new Set(this.contratPld.TabPrimes))};
      this.notificationService.blockUiStop();
      // this.savePld(tabIdCandidaturesForUpdate);

    } else {
      this.notificationService.blockUiStop();
      this.notificationService.showNotificationEchecCopie('bottomRight', 'Vous devez sélectionner au moins une candidature !');
    }

  }

  savePld(contratData) {
    this.notificationService.blockUiStart();
    this.pldService.creerContrat(contratData).subscribe((resp) => {
        this.notificationService.showNotificationSuccessCopie('bottomLeft', 'Mise à jour du contrat avec succès du contrat ' + this.contratPld.NumeroContrat + ' !');
        this.dialog.closeAll();
      },
      error => {
        this.notificationService.showNotificationEchecCopie('bottomLeft', 'Echec de la mise à jour du contrat !');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
        this.utilsService.redirectEspace({espace: 'missionMedecinListe', id: null});
      });
  }

  removeDuplicates(items) {
    const unique = [];
    const idPrimes = [];
    items.forEach(function(item, index) {
      if (index === 0) {
        unique.push(item);
        idPrimes.push(item['IdPrime']);
      } else {
        if (!idPrimes.includes(item['IdPrime'])){
          unique.push(item);
          idPrimes.push(item['IdPrime']);
        }
      }
    });
    return unique;
  }

}
