import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateContratPldComponent } from './update-contrat-pld.component';

describe('UpdateContratPldComponent', () => {
  let component: UpdateContratPldComponent;
  let fixture: ComponentFixture<UpdateContratPldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateContratPldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateContratPldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
