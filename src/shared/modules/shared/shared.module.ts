import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe, getLocaleDateTimeFormat, registerLocaleData } from '@angular/common';
import { DateClickedDirective, MultipleDatePickerComponent } from 'multiple-date-picker-angular/dist';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChecklistModule } from 'angular-checklist';
import { RouterModule } from '@angular/router';
import { SocialLoginModule } from 'angular5-social-auth';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { Ng2IziToastModule } from 'ng2-izitoast';
import { MaterialModule } from '../material/material.module';
import { ChartsModule } from 'ng2-charts';
import { NgxDaterangepickerModule } from '@qqnc/ngx-daterangepicker';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';
import { CKEditorModule } from 'ckeditor4-angular';
import { BsDatepickerModule } from 'ngx-bootstrap';

import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { ModalTypeGardeComponent } from '../erp/erp-container/modules/client-wrapper/client/modal-client/modal-type-garde/modal-type-garde.component';
import { FicheSpecialiteClientEditComponent } from '../erp/erp-container/modules/client-wrapper/client/fiche-specialites/fiche-specialite-client-edit/fiche-specialite-client-edit.component';
import { DialogConfirmationComponent } from '../erp/erp-container/dialog-confirmation/dialog-confirmation.component';


import { ViewMessageProdpectionModalComponent } from '../erp/erp-container/modules/prospection/view-message-prodpection-modal/view-message-prodpection-modal.component';
import { FromEmailComponent } from '../erp/erp-container/modules/client-wrapper/client/modal-client/from-email/from-email.component';
import { EditProspectionComponent } from '../erp/erp-container/modules/client-wrapper/client/single-client-container/prospection-interim/edit-prospection/edit-prospection.component';
import { ListProspectionModalComponent } from '../erp/erp-container/modules/prospection/list-prospection-modal/list-prospection-modal.component';
import { InputAutocompletInterimComponent } from '../erp/erp-container/modules/client-wrapper/client/modal-client/input-autocomplet-interim/input-autocomplet-interim.component';

import { ClientWrapperComponent } from '../erp/erp-container/modules/client-wrapper/client-wrapper/client-wrapper.component';
import { ListeClientComponent } from '../erp/erp-container/modules/client-wrapper/client/liste-client/liste-client.component';

import { TransactionModalComponent } from '../erp/erp-container/modules/sms/transaction/transaction-modal/transaction-modal.component';
import { PldContratComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/pld-contrat/pld-contrat.component';
import { ListHebergementModalComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/list-hebergement-modal/list-hebergement-modal.component';
import { ListRecruteurModalComponent } from '../erp/erp-container/modules/client-wrapper/client/modal-client/list-recruteur-modal/list-recruteur-modal.component';
import { AdminConfigHebergementEditComponent } from '../erp/erp-container/modules/admin-config/admin-config-hebergement-edit/admin-config-hebergement-edit.component';
import { StatutContratSignComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/statut-contrat-sign/statut-contrat-sign.component';
import { ModalPostulerInterimComponent } from '../erp/erp-container/modules/interim/modal-interim/modal-postuler-interim/modal-postuler-interim.component';
import { UpdateContratPldComponent } from './update-contrat-pld/update-contrat-pld.component';
import { EditListeClientComponent } from '../erp/erp-container/modules/client-wrapper/client/edit-liste-client/edit-liste-client.component';
import { ListClientRecruteurComponent } from '../erp/erp-container/modules/client-wrapper/client/list-client-recruteur/list-client-recruteur.component';
import { MatPaginatorIntl } from '@angular/material';
import { getDutchPaginatorIntl } from 'src/shared/utils/custom-mat.paginator';
import { ProfilGestionnaireInterneComponent } from '../erp/erp-container/modules/gestionnaire-interne/profil-gestionnaire-interne/profil-gestionnaire-interne.component';
import { TacheGestionnaireInterneComponent } from '../erp/erp-container/modules/gestionnaire-interne/tache-gestionnaire-interne/tache-gestionnaire-interne.component';
import { EndAuthRsComponent } from '../espace-membre/end-auth-rs/end-auth-rs.component';
import { NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { SingleInterimComponent } from '../erp/erp-container/modules/interim/single-interim/single-interim.component';
import { BienvenueMyRMSComponentComponent } from '../erp/erp-container/modules/interim/bienvenue-my-rmscomponent/bienvenue-my-rmscomponent.component';
import { SpecialiteInterimComponent } from '../erp/erp-container/modules/interim/single-interim/specialite-interim/specialite-interim.component';
import { DisponibiliteComponent } from '../erp/erp-container/modules/interim/single-interim/disponibilite/disponibilite.component';
import { DossierComponent } from '../erp/erp-container/modules/interim/single-interim/dossier/dossier.component';
import { EditInterimComponent } from '../erp/erp-container/modules/interim/edit-interim/edit-interim.component';
import { MissionMedecinComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/mission-medecin.component';
import { ModalInterimComponent } from '../erp/erp-container/modules/interim/modal-interim/modal-interim.component';
import { RedirectionMedComponent } from '../erp/erp-container/modules/interim/redirection-med/redirection-med.component';
import { OffresMedecinComponent } from '../erp/erp-container/modules/interim/single-interim/offres-medecin/offres-medecin.component';
import { ProfilComponent } from '../erp/erp-container/modules/interim/single-interim/profil/profil.component';
import { StatistiqueeComponent } from '../erp/erp-container/modules/interim/single-interim/statistiquee/statistiquee.component';
import { ProspectionInterimComponent } from '../erp/erp-container/modules/interim/single-interim/prospection-interim/prospection-interim.component';
import { ReleveHeureModalComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/releve-heure-modal/releve-heure-modal.component';
import { AcompteModalComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/acompte-modal/acompte-modal.component';
import { ModalCandidaturesComponent } from '../erp/erp-container/modules/interim/single-interim/mission-medecin/modal-candidatures/modal-candidatures.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SpecialiteInterimModalComponent } from '../erp/erp-container/modules/interim/single-interim/specialite-interim/specialite-interim-modal/specialite-interim-modal.component';
import { ModalDisponibiliteGestionComponent } from '../erp/erp-container/modules/interim/modal-interim/modal-disponibilite-gestion/modal-disponibilite-gestion.component';
import { ModalListeMissionForCandidaturesValidationComponent } from '../erp/erp-container/modules/client-wrapper/client/modal-client/modal-liste-mission-for-candidatures-validation/modal-liste-mission-for-candidatures-validation.component';
import { ListeRecruteurInterimComponent } from '../erp/erp-container/modules/client-wrapper/client/modal-client/liste-recruteur-interim/liste-recruteur-interim.component';
import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as candyTheme from 'fusioncharts/themes/fusioncharts.theme.candy';
import * as umberTheme from 'fusioncharts/themes/fusioncharts.theme.umber';
import * as gammelTheme from 'fusioncharts/themes/fusioncharts.theme.gammel';
import {AddIdPldManuelComponent} from '../erp/erp-container/modules/missions-interimaires/add-id-pld-manuel/add-id-pld-manuel.component';
import {ModalPldContratComponent} from '../erp/erp-container/modules/missions-interimaires/missions-interimaires/modal-pld-contrat/modal-pld-contrat.component';
FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme, candyTheme, umberTheme, gammelTheme);
@NgModule({
  declarations: [
    MultipleDatePickerComponent,
    DateClickedDirective,
    ModalTypeGardeComponent,
    FicheSpecialiteClientEditComponent,
    DialogConfirmationComponent,
    ViewMessageProdpectionModalComponent,
    FromEmailComponent,
    ListProspectionModalComponent,
    EditProspectionComponent,
    InputAutocompletInterimComponent,
    ListRecruteurModalComponent,
    TransactionModalComponent,
    ClientWrapperComponent,
    ListeClientComponent,
    PldContratComponent,
    ListHebergementModalComponent,
    AdminConfigHebergementEditComponent,
    StatutContratSignComponent,
    ModalPostulerInterimComponent,
    UpdateContratPldComponent,
    EditListeClientComponent,
    ListClientRecruteurComponent,
    ProfilGestionnaireInterneComponent,
    TacheGestionnaireInterneComponent,
    EndAuthRsComponent,
    SingleInterimComponent,
    SpecialiteInterimComponent,
    BienvenueMyRMSComponentComponent,
    EditInterimComponent,
    MissionMedecinComponent,
    ModalInterimComponent,
    RedirectionMedComponent,
    OffresMedecinComponent,
    ProfilComponent,
    DisponibiliteComponent,
    DossierComponent,
    StatistiqueeComponent,
    ProspectionInterimComponent,
    ReleveHeureModalComponent,
    AcompteModalComponent,
    ModalCandidaturesComponent,
    SpecialiteInterimModalComponent,
    ModalDisponibiliteGestionComponent,
    ModalInterimComponent,
    ModalListeMissionForCandidaturesValidationComponent,
    ListeRecruteurInterimComponent,
    AddIdPldManuelComponent,
    ModalPldContratComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChecklistModule,
    RouterModule,
    Ng2IziToastModule,
    MaterialModule,
    NgxDaterangepickerModule,
    NgxDaterangepickerMd.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxMatDrpModule,
    CKEditorModule,
    SelectAutocompleteModule,
    NgbModule,
    InfiniteScrollModule,
    FusionChartsModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    ChecklistModule,
    RouterModule,
    SocialLoginModule,
    JwSocialButtonsModule,
    MultipleDatePickerComponent,
    DateClickedDirective,
    ChartsModule,
    CKEditorModule,
    NgxDaterangepickerMd,
    BsDatepickerModule,
    NgxMatDrpModule,
    SelectAutocompleteModule,
    ListRecruteurModalComponent,
    ClientWrapperComponent,
    ListeClientComponent,
    ProfilGestionnaireInterneComponent,
    TacheGestionnaireInterneComponent,
    EndAuthRsComponent,
    InfiniteScrollModule,
    StatistiqueeComponent,
    FusionChartsModule,
    MaterialModule,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useValue: getDutchPaginatorIntl()
    },
    DatePipe
  ],
  entryComponents: [
    ModalTypeGardeComponent,
    FicheSpecialiteClientEditComponent,
    ViewMessageProdpectionModalComponent,
    DialogConfirmationComponent,
    FromEmailComponent,
    InputAutocompletInterimComponent,
    ListProspectionModalComponent,
    EditProspectionComponent,
    ListRecruteurModalComponent,
    TransactionModalComponent,
    ListHebergementModalComponent,
    AdminConfigHebergementEditComponent,
    StatutContratSignComponent,
    ModalPostulerInterimComponent,
    PldContratComponent,
    UpdateContratPldComponent,
    EditListeClientComponent,
    EndAuthRsComponent,
    ReleveHeureModalComponent,
    AcompteModalComponent,
    ModalCandidaturesComponent,
    SpecialiteInterimModalComponent,
    ModalDisponibiliteGestionComponent,
    ModalInterimComponent,
    ModalListeMissionForCandidaturesValidationComponent,
    ListeRecruteurInterimComponent,
    EditInterimComponent,
    AddIdPldManuelComponent,
    ModalPldContratComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class SharedModule {
}
