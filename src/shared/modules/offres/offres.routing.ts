import {Route, RouterModule} from '@angular/router';
import {ListeOffreSiteComponent} from './liste-offre-site/liste-offre-site.component';
import {SingleOffreSiteComponent} from './single-offre-site/single-offre-site.component';
import {ModalPostulerComponent} from './single-offre-site/modal-postuler/modal-postuler.component';
import {OffreSiteResolver} from '../../resolver/offreSite.resolver';
const OFFRES_ROUTES: Route[] = [
  {path: '',   component: ListeOffreSiteComponent},
  {path: ':id', component: SingleOffreSiteComponent},
  {path: 'modal-postuler', component: ModalPostulerComponent},
];

export const OffresRouting = RouterModule.forChild(OFFRES_ROUTES);
