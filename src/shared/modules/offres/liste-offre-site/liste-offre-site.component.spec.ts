import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeOffreSiteComponent } from './liste-offre-site.component';

describe('ListeOffreSiteComponent', () => {
  let component: ListeOffreSiteComponent;
  let fixture: ComponentFixture<ListeOffreSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeOffreSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeOffreSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
