import {Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/shared/services/notification.service';

import {OffresService} from '../../../services/recruteur/offres.service';


declare const chance;

@Component({
  selector: 'app-liste-offre-site',
  templateUrl: './liste-offre-site.component.html',
  styleUrls: ['./liste-offre-site.component.css']
})
export class ListeOffreSiteComponent implements OnInit {
  data: any[];
  filterWp = false
  filterMonth: any[];
  filterDept: any[];
  filterSpecialite: any[];
  checkValue: any = {
    specialite: [],
    mois: [],
    dept: [],
    inputFilter: null,
  };
  inputFilter: String;


  constructor(
    private offresService: OffresService,
    private notificationService: NotificationService,
    private activatedRoute:ActivatedRoute
            ) {
   
  
  }
  //https://reseaumedical.fr/offres?mois=[field id="mois"]&&specialite=[field id="specialite"]&&dept=[field id="dept"]
  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe((params: any) => {
    
      if (params.keys.length > 0) {
        
      
        params.keys.forEach(element => {

          if (element === "mois") {
            if (params.params.mois)
            {
              this.checkValue.mois.push(params.params.mois);
              }
           
          }
          else if (element === "dept") {
            if (params.params.dept) {
              this.checkValue.dept.push(params.params.dept);
            }
           
          }
          else if (
            element === "specialite") {
            if (params.params.specialite) {
              this.checkValue.specialite.push(params.params.specialite);
              }
            
          }
          else if (element === "s") {
            if (params.params.s) {
              this.inputFilter = params.params.s
              this.getInputSearch()
            }
           
          }
        });
        this.filterWp = true

        console.log(this.checkValue)
        this.getData(this.checkValue);
      } else {
        this.getData();
        this.filterWp = false
      }
      
    })
   // this.getData()
  }

  getData(data = null) {
    this.notificationService.blockUiStart();
    this.offresService.getOffreForSite(data).subscribe((rep: any) => {
      this.notificationService.blockUiStop();
      if (rep) {
        
        if (!rep.filter) {
            this.customData(rep.missions)
            this.filterMonth = rep.filterMois;
            this.filterDept = rep.filterDept;
            this.filterSpecialite = rep.filterSpecialite;
        } else {

          this.customData(rep.missions)
          }
      }


    });
   
  }

  customData(data: any) {
    this.data = data.map(
      item => {
        let ip = '';
        const line = item.infoPratique.split('\n');
        for (let i = 0; i < line.length; i++)
        {
          if (line[i] !== "") {
            ip += line[i];
          }
          if (i == 3) {
            break;
          }
          }
      
      
        return {
          reference: item.reference,
          specialite: item.specialite,
          profil_rechercher: item.profil_rechercher,
          infoPratique: ip,
          departement: item.departement,
          dateenreg: item.dateenreg,
          nomspecialite:item.nomspecialite
          
        }
      }
    ) 

  }

  getCheckboxValue(e, id) {
    if (e.target.checked) {
      if (id === 3) {
        this.checkValue.specialite.push(e.target.value);
      } else if (id == 2) {
        this.checkValue.dept.push(e.target.value);
      } else if (id == 1) {
        this.checkValue.mois.push(e.target.value);
      }
    } else {
      if (id === 3) {
        const index = this.checkValue.specialite.findIndex(
          (x) => x === e.target.value
        );
        this.checkValue.specialite.splice(index, 1);
      } else if (id == 2) {
        const index = this.checkValue.dept.findIndex(
          (x) => x === e.target.value
        );
        this.checkValue.dept.splice(index, 1);
      } else if (id == 1) {
        const index = this.checkValue.mois.findIndex(
          (x) => x === e.target.value
        );
        this.checkValue.mois.splice(index, 1);
      }
    }

    this.getData(this.checkValue);
  }

  getInputSearch() {
    if (this.inputFilter) {
      this.checkValue.inputFilter = this.inputFilter;
    
      
      this.getData(this.checkValue);
    }
  }






 

}
