import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OffresRouting} from './offres.routing';
import {ListeOffreSiteComponent} from './liste-offre-site/liste-offre-site.component';
import {SingleOffreSiteComponent} from './single-offre-site/single-offre-site.component';
import {SharedModule} from '../shared/shared.module';
import {ModalPostulerComponent} from './single-offre-site/modal-postuler/modal-postuler.component';
import { PagePostulerComponent } from './single-offre-site/page-postuler/page-postuler.component';



@NgModule({
  declarations: [
    ListeOffreSiteComponent,
    SingleOffreSiteComponent,
    ModalPostulerComponent,
    PagePostulerComponent,
  ],
  imports: [
    CommonModule,
    OffresRouting,
    SharedModule,
  ]
})
export class OffresModule { }
