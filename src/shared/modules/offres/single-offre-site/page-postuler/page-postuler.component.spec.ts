import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePostulerComponent } from './page-postuler.component';

describe('PagePostulerComponent', () => {
  let component: PagePostulerComponent;
  let fixture: ComponentFixture<PagePostulerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagePostulerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePostulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
