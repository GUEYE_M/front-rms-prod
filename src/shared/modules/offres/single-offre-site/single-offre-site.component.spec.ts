import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleOffreSiteComponent } from './single-offre-site.component';

describe('SingleOffreSiteComponent', () => {
  let component: SingleOffreSiteComponent;
  let fixture: ComponentFixture<SingleOffreSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleOffreSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleOffreSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
