import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {
  MAT_DIALOG_DATA, MatPaginator, MatSort, MatTableDataSource
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {CandidatureService} from '../../../../services/candidature.service';
import {RolesService} from '../../../../services/roles.service';
import {NotificationService} from '../../../../services/notification.service';
import {OffresService} from '../../../../services/recruteur/offres.service';
import {UserService} from '../../../../services/user.service';


@Component({
  selector: 'app-modal-postuler',
  templateUrl: './modal-postuler.component.html',
  styleUrls: ['./modal-postuler.component.css']
})
export class ModalPostulerComponent implements OnInit, AfterViewInit {
 
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  displayedColumnsModal = ['select', 'date', 'libelle', 'debut', 'fin', 'salaire'];
  is_connect = true;
  etat_dossier = true;
  plannings = [];
  interimaire: any;
  reference: any;
  offres: any;
  selection = new SelectionModel<any[]>(true, []);
  dataSource: MatTableDataSource<any>;

	length: number = 0;
  pageSizeOptions: number[] = [5, 10, 20];
  pageSize = 5;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private router_activated: ActivatedRoute,
    private candidatureService: CandidatureService,
    public roleService: RolesService,
    private notifierService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private offresService: OffresService,
    private userService: UserService,
  ) {
  }

  ngOnInit() {

    this.dataSource = new MatTableDataSource(this.data.items);
    this.length = this.data.items.length;
 
  

    console.log(this.data)
    // this.dataSourceModal = this.data[0].donnee;
    // this.etat_dossier = this.data[0].etat_dossier;
     this.is_connect = this.data.is_connect;
    this.offres = this.data.items[0];
    this.reference = this.data.items[0].reference;
     this.interimaire = this.userService.getCurrentUser();
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  

  clearSelection() {
    this.selection = new SelectionModel<any[]>(true, []);
  }

  onPostuler() {
    this.notifierService.blockUI.start();
    const id_missionAndInterim = [];
    id_missionAndInterim[0] = this.interimaire ? this.interimaire.interim.id : null;
    id_missionAndInterim[1] = this.offres.id;
  
    let tmp = [];
    this.selection.selected.forEach(function (element: any) {
       
      tmp.push(element.planning_id);
    });
    this.selection = new SelectionModel<any[]>(true, []);
    this.plannings = tmp;
    this.plannings['interim_id'] = this.interimaire ? this.interimaire.interim.id : null;
    this.plannings['id'] = this.offres.mission_id;
    this.candidatureService.create_candidature_medecin(false,null,false,JSON.stringify(this.plannings), JSON.stringify(id_missionAndInterim)).subscribe(
      (next: any) => {
        if( next.erreur ){
          this.notifierService.showNotificationEchecCopie('bottomRight', next.erreur);
          this.notifierService.blockUI.stop();

        } else {
          this.notifierService.showNotificationSuccessCopie('bottomRight', 'Candidature effectuée avec success');
          this.offresService.getJsonOffresByReference(this.reference, this.interimaire.interim.id).subscribe(
            (response: any) => {
              (response);
              if(response.length > 0 ){
                this.offres = response;
                this.offresService.offresSubjectSingle.next(this.offres);
              } else{
                this.offresService.getJsonOffres(this.interimaire.interim.id);
                this.router.navigate(['/offres']);
              }
            });
          this.notifierService.blockUI.stop();
        }
      },
      (error2) => {
        this.notifierService.showNotificationEchecCopie('bottomRight', error2);
      }
    );
    // }

  }

  // pour ceux qui ne sont connecter ils sont directemment rediriger lespace de connexion
  goToEspaceMembre() {
    const dataTmp = {
      candidatures: null,
      id_missionAndInterim: null
    };
    const id_missionAndInterim = [];
    id_missionAndInterim[0] = 0;
    id_missionAndInterim[1] = this.offres.id;
    const tmp = [];
    this.selection.selected.forEach((element: any) => {
      tmp.push(element.planning_id);
    });
    dataTmp.candidatures = JSON.stringify(tmp);
    dataTmp.id_missionAndInterim = JSON.stringify(id_missionAndInterim);
    localStorage.setItem('candidatureTmp', JSON.stringify(dataTmp));
    this.router.navigate(['/espace-membre']);
  }

  ngAfterViewInit() {
  
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
