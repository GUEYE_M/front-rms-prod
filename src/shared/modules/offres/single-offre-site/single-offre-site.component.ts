import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UserService } from '../../../services/user.service';
import { OffresService } from '../../../services/recruteur/offres.service';
import { CandidatureService } from '../../../services/candidature.service';
import { InterimService } from '../../../services/interim/interim.service';
import { RolesService } from '../../../services/roles.service';
import { UtilsService } from '../../../services/utils.service';
import { NotificationService } from '../../../services/notification.service';
import { ModalPostulerComponent } from './modal-postuler/modal-postuler.component';

@Component({
	selector: 'app-single-offre-site',
	templateUrl: './single-offre-site.component.html',
	styleUrls: [ './single-offre-site.component.css' ]
})
export class SingleOffreSiteComponent implements OnInit {
	nbr_vacation: number;
	offre: any;
	missionSimilaire: any[];
	interim: number = 37;
	curentUser: any;
	interimaire: any;
	is_connect = true;
	etat_dossier = true;
	interim_id = 0;
	constructor(
		private userService: UserService,
		private dialog: MatDialog,
		private offresService: OffresService,
		private activatedRoute: ActivatedRoute,
		private roleService: RolesService,
		private notificationService:NotificationService
	) {}

	ngOnInit(): void {
		this.getData(this.activatedRoute.snapshot.params.id);
		this.curentUser = this.userService.getCurrentUser();
		this.interimaire = this.userService.getCurrentUser();
		if (this.interimaire && this.roleService.isCandidatUnique(this.interimaire.roles)) {
			this.interim_id = this.interimaire.interim.id;
			this.is_connect = true;
		} else {
			this.is_connect = false;
		}
	}
	
	getData(reference: any) {
		this.notificationService.blockUiStart();
		this.offresService
			.getOffresByReference({ ref: reference }).subscribe((rep: any) => {
				this.notificationService.blockUiStop();
			if (!rep.erreur) {
			  this.nbr_vacation = rep.offre.length;
			  this.offre = rep.offre;
			  this.missionSimilaire = rep.mission_similaire;
			}
		  });
	  }
	
	  candidatureModal(data: any) {
		const elements = {
		    interim: this.interim,
		    items: data,
			user: this.curentUser,
			is_connect: this.is_connect,
			etat_dossier: this.etat_dossier,
		};
	
		//  console.log(data);
		const dialogRef_mssage: MatDialogRef<ModalPostulerComponent> = this.dialog.open(
		  ModalPostulerComponent,
		  {
			width: "900px",
			height: "500px",
			data: elements,
			panelClass: "myapp-no-padding-dialog",
		  }
		);
	
		dialogRef_mssage.afterClosed().subscribe((el) => {
		  if (el) {
		  }
		});
	  }
}
