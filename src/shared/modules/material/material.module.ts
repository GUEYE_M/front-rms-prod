import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { A11yModule } from '@angular/cdk/a11y';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import {
	MatAutocompleteModule,
	MatBadgeModule,
	MatButtonModule,
	MatPseudoCheckboxModule,
	MatCardModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatFormFieldModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatTableModule,
	MatSortModule,
	MatTabsModule,
	MatTooltipModule,
	MatBottomSheet,
	MatBottomSheetModule,
	MatSidenavModule,
	MatStepperModule,
	MatDividerModule,
	MatButtonToggleModule,
	MatTreeModule,
	MatToolbarModule
} from '@angular/material';
import { ScrollingModule } from '@angular/cdk/scrolling';

const MATERIAL = [
	A11yModule,
	CdkStepperModule,
	CdkTableModule,
	CdkTreeModule,
	DragDropModule,
	MatAutocompleteModule,
	MatBadgeModule,
	MatBottomSheetModule,
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSidenavModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatSortModule,
	MatTableModule,
	MatTabsModule,
	MatTooltipModule,
	MatToolbarModule,
	MatTreeModule,
	ScrollingModule,
	MatButtonToggleModule,
	MatDividerModule,
	MatStepperModule
];
@NgModule({
	declarations: [],
	imports: [ CommonModule, ...MATERIAL ],
	providers: [ MatBottomSheet ],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
	exports: MATERIAL
})
export class MaterialModule {}
