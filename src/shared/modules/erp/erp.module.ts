import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErpRouting } from './erp.routing';
import { SharedModule } from '../shared/shared.module';
import { ErpContainerComponent } from './erp-container/erp-container.component';
import { LeftPanelComponent } from './inc/left-panel/left-panel.component';
import { HeadPanelComponent } from './inc/head-panel/head-panel.component';
import { MonProfilModule } from './erp-container/modules/mon-profil/mon-profil.module';
import { WrapperComponent } from './erp-container/wrapper/wrapper.component';
import { StatInterimaireComponent } from './erp-container/wrapper/stat-interimaire/stat-interimaire.component';
import { StatClientComponent } from './erp-container/wrapper/stat-client/stat-client.component';
import { ConfirmDialogComponent } from './erp-container/modules/confirm-dialog/confirm-dialog.component';
import { RedirectionAdminComponent } from './erp-container/redirection-admin/redirection-admin.component';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';


import { FusionChartsModule } from "angular-fusioncharts";
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { HistoriqueComponent } from './erp-container/modules/historique/historique.component';


// @ts-ignore
// @ts-ignore

FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);
@NgModule({
  declarations: [
    ErpContainerComponent,
    LeftPanelComponent,
    HeadPanelComponent,
    WrapperComponent,
    StatInterimaireComponent,
    StatClientComponent,
    ConfirmDialogComponent,
    RedirectionAdminComponent,
    HistoriqueComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    MonProfilModule,
    ErpRouting,
    NgxMatDrpModule,
    FusionChartsModule

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class ErpModule { }
