import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErpContainerComponent } from './erp-container.component';

describe('ErpContainerComponent', () => {
  let component: ErpContainerComponent;
  let fixture: ComponentFixture<ErpContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErpContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErpContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
