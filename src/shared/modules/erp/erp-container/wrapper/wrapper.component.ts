import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Specialite } from '../../../../models/Specialite.model';
import { UserService } from '../../../../services/user.service';
import { UtilsService } from '../../../../services/utils.service';
import { RolesService } from '../../../../services/roles.service';
import { NotificationService } from '../../../../services/notification.service';
import { PldService } from '../../../../services/pld.service';
import { SpecialiteService } from '../../../../services/specialite.service';
import {
	ApexTitleSubtitle,
	ApexChart,
	ApexAxisChartSeries,
	ApexNonAxisChartSeries,
	ChartComponent
} from 'ng-apexcharts';
import { LOCALE } from '../../../../utils/date_filter_config';
import { DatePipe } from '@angular/common';
import { InterimService } from '../../../../services/interim/interim.service';
import { ActivatedRoute } from '@angular/router';
import { GestionnaireInterneService } from '../../../../services/gestionnaire interne/gestionnaireInterne.service';
import { BehaviorSubject } from 'rxjs';
import { TypeChartModel, ChartModel, DataModel } from 'src/shared/chart.model';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';
moment.locale('fr', localization);
import { CandidatureService } from '../../../../services/candidature.service';
import { OffresService } from '../../../../services/recruteur/offres.service';

@Component({
	selector: 'app-wrapper',
	templateUrl: './wrapper.component.html',
	styleUrls: [ './wrapper.component.css' ]
})
export class WrapperComponent implements OnInit {
	filtrer = false;
	indicateurs: any;

	specialite = new FormControl('', [ Validators.required ]);
	annee = new FormControl('', [ Validators.required ]);
	annees: any[] = [
		{ annee: new Date().getFullYear() },
		{ annee: new Date().getFullYear() - 1 },
		{ annee: new Date().getFullYear() - 2 },
		{ annee: new Date().getFullYear() - 3 },
		{ annee: new Date().getFullYear() - 4 },
		{ annee: new Date().getFullYear() - 5 },
		{ annee: new Date().getFullYear() - 6 },
		{ annee: new Date().getFullYear() - 7 },
		{ annee: new Date().getFullYear() - 8 },
		{ annee: new Date().getFullYear() - 9 }
	];

	range: Range = { fromDate: new Date(), toDate: new Date() };

	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];

	listSpecialites: Specialite[] = [];

	contratPld = [];

	gestionnaireInterne: any;

	@ViewChild('chartObj', { static: false })
	chart: ChartComponent;

	currentUser: any;

	public barChartOptionsInterim = {
		scaleShowVerticalLines: false,
		responsive: true
	};

	anneesInterim: any[];

	anneeInterim = new FormControl('', [ Validators.required ]);
	@ViewChild('dateRangePicker', { static: false })
	dateRangePicker;
	date_debut: number = null;
	date_fin: number = null;

	salaireInterim: any = 0;
	heureTravaillerInterim: any = 0;
	nbrMissionInterim: any = 0;
	nbrSpecilaiteInterim: any = 0;
	interim_id: any;
	pie3dChart: any;
	barChart: any;

	candiddatues = [];

	constructor(
		private datePipe: DatePipe,
		private interimService: InterimService,
		private activateRoute: ActivatedRoute,
		private utiles: UtilsService,
		private candidatureService: CandidatureService,
		public rolesService: RolesService,
		private userService: UserService,
		private gestionnaireInterneService: GestionnaireInterneService,
		private utilService: UtilsService,
		public roleService: RolesService,
		private notificationService: NotificationService,
		private pldService: PldService,
		private notifierService: NotificationService,
		private offresService: OffresService,
		private specialiteService: SpecialiteService
	) {}

	ngOnInit() {
		this.interimService.labelTab_text = new BehaviorSubject('default');
		this.currentUser = this.userService.getCurrentUser();
		if (this.currentUser.interim && this.currentUser.interim.id) {
			this.interim_id = this.currentUser.interim.id;

			const candidatureStorage = JSON.parse(localStorage.getItem('candidatureTmp'));

			
		if (candidatureStorage) {
			this.notificationService.blockUiStart();
			const tmp = JSON.parse(candidatureStorage.id_missionAndInterim);
			this.interimService.labelTab_text = new BehaviorSubject('default');
			tmp[0] = this.interim_id;
			candidatureStorage.id_missionAndInterim = tmp;
			candidatureStorage.candidatures = JSON.parse(candidatureStorage.candidatures);
			this.offresService.getCandidaturesMeds(tmp[0], tmp[1]).subscribe((x) => {
				if (x.length > 0) {
					x = x.map((x) => {
						return +x.id;
					});
					const tmpPlannings = [];
					candidatureStorage.candidatures.forEach((y) => {
						if (x.indexOf(+y) === -1) {
							tmpPlannings.push(y);
						}
					});
					if (tmpPlannings.length > 0) {
						this.setCandidaturesMeds(tmpPlannings, candidatureStorage.id_missionAndInterim);
						this.notificationService.blockUiStop();
					} else {
						this.notificationService.showNotificationEchecCopie(
							'bottomRight',
							'vous avez déjà candidaté sur ces date(s)'
						);
						localStorage.removeItem('candidatureTmp');
						this.notificationService.blockUiStop();
					}
				} else {
					this.setCandidaturesMeds(candidatureStorage.candidatures, candidatureStorage.id_missionAndInterim);
					this.notificationService.blockUiStop();
				}
			});
		}
		}
		
		this.anneeInterim.patchValue({
			annee: new Date().getFullYear()
		});
		this.anneesInterim = this.utiles.yearForGraph();

		if (this.roleService.isManager(this.currentUser.roles)) {
			this.notificationService.blockUiStart();
			const data = {
				specialite: null,
				annee: null
			};
			this.nbrPlanningByStatut(data);
			this.listSpecialite();

			this.getAllMissionByMois(2020);

			this.utilService.indicateurs().subscribe(
				(data: any) => {
					this.notificationService.blockUiStop();

					if (data.erreur) {
						this.notificationService.showNotificationEchec(data.erreur);
					} else {
						this.indicateurs = data;
					}
				},
				(error) => {
					this.notificationService.blockUiStart();
				}
			);
		}

		if (
			this.roleService.isCommercialUnique(this.currentUser.roles) &&
			this.currentUser &&
			this.currentUser.gestionnaire
		) {
			this.getTacheGestionnaireConnecte(this.currentUser.gestionnaire.id);
		}
	}
	setCandidaturesMeds(plannings, idMissionAndInterim) {
		this.notificationService.blockUiStart();
		this.candidatureService
			.create_candidature_medecin(
				false,
				null,
				false,
				JSON.stringify(plannings),
				JSON.stringify(idMissionAndInterim)
			)
			.subscribe(
				(next: any) => {
					if (next.erreur) {
						this.notifierService.showNotificationEchecCopie('bottomRight', next.erreur);
						this.notifierService.blockUI.stop();
					} else {
						this.notifierService.showNotificationSuccessCopie(
							'bottomRight',
							'Candidature effectuée avec success'
						);
						this.notifierService.blockUI.stop();
						localStorage.removeItem('candidatureTmp');
					}
				},
				(error2) => {
					this.notifierService.showNotificationEchecCopie('bottomRight', error2);
				}
			);
		this.notificationService.blockUiStop();
	}
	getNewStatistics(item) {
		this.getAllMissionByMois(item.annee);
	}

	pieChart(donnee: any) {
		let data = new DataModel();
		let chartModel = new ChartModel();

		(chartModel.caption = 'Statut Vacation'), (chartModel.subcaption = '');
		chartModel.showpercentintooltip = '0';
		chartModel.numberprefix = '';
		chartModel.enablemultislicing = '1';
		chartModel.theme = 'fusion';

		data.chart = chartModel;
		data.data = donnee;

		this.pie3dChart = new TypeChartModel('100%', 500, 'pie3d', 'json', data);
	}

	chartBar(donnee) {
		let data = new DataModel();
		let chartModel = new ChartModel();

		data.chart = {
			caption: 'Evolution date de planning ',
			yaxisname: 'nombre de date par statut ',
			subcaption: '(par mois)',
			plottooltext: '<b>$dataValue</b> date(s)  $seriesName pour le mois $label',
			showsum: '0',
			theme: 'fusion'
		};
		(data.categories = [
			{
				category: [
					{
						label: 'JANVIER'
					},
					{
						label: 'FÉVRIER'
					},
					{
						label: 'MARS'
					},
					{
						label: 'AVRIL'
					},
					{
						label: 'MAI'
					},
					{
						label: 'JUIN'
					},
					{
						label: 'JUILLET'
					},
					{
						label: 'AOÛT'
					},
					{
						label: 'SEPTEMBRE'
					},
					{
						label: 'OCTOBRE'
					},
					{
						label: 'NOVEMBRE'
					},
					{
						label: 'DÉCEMBRE'
					}
				]
			}
		]),
			(data.dataset = donnee);

		this.barChart = new TypeChartModel('100%', 500, 'stackedcolumn3d', 'json', data);
	}
	voirContratPld() {
		this.contratPld;
		this.utilService.updateIdContratPLD(this.contratPld).subscribe((resp) => {
			resp;
		});
	}
	actionRecherche() {
		this.notificationService.blockUiStart();

		let data = {
			specialite: null,
			annee: null
		};

		if (this.annee.valid && this.specialite.valid) {
			data.annee = this.annee.value.annee;
			data.specialite = this.specialite.value;

			this.getAllMissionByMois(this.annee.value.annee, this.specialite.value);
		} else {
			if (this.annee.valid) {
				this.notificationService.blockUiStart();
				data.annee = this.annee.value.annee;
				this.getAllMissionByMois(this.annee.value.annee, null);
				this.utilService.getMissionByYear(this.annee.value).subscribe(
					(response: any) => {
						this.notificationService.blockUiStop();
						if (!response.erreur) {
							if (response.data == 0) {
							} else {
							}
						}
					},
					(error) => {
						this.notificationService.blockUiStop();
					}
				);
			}

			if (this.specialite.valid) {
				this.notificationService.blockUiStart();
				data.specialite = this.specialite.value;
				this.getAllMissionByMois(null, this.specialite.value);
				this.utilService.getMissionBySpecialite(this.specialite.value).subscribe(
					(response: any) => {
						this.notificationService.blockUiStop();
						if (!response.erreur) {
							if (response.data == 0) {
							} else {
							}
						}
					},
					(error) => {
						this.notificationService.blockUiStop();
					}
				);
			}
		}
		this.nbrPlanningByStatut(data);
		this.notificationService.blockUiStop();
	}
	listSpecialite() {
		this.specialiteService.list(null, true).subscribe((response: any) => {
			if (!response.erreur) {
				this.listSpecialites = response.data;
			}
		});
	}
	getAllMissionByMois(annee, specialite: any = null) {
		let data = {
			annee: annee,
			specialite: specialite
		};

		this.utilService.getAllMissionByMois(data).subscribe(
			(response: any) => {
				if (!response.erreur) {
					if (response.data == 0) {
					} else {
						this.chartBar(response);
					}
				}
			},
			(erreur) => {},
			() => {}
		);
	}
	nbrPlanningByStatut(data) {
		this.utilService.getNbrPlanningByStatut(data).subscribe((response: any) => {
			if (!response.erreur) {
				if (response.data == 0) {
				} else {
					this.pieChart(response.data);
				}
			}
		});
	}
	onFiltrer() {
		this.filtrer = !this.filtrer;
	}
	getTacheGestionnaireConnecte(id) {
		this.gestionnaireInterneService.find(id).subscribe(
			(response) => {
				this.notificationService.blockUiStop();
				this.gestionnaireInterneService.currentGestionnaireInterne.next(response);
				this.gestionnaireInterne = response;
			},
			(error) => {
				this.notificationService.blockUiStop();
			}
		);
	}
	datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
			// excludeWeekends:true,
			// fromMinMax: {fromDate:fromMin, toDate:fromMax},
			// toMinMax: {fromDate:toMin, toDate:toMax}
		};
	}
	setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
	}

	updateRange(range: Range) {
		//this.notificationService.blockUiStart();

		this.range = range;

		// this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd')
		// this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd')
	}
}
