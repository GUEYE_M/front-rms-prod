import { Component, OnInit, ViewChild } from '@angular/core';
import { InterimService } from '../../../../../services/interim/interim.service';
import { NotificationService } from '../../../../../services/notification.service';
import { DatePipe } from '@angular/common';

import { ApiVilleService } from '../../../../../services/api_ville_service';
import { ClientService } from '../../../../../services/recruteur/client.service';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { DataModel, TypeChartModel, ChartModel } from 'src/shared/chart.model';
import { NgForm } from '@angular/forms';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import { UtilsService } from 'src/shared/services/utils.service';
moment.locale('fr', localization);
@Component({
	selector: 'app-stat-interimaire',
	templateUrl: './stat-interimaire.component.html',
	styleUrls: [ './stat-interimaire.component.css' ]
})
export class StatInterimaireComponent implements OnInit {
	color = 'primary';
	color1 = 'warn';
	color2 = 'accent';
	mode = 'determinate';
	value = 50;
	bufferValue = 75;
	diponibilite: any = 0;
	inscrit: any = 0;
	salaire: any = 0;
	sallaire_annuler: any = 0;
	dossier_valider: any = 0;
	homme: any = 0;
	femme: any = 0;
	equivalant_journee: any = 0;
	myrms: any = 0;
	locale = LOCALE;
	range: Range = { fromDate: new Date(), toDate: new Date() };

	viewdatepiker: boolean = false;
	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];

	@ViewChild('dateRangePicker', { static: false })
	dateRangePicker;

	labelPosition: 'nbr' | 'ca' = 'nbr';

	barChartVille: any;
	pie3dChart: any;
	barChart: any;

	currentUser: any;
	elements = [];
	data = {
		filter: null,
		annee: 2020,
		interim: null
	};
	displayedColumnsFilter = [
		{
			name: 'interim',
			label: 'Nom interim'
		},
		// {
		//   name: "specialite",
		//   label: "Specialite"
		// },
		{
			name: 'annee',
			label: 'Annee'
		}
	];
	filtrer = false;
	annees = [];

	listSpecialites = [];
	constructor(
		private interimService: InterimService,
		private notificationService: NotificationService,
		private utilService: UtilsService,
		private clientService: ClientService,
		private specialiteService: SpecialiteService
	) {}

	ngOnInit() {
		this.annees = this.utilService.yearForGraph();

		const data = {
			classe: 1
		};
		this.clientService.getChOrInterimByRegion(data).subscribe((response: any) => {
			if (response.data) {
				this.chartBarVille(response.data);
			}
		});

		this.getStatistique(this.data);
		this.getAllSpecialite();
	}
	getAllSpecialite() {
		this.specialiteService.list(null, true).subscribe((response: any) => {
			this.listSpecialites = response.data;
		});
	}

	chartBarVille(donnee) {
		let data = new DataModel();
		data.chart = {
			caption: 'Interimaire Actif',
			subcaption: '(par region)',
			yaxisname: 'En Chiffre',
			showvalues: '',
			theme: 'fusion'
		};

		data.data = donnee;

		this.barChartVille = new TypeChartModel('100%', 500, 'column2d', 'json', data);
	}

	pieChart(donnee: any) {
		let data = new DataModel();
		let chartModel = new ChartModel();

		if (this.labelPosition === 'ca') {
			chartModel.caption = ' Evolution Rémunération';
			chartModel.numberprefix = '€';
			chartModel.yaxisname = 'Chiffre en euro';
		} else if (this.labelPosition === 'nbr') {
			chartModel.caption = 'Evolution Nombre Candidature';
			chartModel.numbersuffix = 'nombre';
			chartModel.numberprefix = '';
			chartModel.yaxisname = 'En Chiffre';
			chartModel.plottooltext = '$label, <b>$value</b> candidature(s)';
		}

		chartModel.subcaption = 'Par statut';
		chartModel.theme = 'fusion';
		chartModel.showvalues = '1';
		chartModel.valuefontcolor = '1CAF9A';

		data.chart = chartModel;
		data.data = donnee;

		this.pie3dChart = new TypeChartModel('100%', 500, 'pie3d', 'json', data);
	}

	chartBar(donnee) {
		let data = new DataModel();

		if (this.labelPosition === 'ca') {
			data.chart = {
				caption: 'Evolution  Rémunération pour chaque mois',
				subcaption: 'Par statut',
				numberprefix: '€',
				numbersuffix: '',
				plottooltext: '   <b>$dataValue</b> date de  candidature $seriesName pour le mois de $label',
				theme: 'fusion'
			};
		} else {
			data.chart = {
				caption: 'Evolution Nombre De Candidature Par Mois',
				subcaption: 'Par statut',
				plottooltext: ' <b>$dataValue</b> candidature $seriesName  pour le mois de $label ',
				theme: 'fusion'
			};
		}

		(data.categories = [
			{
				category: [
					{
						label: 'Janvier'
					},
					{
						label: 'Fevrier'
					},
					{
						label: 'Mars'
					},
					{
						label: 'Avril'
					},
					{
						label: 'Mai'
					},
					{
						label: 'Juin'
					},
					{
						label: 'Juillet'
					},
					{
						label: 'Aout'
					},
					{
						label: 'Septembre'
					},
					{
						label: 'Octobre'
					},
					{
						label: 'Novembre'
					},
					{
						label: 'Décembre'
					}
				]
			}
		]),
			(data.dataset = donnee);

		this.barChart = new TypeChartModel('100%', 500, 'stackedcolumn3d', 'json', data);
	}

	getStatistique(data: any) {
		this.notificationService.blockUiStart();
		this.interimService.getStatistiqueByYear(data).subscribe((response: any) => {
			this.notificationService.blockUiStop();

			if (!response.erreur) {
				this.inscrit = response.other.interim_inscrit;
				this.myrms = response.other.my_rms;
				this.dossier_valider = response.other.dossier_valide;
				this.diponibilite = +response.other.disponible;
				this.salaire = response.other.salaire;

				this.response = response;
				this.loadgrah(response);
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	loadgrah(response) {
		if (this.labelPosition === 'nbr') {
			this.pieChart(response.data2.nbr);
			this.chartBar(response.dataset_nbr);
		}
		if (this.labelPosition === 'ca') {
			this.pieChart(response.data2.salaire);
			this.chartBar(response.data);
		}
	}

	newStat() {
		this.loadgrah(this.response);
	}

	valutElements() {
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.data.annee = 2020;

			this.loadgrah(this.response);
		}
	}
	response(response: any) {
		throw new Error('Method not implemented.');
	}

	onSubmitFiltre(form: NgForm) {
		this.data.filter = form.value;

		if (form.controls['annee']) {
			this.data.annee = form.controls['annee'].value;
		}

		this.getStatistique(this.data);
	}

	onFiltrer() {
		this.filtrer = !this.filtrer;
	}
}
