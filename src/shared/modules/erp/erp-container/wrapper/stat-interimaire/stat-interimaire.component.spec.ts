import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatInterimaireComponent } from './stat-interimaire.component';

describe('StatInterimaireComponent', () => {
  let component: StatInterimaireComponent;
  let fixture: ComponentFixture<StatInterimaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatInterimaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatInterimaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
