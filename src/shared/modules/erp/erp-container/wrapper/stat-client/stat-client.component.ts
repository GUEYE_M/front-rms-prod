import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { ClientService } from '../../../../../services/recruteur/client.service';
import { DatePickerConfigServiceService } from '../../../../../utils/date-picker-config-service.service';
import { DatePipe } from '@angular/common';
import { ApiVilleService } from '../../../../../services/api_ville_service';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';

import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { DataModel, ChartModel, TypeChartModel } from 'src/shared/chart.model';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Client } from 'src/shared/models/recruteur/Client.model';
import { NotificationService } from 'src/shared/services/notification.service';
import { UtilsService } from 'src/shared/services/utils.service';
moment.locale('fr', localization);

@Component({
	selector: 'app-stat-client',
	templateUrl: './stat-client.component.html',
	styleUrls: [ './stat-client.component.css' ]
})
export class StatClientComponent implements OnInit {
	@ViewChild('dateRangePicker', { static: false })
	dateRangePicker;

	range: Range = { fromDate: new Date(), toDate: new Date() };
	clientList: Client[];
	labelPosition: 'nbr' | 'ca' = 'nbr';

	barChart: any;
	pie3dChart: any;
	barChartVille: any;

	elements = [];
	data = {
		filter: null,
		client: null,
		annee: 2020
	};

	response: any = [];
	listSpecialites = [];

	displayedColumnsFilter = [
		{
			name: 'client',
			label: 'Nom Etablissement'
		},
		// {
		//   name: "specialite",
		//   label: "Specialite"
		// },
		{
			name: 'annee',
			label: 'Annee'
		}
	];

	annees: any[];

	constructor(
		private clientService: ClientService,
		private specialiteService: SpecialiteService,
		private datePipe: DatePipe,
		private notificationService: NotificationService,
		private utiles: UtilsService
	) {}
	ngOnInit() {
		const data = {
			classe: 2
		};
		this.clientService.getChOrInterimByRegion(data).subscribe((response: any) => {
			if (response.data) {
				this.chartBarVille(response.data);
			}
		});
		this.annees = this.utiles.yearForGraph();
		this.getStatistiqueTarifPlanning(this.data);

		this.getAllSpecialite();
		this.allClient();
	}
	getAllSpecialite() {
		this.specialiteService.list(null, true).subscribe((response: any) => {
			this.listSpecialites = response.data;
		});
	}
	newStat() {
		this.loadgrah(this.response);
	}

	getStatistiqueTarifPlanning(data: any) {
		this.notificationService.blockUiStart();
		this.clientService.getStatistiqueTarifCh(data).subscribe((response: any) => {
			this.notificationService.blockUiStop();
			if (!response.erreur) {
				this.response = response;
				this.loadgrah(response);
			}
		});
	}

	loadgrah(response) {
		if (this.labelPosition === 'nbr') {
			this.pieChart(response.data.nbr);
			this.chartBar(response.data_mois_nbr);
		}
		if (this.labelPosition === 'ca') {
			this.pieChart(response.data.tarif);
			this.chartBar(response.data_mois_tarif);
		}
	}

	pieChart(donnee: any) {
		let data = new DataModel();
		let chartModel = new ChartModel();
		if (this.labelPosition === 'ca') {
			chartModel.caption = ' Evolution Planning';
			chartModel.numberprefix = '€';
			chartModel.yaxisname = 'Chiffre en euro';
		} else if (this.labelPosition === 'nbr') {
			chartModel.caption = 'Nombre total de date de planning';
			chartModel.numberprefix = '';
			chartModel.yaxisname = 'En Chiffre';
		}

		chartModel.subcaption = 'Par statut';
		chartModel.theme = 'fusion';

		data.chart = chartModel;
		data.data = donnee;

		this.pie3dChart = new TypeChartModel('100%', 500, 'pie3d', 'json', data);
	}

	chartBar(donnee: any) {
		let data = new DataModel();

		if (this.labelPosition === 'ca') {
			data.chart = {
				caption: 'Evolution  Planning  pour chaque mois',
				subcaption: 'Par statut',
				numberprefix: '€',
				numbersuffix: '',
				plottooltext: '   <b>$dataValue</b> date de planning $seriesName pour le mois de $label',
				theme: 'fusion'
			};
		} else {
			data.chart = {
				caption: 'Nombre  de date de planning   pour chaque mois',
				subcaption: 'Par statut',
				numberprefix: '',
				numbersuffix: '',
				plottooltext: '   <b>$dataValue</b> date de planning  $seriesName pour le mois de $label',
				theme: 'fusion'
			};
		}

		(data.categories = [
			{
				category: [
					{
						label: 'Janvier'
					},
					{
						label: 'Fevrier'
					},
					{
						label: 'Mars'
					},
					{
						label: 'Avril'
					},
					{
						label: 'Mai'
					},
					{
						label: 'Juin'
					},
					{
						label: 'Juillet'
					},
					{
						label: 'Aout'
					},
					{
						label: 'Septembre'
					},
					{
						label: 'Octobre'
					},
					{
						label: 'Novembre'
					},
					{
						label: 'Décembre'
					}
				]
			}
		]),
			(data.dataset = donnee);

		this.barChart = new TypeChartModel('100%', 500, 'mscolumn3d', 'json', data);
	}

	valutElements() {
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.data.annee = 2020;

			this.loadgrah(this.response);
		}
	}

	onSubmitFiltre(form: NgForm) {
		this.data.filter = form.value;

		if (form.controls['annee']) {
			this.data.annee = form.controls['annee'].value;
		}

		this.getStatistiqueTarifPlanning(this.data);
	}

	chartBarVille(donnee) {
		let data = new DataModel();

		data.chart = {
			caption: 'Nombre DE CENTRE HOSPITALIER',
			subcaption: '(par region)',
			yaxisname: 'En Chiffre',
			showvalues: '',
			theme: 'fusion'
		};

		data.data = donnee;

		this.barChartVille = new TypeChartModel('100%', 500, 'column2d', 'json', data);
	}

	allClient() {
		this.clientService.listClients(null, true).subscribe((client: any) => {
			this.clientList = client.data;
		});
	}
}
