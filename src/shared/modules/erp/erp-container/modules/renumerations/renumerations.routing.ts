import {Route, RouterModule} from '@angular/router';
import {RenumerationsComponent} from './renumerations/renumerations.component';
import {UpdateJustificatifComponent} from './update-justificatif/update-justificatif.component';
import {ModalRenumerationComponent} from './modal-renumeration/modal-renumeration.component';
import {ModalVacationsComponent} from './modal-vacations/modal-vacations.component';
import {UpdateSalaireComponent} from './update-salaire/update-salaire.component';
import {ReleveHeureModalComponent} from './releve-heure-modal/releve-heure-modal.component';
import {SingleRenumerationComponent} from './single-renumeration/single-renumeration.component';
import {UpdateReleveDheureComponent} from './update-releve-dheure/update-releve-dheure.component';

const RENUMERATION_CONFIG: Route[] = [
  {path: '', component: RenumerationsComponent},
  {path: ':id/show', component: SingleRenumerationComponent},

  {path: 'Releve-heure-modal', component: ReleveHeureModalComponent},
  {path: 'updat-salaire-modal', component: UpdateSalaireComponent},
  {path: 'update-heure-modal', component: UpdateReleveDheureComponent},
  {path: 'modal-vactions', component: ModalVacationsComponent},
  {path: 'modal-renumeration', component: ModalRenumerationComponent},
  {path: 'modal-update-justificatif', component: UpdateJustificatifComponent},

];

export const RenumerationRouting = RouterModule.forChild(RENUMERATION_CONFIG);



