import { Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../../../../../models/Confirm.dialog';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UpdateJustificatifComponent } from '../update-justificatif/update-justificatif.component';
import { ModalRenumerationComponent } from '../modal-renumeration/modal-renumeration.component';
import { ModalVacationsComponent } from '../modal-vacations/modal-vacations.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../../../../services/notification.service';
import { CandidatureService } from '../../../../../../services/candidature.service';
import { JustificatifService } from '../../../../../../services/interim/justificatif.service';
import { PaieService } from '../../../../../../services/recruteur/paie.service';
import { UtilsService } from '../../../../../../services/utils.service';
import { IMAGES_API_URL } from '../../../../../../utils/images_api_url';
import $ from 'jquery';

@Component({
	selector: 'app-justificatif',
	templateUrl: './justificatif.component.html',
	styleUrls: [ './justificatif.component.css' ]
})
export class JustificatifComponent implements OnInit {
	fileUrl = IMAGES_API_URL;

	data: any;
	renumerationPresta: any;
	vacations: any;
	idContrat: any;
	id: any;
	formatValide = false;
	fd: FormData = new FormData();
	files = [];
	fileDifferentImage = this.fileUrl + 'images/fichier.png';

	typefichierExiste = [];

	vacationsKeys: any;
	justificatifs: any;
	dialogRef: MatDialogRef<ModalVacationsComponent>;
	dialogRef1: MatDialogRef<ModalRenumerationComponent>;
	dialogRef2: MatDialogRef<UpdateJustificatifComponent>;

	pieceForm: FormGroup;
	pieceItemsForm: FormGroup;

	@Input() params: any;



  diplomesItemsForm: FormGroup;
  addDiplomeEtat = false;
  diplomeForm: FormGroup;
  fdiplome: FormData = new FormData();
  constructor(
		private utilsService: UtilsService,
		private paieService: PaieService,
		private justificatifService: JustificatifService,
		private candidatureService: CandidatureService,
		private fb: FormBuilder,
		private notificationService: NotificationService,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private dialog: MatDialog
	) {}

	ngOnInit() {
    this.id = this.params.id;
		this.justificatifs = this.params.justificatif;
    this.initDiplomeForm();
  }

	onModalUpdateJustificatif() {
		const items = { id: this.id, justificatif: this.justificatifs, idInterim: this.params.idInterimaire, idClient: this.params.idClient };
		this.dialogRef2 = this.dialog.open(UpdateJustificatifComponent, {
			width: '50%',
			height: '460px',
			panelClass: 'myapp-no-padding-dialog',
			data: items
		});
	}

	siNullRenderZero(item) {
		return this.utilsService.siNullRenderZero(item);
	}


	// getFile(event) {
	// 	if (event.target.files && event.target.files[0]) {
	// 		const reader = new FileReader();
	// 		const fileType = event.target.files[0].type.split('/');
	// 		const file = event.target.files[0];
	// 		reader.onload = (e: any) => {
	// 			if (fileType.includes('video') === false) {
	// 				this.pieceForm.value.items.forEach((element) => {
	// 					this.fd.append(element.typefichier, file);
	// 				});
	// 			}
	// 		};
	// 		reader.readAsDataURL(event.target.files[0]);
	// 	}
	// }

	save() {
		const tableau = { id: this.justificatifs.id, files: this.fd };
		this.notificationService.blockUiStart();
		this.justificatifService.creerPiece(tableau).subscribe(
			(resp) => {
				this.notificationService.blockUiStop();
				this.utilsService.redirectEspace({ espace: 'renumerations', id: this.id });
			},
			(err) => {
				this.notificationService.blockUiStop();
			},
			() => {}
		);
	}

	onDelete(item) {
		this.notificationService.onConfirm('Êtes-vous sûr de vouloir supprimer ?');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notificationService.blockUiStart();
				this.justificatifService.delete(item).subscribe(
					(resp) => {
            this.utilsService.redirectEspace({ espace: 'renumerations', id: this.id });
            this.notificationService.blockUiStop();
					},
					(err) => {
						this.notificationService.blockUiStop();
					},
					() => {}
				);
			}
		});
	}
  addNewRow() {
    this.formArr.push(this.initItemRows());
  }
  get formArr() {
    return this.diplomeForm.get('diplomes') as FormArray;
  }
	actionJustificatif() {
		this.notificationService.onConfirm('Relancer le mail de demande de justificarif ?');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notificationService.blockUiStart();
				const data = {
					id: this.justificatifs.id
				};
				this.justificatifService.relanceJistificatif(data).subscribe(
					(response) => {
						if (response.erreur) {
							this.notificationService.showNotificationEchec(response.erreur);
						} else {
							this.notificationService.showNotificationSuccess(response.success);
							this.justificatifs.nombreRelance = this.justificatifs.nombreRelance + 1;
						}
					},
					(erreur) => {
						this.notificationService.blockUiStop();
						this.notificationService.showNotificationEchec('erreur server veillez ressayer');
					},
					() => {
						this.notificationService.blockUiStop();
					}
				);
			}
		});
	}

	onActifOuInactif(justificatif, statut) {
		const message = '';
		let content = '';

		if (statut === true) {
			content = 'Êtes-vous sûr de valider les justificatifs ?? ';
		} else {
			content = "Êtes-vous sûr d'annuler les justificatifs ?? ";
		}
		const dialogData = new ConfirmDialogModel(content, message);

		this.notificationService.onConfirm(content);
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notificationService.blockUiStart();

				const data = {
					id: justificatif.id,
					action: statut,
					contratId: this.idContrat
				};

				this.justificatifService.valideOrInvalide(data).subscribe(
					(response: any) => {
						if (response.erreur) {
							this.notificationService.showNotificationEchec(response.erreur);
						} else {
							this.justificatifs.actif = statut;
							this.notificationService.showNotificationSuccess(response.success);
						}
					},
					(erreur) => {},
					() => {
						this.notificationService.blockUiStop();
					}
				);
			}
		});
	}

  initDiplomeForm() {
    this.diplomeForm = this.fb.group({
      diplomes: this.fb.array([ this.initItemRows() ])
    });
  }
  initItemRows() {
    this.diplomesItemsForm = this.fb.group({
      nom: [ '', Validators.required ]
    });
    return this.diplomesItemsForm;
  }

  getFile(event, i) {
    if (event.target.files && event.target.files[0]) {
      const nameTmp = '#nameTmpDiplome' + i;
      const reader = new FileReader();
      const fileType = event.target.files[0].type.split('/');
      const file = event.target.files[0];
      // Pour vider le nom du fichier a la creation
      $(nameTmp).children('span').remove();
      reader.onload = (e: any) => {
        if (fileType.includes('image') === true || fileType.includes('pdf') === true) {
          this.formatValide = true;
          this.diplomeForm.value.diplomes.forEach((element) => {
            $(nameTmp).append('<span>' + file.name + '</span>');
            this.fdiplome.append(element.nom, file);
          });
        } else {
          this.formatValide = false;
          this.notificationService.showNotificationEchec("Le format du fichir est invalide !")
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  deleteRow(index: number) {
    // this.formArr.removeAt(index);
    this.initDiplomeForm();
  }
  addNewDiplome() {
    this.addDiplomeEtat = !this.addDiplomeEtat;
  }
  onUpload() {
    if(this.formatValide){
      const tableau = { id: this.justificatifs.id, files: this.fdiplome };
      this.notificationService.blockUiStart();
      this.justificatifService.creerPiece(tableau).subscribe(
        (resp) => {
          this.notificationService.blockUiStop();
          this.utilsService.redirectEspace({ espace: 'renumerations', id: this.id });
        },
        (err) => {
          this.notificationService.blockUiStop();
        },
        () => {}
      );
    }
    else{
      this.notificationService.showNotificationEchec("Le format du fichier est invalide !")
     }
  }

}
