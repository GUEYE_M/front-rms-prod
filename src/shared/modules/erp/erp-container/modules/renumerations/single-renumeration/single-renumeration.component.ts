import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {UtilsService} from '../../../../../../services/utils.service';

@Component({
  selector: 'app-single-renumeration',
  templateUrl: './single-renumeration.component.html',
  styleUrls: ['./single-renumeration.component.css']
})
export class SingleRenumerationComponent implements OnInit {
  id: number;
  data: any;
  contratPld: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private candidatureService: CandidatureService,
    private notificationService: NotificationService,
    private utilService: UtilsService,
  ) { }

  ngOnInit() {
    this.notificationService.blockUiStart();
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.candidatureService.onDetailPaie(this.id).subscribe((response) => {
          this.notificationService.blockUiStop();
          this.data = response;
          this.contratPld = JSON.parse(this.data.contrat.pld);
          this.utilService.onRevientEnHaut();
        },
        error => {
          this.notificationService.blockUiStop();
        });
    }
  }

}
