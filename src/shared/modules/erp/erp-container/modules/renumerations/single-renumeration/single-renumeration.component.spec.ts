import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRenumerationComponent } from './single-renumeration.component';

describe('SingleRenumerationComponent', () => {
  let component: SingleRenumerationComponent;
  let fixture: ComponentFixture<SingleRenumerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleRenumerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleRenumerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
