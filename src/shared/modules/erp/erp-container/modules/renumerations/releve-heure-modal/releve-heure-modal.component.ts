import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NotificationService } from '../../../../../../services/notification.service';
import { PldService } from '../../../../../../services/pld.service';
import { MissionService } from '../../../../../../services/mission.service';
import { UtilsService } from '../../../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { CandidatureService } from '../../../../../../services/candidature.service';
import { PaieService } from '../../../../../../services/recruteur/paie.service';

@Component({
	selector: 'app-releve-heure-modal',
	templateUrl: './releve-heure-modal.component.html',
	styleUrls: [ './releve-heure-modal.component.css' ]
})
export class ReleveHeureModalComponent implements OnInit {
	form: FormGroup;
	nbrFacture = 0;
	deplacement = 0;
	repas = 0;
	hebergement = 0;
	renumerationPresta: any;
	idContrat: any;
	TabPrimes: [];
	releveHeureObjet = {
		NumeroContrat: 0,
		IdClient: '',
		IdInterimaire: 0,
		IdSalarie: 0,
		DateDebut: '',
		DateFin: '',
		Reference: '',
		IFMICP: '',
		FinDeMission: 'O',
		MontantHeurePaye: 0,
		MontantImposablePaye: 0,
		MontantNonImposablePaye: 0,
		MontantFacture: 0,
		TabJours: [],
		TabHeures: [],
		TabPrimes: []
	};
	constructor(
		private fb: FormBuilder,
		private notificationService: NotificationService,
		private pldService: PldService,
		private dialog: MatDialog,
		private utilsService: UtilsService,
		private candidatureService: CandidatureService,
		private activatedRoute: ActivatedRoute,
		private missionService: MissionService,
		private paieService: PaieService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {}

	ngOnInit() {
		this.notificationService.blockUiStart();
		this.idContrat = this.data.items.contrat;
		this.candidatureService.findCandidaturesByIdContratPld(this.idContrat).subscribe((resp: any) => {
			this.paieService.findByPld(this.idContrat).subscribe((resultat: any) => {
				if (!resultat.erreur) {
					let keys: any;
					this.renumerationPresta = resultat.data;
					keys = Object.keys(this.renumerationPresta);
					keys.forEach((element) => {
						if (
							(element === 'voiture' ||
								element === 'montantIk' ||
								element === 'montantTicketPeage' ||
								element === 'montantLocationVoiture' ||
								element === 'montantParking' ||
								element === 'montantCovoiturage' ||
								element === 'montantBus' ||
								element === 'montantTaxi' ||
								element === 'montantAvion' ||
								element === 'montantTrain') &&
							this.renumerationPresta[element] !== 'null'
						) {
							this.deplacement += Number(this.renumerationPresta[element]);
						}
						if (element === 'montantHotel' && this.renumerationPresta[element] !== 'null') {
							this.hebergement += Number(this.renumerationPresta[element]);
						}
						if (element === 'montantRepas' && this.renumerationPresta[element] !== 'null') {
							this.repas += Number(this.renumerationPresta[element]);
						}
					});
					this.notificationService.blockUiStop();
					this.initForm(this.deplacement, this.hebergement, this.repas);
				}
			});
		});
		this.TabPrimes = this.data.TabPrimes ? this.data.TabPrimes : [];
		this.data = this.data.items ? this.data.items : [];
		if (this.data.jtc === 'h') {
			this.data.tabJours.forEach((element) => {
				this.nbrFacture += element.NbHeureJour;
			});
		} else {
			this.data.tabJours.forEach((element) => {
				this.nbrFacture += 1;
			});
		}
	}

	initForm(deplacement, hebergement, repas) {
		this.form = this.fb.group({
			reference: [ 'PRESTATIONS' ],
			IFMICP: [ 3, Validators.required ],
			deplacement: [ deplacement ],
			hebergement: [ hebergement ],
			repas: [ repas ]
		});
	}
	onSave() {
		// this.notificationService.blockUiStart();
		const TabPrimesRH = [];
		if (this.deplacement > 0) {
			const Primes = {
				IdPrime: '026',
				TxtPrime: 'Prime déplacement',
				LibPrimeComplement: '',
				MethodeCalcul: '',
				QuantiteImposable: 0,
				TauxPayeImposable: this.deplacement,
				QuantiteNonImposable: 0,
				TauxPayeNonImposable: 0,
				QuantiteFacture: this.nbrFacture,
				TauxFacture: ''
			};
			TabPrimesRH.push(Primes);
		}
		if (this.repas > 0) {
			const Primes = {
				IdPrime: '028',
				TxtPrime: 'Prime repas',
				LibPrimeComplement: '',
				MethodeCalcul: '',
				QuantiteImposable: 0,
				TauxPayeImposable: this.repas,
				QuantiteNonImposable: 0,
				TauxPayeNonImposable: 0,
				QuantiteFacture: this.nbrFacture,
				TauxFacture: ''
			};
			TabPrimesRH.push(Primes);
		}
		if (this.hebergement > 0) {
			const Primes = {
				IdPrime: '029',
				TxtPrime: 'Prime hébergement',
				LibPrimeComplement: '',
				MethodeCalcul: '',
				QuantiteImposable: 0,
				TauxPayeImposable: this.deplacement,
				QuantiteNonImposable: 0,
				TauxPayeNonImposable: 0,
				QuantiteFacture: this.nbrFacture,
				TauxFacture: ''
			};
			TabPrimesRH.push(Primes);
		}
		this.TabPrimes.forEach((element) => {
			if (element) {
				const Primes = {
					IdPrime: element['IdPrime'],
					TxtPrime: element['TxtPrime'],
					LibPrimeComplement: '',
					MethodeCalcul: '',
					QuantiteImposable: this.nbrFacture,
					TauxPayeImposable: element['TauxPayeImposable'],
					QuantiteNonImposable: 0,
					TauxPayeNonImposable: 0,
					QuantiteFacture: this.nbrFacture,
					TauxFacture: element['TauxFacture']
				};
				TabPrimesRH.push(Primes);
			}
		});
		//
		this.releveHeureObjet.NumeroContrat = this.data.contrat;
		this.releveHeureObjet.IdInterimaire = this.data.interimaire;
		this.releveHeureObjet.IdSalarie = this.data.interimaire;
		this.releveHeureObjet.IdClient = this.data.client;
		this.releveHeureObjet.DateDebut = this.data.dateDebutPLD;
		this.releveHeureObjet.DateFin = this.data.dateFinPld;
		this.releveHeureObjet.TabJours = this.data.tabJours;
		this.releveHeureObjet.Reference = this.form.value.reference;
		this.releveHeureObjet.IFMICP = this.form.value.IFMICP;
		this.releveHeureObjet.TabPrimes = TabPrimesRH;
		const data = {
			idContratPld: this.data.contrat,
			RH: this.releveHeureObjet
		};
		this.pldService.creerRH(data).subscribe(
			(resp) => {
				if (!resp['erreur']) {
					this.notificationService.blockUiStop();
					this.utilsService.redirectEspace({ espace: 'renumerations', id: data.idContratPld });
				} else {
					this.notificationService.blockUiStop();
				}
				this.dialog.closeAll();
			},
			(error) => {
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}
}
