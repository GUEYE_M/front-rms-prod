import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleveHeureModalComponent } from './releve-heure-modal.component';

describe('ReleveHeureModalComponent', () => {
  let component: ReleveHeureModalComponent;
  let fixture: ComponentFixture<ReleveHeureModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleveHeureModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleveHeureModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
