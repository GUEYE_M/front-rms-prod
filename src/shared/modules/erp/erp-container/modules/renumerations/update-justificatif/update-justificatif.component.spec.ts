import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateJustificatifComponent } from './update-justificatif.component';

describe('UpdateJustificatifComponent', () => {
  let component: UpdateJustificatifComponent;
  let fixture: ComponentFixture<UpdateJustificatifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateJustificatifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateJustificatifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
