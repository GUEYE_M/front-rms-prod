import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NotificationService} from '../../../../../../services/notification.service';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {UtilsService} from '../../../../../../services/utils.service';
import {JustificatifService} from '../../../../../../services/interim/justificatif.service';
import {EditInterimComponent} from '../../interim/edit-interim/edit-interim.component';
import {EditListeClientComponent} from '../../client-wrapper/client/edit-liste-client/edit-liste-client.component';
import {toNumber} from 'ngx-bootstrap/timepicker/timepicker.utils';

@Component({
  selector: 'app-update-justificatif',
  templateUrl: './update-justificatif.component.html',
  styleUrls: ['./update-justificatif.component.css']
})
export class UpdateJustificatifComponent implements OnInit {

  form: FormGroup;
  id: any;
  justificatif: any;
  constructor(
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private router: Router,
    private utilsService: UtilsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private justificatifService: JustificatifService,
  ) {}

  ngOnInit() {
    this.id = this.data.id;
    this.justificatif = this.data.justificatif;
    this.initform();
  }

  initform(params = {
    adresseMedecin: this.justificatif.adresseInterim,
    adresseHopital: this.justificatif.adresseClient,
    nombreKm: this.justificatif.km,
    nombreTrajet: this.justificatif.trajet,
    coefIk: this.justificatif.ik
  }) {
    this.form = this.fb.group(
      {
        adresseMedecin: [params.adresseMedecin],
        adresseHopital: [params.adresseHopital],
        nombreKm: [params.nombreKm],
        nombreTrajet: [params.nombreTrajet],
        coefIk: [params.coefIk]
      });
  }
  save() {
    const params = {'id': this.justificatif.id, 'data': this.form.value};
    this.justificatifService.update(params).subscribe((response) => {
        if (!response['erreur']) {

          this.utilsService.redirectEspace({espace: 'renumerations', id: this.id});
          this.dialog.closeAll();
        } else {
          this.notificationService.showNotificationEchec(response['erreur']);
        }

      },
      error => {
        this.notificationService.showNotificationEchec('Erreur lors du chargement de la requête.Veuillez réessayer !');
      });
  }
  mappy() {
    window.open('http://fr.mappy.com/#/5/M2/Ls/TItinerary/IFR' +  this.justificatif.adresseInterim + ' |TO ' +  this.justificatif.adresseClient + ', _blank,toolbar=0, location=0,directories=0, status=0, scrollbars=1, resizable=1, copyhistory=0, menuBar=0, width=1500, height=944, left=200, top=0');
  }

  OnUpdateInterimaire() {
    const donnees = { id: this.data.idInterim, profil: false };
    this.dialog.open(EditInterimComponent, {
      width: '80%',
      height: '90%',
      panelClass: 'myapp-no-padding-dialog',
      data: donnees
    });
  }

  editClientModal() {
    let data = {
      profil: false,
      id: this.data.idClient
    };
    this.dialog.open(EditListeClientComponent, {
      width: '80%',
      height: '80%',
      panelClass: 'myapp-no-padding-dialog',
      data: data
    });
  }
}
