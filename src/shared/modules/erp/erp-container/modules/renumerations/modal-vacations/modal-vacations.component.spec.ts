import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVacationsComponent } from './modal-vacations.component';

describe('ModalVacationsComponent', () => {
  let component: ModalVacationsComponent;
  let fixture: ComponentFixture<ModalVacationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalVacationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVacationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
