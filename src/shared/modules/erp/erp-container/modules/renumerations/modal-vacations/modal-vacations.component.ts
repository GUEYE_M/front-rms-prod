import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {PaieService} from '../../../../../../services/recruteur/paie.service';

@Component({
  selector: 'app-modal-vacations',
  templateUrl: './modal-vacations.component.html',
  styleUrls: ['./modal-vacations.component.css']
})
export class ModalVacationsComponent implements OnInit {
  data: any;
  renumerationPresta: any;
  vacations: any;
  vacationsKeys: any;

  constructor(
    private paieService: PaieService,
    private notificationService: NotificationService,
    private candidatureService: CandidatureService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public idContrat: any


  ) { }

  ngOnInit() {
    this.notificationService.blockUiStart();
    this.candidatureService.findCandidaturesByIdContratPld(this.idContrat).subscribe((resp) => {
      this.notificationService.blockUiStop();
      this.vacationsKeys = Object.keys(resp['candidatures'][0]);
      this.vacations = resp['candidatures'];
    });
  }

}
