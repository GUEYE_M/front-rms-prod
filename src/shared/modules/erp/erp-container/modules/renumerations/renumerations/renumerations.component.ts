import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { UtilsService } from '../../../../../../services/utils.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../../../services/notification.service';
import { PaieService } from '../../../../../../services/recruteur/paie.service';
import { SelectionModel } from '@angular/cdk/collections';
import { Specialite } from '../../../../../../models/Specialite.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FilterService } from '../../../../../../services/filter.service';
import {RolesService} from '../../../../../../services/roles.service';
import {UserService} from '../../../../../../services/user.service';

@Component({
	selector: 'app-renumerations',
	templateUrl: './renumerations.component.html',
	styleUrls: [ './renumerations.component.scss' ]
})
export class RenumerationsComponent implements OnInit {
	panelOpenState = false;
	formFilter: FormGroup;
  loader = true;
  listSpecialites: Specialite[] = [];
	dataSourceInitial = [];
	dataSourceglobal = [];
  newArrayByidInterim = [];
	view = false;
	actionInput = false;
	action: string;
	dataSource: MatTableDataSource<any>;
	statsSource: any;
	selection = new SelectionModel<any[]>(true, []);
	@ViewChild(MatSort, { static: false }) sort: MatSort;

  @ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;
	listActions: any[];
	displayedColumns = [
		'num',
		'contrat',
		'rh',
		'etablissement',
		'ref',
    'dateContrat',
    'cec',
    'csc',
		'cei',
    'csi',
		'total',
		'prestation',
		'frais',
		'totalTarif'
	];
	dataSourceUniqueInterim = [];
	pageSize = 12;
	length: number = 12;
	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null
	};
	pageSizeOptions: number[] = [ 3, 25, 50, 100, 150 ];

	displayedColumnsFilter = [
		{
			name: 'idPld',
			label: 'contrat'
		},
    {
      name: 'rh',
      label: 'rh'
    },
		{
			name: 'nom',
			label: 'Nom'
		},
		{
			name: 'prenom',
			label: 'Prenom'
		},
		{
			name: 'mois',
			label: 'Mois'
		},
		{
			name: 'annee',
			label: 'Annee'
		},
		{
			name: 'etablissement',
			label: 'CH'
		},
		{
			name: 'specialite',
			label: 'Specialite'
		},
		{
			name: 'statut',
			label: 'Activé'
		}
	];
	allmois = [];
  currentUser: any;

	constructor(
		private paieService: PaieService,
		private notificationService: NotificationService,
		private router: Router,
		private filterService: FilterService,
		private formBuilder: FormBuilder,
		private dialog: MatDialog,
		private utilsService: UtilsService,
		private snackBar: MatSnackBar,
    public roleService: RolesService,
    private userService: UserService,
  ) {}

	ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.formFilter = this.formBuilder.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		this.allmois = this.utilsService.getMois();
		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
        this.onSubmitFiltre();
      }
		}
		this.getNewData(this.data);
		this.getStats(this.data);
	}
	getNewData(data) {
		this.notificationService.blockUiStart();
		this.paieService.list(data).subscribe(
			(response: any) => {
        this.returnUniqueInterimAndStat(response.data);
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSourceglobal = response.data;
				this.length = response.lenght;

				setTimeout(() => {
					this.dataSource.sort = this.sort;
					this.dataSource.paginator = this.paginator;
				}, 2000);
				this.notificationService.blockUiStop();
			},
			(error) => {
				this.notificationService.blockUiStop();
			}
		);
	}

	returnUniqueInterimAndStat(data){
    const resultat = Array.from(data.reduce(
      (m, t) => m.set(
        t.idInterim, {
          interim: t.interim,
          idInterim: t.idInterim,
          client: this.sumClientByData(data, t.idInterim),
          contratByRh: this.sumContratByData(data, t.idInterim),
          rh: this.sumContratByData(data, t.idInterim),
          cs: this.sumCcByData(data, t.idInterim),
          ci: this.sumCiByData(data, t.idInterim),
          total: this.sumPrestaByData(data, t.idInterim) + this.sumFraisByData(data, t.idInterim),
          prestat: this.sumPrestaByData(data, t.idInterim),
          frais: this.sumFraisByData(data, t.idInterim),
          tarif: this.sumTarifByData(data, t.idInterim),
        }
      ),  new Map()
    ).values());
    this.dataSourceUniqueInterim = resultat;
  }
  sumFraisByData(data, idInterim){
	  let rep = 0;
	  data.forEach((el)=>{
	    if(el.idInterim === idInterim){
        rep += el.frais;
      }
    });
	  return rep;
  }
  sumClientByData(data, idInterim){
    let rep = [];
    data.forEach((el)=>{
      if(el.idInterim === idInterim && !rep.includes(el.idClient)){
        rep.push(el.idClient);
      }
    });
    return rep.length;
  }

  sumCcByData(data, idInterim){
    let rep = '0 / 0';
    const esc = [];
    const sc = [];
    data.forEach((el)=>{
      if(el.idInterim === idInterim){
        if(el.esc.etat === true){
          esc.push(el.esc.etat)
        }
        if(el.sc.etat  === true){
          sc.push(el.sc.etat);
        }
      }
    });
    rep = esc.length +' / '+ sc.length;
    return rep;
  }

  sumCiByData(data, idInterim){
    let rep = 0 + '/' + 0;
    const esi = [];
    const si = [];
    data.forEach((el)=>{
      if(el.idInterim === idInterim){
        if(el.esi.etat === true){
          esi.push(el.esc.etat)
        }
        if(el.si.etat === true){
          si.push(el.sc.etat);
        }
      }
    });
    rep = esi.length +' / '+ si.length;
    return rep;
  }
  sumContratByData(data, idInterim){
    let rep = 0 + '/' + 0;
    let c = 0;
    const rh = [];
    data.forEach((el)=>{
      if(el.idInterim === idInterim){
        c++;
        if(el.idRhPld === true){
          rh.push(el.idRhPld)
        }
      }
    });
    rep = c +" / "+ rh.length;
    return rep;
  }

  sumPrestaByData(data, idInterim){
    let rep = 0;
    data.forEach((el)=>{
      if(el.idInterim === idInterim){
        rep += el.prestation;
      }
    });
    return rep;
  }
  sumTarifByData(data, idInterim){
    let rep = 0;
    data.forEach((el)=>{
      if(el.idInterim === idInterim){
        rep += el.totalTarif;
      }
    });
    return rep;
  }
	getStats(data){
    this.loader = true;
    this.paieService.statsRemuneration(data).subscribe(
      (response: any) => {
        this.loader = false;
        this.statsSource = response;
      },
      (error) => {
        this.loader = false;
        this.notificationService.showNotificationEchec("Erreur du chargement des statistiques");
      }
    );
  }
	siNullRenderZero(item) {
		return this.utilsService.siNullRenderZero(item);
	}
	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
			this.getNewData(this.data);
			this.getStats(this.data);
		} else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
      // this.getNewData(this.data);
      this.elements.map((item) => {
				if (item === 'date') {
					// this.datepickerConfig();
				}
			});
		}
	}
  calculateDiff(date1, date2) {
    date1 = new Date(date1.date);
    date2 = new Date(date2.date);
    return Math.floor((Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate()) - Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate()) ) /(1000 * 60 * 60 * 24));
	}
	getTotalFraisAndPrestation(presta, frais){
    return presta + frais;
  }
	pagination(event: any) {
		this.pageSize = event.pageSize;
		// const data = this.defaultValue( * this.pageSize, this.pageSize);
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = event.pageSize;
		this.getNewData(this.data);
		this.getStats(this.data);
	}

	onSubmitFiltre() {
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = JSON.stringify(this.formFilter.value);

		this.getNewData(this.data);
		this.getStats(this.data);
	}
  filterDataByIdInterim(idInterim){
    this.newArrayByidInterim = this.dataSourceglobal.filter(function(item) {
      return item.idInterim === idInterim;
    });
  }
	show(id) {
    if(this.roleService.isSuperAdmin(this.currentUser.roles)){
      this.router.navigate([ '/RMS-Admin/renumerations/' + id + '/show/' ]);
    }
  }
}
