import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenumerationsComponent } from './renumerations.component';

describe('RenumerationsComponent', () => {
  let component: RenumerationsComponent;
  let fixture: ComponentFixture<RenumerationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenumerationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenumerationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
