import {Component, Input, OnInit} from '@angular/core';
import {UpdateSalaireComponent} from '../update-salaire/update-salaire.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {PaieService} from '../../../../../../services/recruteur/paie.service';
import {JustificatifService} from '../../../../../../services/interim/justificatif.service';

@Component({
  selector: 'app-salaire',
  templateUrl: './salaire.component.html',
  styleUrls: ['./salaire.component.css']
})
export class SalaireComponent implements OnInit {

  renumerationPrestaKeys = [];
  data: any;
  idContrat: any;
  renumerationPresta: any;
  idRenumerationPresta: number;
  renumerationFraisKeys = [];
  vacations: any;
  montantIk: any;
  vacationsKeys: any;
  renumerationExiste = false;
  totalKeys = [];
  dialogRef: MatDialogRef<UpdateSalaireComponent>;
  @Input() params: any;

  constructor(
    private paieService: PaieService,
    private candidatureService: CandidatureService,
    private justificatifService: JustificatifService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog

  ) { }

  ngOnInit() {
    this.idContrat = this.params.id;
    this.vacations = this.params.candidature;
    this.vacationsKeys = Object.keys(this.vacations[0]);
    let keys: any;
    this.renumerationPresta = this.params.tabRenumPresta;
    this.data = {idContrat: this.idContrat, renumerationPresta: this.renumerationPresta};
    keys = Object.keys(this.renumerationPresta);
    keys.forEach((element) => {
      if (
          element !== 'datePaiement'
          && element !== 'modePaiement'
          && element !== 'totalPrestation'
          && element !== 'etat' && element !== 'total'
          && element !== 'remarque' && element !== 'id'
          && element !== 'statut'
          && element !== 'contrat_pld'
      ) {
        this.renumerationFraisKeys.push(element);
      }
      if (element === 'datePaiement' || element === 'totalPrestation' || element === 'etat') {
        this.renumerationPrestaKeys.push(element);
      }
      if (element === 'total' || element === 'modePaiement' || element === 'remarque') {
        this.totalKeys.push(element);
      }
    });
    this.renumerationExiste = true;
  }
  updateSalaire(): void {
    this.dialogRef = this.dialog.open(UpdateSalaireComponent, {
      width: '50%',
      height: '450px',
      panelClass: 'myapp-no-padding-dialog',
      data: this.data
    });
  }


  formaterItem(item) {
    return this.paieService.formaterItem(item);
  }

  TotalTarif() {
    let response = 0;
    this.vacations.forEach((element) => {
      response += element['Tarif'];
    });
    return response;
  }
  TotalPresta() {
    let response = 0;
    this.vacations.forEach((element) => {
      response += element['Net'];
    });
    return response;
  }
  Totalfrais() {
    let response = 0;
    this.renumerationFraisKeys.forEach((element) => {
      if(element !== 'datePaiementFrais'
          && element !== 'etatFrais'
          && element !== 'dateEnreg'
      )
      response += +this.renumerationPresta[element];
    });
    return response;
  }
  Total() {
    return this.Totalfrais() + this.TotalPresta();
  }

}
