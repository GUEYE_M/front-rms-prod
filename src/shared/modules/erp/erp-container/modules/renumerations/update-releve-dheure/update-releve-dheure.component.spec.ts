import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReleveDheureComponent } from './update-releve-dheure.component';

describe('UpdateReleveDheureComponent', () => {
  let component: UpdateReleveDheureComponent;
  let fixture: ComponentFixture<UpdateReleveDheureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReleveDheureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReleveDheureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
