import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {NotificationService} from '../../../../../../services/notification.service';
import {PldService} from '../../../../../../services/pld.service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {element} from 'protractor';
import {UtilsService} from '../../../../../../services/utils.service';

@Component({
  selector: 'app-update-releve-dheure',
  templateUrl: './update-releve-dheure.component.html',
  styleUrls: ['./update-releve-dheure.component.css']
})
export class UpdateReleveDheureComponent implements OnInit {
  form: FormGroup;
  constructor(
    private dialog: MatDialog,
    private pldService: PldService,
    private utilsService: UtilsService,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    let qtHPaye = 0;
    let qtJtPaye = 0;
    this.data.TabJours.forEach(el => {
      Object.keys(form.value).forEach( item => {
        if (el.NomJour === item)
          el.NbHeureJour = form.value[item];
        qtHPaye += el.NbHeureJour;
        qtJtPaye += 1;
      });
    });

    this.data.TabPrimes.forEach(el => {
      if (el.MethodeCalcul === 'H') {
        el.QuantiteImposable = qtHPaye;
        el.QuantiteFacture = qtHPaye;
      }
      if (el.MethodeCalcul === 'jt') {
        el.QuantiteImposable = qtJtPaye;
        el.QuantiteFacture = qtJtPaye;
      }
    });

    this.notificationService.blockUiStart();
    const data = {
      idContratPld: this.data.NumeroContrat,
      RH: this.data,
    };
    this.pldService.creerRH(data).subscribe((resp) => {
        if (!resp['erreur']) {
          this.notificationService.blockUiStop();
        } else {
          this.notificationService.blockUiStop();
        }
        this.dialog.closeAll();
      },
      error => {
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      });
  }


}
