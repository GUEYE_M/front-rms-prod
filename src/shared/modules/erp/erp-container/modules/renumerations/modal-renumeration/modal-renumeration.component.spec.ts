import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRenumerationComponent } from './modal-renumeration.component';

describe('ModalRenumerationComponent', () => {
  let component: ModalRenumerationComponent;
  let fixture: ComponentFixture<ModalRenumerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRenumerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRenumerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
