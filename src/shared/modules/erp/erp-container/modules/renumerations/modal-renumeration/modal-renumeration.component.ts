import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {PaieService} from '../../../../../../services/recruteur/paie.service';

@Component({
  selector: 'app-modal-renumeration',
  templateUrl: './modal-renumeration.component.html',
  styleUrls: ['./modal-renumeration.component.css']
})
export class ModalRenumerationComponent implements OnInit {

  renumerationPrestaKeys = [];
  data: any;
  renumerationPresta: any;
  idRenumerationPresta: number;
  renumerationFraisKeys = [];
  vacations: any;
  vacationsKeys: any;
  renumerationExiste = false;
  totalKeys = [];
  constructor(
    private paieService: PaieService,
    private notificationService: NotificationService,
    private candidatureService: CandidatureService,
    @Inject(MAT_DIALOG_DATA) public idContrat: any


  ) { }
  ngOnInit() {
    this.notificationService.blockUiStart();

    this.candidatureService.findCandidaturesByIdContratPld(this.idContrat).subscribe((resp) => {
      this.notificationService.blockUiStop();
      this.vacationsKeys = Object.keys(resp['candidatures'][0]);
      this.vacations = resp['candidatures'];
      this.paieService.findByMM(resp['idMM']).subscribe((resultat) => {
        if (!resultat['erreur']) {
          let keys: any;
          this.renumerationPresta = resultat['data'];
          this.data = {'idContrat': this.idContrat, 'renumerationPresta': this.renumerationPresta};
          keys = Object.keys(this.renumerationPresta);
          keys.forEach((element) => {
            if(element !== 'datePaiement' && element !== 'modePaiement' && element !== 'totalPrestation' && element !== 'etat' && element !== 'total' && element !== 'remarque' && element !== 'id'  && element !== 'voiture') {
              this.renumerationFraisKeys.push(element);
            }
            if(element === 'datePaiement' || element === 'totalPrestation' || element === 'etat') {
              this.renumerationPrestaKeys.push(element);
            }
            if(element === 'total' || element === 'modePaiement' || element === 'remarque') {
              this.totalKeys.push(element);
            }
          });
          this.renumerationExiste = true;
        }
      });
    });
  }

  stringToFloat(item) {
    return parseFloat(item);
  }
}
