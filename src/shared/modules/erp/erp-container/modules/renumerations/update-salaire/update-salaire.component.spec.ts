import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSalaireComponent } from './update-salaire.component';

describe('UpdateSalaireComponent', () => {
  let component: UpdateSalaireComponent;
  let fixture: ComponentFixture<UpdateSalaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSalaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSalaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
