import {Component, Inject, OnInit} from '@angular/core';
import {PaieService} from '../../../../../../services/recruteur/paie.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {NotificationService} from '../../../../../../services/notification.service';
import {UtilsService} from '../../../../../../services/utils.service';

@Component({
  selector: 'app-update-salaire',
  templateUrl: './update-salaire.component.html',
  styleUrls: ['./update-salaire.component.css']
})
export class UpdateSalaireComponent implements OnInit {

  form: FormGroup;
  idContrat: any;
  renumerationPresta: any;
  constructor(
    private paieService: PaieService,
    private fb: FormBuilder,
    private router: Router,
    private utilsService: UtilsService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.idContrat = this.data.idContrat;
    this.renumerationPresta = this.data.renumerationPresta;
    this.initform();
  }
  initform() {
    this.form = this.fb.group(
      {
        datePaiement: [this.renumerationPresta.datePaiement ],
        etat: [this.renumerationPresta.etat],
        datePaiementFrais: [this.renumerationPresta.datePaiementFrais],
        etatFrais: [this.renumerationPresta.etatFrais],
        montantTrain: [this.renumerationPresta.montantTrain],
        montantAvion: [this.renumerationPresta.montantAvion],
        montantTaxi: [this.renumerationPresta.montantTaxi],
        montantBus: [this.renumerationPresta.montantBus],
        montantCovoiturage: [this.renumerationPresta.montantCovoiturage],
        montantParking: [this.renumerationPresta.montantParking],
        montantLocationVoiture: [this.renumerationPresta.montantLocationVoiture],
        montantTicketPeage: [this.renumerationPresta.montantTicketPeage],
        montantHotel: [this.renumerationPresta.montantHotel],
        montantRepas: [this.renumerationPresta.montantRepas],
        montantIk: [this.renumerationPresta.montantIk],
        autre: [this.renumerationPresta.autre],
        modePaiement: [this.renumerationPresta.modePaiement],
        remarque: [this.renumerationPresta.remarque],
      }
    );
  }
  save() {
    this.notificationService.blockUiStart();
    const content = {id: this.renumerationPresta.id, data: this.form.value};
    this.paieService.updateRenumerationContrat(content).subscribe((response) => {
        this.notificationService.blockUiStop();
        if (!response['erreur']) {
          this.notificationService.showNotificationSuccess('La mise à jour a été effectué avec succès!');
          this.utilsService.redirectEspace({espace: 'renumerations', id: this.idContrat});
          this.dialog.closeAll();
        } else {
          this.notificationService.showNotificationEchec(response['erreur']);
        }
      },
      error => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('Veuillez ressayer et si l\'erreur persiste, avertir le service IT');
      });
  }

}
