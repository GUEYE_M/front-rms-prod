import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import { RenumerationsComponent } from './renumerations/renumerations.component';
import {RenumerationRouting} from './renumerations.routing';
import { SingleRenumerationComponent } from './single-renumeration/single-renumeration.component';
import { UpdateReleveDheureComponent } from './update-releve-dheure/update-releve-dheure.component';
import { ReleveDheureComponent } from './releve-dheure/releve-dheure.component';
import { JustificatifComponent } from './justificatif/justificatif.component';
import { SalaireComponent } from './salaire/salaire.component';
import { UpdateSalaireComponent } from './update-salaire/update-salaire.component';
import { UpdateJustificatifComponent } from './update-justificatif/update-justificatif.component';
import { ModalVacationsComponent } from './modal-vacations/modal-vacations.component';
import { ModalRenumerationComponent } from './modal-renumeration/modal-renumeration.component';
import { ReleveHeureModalComponent} from './releve-heure-modal/releve-heure-modal.component';



@NgModule({
  declarations: [
    RenumerationsComponent,
    SingleRenumerationComponent,
    UpdateReleveDheureComponent,
    ReleveDheureComponent,
    JustificatifComponent,
    SalaireComponent,
    UpdateSalaireComponent,
    UpdateJustificatifComponent,
    ModalVacationsComponent,
    ModalRenumerationComponent,
    ReleveHeureModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RenumerationRouting,
  ],
  entryComponents: [
    UpdateReleveDheureComponent
  ]
})
export class RenumerationsModule { }
