import {Component, Input, OnInit} from '@angular/core';
import {UpdateReleveDheureComponent} from '../update-releve-dheure/update-releve-dheure.component';
import {PldService} from '../../../../../../services/pld.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {MissionService} from '../../../../../../services/mission.service';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {UtilsService} from '../../../../../../services/utils.service';
import {ReleveHeureModalComponent} from '../releve-heure-modal/releve-heure-modal.component';
import {DatePipe} from '@angular/common';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {PaieService} from '../../../../../../services/recruteur/paie.service';
import {element} from 'protractor';
import {JustificatifService} from '../../../../../../services/interim/justificatif.service';
import {ModalPldContratComponent} from '../../missions-interimaires/missions-interimaires/modal-pld-contrat/modal-pld-contrat.component';
import {toNumber} from 'ngx-bootstrap/timepicker/timepicker.utils';

@Component({
  selector: 'app-releve-dheure',
  templateUrl: './releve-dheure.component.html',
  styleUrls: ['./releve-dheure.component.css']
})
export class ReleveDheureComponent implements OnInit {
  erreur: boolean;
  remuneration = 0;
  nbrheure = 0;
  data: any;
  fraisexiste = false;
  contratPld: any;
  id: any;
  formPrestations: FormGroup;
  TabPrimes: [];
  primesPldPresta: [];
  primesPldFrais = [];
  TabJours = [];
  TabHeures = [];
  rhData: any;
  rhArray = ['PRESTATIONS', 'FRAIS'];
  dialogRef: MatDialogRef<UpdateReleveDheureComponent>;
  dialogRefTmRH: MatDialogRef<ReleveHeureModalComponent>;
  dialogRefContrat: MatDialogRef<ModalPldContratComponent>;

  @Input() params: any;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private fb: FormBuilder,
    private paieService: PaieService,
    private utilsService: UtilsService,
    private activatedRoute: ActivatedRoute,
    private candidatureService: CandidatureService,
    private missionService: MissionService,
    private notificationService: NotificationService,
    private pldService: PldService,
  ) {
    this.getPrimePld();
  }

  ngOnInit() {
    this.id = this.params.id;
    this.contratPld = JSON.parse(this.params.contrat.pld);
    this.rhData = this.params.contrat.rh;
    this.remuneration = this.params.tabRenumPresta;
    this.TabPrimes = JSON.parse(this.params.contrat.pld).TabPrimes;
    this.initFormPrestations();
    this.getMontantIk();
    this.totalHeure();
    this.TabPrimes.forEach((el: any) => {
      if(el.TxtPrime.toLocaleLowerCase().includes("heures")){
        el.QuantiteImposable = this.nbrheure;
        el.QuantiteFacture = this.nbrheure;
      }
      else{
        this.distinctVaccation(this.params.candidature).forEach((item)=> {
          if(el.TxtPrime.toLocaleLowerCase().includes(item.libelle.toLocaleLowerCase())){
            el.QuantiteImposable = item.nombre;
            el.QuantiteFacture = item.nombre;
          }
        });
      }
    });

  }

  distinctVaccation(candidatures){
    const vaccationsTmp = [];
    let vaccations: any;
    candidatures.forEach((el)=> {
      vaccationsTmp.push({libelle: el.Vacation, nombre: 0});
    });
    vaccations = Array.from(
      new Set(vaccationsTmp.map(v => v.libelle))
    ).map( libelle => {
      return {
        libelle: vaccationsTmp.find(v => v.libelle === libelle).libelle,
        nombre: vaccationsTmp.find(v => v.libelle === libelle).nombre
    }});

    candidatures.forEach((el)=> {
      vaccations.forEach((item) => {
        if(el.Vacation == item.libelle ){
          item.nombre += 1;
        }
      });
    });
    return vaccations;
  }
  OnUpdateRh(donnees) {
    this.dialogRef = this.dialog.open(UpdateReleveDheureComponent, {
      width: '30%',
      height: '50%',
      panelClass: 'myapp-no-padding-dialog',
      data: donnees
    });
  }

  tmpRHModal() {
    const donnees = {items: this.data, TabPrimes: this.TabPrimes};
    this.dialogRefTmRH = this.dialog.open(ReleveHeureModalComponent, {
      width: '60%',
      height: '60%',
      panelClass: 'myapp-no-padding-dialog',
      data: donnees
    });
  }
  initFormPrestations() {
    this.formPrestations = this.fb.group(
      {
        IFMICP: [3,  Validators.required],
      });
  }
  getPrimePld() {
    this.pldService.listPrime(1).subscribe((rep) => {
      this.primesPldPresta = rep;
      this.remunerationPresta();

    });
  }
  calculateDiffHeure(date1, date2) {
    date1 = new Date(date1.date);
    date2 = new Date(date2.date);
    return Math.abs(Math.floor((date2.getTime() - date1.getTime()) / 1000 / 60 / 60 ));
  }
  totalHeure(){
    this.params.candidature.forEach(el=> {
      const nh = this.calculateDiffHeure(el.Debut, el.Fin) == 0 ? 24 : this.calculateDiffHeure(el.Debut, el.Fin);
      this.nbrheure += nh;
      const jr = new Date(el.Date.date).getDay();
      const vacation = el.Vacation.toLowerCase();
      const tabjours = {
        NumeroJour: jr == 0 ? 7 : jr ,
        NomJour: '',
        DateJour: el.Date.date,
        NbHeureJour: vacation.includes('journée + astreinte') ? Math.abs(nh/2) :  vacation.includes('journée') ? nh : vacation.includes('24') ? 12 : 0,
        NbHeureNuit: vacation.includes('journée + astreinte') ? Math.abs(nh/2) :  vacation.includes('nuit') ? nh : vacation.includes('24') ? 12 : vacation.includes('astreinte') ? nh : 0,
        Chantier: "",
        IdMotifAbsence: "",
        TxtMotifAbsence: "",
      };
      this.TabJours.push(tabjours);
    });
    const tabheure =  {
      NumeroHeure: this.params.candidature.length,
      TxtHeure: "Hrs travaillées",
      NbHeurePaye: this.nbrheure,
      TauxPaye: 0,
      NbHeureFacture: this.nbrheure,
      TauxFacture: 0
    };
    this.TabHeures.push(tabheure);
  }
  remunerationPresta() {
    const response = [];
    let keys: any;
    keys = Object.keys(this.remuneration);
    const keysPrimeContrat = Object.keys(this.TabPrimes);
    keysPrimeContrat.forEach((element) => {
      const Primes = {
        IdPrime: this.TabPrimes[element].IdPrime,
        TxtPrime: this.TabPrimes[element].TxtPrime,
        LibPrimeComplement: '',
        MethodeCalcul: '',
        QuantiteImposable: 0,
        TauxPayeImposable: this.TabPrimes[element].TauxPayeImposable,
        QuantiteNonImposable: 0,
        TauxPayeNonImposable: 0,
        QuantiteFacture: 0,
        TauxFacture: this.TabPrimes[element].TauxFacture
      };
      this.primesPldFrais.push(Primes);
    });
    keys.forEach((element) => {
      if (Number(this.remuneration[element]) && element !== 'id' && element !== 'statut') {
        response[this.paieService.formaterItem(element)] = this.remuneration[element];
        this.fraisexiste = true;
        this.getPrimePldByPrimeFrais({libelle: this.paieService.formaterItem(element), value: this.remuneration[element]});
      }
    });
    // this.initForm(this.deplacement, this.hebergement, this.repas);
    return response;
  }
  getPrimePldByPrimeFrais(item) {
    const keys = Object.keys(this.primesPldPresta);
    keys.forEach((element) => {
      if (this.primesPldPresta[element].Designation.toLowerCase().includes(item.libelle.toLowerCase())) {
        const Primes = {
          rh: 'f',
          IdPrime: this.primesPldPresta[element].IdPrime,
          TxtPrime: this.primesPldPresta[element].Designation,
          LibPrimeComplement: '',
          MethodeCalcul: '',
          QuantiteNonImposable: 1,
          TauxPayeNonImposable: item.value,
          QuantiteFacture: 1,
          TauxFacture: item.value
        };
        // this.rh.push('FRAIS');
        this.primesPldFrais.push(Primes);
      }
    });
  }
  removeDuplicates(items) {
    const unique = [];
    items.forEach(function(item, index) {
      if (index === 0) {
        unique.push(item);
      } else {
        if (!unique.includes(item)) {
          unique.push(item);
        }
      }
    });
    return unique;
  }
  onSave() {
    const tabRH = [];
    this.rhArray.forEach(el => {
      const releveHeure = {
        NumeroContrat: this.contratPld.IdContrat,
        IdClient: this.contratPld.IdClient,
        IdInterimaire: this.contratPld.IdInterimaire,
        IdSalarie: this.contratPld.IdSalarie,
        DateDebut: this.contratPld.DateDebut,
        DateFin: this.contratPld.DateFin,
        Reference: 'PRESTATIONS',
        IFMICP: this.formPrestations.value.IFMICP,
        FinDeMission: 'O',
        MontantHeurePaye: 0,
        MontantImposablePaye: 0,
        MontantNonImposablePaye: 0,
        MontantFacture: 0,
        TabJours: [],
        TabHeures: [],
        TabPrimes: []
      };
      if(el === 'PRESTATIONS'){
        releveHeure.TabPrimes = this.TabPrimes;
        releveHeure.TabHeures = this.TabHeures;
        releveHeure.TabJours = this.TabJours;
      }
      if(el === 'FRAIS'){
        releveHeure.TabPrimes = [];
        releveHeure.Reference = 'FRAIS';
        releveHeure.IFMICP = '-1';
        releveHeure.FinDeMission = 'N';
        releveHeure.TabPrimes = this.primesPldFrais;
      }
      tabRH.push(releveHeure);

    });
    this.notificationService.onConfirm('Voulez-vous générer le(s) relevé(s) d\'heure(s) ?');
    this.notificationService.dialogRef.afterClosed().subscribe(
      (x) => {
        if (x) {
          this.notificationService.blockUiStart();
          this.pldService.creerRH(tabRH).subscribe((resp: any) => {
              if (resp.body.success) {
                this.notificationService.blockUiStop();
                this.notificationService.showNotificationSuccess(resp.body.success);
                this.utilsService.redirectEspace({espace: 'renumerations', id: this.params.id});
              } else {
                this.notificationService.showNotificationSuccess(resp.body.erreur);
                this.notificationService.blockUiStop();
              }
              this.dialog.closeAll();
            },
            error => {
              this.notificationService.blockUiStop();
            });
        }
      });
  }
  onPrimesFraisExistePasPld() {
    const response = [];
    const keysRemuneration = Object.keys(this.remuneration);
    const keysPrime = Object.keys(this.primesPldFrais);
    const primesFrais = [];
    keysPrime.forEach((item) => {
      primesFrais.push(this.primesPldFrais[item].TxtPrime.toLowerCase());
    });
    keysRemuneration.forEach((el) => {
      if (Number(this.remuneration[el]) && el !== 'id' && el !== 'statut' && el !== 'montantIk') {
        if (!primesFrais.includes((this.paieService.formaterItem(el) + ' 0000000').toLowerCase())) {
          response.push(this.paieService.formaterItem(el).toLowerCase() + ' 0000000');
        }
      }
    });
    return response;
  }
  primeFraisChanged(form: NgForm, libelle) {
    const keysPrime = Object.keys(this.primesPldFrais);
    keysPrime.forEach((item) => {
      if (this.primesPldFrais[item].TxtPrime.toLowerCase() === libelle.toLowerCase()) {
        const keysForm = Object.keys(form.value);
        keysForm.forEach((el) => {
          this.primesPldFrais[item][el] = +form.value[el];
        });
      }
    });
  }
  onGenererRh(id) {
    this.notificationService.blockUiStart();
    this.pldService.genereRH(id).subscribe((resultBlob) => {
      const blob = new Blob([resultBlob], { type: 'application/pdf'} );
      const url = window.URL.createObjectURL(blob);
      window.open(url);
      this.notificationService.blockUiStop();
    },
      error => {
        this.notificationService.blockUiStop();
      });
  }
  getMontantIk() {
    let montantIk = 0;
    const km = +this.params.justificatif.km;
    const trajet = +this.params.justificatif.trajet;
    const ik = +this.params.justificatif.ik;
    montantIk = ((km * 2) * (trajet)) * (ik);
    if (montantIk > 0 || this.fraisexiste) {
      if(montantIk > 0){
        const Primes = {
          rh: 'f',
          IdPrime: 'IK',
          TxtPrime: 'Indemnités Kilométriques',
          LibPrimeComplement: '',
          MethodeCalcul: '',
          QuantiteNonImposable: (km * 2) * trajet,
          TauxPayeNonImposable: ik,
          QuantiteFacture: (km * 2) * trajet,
          TauxFacture: ik
        };
        this.primesPldFrais.push(Primes);
      }
    }
  }
  formatageDate(date) {
    return date.slice(6, 8) + '/' + date.slice(4, 6) + '/' + date.slice(0, 4);
  }
  modalContrat() {
    const items = {
      idMission: this.params.idMM,
      refMM: this.params.ref,
      idClient: this.params.idClient,
      idInterim: this.params.idInterimaire,
      interim: this.contratPld.NomPrenom,
      client: this.contratPld.RaisonSociale,
      specialite: this.params.specialite
    };

    this.dialogRefContrat = this.dialog.open(ModalPldContratComponent, {
      panelClass: 'myapp-no-padding-dialog',
      data: items,
      width: '70%',
      height: '70%'
    });
  }
  miseAJourRHFrais(rhFrais){
    const primesPldFrais =  rhFrais.TabPrimes;
    this.notificationService.onConfirm('Voulez-vous mettre à jour ce RH ?');
    this.notificationService.dialogRef.afterClosed().subscribe(
      (x) => {
        if (x) {
          this.notificationService.blockUiStart();
          if(rhFrais == null){
            rhFrais = {
              NumeroContrat: this.contratPld.IdContrat,
              IdClient: this.contratPld.IdClient,
              IdInterimaire: this.contratPld.IdInterimaire,
              IdSalarie: this.contratPld.IdSalarie,
              DateDebut: this.contratPld.DateDebut,
              DateFin: this.contratPld.DateFin,
              Reference: 'FRAIS',
              IFMICP: '-1',
              FinDeMission: 'N',
              MontantHeurePaye: 0,
              MontantImposablePaye: 0,
              MontantNonImposablePaye: 0,
              MontantFacture: 0,
              TabJours: [],
              TabHeures: [],
              TabPrimes: this.primesPldFrais,
            };
          }
          else{
            rhFrais.NumeroContrat = this.contratPld.IdContrat;
            rhFrais.TabPrimes =  this.primesPldFrais;
          }
          this.pldService.creerRH([rhFrais]).subscribe((resp: any) => {
              if (resp.body.success) {
                this.notificationService.blockUiStop();
                this.notificationService.showNotificationSuccess(resp.body.success);
                this.utilsService.redirectEspace({espace: 'renumerations', id: this.params.id});
              } else {
                this.notificationService.showNotificationSuccess(resp.body.erreur);
                this.notificationService.blockUiStop();
              }
              this.dialog.closeAll();
            },
            error => {
            console.log(error);
              this.notificationService.showNotificationEchec(error.error.message);
              rhFrais.TabPrimes =  primesPldFrais;
              this.notificationService.blockUiStop();
            });
        }
      });
  }
}
