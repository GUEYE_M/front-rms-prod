import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleveDheureComponent } from './releve-dheure.component';

describe('ReleveDheureComponent', () => {
  let component: ReleveDheureComponent;
  let fixture: ComponentFixture<ReleveDheureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleveDheureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleveDheureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
