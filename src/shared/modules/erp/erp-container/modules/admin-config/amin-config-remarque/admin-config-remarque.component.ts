import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef, PageEvent, MatTableDataSource, MatSort, MatDialog, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {RemarqueService} from 'src/shared/services/erp/remarque.service';
import {NotificationService} from 'src/shared/services/notification.service';
import {AdminConfigEditRemarqueComponent} from './admin-config-edit-remarque/admin-config-edit-remarque.component';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {FilterService} from '../../../../../../services/filter.service';

@Component({
  selector: 'app-admin-config-remarque',
  templateUrl: './admin-config-remarque.component.html',
  styleUrls: ['./admin-config-remarque.component.css']
})
export class AdminConfigRemarqueComponent implements OnInit {
  formFilter: FormGroup;
  dialogRef: MatDialogRef<AdminConfigEditRemarqueComponent>;
  list_type: any[];
  dataSourceInitial = [];
  action: string;
  displayedColumns = ['number', 'designation', 'type', 'Edit'];
  displayedColumnsFilter = [
    {
      name: 'designation',
      label: 'Designatione'
    },
    {
      name: 'type',
      label: 'Type'
    }
  ];
  elements = [];
  data = {

    filter: null,
  };


  pageEvent: PageEvent;
  pageSize = 12;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];

  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;


  constructor(private dialog: MatDialog,
              private remarqueService: RemarqueService,
              private filterService: FilterService,
              private fb: FormBuilder,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.initData();
      }
    }
    this.initData();
  }

  editRessource(id: number = null): void {
    this.dialogRef = this.dialog.open(AdminConfigEditRemarqueComponent, {
      width: '750px',
      height: '450',
      panelClass: 'myapp-no-padding-dialog',
      data: id
    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {
          this.initData();
        }

      }
    );


  }

  initData() {
    this.notificationService.blockUiStart();
    this.remarqueService.list(this.data).subscribe(
      (response: any) => {
        if (!response.erreur) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
          this.notificationService.blockUiStop();
        }
      },
      (error) => {
        this.notificationService.blockUiStop();
      }
    );
  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }

  // fonction qui envoie le formulaire de filtre en base de donnee
  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    //(form.value)
    this.initData();
  }


}
