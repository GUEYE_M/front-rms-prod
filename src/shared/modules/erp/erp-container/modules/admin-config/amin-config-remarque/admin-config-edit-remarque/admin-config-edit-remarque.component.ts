import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'src/shared/services/notification.service';
import { RemarqueService } from 'src/shared/services/erp/remarque.service';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AdminConfigRessourceEditComponent } from '../../admin-config-ressource/admin-config-ressource-edit/admin-config-ressource-edit.component';

@Component({
	selector: 'app-admin-config-edit-remarque',
	templateUrl: './admin-config-edit-remarque.component.html',
	styleUrls: [ './admin-config-edit-remarque.component.css' ]
})
export class AdminConfigEditRemarqueComponent implements OnInit {
	editForm: FormGroup;

	displayName: string;
	edit: boolean = false;

	type = {
		client: true,
		interim: false
	};

	constructor(
		private fb: FormBuilder,
		private notificationService: NotificationService,
		private remarqueService: RemarqueService,
		private router: Router,
		private dialog: MatDialog,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public dialogRef: MatDialogRef<AdminConfigRessourceEditComponent>
	) {
		if (this.data) {
			this.displayName = 'Mis a jour remarque';
			this.notificationService.blockUiStart();

			this.remarqueService.find(+this.data).subscribe((response: any) => {
				this.initform(response.data);
				this.notificationService.blockUiStop();
				this.edit = true;

				this.editForm.patchValue({
					id: this.data
				});
			});
		} else {
			this.displayName = 'Nouvelle Remarque';
			this.initform();
		}
	}

	ngOnInit() {}

	initform(
		remarque = {
			designation: '',
			type: false
		}
	) {
		this.editForm = this.fb.group({
			designation: [ remarque.designation, Validators.required ],
			type: [ remarque.type ],

			id: []
		});
	}

	compareWithFunc(a, b) {
		return a === b;
	}

	save() {
		this.notificationService.blockUiStart();
		this.remarqueService.edit(this.editForm.value).subscribe(
			(response: any) => {
				this.notificationService.blockUiStop();

				if (response.erreur) {
					this.notificationService.showNotificationEchec(response.erreur);
				} else {
					this.dialogRef.close(true);

					this.notificationService.showNotificationSuccess(response.success);
				}
			},
			(erreur) => {
				this.notificationService.showNotificationEchec(
					'erreur  de connexion , contacter le service IT si le probleme persiste'
				);
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}
}
