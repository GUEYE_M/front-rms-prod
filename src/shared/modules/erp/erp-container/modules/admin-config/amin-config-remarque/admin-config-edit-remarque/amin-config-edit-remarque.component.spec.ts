import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminConfigEditRemarqueComponent } from './admin-config-edit-remarque.component';



describe('AminConfigEditRemarqueComponent', () => {
  let component: AdminConfigEditRemarqueComponent;
  let fixture: ComponentFixture<AdminConfigEditRemarqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminConfigEditRemarqueComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigEditRemarqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
