import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminConfigRemarqueComponent } from './admin-config-remarque.component';



describe('AminConfigRemarqueComponent', () => {
  let component: AdminConfigRemarqueComponent;
  let fixture: ComponentFixture<AdminConfigRemarqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminConfigRemarqueComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigRemarqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
