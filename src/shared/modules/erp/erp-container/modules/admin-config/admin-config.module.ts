import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';
import { AdminConfigRouting } from './admin-config.routing';
import { AdminConfigComponent } from './admin-config/admin-config.component';
import { ConfigSpecialiteListeComponent } from './config-specialite-wrapper/config-specialite-liste/config-specialite-liste.component';
import { ConfigTypeProspectionListeComponent } from './config-type-prospection-wrapper/config-type-prospection-liste/config-type-prospection-liste.component';
import { ConfigTypeProspectionEditComponent } from './config-type-prospection-wrapper/config-type-prospection-edit/config-type-prospection-edit.component';
import { ConfigTypeTacheListesComponent } from './typeTaches/config-type-tache-listes/config-type-tache-listes.component';
import { ConfigTypeTacheEditComponent } from './typeTaches/config-type-tache-edit/config-type-tache-edit.component';
import { ConfigTypeVacationsListeComponent } from './admin-config-type-vacation/config-type-vacations-liste/config-type-vacations-liste.component';
import { ConfigTypeVacationsEditComponent } from './admin-config-type-vacation/config-type-vacations-edit/config-type-vacations-edit.component';
import { AdminConfigTypeLignesListeComponent } from './admin-config-type-lignes/admin-config-type-lignes-liste/admin-config-type-lignes-liste.component';
import { AdminConfigTypeLignesEditComponent } from './admin-config-type-lignes/admin-config-type-lignes-edit/admin-config-type-lignes-edit.component';
import { AdminConfigNosLignesEditComponent } from './admin-config-ns-lignes/admin-config-nos-lignes-edit/admin-config-nos-lignes-edit.component';
import { AdminConfigNosLignesListComponent } from './admin-config-ns-lignes/admin-config-nos-lignes-list/admin-config-nos-lignes-list.component';
import { ConfigAdminNosBoitesMailsListeComponent } from './config-nos-boites-mails/config-admin-nos-boites-mails-liste/config-admin-nos-boites-mails-liste.component';
import { ConfigAdminNosBoitesMailsEditComponent } from './config-nos-boites-mails/config-admin-nos-boites-mails-edit/config-admin-nos-boites-mails-edit.component';
import { ConfigAdminNosCampagneEditComponent } from './config-campagnes/config-admin-nos-campagne-edit/config-admin-nos-campagne-edit.component';
import { ConfigAdminNosCampagneListeComponent } from './config-campagnes/config-admin-nos-campagne-liste/config-admin-nos-campagne-liste.component';
import { AdminConfigClientMajorationComponent } from './admin-config-client-majoration/admin-config-client-majoration.component';
import { AdminConfigClientMajorationEditComponent } from './admin-config-client-majoration/admin-config-client-majoration-edit/admin-config-client-majoration-edit.component';
import { AdminConfigHebergementComponent } from './admin-config-hebergement/admin-config-hebergement.component';
import { ConfigAddSpecialiteComponent } from './config-specialite-wrapper/config-add-specialite/config-add-specialite.component';
import { AdminConfigRessourceComponent } from './admin-config-ressource/admin-config-ressource.component';
import { AdminConfigRessourceEditComponent } from './admin-config-ressource/admin-config-ressource-edit/admin-config-ressource-edit.component';
import { AdminConfigRemarqueComponent } from './amin-config-remarque/admin-config-remarque.component';
import { AdminConfigEditRemarqueComponent } from './amin-config-remarque/admin-config-edit-remarque/admin-config-edit-remarque.component';





@NgModule({
  declarations: [
    AdminConfigComponent,
    ConfigSpecialiteListeComponent,
    ConfigTypeProspectionListeComponent,
    ConfigTypeProspectionEditComponent,
    ConfigTypeTacheListesComponent,
    ConfigTypeTacheEditComponent,
    ConfigTypeVacationsListeComponent,
    ConfigTypeVacationsEditComponent,
    AdminConfigTypeLignesListeComponent,
    AdminConfigTypeLignesEditComponent,
    AdminConfigNosLignesEditComponent,
    AdminConfigNosLignesListComponent,
    ConfigAdminNosBoitesMailsListeComponent,
    ConfigAdminNosBoitesMailsEditComponent,
    ConfigAdminNosCampagneEditComponent,
    ConfigAdminNosCampagneListeComponent,
    AdminConfigClientMajorationComponent,
    AdminConfigClientMajorationEditComponent,
    AdminConfigHebergementComponent,
    ConfigAddSpecialiteComponent,
    AdminConfigRessourceComponent,
    AdminConfigRessourceEditComponent,
    AdminConfigRemarqueComponent,
    AdminConfigEditRemarqueComponent

  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminConfigRouting
  ], schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ], entryComponents: [
    ConfigAddSpecialiteComponent
  ]
})


export class AdminConfigModule { }
