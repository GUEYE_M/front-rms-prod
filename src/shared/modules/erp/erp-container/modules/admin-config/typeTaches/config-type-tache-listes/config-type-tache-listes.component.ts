import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {FormGroup, NgForm} from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import {TacheService} from '../../../../../../../services/erp/tache.service';
import {UtilsService} from '../../../../../../../services/utils.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {ConfigTypeTacheEditComponent} from '../config-type-tache-edit/config-type-tache-edit.component';

@Component({
  selector: 'app-config-type-tache-listes',
  templateUrl: './config-type-tache-listes.component.html',
  styleUrls: ['./config-type-tache-listes.component.css']
})
export class ConfigTypeTacheListesComponent implements OnInit, AfterViewInit {

  dataSourceInitial = [];
  dialogRef: MatDialogRef<ConfigTypeTacheEditComponent>;
  showLoadClient: boolean;
  actionInput: Boolean = false;
  action: string;
  public form: FormGroup;
  displayedColumns = ['select', 'number', 'Designation', 'tache', 'Edit'];
  displayedColumnsFilter = [
    {
      name: 'nom',
      label: 'Nom'
    }
  ];

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null,
    interim: true
  };
  pageEvent: PageEvent;
  pageSize = 12;
  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;


  constructor(private tacheService: TacheService,
              private dialog: MatDialog,
              private utilsService: UtilsService,
              private notificationService: NotificationService
  ) {

  }

  ngOnInit() {
    this.utilsService.onRevientEnHaut();

    this.tacheService.closemodaletype.subscribe(
      (response: boolean) => {
        if (response) {
          this.dialogRef.close();
        }
      }
    );
    this.initData();
  }
  editTypeTacheModal(id: number = null): void {
    this.dialogRef = this.dialog.open(ConfigTypeTacheEditComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (re)=>{
        if(re){
            this.getNewData()
        }
      }
    )
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  initData() {
    this.notificationService.blockUiStart();
    this.tacheService.listTypeTache().subscribe(
      (response: any) => {
        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
          this.notificationService.blockUiStop();
        }
      }
    );
  }

  valutElements() {
    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
    }
  }

  // fonction qui envoie le formulaire de filtre en base de donnee
  onSubmitFiltre(form: NgForm) {
    this.data.filter = form.value;
    this.getNewData();
  }

  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }

  getNewData() {
    this.notificationService.blockUiStart();
    this.tacheService.listTypeTache(this.data).subscribe(
      (response: any) => {
        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
        }
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur !! Veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }

}
