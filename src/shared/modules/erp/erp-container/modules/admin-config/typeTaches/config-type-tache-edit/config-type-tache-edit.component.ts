import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../../services/notification.service';
import {TacheService} from '../../../../../../../services/erp/tache.service';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-config-type-tache-edit',
  templateUrl: './config-type-tache-edit.component.html',
  styleUrls: ['./config-type-tache-edit.component.css']
})
export class ConfigTypeTacheEditComponent implements OnInit {
  editForm: FormGroup;
  displayName: string;
  edit: boolean = false;

  constructor(private fb: FormBuilder,
              private notificationService: NotificationService,
              private tacheService: TacheService,
              private router: Router,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<ConfigTypeTacheEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (this.data) {
      this.displayName = 'Mis a jour Type Tache';
      this.notificationService.blockUiStart();
      this.tacheService.findTypeById(+this.data).subscribe
      (
        (response: any) => {
          this.initform(response.data);
          this.notificationService.blockUiStop();
          this.edit = true;
          this.editForm.patchValue(
            {
              type: this.data
            }
          );
        }
      );
    } else {
      this.displayName = 'Nouveau Type Tache';
      this.initform();
    }
  }

  ngOnInit() {
  }

  initform(type = {
    designation: '',
  }) {
    this.editForm = this.fb.group(
      {
        designation: [type.designation, Validators.required],
        type: []
      }
    );
  }

  save() {
    if (this.editForm.valid) {
      this.notificationService.blockUiStart();
      this.tacheService.editType(this.editForm.value).subscribe(
        (response: any) => {
          if (response.erreur) {
            this.tacheService.closemodaletype.next(false);

            this.notificationService.showNotificationEchec(response.erreur);
            this.notificationService.blockUiStop();
          } else {
            this.notificationService.showNotificationSuccess(response.success)
            this.dialogRef.close(true)
          }
        },
        (erreur) => {
          this.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
        },
        () => {
          this.notificationService.blockUiStop();
        }
      );
    } else {
      this.notificationService.showNotificationEchec('le champ designation est obligatoire');
    }
  }
}
