import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypeTacheEditComponent } from './config-type-tache-edit.component';

describe('ConfigTypeTacheEditComponent', () => {
  let component: ConfigTypeTacheEditComponent;
  let fixture: ComponentFixture<ConfigTypeTacheEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTypeTacheEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypeTacheEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
