import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypeTacheListesComponent } from './config-type-tache-listes.component';

describe('ConfigTypeTacheListesComponent', () => {
  let component: ConfigTypeTacheListesComponent;
  let fixture: ComponentFixture<ConfigTypeTacheListesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTypeTacheListesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypeTacheListesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
