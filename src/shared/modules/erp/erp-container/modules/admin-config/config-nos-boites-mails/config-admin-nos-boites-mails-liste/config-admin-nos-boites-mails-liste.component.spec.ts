import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigAdminNosBoitesMailsListeComponent } from './config-admin-nos-boites-mails-liste.component';

describe('ConfigAdminNosBoitesMailsListeComponent', () => {
  let component: ConfigAdminNosBoitesMailsListeComponent;
  let fixture: ComponentFixture<ConfigAdminNosBoitesMailsListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAdminNosBoitesMailsListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigAdminNosBoitesMailsListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
