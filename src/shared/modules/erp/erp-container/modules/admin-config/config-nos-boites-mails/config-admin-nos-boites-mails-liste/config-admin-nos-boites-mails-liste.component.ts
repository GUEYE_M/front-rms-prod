import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {ConfigAdminNosBoitesMailsEditComponent} from '../config-admin-nos-boites-mails-edit/config-admin-nos-boites-mails-edit.component';
import {NotificationService} from '../../../../../../../services/notification.service';
import {UtilsService} from '../../../../../../../services/utils.service';
import {BoiteEmailService} from '../../../../../../../services/erp/boite-email.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import {FilterService} from '../../../../../../../services/filter.service';

@Component({
  selector: 'app-config-admin-nos-boites-mails-liste',
  templateUrl: './config-admin-nos-boites-mails-liste.component.html',
  styleUrls: ['./config-admin-nos-boites-mails-liste.component.css']
})
export class ConfigAdminNosBoitesMailsListeComponent implements OnInit {
  formFilter: FormGroup;
  dialogRef: MatDialogRef<ConfigAdminNosBoitesMailsEditComponent>;
  list_email: any[];

  action: string;
  filterChoixCheckboxStorage: any;
  filterValuesStorage: any;
  filterchoixCheckBox = [];
  public form: FormGroup;

  displayedColumns = ['select', 'number', 'Adresse E-mail', 'Nom', 'Description', 'Edit'];

  filterValues = {
    designation: '',
  };
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  displayedColumnsFilter = [
    {
      name: 'nom',
      label: 'Nom'
    },
    {
      name: 'email',
      label: 'Adresse E-mail'
    }
  ];
  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,

  };
  pageEvent: PageEvent;
  pageSize = 12;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];

  constructor(
    private dialog: MatDialog,
    private boiteEmailService: BoiteEmailService,
    private utilsService: UtilsService,
    private filterService: FilterService,
    private fb: FormBuilder,
    private notificationService: NotificationService
  ) {

  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    } else {
      this.initData();
    }
    this.utilsService.onRevientEnHaut();
    this.utilsService.onRevientEnHaut();
    this.boiteEmailService.closemodale.subscribe(
      (response: boolean) => {
        if (response) {
          this.dialogRef.close();
        }
      }
    );
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  editEmailModal(id: number = null): void {

    this.dialogRef = this.dialog.open(ConfigAdminNosBoitesMailsEditComponent, {
      width: '700px',
      height: '320px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });
    this.dialogRef.afterClosed().subscribe(
      (r) => {
        if (r) {

          this.getNewData();
        }
      }
    );
  }

  initData() {
    this.notificationService.blockUiStart();
    this.boiteEmailService.list().subscribe(
      (response: any) => {
        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
        }
      },
      () => {
        this.notificationService.showNotificationEchec('erreur chargement de donnée!! veillez ressayer');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }

  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();
  }

  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }

  getNewData() {
    this.notificationService.blockUiStart();
    this.boiteEmailService.list(this.data).subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.length = response.lenght;
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur !! Veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }


}
