import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {NotificationService} from "../../../../../../../services/notification.service";
import {BoiteEmailService} from "../../../../../../../services/erp/boite-email.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-config-admin-nos-boites-mails-edit',
  templateUrl: './config-admin-nos-boites-mails-edit.component.html',
  styleUrls: ['./config-admin-nos-boites-mails-edit.component.css']
})
export class ConfigAdminNosBoitesMailsEditComponent implements OnInit {

  editForm: FormGroup;
  displayName: string;
  edit: boolean = false;

  constructor(private fb: FormBuilder, private notificationService: NotificationService, private router: Router, private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any, private boiteEmailService: BoiteEmailService,private dialogRef: MatDialogRef<ConfigAdminNosBoitesMailsEditComponent>
  ) {
    if (this.data) {
      this.displayName = 'Mis a jour boite e-mail';
      this.notificationService.blockUiStart();

      this.boiteEmailService.find(+this.data).subscribe
      (
        (response: any) => {


          this.initform(response.data);
          this.notificationService.blockUiStop();
          this.edit = true;


        }
      );
    } else {
      this.displayName = 'Nouvelle Boite E-mail';
      this.initform();
    }
  }


  ngOnInit() {
  }
  initform(boite_email = {
    designation: '',
    name: '',
    description: ''
  }) {
    this.editForm = this.fb.group(
      {
        designation: [boite_email.designation, Validators.required],
        name: [boite_email.name, Validators.required],
        description: [boite_email.description],


      }
    );
  }
  save() {
    this.notificationService.blockUiStart();
    if (this.editForm.valid) {
      if (this.edit) {
        this.boiteEmailService.update(this.data, this.editForm.value).subscribe(
          (response: any) => {
            if (response.erreur) {
              this.boiteEmailService.closemodale.next(false);

              this.notificationService.showNotificationEchec(response.erreur);
            } else {
              this.notificationService.showNotificationSuccess(response.success)
              this.dialogRef.close(true)
            }
          },
          (erreur) => {
            this.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
            this.notificationService.blockUiStop();
          },
          () => {
            this.notificationService.blockUiStop();
          }
        );
      } else {
        this.boiteEmailService.create(this.editForm.value).subscribe(
          (response: any) => {

            if (response.erreur) {
              this.boiteEmailService.closemodale.next(false);

              this.notificationService.showNotificationEchec(response.erreur);
              this.notificationService.blockUiStop();
            } else {

              this.notificationService.showNotificationSuccess(response.success)
               this.dialogRef.close(true)


            

            }
          },
          () => {

          },
          () => {
            this.notificationService.blockUiStop();

          }
        );

      }
    } else {
      this.notificationService.showNotificationEchec('le champ E-mail est invalide');
      this.notificationService.blockUiStop();
    }
  }

}
