import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigAdminNosBoitesMailsEditComponent } from './config-admin-nos-boites-mails-edit.component';

describe('ConfigAdminNosBoitesMailsEditComponent', () => {
  let component: ConfigAdminNosBoitesMailsEditComponent;
  let fixture: ComponentFixture<ConfigAdminNosBoitesMailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAdminNosBoitesMailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigAdminNosBoitesMailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
