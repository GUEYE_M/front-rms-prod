import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Specialite} from "../../../../../../../models/Specialite.model";
import {BehaviorSubject} from "rxjs";
import {NotificationService} from "../../../../../../../services/notification.service";
import {SpecialiteService} from "../../../../../../../services/specialite.service";

@Component({
  selector: 'app-config-add-specialite',
  templateUrl: './config-add-specialite.component.html',
  styleUrls: ['./config-add-specialite.component.css']
})
export class ConfigAddSpecialiteComponent implements OnInit {
  statusAdd: any;
  listeFicheSpecialiteSubject: BehaviorSubject<any> = new BehaviorSubject([]);
  addSpecialiteForm: FormGroup;
  constructor(
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private specialiteService: SpecialiteService,
    public dialogRef: MatDialogRef<ConfigAddSpecialiteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.statusAdd = data.statusAdd;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.initSpecialiteForm();
    this.listeFicheSpecialiteSubject.next([]);
  }
  initSpecialiteForm() {
    this.addSpecialiteForm = this.fb.group({
      nomspecialite: ['',Validators.required],
      qualif: [''],
      specialite: ['',Validators.required],
      abreviation: ['', [
        Validators.required,
        Validators.maxLength(15),
        Validators.minLength(3),
        Validators.pattern('^[a-zA-Z]+$')
      ]
      ]
    });
  }

  OnaddSpecialite() {
    this.listeFicheSpecialiteSubject.next([]);
    this.onNoClick();
    this.statusAdd = null;
    // this.initSpecialiteForm();
    this.notificationService.blockUiStart();
    const specialite = new Specialite(
      null,
      this.addSpecialiteForm.get('specialite').value,
      this.addSpecialiteForm.get('nomspecialite').value,
      this.addSpecialiteForm.get('abreviation').value,
      this.addSpecialiteForm.get('qualif').value,
   
    );
    this.specialiteService.create(specialite).subscribe((response) => {
      if (!response['erreur']) {
        this.specialiteService.list().subscribe(
          (next) => {
            this.specialiteService.SpecialiteSubject.next(next);
            this.notificationService.showNotificationSuccessCopie('topCenter', response['success']);
            this.specialiteService.list().subscribe((x) => {
              this.specialiteService.SpecialiteSubject.next(x);
              this.notificationService.blockUiStop();
            });
          }
        );
      } else {
        this.specialiteService.list().subscribe((x) => {
          this.listeFicheSpecialiteSubject.next(x);
          this.notificationService.blockUiStop();
        });
      }
    });
  }
  OnAnnulerAddSpecialite() {
    this.onNoClick();
    this.statusAdd = null;
    this.initSpecialiteForm();
  }

}
