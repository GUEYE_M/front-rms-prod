import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigSpecialiteListeComponent } from './config-specialite-liste.component';

describe('ConfigSpecialiteListeComponent', () => {
  let component: ConfigSpecialiteListeComponent;
  let fixture: ComponentFixture<ConfigSpecialiteListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigSpecialiteListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigSpecialiteListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
