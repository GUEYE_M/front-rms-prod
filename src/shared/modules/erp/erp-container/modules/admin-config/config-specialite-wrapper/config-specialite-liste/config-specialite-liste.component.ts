import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormArray, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { InterimService } from '../../../../../../../services/interim/interim.service';
import { SpecialiteService } from '../../../../../../../services/specialite.service';
import { QualificationService } from '../../../../../../../services/qualification.service';
import { FilterService } from '../../../../../../../services/filter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../../../../../services/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { Specialite } from '../../../../../../../models/Specialite.model';
import { BehaviorSubject } from 'rxjs';
import { ConfigAddSpecialiteComponent } from '../config-add-specialite/config-add-specialite.component';
import $ from 'jquery';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from '../../../../../../../services/utils.service';

@Component({
	selector: 'app-config-specialite-liste',
	templateUrl: './config-specialite-liste.component.html',
	styleUrls: [ './config-specialite-liste.component.css' ]
})
export class ConfigSpecialiteListeComponent implements OnInit, AfterViewInit, OnDestroy {
	formFilter: FormGroup;
	displayedColumns = [
		'number',
		'Specialité',
		'abreviation',
		'Qualification(s)',
		'interimaires',
		'missions',
		"chiffre d'affaire",
		'Edit'
	];
	dataSource: MatTableDataSource<any[]>;
	closeResult: string;
	selection = new SelectionModel<any[]>(true, []);
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;
	listActions = [ 'Supprimer', 'Desactiver' ];
	listsource: any[] = [];
	showLoadInterim: boolean;
	colMissions = 'col-12';
	detailsQualifs: string;
	Qualifs: any;
	itemsQualifs: FormArray;
	QualificationForm: FormGroup;
	QualificationItemsForm: FormGroup;
	addSpecialiteForm: FormGroup;
	idSpecialiteModal: any;
	idQualification = 0;
	idSpecialite = 0;
	statusAdd: any;
	result: string = '';
	listeFicheSpecialiteSubject: BehaviorSubject<any> = new BehaviorSubject([]);
	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null,
		date_debut: null,
		date_fin: null,
		interim: true
	};
	displayedColumnsFilter = [];
	pageEvent: PageEvent;
	pageSize = 12;
	length: number = 0;
	pageSizeOptions: number[] = [ 12, 25, 100 ];

	constructor(
		private interimService: InterimService,
		private specialiteService: SpecialiteService,
		private qualificationService: QualificationService,
		private fb: FormBuilder,
		private filterService: FilterService,
		private utilsService: UtilsService,
		private activatedRoute: ActivatedRoute,
		private formBuilder: FormBuilder,
		private notificationService: NotificationService,
		private router: Router,
		private modalService: NgbModal,
		public dialog: MatDialog
	) {
		this.initData();
	}

	get formArr() {
		return this.QualificationForm.get('qualifications') as FormArray;
	}

	ngOnInit() {
		this.formFilter = this.fb.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
				this.getNewData();
			}
		}

		this.initQualificationForm();
		this.initSpecialiteForm();
		this.notificationService.blockUiStop();
		this.displayedColumnsFilter = this.specialiteService.displayFilterName();
	}

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	initQualificationForm() {
		this.QualificationForm = this.fb.group({
			qualifications: this.fb.array([ this.initItemRows() ])
		});
	}

	initSpecialiteForm() {
		this.addSpecialiteForm = this.fb.group({
			nomspecialite: [ '' ],
			qualif: [ '' ],
			specialite: [''],
			abreviation:['']
		});
	}

	addNewSpecialite() {
		const dialogRef = this.dialog.open(ConfigAddSpecialiteComponent, {
			width: '550px',
			height: '550px',
			panelClass: 'myapp-no-padding-dialog',
			data: { statusAdd: this.statusAdd }
		});
	}

	onAddQualif() {
		this.qualificationService
			.newQualification(this.QualificationForm.value['qualifications'])
			.subscribe((response) => {
				this.listeFicheSpecialiteSubject.next([]);
				if (!response['erreur']) {
					this.initQualificationForm();
					this.fermerModalAddQualifs();
					this.specialiteService.list().subscribe((x) => {
						this.listeFicheSpecialiteSubject.next(x);
						this.utilsService.redirectEspace({ espace: 'specialiteListe', id: null });
					});

					this.notificationService.showNotificationSuccess(response['success']);
				} else {
					this.specialiteService.list().subscribe((x) => {
						this.listeFicheSpecialiteSubject.next(x);
					});
					this.notificationService.showNotificationEchec(response['erreur']);
				}
			});
	}

	initItemRows() {
		this.QualificationItemsForm = this.fb.group({
			specialite: [ '' ],
			qualification: [ '' ]
		});
		return this.QualificationItemsForm;
	}

	addNewRow() {
		this.formArr.push(this.initItemRows());
		this.QualificationItemsForm.patchValue({ specialite: this.idSpecialiteModal });
	}

	deleteRow(index: number) {
		this.formArr.removeAt(index);
	}

	fermerModalAddQualifs() {
		const fermerModal: HTMLElement = document.getElementsByName('fermerModalAddQualifs')[0] as HTMLElement;
		fermerModal.click();
	}

	fermerModalAddAspecialite() {
		const fermerModal: HTMLElement = document.getElementsByName('fermerModalAddAspecialite')[0] as HTMLElement;
		fermerModal.click();
	}

	onDetailQualifications(specialite) {
		this.colMissions = 'd-none table table-responsive animated fadeIn';
		this.detailsQualifs = 'La liste des qualifications de la spécialité: ' + specialite.specialite;
		this.idSpecialiteModal = specialite.id;
		$('#detailQualifications').removeClass('d-none');
		this.QualificationItemsForm.patchValue({ specialite: this.idSpecialiteModal });
		this.specialiteService.getQualifsBySpecialite(specialite.id).subscribe((rep: any) => {
			this.Qualifs = rep.qualifications;
		});
		// for (let i = 0; i < this.listsource.length; i++) {
		//   if (this.listsource[i].specialite === specialite.specialite) {
		//
		//     this.Qualifs = this.listsource[i].qualifications;
		//   }
		// }
	}

	open(content) {
		this.QualificationItemsForm.patchValue({ specialite: this.idSpecialiteModal });
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			}
		);
	}

	onUpdateQualification(item) {
		this.idQualification = item.id_qualification;
	}

	annulerModifQualif() {
		this.idQualification = 0;
	}

	deleteQualification(item) {
		const thiss = this;
		this.notificationService.onConfirm('Etes vous sur de supprimer cette qualification ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				thiss.qualificationService.deleteQualification(item).subscribe(
					(response: any) => {
						if (response['erreur']) {
							thiss.notificationService.showNotificationEchec(response['erreur']);
						} else {
							thiss.notificationService.showNotificationSuccess(response['success']);
						}
					},
					(error) => {
						thiss.notificationService.blockUiStop();
					},
					() => {
						thiss.utilsService.redirectEspace({ espace: 'specialiteListe', id: null });
					}
				);
			}
		});
	}

	OnaddSpecialite() {
		this.statusAdd = null;
		this.initSpecialiteForm();
		this.specialiteService.SpecialiteSubject.next([]);
		this.notificationService.blockUiStart();
		const specialite = new Specialite(
			null,
			this.addSpecialiteForm.value['specialite'],
			this.addSpecialiteForm.value['nomspecialite'],
			this.addSpecialiteForm.value['qualif'],
			this.addSpecialiteForm.value['abreviation']
		);
		this.specialiteService.create(specialite).subscribe((response) => {
			if (!response['erreur']) {
				this.fermerModalAddAspecialite();
				this.specialiteService.list().subscribe((next) => {
					this.specialiteService.SpecialiteSubject.next(next);
					this.statusAdd = response['success'];
					this.notificationService.showNotificationSuccess(this.statusAdd);
					this.specialiteService.list().subscribe((x) => {
						this.listeFicheSpecialiteSubject.next(x);
						this.notificationService.blockUiStop();
					});
				});
			} else {
				this.specialiteService.list().subscribe((x) => {
					this.listeFicheSpecialiteSubject.next(x);
					this.notificationService.blockUiStop();
				});
				this.statusAdd = response['erreur'];
			}
		});
	}

	OnAnnulerAddSpecialite() {
		this.statusAdd = null;
		this.initSpecialiteForm();
	}

	onConfirmerUpdateSpecialite(specialite) {
	  if(specialite.abreviation.indexOf('-') === -1){
      this.specialiteService.update(specialite).subscribe((response) => {
        if (!response['erreur']) {
          this.fermerModalAddAspecialite();
          this.notificationService.showNotificationSuccess(response['success']);
        } else {
          this.notificationService.showNotificationEchec(response['erreur']);
        }
      });
      this.idSpecialite = 0;
    } else {
      this.notificationService.showNotificationEchec('L\'abreviation doit pas contenir le caractère spécial "-" !');
    }
	}

	onUpdateSpecialite(item) {
		this.idSpecialite = item.id;
	}

	annulerModifSpecialite() {
		this.idSpecialite = 0;
	}

	onConfirmerUpdateQualification(modifieItem, id) {
		this.idQualification = id;
		const qualif = { id: id, qualification: modifieItem.value };
		this.qualificationService.updateQualification(qualif).subscribe((response) => {
			if (!response['erreur']) {
				this.idQualification = 0;
				this.utilsService.redirectEspace({ espace: 'specialiteListe', id: null });
				this.notificationService.showNotificationSuccess(response['success']);
			} else {
				this.notificationService.showNotificationEchec(response['erreur']);
			}
		});
	}

	confirmDialog(specialite): void {
		const thiss = this;

		this.notificationService.onConfirm('êtes-vous sûr de vouloir continuer ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				thiss.specialiteService.delete(specialite).subscribe(
					(response: any) => {
						if (response.erreur) {
							thiss.notificationService.showNotificationEchec(response.erreur);
						} else {
							thiss.notificationService.showNotificationSuccess(response.success);
						}
					},
					(error) => {
						thiss.notificationService.blockUiStop();
					},
					() => {
						thiss.utilsService.redirectEspace({ espace: 'specialiteListe', id: null });
						thiss.notificationService.blockUiStop();
					}
				);
			}
		});
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	colspanUpdateSpecalite(): number {
		return 1;
	}

	onFermeDetailM() {
		this.colMissions = 'col-12 animated fadeIn';
		$('#detailQualifications').addClass('d-none');
	}

	ngOnDestroy() {
		this.interimService.showLoadInterim.next(true);
	}

	initData() {
		const listeSpecialites = this.activatedRoute.snapshot.data['listes_specialites'];
		this.specialiteService.SpecialiteSubject.next(listeSpecialites);
		this.specialiteService.SpecialiteSubject.subscribe((x) => {
			this.dataSource = new MatTableDataSource(x.data);
			this.length = x.lenght;
			this.listsource = x.data;
		});
	}

	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.initData();
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
		}
	}

	// fonction qui envoie le formulaire de filtre en base de donnee
	onSubmitFiltre() {
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = this.formFilter.value;
		this.getNewData();
	}

	pagination(event: any) {
		this.data.offset = event.pageIndex;
		this.data.limit = event.pageSize;
		this.getNewData();
	}

	getNewData() {
		this.notificationService.blockUiStart();
		this.specialiteService.list(this.data).subscribe(
			(response: any) => {
				this.dataSource = new MatTableDataSource(response['data']);
				this.length = response['lenght'];
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
}
