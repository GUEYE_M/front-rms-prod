import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigAddSpecialiteComponent } from './config-add-specialite.component';

describe('ConfigAddSpecialiteComponent', () => {
  let component: ConfigAddSpecialiteComponent;
  let fixture: ComponentFixture<ConfigAddSpecialiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAddSpecialiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigAddSpecialiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
