import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigRessourceEditComponent } from './admin-config-ressource-edit.component';

describe('AdminConfigRessourceEditComponent', () => {
  let component: AdminConfigRessourceEditComponent;
  let fixture: ComponentFixture<AdminConfigRessourceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigRessourceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigRessourceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
