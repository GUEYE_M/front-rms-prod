import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigRessourceComponent } from './admin-config-ressource.component';

describe('AdminConfigRessourceComponent', () => {
  let component: AdminConfigRessourceComponent;
  let fixture: ComponentFixture<AdminConfigRessourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigRessourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigRessourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
