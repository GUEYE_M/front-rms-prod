import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'src/shared/services/notification.service';
import { ClientMajorationService } from 'src/shared/services/recruteur/client-majoration.service';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RessourceService } from 'src/shared/services/erp/ressource.service';

@Component({
  selector: 'app-admin-config-ressource-edit',
  templateUrl: './admin-config-ressource-edit.component.html',
  styleUrls: ['./admin-config-ressource-edit.component.css']
})
export class AdminConfigRessourceEditComponent implements OnInit {

  editForm: FormGroup;

  displayName: string;
  edit: boolean = false;

  type = {
    client: true,
    interim: false
  }

  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private ressourceService: RessourceService,
    private router: Router, private dialog: MatDialog,

    @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<AdminConfigRessourceEditComponent>
  ) {




    if (this.data) {

      this.displayName = "Mis a jour ressource"
      this.notificationService.blockUiStart();

      this.ressourceService.find(+this.data).subscribe
        (
          (response: any) => {



            this.initform(response.data)
            this.notificationService.blockUiStop();
            this.edit = true;

            this.editForm.patchValue(
              {
                id: this.data
              }
            )





          }
        )
    }
    else {
      this.displayName = "Nouvelle Ressource";
      this.initform();
    }


  }


  ngOnInit() {






  }


  initform(ressource = {
    nom: '',
    prenom: '',
    telephone: '',
    raison_social: '',
    type: false,
    url: 'https://'




  }) {
    this.editForm = this.fb.group(
      {
        nom: [ressource.nom, Validators.required],
        prenom: [ressource.prenom],
        telephone: [ressource.telephone],
        raison_sociale: [ressource.raison_social],
        type: [ressource.type],
        url: [ressource.url],
        id: []

      }
    )

  }



  compareWithFunc(a, b) {


    return a === b;
  }

  save() {


    this.notificationService.blockUiStart();
    this.ressourceService.edit(this.editForm.value).subscribe(
      (response: any) => {
        this.notificationService.blockUiStop();

        if (response.erreur) {

          this.notificationService.showNotificationEchec(response.erreur)

        }
        else {

          this.dialogRef.close(true)

          this.notificationService.showNotificationSuccess(response.success)


        }
      },
      (erreur) => {
        this.notificationService.showNotificationEchec("erreur  de connexion , contacter le service IT si le probleme persiste")
        this.notificationService.blockUiStop();
      },
      () => {

        this.notificationService.blockUiStop();
      }
    )




  }


}
