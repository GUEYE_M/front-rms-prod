import { Route, RouterModule } from '@angular/router';
import { AdminConfigComponent } from './admin-config/admin-config.component';
import { ConfigSpecialiteListeComponent } from './config-specialite-wrapper/config-specialite-liste/config-specialite-liste.component';
import { SpecialiteListesResolver } from '../../../../../resolver/specialite_listes.resolve';
import { ConfigTypeProspectionListeComponent } from './config-type-prospection-wrapper/config-type-prospection-liste/config-type-prospection-liste.component';
import { ConfigTypeProspectionEditComponent } from './config-type-prospection-wrapper/config-type-prospection-edit/config-type-prospection-edit.component';
import { ConfigTypeTacheListesComponent } from './typeTaches/config-type-tache-listes/config-type-tache-listes.component';
import { ConfigTypeTacheEditComponent } from './typeTaches/config-type-tache-edit/config-type-tache-edit.component';
import { ConfigTypeVacationsListeComponent } from './admin-config-type-vacation/config-type-vacations-liste/config-type-vacations-liste.component';
import { ConfigTypeVacationsEditComponent } from './admin-config-type-vacation/config-type-vacations-edit/config-type-vacations-edit.component';
import { AdminConfigTypeLignesListeComponent } from './admin-config-type-lignes/admin-config-type-lignes-liste/admin-config-type-lignes-liste.component';
import { AdminConfigTypeLignesEditComponent } from './admin-config-type-lignes/admin-config-type-lignes-edit/admin-config-type-lignes-edit.component';
import { AdminConfigNosLignesListComponent } from './admin-config-ns-lignes/admin-config-nos-lignes-list/admin-config-nos-lignes-list.component';
import { AdminConfigNosLignesEditComponent } from './admin-config-ns-lignes/admin-config-nos-lignes-edit/admin-config-nos-lignes-edit.component';
import { ConfigAdminNosCampagneEditComponent } from './config-campagnes/config-admin-nos-campagne-edit/config-admin-nos-campagne-edit.component';
import { ConfigAdminNosCampagneListeComponent } from './config-campagnes/config-admin-nos-campagne-liste/config-admin-nos-campagne-liste.component';
import { AdminConfigClientMajorationComponent } from './admin-config-client-majoration/admin-config-client-majoration.component';
import { AdminConfigClientMajorationEditComponent } from './admin-config-client-majoration/admin-config-client-majoration-edit/admin-config-client-majoration-edit.component';
import { AdminConfigHebergementComponent } from './admin-config-hebergement/admin-config-hebergement.component';
import { AdminConfigHebergementEditComponent } from './admin-config-hebergement-edit/admin-config-hebergement-edit.component';
import { ConfigAdminNosBoitesMailsListeComponent } from './config-nos-boites-mails/config-admin-nos-boites-mails-liste/config-admin-nos-boites-mails-liste.component';
import { ConfigAdminNosBoitesMailsEditComponent } from './config-nos-boites-mails/config-admin-nos-boites-mails-edit/config-admin-nos-boites-mails-edit.component';
import { AdminConfigRessourceComponent } from './admin-config-ressource/admin-config-ressource.component';
import { AdminConfigRessourceEditComponent } from './admin-config-ressource/admin-config-ressource-edit/admin-config-ressource-edit.component';
import { AdminConfigRemarqueComponent } from './amin-config-remarque/admin-config-remarque.component';
import { AdminConfigEditRemarqueComponent } from './amin-config-remarque/admin-config-edit-remarque/admin-config-edit-remarque.component';

const ADMIN_CONFIG: Route[] = [
  {
    path: '',
    component: AdminConfigComponent,
    children: [{
      path: 'specialite',
      component: ConfigSpecialiteListeComponent,
      resolve: { listes_specialites: SpecialiteListesResolver },
      children: [
        {
          path: 'liste',
          component: ConfigSpecialiteListeComponent,
        }
      ]
    },
    {
      path: 'type-prospection', component: ConfigTypeProspectionListeComponent,
      children: [
        {
          path: 'liste', component: ConfigTypeProspectionListeComponent,
        }, {
          path: 'edit', component: ConfigTypeProspectionEditComponent,
        }
      ]
    }, {
      path: 'type-taches', component: ConfigTypeTacheListesComponent,
      children: [
        {
          path: 'liste', component: ConfigTypeTacheListesComponent,
        }, {
          path: 'edit', component: ConfigTypeTacheEditComponent,
        }
      ]
    }, {
      path: 'type-vacations', component: ConfigTypeVacationsListeComponent,
      children: [
        {
          path: 'liste', component: ConfigTypeVacationsListeComponent,
        }, {
          path: 'edit', component: ConfigTypeVacationsEditComponent,
        }
      ]
    }, {
      path: 'type-ligne-telephonique', component: AdminConfigTypeLignesListeComponent,
      children: [
        {
          path: 'liste', component: AdminConfigTypeLignesListeComponent,
        }, {
          path: 'edit', component: AdminConfigTypeLignesEditComponent,
        }
      ]
    }, {
      path: 'ligne-telephonique', component: AdminConfigNosLignesListComponent,
      children: [
        {
          path: 'liste', component: AdminConfigNosLignesListComponent,
        }, {
          path: 'edit', component: AdminConfigNosLignesEditComponent,
        }
      ]
    }, {
      path: 'boite-email', component: ConfigAdminNosBoitesMailsListeComponent,
      children: [
        {
          path: 'liste', component: ConfigAdminNosBoitesMailsListeComponent,
        }, {
          path: 'edit', component: ConfigAdminNosBoitesMailsEditComponent,
        }
      ]
    }, {
      path: 'nos-campagnes', component: ConfigAdminNosCampagneListeComponent,
      children: [
        {
          path: 'liste', component: ConfigAdminNosCampagneListeComponent,
        }, {
          path: 'edit', component: ConfigAdminNosCampagneEditComponent,
        }
      ]
    },

    {
      path: 'client-majoration', component: AdminConfigClientMajorationComponent,
      children: [
        {
          path: 'liste', component: AdminConfigClientMajorationComponent,
        }, {
          path: 'edit', component: AdminConfigClientMajorationEditComponent,
        }
      ]
    },
    {
      path: 'hebergement', component: AdminConfigHebergementComponent,
      children: [
        {
          path: 'liste', component: AdminConfigHebergementComponent,
        }, {
          path: 'edit', component: AdminConfigHebergementEditComponent,
        }
      ]
    },
    {
      path: 'ressource', component: AdminConfigRessourceComponent,
      children: [
        {
          path: 'liste', component: AdminConfigRessourceComponent,
        }, {
          path: 'edit', component: AdminConfigRessourceEditComponent,
        }
      ]
    },
    {
      path: 'remarque', component: AdminConfigRemarqueComponent,
      children: [
        {
          path: 'liste', component: AdminConfigRemarqueComponent,
        }, {
          path: 'edit', component: AdminConfigEditRemarqueComponent,
        }
      ]
    }
    ]
  }
];

export const AdminConfigRouting = RouterModule.forChild(ADMIN_CONFIG);



