import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigClientMajorationEditComponent } from './admin-config-client-majoration-edit.component';

describe('AdminConfigClientMajorationEditComponent', () => {
  let component: AdminConfigClientMajorationEditComponent;
  let fixture: ComponentFixture<AdminConfigClientMajorationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigClientMajorationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigClientMajorationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
