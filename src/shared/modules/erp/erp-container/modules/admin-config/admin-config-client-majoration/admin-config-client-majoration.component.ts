import {Component, OnInit, ViewChild} from '@angular/core';
import {PageEvent, MatDialogRef, MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';

import {SelectionModel} from '@angular/cdk/collections';
import {NotificationService} from 'src/shared/services/notification.service';
import {AdminConfigClientMajorationEditComponent} from './admin-config-client-majoration-edit/admin-config-client-majoration-edit.component';
import {ClientMajorationService} from 'src/shared/services/recruteur/client-majoration.service';
import {from} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {FilterService} from '../../../../../../services/filter.service';


@Component({
  selector: 'app-admin-config-client-majoration',
  templateUrl: './admin-config-client-majoration.component.html',
  styleUrls: ['./admin-config-client-majoration.component.css']
})
export class AdminConfigClientMajorationComponent implements OnInit {
  formFilter: FormGroup;
  dialogRef: MatDialogRef<AdminConfigClientMajorationEditComponent>;
  public form: FormGroup;
  displayedColumns = ['select', 'number', 'nom', 'dimanche', 'samedi', 'ferier', '24', '31', 'Edit'];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);


  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  displayedColumnsFilter = [
    {
      name: 'etablissement',
      label: 'Etablissement'
    },

  ];

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,

  };


  pageEvent: PageEvent;
  pageSize = 12;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];

  constructor(
    private clientMajorationService: ClientMajorationService,
    private dialog: MatDialog,
    private filterService: FilterService,
    private fb: FormBuilder,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getData();
      }
    } else {
      this.getData();
    }


  }

  edit(id: number = null): void {

    this.dialogRef = this.dialog.open(AdminConfigClientMajorationEditComponent, {
      width: '670px',
      height: '260px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {
          this.getData();
        }
      }
    );
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  ngAfterViewInit() {

  }


  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.getData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getData();

  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getData();


  }

  getData() {
    this.notificationService.blockUiStart();

    this.clientMajorationService.list(this.data).subscribe(
      (response: any) => {

        this.notificationService.blockUiStop();


        this.dataSource = new MatTableDataSource(response.data);


        this.length = response.lenght;


      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur !! Veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );


  }

  getbooleanString(value: boolean) {
    let v: string;
    if (value == true) {
      v = 'oui';
    } else {
      v = 'non';

    }
    return v;
  }
}
