import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/shared/services/notification.service';
import { ClientMajorationService } from 'src/shared/services/recruteur/client-majoration.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-admin-config-client-majoration-edit',
  templateUrl: './admin-config-client-majoration-edit.component.html',
  styleUrls: ['./admin-config-client-majoration-edit.component.css']
})
export class AdminConfigClientMajorationEditComponent implements OnInit {

  editForm: FormGroup;
 
  displayName : string;
  edit: boolean = false;



  constructor( private fb: FormBuilder, private notificationService: NotificationService,private MajorationService:ClientMajorationService,private router:Router,private dialog: MatDialog,
    
    @Inject(MAT_DIALOG_DATA) public data: any, public  dialogRef: MatDialogRef<AdminConfigClientMajorationEditComponent>
  ) {

   
  
   
    if(this.data)
    {
    
      this.displayName = "Mis a jour Majoration"
      this.notificationService.blockUiStart();
     
      this.MajorationService.find(+this.data).subscribe
      (
        (response:any)=>
        {
         
         
       
           this.initform(response.data)
           this.notificationService.blockUiStop();
           this.edit = true;

           this.editForm.patchValue(
             {
              id:this.data
             }
           )
           
          
       
        
         
        }
      )
    }
    else{
       this.displayName = "Nouvelle Majoration";
       this.initform();
    }
  
    
       }
   

  ngOnInit() {

   
  

    
  
}


  initform(majoration = {
    nom:'',
    dimanche: false,
    samedi:false,
    ferie:false,
    trente_un:false,
    vingt_quatre:false
    
  
  

  }) {
    this.editForm = this.fb.group(
      {
        nom: [majoration.nom,Validators.required],
        dimanche: [majoration.dimanche],
        samedi: [majoration.samedi],
        ferie: [majoration.ferie],
        trente_un:[majoration.trente_un],
        vingt_quatre:[majoration.vingt_quatre],
        id:[]
        
      }
    )
    
}





save() {
 
 
  this.notificationService.blockUiStart();
      this.MajorationService.edit(this.editForm.value).subscribe(
        (response:any)=>{
          this.notificationService.blockUiStop();
         
          if(response.erreur)
          {
        
            this.notificationService.showNotificationEchec(response.erreur)
         
        }
        else{
          
          this.dialogRef.close(true)
          
          this.notificationService.showNotificationSuccess(response.success)
         
        
        }
      },
      (erreur)=>{
        this.notificationService.showNotificationEchec("erreur  de connexion , contacter le service IT si le probleme persiste")
        this.notificationService.blockUiStop();
      },
      ()=>{
       
        this.notificationService.blockUiStop();
      }
      )
    

    
    
  }
 
}
