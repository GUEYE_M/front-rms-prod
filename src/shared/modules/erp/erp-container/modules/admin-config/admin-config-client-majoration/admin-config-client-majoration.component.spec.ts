import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigClientMajorationComponent } from './admin-config-client-majoration.component';

describe('AdminConfigClientMajorationComponent', () => {
  let component: AdminConfigClientMajorationComponent;
  let fixture: ComponentFixture<AdminConfigClientMajorationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigClientMajorationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigClientMajorationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
