import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigTypeLignesListeComponent } from './admin-config-type-lignes-liste.component';

describe('AdminConfigTypeLignesListeComponent', () => {
  let component: AdminConfigTypeLignesListeComponent;
  let fixture: ComponentFixture<AdminConfigTypeLignesListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigTypeLignesListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigTypeLignesListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
