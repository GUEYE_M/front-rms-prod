import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigTypeLignesEditComponent } from './admin-config-type-lignes-edit.component';

describe('AdminConfigTypeLignesEditComponent', () => {
  let component: AdminConfigTypeLignesEditComponent;
  let fixture: ComponentFixture<AdminConfigTypeLignesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigTypeLignesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigTypeLignesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
