import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {NotificationService} from "../../../../../../../services/notification.service";
import {LigneTelephoniqueService} from "../../../../../../../services/ligne.telephonique.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-config-type-lignes-edit',
  templateUrl: './admin-config-type-lignes-edit.component.html',
  styleUrls: ['./admin-config-type-lignes-edit.component.css']
})
export class AdminConfigTypeLignesEditComponent implements OnInit {

  editForm: FormGroup;
  displayName: string;
  edit: boolean = false;

  constructor(private fb: FormBuilder, private notificationService: NotificationService, private ligneService: LigneTelephoniqueService, private router: Router, private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<AdminConfigTypeLignesEditComponent>
  ) {
    if (this.data) {
      this.displayName = 'Mis a jour Type ligne ';
      this.notificationService.blockUiStart();

      this.ligneService.findByIdLigneType(+this.data).subscribe
      (
        (response: any) => {
          if (response.erreur) {
            this.notificationService.showNotificationEchec(response.erreur);
          } else {

            this.initform(response.data);
            this.edit = true;
            this.editForm.get('type').setValue(this.data);
            this.editForm.patchValue(
              {
                appertenance: response.data.appartenace
              }
            );
          }
          this.notificationService.blockUiStop();
        }
      );
    } else {
      this.displayName = 'Nouveau Type de Ligne Telephonique';
      this.initform();
    }
  }
  ngOnInit() {
  }
  initform(ligne = {
    designation: '',
    appertenance: '',
    description: '',
  }) {
    this.editForm = this.fb.group(
      {
        designation: [ligne.designation, Validators.required],
        description: [ligne.description],
        appertenance: [],
        type: []
      }
    );

  }
  save() {
    if (this.editForm.valid) {
      this.notificationService.blockUiStart();
      this.ligneService.editLigneType(this.editForm.value).subscribe(
        (response: any) => {
          if (response.erreur) {
            this.ligneService.closemodaletype.next(false);
            this.notificationService.showNotificationEchec(response.erreur);
            this.notificationService.blockUiStop();
          } else {
            this.notificationService.showNotificationSuccess(response.success)
            this.dialogRef.close(true)
          }
        },
        (erreur) => {
        },
        () => {

          this.notificationService.blockUiStop();
        }
      );
    } else {
      this.notificationService.showNotificationEchec('le champ nom campagne est obligatoire');
    }
  }
}
