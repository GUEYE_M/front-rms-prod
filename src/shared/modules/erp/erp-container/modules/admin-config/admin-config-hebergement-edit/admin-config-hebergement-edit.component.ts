import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Client } from 'src/shared/models/recruteur/Client.model';
import { ClientService } from 'src/shared/services/recruteur/client.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificationService } from 'src/shared/services/notification.service';
import { UserService } from 'src/shared/services/user.service';
import { Router } from '@angular/router';
import { HebergementServiceService } from 'src/shared/services/recruteur/hebergement.service.service';

@Component({
	selector: 'app-admin-config-hebergement-edit',
	templateUrl: './admin-config-hebergement-edit.component.html',
	styleUrls: [ './admin-config-hebergement-edit.component.css' ]
})
export class AdminConfigHebergementEditComponent implements OnInit {
	clientList: Client[];

	edit: boolean = false;

	gestionnaire: any;
	displayName: string;
	hebergementForm: FormGroup;
	constructor(
		private fb: FormBuilder,
		private clientService: ClientService,
		public dialogRef: MatDialogRef<AdminConfigHebergementEditComponent>,
		private hebergementService: HebergementServiceService,
		private notificationService: NotificationService,
		private userService: UserService,
		@Inject(MAT_DIALOG_DATA) public id: any
	) {
		if (this.id) {
			this.displayName = 'Mis a jour Vacation ';
			this.notificationService.blockUiStart();

			this.hebergementService.find(+this.id).subscribe((response: any) => {
				if (response.erreur) {
					this.notificationService.showNotificationEchec(response.erreur);
				} else {
					this.initform(response.data);
					this.edit = true;

					this.hebergementForm.patchValue({
						id: +this.id,
						client: response.data.fiche_client.client
					});
				}

				this.notificationService.blockUiStop();
			});
		} else {
			this.displayName = 'Nouveau Hebergement';
			this.initform();
		}
	}

	ngOnInit() {
		this.allClient();

		this.gestionnaire = this.userService.getCurrentUser();
	}
	ngOnChanges() {}

	initform(
		hebergement = {
			lieu: '',
			adresse: '',
			telephone: '',
			nombre_etoile: null,
			remarque: ''
		}
	) {
		this.hebergementForm = this.fb.group({
			id: [],
			client: [ [], Validators.required ],
			lieu: [ hebergement.lieu, Validators.required ],
			adresse: [ hebergement.adresse, Validators.required ],
			telephone: [ hebergement.telephone, Validators.required ],
			etoile: [ hebergement.nombre_etoile, Validators.required ],
			remarque: [ hebergement.remarque ],

			auteur: []
		});
	}
	compareWithFunc(a, b) {
		return a.id === b.id;
	}

	onClient() {
		this.hebergementForm.value;
	}

	allClient() {
		this.clientService.listClients(null, true).subscribe((client: any) => {
			this.clientList = client.data;
		});
	}

	save() {
		this.notificationService.blockUiStart();
		this.hebergementForm.get('auteur').setValue(this.gestionnaire.gestionnaire.id);
		this.hebergementForm.get('id').setValue(this.id);

		if (this.hebergementForm.valid) {
			this.hebergementService.update(this.hebergementForm.value).subscribe(
				(response: any) => {
					if (response.erreur) {
						this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
						this.notificationService.blockUiStop();
					} else {
						this.dialogRef.close(true);
						this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
						this.notificationService.blockUiStop();
					}
				},
				(error) => {
					this.notificationService.showNotificationEchec('erreur de connexion au server, veillez ressayer');
					this.notificationService.blockUiStop();
				},
				() => {}
			);
		} else {
			this.notificationService.showNotificationEchec(
				'formulaire invalide , veillez renseignez  les champs obligatoires'
			);
		}
	}
}
