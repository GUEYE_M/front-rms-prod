import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigHebergementEditComponent } from './admin-config-hebergement-edit.component';

describe('AdminConfigHebergementEditComponent', () => {
  let component: AdminConfigHebergementEditComponent;
  let fixture: ComponentFixture<AdminConfigHebergementEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigHebergementEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigHebergementEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
