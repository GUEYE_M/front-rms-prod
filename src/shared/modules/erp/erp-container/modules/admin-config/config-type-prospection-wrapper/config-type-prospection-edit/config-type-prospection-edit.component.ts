import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../../services/notification.service';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MessagaProspectionService} from '../../../../../../../services/messaga-prospection.service';

@Component({
  selector: 'app-config-type-prospection-edit',
  templateUrl: './config-type-prospection-edit.component.html',
  styleUrls: ['./config-type-prospection-edit.component.css']
})
export class ConfigTypeProspectionEditComponent implements OnInit {

  editTypeMessageForm: FormGroup;
  displayName: string = '';
  edit: boolean = false;

  constructor(private fb: FormBuilder, private notificationService: NotificationService, private router: Router, private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any, private prospectionService: MessagaProspectionService, private dialogRef: MatDialogRef<ConfigTypeProspectionEditComponent>,
  ) {


    if (this.data) {
      this.displayName = 'Mis a jour type message prospection';
      this.notificationService.blockUiStart();

      this.prospectionService.findTypeProspectionById(+this.data).subscribe
      (
        (response) => {

          (response.data);
          this.initform(response.data);
          this.notificationService.blockUiStop();
          this.edit = true;


        }
      )
    } else {
      this.displayName = 'Nouveau Type Message de Prospection';
      this.initform();
    }
  }


  ngOnInit() {
  }

  initform(type = {

    designation: '',


  }) {
    this.editTypeMessageForm = this.fb.group(
      {
        designation: [type.designation, Validators.required],

      }
    )

  }

// save(){
//   this.prospectionService.newTypeProspection(this.editTypeMessageForm.value);

// }
  save() {
    this.notificationService.blockUiStart();
    if (this.editTypeMessageForm.valid) {
      if (this.edit) {

        this.prospectionService.updateTypeProspection(this.data, this.editTypeMessageForm.value).subscribe(
          (response: any) => {
            if (response.erreur) {
              this.prospectionService.closemodale.next(false);

              this.notificationService.showNotificationEchec(response.erreur);
              this.notificationService.blockUiStop();
            } else {
              this.notificationService.showNotificationSuccess(response.success)
              this.dialogRef.close(true)
            }
          },
          (erreur) => {
            this.notificationService.showNotificationEchec("erreur  de connexion , contacter le service IT si le probleme persiste")
          },
          () => {
            this.notificationService.blockUiStop();
          }
        );
      } else {
        this.prospectionService.newTypeProspection(this.editTypeMessageForm.value).subscribe(
          (response: any) => {
            if (response.erreur) {
              this.prospectionService.closemodale.next(false);
              this.notificationService.showNotificationEchec(response.erreur);
              this.notificationService.blockUiStop()
            } else {
              this.dialogRef.close(true)
              this.notificationService.showNotificationSuccess(response.success)

            }
          },
          () => {

          },
          () => {
            this.notificationService.blockUiStop();

          }
        );

      }
    } else {
      this.notificationService.showNotificationEchec("le champ designation est obligatoire");
      this.notificationService.blockUiStop();
    }
  }

}
