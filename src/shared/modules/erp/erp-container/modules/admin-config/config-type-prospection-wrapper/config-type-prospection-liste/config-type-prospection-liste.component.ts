import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSort} from '@angular/material/sort';
import {MessagaProspectionService} from '../../../../../../../services/messaga-prospection.service';
import {UtilsService} from '../../../../../../../services/utils.service';
import {FilterService} from '../../../../../../../services/filter.service';
import {ConfigTypeProspectionEditComponent} from '../config-type-prospection-edit/config-type-prospection-edit.component';
import {NotificationService} from '../../../../../../../services/notification.service';

@Component({
  selector: 'app-config-type-prospection-liste',
  templateUrl: './config-type-prospection-liste.component.html',
  styleUrls: ['./config-type-prospection-liste.component.css']
})
export class ConfigTypeProspectionListeComponent implements OnInit {

  dataSourceInitial = [];
  dialogRef: MatDialogRef<ConfigTypeProspectionEditComponent>;
  list_type: any[];

  action: string;

  displayedColumns = ['select', 'number', 'Designation', 'Nbr Message Prospection', 'Edit'];


  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null,
    interim: true


  };
  displayedColumnsFilter = [];

  pageEvent: PageEvent;
  pageSize = 12;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];

  filterValuesStorage: any;
  filterChoixCheckboxStorage: any;
  filterchoixCheckBox = [];
  filterValues = {
    number: '',
    designation: '',
    nbr_message: '',
  };
  filterValuesCheckbox = {
    numberCheckox: false,
    designationCheckox: false,
    nbr_messageCheckox: false,

  };
  public form: FormGroup;
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;


  constructor(private dialog: MatDialog,
              private prospectionService: MessagaProspectionService,
              private formBuilder: FormBuilder,
              private utilsService: UtilsService,
              private notificationService: NotificationService,
              private filterService: FilterService) {
  }

  ngOnInit() {

    this.displayedColumnsFilter = this.prospectionService.displayFilterTypeName();
    this.form = this.formBuilder.group({
      numberFilter: ['', [Validators.required]],
      designationFilter: ['', [Validators.required]],
      nbr_messageFilter: ['', [Validators.required]],
    });
    this.prospectionService.closemodale.subscribe(
      (response: boolean) => {

        if (response === true) {
          if (this.dialogRef) {
            this.dialogRef.close();
          }

        }
      }
    );

    this.initData();
    this.utilsService.onRevientEnHaut();
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  editTypeProspectionModal(id: number = null): void {

    this.dialogRef = this.dialog.open(ConfigTypeProspectionEditComponent, {
      width: '700px',
      height: '270px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (response)=>{

        if(response){

          this.getNewData()
        }
      }
    )
  }

  // 'number', 'designation', 'nbr_message'
  ngAfterViewInit() {

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    //on verifie si dans notre localstorage si ya pas de flitres enclenche
    this.filterChoixCheckboxStorage = localStorage.getItem('filterchoixCheckBox');
    this.filterValuesStorage = localStorage.getItem('filterValues');
  }

  initData() {
    this.notificationService.blockUiStart();
    this.prospectionService.listTypeProspection().subscribe(
      (response: any) => {
        if (response) {
          this.dataSource = new MatTableDataSource(response['data']);
          this.length = response.lenght;
          this.notificationService.blockUiStop();
        }
      }
    );
  }

  valutElements() {


    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
    }


  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre(form: NgForm) {

    this.data.filter = form.value;

    (form.value);


    this.getNewData();

  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getNewData();


  }

  getNewData() {
    this.prospectionService.listTypeProspection(this.data).subscribe(
      (response: any) => {
        (response.data);
        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response['lenght'];


        }
      }
    );


  }

}
