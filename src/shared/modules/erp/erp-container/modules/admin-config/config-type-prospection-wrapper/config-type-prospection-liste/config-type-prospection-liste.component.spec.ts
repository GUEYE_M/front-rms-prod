import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypeProspectionListeComponent } from './config-type-prospection-liste.component';

describe('ConfigTypeProspectionListeComponent', () => {
  let component: ConfigTypeProspectionListeComponent;
  let fixture: ComponentFixture<ConfigTypeProspectionListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTypeProspectionListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypeProspectionListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
