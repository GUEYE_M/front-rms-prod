import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypeProspectionEditComponent } from './config-type-prospection-edit.component';

describe('ConfigTypeProspectionEditComponent', () => {
  let component: ConfigTypeProspectionEditComponent;
  let fixture: ComponentFixture<ConfigTypeProspectionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTypeProspectionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypeProspectionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
