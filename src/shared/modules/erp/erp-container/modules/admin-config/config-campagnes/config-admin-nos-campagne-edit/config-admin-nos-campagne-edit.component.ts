import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {NotificationService} from "../../../../../../../services/notification.service";
import {CampagneService} from "../../../../../../../services/erp/campagne.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-config-admin-nos-campagne-edit',
  templateUrl: './config-admin-nos-campagne-edit.component.html',
  styleUrls: ['./config-admin-nos-campagne-edit.component.css']
})
export class ConfigAdminNosCampagneEditComponent implements OnInit {

  editForm: FormGroup;
  displayName: string;
  edit: boolean = false;

  constructor(private fb: FormBuilder, private notificationService: NotificationService, private campagneService: CampagneService, private router: Router, private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any,private dialogRef: MatDialogRef<ConfigAdminNosCampagneEditComponent>
  ) {
    if (this.data) {
      this.displayName = 'Mis a jour Camapagne';
      this.notificationService.blockUiStart();

      this.campagneService.find(+this.data).subscribe
      (
        (response: any) => {


          this.initform(response.data);
          this.notificationService.blockUiStop();
          this.edit = true;

          this.editForm.patchValue(
            {
              campagne: this.data
            }
          );


        }
      );
    } else {
      this.displayName = 'Nouvelle Campagne';
      this.initform();
    }
  }


  ngOnInit() {


  }

  initform(campagne = {

    libelle: '',
    description: '',


  }) {
    this.editForm = this.fb.group(
      {
        libelle: [campagne.libelle, Validators.required],
        description: [campagne.description],
        campagne: []


      }
    );

  }


  save() {
    this.notificationService.blockUiStart();
    if (this.editForm.valid) {


      this.campagneService.edit(this.editForm.value).subscribe(
        (response: any) => {
          this.notificationService.blockUiStop();
          if (response.erreur) {
         
            this.notificationService.showNotificationEchec(response.erreur);
            this.notificationService.blockUiStop();
          } else {


            this.notificationService.showNotificationSuccess(response.success)
            this.dialogRef.close(true)

           

          }
        },
        (erreur) => {
          this.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
        },
        () => {

          this.notificationService.blockUiStop();
        }
      );


    } else {
      this.notificationService.showNotificationEchec('le champ nom campagne est obligatoire');
      this.notificationService.blockUiStop();
    }


  }


}
