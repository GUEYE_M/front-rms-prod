import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigAdminNosCampagneListeComponent } from './config-admin-nos-campagne-liste.component';

describe('ConfigAdminNosCampagneListeComponent', () => {
  let component: ConfigAdminNosCampagneListeComponent;
  let fixture: ComponentFixture<ConfigAdminNosCampagneListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAdminNosCampagneListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigAdminNosCampagneListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
