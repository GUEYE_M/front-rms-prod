import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {ConfigAdminNosCampagneEditComponent} from '../config-admin-nos-campagne-edit/config-admin-nos-campagne-edit.component';
import {NotificationService} from '../../../../../../../services/notification.service';
import {UtilsService} from '../../../../../../../services/utils.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FilterService} from '../../../../../../../services/filter.service';
import {CampagneService} from '../../../../../../../services/erp/campagne.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
    selector: 'app-config-admin-nos-campagne-liste',
    templateUrl: './config-admin-nos-campagne-liste.component.html',
    styleUrls: ['./config-admin-nos-campagne-liste.component.css']
})
export class ConfigAdminNosCampagneListeComponent implements OnInit {
    formFilter: FormGroup;
    dataSourceInitial = [];
    dialogRef: MatDialogRef<ConfigAdminNosCampagneEditComponent>;
    ligne_campagne: any[];
    showLoadClient: boolean;
    actionInput: Boolean = false;
    action: string;
    displayedColumns = ['select', 'number', 'Nom Campagne', 'Description', 'Edit'];
    dataSource: MatTableDataSource<any[]>;
    displayedColumnsFilter = [
        {
            name: 'nom',
            label: 'Nom'
        }
    ];

    elements = [];
    data = {
        offset: 0,
        limit: 12,
        filter: null,

    };


    pageEvent: PageEvent;
    pageSize = 12;

    length: number = 0;
    pageSizeOptions: number[] = [12, 25, 100];

    selection = new SelectionModel<any[]>(true, []);
    listActions = ['Archiver'];
    listsource: any[] = [];
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;


    constructor(private campagneService: CampagneService,
                private formBuilder: FormBuilder,
                private filterService: FilterService,
                private dialog: MatDialog,
                private utilsService: UtilsService,
                private notificationService: NotificationService
    ) {

    }

    ngOnInit() {
        this.formFilter = this.formBuilder.group({});
        // recuperation du filtre du sauvegarde
        this.elements = JSON.parse(localStorage.getItem('elements')) || [];
        const filterValues = JSON.parse(localStorage.getItem('filterValues'));
        this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
        if (filterValues) {
            if (this.filterService.sendFilters(this.formFilter)) {
                this.data.filter = this.formFilter.value;
                this.getNewData();
            }
        } else {
            this.initData();
        }
    }

    editCampagneModal(id: number = null): void {

        this.dialogRef = this.dialog.open(ConfigAdminNosCampagneEditComponent, {
            width: '700px',
            height: '250px',
            panelClass: 'myapp-no-padding-dialog',
            data: id

        });
        this.dialogRef.afterClosed().subscribe(
            (r) => {
                if (r) {

                    this.getNewData();
                }
            }
        );
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(
                row => this.selection.select(row));
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }


    ngAfterViewInit() {
    }


    initData() {
        this.notificationService.blockUiStart();
        this.campagneService.list().subscribe(
            (response: any) => {

                if (response) {
                    this.dataSource = new MatTableDataSource(response.data);
                    this.length = response.lenght;
                }


            },
            () => {
                this.notificationService.showNotificationEchec('erreur chargement de donnée!! veillez ressayer');
                this.notificationService.blockUiStop();
            },
            () => {
                this.notificationService.blockUiStop();
            }
        );


    }

    valutElements() {
        // creation du filtre depuis le storage
        this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
        if (this.elements.length === 0) {
            this.data.filter = null;
            this.initData();
            localStorage.removeItem('elements');
            localStorage.removeItem('filterValues');
        } else {
            localStorage.setItem('elements', JSON.stringify(this.elements));
        }
    }

// fonction qui envoie le formulaire de filtre en base de donnee

    onSubmitFiltre() {
        localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
        this.data.filter = this.formFilter.value;
        //(form.value)
        this.getNewData();

    }

    pagination(event: any) {

        this.data.offset = event.pageIndex;
        this.data.limit = event.pageSize;

        this.getNewData();


    }

    getNewData() {
        this.notificationService.blockUiStart();

        this.campagneService.list(this.data).subscribe(
            (response: any) => {
                this.dataSource = new MatTableDataSource(response.data);
                this.length = response.lenght;
            },
            (erreur) => {
                this.notificationService.showNotificationEchec('erreur !! Veillez ressayer a nouveau');
                this.notificationService.blockUiStop();
            },
            () => {
                this.notificationService.blockUiStop();
            }
        );
    }

}
