import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigAdminNosCampagneEditComponent } from './config-admin-nos-campagne-edit.component';

describe('ConfigAdminNosCampagneEditComponent', () => {
  let component: ConfigAdminNosCampagneEditComponent;
  let fixture: ComponentFixture<ConfigAdminNosCampagneEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAdminNosCampagneEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigAdminNosCampagneEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
