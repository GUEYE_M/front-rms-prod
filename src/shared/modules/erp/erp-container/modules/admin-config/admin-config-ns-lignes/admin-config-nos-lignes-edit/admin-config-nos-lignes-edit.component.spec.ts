import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigNosLignesEditComponent } from './admin-config-nos-lignes-edit.component';

describe('AdminConfigNosLignesEditComponent', () => {
  let component: AdminConfigNosLignesEditComponent;
  let fixture: ComponentFixture<AdminConfigNosLignesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigNosLignesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigNosLignesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
