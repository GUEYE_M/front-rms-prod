import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigNosLignesListComponent } from './admin-config-nos-lignes-list.component';

describe('AdminConfigNosLignesListComponent', () => {
  let component: AdminConfigNosLignesListComponent;
  let fixture: ComponentFixture<AdminConfigNosLignesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigNosLignesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigNosLignesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
