import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {map, tap} from "rxjs/operators";
import {from} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {NotificationService} from "../../../../../../../services/notification.service";
import {SpecialiteService} from "../../../../../../../services/specialite.service";
import {UserService} from "../../../../../../../services/user.service";
import {LigneTelephoniqueService} from "../../../../../../../services/ligne.telephonique.service";
import {BoiteEmailService} from "../../../../../../../services/erp/boite-email.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-config-nos-lignes-edit',
  templateUrl: './admin-config-nos-lignes-edit.component.html',
  styleUrls: ['./admin-config-nos-lignes-edit.component.css']
})
export class AdminConfigNosLignesEditComponent implements OnInit {


  editForm: FormGroup;
  list_emails: any;
  list_types: any;
  list_specialites: any[] = [];
  displayName: string;
  edit: boolean = false;
  currentUser: any;


  constructor(private fb: FormBuilder, private notificationService: NotificationService, private boiteEmailService: BoiteEmailService, private router: Router, private dialog: MatDialog,
              private specialiteService: SpecialiteService, private userService: UserService,
              @Inject(MAT_DIALOG_DATA) public data: any, private ligneTelephoniqueService: LigneTelephoniqueService,private dialogRef: MatDialogRef<AdminConfigNosLignesEditComponent>
  ) {

    this.currentUser = this.userService.getCurrentUser();


    if (this.data) {

      this.displayName = 'Mis a jour Ligne Telephonique';
      this.notificationService.blockUiStart();

      this.ligneTelephoniqueService.find(+this.data).subscribe
      (
        (response: any) => {


          this.initform(response.data);
          this.notificationService.blockUiStop();
          this.edit = true;

          this.editForm.patchValue(
            {
              ligne: this.data
            }
          );


        }
      );
    } else {
      this.displayName = 'Nouvelle Ligne Telephonique';
      this.initform();
    }


  }


  ngOnInit() {


    this.ligneTelephoniqueService.listtype().subscribe(
      (response: any) => {

        this.list_types = response.data;
      }
    );

    this.specialiteService.list(null,true).subscribe((response) => {
      from(response['data']).pipe(
        map(
          (rep) => {

            return {
              id: rep['id'],
              specialite: rep['specialite'],

            };
          }
        ),
        tap((specialite) => {
          this.list_specialites.push(specialite);
        })
      ).subscribe();

    });


  }

  initform(ligne = {

    email: '',
    telephone: '',
    commentaire: '',
    specialite: '',
    type: ''


  }) {
    this.editForm = this.fb.group(
      {
        email: [ligne.email, Validators.required],
        telephone: [ligne.telephone, Validators.required],
        commentaire: [ligne.commentaire],
        specialite: [ligne.specialite, Validators.required],
        type: [ligne.type, Validators.required],
        ligne: [],
        auteur: []

      }
    );

  }


  ngAfterViewInit() {
    this.boiteEmailService.list().subscribe(
      (response: any) => {

        this.list_emails = response.data;


      }
    );
  }

  compareWithFunc(a, b) {

    return a === b;
  }


  
  compareWithTypeLigne(a, b) {

    return a.designation === b.designation;
  }

  compareWithEmail(a, b) {

    return a.email === b.email;

  }

  save() {


    this.notificationService.blockUiStart()
    if (this.editForm.valid) {


      this.editForm.get('email').setValue(this.editForm.get('email').value.id);
      this.editForm.get('type').setValue(this.editForm.get('type').value.id);
      this.editForm.get('auteur').setValue(this.currentUser.gestionnaire.id);


      this.ligneTelephoniqueService.edit(this.editForm.value).subscribe(
        (response: any) => {
           
          this.notificationService.blockUiStop()
         
          if (response.erreur) {
            this.ligneTelephoniqueService.closemodale.next(false);

            this.notificationService.showNotificationEchec(response.erreur);
            this.notificationService.blockUiStop();
          } else {
            this.notificationService.showNotificationSuccess(response.success)
            this.dialogRef.close(true)


         


          }
        },
        (erreur) => {
          this.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
          this.notificationService.blockUiStop();
        },
        () => {

          this.notificationService.blockUiStop();
        }
      );


    } else {
      if (this.editForm.get('specialite').invalid) {
        this.notificationService.showNotificationEchec('le champ specialite est obligatoire');
      } else if (this.editForm.get('telephone').invalid) {
        this.notificationService.showNotificationEchec('le champ numero de telephone est obligatoire');
      } else if (this.editForm.get('email').invalid) {
        this.notificationService.showNotificationEchec('le champ Adresse E-mail est obligatoire');
      } else if (this.editForm.get('type').invalid) {
        this.notificationService.showNotificationEchec('le champ Type ligne est obligatoire');
      }

      this.notificationService.blockUiStop();
    }


  }


}
