import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {AdminConfigNosLignesEditComponent} from '../admin-config-nos-lignes-edit/admin-config-nos-lignes-edit.component';
import {NotificationService} from '../../../../../../../services/notification.service';
import {UtilsService} from '../../../../../../../services/utils.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {LigneTelephoniqueService} from '../../../../../../../services/ligne.telephonique.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import {FilterService} from '../../../../../../../services/filter.service';

@Component({
  selector: 'app-admin-config-nos-lignes-list',
  templateUrl: './admin-config-nos-lignes-list.component.html',
  styleUrls: ['./admin-config-nos-lignes-list.component.css']
})
export class AdminConfigNosLignesListComponent implements OnInit, AfterViewInit {
  formFilter: FormGroup;
  dialogRef: MatDialogRef<AdminConfigNosLignesEditComponent>;
  ligne_list: any[];
  showLoadClient: boolean;
  actionInput: Boolean = false;
  action: string;
  filterChoixCheckboxStorage: any;
  filterValuesStorage: any;
  filterchoixCheckBox = [];
  public form: FormGroup;

  displayedColumns = ['select', 'number', 'Adresse E-mail', 'Telephone', 'type', 'apartenance', 'Specialite', 'Edit'];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  displayedColumnsFilter = [
    {
      name: 'tel',
      label: 'Telephone'
    },
    {
      name: 'email',
      label: 'Adresse E-mail'
    },
    {
      name: 'type',
      label: 'Type Ligne'
    }
  ];

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,

  };


  pageEvent: PageEvent;
  pageSize = 12;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];

  constructor(
    private ligneService: LigneTelephoniqueService,
    private dialog: MatDialog,
    private filterService: FilterService,
    private fb: FormBuilder,
    private utilsService: UtilsService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    } else {
      this.initData();
    }

    this.utilsService.onRevientEnHaut();
    this.ligneService.closemodale.subscribe(
      (response: boolean) => {
        if (response) {
          this.dialogRef.close();
        }
      }
    );
  }

  editLigneModal(id: number = null): void {

    this.dialogRef = this.dialog.open(AdminConfigNosLignesEditComponent, {
      width: '800px',
      height: '620px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (r) => {
        if (r) {

          this.getNewData();
        }
      }
    );
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  ngAfterViewInit() {

  }

  initData() {

    this.notificationService.blockUiStart();
    this.ligneService.list().subscribe(
      (response: any) => {

        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
        }


      },
      () => {
        this.notificationService.showNotificationEchec('erreur chargement de donnée!! veillez ressayer');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );


  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();
  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getNewData();


  }

  getNewData() {
    this.notificationService.blockUiStart();

    this.ligneService.list(this.data).subscribe(
      (response: any) => {


        this.dataSource = new MatTableDataSource(response.data);
        this.length = response.lenght;


      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur !! Veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );


  }

}
