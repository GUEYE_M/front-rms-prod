import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigHebergementComponent } from './admin-config-hebergement.component';

describe('AdminConfigHebergementComponent', () => {
  let component: AdminConfigHebergementComponent;
  let fixture: ComponentFixture<AdminConfigHebergementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConfigHebergementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigHebergementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
