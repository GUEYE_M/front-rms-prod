import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm, FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource, MatDialogRef, MatSort, MatPaginator, MatDialog} from '@angular/material';
import {AdminConfigHebergementEditComponent} from '../admin-config-hebergement-edit/admin-config-hebergement-edit.component';
import {SelectionModel} from '@angular/cdk/collections';

import {NotificationService} from 'src/shared/services/notification.service';
import {ActivatedRoute} from '@angular/router';
import {HebergementServiceService} from 'src/shared/services/recruteur/hebergement.service.service';
import {FilterService} from '../../../../../../services/filter.service';

@Component({
  selector: 'app-admin-config-hebergement',
  templateUrl: './admin-config-hebergement.component.html',
  styleUrls: ['./admin-config-hebergement.component.css']
})
export class AdminConfigHebergementComponent implements OnInit {
  formFilter: FormGroup;
  dialogRef: MatDialogRef<AdminConfigHebergementEditComponent>;
  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
  };
  modal: boolean = false;
  pageSize: number = 12;
  length: number = 0;
  pageSizeOptions: number[] = [5, 12, 25, 50, 100];
  displayedColumns = [
    'select',
    'number',
    'etablissement',
    'lieu',
    'etoile',
    'adresse',
    'telephone',
    'auteur',
    'archiver',
    'Edit'];

  selection = new SelectionModel<any[]>(true, []);

  displayedColumnsFilter = [];


  edit: boolean = false;
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  constructor(private hebergementService: HebergementServiceService, private filterService: FilterService,
              private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
              private dialog: MatDialog, private notifierService: NotificationService) {
    // this.initData();
  }

  ngOnInit() {
    this.formFilter = this.formBuilder.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.initData();
      }
    } else {
      this.initData();
    }
    this.displayedColumnsFilter = this.hebergementService.displayedColumnsName();
  }

  editHebergementModal(id: number = null): void {


    this.dialogRef = this.dialog.open(AdminConfigHebergementEditComponent, {
      width: '800px',
      height: '400',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {
          this.initData();
        }
      }
    );
  }


  initData() {

    this.notifierService.blockUiStart();
    this.hebergementService.list(this.data).subscribe(
      (response) => {
        this.notifierService.blockUiStop();
        if (!response.erreur) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
        } else {
          this.notifierService.showNotificationEchecCopie('bottomRight', response.erreur);
        }
      }
    );


  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }

  }

// fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.initData();

  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.initData();


  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  archivedAction(id: number = null): void {
  
       

    this.notifierService.onConfirm()
      this.notifierService.dialogRef.afterClosed().subscribe(
      (x)=>{
        if(x){
          this.notifierService.blockUiStart();
          this.hebergementService.archived({id:id}).subscribe(
            (response) => {
              this.notifierService.blockUiStop();
              if (!response.erreur) {
                this.notifierService.showNotificationSuccess('Archivage effectuer');
                this.initData();
              
              } else {
                this.notifierService.showNotificationEchecCopie('bottomRight', response.erreur);
              }
            }
          );
        }
     
        
      }
    )
  }
}
