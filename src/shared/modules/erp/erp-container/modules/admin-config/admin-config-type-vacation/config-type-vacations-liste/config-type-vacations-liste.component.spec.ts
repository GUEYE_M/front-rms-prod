import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypeVacationsListeComponent } from './config-type-vacations-liste.component';

describe('ConfigTypeVacationsListeComponent', () => {
  let component: ConfigTypeVacationsListeComponent;
  let fixture: ComponentFixture<ConfigTypeVacationsListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTypeVacationsListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypeVacationsListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
