import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypeVacationsEditComponent } from './config-type-vacations-edit.component';

describe('ConfigTypeVacationsEditComponent', () => {
  let component: ConfigTypeVacationsEditComponent;
  let fixture: ComponentFixture<ConfigTypeVacationsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTypeVacationsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypeVacationsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
