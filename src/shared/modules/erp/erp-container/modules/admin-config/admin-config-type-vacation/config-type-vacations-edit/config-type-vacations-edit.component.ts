import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../../services/notification.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {TitleCasePipe} from '@angular/common';
import {VacationClientService} from '../../../../../../../services/recruteur/vacation-client.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-config-type-vacations-edit',
  templateUrl: './config-type-vacations-edit.component.html',
  styleUrls: ['./config-type-vacations-edit.component.css']
})
export class ConfigTypeVacationsEditComponent implements OnInit {

  editTypeVactionForm: FormGroup;
  displayName: string = 'Nouveau Type de vacation';
  edit: boolean = false;

  constructor(private fb: FormBuilder,
              private notificationService: NotificationService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private vacationService: VacationClientService,
              private router: Router,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<ConfigTypeVacationsEditComponent>,
              private titlecasePipe: TitleCasePipe
  ) {}

  ngOnInit() {
    this.initform();
  }

  initform(){
    if(this.data) {
      this.displayName = 'Mise à jour du type vacation';
      this.editTypeVactionForm = this.fb.group(
        {
          id: [this.data.id],
          designation: [this.data.designation, Validators.required],
          description: [this.data.description],
          nom: [this.data.nom, Validators.required],
          abreviation: [this.data.abreviation, Validators.required],
        }
      );

    }
    else {
      this.editTypeVactionForm = this.fb.group(
        {
          designation: ['', Validators.required],
          nom: ['', Validators.required],
          description: [],
          abreviation: ['', [Validators.minLength(3), Validators.required, Validators.pattern('^[a-zA-Z0-9+*/#@! ]+$')]],
        }
      );
    }

  }


// save(){
//   this.prospectionService.newTypeProspection(this.editTypeMessageForm.value);
// }
  save() {
    this.vacationService.editTypeVacation(this.editTypeVactionForm.value).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.vacationService.closemodaleType.next(false);
          this.notificationService.showNotificationEchec(response.erreur);
          this.notificationService.blockUiStop();
        } else {
          this.notificationService.showNotificationSuccess(response.success);
          this.dialogRef.close(true)
        }
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }
}
