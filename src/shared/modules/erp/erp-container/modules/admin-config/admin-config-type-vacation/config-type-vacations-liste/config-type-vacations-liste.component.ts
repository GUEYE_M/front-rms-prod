import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {ConfigTypeVacationsEditComponent} from '../config-type-vacations-edit/config-type-vacations-edit.component';
import {SelectionModel} from '@angular/cdk/collections';
import {FilterService} from '../../../../../../../services/filter.service';
import {UtilsService} from '../../../../../../../services/utils.service';
import {VacationClientService} from '../../../../../../../services/recruteur/vacation-client.service';
import {NotificationService} from '../../../../../../../services/notification.service';

@Component({
  selector: 'app-config-type-vacations-liste',
  templateUrl: './config-type-vacations-liste.component.html',
  styleUrls: ['./config-type-vacations-liste.component.css']
})
export class ConfigTypeVacationsListeComponent implements OnInit {
  formFilter: FormGroup;
  dialogRef: MatDialogRef<ConfigTypeVacationsEditComponent>;
  list_type: any[];
  dataSourceInitial = [];
  action: string;
  public form: FormGroup;
  displayedColumns = ['select', 'number', 'nom vacation','designation', 'abreviation', 'nbr vacation', 'edit'];
  displayedColumnsFilter = [
    {
      name: 'nom',
      label: 'Nom'
    }
  ];
  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
  };
  pageEvent: PageEvent;
  pageSize = 12;
  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  constructor(private dialog: MatDialog,
              private formBuilder: FormBuilder,
              private filterService: FilterService,
              private utilsService: UtilsService,
              private vacationSrvice: VacationClientService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.formFilter = this.formBuilder.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    }

    this.utilsService.onRevientEnHaut();
    this.initData();
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  editTypeProspectionModal(item): void {
    this.dialogRef = this.dialog.open(ConfigTypeVacationsEditComponent, {
      width: '700px',
      height: '560px',
      panelClass: 'myapp-no-padding-dialog',
      data: item
    });

    this.dialogRef.afterClosed().subscribe(
      (r) => {
        if (r) {
          this.getNewData();
        }
      }
    );
  }

  initData() {
    this.notificationService.blockUiStart();
    this.vacationSrvice.listtype().subscribe(
      (response: any) => {
        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
          this.notificationService.blockUiStop();
        }
      }
    );
  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }

  // fonction qui envoie le formulaire de filtre en base de donnee
  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();
  }

  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }

  getNewData() {
    this.notificationService.blockUiStart();
    this.vacationSrvice.listtype(this.data).subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.length = response.lenght;
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur !! Veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }
}
