import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListInterimComponent } from './list-interim/list-interim.component';
import { FormEmailComponent } from './single-interim/form-email/form-email.component';
import { SharedModule } from '../../../../shared/shared.module';
import { InterimRouting } from './interim.routing';
import { ChAEviterModalComponent } from './single-interim/ch-a-eviter-modal/ch-a-eviter-modal.component';
import { ContratSignComponent } from './single-interim/mission-medecin/contrat-sign/contrat-sign.component';
import { ConfirmDeleteSpecialiteComponent } from './modal-interim/confirm-delete-specialite/confirm-delete-specialite.component';
import { ConstitutionDossierModalComponent } from './modal-interim/constitution-dossier-modal/constitution-dossier-modal.component';

@NgModule({
	declarations: [
		ListInterimComponent,
		FormEmailComponent,
		ChAEviterModalComponent,
		ContratSignComponent,
		ConfirmDeleteSpecialiteComponent,
		ConstitutionDossierModalComponent
	],
	imports: [ CommonModule, SharedModule, InterimRouting ],
	entryComponents: [ ConfirmDeleteSpecialiteComponent, ChAEviterModalComponent, ConstitutionDossierModalComponent ],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class InterimModule {}
