import {Route, RouterModule} from '@angular/router';
import {ListInterimComponent} from './list-interim/list-interim.component';
import {InterimResolve} from '../../../../../resolver/interim.resolve';
import {DisponibiliteComponent} from './single-interim/disponibilite/disponibilite.component';
import {DossierComponent} from './single-interim/dossier/dossier.component';
import {InterimSingleResolve} from '../../../../../resolver/InterimSingle.resolve';
import {SingleInterimComponent} from './single-interim/single-interim.component';
import {SpecialiteInterimModalComponent} from './single-interim/specialite-interim/specialite-interim-modal/specialite-interim-modal.component';
import {ModalDisponibiliteGestionComponent} from './modal-interim/modal-disponibilite-gestion/modal-disponibilite-gestion.component';
import {BienvenueMyRMSComponentComponent} from './bienvenue-my-rmscomponent/bienvenue-my-rmscomponent.component';


const INTERIM_CONFIG: Route[] = [
  {path: 'liste', resolve: {listInterims: InterimResolve}, component: ListInterimComponent},
  {path: ':id/show', resolve: {interimSingle: InterimSingleResolve}, component: SingleInterimComponent},
  {path: 'specialite-modal', component: SpecialiteInterimModalComponent},
  // {path: 'disponibilite-modal', component: ModalDisponibiliteGestionComponent},
  {path: 'disponibilite', component: DisponibiliteComponent},
  {path: 'dossier', component: DossierComponent},
];

export const InterimRouting = RouterModule.forChild(INTERIM_CONFIG);



