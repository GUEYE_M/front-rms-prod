import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bienvenue-my-rmscomponent',
  templateUrl: './bienvenue-my-rmscomponent.component.html',
  styleUrls: ['./bienvenue-my-rmscomponent.component.css']
})
export class BienvenueMyRMSComponentComponent implements OnInit {

  status = true;
  item = '';
  cpt  = true ;
  constructor() { }

  ngOnInit() {

  }
  deroule(status, item) {
    this.cpt = false;
    this.status = status;
    this.item = item;
  }

}
