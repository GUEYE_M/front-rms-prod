import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienvenueMyRMSComponentComponent } from './bienvenue-my-rmscomponent.component';

describe('BienvenueMyRMSComponentComponent', () => {
  let component: BienvenueMyRMSComponentComponent;
  let fixture: ComponentFixture<BienvenueMyRMSComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienvenueMyRMSComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienvenueMyRMSComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
