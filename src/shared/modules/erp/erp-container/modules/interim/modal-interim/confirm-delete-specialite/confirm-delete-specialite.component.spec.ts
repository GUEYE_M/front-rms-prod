import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeleteSpecialiteComponent } from './confirm-delete-specialite.component';

describe('ConfirmDeleteSpecialiteComponent', () => {
  let component: ConfirmDeleteSpecialiteComponent;
  let fixture: ComponentFixture<ConfirmDeleteSpecialiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDeleteSpecialiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeleteSpecialiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
