import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstitutionDossierModalComponent } from './constitution-dossier-modal.component';

describe('ConstitutionDossierModalComponent', () => {
  let component: ConstitutionDossierModalComponent;
  let fixture: ComponentFixture<ConstitutionDossierModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstitutionDossierModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstitutionDossierModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
