import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {take} from 'rxjs/operators';
import {NotificationService} from '../../../../../../../services/notification.service';
import {QualificationService} from '../../../../../../../services/qualification.service';
import {SpecialiteService} from '../../../../../../../services/specialite.service';

@Component({
  selector: 'app-confirm-delete-specialite',
  templateUrl: './confirm-delete-specialite.component.html',
  styleUrls: ['./confirm-delete-specialite.component.css']
})
export class ConfirmDeleteSpecialiteComponent implements OnInit {
  item: any;
  id_interim = 0;
  choixCheckBox = [];
  edit: any;

  constructor(
    private notificationService: NotificationService,
    private qualificationService: QualificationService,
    private specialiteService: SpecialiteService,
    public dialogRef: MatDialogRef<ConfirmDeleteSpecialiteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.id_interim = data.id_interim;
    this.item = data.item_;
    this.choixCheckBox = data.choixEdits;
    this.edit = data.edit;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onEditQualifs() {
    this.notificationService.blockUiStart();
    this.qualificationService.update(this.id_interim, this.choixCheckBox).subscribe(
      (next: any) => {
        if (next.body !== 'Imposssible d\'effectuer la mis a jour') {
          this.notificationService.showNotificationSuccessCopie('topCenter', next.body);
        } else {
          this.notificationService.showNotificationEchecCopie('topCenter', next.body);
        }
        this.qualificationService.getQualificationsForMed(this.id_interim).subscribe(
          (val) => {
            this.specialiteService.listeSpecialitesForInterimSubject.next(val);
          }
        );
        this.notificationService.blockUiStop();
      },
      error => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('veuillez réessayer svp ! si sa persiste veuillez vous approcher du service informatique');
      }
    );
  }

  onDeleteQualif() {
    this.notificationService.blockUiStart();
    this.specialiteService.deleteQualifsBySpecialite(this.id_interim, this.item).pipe(take(1)).subscribe(
      (next: any) => {
        this.qualificationService.getQualificationsForMed(this.id_interim).subscribe(
          (val) => {
            this.specialiteService.listeSpecialitesForInterimSubject.next(val);
            this.notificationService.blockUiStop();
            this.specialiteService.detail = false;
          }
        );
        this.notificationService.showNotificationSuccessCopie('topCenter', next + ' (' + this.item.nom_specialite + ' : ' + this.item.qualification + ')');
      }
    );
  }

  ngOnInit() {
  }

}
