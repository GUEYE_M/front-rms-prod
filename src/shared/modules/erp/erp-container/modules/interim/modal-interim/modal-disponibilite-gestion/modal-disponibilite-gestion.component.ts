import { AfterViewInit, Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import $ from 'jquery';
import { from, of } from 'rxjs';
import { distinct, groupBy, mergeMap, toArray } from 'rxjs/operators';
import { DisponibiliteService } from '../../../../../../../services/interim/disponibilite.service';
import { InterimService } from '../../../../../../../services/interim/interim.service';
import { UserService } from '../../../../../../../services/user.service';
import { CandidatureService } from '../../../../../../../services/candidature.service';
import { RedirectionSercive } from '../../../../../../../services/redirection.sercive';
import { NotificationService } from '../../../../../../../services/notification.service';
import { RolesService } from '../../../../../../../services/roles.service';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

moment().locale('fr-Fr');

@Component({
	selector: 'app-modal-disponibilite-gestion',
	templateUrl: './modal-disponibilite-gestion.component.html',
	styleUrls: [ './modal-disponibilite-gestion.component.css' ]
})
export class ModalDisponibiliteGestionComponent implements OnInit, AfterViewInit {
	dateFormatFr = [
		'Janvier',
		'Février',
		'Mars',
		'Avril',
		'Mai',
		'Juin',
		'Juillet',
		'Août',
		'Septembre',
		'Octobre',
		'Novembre',
		'Décembre'
	];
	listDateForMonth = [];
	displayedColumns = [ 'select', 'date' ];
	displaydColumnsMatching = [
		'select',
		'date',
		'libelle',
		'debut',
		'fin',
		'salaire',
		'etablissement',
		'departement'
	];
	listsource: any[] = [];
	adminOrInterim = 'admin';
	loaderInterim = false;
	interim_id: number;
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild(MatSort, { static: false })
	sortMatching: MatSort;
	@ViewChild('listPaginatorDispo', { static: false })
	paginator: MatPaginator;
	@ViewChild('listPaginator', { static: false })
	paginatorMatching: MatPaginator;
	@Input() dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
	dataSourceOffresMatching: MatTableDataSource<any[]> = new MatTableDataSource([]);
	select_specialite = [];
	listes_offfres_by_specialite = [];
	conditionMedecin = '';
	selection = new SelectionModel<any[]>(true, []);
	selectionMatching = new SelectionModel<any[]>(true, []);
	p = 1;
	mois = [];

	constructor(
		private disponibiliteService: DisponibiliteService,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public dialog: MatDialog,
		public rolesService: RolesService,
		private interimService: InterimService,
		public userService: UserService,
		private candidatureService: CandidatureService,
		private notifierService: NotificationService
	) {}

	ngOnInit() {
		this.notifierService.blockUiStart();
		this.interim_id = this.disponibiliteService.interimIdDis;

		if (this.data === 1) {
			this.disponibiliteService.listeDisponibiliteChoisiSubject.subscribe((x) => {
				this.dataSource.data = x;
				this.dataSource.sort = this.sort;
				this.notifierService.blockUiStop();
			});
			// this.disponibiliteService.listeDisponibilitesForInterimSubject.next([]);
			this.getData(this.interim_id);
		}
		if (this.data === 2) {
			this.interimService.listesNomSpeciliatesForInterim.subscribe((data) => {
				this.select_specialite = data.data;
			});
			this.disponibiliteService.listeDisponibiliteOffreMatchingSubject.subscribe((value: any) => {
				this.notifierService.blockUiStop();
				this.listes_offfres_by_specialite = value.data;
				if (value) {
					const tmp_dates = [];
					moment.locale('fr-Fr');
					value.forEach((element) => {
						tmp_dates.push({
							date: moment(element.date).format('LLLL'),
							date_sup: moment(element.date),
							mois: element.mois
						});
					});
					this.dataSource.data = tmp_dates;
					this.listDateForMonth = value;
				}
			});
		}
	}
	onNoClick(): void {
		this.disponibiliteService.dialogRef.close();
	}
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	masterToggleMatching() {
		this.isAllSelectedMatching()
			? this.clearSelectionMatching()
			: this.dataSourceOffresMatching.data.forEach((row) => this.selectionMatching.select(row));
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	isAllSelectedMatching() {
		const numSelected = this.selectionMatching.selected.length;
		const numRows = this.dataSourceOffresMatching.data.length;
		return numSelected === numRows;
	}

	clearSelection() {
		this.selection = new SelectionModel<any[]>(true, []);
	}

	clearSelectionMatching() {
		this.selectionMatching = new SelectionModel<any[]>(true, []);
		this.dataSourceOffresMatching.data = [];
	}

	deleteChoiceDisponibilite() {
		this.notifierService.onConfirm();
		this.notifierService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notifierService.blockUiStart();
				this.disponibiliteService.deleteChoiceDisponibilite(this.interim_id, this.selection.selected).subscribe(
					(next: any) => {
						this.disponibiliteService.listeDisponibilitesForInterimSubject.next([]);
						this.clearSelection();
						if (next.erreur) {
							this.notifierService.showNotificationEchecCopie('topCenter', next.erreur);
						} else {
							this.getData(this.interim_id);
							this.notifierService.showNotificationSuccessCopie('topCenter', next);
						}
					},
					(error) => {
						this.notifierService.showNotificationEchecCopie('topCenter', error.erreur);
						this.clearSelection();
					}
				);
			} else {
				this.clearSelection();
			}
		});
	}

	getData(interim_id) {
		this.disponibiliteService.getDisponibilite(interim_id).subscribe((disponibilite: any) => {
			if (disponibilite) {
				const tmp = [];
				this.interimService.listes_mois_disponibilite = [];
				const tmpMois = [];
				disponibilite.forEach(function(elememt) {
					tmp.push({
						date: new Date(elememt.date),
						css: 'stay-dates',
						selectable: false,
						title: 'Holiday time !'
					});
					tmpMois.push(elememt);
				});
				const source = from(tmpMois);

				const data = source
					.pipe(
						groupBy((moisG) => moisG.mois),
						// return each item in group as array
						mergeMap((group) => group.pipe(toArray()))
					)
					.subscribe((val) => this.interimService.listes_mois_disponibilite.push(val));
				this.disponibiliteService.listeDisponibilitesForInterimSubject.next(tmp);
			}
			this.notifierService.blockUiStop();
		});
	}

	getItemChoiceOnSpecialite(nomSpecialite) {
		this.notifierService.blockUiStart();
		let dataTmp = [];
		const dateDebutSplit = this.disponibiliteService.dateDebut.split('-');
		const moisDisponibilite = this.disponibiliteService.dateDebut;
		const mois = this.dateFormatFr[+dateDebutSplit[0] - 1];
		const annee = dateDebutSplit[1];
		this.disponibiliteService
			.getDisponibiliteMatchingMyRMS(this.interim_id, nomSpecialite, moisDisponibilite, mois, annee)
			.subscribe(
				(x: any) => {
					if (x.erreur === '') {
						x = x.data;
						dataTmp = [];
						const plannings = x.plannings;
						const disponibilitesInterimsOnMonth = x.disponibilites;
						disponibilitesInterimsOnMonth.forEach((y) => {
							plannings.filter((tmp) => {
								if (y.date === tmp.date) {
									dataTmp.push(tmp);
								}
							});
						});
						if (dataTmp.length === 0) {
							this.notifierService.showNotificationEchecCopie(
								'topCenter',
								'Aucune correspondance trouvée'
							);
						}
						this.dataSourceOffresMatching.paginator = this.paginatorMatching;
						this.dataSourceOffresMatching.data = dataTmp;
						this.dataSourceOffresMatching.sort = this.sortMatching;
						this.notifierService.blockUiStop();
					} else {
						this.notifierService.showNotificationEchecCopie('topCenter', x.erreur);
						this.notifierService.blockUiStop();
					}
				},
				(error) => {
					this.notifierService.blockUiStop();
				}
			);
	}

	onPostuler() {
		if (this.selectionMatching.selected.length === 0) {
			this.clearSelectionMatching();
			this.notifierService.showNotificationEchecCopie(
				'topCenter',
				'Au moins une date est necessaire pour postuler !'
			);
		} else {
			this.notifierService.blockUiStart();
			let parcours = 0;
			this.selectionMatching.selected.forEach((element) => {
				this.selectionMatching.selected[parcours]['id_interim'] = this.interim_id;
				this.selectionMatching.selected[parcours]['condition_medecin'] = this.conditionMedecin;
				parcours = parcours + 1;
			});
			this.candidatureService
				.create_candidature_medecin_matching(JSON.stringify(this.selectionMatching.selected))
				.subscribe(
					(next: any) => {
						this.clearSelectionMatching();
						if (next.erreur) {
							this.notifierService.showNotificationEchec(next.erreur);
						} else {
							this.notifierService.showNotificationSuccessCopie('topCenter', next.success);
						}
						this.notifierService.blockUiStop();
					},
					(error) => {
						this.notifierService.blockUiStop();
					}
				);
		}
	}

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		this.dataSourceOffresMatching.paginator = this.paginatorMatching;
		this.dataSourceOffresMatching.sort = this.sortMatching;

		// this.disponibiliteService.listeDisponibiliteOffreMatchingSubject.subscribe(
		//     (next: any) => {
		//        if(next){
		//            if(next.data){
		//                this.select_specialite = [];
		//                this.select_specialite = next.data;
		//            }
		//        }
		//     }
		// );
	}

	modal_hide() {
		if ($('#myModalDispobiliteMatching').is(':visible') === false) {
			this.dataSourceOffresMatching.data = [];
		}
	}
}
