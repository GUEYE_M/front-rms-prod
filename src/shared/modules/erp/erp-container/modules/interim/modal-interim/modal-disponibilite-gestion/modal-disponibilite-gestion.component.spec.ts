import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDisponibiliteGestionComponent } from './modal-disponibilite-gestion.component';

describe('ModalDisponibiliteGestionComponent', () => {
  let component: ModalDisponibiliteGestionComponent;
  let fixture: ComponentFixture<ModalDisponibiliteGestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDisponibiliteGestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDisponibiliteGestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
