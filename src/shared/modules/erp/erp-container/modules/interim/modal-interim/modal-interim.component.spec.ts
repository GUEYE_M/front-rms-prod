import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInterimComponent } from './modal-interim.component';

describe('ModalInterimComponent', () => {
  let component: ModalInterimComponent;
  let fixture: ComponentFixture<ModalInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
