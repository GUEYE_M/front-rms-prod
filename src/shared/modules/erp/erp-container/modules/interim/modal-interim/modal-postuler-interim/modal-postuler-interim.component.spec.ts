import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPostulerInterimComponent } from './modal-postuler-interim.component';

describe('ModalPostulerInterimComponent', () => {
  let component: ModalPostulerInterimComponent;
  let fixture: ComponentFixture<ModalPostulerInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPostulerInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPostulerInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
