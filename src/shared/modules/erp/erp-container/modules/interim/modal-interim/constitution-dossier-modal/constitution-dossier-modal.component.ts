import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DossierService } from 'src/shared/services/interim/dossier.service';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
	selector: 'app-constitution-dossier-modal',
	templateUrl: './constitution-dossier-modal.component.html',
	styleUrls: [ './constitution-dossier-modal.component.css' ]
})
export class ConstitutionDossierModalComponent implements OnInit {
	elements: any[] = [];
	elementDossiers = [
		{
			name: 'CV',
			description: 'CV à jour',
			checked: false
		},
		{
			name: "Attestation d'inscription à l'ordre",
			description: "Attestation d'inscription à l'ordre en cour",
			checked: false
		},

		{
			name: 'Carte grise',
			description: 'Carte grise, si véhiculé',
			checked: false
		},
		{
			name: 'Justificatif de domicile',
			description: 'Justificatif de domicile de moins de trois mois',
			checked: false
		},

		{
			name: 'Diplôme doctorat',
			description: 'Diplôme Doctorat (+ la traduction française si le diplôme est en langue étrangère)',
			checked: false
		},
		{
			name: 'Diplôme de spécialisation',
			description:
				"Diplôme de spécialisation (+ attestation de formation spécialisée ou autre attestation de reconnaissance de l'équivalence du diplôme en France, pour les diplômes obtenus à l'étranger)",
			checked: false
		},

		{
			name: 'Autres diplômes ou attestations',
			description: 'Autres diplômes et/ou attestations',
			checked: false
		},
		{
			name: "Carte d'identité",
			description: "Carte d'identité valide",
			checked: false
		},
		{
			name: 'Carte Vitale',
			description: 'Carte Vitale',
			checked: false
		},
		{
			name: 'A.A.R.C en cours',
			description: 'Attestation Assurance Responsabilité Civile Professionnelle en cours',
			checked: false
		},

		{
			name: 'RIB',
			description: 'RIB',
			checked: false
		},
		{
			name: 'Passe Sanitaire',
			description: 'Passe Sanitaire',
			checked: false
		},
		{
			name: 'Casier judiciaire ',
			description: "Casier Judiciaire",
			checked: false
		}

		
	];

	allCheckboxSelect: false;
	constructor(
		private dossierService: DossierService,
		private notificationService: NotificationService,
		private dialogRef: MatDialogRef<ConstitutionDossierModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {}

	ngOnInit() {}

	changeCheckbox(i) {
		this.elementDossiers[i].checked = !this.elementDossiers[i].checked;

		if (this.elementDossiers[i].checked == true) {
			this.elements.push(this.elementDossiers[i].description);
		} else {
			this.elements.map((el) => {
				if (this.elementDossiers[i].description === el) {
					const index = this.elements.indexOf(el);
					this.elements.splice(index, 1);
				}
			});
		}
	}

	allSelect() {
		this.elementDossiers.map((item, i) => {
			if (this.allCheckboxSelect) {
				this.changeCheckbox(i);
				return (item.checked = true);
			} else {
				this.elements = [];
				return (item.checked = false);
			}
		});
	}

	send() {
		const data = {
			data: this.elements,
			users: this.data
		};

		if (this.elements.length > 0) {
			this.notificationService.blockUiStart();

			this.dossierService.constitutionDossier(data).subscribe(
				(response: any) => {
					this.notificationService.blockUiStop();
					if (response.erreur) {
						this.notificationService.showNotificationEchec(response.erreur);
					} else {
					}
				},
				(erreur) => {},
				() => {
					this.notificationService.showNotificationSuccess(
						'mail de constitution de dossier a bien été envoyer'
					);
					this.dialogRef.close();
				}
			);
		} else {
			this.notificationService.showNotificationEchec('aucun élement selectionné');
		}
	}
}
