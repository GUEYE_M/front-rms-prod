import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { EditInterimComponent } from "../edit-interim/edit-interim.component";
import { FormGroup, FormControl, FormBuilder, NgForm } from "@angular/forms";
import { InterimService } from "src/shared/services/interim/interim.service";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "src/shared/services/user.service";
import { NotificationService } from "src/shared/services/notification.service";
import { UtilsService } from "src/shared/services/utils.service";
import {
  MatDialog,
  MatSort,
  MatPaginator,
  MatTableDataSource,
  PageEvent,
  MAT_DIALOG_DATA
} from "@angular/material";
import { SpecialiteService } from "src/shared/services/specialite.service";
import { from } from "rxjs";
import { map, tap } from "rxjs/operators";
import { SelectionModel } from "@angular/cdk/collections";
import { SelectAutocompleteComponent } from "mat-select-autocomplete";

@Component({
  selector: "app-modal-interim",
  templateUrl: "./modal-interim.component.html",
  styleUrls: ["./modal-interim.component.css"]
})
export class ModalInterimComponent implements OnInit {
  listSpecialites = [];
  specialite: string = ""

  pageEvent: PageEvent;
  interimForm: FormGroup;

  length = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];
  // tslint:disable-next-line:no-inferrable-types

  displayedColumns = [
    "select",
    "number",
    "Nom",
    "Prenom",
    "Email",
    "telephone"

  ];

  elements = [];
  data = {
    specialite: null,
    id_client: null
  };
  displayedColumnsFilter = [];

  profileForm = new FormGroup({
    selected: new FormControl([])
  });
  listsource: any[] = [];
  showLoadInterim = false;
  idInterim = 0;
  loader = 0;
  ModalUpdateInterim: any;
  KeysInterimModalInterim: any;
  KeysUserModalInterim: any;

  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild("listPaginator", { static: false }) paginator: MatPaginator;

  @ViewChild(SelectAutocompleteComponent, { static: false })
  multiSelect: SelectAutocompleteComponent;
  pageSize = [5, 10, 20];
  constructor(
    private interimService: InterimService,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private utilService: UtilsService,
    @Inject(MAT_DIALOG_DATA) idata: any
  ) {

    if (idata) {
      this.getData(idata);
    }

  }

  ngOnInit() {






  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // fonction qui envoie le formulaire de filtre en base de donnee

  getData(data: any) {
    this.notificationService.blockUiStart();
    this.interimService
      .getAllInterimByspecialiteAndInscription(data)
      .subscribe(
        (response: any) => {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.lenght;
          this.dataSource.paginator = this.paginator;
          this.showLoadInterim = true;
        },
        erreur => {
          this.notificationService.showNotificationEchec(
            "erreur recherche!! veillez ressayer a nouveau"
          );
          this.notificationService.blockUiStop();
        },
        () => {
          this.notificationService.blockUiStop();
        }
      );
  }



  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  // tslint:disable-next-line:use-lifecycle-interface

  // tslint:disable-next-line:use-lifecycle-interface
}
