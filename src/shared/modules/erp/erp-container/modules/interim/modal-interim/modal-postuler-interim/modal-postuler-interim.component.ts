import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {NotificationService} from '../../../../../../../services/notification.service';
import {CandidatureService} from '../../../../../../../services/candidature.service';
import {OffresServiceAll} from '../../../../../../../services/offres.service';

@Component({
  selector: 'app-modal-postuler-interim',
  templateUrl: './modal-postuler-interim.component.html',
  styleUrls: ['./modal-postuler-interim.component.css']
})
export class ModalPostulerInterimComponent implements OnInit, AfterViewInit {
  mission_choisie: any = [];
  pageSizeOptions: number[] = [12, 25, 50, 100];
  dataSourceModal: MatTableDataSource<any[]> = new MatTableDataSource();
  @ViewChild(MatSort, {static: false}) sortModal: MatSort;
  @ViewChild('listPaginatorModal', {static: false}) paginatorModal: MatPaginator;
  enabled_candidature = false;
  selection = new SelectionModel<any[]>(true, []);
  displayedColumnsModal = ['number', 'select', 'date', 'libelle', 'debut', 'fin', 'tarif'];
  lengthModal = 0;
  plannings = [];
  length = 0;
  enable = false;
  send_mail = true;
  plannings_for_modal = [];
  condition_medecin = '';
  id_interim = 0;

  data_ = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null


  };

  constructor(
    private candidatureService: CandidatureService,
    private notifierService: NotificationService,
    private offreService: OffresServiceAll,
    public dialogRef: MatDialogRef<ModalPostulerInterimComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.mission_choisie = this.data.mission_choisie[0];
    this.id_interim = this.data.id_interim;
    this.dataSourceModal.data = this.data.plannings_;
    this.plannings_for_modal = this.data.plannings_;
    this.enable = this.data.enable;
    this.dataSourceModal.sort = this.sortModal;
    this.dataSourceModal.paginator = this.paginatorModal;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceModal.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceModal.data.length;
    return numSelected === numRows;
  }

  clearSelection() {
    this.selection = new SelectionModel<any[]>(true, []);
  }

  onPostuler() {
    this.notifierService.blockUiStart();
    const id_missionAndInterim = [];
    let showAlert = [];
    id_missionAndInterim[0] = this.id_interim;
    id_missionAndInterim[1] = this.mission_choisie.id_mission;
    let tmp = [];
    this.selection.selected.forEach((element: any) => {
      tmp.push(element.idPlan);
    });
    this.plannings = tmp;
    this.plannings['interim_id'] = this.id_interim;
    this.plannings['mission_id'] = this.mission_choisie.id_mission;

    this.candidatureService.create_candidature_medecin(this.enabled_candidature, this.condition_medecin, this.send_mail, JSON.stringify(this.plannings), JSON.stringify(id_missionAndInterim)).subscribe(
      (next: any) => {
        if (next.erreur) {
          this.notifierService.showNotificationEchec(next.erreur);
        } else {
          this.dataSourceModal = new MatTableDataSource([]);
          this.notifierService.showNotificationSuccessCopie('topCenter', next.success);
          this.offreService.getJsonOffres(this.id_interim, this.data_).subscribe(
            (response: any) => {
              this.notifierService.blockUiStop();
              if (!response.erreur) {
                this.offreService.listeDesOffresSubject.next(response.data);
              } else {
                this.notifierService.showNotificationEchec(response.erreur);
              }
            }

          );
          this.notifierService.blockUiStop();
        }
        // ('ok');

      },
      (error2) => {
        this.notifierService.blockUiStop();
        // showAlert[0] = "alert alert-success alert-dismissible text-center fade show col-12";
        //  showAlert[1] = "Candidature effectuée avec success";
        //this.notifierService.showNotificationSuccessCopie('topCenter', "Candidature effectuée avec success");
        // this.interimService.showAlertSubjectInterim.next(showAlert);
        // this.interimService.labelTab.next(5);
        // this.interimService.loader.next(true);
        // this.router.navigate(['/RMS-Admin/redirectionMed/', this.interim_id, 'interim']);
      }
    );
  }
  ngAfterViewInit(): void {
    this.dataSourceModal.sort = this.sortModal;
    this.dataSourceModal.paginator = this.paginatorModal;
  }
}
