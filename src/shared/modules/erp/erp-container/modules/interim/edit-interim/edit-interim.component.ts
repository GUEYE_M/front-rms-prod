import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { from, Observable, BehaviorSubject } from 'rxjs';
import { map, startWith, filter, retry } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { NotificationService } from 'src/shared/services/notification.service';
import { Commune } from '../../../../../../models/commune.model';
import { Interim } from '../../../../../../models/interim/Interim.model';
import { UserService } from '../../../../../../services/user.service';
import { InterimService } from '../../../../../../services/interim/interim.service';
import { AddresseService } from '../../../../../../services/addresse.service';
import { AuthNormalService } from '../../../../../../services/authNormal.service';
import { ApiVilleService } from '../../../../../../services/api_ville_service';
import { SpecialiteService } from '../../../../../../services/specialite.service';
import { UtilsService } from '../../../../../../services/utils.service';
import { PldService } from '../../../../../../services/pld.service';
import { RessourceService } from 'src/shared/services/erp/ressource.service';
import { RemarqueService } from 'src/shared/services/erp/remarque.service';
import { AddIdPldManuelComponent } from '../../missions-interimaires/add-id-pld-manuel/add-id-pld-manuel.component';
import {DatePipe} from '@angular/common';

@Component({
	selector: 'app-edit-interim',
	templateUrl: './edit-interim.component.html',
	styleUrls: [ './edit-interim.component.css' ]
})
export class EditInterimComponent implements OnInit {
	interimForm: FormGroup;
	communes: any[];
	countries: any[];
	// specialites: any[];
	color = 'primary';
	mode = 'indeterminate';
	value = 50;
	loader = false;
	specialites = [];
	qualifications = [];
	edit = false;
	idInterimPld = null;
	selection = new SelectionModel<any[]>(true, []);
	filteredCommune: Observable<any[]>;
	interim: any;
	choixCheckBox = [];
	response: any;
	sources_interim: any = [];
	remarques: any = [];
	filterCountriesPays : Observable<any[]>
	filterCountrieNatianalite : Observable<any[]>
	dialogRefAddidPldContrat: MatDialogRef<AddIdPldManuelComponent>;
	joignabilites: any = [
		{
			name: 'Matin',
			id: 1
		},

		{
			name: 'Midi',
			id: 2
		},
		{
			name: 'Soir',
			id: 3
		}
	];
	mail: boolean = true;
	actif_champ: boolean = true;

	myFilter = (d: Date): boolean => {
		const day = d.getDay();
		// Prevent Saturday and Sunday from being selected.
		return day !== 0 && day !== 6;
	};

	// qualifications = new FormControl();
	constructor(
		private fb: FormBuilder,
		private userService: UserService,
		private interimService: InterimService,
		private adresseService: AddresseService,
		private utilsService: UtilsService,
		private router: Router,
		private dialog: MatDialog,
		private authNormalService: AuthNormalService,
		private apiVilleService: ApiVilleService,
		public route: ActivatedRoute,
		private specialiteService: SpecialiteService,
		private notificationService: NotificationService,
		private pldService: PldService,
		private dialogRef: MatDialogRef<EditInterimComponent>,
		private ressourceService: RessourceService,
		private remarqueService: RemarqueService,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any
	) {
		if (this.data) this.specialites = this.data.specialites;
		if (this.data.id) {
			this.notificationService.blockUiStart();
			this.edit = true;
			this.mail = false;
			this.interimService.find(+this.data.id).pipe(retry(3)).subscribe((interim) => {
				this.response = interim;
        this.idInterimPld = this.response.idPld;
				this.interim = interim['interim'];
				this.initform(this.interim);

				this.interimForm.patchValue({
					id: this.data.id,
					source: interim['source']
				});

				this.notificationService.blockUiStop();
			});
		} else {
			this.initform();
		}
	}
	ngOnInit() {

	
		this.allSpecialite();
		this.allSources();
		this.allRemarque();
	}
	compareWithFunc(a, b) {
		if (a.id === b.id) {
			return a;
		}
	}

	compareWithFuncHoraire(a, b) {
		if (a === b) {
			return a;
		}
	}
	compareWithFunc_Remarque(a, b) {
		if (a == b) {
			return a;
		}
	}
	initform(
		interim = {
			user: {
				nom: '',
				prenom: '',
				telephone: '',
				description: '',
				remarque: '',
				username: '',
				email: '',
				civilite: '',
				adresse: '',
				complement_adresse: '',
				cp: '',
				ville: '',
				numSecu: '',
				auteur: '',
				region: '',
				departement: '',
				lieuNaissance: '',
				dateNaissance: null,
				situationFamiliale:null,
				nomNaissance:'',
				nationalite:'',
				paysNaissance:'',
				idPaysNaissance: '',
				idNationalite: '',
				nbEnfants:0
			},
			actif: false,
			numRPPS: '',
			inscription: '',
			idReglementAcompte: 1,
			idReglement: 1,
			pcs: '344a',
			idMoyenLocomotion: 1,
			statut: '',
			remarque: '',
			autre_remarques: '',
			coefIK: '',
			rayonIntervention: '',
			preferenceDeplacement: '',
			preferenceContact: '',
			horaire_joignabilite: '',
		
			
			source: '',
			tabQualif: '',
      cible: false,
      banqueIBAN: '',
      banqueBIC: '',
      banqueDomiciliation: '',
      banqueIBANPaie: '',
      banqueBICPaie: '',
      estBulletinMail: 'O',
      idCategoriePaie: 2,
		}
	) {
		this.interimForm = this.fb.group({
			user: this.fb.group({
				adresse: [ interim.user.adresse, Validators.required ],
				complement_adresse: [ interim.user.complement_adresse ],
				cp: [ interim.user.cp, Validators.required ],
				region: [ interim.user.region ],
				departement: [ interim.user.departement ],
				ville: [ interim.user.ville, Validators.required ],
				civilite: [ interim.user.civilite, Validators.required ],
				nom: [ interim.user.nom, Validators.required ],
				prenom: [ interim.user.prenom, Validators.required ],
				telephone: [ interim.user.telephone, Validators.required ],
				remarque: [ interim.user.remarque ],
				auteur: [],
				description: [ interim.user.description ],
				username: [ interim.user.username, Validators.required ],
				numSecu: [ interim.user.numSecu ],
				email: [ interim.user.email, [ Validators.required, Validators.email ] ],
				lieuNaissance: [ interim.user.lieuNaissance ],
				dateNaissance: [ interim.user['dateNaissance'] ],
				idNationalite: [ interim.user['idNationalite'] ],
				situationFamiliale: [ interim.user.situationFamiliale ],
				nomNaissance: [ interim.user.nomNaissance ],
				nationalite: [ interim.user.nationalite ],
				paysNaissance: [ interim.user.paysNaissance ],
				idPaysNaissance:[interim.user.idPaysNaissance],
				nbEnfants:[interim.user.nbEnfants]
			}),
			actif: [ interim.actif ],
			numRPPS: [ interim.numRPPS ],
			coefIK: [ interim.coefIK ],
			inscription: [ interim.inscription ],
			statut: [ interim.statut ],
			idReglementAcompte: [ 1 ],
			idReglement: [ interim.idReglement ],
			pcs: [ interim.pcs ],
			idMoyenLocomotion: [ interim.idMoyenLocomotion ],
			remarque: [ interim.remarque ],
			autre_remarques: [ interim.autre_remarques ],
			rayonIntervention: [ interim.rayonIntervention ],
			preferenceDeplacement: [ interim.preferenceDeplacement ],
			preferenceContact: [ interim.preferenceContact ],
			horaire_joignabilite: [ interim.horaire_joignabilite ],
			source: [ interim.source ],
			tabQualif: [],
      banqueIBAN: [ interim.banqueIBAN ],
      banqueBIC: [ interim.banqueBIC ],
      banqueDomiciliation: [ interim.banqueDomiciliation ],
      banqueIBANPaie: [ interim.banqueIBANPaie ],
      banqueBICPaie: [ interim.banqueBICPaie ],
      estBulletinMail: [ interim.estBulletinMail ],
      idCategoriePaie: [ interim.idCategoriePaie ],
			cible: [ interim.cible ],
			id: [],
			mail: []
		});
	}
	onCommune(commune: string) {
		this.apiVilleService.findbyCommune(commune).subscribe((response: any) => {
			//(response)
			if (response) {
				this.communes = response.data;
				this.filteredCommune = this.interimForm
					.get('user')
					.get('ville')
					.valueChanges.pipe(
						startWith(''),
						map((value) =>
							this.communes.filter((commune) => commune.properties.label.toLowerCase().includes(value))
						)
					);
			}
		});
	}

	compareWith(a, b) {

		return +a === +b;
	
	  }

	onPays(name:string){
		    console.log(name)
		this.apiVilleService.getCountriesByName(name).subscribe((response: any) => {
			//(response)
			if (response) {
				this.countries = response
					this.filterCountriesPays = this.interimForm
					.get('user')
					.get("paysNaissance")
					.valueChanges.pipe(
						startWith(''),
						map((value) =>
						response.filter((countrie) => countrie.translations.fra.common.toLowerCase().includes(value))
						)
					);
				 
			}
		});

	}

	onNationalite(name: string) {
       
		 
		    
		this.apiVilleService.getCountriesByName(name).subscribe((response: any) => {
			//(response)
			if (response) {
				this.countries = response
					this.filterCountrieNatianalite = this.interimForm
					.get('user')
					.get("nationalite")
					.valueChanges.pipe(
						startWith(''),
						map((value) =>
						response.filter((countrie) => countrie.demonyms.fra.f.toLowerCase().includes(value))
						)
					);
				 

				
			}
		});

		

	}

	


	onblurCountrie(){

		if (this.countries.length === 0) {
		
			('valeure incorecte');
		} else {
			this.countries.filter((c) => {
			      
				if (c.translations.fra.common === this.interimForm.get('user').get("paysNaissance").value) {
					this.interimForm.get('user').get('idPaysNaissance').setValue(c.cca2);
					
				}
				if (c.demonyms.fra.f === this.interimForm.get('user').get("nationalite").value) {
					this.interimForm.get('user').get('idNationalite').setValue(c.cca2);
					
				}
			});
		}
         
	}


	cibleAction() {
		if (this.interimForm.get('cible').value) {
			this.interimForm.get('actif').setValue(false);
			this.actif_champ = false;
			this.mail = false;
		} else {
			this.actif_champ = true;
			this.mail = true;
		}
	}
	onblurCommune() {
		if (this.communes.length === 0) {
			this.interimForm.get('user').get('ville').reset();
			this.interimForm.get('user').get('cp').reset();
			('ville incorecte');
		} else {
			this.communes.filter((c) => {
				let donne = c.properties.context.split(',');
				let dept = donne[0] + ' , ' + donne[1];
				if (c.properties.label === this.interimForm.get('user').get('ville').value) {
					this.interimForm.get('user').get('cp').setValue(c.properties.postcode);
					this.interimForm.get('user').get('region').setValue(donne[2]);
					this.interimForm.get('user').get('departement').setValue(dept);
				}
			});
		}
	}

	allSpecialite() {
		this.specialiteService.list(null, true).subscribe((next) => {
			this.specialites = next.data;
		});
	}
	saveInterim() {
		if (this.interimForm.valid) {
			this.notificationService.blockUiStart();
			if (!this.edit) {
				this.interimForm.get('user').get('auteur').setValue(this.userService.getCurrentUser()['username']);
			}
			this.interimForm.patchValue({
        user: {
          dateNaissance: this.datePipe.transform(this.interimForm.value.user.dateNaissance, 'yyyy-MM-dd')
        }
      });
			this.interimForm.get('mail').setValue(this.mail);
			this.authNormalService.signupInterimErp(this.interimForm.value).subscribe(
				(response: any) => {
					this.notificationService.blockUiStop();
					if (response['erreur']) {
						this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
					} else {
            this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
            this.pldService.addInterimaire(response.id).subscribe((repPld) => {
              repPld = JSON.parse(repPld);
              if (repPld.Code === 200) {
                this.notificationService.showNotificationSuccessCopie('bottomRight', 'ID ' + repPld.ID + ' ' + repPld.Message);
                location.reload();
              } else {
                this.notificationService.showNotificationSuccessCopie('bottomRight', repPld.Message);
              }
            });
					}
				},
				(erreur) => {
					this.notificationService.showNotificationEchec(
						'erreur  de connexion , contacter le service IT si le probleme persiste'
					);
					this.notificationService.blockUiStop();
				},
				() => {
					this.notificationService.blockUiStop();
				}
			);
		} else {
			this.notificationService.showNotificationEchec(
				'Formulaire invalide! veillez renseigner les champs obligatoires'
			);
			this.notificationService.blockUiStop();
		}
	}
	elementsPld(data) {
		let rep = 0;
		const keys = Object.keys(data);
		const keysNull = [];
		keys.forEach(function(element) {
			if (element === 'user') {
				const keysUser = Object.keys(data[element]);
				keysUser.forEach(function(item) {
					if (
						item === 'civilite' ||
						item === 'nom' ||
						item === 'prenom' ||
						item === 'email' ||
						item === 'cp' ||
						item === 'telephone' ||
						item === 'adresse' ||
						item === 'complement_adresse' ||
						item === 'ville' ||
						item === 'dateNaissance' ||
						item === 'lieuNaissance' ||
						item === 'idPaysNaissance' ||
						item === 'idNationalite' ||
						item === 'numSecu'
					) {
						if (!data[element]) rep += 1;
					}
				});
			}
			if (
				element === 'idReglementAcompte' ||
				element === 'idReglement' ||
				element === 'pcs' ||
				element === 'idMoyenLocomotion'
			) {
				if (!data[element]) rep += 1;
			}
		});
		return rep;
	}

	allSources() {
		const data = {
			filter: null
		};
		this.ressourceService.list(data).subscribe((response: any) => {
			if (!response.erreur) {
				this.sources_interim = response.data;
			}
		});
	}

	allRemarque() {
		const data = {
			filter: {
				type: false
			}
		};
		this.remarqueService.list(data).subscribe((response: any) => {
			if (!response.erreur) {
				this.remarques = response.data;
			}
		});
	}

	addIdPldContrat() {
		this.dialogRefAddidPldContrat = this.dialog.open(AddIdPldManuelComponent, {
			panelClass: 'myapp-no-padding-dialog',
			data: { id: this.data.id, user: 'interim' },
			width: '20%',
			height: '30%'
		});
	}
	onChoiseSpecialite(event) {
		this.qualifications = [];
		this.specialites.filter((specialite) => {
			if (specialite.id === +event.value) {
				this.choixCheckBox = [];
				this.qualifications = specialite.qualifications;
			}
		});
	}
	onChoiseQualification() {
		this.interimForm.get('tabQualif').setValue(this.choixCheckBox);
	}
}
