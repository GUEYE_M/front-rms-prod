import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInterimComponent } from './edit-interim.component';

describe('EditInterimComponent', () => {
  let component: EditInterimComponent;
  let fixture: ComponentFixture<EditInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
