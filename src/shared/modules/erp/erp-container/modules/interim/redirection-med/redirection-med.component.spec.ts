import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectionMedComponent } from './redirection-med.component';

describe('RedirectionMedComponent', () => {
  let component: RedirectionMedComponent;
  let fixture: ComponentFixture<RedirectionMedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectionMedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectionMedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
