import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectionInterimComponent } from './prospection-interim.component';

describe('ProspectionInterimComponent', () => {
  let component: ProspectionInterimComponent;
  let fixture: ComponentFixture<ProspectionInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectionInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectionInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
