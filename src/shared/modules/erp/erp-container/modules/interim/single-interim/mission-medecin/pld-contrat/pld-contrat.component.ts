import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
  MatChipInputEvent,
  MatDialog,
  MatSort, MatTableDataSource
} from '@angular/material';
import { ContratPldModel } from '../../../../../../../../models/interim/ContratPld.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { InterimService } from '../../../../../../../../services/interim/interim.service';
import { RolesService } from '../../../../../../../../services/roles.service';
import { UserService } from '../../../../../../../../services/user.service';
import { Router } from '@angular/router';
import { CandidatureService } from '../../../../../../../../services/candidature.service';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { PldService } from '../../../../../../../../services/pld.service';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { UtilsService } from '../../../../../../../../services/utils.service';
import { DatePipe } from '@angular/common';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
	selector: 'app-pld-contrat',
	templateUrl: './pld-contrat.component.html',
	styleUrls: [ './pld-contrat.component.css' ]
})
export class PldContratComponent implements OnInit {
	form: FormGroup;
	title: string;
	isUpdateTauxHoraire = false;
	labelTab = 7;
	contratByDates = false;
	tabIdCandidatures = [];
	caracteristiques = [];
	idClientPld: any;
	idInterimPld: any;
	tabPrime = [];
	ReferenceMM: any;
	erreurPld: any;
	newTabPrime = [];
	interim: any;
	salaireNet = 0;
	refMission: any;
	client: any;
  contratPld: any;

  candidatures: any;
  displayedColumns = [
    'number',
    'select',
    'date',
    'vacation',
    'debut',
    'fin',
    'salaireNet',
    'salaireBrute',
    'tarif',
  ];
  selection = new SelectionModel<any[]>(true, []);


	dateDebutPld: any;
	dateFinPld: any;
	dateDebut: any;
	currentUser: any;
	dateFin: any;
	heureDebut: any;
	heureFin: any;
	heureDebutPld: any;
	heureFinPld: any;
	specialite: any;
	tarif: any;
	txtreference: any;
	jtc: any;
	pcs = '344a';
	adresseClient: any;
	cpClient: any;
	villeCLient: any;
	interlocutaireClient: any;
  primesDuContrat: string[];

	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ ENTER, COMMA ];
	designationCtrl = new FormControl();
	filteredDesignationPrimes: Observable<string[]>;
	designationPrimes: string[] = [];
	allDesignationPrimes: string[] = [];

	/////////////////////////////////////////
	idClient: number;
	idInterim: number;

	@ViewChild('designationInput', { static: false })
	designationInput: ElementRef<HTMLInputElement>;
	@ViewChild('chipList', { static: false })
	chipList: any;
	@ViewChild('auto', { static: false })
	matAutocomplete: MatAutocomplete;
	constructor(
		private pldService: PldService,
		private fb: FormBuilder,
    public datepipe: DatePipe,
    private notificationService: NotificationService,
		private candidatureService: CandidatureService,
		private router: Router,
		private userService: UserService,
		private roleService: RolesService,
		private interimService: InterimService,
		private dialog: MatDialog,
		private datePipe: DatePipe,
		private utilsService: UtilsService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		this.filteredDesignationPrimes = this.designationCtrl.valueChanges.pipe(
			startWith(null),
			map((prime: string | null) => (prime ? this._filter(prime) : this.allDesignationPrimes.slice()))
		);
	}

	ngOnInit() {
    this.caracteristiquesPoste();
    if(this.data.idContratPld){
      this.notificationService.blockUiStart();
      this.updateContrat();
   }
	 else{
	   this.createContrat();
   }
    this.primesPldClientById();
	}
	updateContrat(){
	  this.title = "Mise à jour du contrat: " + this.data.idContratPld;
    this.candidatureByIdMission(this.data.idMM, this.data.interimId);
    this.getContratPld(this.data.idContratPld);
  }
	createContrat(){
    this.title = "Création du contrat ";
    this.initForm(null);
    this.currentUser = this.userService.getCurrentUser();
    this.candidatures = this.data.candidatures;
    if (this.candidatures.length > 0) {
      // on recuper l'heure de pour le contrat quon va generer sur notre ERP
      this.heureDebut = this.candidatures[0].heure_debut;
      const heureDebutArray = this.candidatures[0].heure_debut.split(':');
      this.heureDebutPld = heureDebutArray[0] + heureDebutArray[1];

      this.heureFin = this.candidatures[0].heure_fin;
      const heureFinArray = this.candidatures[0].heure_fin.split(':');
      this.heureFinPld = heureFinArray[0] + heureFinArray[1];

      this.idClientPld = this.candidatures[0].idClientPld;
      this.idInterimPld = this.candidatures[0].idInterimPld;
      this.jtc = this.candidatures[0].jtc;
      this.adresseClient = this.candidatures[0].clientAdresse;
      this.villeCLient = this.candidatures[0].clientVille;
      this.cpClient = this.candidatures[0].clientCp;
      this.interlocutaireClient = this.candidatures[0].interlocutaire;
    }
    this.idClient = this.data.clientId;
    this.idInterim = this.data.interimId;
    this.ReferenceMM = this.data.refMission;
    this.specialite = this.data.specialite;
	}
  getContratPld(idpld) {
    this.pldService.findContrat(idpld).subscribe((response) => {
        this.notificationService.blockUiStop();
        response = JSON.parse(response);
        this.contratPld = response;
        const dateDebut = this.contratPld ? this.contratPld.DateDebut.split('', ) : [];
        const dateFin = this.contratPld ? this.contratPld.DateFin.split('') : [];
        if(dateDebut.length > 0 && dateFin.length > 0){

          this.title += " pour la période du (" + this.contratPld.DateDebut + "-" + this.contratPld.DateFin + ")";
        }
        this.idClientPld = response.IdClient;
        this.tabPrimeOldPld(response.TabPrimes);
        this.initForm(this.contratPld);
      },
      error => {
        this.notificationService.blockUiStop();
      });
  }
  tabPrimeOldPld(primes) {
    const tabPrimes = [];
    const thiss = this;
    primes.forEach(function(element) {
      tabPrimes.push( element['TxtPrime']);
    });
    this.primesDuContrat = tabPrimes;
  }
  caracteristiquesPoste(){
    this.pldService.caracteristiquesPostePldSubject.subscribe((caracteristiques: any) => {
      this.caracteristiques = caracteristiques;
    });
  }
  candidatureByIdMission(idMM, idInterim) {
    this.notificationService.blockUiStart();
    this.candidatureService.getCandidaturesByIdMM(idMM, idInterim).subscribe((response) => {
        this.notificationService.blockUiStop();
        if (response['data']) {
          this.candidatures = new MatTableDataSource(response['data']);
          // this.returnDateAndTimeByCandidature(this.candidatures);
        }
      },
      error => {
        this.notificationService.blockUiStop();
      });
  }
	primesPldClientById(){
    this.pldService.prmesPldByClientSubject.subscribe(
      (response: any) => {
        Object.values(response).forEach((element: any) => {
          this.tabPrime.push(element);
          this.allDesignationPrimes.push(element.Designation);
        });
      },
    );
  }
	initForm(contratPld) {
	  if(contratPld){
      this.form = this.fb.group(
        {
          recours: [contratPld.TxtRecours,  Validators.required],
          NbHeureHebdo: [contratPld.NbHeureHebdo,  Validators.required],
          justificatif: ['Recherche candidat pour le poste'],
          salaireReference: [contratPld.SalaireReference],
          heureDebut: [contratPld.HoraireAMDebut],
          heureFin: [contratPld.HoraireAMFin],
          nbJoursEssai: [contratPld.NbJoursEssai],
          horaires: [contratPld.Horaires ],
          txtConsignePoste: [contratPld.TxtConsignePoste ],
          caracteristiquePoste: [ '' ],
          moyenAcces: [contratPld.MoyenAcces]
        });
    } else {
      this.form = this.fb.group({
        recours: [ '', Validators.required ],
        NbHeureHebdo: [ '35', Validators.required ],
        justificatif: [ 'Recherche candidat pour le poste' ],
        salaireReference: [ '' ],
        heureDebut: [ '' ],
        heureFin: [ '' ],
        nbJoursEssai: [ '2' ],
        horaires: [ '' ],
        txtConsignePoste: [ '' ],
        caracteristiquePoste: [ '' ],
        moyenAcces: [ '' ]
      });
    }
	}
	onSave() {
    this.returnDateAndTimeByCandidature(this.candidatures);
    let tabIdCandidaturesForUpdate: any;
    const recours = this.form.value.recours.split(',').map((n) => {
      return n;
    });

    if(this.data.idContratPld){
      this.contratPld.TabPrimes =  Array.from(new Set(this.returnPrimeContrat(this.selection.selected)));
      this.returnSalairenet(this.selection.selected);
      const tabIdCandidatures = [];
      this.selection.selected.forEach((element) => {
        tabIdCandidatures.push(element['id']);
      });
      tabIdCandidaturesForUpdate = {candidatureIds: Array.from(new Set(tabIdCandidatures)), contrat : this.contratPld , primes : this.returnPrimeContrat(this.selection.selected)};
      tabIdCandidaturesForUpdate.contrat.HoraireAMDebut = this.heureDebutPld;
      tabIdCandidaturesForUpdate.contrat.HoraireAMFin = this.heureFinPld;
      this.notificationService.blockUiStop();
    }
    else {
      const contratPld = new ContratPldModel(
        this.idClientPld,
        this.idInterimPld,
        this.idInterimPld,
        this.currentUser.gestionnaire.IdReferentiel,
        1,
        1,
        this.dateDebutPld,
        this.dateFinPld,
        this.dateDebutPld,
        this.dateFinPld,
        +recours[0],
        recours[1],
        this.form.value.justificatif,
        this.form.value.caracteristiquePoste,
        this.specialite,
        this.pcs,
        1,
        1,
        this.cpClient,
        this.villeCLient,
        this.adresseClient,
        this.cpClient,
        this.interlocutaireClient,
        this.form.value.nbJoursEssai,
        Number(this.form.value.NbHeureHebdo),
        this.form.value.horaires,
        this.heureDebutPld,
        this.heureFinPld,
        '',
        '',
        this.form.value.moyenAcces,
        '',
        this.form.value.salaireReference,
        'Salaire net avant Impôt: ' + this.salaireNet + '€',
        '',
        Array.from(new Set(this.returnPrimeContrat(this.candidatures))),
        []
      );
      this.candidatures.forEach((element: any) => {
        this.tabIdCandidatures.push(element.idCandidature);
      });
      this.returnSalairenet(this.candidatures);
      tabIdCandidaturesForUpdate = {
        candidatureIds: Array.from(new Set(this.tabIdCandidatures)),
        contrat: contratPld,
        primes: Array.from(new Set(this.returnPrimeContrat(this.candidatures)))
      };
      this.notificationService.blockUiStop();
    }
    this.savePld(tabIdCandidaturesForUpdate);
  }

	savePld(contratData) {
    const heureDebutArray = this.form.value.heureDebut ? this.form.value.heureDebut.split(':') : [];
    const heureFinArray = this.form.value.heureFin ? this.form.value.heureFin.split(':') : [];
		this.notificationService.blockUiStart();
    contratData.contrat.TxtJustificatif = this.form.value.justificatif;
    contratData.contrat.NbJoursEssai = this.form.value.nbJoursEssai;
    contratData.contrat.MoyenAcces = this.form.value.moyenAcces;
    contratData.contrat.Horaires = this.form.value.horaires;
    contratData.contrat.TxtCaracteristiquePoste =	this.form.value.caracteristiquePoste;
    contratData.contrat.TxtConsignePoste =	this.form.value.txtConsignePoste;
    contratData.contrat.TxtConsigneRemuneration =	'Salaire net avant Impôt: ' + this.salaireNet + '€';
    if(this.isUpdateTauxHoraire){
      heureDebutArray.length > 0 ? contratData.contrat.HoraireAMDebut = heureDebutArray[0] + heureDebutArray[1] : null;
      heureFinArray.length > 0 ? contratData.contrat.HoraireAMFin = heureFinArray[0] + heureFinArray[1] : null;
    }
		this.pldService.creerContrat(contratData).subscribe(
			(resp) => {
				this.notificationService.showNotificationSuccessCopie('bottomRight', resp.body);
				this.dialog.closeAll();
				this.notificationService.blockUiStop();
				this.utilsService.redirectEspace({ espace: 'missionMedecinListe', id: null });
			},
			(error) => {
				this.notificationService.showNotificationEchecCopie(
					'bottomRight',
					error.error.message
				);
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
				this.utilsService.redirectEspace({ espace: 'missionMedecinListe', id: null });
			}
		);
	}
	returnSalairenet(candidatures){
    for (let i = 0; i < candidatures.length; i++) {
      this.salaireNet += +candidatures[i].salaireNet;
    }
  }

	returnPrimeContrat(candidatures){
    console.log(candidatures)
    let newDesignationPrime: any;
    const tabPrime = this.tabPrime;
    let newTabPrime = [];
    this.designationPrimes = Array.from(new Set(this.designationPrimes));
    if (this.designationPrimes.length) {
      const tabTypeDeGardeCandidatures = [];

      this.designationPrimes.forEach((primeSelectionnee) => {
        const modelPrime = { IdPrime: '', TxtPrime: '', LibPrimeComplement: '', TauxReference: 0, JTC: '', TauxPayeImposable: 0, TauxPayeNonImposable: 0, TauxFacture: 0 };
        tabPrime.forEach((element: any) => {
          newDesignationPrime = element.Designation;
          if (newDesignationPrime.trim() === primeSelectionnee.trim()) {
            for (let i = 0; i < candidatures.length; i++) {
              let vacation =  candidatures[i]['abreviation'] ? candidatures[i]['abreviation'] : candidatures[i].designation;
          
              const designationCandidature = candidatures[i].designation ? vacation.toLowerCase().normalize('NFD') + '-' + candidatures[i].abreviationSpecialite.toLowerCase().normalize('NFD') : vacation.toLowerCase().normalize('NFD') + '-' + candidatures[i].abreviationSpecialite.toLowerCase().normalize('NFD');
              const designation = element.Designation.toLowerCase().normalize('NFD');

              if (designation.includes(designationCandidature)) {
                tabTypeDeGardeCandidatures.push(vacation.trim() + ' ' + candidatures[i].abreviationSpecialite.trim()
                );
                modelPrime.TauxReference = candidatures[i].txtreference;
                modelPrime.TauxPayeImposable = candidatures[i].salaireBrute;
                modelPrime.TauxFacture = candidatures[i].tarif;
                modelPrime.IdPrime = element.IdPrime;
                modelPrime.TxtPrime = newDesignationPrime;
                modelPrime.JTC = this.jtc;
                modelPrime.TauxPayeNonImposable = 0;
                newTabPrime.push(modelPrime);
              }
            }
            if (!tabTypeDeGardeCandidatures.includes(newDesignationPrime.trim()) && this.designationPrimes.includes(newDesignationPrime.trim())) {
              modelPrime.IdPrime = element.IdPrime;
              modelPrime.JTC = element.JTC;
              modelPrime.TxtPrime = newDesignationPrime;
              modelPrime.TauxPayeNonImposable = 0;
              newTabPrime.push(modelPrime);
            }
          }
        });
      });
    }
    return newTabPrime;
  }
  returnDateAndTimeByCandidature(candidatures){
	  if(candidatures.length > 0){
      let candidatureDate = this.data.idContratPld ? candidatures[0].date.date : new Date(candidatures[0].date) ;
      for (let i = 0; i < candidatures.length; i++) {
        let candidatureDate = this.data.idContratPld ? candidatures[i].date.date : new Date(candidatures[i].date) ;
        if (i === 0) {
          // on initialise la dconsoleate de debut et la date de fin de la candidature
          this.dateDebut = candidatureDate;
          this.dateFin = candidatureDate;

          // on recuper l'heure de pour le contrat quon va generer sur notre ERP
          this.heureDebut = candidatures[i].heure_debut;
          const heureDebutArray = candidatures[i].heure_debut.split(':');
          this.heureDebutPld = heureDebutArray[0] + heureDebutArray[1];

          this.heureFin = candidatures[i].heure_fin;
          const heureFinArray = candidatures[i].heure_fin.split(':');
          this.heureFinPld = heureFinArray[0] + heureFinArray[1];
        } else {
          // a ce niveau on flitre pour recuperer le dernier jour (candidature) de la mission
          if (this.dateFin < candidatureDate) this.dateFin = candidatureDate;
          // a ce niveau on flitre pour recuperer le premier jour (candidature) de la mission
          if (this.dateDebut > candidatureDate) this.dateDebut = candidatureDate;
        }
      }
      this.dateDebutPld = this.datePipe.transform(this.dateDebut, 'yyyyMMdd');
      this.dateFinPld = this.datePipe.transform(this.dateFin, 'yyyyMMdd');
    }
  }
	add(event: MatChipInputEvent): void {
		// Add fruit only when MatAutocomplete is not open
		// To make sure this does not conflict with OptionSelected Event
		if (!this.matAutocomplete.isOpen) {
			const input = event.input;
			const value = event.value;

			// Add our fruit
			if ((value || '').trim()) {
				this.designationPrimes.push(value.trim());
			}

			// Reset the input value
			if (input) {
				input.value = '';
			}

			this.designationCtrl.setValue(null);
		}
	}

	remove(fruit: string): void {
		const index = this.designationPrimes.indexOf(fruit);

		if (index >= 0) {
			this.designationPrimes.splice(index, 1);
		}
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		this.designationPrimes.push(event.option.viewValue);
		this.designationInput.nativeElement.value = '';
		this.designationCtrl.setValue(null);
	}

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
		return this.allDesignationPrimes.filter((prime) => prime.toLowerCase().indexOf(filterValue) === 0);
	}
	updateTauxHoraire() {
		this.isUpdateTauxHoraire = !this.isUpdateTauxHoraire;
	}
	suppIdentifantPrime(item) {
		let response = 'existe pas';
		if (item.includes('0000000')) response = item.replace('0000000', '');
		if (item.includes(this.idClientPld)) response = item.replace(this.idClientPld, '');
		return response;
	}

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.candidatures.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.candidatures.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  oncontratByDates(etat){
    if(etat){
      this.contratByDates = !this.contratByDates;
    }
    if(this.contratByDates){
      const consignesPosteArray = [];
      const candidatures = this.data.idContratPld ? this.selection.selected : this.candidatures;
      for (let i = 0; i < candidatures.length; i++) {
        if(this.contratByDates){
          const date = candidatures[i]['date'].date ? this.datePipe.transform(candidatures[i]['date'].date, 'dd') : candidatures[i]['date'].split('-')[2] ;
          const vacation = candidatures[i]['vacation'] ? candidatures[i]['vacation'] : candidatures[i]['designation'];
          const heureDebut = candidatures[i]['heure_debut'].split(':');
          const heureFin = candidatures[i]['heure_fin'].split(':');
          if(i === 0){
            consignesPosteArray.push(vacation + ' (' + heureDebut[0] +':'+ heureDebut[1] + ' - ' + heureFin[0] +':'+ heureFin[1] + ') -- le(s) ' + date+ ', ');
          }
          else {
            let existe = false;
            consignesPosteArray.forEach((el, index:number) => {
              if(el.includes(vacation)){
                el += date + ', ';
                existe = true;
                consignesPosteArray[index] = el;
              }
            });
            if(!existe){
              consignesPosteArray.push(vacation + ' (' + heureDebut[0] +':'+ heureDebut[1] + ' - ' + heureFin[0] +':'+ heureFin[1] + ') -- pour le(s) ' + date+ ', ');
            }
          }
        }
      }
      this.form.patchValue({txtConsignePoste: consignesPosteArray.join('; ')});
      this.form.patchValue({horaires: ' '});

    } else {
      this.form.patchValue({txtConsignePoste: ' '});
    }
  }
}
