import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionMedecinComponent } from './mission-medecin.component';

describe('MissionMedecinComponent', () => {
  let component: MissionMedecinComponent;
  let fixture: ComponentFixture<MissionMedecinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionMedecinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionMedecinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
