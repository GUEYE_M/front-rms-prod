import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCandidaturesComponent } from './modal-candidatures.component';

describe('ModalCandidaturesComponent', () => {
  let component: ModalCandidaturesComponent;
  let fixture: ComponentFixture<ModalCandidaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCandidaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCandidaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
