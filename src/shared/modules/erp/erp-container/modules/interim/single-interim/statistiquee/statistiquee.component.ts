import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from 'src/shared/services/notification.service';
import { DatePipe } from '@angular/common';
import { InterimService } from 'src/shared/services/interim/interim.service';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from 'src/shared/services/utils.service';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { UserService } from '../../../../../../../services/user.service';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { RolesService } from "../../../../../../../services/roles.service";
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { DataModel, ChartModel, TypeChartModel } from 'src/shared/chart.model';
import { SpecialiteService } from 'src/shared/services/specialite.service';
moment.locale('fr', localization);

@Component({
  selector: 'app-statistiquee',
  templateUrl: './statistiquee.component.html',
  styleUrls: ['./statistiquee.component.css']
})
export class StatistiqueeComponent implements OnInit {


  labelPosition: 'nbr' | 'ca' = 'nbr';
  checked = false;



  annees: any[];
  response: any = []



  salaire: any = 0;
  heure_travailler: any = 0;
  nbr_mission: any = 0;
  nbr_specilaite: any = 0
  currentUser: any;
  interim_id: any;

  barChart: any;
  pie3dChart: any


  elements = [];
  data = {
    filter: null,
    annee: 2020,
    interim: null
  };
  candiddatues = []
  listSpecialites = [];

  displayedColumnsFilter = [

    {
      name: "specialite",
      label: "Specialite"
    },
    {
      name: "annee",
      label: "Annee"
    },


  ];

  constructor(
    private specialiteService: SpecialiteService,
    private interimService: InterimService,
    private notificationService: NotificationService,
    private activateRoute: ActivatedRoute,
    private utiles: UtilsService,
    private userService: UserService,

  ) {


  }


  ngOnInit() {


    this.currentUser = this.userService.getCurrentUser();
    if (this.activateRoute.snapshot.params['id'])
      this.interim_id = this.activateRoute.snapshot.params['id'];
    else
      this.interim_id = this.currentUser.interim.id;

    this.annees = this.utiles.yearForGraph();

    this.data.interim = + this.interim_id
    this.getStatistiqueProfil(this.data)
    this.getStatistique(this.data)
    this.getAllSpecialite()


  }

  getAllSpecialite() {
    this.specialiteService.list(null, true).subscribe((response: any) => {
      this.listSpecialites = response.data

    });
  }
  newStat() {
    this.loadgrah(this.response)
  }


  getStatistiqueProfil(data: any) {
    this.interimService.getStatistiqueProfil(data).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur)
        }
        else {



          this.heure_travailler = response.data[0].heure_travailler
          this.nbr_mission = response.data[1]
          this.salaire = response.data[0].salaire
          this.nbr_specilaite = response.data[0].nbr_specialite









        }
      }
    )
  }




  getStatistique(data: any) {
    this.notificationService.blockUiStart()
    this.interimService.getStatistiqueByYear(data).subscribe(
      (response: any) => {

        this.notificationService.blockUiStop()

        if (!response.erreur) {

          this.response = response
          this.loadgrah(response)

        }
        else {
          this.notificationService.showNotificationEchec(response.erreur)
        }
      }
    );


  }




  loadgrah(response) {
    if (this.labelPosition === "nbr") {
      this.pieChart(response.data2.nbr)
      this.chartBar(response.dataset_nbr)
    }
    if (this.labelPosition === "ca") {
      this.pieChart(response.data2.salaire)
      this.chartBar(response.data)
    }

  }




  pieChart(donnee: any) {
    let data = new DataModel()
    let chartModel = new ChartModel()
    if (this.labelPosition === "ca") {
      chartModel.caption = " Evolution Rémunération"
      chartModel.numberprefix = "€"
      chartModel.yaxisname = "Chiffre en euro"
    }
    else if (this.labelPosition === "nbr") {
      chartModel.caption = "Traitement Candidature"
      chartModel.numberprefix = ""
      chartModel.yaxisname = "En Chiffre Chiffre"
    }

    chartModel.subcaption = "Par statut"
    chartModel.theme = "fusion"



    data.chart = chartModel
    data.data = donnee



    this.pie3dChart = new TypeChartModel('100%', 500, "pie3d", "json", data)
  }




  chartBar(donnee: any) {
    let data = new DataModel()

    if (this.labelPosition === "ca") {
      data.chart = {
        caption: "Evolution  Rémunération pour chaque mois",
        subcaption: "Par statut",
        numberprefix: "€",
        numbersuffix: "",
        plottooltext: "   <b>$dataValue</b>  candidature $seriesName pour le mois de $label",
        theme: "fusion"
      }
    }
    else {
      data.chart = {
        caption: "Traitement  candidature pour chaque mois",
        subcaption: "Par statut",
        numberprefix: "",
        numbersuffix: "",
        plottooltext: "   <b>$dataValue</b>  candidature $seriesName pour le mois de $label",
        theme: "fusion"
      }
    }



    data.categories =
      [
        {
          category: [
            {
              label: "Janvier"
            },
            {
              label: "Fevrier"
            },
            {
              label: "Mars"
            },
            {
              label: "Avril"
            },
            {
              label: "Mai"
            },
            {
              label: "Juin"
            },
            {
              label: "Juillet"
            },
            {
              label: "Aout"
            },
            {
              label: "Septembre"
            },
            {
              label: "Octobre"
            },
            {
              label: "Novembre"
            },
            {
              label: "Décembre"
            },

          ]
        }
      ],

      data.dataset = donnee

    this.barChart = new TypeChartModel('100%', 500, "mscolumn3d", "json", data)
  }

  valutElements() {
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.data.annee = 2020

      this.getStatistique(this.data)
      this.getStatistiqueProfil(this.data)
    }

  }

  onSubmitFiltre(form: NgForm) {


    this.data.filter = form.value;

    if (form.controls["annee"]) {
      this.data.annee = form.controls["annee"].value


    }



    this.getStatistique(this.data)
    this.getStatistiqueProfil(this.data)
  }

}

