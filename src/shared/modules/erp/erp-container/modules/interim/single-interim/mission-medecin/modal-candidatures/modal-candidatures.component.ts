import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-modal-candidatures',
  templateUrl: './modal-candidatures.component.html',
  styleUrls: ['./modal-candidatures.component.css']
})
export class ModalCandidaturesComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  onProgress(etat) {
    let rep: number;
    if (etat === 'En Attente')
      rep = 40;
    else if (etat === 'Validee')
      rep = 70;
    return rep;
  }

}
