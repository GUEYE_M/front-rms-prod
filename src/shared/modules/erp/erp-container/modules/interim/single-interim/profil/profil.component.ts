import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { EditInterimComponent } from "../../edit-interim/edit-interim.component";
import { InterimService } from "../../../../../../../services/interim/interim.service";
import { UtilsService } from "../../../../../../../services/utils.service";
import { NotificationService } from "../../../../../../../services/notification.service";
import { ClientService } from "../../../../../../../services/recruteur/client.service";
import { Router } from "@angular/router";
import { ChAEviterModalComponent } from "../ch-a-eviter-modal/ch-a-eviter-modal.component";
import $ from "jquery";
import { map, tap } from 'rxjs/operators';
import { from } from 'rxjs';
import { RolesService } from 'src/shared/services/roles.service';
import { UserService } from 'src/shared/services/user.service';
import {AddIdPldManuelComponent} from '../../../missions-interimaires/add-id-pld-manuel/add-id-pld-manuel.component';
import {IMAGES_API_URL} from '../../../../../../../utils/images_api_url';

@Component({
  selector: "app-profil",
  templateUrl: "./profil.component.html",
  styleUrls: ["./profil.component.css"]
})
export class ProfilComponent implements OnInit {
  currentinterim: any;
  keysUserItems: any;
  photo: string;
  keysInterimsItems: any;
  idpld: any;
  keysInterimInfospersonnels = [];
  keysDetailsCompte = [];
  itemUpdate = "";
  labelTab = 1;
  afficherUpdatePhoto = false;
  idIterim: number;
  dialogRef_profil: MatDialogRef<EditInterimComponent>;
  panelOpenState = false;
  currentUser: any;
  private fileUrl = IMAGES_API_URL;
  photoProfil = this.fileUrl + 'images/photo.png';

  interim: any
  dialogRefAddidPldContrat: MatDialogRef<AddIdPldManuelComponent>;

  ch_a_eviter: any[] = [];
  dialogRef: MatDialogRef<ChAEviterModalComponent>;
  loaderUpdateItem = 0;
  fichier: any;
  fd: FormData = new FormData();
  constructor(
    private interimService: InterimService,
    private clientService: ClientService,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private router: Router,
    private utileService: UtilsService,
    public roleService: RolesService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.interimService.currentInterimaire.subscribe(response => {
      if (response) {
        this.idIterim = response.interim.id;
        this.interim = response;
        console.log(this.interim)
      }
    });



    this.getDataChAEviter();
  }
  getDataChAEviter() {
    let data = {
      client: null,
      interim: this.idIterim
    };
    this.utileService.getChAviterForInterim(data).subscribe((response: any) => {
      if (!response.erreur) {
        this.ch_a_eviter = response.data;
      }
    });
  }

  OnUpdateInterimaire() {
    const donnees = { id: this.idIterim, profil: true };
    this.dialogRef_profil = this.dialog.open(EditInterimComponent, {
      width: "800px",
      height: '90%',
      panelClass: "myapp-no-padding-dialog",
      data: donnees
    });

    this.dialogRef_profil.afterClosed().subscribe(response => {
      if (response) {
        if (response.profil) {
          this.interimService.find(response.id).subscribe(response => {
            this.keysDetailsCompte = [];

            this.interimService.currentInterimaire.next(response);
          });
        }
      }
    });
  }

  chAEviterModal(): void {
    this.clientService.listClients(null, true).subscribe((response: any) => {
      if (response) {
        this.dialogRef = this.dialog.open(ChAEviterModalComponent, {
          width: "800px",
          height: '300px',
          panelClass: "myapp-no-padding-dialog",
          data: {
            profil_client: null,
            client: response,
            interim: this.idIterim
          }
        });

        this.dialogRef.afterClosed().subscribe(response => {
          if (response) {
            let data = {
              client: response.client,
              interim: this.idIterim,
              description: response.remarque
            };

            this.utileService
              .newChAviterForInterim(data)
              .subscribe((item: any) => {
                if (!item.erreur) {
                  this.getDataChAEviter();
                  this.notificationService.showNotificationSuccessCopie(
                    "bottomRight",
                    item.success
                  );
                } else {
                  this.notificationService.showNotificationEchecCopie(
                    "bottomRight",
                    item.erreur
                  );
                }
              });
          }
        });

        //this.notificationService.blockUiStop();
      }
    });
  }
  retirerChAEviter(id: any) {
    this.notificationService.onConfirm();
    this.notificationService.dialogRef.afterClosed().subscribe(item => {
      if (item) {
        this.notificationService.blockUiStart();
        this.utileService
          .retirerChAviterForInterim(+id)
          .subscribe((response: any) => {
            this.notificationService.blockUiStop();
            if (!response.erreur) {
              this.getDataChAEviter();
              this.notificationService.showNotificationSuccessCopie(
                "bottomRight",
                response.success
              );
            } else {
              this.notificationService.showNotificationEchecCopie(
                "bottomRight",
                response.erreur
              );
            }
          });
      }
    });
  }

  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }



  OnchangephotoProfilInterim(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      const img = '.imgphotoProfilInterim';
      const fileType = event.target.files[0].type;
      let localUrl: string;
      const thiss = this;
      reader.onload = (e: any) => {
        if (fileType.includes('image')) {
          localUrl = e.target.result;
          $(img).attr('src', localUrl);
          this.notificationService.onConfirm('Êtes-vous sûr de vouloir supprimer ?');
          this.notificationService.dialogRef.afterClosed().subscribe((x) => {
            if (x) {
              $(img).attr('src', localUrl);
              thiss.onUploadPP();
            } else {
              $(img).attr('src', thiss.interim.photo);
            }
          });
          this.fd.append('photoProfil', event.target.files[0]);
          this.fichier = event.target.files[0];
        } else {
          this.notificationService.showNotificationEchec('Cet type de fichier n\'est pas prise en compte !');
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  afficherUpdatePP() {
    this.afficherUpdatePhoto = true;
  }
  fermerUpdatePP() {
    this.afficherUpdatePhoto = false;
  }
  onCliqueinputfileInterim() {
    const inputfile = '.photoProfilInterim';
    $(inputfile).click();
  }
  // =======================================================================================================================================
  // Fonction pour uploder un element cote API
  onUploadPP() {
    const idUser = this.interim.interim.user.id;
    const tableau = {id: idUser, photo : this.fd};
    this.notificationService.blockUiStart();
    this.userService.updatePhotoProfil(tableau).subscribe(
      (response) => {
        this.interim.photo = response;
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationSuccess('Mise à jour avec succès !');
      },
      err => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('Echec de la mise à jour !');
      });
  }

  addIdPldContrat() {
    this.dialogRefAddidPldContrat = this.dialog.open(AddIdPldManuelComponent, {
      panelClass: 'myapp-no-padding-dialog',
      data: { id: this.idIterim, user: 'interim' },
      width: '20%',
      height: '30%'
    });
  }
}
