import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ViewMessageProdpectionModalComponent } from '../../../prospection/view-message-prodpection-modal/view-message-prodpection-modal.component';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, NgForm } from '@angular/forms';

import { NotificationService } from '../../../../../../../services/notification.service';
import { UtilsService } from '../../../../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '../../../../../../../services/recruteur/client.service';
import { InterimService } from '../../../../../../../services/interim/interim.service';
import { MessagaProspectionService } from '../../../../../../../services/messaga-prospection.service';
import { SelectionModel } from '@angular/cdk/collections';
import { EditProspectionComponent } from '../../../client-wrapper/client/single-client-container/prospection-interim/edit-prospection/edit-prospection.component';

@Component({
  selector: 'app-prospection-interim',
  templateUrl: './prospection-interim.component.html',
  styleUrls: ['./prospection-interim.component.css']
})
export class ProspectionInterimComponent implements OnInit {

  user: number;
  dialogRef: MatDialogRef<EditProspectionComponent>;
  message_list: any[];
  showLoadClient: boolean;
  actionInput: Boolean = false;
  action: string;
  list_message = [];
  public form: FormGroup;
  elements = [];
  datafiltre = {
    filter: null,
    date_debut: null,
    date_fin: null,
    interim: null,
    client: null
  };
  pageSize = 5;


  @Input() length: number = 0;
  pageSizeOptions: number[] = [5, 12, 25, 50, 100];

  displayedColumnsFilter = [
    {
      name: "type",
      label: "Type"
    },
    {
      name: "rappeler",
      label: "A Rappeler"
    },

  ];

  displayedColumns = ['number', 'Type', 'Date Rapel', 'Date Enregistrement', 'auteur', 'commentaire', 'Edit'];


  dataSource: MatTableDataSource<any[]> = new MatTableDataSource([]);
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  listrecruteur: any[] = null;
  types = [];


  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('listPaginator', { static: false }) paginator: MatPaginator;
  data: { id: number; recruteur: any[]; client: number; modal: false };



  constructor(private prospectionService: MessagaProspectionService,
    private interimService: InterimService,
    private clientService: ClientService,
    private dialog: MatDialog,
    private router: ActivatedRoute,
    private utilsService: UtilsService,
    private notificationService: NotificationService) {

  }

  ngOnInit() {
    this.utilsService.onRevientEnHaut();

    if (this.router.snapshot.routeConfig.component.name === 'SingleClientComponent') {
      //this.notificationService.blockUiStart()
      this.clientService.currentClient.subscribe(
        (response) => {
          if (response) {


            this.listrecruteur = response.recruteur;
            this.datafiltre.client = response.id;

            this.clientService.labelTab_text.subscribe(
              (index) => {
                if (index === 'message_de_prospections') {
                  this.getData();
                }
              }
            );
          }
        },
        (erreur) => {
          this.notificationService.showNotificationEchec('erreur chargement des messages prospection');
          this.notificationService.blockUiStop();
        },
        () => {
          this.notificationService.blockUiStop();
        }
      );
    }
    else {
      this.interimService.currentInterimaire.subscribe(
        (response) => {
          if (response) {
            // (response.interim.user);
            this.user = response.interim.user.id;

            this.datafiltre.interim = response.interim.user.id;
            this.interimService.labelTab_text.subscribe(
              (index) => {
                if (index === 'message_de_prospections') {
                  this.getData();
                }
              }
            );
          }

        },
        (erreur) => {
          this.notificationService.showNotificationEchec("echec chargement message prospection, veillez contacter le service IT")
        },
        () => {

        }
      );
    }

    this.prospectionService.closemodaleAddMsg.subscribe(
      (response: boolean) => {
        if (response) {
          this.dialogRef.close();
        }
      }
    );
    this.prospectionService.ProspectSubject.subscribe(
      (response) => {
        (response);
        // this.list_message = response.data;
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
      }
    );

    if (this.list_message.length > 0) {
      this.dataSource = new MatTableDataSource(this.list_message);
    }


    this.prospectionService.listTypeProspection().subscribe(
      (response: any) => {

        if (response) {

          this.types = response.data;
          (this.types)


        }
      }
    );
  }

  editMessageModal(id: number = null): void {
    this.data = {
      id: id,
      recruteur: this.listrecruteur,
      client: +this.router.snapshot.params["id"],
      modal: false
    };
    this.dialogRef = this.dialog.open(EditProspectionComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: this.data

    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {
          this.getData()
        }
      }
    )


  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre(form: NgForm) {

    this.datafiltre.filter = form.value;

    // (this.datafiltre);


    this.getData();
  }

  getData() {
    //  this.notificationService.blockUiStart()
    this.prospectionService.listMessageByProspect(this.datafiltre).subscribe(
      (response: any) => {
        this.prospectionService.list_message_by_prospect = response.data.data;
        this.dataSource = new MatTableDataSource(response.data.data);
        // this.prospectionService.emitMessageByProspect();
        this.length = response.data.data.length;
        this.dataSource.paginator = this.paginator;
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur recherche!! veuillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }

  valutElements() {
    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.datafiltre.filter = null;
      this.datafiltre.date_debut = null;
      this.datafiltre.date_fin = null;
      this.getData()
    }
  }

  message(message: any) {
    let dialogRef: MatDialogRef<ViewMessageProdpectionModalComponent>;
    dialogRef = this.dialog.open(ViewMessageProdpectionModalComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: message

    });
  }
}
