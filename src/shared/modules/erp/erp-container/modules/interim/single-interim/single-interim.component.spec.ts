import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleInterimComponent } from './single-interim.component';

describe('SingleInterimComponent', () => {
  let component: SingleInterimComponent;
  let fixture: ComponentFixture<SingleInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
