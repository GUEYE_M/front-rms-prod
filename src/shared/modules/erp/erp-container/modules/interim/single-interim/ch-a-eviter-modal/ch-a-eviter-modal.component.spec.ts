import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChAEviterModalComponent } from './ch-a-eviter-modal.component';

describe('ChAEviterModalComponent', () => {
  let component: ChAEviterModalComponent;
  let fixture: ComponentFixture<ChAEviterModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChAEviterModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChAEviterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
