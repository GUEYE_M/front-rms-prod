import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Observable, from} from 'rxjs';
import {MAT_DIALOG_DATA, MatAutocomplete, MatDialog, MatDialogRef} from '@angular/material';
import {NotificationService} from '../../../../../../../services/notification.service';
import {map, startWith, tap} from 'rxjs/operators';

@Component({
  selector: 'app-ch-a-eviter-modal',
  templateUrl: './ch-a-eviter-modal.component.html',
  styleUrls: ['./ch-a-eviter-modal.component.css']
})
export class ChAEviterModalComponent implements OnInit {

  itemCtrl = new FormControl('', Validators.required);
  remarqueCtrl = new FormControl('', Validators.required);
  filteredItems: Observable<any[]>;
  client_profil = false;
  items: any[];
  titre:any =  ""



  @ViewChild('interimInput', {static: false}) interimInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
  listClientFilter = [];


  constructor(
    private notificationService: NotificationService,
    public dialogRef: MatDialogRef<ChAEviterModalComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

    if(data.profil_client == null)
    {
      this.items= data.client;

      this.titre = "Choisir Centre Hospitalier a eviter"


      from(data.client.data).pipe(
        map(
          (item: any) => {
          
            return {
              display: item.nom_etablissement,
              value: item.id,

            };

          }),
        tap((el) => {
          this.listClientFilter.push(el);
        })
      ).subscribe();

    }
    else{
      this.items = data.interim;
      this.client_profil = true;
      this.titre = "Choisir interimaire a eviter"
    }

   

    this.filteredItems = this.itemCtrl.valueChanges
      .pipe(
        startWith(''),
        map(item => item ? this._filterItems(item) : this.items.slice())
      );
  }
  ngOnInit() {


  }



  private _filterItems(value: string): any[] {
    const filterValue = value.toLowerCase();


    if(this.client_profil == false){
      return this.items.filter(state => state.nom_etablissement.toLowerCase().indexOf(filterValue) === 0);
    }
    else
    {
      return this.items.filter(state => state.nom.toLowerCase().indexOf(filterValue) === 0);
    }


  }

  save(){

   if(this.itemCtrl.valid && this.remarqueCtrl.valid)
   {
    this.notificationService.onConfirm();
    this.notificationService.dialogRef.afterClosed().subscribe(
      (item)=>{
        if(item)
        {
          const data = {
            client: this.itemCtrl.value,
            remarque: this.remarqueCtrl.value
          };
      
         
          this.dialogRef.close(data);
        }
      }
    )
   }
   else
   {
    this.notificationService.showNotificationEchec("formulaire invalide! veillez remplir tout les champs")
   }
   


  }
}
