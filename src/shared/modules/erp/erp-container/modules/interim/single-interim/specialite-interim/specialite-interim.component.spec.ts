import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialiteInterimComponent } from './specialite-interim.component';

describe('SpecialiteInterimComponent', () => {
  let component: SpecialiteInterimComponent;
  let fixture: ComponentFixture<SpecialiteInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialiteInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialiteInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
