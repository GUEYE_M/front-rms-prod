import { Component, Inject, OnInit } from '@angular/core';
import { Specialite } from '../../../../../../../../models/Specialite.model';
import { SpecialiteService } from '../../../../../../../../services/specialite.service';
import { ActivatedRoute, Router } from '@angular/router';
import { QualificationService } from '../../../../../../../../services/qualification.service';
import { InterimService } from '../../../../../../../../services/interim/interim.service';
import { UserService } from '../../../../../../../../services/user.service';
import { NotificationService } from '../../../../../../../../services/notification.service';
import $ from 'jquery';
import { ConfirmDeleteSpecialiteComponent } from '../../../modal-interim/confirm-delete-specialite/confirm-delete-specialite.component';
import { MatDialog } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UtilsService } from 'src/shared/services/utils.service';

@Component({
	selector: 'app-specialite-interim-modal',
	templateUrl: './specialite-interim-modal.component.html',
	styleUrls: [ './specialite-interim-modal.component.css' ]
})
export class SpecialiteInterimModalComponent implements OnInit {
	listes_specialites = [];
	specialite;
	specialiteChoisi: any;
	specialiteChoisiForInterim: any;
	specialiteUpdate: any;
	showBloc = false;
	mail: boolean = true;
	message_notif = '';
	public choixCheckBox = [];

	constructor(
		private specialiteService: SpecialiteService,
		private activatedRoute: ActivatedRoute,
		private qualificationService: QualificationService,
		private router: Router,
		@Inject(MAT_DIALOG_DATA) public id_interim: any,
		public dialog: MatDialog,
		private utilService: UtilsService,
		private userService: UserService,
		private notifierService: NotificationService
	) {}

	ngOnInit() {
		this.getSpecialiteAndQualifsForInterimHaveNot(this.id_interim);
		this.message_notif = this.utilService.messageNotifMail();
		this.specialite = [];
		this.specialiteService.SpecialiteSubject.subscribe((next) => {
			this.listes_specialites = next;
		});
		this.specialiteService.specialite_choisi.subscribe((next: Specialite) => {
			if (next !== null) {
				const tmp = [];
				this.specialiteChoisi = next;
				if (next.qualifications.length > 0) {
					this.choixCheckBox = [];
					next.qualifications.forEach(function(element: any) {
						tmp.push(element.id_qualification);
					});
				}
				this.choixCheckBox = tmp;
				if (typeof this.specialiteChoisi.id_specialite !== 'undefined') {
					this.specialiteUpdate = [];
					this.specialiteService
						.getQualifsBySpecialite(this.specialiteChoisi.id_specialite)
						.subscribe((item) => {
							this.specialiteUpdate = item;
						});
				}
			}
		});
	}

	sendQualifications(index) {
		this.choixCheckBox = [];
		this.specialite = this.listes_specialites[index];
		this.specialiteChoisiForInterim = this.specialite.nomspecialite;
		if (this.specialite) {
			if (this.specialite.qualifs.length > 1) {
				this.showBloc = true;
			} else {
				this.showBloc = false;
				const tmp = [];
				if (this.specialite.qualifs.length > 0) {
					tmp.push(this.specialite.qualifs[0].id);
				}
				this.choixCheckBox = tmp;
			}
		}
	}

	add_specialite() {
		this.notifierService.showNotificationSuccessCopie('', '');
		this.notifierService.blockUiStart();
		this.qualificationService.create(this.id_interim, this.choixCheckBox, this.mail).subscribe((next: any) => {
			this.qualificationService.getQualificationsForMed(this.id_interim).subscribe((val) => {
				this.notifierService.blockUiStop();
				this.specialiteService.listeSpecialitesForInterimSubject.next(val);
				if (next.body === 'La spécialité a bien été enregistrée') {
					this.dialog.closeAll();
					this.specialite = [];
					this.notifierService.showNotificationSuccessCopie(
						'topCenter',
						'La spécialité a bien été enregistrée' + ' (' + this.specialiteChoisiForInterim + ')'
					);
				} else {
					this.notifierService.showNotificationEchecCopie(
						'topCenter',
						next.body + ' (' + this.specialiteChoisiForInterim + ')'
					);
				}
			});
		});
	}

	edit_specialite() {
		const dialogRef = this.dialog.open(ConfirmDeleteSpecialiteComponent, {
			width: '550px',
			height: '200px',
			data: { id_interim: this.id_interim, item_: [], choixEdits: this.choixCheckBox, edit: true }
		});
	}
	getSpecialiteAndQualifsForInterimHaveNot(idInterim) {
		this.notifierService.blockUiStart();
		this.specialiteService.getSpecialiteAndQualifsForInterimHaveNot(idInterim).subscribe((specialites) => {
			this.specialiteService.SpecialiteSubject.next(specialites);
			this.notifierService.blockUiStop();
		});
	}
}
