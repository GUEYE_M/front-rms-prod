import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatDialog, MatDialogRef, MatSnackBar, MatTableDataSource } from '@angular/material';
import { UserService } from '../../../../../../services/user.service';
import { InterimService } from '../../../../../../services/interim/interim.service';
import { CandidatureService } from '../../../../../../services/candidature.service';
import { DisponibiliteService } from '../../../../../../services/interim/disponibilite.service';
import { RedirectionSercive } from '../../../../../../services/redirection.sercive';
import { PldService } from '../../../../../../services/pld.service';
import { RolesService } from '../../../../../../services/roles.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { OffresServiceAll } from '../../../../../../services/offres.service';
import { InterimPldModel } from '../../../../../../models/interim/InterimPld.model';
import { LOCALE } from '../../../../../../utils/date_filter_config';
import { FormControl, Validators } from '@angular/forms';
import { UtilsService } from '../../../../../../services/utils.service';
import { BehaviorSubject } from "rxjs";
import { empty } from "rxjs/internal/Observer";
import { EditInterimComponent } from '../edit-interim/edit-interim.component';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';

@Component({
  selector: 'app-single-interim',
  templateUrl: './single-interim.component.html',
  styleUrls: ['./single-interim.component.css']
})

export class SingleInterimComponent implements OnInit, AfterViewInit, OnDestroy {
  public barChartOptionsInterim = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  dialogRefProfil: MatDialogRef<EditInterimComponent>;

  public barChartLabelsInterim = ['JANVIER', 'FÉVRIER', 'MARS', 'AVRIL',
    'MAI', 'JUIN', 'JUILLET', 'AOUT', 'SEPTEMBRE', 'OCTOBRE', 'NOVEMBRE',
    'DÉCEMBRE'];
  public barChartTypeInterim = 'bar';
  public barChartLegendInterim = true;
  colorInterim = 'primary';
  modeInterim = 'determinate';
  valueInterim = 50;
  bufferValueInterim = 75;
  locale = LOCALE;

  anneesInterim: any[];

  anneeInterim = new FormControl('', [Validators.required]);

  @ViewChild('dateRangePicker', { static: false }) dateRangePicker;


  date_debut: number = null;
  date_fin: number = null;

  elements = [];
  data = {
    filter: null,
    annee: 2020,
    interim: null
  };
  displayedColumnsFilter = [

    {
      name: "specialite",
      label: "Specialite"
    },
    {
      name: "annee",
      label: "Annee"
    },


  ];

  salaireInterim: any = 0;
  heureTravaillerInterim: any = 0;
  nbrMissionInterim: any = 0;
  nbrSpecilaiteInterim: any = 0;
  currentUser: any;
  interim_id: any;

  candiddatues = [];


  range: Range = { fromDate: new Date(), toDate: new Date() };



  viewdatepiker: boolean = false
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];
  tabIndex: number;
  interimPldMessage = false;
  interim: any;
  interimairePld: any;
  listesOffress: any = [];
  listes_des_disponibilites_choisie: MatTableDataSource<any[]> = new MatTableDataSource([]);
  load = false;
  showAlertInterim: any;
  annees: any[];

  constructor(
    private datePipe: DatePipe,
    private activateRoute: ActivatedRoute,
    private utiles: UtilsService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private userService: UserService,
    private interimService: InterimService,
    private candidatureService: CandidatureService,
    private disponibiliteService: DisponibiliteService,
    private activatedRoute: ActivatedRoute,
    private redirectionInterim: RedirectionSercive,
    private pldService: PldService,
    public datepipe: DatePipe,
    private snackBar: MatSnackBar,
    public rolesService: RolesService,
    private notificationService: NotificationService,
    private offreService: OffresServiceAll
  ) {
  }

  ngOnInit() {
    // on instancie afin de vider le behaviorsubject
    this.interimService.labelTab_text = new BehaviorSubject('default');
    this.currentUser = this.userService.getCurrentUser();
    if (this.activateRoute.snapshot.params['id']) {
      this.interim_id = this.activateRoute.snapshot.params['id'];
      this.espaceCommercial();
    } else {
      this.interim_id = this.currentUser.interim.id;
      this.espaceCandidat();
    }

    this.anneeInterim.patchValue(
      {
        annee: new Date().getFullYear()
      }
    );
    this.anneesInterim = this.utiles.yearForGraph();

    this.annees = this.utiles.yearForGraph();

    this.getDataGraphStatCandidature();

    this.datepickerConfig()

    // this.disponibiliteService.listeDisponibiliteChoisiSubject.subscribe(
    //   (next) => {
    //     this.listes_des_disponibilites_choisie.data = next;
    //   }
    // );
    // this.interimService.showAlertSubjectInterim.subscribe(
    //   (showAlert) => {
    //     if (showAlert) {
    //       this.showAlertInterim = showAlert;
    //     }
    //   }
    // );
  }
  roleSuperAdmin() {
    return this.rolesService.isSuperAdmin(this.currentUser.roles);
  }
  changeItemTab(index) {
    // this.roleSuperAdmin()
    if (this.roleInterimUnique()) {
      switch (index) {
        case 0:
          this.interimService.labelTab_text.next('specialite');
          break;
        case 1:
          this.interimService.labelTab_text.next('dossier');
          break;
        case 2:
          this.interimService.labelTab_text.next('disponibilite');
          break;
        case 3:
          this.interimService.labelTab_text.next('missions');
          break;
        default:
        // code block
      }
    } else if (this.roleCommercialUnique()) {
      switch (index) {
        case 0:
          // this.interimService.labelTab_text.next()
          break;
        case 1:
          this.interimService.labelTab_text.next('specialite');
          break;
        case 2:
          this.interimService.labelTab_text.next('dossier');
          break;
        case 3:
          this.interimService.labelTab_text.next('disponibilite');
          break;
        case 4:
          this.interimService.labelTab_text.next('message_de_prospections');
          break;
        case 5:
          this.interimService.labelTab_text.next('offres');
          break;
        case 6:
          this.interimService.labelTab_text.next('missions');
          break;
        default:
        // code block
      }
    } else if (this.roleManagerOrComptable()) {
      switch (index) {
        case 0:
          // this.interimService.labelTab_text.next()
          break;
        case 1:
          break;
        case 2:
          this.interimService.labelTab_text.next('specialite');
          break;
        case 3:
          this.interimService.labelTab_text.next('dossier');
          break;
        case 4:
          this.interimService.labelTab_text.next('disponibilite');
          break;
        case 5:
          this.interimService.labelTab_text.next('message_de_prospections');
          break;
        case 6:
          this.interimService.labelTab_text.next('offres');
          break;
        case 7:
          this.interimService.labelTab_text.next('missions');
          break;
        default:
        // code block
      }
    }
  }
  ngAfterViewInit() {
    if (this.roleInterim()) {
      this.interimService.labelTab_text.next('specialite');
    }
  }

  getStatistiqueProfil(data: any) {
    this.interimService.getStatistiqueProfil(data).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
        } else {
          this.heureTravaillerInterim = response.data[0].heure_travailler;
          this.nbrMissionInterim = response.data[0].nombre_mission;
          this.salaireInterim = response.data[0].salaire;
          this.nbrSpecilaiteInterim = response.data[0].nbr_specialite;
          this.candiddatues = response.data[1];
        }
      }
    );
  }
  getDataGraphStatCandidature() {
    if (this.anneeInterim.valid) {
      const data: any = {
        annee: this.anneeInterim.value.annee,
        interim: +this.interim_id
      };
      // this.interimService.getStatistiqueCandidature(data).subscribe(
      //   (response) => {
      //     this.initBarChartData();
      //     this.barChartDataInterim.map(
      //       (item, index) => {
      //         response.data[item.label].map(
      //           (rep) => {
      //             if (rep.mois) {
      //               const index_mois = this.barChartLabelsInterim.indexOf(rep.mois.toUpperCase());
      //               this.bargraphData(index, index_mois, rep.nbr);
      //             }
      //           }
      //         );
      //       }
      //     );
      //   },
      //   () => {
      //     this.notificationService.showNotificationEchec("echec chargement de donnée");
      //     this.notificationService.blockUiStop();
      //   },
      //   () => {
      //     this.notificationService.blockUiStop();
      //   }
      // );
    }
  }
  initBarChartData() {

  }
  bargraphData(index_bar, index_val, nbr) {

  }
  getInterim(id) {
    this.interimService.find(id).subscribe((interim) => {
      this.interimService.currentInterimaire.next(interim);
      this.interim = interim['interim'];
      this.disponibiliteService.disponibiliteSubject.next(interim['disponibilites']);
      this.offreService.offreErpSubject.next(interim['listesOffres']);
      this.listesOffress = interim['listesOffres'];
    });
  }
  OnUpdateInterimaire() {
    const donnees = { id: this.interim_id, profil: true };
    this.dialogRefProfil = this.dialog.open(EditInterimComponent, {
      width: '800px',
      height: '90%',
      panelClass: 'myapp-no-padding-dialog',
      data: donnees
    });
  }
  espaceCandidat() {
    this.activatedRoute.data.subscribe(
      (next) => {
        if (next.interimSingle) {
          this.interimService.interim_choisi = next.interimSingle.interim;
          this.interim = next.interimSingle.interim;
        } else {
          this.getInterim(this.userService.getCurrentUser()['interim'].id);
        }
        if (this.interimService.labelTab.getValue()) {
          this.tabIndex = this.interimService.labelTab.getValue();

        }
      }
    );
  }
  espaceCommercial() {
    this.activatedRoute.data.subscribe(
      (next) => {
        if (next.interimSingle) {
          this.interimService.interim_choisi = next.interimSingle.interim;
          this.interimService.currentInterimaire.subscribe(response => {
            if (response && response.idPld === null) {
              this.interimPldMessage = true;
            }
          });

          // this.offreService.offreErpSubject.next([]);
          // this.offreService.offreErpSubject.next(next.interimSingle.listesOffres);
          this.interim = next.interimSingle.interim;
          // this.disponibiliteService.disponibiliteSubject.next(next.interimSingle.disponibilites);
          // this.listesOffress = next.interimSingle.listesOffres;
          this.interimService.currentInterimaire.next(next.interimSingle);
        } else {
          this.getInterim(this.userService.getCurrentUser()['interim'].id);
        }
        if (this.interimService.labelTab.getValue()) {
          this.tabIndex = this.interimService.labelTab.getValue();

        }
      }
    );
  }

  roleInterim() {
    return this.rolesService.isCandidat(this.currentUser.roles);
  }
  roleInterimUnique() {
    return this.rolesService.isCandidatUnique(this.currentUser.roles);
  }
  roleRecruteur() {
    return this.rolesService.isCandidat(this.currentUser.roles);
  }
  roleCommercialUnique() {
    return this.rolesService.isCommercialUnique(this.currentUser.roles);
  }
  roleManagerOrComptable() {
    return this.rolesService.isManager(this.currentUser.roles);
  }
  roleSuperviseur() {
    return this.rolesService.isSuperviseur(this.currentUser.roles);
  }

  ngOnDestroy(): void {
    // on instancie afin de vider le behaviorsubject
    this.interimService.labelTab_text = new BehaviorSubject('default');
  }

  datepickerConfig() {
    const today = new Date();
    const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
    const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

    //   const resetRange = {fromDate: today, toDate: today};
    //   this.dateRangePicker.resetDates(resetRange);

    this.setupPresets();
    this.options = {
      presets: this.presets,
      format: 'dd-MM-yyyy',
      range: { fromDate: null, toDate: null },
      applyLabel: "OK",
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: false,
        hasBackdrop: false
      },
      locale: 'de-DE',
      cancelLabel: "Annuler",
      startDatePrefix: "Debut",
      endDatePrefix: "Fin",
      placeholder: "Rechercher",
      animation: true
      // excludeWeekends:true,
      // fromMinMax: {fromDate:fromMin, toDate:fromMax},
      // toMinMax: {fromDate:toMin, toDate:toMax}
    };
  }


  setupPresets() {

    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    }

    const today = new Date()
    const yesterday = backDate(1);
    const minus7 = backDate(7)
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    this.presets = [
      { presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
      { presetLabel: "Les 7 derniers jours", range: { fromDate: minus7, toDate: today } },
      { presetLabel: "Les 30 derniers jours", range: { fromDate: minus30, toDate: today } },
      { presetLabel: "Ce mois", range: { fromDate: currMonthStart, toDate: currMonthEnd } },
      { presetLabel: "Le mois dernier", range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
    ]
  }


  updateRange(range: Range) {

    //this.notificationService.blockUiStart();

    this.range = range;


    if (this.range.fromDate && this.range.toDate) {
      let data: any = {
        "date_debut": this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd'),
        "date_fin": this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd'),
        "interim": +this.interim_id,
      };
      this.getStatistiqueProfil(data);
    }




  }

}
