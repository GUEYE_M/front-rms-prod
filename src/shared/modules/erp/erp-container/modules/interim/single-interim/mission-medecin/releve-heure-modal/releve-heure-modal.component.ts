import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MissionService} from '../../../../../../../../services/mission.service';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {PldService} from '../../../../../../../../services/pld.service';

@Component({
  selector: 'app-releve-heure-modal',
  templateUrl: './releve-heure-modal.component.html',
  styleUrls: ['./releve-heure-modal.component.css']
})
export class ReleveHeureModalComponent implements OnInit {
  idContratPld: any;
  content: any;
  getData: any;
  content1: any;
  releveHr: any;
  commentaireForm: FormGroup;
  constructor(
    private missionService: MissionService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private notificationService: NotificationService,
    private pldService: PldService,
    private dialog: MatDialog,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.initCommentaireForm();
    this.getData = this.data;
    this.idContratPld = this.getData.idContratPld;
    this.notificationService.blockUiStart();
    this.missionService.getRhByContrat(this.idContratPld).subscribe((rep) => {
        this.content1 = rep.data;
        if(this.content1) {
          this.pldService.findRH(rep.data.idRhPld).subscribe((response) => {
              this.releveHr = response[0];
              this.notificationService.blockUiStop();
            },
            error1 => {
              this.notificationService.blockUiStop();
            });
        } else {
          this.notificationService.blockUiStop();
        }
        this.content = rep.data;
      },
      error1 => {
        this.notificationService.blockUiStop();
      });
  }

  initCommentaireForm() {
    this.commentaireForm = this.fb.group({
      commentaire: ['']
    });
  }
  onEnregistre() {
    const data = {idRHPld: this.releveHr.IdRH, commentaire: this.commentaireForm.value['commentaire'], interim: this.getData.interimExiste };
    this.notificationService.blockUiStart();
    this.missionService.updateComRH(data).subscribe((response) => {
      this.dialog.closeAll();
      this.notificationService.blockUiStop();
    });
  }
  onTelechargeRh() {
    this.missionService.telechargeRH(this.releveHr).subscribe();
  }

}
