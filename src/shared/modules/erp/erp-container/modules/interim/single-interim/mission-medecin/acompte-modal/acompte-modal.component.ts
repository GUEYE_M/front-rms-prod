import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../../../../../services/user.service';
import {InterimService} from '../../../../../../../../services/interim/interim.service';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
  selector: 'app-acompte-modal',
  templateUrl: './acompte-modal.component.html',
  styleUrls: ['./acompte-modal.component.css']
})
export class AcompteModalComponent implements OnInit {
  currentUser: any;
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    private interimService: InterimService,
    private notificationService:NotificationService,
    private dialogRef: MatDialogRef<AcompteModalComponent>,
  ) { }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      somme: ['', Validators.required],
      commentaire: ['', Validators.required],
    });
  }
  save() {

    this.notificationService.blockUiStart();
    const data = {
      idMission: this.data.idMission,
      idContrat: this.data.contratPld.idPld,
      somme: this.form.value.somme,
      commentaire: this.form.value.commentaire,
      nom: this.currentUser.nom,
      prenom: this.currentUser.prenom,
      email: this.currentUser.email,
      civilite: this.currentUser.civilite
    };

    this.interimService.acompte(data).subscribe((response: any) => {
      this.notificationService.blockUiStop();
        if(response.erreur) {
          this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
        } else {
          this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
          this.dialogRef.close();
        }
    });
  }
}
