import {Component, OnInit, ViewChild} from '@angular/core';
import {QualificationService} from '../../../../../../../services/qualification.service';
import {SpecialiteService} from '../../../../../../../services/specialite.service';
import {ActivatedRoute, Router} from '@angular/router';
import {InterimService} from '../../../../../../../services/interim/interim.service';
import {UserService} from '../../../../../../../services/user.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {merge} from 'rxjs';
import {startWith, switchMap, take, takeLast} from 'rxjs/operators';
import {ConfirmDeleteSpecialiteComponent} from '../../modal-interim/confirm-delete-specialite/confirm-delete-specialite.component';
import {MatDialogRef} from "@angular/material/dialog";
import {SpecialiteInterimModalComponent} from "./specialite-interim-modal/specialite-interim-modal.component";

export interface SpecialiteData {
  id: string;
  nom_specialite: string;
  action: string;
}

@Component({
  selector: 'app-specialite-interim',
  templateUrl: './specialite-interim.component.html',
  styleUrls: ['./specialite-interim.component.css']
})

export class SpecialiteInterimComponent implements OnInit {
  choixCheckBoxEdit: any = {
    list: [],
    edit: []
  };
  specialites: any;
  specialiteChoisiForInterim = '';
  specialite_choisie = [];
  id_interim = 0;
  lenght = 0;
  col12 = 'col col-md-12 col-sm-12 col-xs-12 col-lg-12';
  displayedColumns: string[] = ['id', 'specialite', 'action'];
  displayedColumnsQualifs: string[] = ['id', 'qualification', 'action'];
  dataSource: MatTableDataSource<SpecialiteData> = new MatTableDataSource([]);
  dataSourceQualif: MatTableDataSource<any[]> = new MatTableDataSource([]);

  @ViewChild('MatPaginator', {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  dialogRef: MatDialogRef<SpecialiteInterimModalComponent>;

  constructor(private qualificationService: QualificationService,
              public specialiteService: SpecialiteService,
              private activatedRoute: ActivatedRoute,
              private interimService: InterimService,
              private userService: UserService,
              public dialog: MatDialog,
              private notificationService: NotificationService,
              private notifierService: NotificationService,
              private router: Router) {
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.params.id) {
      this.id_interim = this.interimService.interim_choisi.id;
    } else {
      this.id_interim = this.userService.getCurrentUser()['interim'].id;
    }
    this.interimService.labelTab_text.subscribe(
      (label) => {
        if (label === 'specialite') {
          this.notificationService.blockUiStart();
          this.qualificationService.getQualificationsForMed(this.id_interim).subscribe(
            (next) => {
              this.notificationService.blockUiStop();
              this.specialiteService.listeSpecialitesForInterimSubject.next(next);
            },
            error1 => {
              this.notificationService.blockUiStop();
            }
          );
          this.specialiteService.listeSpecialitesForInterimSubject
            .subscribe(
              (value) => {
                if (value) {
                  this.specialites = value;
                  this.dataSource = new MatTableDataSource([]);
                  this.dataSource = new MatTableDataSource(value);
                  this.lenght = value.length;
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
                }
              });
        }
      }
    );
  }

  onAddSpecialite() {
    this.dialogRef = this.dialog.open(SpecialiteInterimModalComponent, {
      width: '600px',
      height: '400px',
      panelClass: 'myapp-no-padding-dialog',
      data: this.id_interim
    });

  }

  showQualifs(item, etat) {
    this.specialiteService.detail = etat;
    this.dataSourceQualif = new MatTableDataSource(item.qualifications);
    this.dataSourceQualif.paginator = this.paginator;
    this.dataSourceQualif.sort = this.sort;
    this.specialite_choisie = item;
  }

  editSpecialite(specialite) {
    this.specialiteService.specialite_choisi.next(specialite);
  }

  deleteQualifInterim(item) {
    const dialogRef = this.dialog.open(ConfirmDeleteSpecialiteComponent, {
      width: '550px',
      height: '200px',
      data: {id_interim: this.id_interim, item_: item, choixEdits: [], edit: false}
    });

    dialogRef.afterClosed().subscribe(result => {
      // ('The dialog was closed');
      // this.animal = result;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
