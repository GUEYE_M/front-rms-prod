import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { UtilsService } from '../../../../../../../../services/utils.service';
import { SellandsignService } from '../../../../../../../../services/sellandsign.service';
import { ListCandidatureValiderModalComponent } from '../../../../missions-interimaires/list-candidature-valider-modal/list-candidature-valider-modal.component';

@Component({
	selector: 'app-statut-contrat-sign',
	templateUrl: './statut-contrat-sign.component.html',
	styleUrls: ['./statut-contrat-sign.component.css']
})
export class StatutContratSignComponent implements OnInit {
	docpdf: any;
	action: any;
	sampleBytes = new Int8Array(4096);
	transaction_contrat: any;
	client: any;
	statut_contrat = {
		color: '',
		statut: ''
	};
	etatContratSignature: string = '';
	contratOption: string[] = [];

	pdfSrc: string = '';

	element: any;

	constructor(
		public dialogRef: MatDialogRef<StatutContratSignComponent>,
		public dialogRefListCand: MatDialogRef<ListCandidatureValiderModalComponent>,
		private notificationService: NotificationService,
		private utilService: UtilsService,
		private sellAndSignService: SellandsignService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		if (this.data) {
			if (this.data.transaction.status === 'ABANDONED') {
				this.statut_contrat.statut = 'CONTRAT ABANDONNÉ';
				this.contratOption.push('supprimer')
				this.statut_contrat.color = '#ff4081';
			} else if (this.data.transaction.status === 'SIGNED') {
				this.statut_contrat.statut = 'CONTRAT SIGNÉ';
				this.statut_contrat.color = '#1CAF9A';
				this.contratOption.push(
					'Valider',
					'Annuler',
					'Telecharger le  document envoyé',
					'Telecharger toutes les pieces'
				);
			} else if (this.data.transaction.status === 'CLOSED') {
				this.statut_contrat.statut = 'CONTRAT VALIDÉ';
				this.statut_contrat.color = '#3f51b5';
				this.contratOption.push(
					'Telecharger le  document signé',
					'Telecharger le  dossier de preuve',
					'Telecharger le certificat de preuve',
					'Telecharger toutes les pieces'
				);
			} else if (this.data.transaction.status === 'OPEN') {
				this.statut_contrat.statut = 'CONTRAT  PRESENTER POUR SIGNATURE';
				this.statut_contrat.color = '#ff9800';
				this.contratOption.push(
					'Relancer',
					'Annuler',
					'Telecharger le  document envoyé',
					'Telecharger toutes les pieces'
				);
			} else if (this.data.transaction.status === 'ARCHIVED') {
				this.statut_contrat.statut = 'CONTRAT ARCHIVER';
				this.statut_contrat.color = '#ff4081';
				this.contratOption.push(
					'Telecharger le  document signé',
					'Telecharger le  dossier de preuve',
					'Telecharger le certificat de preuve',
					'Telecharger toutes les pieces'
				);
			}
			//

			this.transaction_contrat = {
				id: this.data.transaction.id,
				transactionId: this.data.transaction.transactionId,
				representant: this.data.transaction.vendorEmail,
				status: this.statut_contrat,
				modele: this.data.transaction.filename,
				date: this.data.transaction.date,
				closedDate: this.data.transaction.closedDate
			};
			this.etatContratSignature = this.utilService.satutContratFr(
				this.data.etatContrat.elements[0].signatureStatus
			);
			this.element = this.data.signataire;

			this.client = this.data.client;
		}
	}

	ngOnInit() { }

	getDocPdfSellAndSign() {
		this.notificationService.blockUiStart();
		this.sellAndSignService.getDocumentPdfSellAndSign(this.transaction_contrat.id).subscribe(
			(response: any) => {
				if (response) {
					this.pdfSrc = this.viewdocumentPdf(response);
					var fileURL = URL.createObjectURL(response);
					window.open(fileURL);
				}
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur chargement fichier, veillez ressayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	saveByteArray([], file: any) {
		let a: any = document.createElement('a');
		document.body.appendChild(a);
		a.style = 'display: none';
		return function (data, name) {
			var blob = new Blob(data, { type: 'octet/stream' }),
				url = window.URL.createObjectURL(blob);
			a.href = url;
			a.download = name;
			a.click();
			window.URL.revokeObjectURL(url);
		};
	}

	actionbtn(action: string = null, contrat_id: number, transactionId: any) {
		if (action) {
			let data = {
				contrat_id: contrat_id,
				transactionId: transactionId
			};
			if (action.toLowerCase().trim() == 'Valider'.toLowerCase().trim()) {
				this.notificationService.blockUiStart();

				this.sellAndSignService.validationContrat(data).subscribe(
					(response: any) => {
						this.notificationService.blockUiStop();
						if (!response.erreur) {
							this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
							this.dialogRef.close(false);
						} else {
							this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
						}
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'une erreure est survenue lors de la validation du contrat, veillez ressayer'
						);
					},
					() => { }
				);
			} else if (action.toLowerCase().trim() == 'Annuler'.toLowerCase().trim()) {
				this.notificationService.blockUiStart();
				this.sellAndSignService.annulerContrat(contrat_id).subscribe(
					(response: any) => {
						if (!response.erreur) {
							this.notificationService.showNotificationSuccessCopie(
								'bottomRight',
								'le contrat ' + contrat_id + ' a été bien annuler'
							);

							this.dialogRef.close(action.toLowerCase().trim());
							this.notificationService.blockUiStop();
						} else {
							this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
						}
					},
					(erreur) => {
						this.notificationService.showNotificationEchec('echec annulation du contrat, veillez ressayer');
					},
					() => { }
				);
			} else if (action.toLowerCase().trim() == 'Telecharger le  document signé'.toLowerCase().trim()) {
				this.notificationService.blockUiStart();

				this.sellAndSignService.getDocumentSigné(contrat_id).subscribe(
					(response) => {
						this.dowloadFile(response, 'document signer'.toLowerCase().trim() + '.pdf');
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'echec telechargement du contrat, veillez ressayer'
						);
					},
					() => {
						// this.notificationService.showNotificationSuccess("le contra a été bien annuler")
						this.notificationService.blockUiStop();
					}
				);
			} else if (action.toLowerCase().trim() == 'Telecharger le  dossier de preuve'.toLowerCase().trim()) {
				this.notificationService.blockUiStart();

				this.sellAndSignService.getDossierPreuve(contrat_id).subscribe(
					(response) => {
						this.dowloadFile(response, 'document preuve'.toLowerCase().trim() + '.zip');
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'echec telechargement du contrat, veillez ressayer'
						);
					},
					() => {
						// this.notificationService.showNotificationSuccess("le contra a été bien annuler")
						this.notificationService.blockUiStop();
					}
				);
			} else if (action.toLowerCase().trim() == 'Telecharger le  document envoyé'.toLowerCase().trim()) {
				this.sellAndSignService.getDocumentPdfSellAndSign(contrat_id).subscribe((response: any) => {
					if (response) {
						this.dowloadFile(response, 'document'.toLowerCase().trim() + '.pdf');
					}
				});
			} else if (action.toLowerCase().trim() === 'Relancer'.toLowerCase().trim()) {
				this.notificationService.blockUiStart();
				let data = {
					all: false,
					contract_id: '' + contrat_id,
					contractors: []
				};

				this.sellAndSignService.getListSignataire({
					contract_id: contrat_id,
					keyword: "",
					status: "",
					offset: 0,
					size: 50,
				}).subscribe(
					(rep: any) => {
						if (rep) {
							rep.elements.forEach(element => {
								data.contractors.push('' + element.contractId)
							});

						}

					}, (error => { }),
					() => {
						this.sellAndSignService.relancerContrat(data).subscribe(
							(response) => {
								if (response) {
									this.notificationService.showNotificationSuccessCopie(
										'bottomRight',
										'le  contrat ' + contrat_id + ' a bien été relancé'
									);
									this.dialogRef.close(false);
									this.notificationService.blockUiStop();
								}
							},
							(erreur) => {
								console.log(erreur)
								this.notificationService.showNotificationEchecCopie(
									'bottomRight',
									'erreur relance contrat ' + contrat_id + ' veillez ressayer'
								);
								this.notificationService.blockUiStop();
							},
							() => { }
						);
					}
				)




			} else if (action.toLowerCase().trim() == 'supprimer'.toLowerCase().trim()) {
				this.notificationService.blockUiStart();
				this.sellAndSignService.deleteContrat(data).subscribe(
					(response: any) => {
						this.notificationService.blockUiStop();
						if (!response.erreur) {
							this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
							this.dialogRef.close(false);
							this.dialogRefListCand.close(true)

						} else {
							this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
						}
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'une erreure est survenue lors de la validation du contrat, veillez ressayer'
						);
					},
					() => { }
				);
			}
		} else {
			this.notificationService.showNotificationEchec('veillez selectionnez une action');
		}
	}
	dowloadFile(file: any, name: string) {
		// It is necessary to create a new blob object with mime-type explicitly set
		// otherwise only Chrome works like it should
		var newBlob = new Blob([file], { type: 'application/pdf' });

		// IE doesn't allow using a blob object directly as link href
		// instead it is necessary to use msSaveOrOpenBlob
		// if (window.navigator && window.navigator.msSaveOrOpenBlob) {
		// 	window.navigator.msSaveOrOpenBlob(newBlob);
		// 	return;
		// }

		// For other browsers:
		// Create a link pointing to the ObjectURL containing the blob.
		const data = window.URL.createObjectURL(newBlob);

		var link = document.createElement('a');
		link.href = data;
		link.download = name;
		// this is necessary as link.click() does not work on the latest firefox
		link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

		setTimeout(function () {
			// For Firefox it is necessary to delay revoking the ObjectURL
			window.URL.revokeObjectURL(data);
			link.remove();
		}, 100);
	}

	viewdocumentPdf(file: any) {
		var newBlob = new Blob([file], { type: 'application/pdf' });

		// IE doesn't allow using a blob object directly as link href
		// instead it is necessary to use msSaveOrOpenBlob
		// if (window.navigator && window.navigator.msSaveOrOpenBlob) {
		// 	window.navigator.msSaveOrOpenBlob(newBlob);
		// 	return;
		// }

		// For other browsers:
		// Create a link pointing to the ObjectURL containing the blob.
		return window.URL.createObjectURL(newBlob);
	}
}
