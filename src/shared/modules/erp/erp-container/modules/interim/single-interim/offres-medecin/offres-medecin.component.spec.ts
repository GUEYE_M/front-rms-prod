import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffresMedecinComponent } from './offres-medecin.component';

describe('OffresMedecinComponent', () => {
  let component: OffresMedecinComponent;
  let fixture: ComponentFixture<OffresMedecinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffresMedecinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffresMedecinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
