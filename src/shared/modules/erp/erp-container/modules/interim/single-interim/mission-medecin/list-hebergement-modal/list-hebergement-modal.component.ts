import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {SelectionModel} from '@angular/cdk/collections';
import { HebergementServiceService } from 'src/shared/services/recruteur/hebergement.service.service';
import { AdminConfigHebergementEditComponent } from '../../../../admin-config/admin-config-hebergement-edit/admin-config-hebergement-edit.component';

@Component({
  selector: 'app-list-hebergement-modal',
  templateUrl: './list-hebergement-modal.component.html',
  styleUrls: ['./list-hebergement-modal.component.css']
})
export class ListHebergementModalComponent implements OnInit, AfterViewInit {

  dialogRef: MatDialogRef<AdminConfigHebergementEditComponent>;
   
  elements = []
  data = {
      offset:0,
      limit:12,
     filter:null,
    


  }
  modal : boolean = false;
  pageSize : number = 12;
  length :number = 0;
  pageSizeOptions: number[] = [5,12, 25, 50, 100];
  displayedColumns = [
       'select',
       'number',
       'etablissement', 
       'lieu', 
       'etoile',
       'adresse',
       'telephone',
       'auteur',
       'Edit'];
       
  selection = new SelectionModel<any[]>(true, []);

  displayedColumnsFilter = [];
 


  edit: boolean = false;
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatSort,{static:false}) sort: MatSort;
  @ViewChild('listPaginator',{static:false}) paginator: MatPaginator;

  constructor(private hebergementService: HebergementServiceService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
              private dialog: MatDialog, private notifierService: NotificationService, @Inject(MAT_DIALOG_DATA) public item:any,public  dialogRef_heber: MatDialogRef<ListHebergementModalComponent> ) {
  
               
                this.initData()
  }

  ngOnInit() {
      this.displayedColumnsFilter = this.hebergementService.displayedColumnsName()
     

  }

  ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  }
 
 

  editHebergementModal(id: number = null): void {


      this.dialogRef = this.dialog.open(AdminConfigHebergementEditComponent, {
          width: '800px',
          height: '400px',
          panelClass: 'myapp-no-padding-dialog',
          data: id

      })

      this.dialogRef.afterClosed().subscribe(
        (response)=>{
         if(response)
         {
          this.initData()
         }
            
            
        }
      )


  }


  initData()
  {
   
   
    this.dataSource = new MatTableDataSource(this.item.data);
    this.length =this.item.data.length 
      
  }



  
  // fonction qui envoie le formulaire de filtre en base de donnee


  pagination(event:any)
  {
    
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize

    this.initData()
   
  

  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(
            row => this.selection.select(row));
}

isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
}

onNoClick(): void {
  this.dialogRef_heber.close();
}

onClick():void{
  let erreur  = null;
  if(this.selection.selected.length === 0 )
  {
    erreur = "veillez choisir un hebergement";
  }
  if(this.selection.selected.length > 1 )
  {
    erreur = "selection invalide, veillez choisir un seul hebergement";
  
  }
  if(erreur)
  {
    this.notifierService.showNotificationEchecCopie("bottomRight",erreur)
  }
  else
  {
  
    this.dialogRef_heber.close(this.selection.selected);
  }
}

}
