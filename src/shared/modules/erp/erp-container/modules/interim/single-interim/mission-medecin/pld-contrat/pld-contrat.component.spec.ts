import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PldContratComponent } from './pld-contrat.component';

describe('PldContratComponent', () => {
  let component: PldContratComponent;
  let fixture: ComponentFixture<PldContratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PldContratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PldContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
