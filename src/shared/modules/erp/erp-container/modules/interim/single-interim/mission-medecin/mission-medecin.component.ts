import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { ListHebergementModalComponent } from './list-hebergement-modal/list-hebergement-modal.component';
import { ReleveHeureModalComponent } from './releve-heure-modal/releve-heure-modal.component';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { StatutContratSignComponent } from './statut-contrat-sign/statut-contrat-sign.component';
import { SelectionModel } from '@angular/cdk/collections';
import { PldContratComponent } from './pld-contrat/pld-contrat.component';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../../../../services/user.service';
import { ClientService } from '../../../../../../../services/recruteur/client.service';
import { InterimService } from '../../../../../../../services/interim/interim.service';
import { MissionService } from '../../../../../../../services/mission.service';
import { PldService } from '../../../../../../../services/pld.service';
import { CandidatureService } from '../../../../../../../services/candidature.service';
import { NotificationService } from '../../../../../../../services/notification.service';
import { DomSanitizer } from '@angular/platform-browser';
import { RolesService } from '../../../../../../../services/roles.service';
import { RecruteurService } from '../../../../../../../services/recruteur/recruteur.service';
import { SellandsignService } from '../../../../../../../services/sellandsign.service';
import { DatePipe } from '@angular/common';
import { ClientPldModel } from '../../../../../../../models/interim/ClientPld.model';
import { HebergementServiceService } from 'src/shared/services/recruteur/hebergement.service.service';
import { ListRecruteurModalComponent } from '../../../client-wrapper/client/modal-client/list-recruteur-modal/list-recruteur-modal.component';
import { UpdateContratPldComponent } from '../../../../../../shared/update-contrat-pld/update-contrat-pld.component';
import { ModalCandidaturesComponent } from './modal-candidatures/modal-candidatures.component';
import { AcompteModalComponent } from './acompte-modal/acompte-modal.component';
import { UtilsService } from '../../../../../../../services/utils.service';
import { URL_ANNEXE_CONTRAT } from '../../../../../../../utils/server_api_url';

import $ from 'jquery';

@Component({
	selector: 'app-mission-medecin',
	templateUrl: './mission-medecin.component.html',
	styleUrls: [ './mission-medecin.component.css' ]
})
export class MissionMedecinComponent implements OnInit {
	urlAnnexeContrat = URL_ANNEXE_CONTRAT;
	form: FormGroup;
	tabIdCandidatures = [];
	idpld: number;
	missionMed: any;
	itemMissionMed: any;
	currentInterimaire: any;
	carecteristiquePoste: any;
	idInterimPld: number;
	idClientPld: string;
	adresseClient: string;
	detailMissionMed: string;
	titreModalCandidature: string;
	modalCandidatures: string;
	ContratCandidatures = [];
	listeContrats = [];
	idReferenceMM: string;
	client: any;
	colgestionMissions = 'col-md-12 col-sm-12 col-xs-12 col-lg-12';
	actionContrat = false;
	idIterim: number;
	dialogRef: MatDialogRef<ReleveHeureModalComponent>;
	window: any = window;
	displayedColumns = [ 'select', 'number', 'date', 'vacation', 'debut', 'fin', 'tarif', 'contrat-pld' ];
	displayedColumnslisteContrats = [ 'contrat-pld' ];
	listsource: any[] = [];
	uploadForm: FormGroup;
	currentUser: any;
	uploadFormContrat: FormGroup;

	statutContratSign: any;
	dialogRefContratSign: MatDialogRef<StatutContratSignComponent>;

	data = {
		id_ch: null,
		id_interim: null,
		id_recruteur: null,
		id_pld: null,
		apartenance: null,
		interim: false,
		reference_mission: null,
		hebergement: []
	};

	listSourcesContrats: any[] = [];
	loaderInterim = false;
	interim_id: number;

	dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
	dataSourceContrats: MatTableDataSource<any[]> = new MatTableDataSource();
	selection = new SelectionModel<any[]>(true, []);
	dialogueModalPldContrat: MatDialogRef<PldContratComponent>;
	dialogueUpdateModalPldContrat: MatDialogRef<UpdateContratPldComponent>;
	dialogueDemandeAcompte: MatDialogRef<AcompteModalComponent>;
	dialogueModalCandidatures: MatDialogRef<ModalCandidaturesComponent>;

	labelTab = 7;
	// variable qui definit l'apartenance d'un contrat c'est a dire pour un interimeraire ou CH
	apartenance_contrat: number = null;
	id_candidature: any;

	displayedColumnsMission: string[] = [
		'id',
		'reference',
		'hopital',
		'renumeration',
		'candidatures',
		'action'
	];
	dataSourceMission: MatTableDataSource<any[]> = new MatTableDataSource<any[]>([]);

	private paginator: MatPaginator;
	private sort: MatSort;

	@ViewChild(MatSort, { static: false })
	set matSort(ms: MatSort) {
		this.sort = ms;
	}

	@ViewChild(MatPaginator, { static: false })
	set matPaginator(mp: MatPaginator) {
		this.paginator = mp;
	}

	constructor(
		private route: ActivatedRoute,
		private userService: UserService,
		private clientService: ClientService,
		private interimService: InterimService,
		private missionService: MissionService,
		private pldService: PldService,
		private utilsService: UtilsService,
		private candidatureService: CandidatureService,
		private router: Router,
		private activatedRouter: ActivatedRoute,
		private notificationService: NotificationService,
		private formBuilder: FormBuilder,
		private sanitizer: DomSanitizer,
		public roleService: RolesService,
		private recruteurService: RecruteurService,
		private sellAndSign: SellandsignService,
		private dialog: MatDialog,
		private datePipe: DatePipe,
		private hebergementService: HebergementServiceService
	) {
		this.form = this.formBuilder.group({
			orders: new FormArray([])
		});
	}

	ngOnInit() {
    this.initData();
	}
	initData(){
    this.currentUser = this.userService.getCurrentUser();
    const idInterim = this.activatedRouter.snapshot.params['id'] ? this.activatedRouter.snapshot.params['id'] : this.currentUser.interim.id;
    this.interimService.labelTab_text.subscribe((index) => {
      if (index === 'missions') {
        this.notificationService.blockUiStart();
        this.interimService.missionByInterim(idInterim).subscribe(
          (response: any) => {
            if (response) {
              this.currentInterimaire = response['interim'];
              this.idIterim = Number(response['interim'].id);
              this.idInterimPld = response.idPld;
              this.missionMed = response.missionMed;
              this.dataSourceMission = new MatTableDataSource(this.missionMed);
              setTimeout(() => {
                this.dataSourceMission.paginator = this.paginator;
              }, 1000);
              this.itemMissionMed = Object.keys(this.missionMed);
            }
            this.notificationService.blockUiStop();
          },
          (error) => {
            this.notificationService.blockUiStop();
          }
        );
      }
    });
    this.currentUser = this.userService.getCurrentUser();
    if (this.currentUser && this.roleService.isCandidatUnique(this.currentUser.roles)) {
      this.displayedColumnsMission = [
        'id',
        'reference',
        'hopital',
        'renumeration',
        'candidatures',
        'action'
      ];
    }

    // ppp
    this.uploadFormContrat = this.formBuilder.group({
      contrat: []
    });
    // this.getSingatureIdForCandidature(1);
    this.uploadForm = this.formBuilder.group({
      profile: [ '' ]
    });

  }

	onModalCandidature(ref) {
		this.titreModalCandidature = ref;
		for (let i = 0; i < this.missionMed.length; i++) {
			if (this.missionMed[i].reference === ref) {
				this.modalCandidatures = this.missionMed[i].candidatures;
			}
		}
		$('#candidaturesModal').modal('show');
	}

	onDetailMM(mission) {
		this.ContratCandidatures = [];
		this.idReferenceMM = mission.reference;
		this.idClientPld = mission.idClientPld;
		// this.findClient(mission.idClient);

		this.detailMissionMed = 'Les détails de la mission:  ' + this.idReferenceMM;
		this.colgestionMissions = 'd-none';
		$('#detailMissionMed').removeClass('d-none');
		const tmp = this.missionMed.filter((x) => {
			if (x.reference.toLowerCase() === this.idReferenceMM.toLowerCase()) {
				this.listeContrats = [];
				this.carecteristiquePoste = ' Service ' + x.specialite;
				// this.notificationService.blockUiStart();
				// if (this.currentUser && this.roleService.isCommercial(this.currentUser.roles)) {
				//   this.creerClientPLD();
				// } else {
				//   this.notificationService.blockUiStop();
				// }
				this.adresseClient = x.adresseClient;
				const candidatures = x.candidatures;
				this.ContratCandidatures = candidatures.filter((y) => {
					if (y.etat === 'Validee' || y.etat === 'Rémunération') {
						if (y.idpld) {
							const items = { idMission: y.id, candidature: y };
							this.listeContrats.push(items);
						}
						return y;
					}
				});
			}
		});
		this.dataSource.data = this.ContratCandidatures;
		this.dataSourceContrats.data = this.supDoublonsSurContrats(this.dataSource.data);
		console.log(this.dataSource.data);
	}

	onFermeDetailMM() {
		this.detailMissionMed = '';
		this.colgestionMissions = 'col-md-12 col-sm-12 col-xs-12 col-lg-12 animated fadeIn';
		$('#detailMissionMed').addClass('d-none');
	}

	onProgress(etat) {
		let rep: number;
		if (etat === 'En Attente') {
			rep = 40;
		} else if (etat === 'Validee') {
			rep = 70;
		}
		return rep;
	}

	OnDemandeAcompte(item) {
		item;
		this.dialogueDemandeAcompte = this.dialog.open(AcompteModalComponent, {
			width: '40%',
			height: '70%',
			panelClass: 'myapp-no-padding-dialog',
			data: item
		});
	}

	findClient(idClient) {
		this.notificationService.blockUiStart();
		this.clientService.find_new(idClient).subscribe(
			(response) => {
				this.client = response['data'];
			},
			(error) => {
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	OnUpdateModalPld(item, el) {
		item.idMM = el.candidature.idMission;
		this.dialogueUpdateModalPldContrat = this.dialog.open(UpdateContratPldComponent, {
			width: '50%',
			height: '80%',
			panelClass: 'myapp-no-padding-dialog',
			data: item
		});
	}
	OnModalCandidatures(mission) {
		this.dialogueModalCandidatures = this.dialog.open(ModalCandidaturesComponent, {
			width: '60%',
			height: '60%',
			panelClass: 'myapp-no-padding-dialog',
			data: mission
		});
	}

	OnOuvreModalPld() {
		this.notificationService.blockUiStart();
		if (!this.onContratExiste() && this.currentUser.gestionnaire.IdReferentiel) {
			const data = {
				candidatures: this.selection.selected,
				carecteristiquePoste: this.carecteristiquePoste,
				interim: this.currentInterimaire,
				refMission: this.idReferenceMM,
				client: this.client
			};
			this.notificationService.blockUiStop();
			this.dialogueModalPldContrat = this.dialog.open(PldContratComponent, {
				width: '40%',
				height: '600px',
				panelClass: 'myapp-no-padding-dialog',
				data: data
			});
		} else if (!this.currentUser.gestionnaire.IdReferentiel) {
			this.notificationService.showNotificationEchecCopie(
				'bottomRight',
				"Vous pouvez pas créer de contrat si vous n'êtes pas encore inscrit sur PLD!"
			);
			this.notificationService.blockUiStop();
		} else {
			this.notificationService.showNotificationEchecCopie(
				'bottomRight',
				'Vous avez saisi une cadidature qui a déjà son contrat créer !'
			);
			this.notificationService.blockUiStop();
		}
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	onContratExiste(): boolean {
		let response = false;
		this.selection.selected.forEach((element) => {
			this.selection.selected;
			if (element['idpld'] !== null) {
				response = true;
			}
		});
		return response;
	}

  OnAfficheReleveHr(item) {
    this.dialogRef = this.dialog.open(ReleveHeureModalComponent, {
      width: '70%',
      height: '70%',

      panelClass: 'myapp-no-padding-dialog',
      data: item
    });
  }

	supDoublonsSurContrats(candidatures){
  const newCandidatures = [];
  let out = [];

    candidatures.forEach(candidature => {
	    if(candidature.contratPld) {
        newCandidatures.push(candidature);
      }
    });

  if(newCandidatures.length > 0){
    let obj = {};
    for (let i = 0; i < newCandidatures.length; i++) {
      let existe = false;
      if (i === 0 ) {
        out.push(newCandidatures[i]);
      } else {
        for (let j = 0; j < out.length; j++) {
          if (newCandidatures[i].contratPld !== null) {
            if(newCandidatures[i].contratPld.idPld === out[j].contratPld.idPld){
              existe = true;
            }
          }
        }
        if (!existe) {
          out.push(newCandidatures[i]);
        }
      }
    }
  }
    return out;
	}
}
