import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcompteModalComponent } from './acompte-modal.component';

describe('AcompteModalComponent', () => {
  let component: AcompteModalComponent;
  let fixture: ComponentFixture<AcompteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcompteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcompteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
