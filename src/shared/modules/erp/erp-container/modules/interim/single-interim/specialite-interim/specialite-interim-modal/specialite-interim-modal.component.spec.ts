import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialiteInterimModalComponent } from './specialite-interim-modal.component';

describe('SpecialiteInterimModalComponent', () => {
  let component: SpecialiteInterimModalComponent;
  let fixture: ComponentFixture<SpecialiteInterimModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialiteInterimModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialiteInterimModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
