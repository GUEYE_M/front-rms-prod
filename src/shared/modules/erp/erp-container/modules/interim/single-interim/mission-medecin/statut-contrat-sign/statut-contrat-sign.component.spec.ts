import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatutContratSignComponent } from './statut-contrat-sign.component';

describe('StatutContratSignComponent', () => {
  let component: StatutContratSignComponent;
  let fixture: ComponentFixture<StatutContratSignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatutContratSignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatutContratSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
