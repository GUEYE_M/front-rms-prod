import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatistiqueeComponent } from './statistiquee.component';

describe('StatistiqueeComponent', () => {
  let component: StatistiqueeComponent;
  let fixture: ComponentFixture<StatistiqueeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatistiqueeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatistiqueeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
