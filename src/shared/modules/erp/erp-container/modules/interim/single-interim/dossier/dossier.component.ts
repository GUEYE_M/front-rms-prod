import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { InterimService } from '../../../../../../../services/interim/interim.service';
import { NotificationService } from '../../../../../../../services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesService } from '../../../../../../../services/roles.service';
import { UserService } from '../../../../../../../services/user.service';
import { DossierService } from '../../../../../../../services/interim/dossier.service';
import { IMAGES_API_URL } from '../../../../../../../utils/images_api_url';
import { UtilsService } from '../../../../../../../services/utils.service';
import $ from 'jquery';

@Component({
	selector: 'app-dossier',
	templateUrl: './dossier.component.html',
	styleUrls: [ './dossier.component.css' ]
})
export class DossierComponent implements OnInit {
	private fileUrl = IMAGES_API_URL;
	displayedColumns: string[] = [ 'id', 'libelle' ];
	dataSource: MatTableDataSource<any[]>;
	addDiplomeEtat = false;
	@ViewChild(MatPaginator, { static: false })
	paginator: MatPaginator;
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	showDossierNow = false;
	labelTab = 1;
	placeholder = 'aucun fichier choisi';
	idIterim: number;
	adminOrInterim = 'admin';
	infosPerso: any = [];
	imgFiledefault = this.fileUrl + 'images/addfile.jpg';
	fileDifferentImage = this.fileUrl + 'images/fichier.png';
	ajout = false;
	fileValide: boolean;
	fd: FormData = new FormData();
	fdiplome: FormData = new FormData();
	files = [];
	dossierInterim: any;
	etat: boolean;
	dossierEnable: boolean;
	dossierEtat: boolean;
	soumettre_dossier: boolean;
	addDiplomes: any = [];
	diplomeForm: FormGroup;
	diplomesItemsForm: FormGroup;
	diplomeItems: any = [];
	loader = false;
	actiyeN = true;
	mail: boolean = true;
	currentUser: any;

	onMiseAjourFile = null;
	constructor(
		public dossierService: DossierService,
		private userService: UserService,
		private router: Router,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		public roleService: RolesService,
		public utilsService: UtilsService,
		private activatedRoute: ActivatedRoute,
		private notificationService: NotificationService,
		private interimService: InterimService
	) {}

	ngOnInit() {
		this.currentUser = this.userService.getCurrentUser();
		this.interimService.currentInterimaire.subscribe((response) => {
			if (response) {
				this.idIterim = Number(response.interim.id);
			}
		});
		this.interimService.labelTab_text.subscribe((label) => {
			if (label === 'dossier') {
				this.dossierService.showDossierSubject.subscribe((next) => {
					this.showDossierNow = next;
					this.notificationService.blockUiStop();
				});
				this.notificationService.blockUiStart();
				this.getData();
			}
		});
		this.initDiplomeForm();
		this.fileValide = null;
		if (this.activatedRoute.snapshot.params.id) {
			this.idIterim = this.activatedRoute.snapshot.params.id;
		} else {
			this.adminOrInterim = 'interim';
			this.idIterim = this.userService.getCurrentUser()['interim'].id;
		}
	}
	getData() {
		this.dossierService.find(this.idIterim).subscribe((rep) => {
			this.dossierService.showDossierSubject.next(true);
			const dossier: any = rep['data'];
			this.dossierInterim = JSON.parse(rep['dossier']);
			this.diplomeItems = JSON.parse(rep['diplome']);
			this.dataSource = new MatTableDataSource(this.diplomeItems);
			this.dataSource.paginator = this.paginator;
			// this.dataSource.sort = this.sort;
			this.infosPerso = Object.keys(this.dossierInterim);
			// supprimer les elements dans le tableau infosPerso
			const updatedAt = this.infosPerso.indexOf('updatedAt');
			if (updatedAt > -1) {
				this.infosPerso.splice(updatedAt, 1);
			}
			const diplome = this.infosPerso.indexOf('diplome');
			if (diplome > -1) {
				this.infosPerso.splice(diplome, 1);
			}
			const validitePieceIdentite = this.infosPerso.indexOf('validitePieceIdentite');
			if (validitePieceIdentite > -1) {
				this.infosPerso.splice(validitePieceIdentite, 1);
			}
			const etat = this.infosPerso.indexOf('etat');
			if (etat > -1) {
				this.infosPerso.splice(etat, 1);
			}
			const enable = this.infosPerso.indexOf('enable');
			if (enable > -1) {
				this.infosPerso.splice(enable, 1);
			}
			this.dossierEtat = this.dossierInterim.etat;
			this.dossierEnable = this.dossierInterim.enable;
			if (this.dossierEtat === true) {
				this.actiyeN = true;
			} else if (this.dossierEtat === false) {
				this.actiyeN = false;
			}
			this.fdiplome = new FormData();
		});
	}

	onUploader() {
		this.notificationService.onConfirm('Voulez-Vous modifier ce dossier ? ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notificationService.blockUiStart();
				const tableau = { id: this.idIterim, dossier: this.fd };

				this.dossierService.creer(tableau).subscribe(
					(response: any) => {
						if (!response.erreur) {
							this.ajout = false;
							this.notificationService.blockUiStart();
							this.dossierService.showDossierSubject.next(false);
							this.getData();
							this.notificationService.showNotificationSuccess(response.success);
						} else {
							this.notificationService.blockUiStart();
							this.dossierService.showDossierSubject.next(false);
							this.getData();
							this.files = [];
							this.fd = new FormData();
							this.initItemRows();
							this.initDiplomeForm();
							this.fdiplome = new FormData();
							this.notificationService.showNotificationEchec(response['erreur']);
						}
					},
					(err) => {
						this.notificationService.blockUiStop();
						// this.notificationService.showNotificationEchec( response['erreur']);
					}
				);
			}
		});
	}

	onAnnuleUploader() {
		this.ajout = false;
		this.notificationService.blockUiStart();
		this.dossierService.showDossierSubject.next(false);
		this.getData();
	}

	onSoumettre() {
		const status = $('input[id="soumettre"]');
		if (status.is(':checked')) {
			this.soumettre_dossier = true;
		} else {
			this.soumettre_dossier = false;
		}
	}

	initDiplomeForm() {
		this.diplomeForm = this.fb.group({
			diplomes: this.fb.array([ this.initItemRows() ])
		});
	}

	get formArr() {
		return this.diplomeForm.get('diplomes') as FormArray;
	}

	initItemRows() {
		this.diplomesItemsForm = this.fb.group({
			nom: [ '', Validators.required ]
		});
		return this.diplomesItemsForm;
	}

	addNewRow() {
		this.formArr.push(this.initItemRows());
	}

	deleteRow(index: number) {
		// this.formArr.removeAt(index);
		this.initDiplomeForm();
	}

	getFile(event, i) {
		if (event.target.files && event.target.files[0]) {
			const nameTmp = '#nameTmpDiplome' + i;
			const reader = new FileReader();
			const fileType = event.target.files[0].type.split('/');
			const file = event.target.files[0];
			// Pour vider le nom du fichier a la creation
			$(nameTmp).children('span').remove();
			reader.onload = (e: any) => {
				if (fileType.includes('video') === false) {
					this.diplomeForm.value.diplomes.forEach((element) => {
						$(nameTmp).append('<span>' + file.name + '</span>');
						this.fdiplome.append(element.nom, file);
					});
				} else {
					$(nameTmp).append('<span> Le type de fichier est invalide !</span>');
				}
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	}

	// Pour ajouter la ligne dajout dun nouveau diplome quand on clique sur ajouter un nouveau diplome dans la fenetre modal
	onAjoutDiplome() {
		let size = this.addDiplomes.length;
		this.addDiplomes.push(size);
	}

	//
	// Pour supprime la ligne dajout dun nouveau diplome quand on clique sur le button supprmer dans la fenetre modal
	//
	onAnnulerAjoutDiplome(index, objet) {
		objet.remove(index);
		this.addDiplomes.splice(index, 1);
		// this.fd.append(nomfile, files[0]);
	}
	onUploadDiplome() {
		const tableau = { id: this.idIterim, diplome: this.fdiplome };
		this.notificationService.blockUiStart();
		this.dossierService.creerDiplome(tableau).subscribe(
			(resp) => {
				this.notificationService.blockUiStop();
				if (!resp['erreur']) {
					this.initDiplomeForm();
					this.getData();
					this.files = [];
					this.initItemRows();
					this.initDiplomeForm();
					this.fdiplome = new FormData();
					this.notificationService.showNotificationSuccessCopie('bottomRight', resp['success']);
				} else {
					this.getData();
					this.files = [];
					this.fd = new FormData();
					this.initItemRows();
					this.initDiplomeForm();
					this.notificationService.showNotificationSuccessCopie('bottomRight', resp['erreur']);
				}
				this.dossierService.showDossierSubject.next(false);
			},
			(err) => {
				this.notificationService.showNotificationSuccessCopie(
					'bottomRigth',
					'Veuillez choisir un fichier valide SVP !'
				);
				this.notificationService.blockUiStop();
			}
		);
	}

	onCliqueInputDiplome(item) {
		const element: HTMLElement = document.getElementsByClassName(item)[0] as HTMLElement;
		element.click();
	}

	//
	// Pour supprime un diplome
	//
	onDeleteDiplomeElement(item) {
		const tableau = { id: this.idIterim, item: item };
		this.notificationService.onConfirm('Êtes-vous sûr de vouloir supprimer ?');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notificationService.blockUiStart();
				this.dossierService.deleteDiplome(tableau).subscribe(
					(response) => {
						this.dossierService.showDossierSubject.next(false);
						this.getData();
						// this.notificationService.blockUiStop();
						// if (this.adminOrInterim === 'admin') {
						//   this.interimService.labelTab.next(this.labelTab);
						//   this.router.navigate(['/RMS-Admin/redirectionMed/', this.idIterim, 'interim']);
						// } else {
						//   this.interimService.labelTab.next(this.labelTab);
						//   this.router.navigate(['RMS-Admin/redirection-admin/', 'Mes-Rubriques']);
						// }
						this.notificationService.showNotificationSuccessCopie(
							'bottomRight',
							'Suppression avec succès le diplome: ' + item + '!'
						);
					},
					(error1) => {
						this.notificationService.showNotificationEchec("echec de la supprission de l'élement !");
						this.notificationService.blockUiStop();
					}
				);
			}
		});
	}

	// =======================================================================================================================================
	// Pour supprimer un element du dossier
	onDeleteDossierElement(item) {
		const tableau = { id: this.idIterim, item: item };
		const rep = confirm('Etes-vous sur de vouloir supprimer?');
		//
		// Si l'utilisateur confirme la suppression on effectue le delete en base de donnees
		//
		if (rep === true) {
			this.notificationService.blockUiStart();
			this.dossierService.deleteDossier(tableau).subscribe(
				(response) => {
					if (!response['erreur']) {
						this.dossierService.showDossierSubject.next(false);
						this.getData();
						// this.notificationService.blockUiStop();
						// if (this.adminOrInterim === 'admin') {
						//   this.interimService.labelTab.next(this.labelTab);
						//   this.router.navigate(['/RMS-Admin/redirectionMed/', this.idIterim, 'interim']);
						// } else {
						//   this.interimService.labelTab.next(this.labelTab);
						//   this.router.navigate(['RMS-Admin/redirection-admin/', 'Mes-Rubriques']);
						// }
						this.notificationService.showNotificationSuccess(response['success']);
					} else {
						this.dossierService.showDossierSubject.next(false);
						this.getData();
						this.notificationService.showNotificationEchec(response['erreur']);
					}
				},
				(err) => {
					this.notificationService.showNotificationEchec("Echec de la suppression de l'élement !");
					this.notificationService.blockUiStop();
				}
			);
		}
	}

	onSoumetsDossier(val) {
		if (val === true) {
			this.actiyeN = true;
		}
		this.notificationService.blockUiStart();
		this.dossierService.soumettreDossier(this.idIterim).subscribe(
			(response: any) => {
				if (!response.erreur) {
					this.dossierEnable = true;
					this.notificationService.blockUiStop();
					this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
				} else {
					this.notificationService.blockUiStop();
					this.actiyeN = false;
					this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
				}
			},
			(erreur) => {
				this.actiyeN = false;
				this.notificationService.showNotificationEchecCopie(
					'bottomRight',
					"une erreur s'est produite lors de la soumission du dossier, veillez ressayer à nouveau!"
				);
				this.notificationService.blockUiStop();
			}
		);
	}

	onActiveBtnRadio(val, mail) {
		this.notificationService.blockUiStart();
		let dossierExiste = false;
		this.infosPerso.forEach((element) => {
			if (this.dossierInterim[element] !== null) {
				dossierExiste = true;
			}
		});
		if (dossierExiste) {
			this.notificationService.blockUiStart();
			this.dossierService.activer(this.idIterim, mail).subscribe((response) => {
				if (!response['erreur']) {
					this.dossierEtat = !this.dossierEtat;
					this.notificationService.blockUiStop();
					this.notificationService.showNotificationSuccessCopie('bottomRight', response['success']);
					this.dossierService.showDossierSubject.next(false);
					this.getData();
				} else {
					this.notificationService.blockUiStop();
					this.dossierService.showDossierSubject.next(false);
					this.getData();
					this.notificationService.showNotificationEchecCopie('bottomRight', response['erreur']);
				}
			});
		} else {
			this.dossierService.showDossierSubject.next(false);
			this.getData();
			this.notificationService.showNotificationEchecCopie(
				'bottomRight',
				'Vous ne pouvez pas activer ce dossier car il est vide !'
			);
		}
		if (val === true && dossierExiste) {
			this.actiyeN = true;
		} else if (val === false && dossierExiste) {
			this.actiyeN = false;
		}
		// this.utilsService.redirectEspace({espace: 'interim', id: this.idIterim});
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	addNewDiplome() {
		this.addDiplomeEtat = !this.addDiplomeEtat;
	}

	onMailNotification(val) {
		if (!this.actiyeN) {
			this.notificationService.onConfirm(
				// tslint:disable-next-line:max-line-length
				'Infos ! Souhaitez-vous envoyer un mail de notification ?'
			);

			this.notificationService.dialogRef.afterClosed().subscribe((response) => {
				this.onActiveBtnRadio(val, response);
			});
		} else {
			this.onActiveBtnRadio(val, false);
		}
	}

	onChargeFileDossier(event, item) {
		const nameTmp = '.nameTmp' + item;
		const nameTmpUpdate = '.nameTmpUpdate' + item;
		if (event.target.files && event.target.files[0]) {
			const reader = new FileReader();
			const fileType = event.target.files[0].type.split('/');
			const file = event.target.files[0];
			reader.onload = (e: any) => {
				// Pour vider le nom du fichier a la creation
				$(nameTmp).children('span').remove();
				// Pour vider le nom du fichier a la modification
				$(nameTmpUpdate).children('img').remove();
				$(nameTmpUpdate).children('span').remove();

				if (fileType.includes('video') === false) {
					$(nameTmp).append('<span>' + event.target.files[0].name + '</span>');
					$(nameTmpUpdate).append('<span>' + event.target.files[0].name + '</span>');
					this.ajout = true;
					this.fd.append(item, file);
				} else {
					$(nameTmp).append('<span> Le type de fichier est invalide !</span>');
					$(nameTmpUpdate).append('<span class="text-danger"> Type invalide ! </span>');
				}
			};
			// Permet de lire le fichier a l'etat temporaire
			reader.readAsDataURL(event.target.files[0]);
		}
	}
}
