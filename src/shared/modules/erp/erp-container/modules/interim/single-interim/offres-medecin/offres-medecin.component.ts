import {AfterViewInit, Component, Input, OnInit, ViewChild, Inject} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../../../../../../services/utils.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {FilterService} from '../../../../../../../services/filter.service';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {RolesService} from '../../../../../../../services/roles.service';
import {UserService} from '../../../../../../../services/user.service';
import {OffresServiceAll} from '../../../../../../../services/offres.service';
import {Router} from '@angular/router';
import {InterimService} from '../../../../../../../services/interim/interim.service';
import {CandidatureService} from '../../../../../../../services/candidature.service';
import {Range} from 'ngx-mat-daterange-picker';
import {MatDialog} from '@angular/material/dialog';
import {ModalPostulerComponent} from '../../../../../../offres/single-offre-site/modal-postuler/modal-postuler.component';
import {ModalPostulerInterimComponent} from '../../modal-interim/modal-postuler-interim/modal-postuler-interim.component';
import {BehaviorSubject} from "rxjs";


@Component({
  selector: 'app-offres-medecin',
  templateUrl: './offres-medecin.component.html',
  styleUrls: ['./offres-medecin.component.css']
})
export class OffresMedecinComponent implements OnInit, AfterViewInit {

  plannings = [];
  labelTab = 6;
  dataSourceInitial = [];
  plannings_modal = [];
  send_mail: boolean = true;
  enabled_candidature: boolean = false;
  pageSize: number = 12;
  condition_medecin = '';
  displayedColumns = ['number', 'nomEtablissement', 'qualif', 'reference', 'mois', 'annee', 'addresse', 'cp', 'dateenreg', 'action'];

  displayedColumnsFilter = [
    {
      name: 'etablissement',
      label: 'Nom Etablissement'
    },

    {
      name: 'mois',
      label: 'Mois'
    },
    {
      name: 'annee',
      label: 'Annee'
    },
    {
      name: 'reference',
      label: 'Reference'
    },

    {
      name: 'addresse',
      label: 'Addresse'
    },

    {
      name: 'specialite',
      label: 'Specialite'
    },
    {
      name: 'ville',
      label: 'Ville'
    },

    {
      name: 'departement',
      label: 'Departement'
    },
  ];

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null


  };
  allmois = [];
  range: Range = {fromDate: new Date(), toDate: new Date()};
  length: number = 0;
  lengthModal: number = 0;

  pageSizeOptions: number[] = [12, 25, 50, 100];

  pageSizeOptionsModale: number[] = [5, 12, 25];

  public formFilter: FormGroup;
  displayedColumnsModal = ['number', 'select', 'date', 'libelle', 'debut', 'fin', 'tarif'];
  @Input() listesOffress = [];
  dataSource: MatTableDataSource<any[]>;
  dataSourceModal: MatTableDataSource<any[]> = new MatTableDataSource();
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatSort, {static: false}) sortModal: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  @ViewChild('listPaginatorModal', {static: false}) paginatorModal: MatPaginator;
  mission_choice = [];
  offres = [];
  etat_dossier_interim: boolean = false;
  p = 0;
  verifieLesCandidaturesDeLinterimaire = 0;
  nbreOffres = -1;
  loaderlisteOffreMedecin = false;
  mission_id: number;
  interim_id: number;
  currentUser: any;
  plannings_for_modal = [];
  selection = new SelectionModel<any[]>(true, []);

  constructor(
    private candidatureService: CandidatureService,
    private interimService: InterimService,
    private router: Router,
    public dialog: MatDialog,
    private offreService: OffresServiceAll,
    private userService: UserService,
    private rolesService: RolesService,
    private formBuilder: FormBuilder,
    private filterService: FilterService,
    private notifierService: NotificationService,
    private utilsService: UtilsService) {
  }

  ngOnInit() {
    this.offreService.listeDesOffresSubject = new BehaviorSubject([]);
    this.currentUser = this.userService.getCurrentUser();
    this.allmois = this.utilsService.getMois();
    // this.etat_dossier_interim = this.listesOffress[0].etat_dossier;
    // this.dataSource = new MatTableDataSource(this.listesOffress);
    // this.dataSourceInitial = this.listesOffress;
    // this.dataSource.filterPredicate = this.createFilter();
    // }
    //  recuperation de l'id du linterimaire choisi
    this.interimService.currentInterimaire.subscribe(response => {
      if (response) {
        // (response);
        this.interim_id = response.interim.id;
        this.interimService.labelTab_text.subscribe(
          (val) => {
            if (val === 'offres') {
              this.offreService.listeDesOffresSubject = new BehaviorSubject([]);
              this.interimService.getDossierInterim(this.interim_id).subscribe(
                (next: boolean) => {
                  this.etat_dossier_interim = next;
                }
              );
              // this.notifierService.blockUiStart();
              // this.offreService.getJsonOffres(this.interim_id).subscribe(
              //   (response) => {
              //     this.notifierService.blockUiStop();
              //     // this.etat_dossier_interim = response[0].etat_dossier;
              //     this.offreService.listeDesOffresSubject.next(response);
              //     // (this.etat_dossier_interim);
              //   }
              // );
              this.getData();
              this.offreService.listeDesOffresSubject.subscribe(
                (x: any) => {
                  this.dataSource = new MatTableDataSource(x.data);
                  this.length = x.lenght;
                  this.dataSource.paginator = this.paginator;
                });
            }
          }
        );
      }
    });
  }

  onShowModalPostuler(item, plannings) {

    this.mission_id = item.mission_id;
    this.plannings_for_modal = [];
    // if(this.verifieLesCandidaturesDeLinterimaire !== this.mission_id)
    //     this.candidatureService.get_candidature_interim(this.interim_id, this.mission_id);
    this.mission_choice = item.plannings;
    // on filtre les offres en supprimant tous les plannings sur les quelles linterimaire a candidadter
    // this.candidatureService.candidatureInterimSubject.subscribe(
    //     (next) => {
    //         if (next.length > 0) {
    //             next.filter((item: any) => {
    //                 plannings.forEach(function (element) {
    //                     if ((item.id_mission == element[0].id_mission) &&
    //                         (item.libelleGarde == element[0].libelleGarde) &&
    //                         (item.heureFin == element[0].heureFin) &&
    //                         (item.heureDebut == element[0].heureDebut) &&
    //                         (item.datePlanning == element[0].datePlanning) &&
    //                         (item.salaire == element[0].salaire) &&
    //                         (item.tarif == element[0].tarif)
    //                     ) {
    //                         plannings.splice(plannings.indexOf(element), 1);
    //                         // (plannings);
    //                     }
    //                 });
    //             });
    //             this.plannings_for_modal = plannings;
    //             this.dataSourceModal.data = plannings;
    //         } else {
    //
    //         }
    //     }
    // );
    this.plannings_for_modal = plannings;
    this.dataSourceModal.data = plannings;
    this.dataSourceModal.paginator = this.paginatorModal;
    this.dataSourceModal.sort = this.sortModal;
    this.nbreOffres = item.mission_id;
    this.verifieLesCandidaturesDeLinterimaire = item.mission_id;
  }

  showModalPostuler(plannings) {
    const dialogRef = this.dialog.open(ModalPostulerInterimComponent, {
      width: '60%',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: {
        mission_choisie: this.mission_choice,
        enable: this.etat_dossier_interim,
        plannings_: plannings, id_interim: this.interim_id
      }
    });
  }

  clearNombreOffres() {
    this.nbreOffres = -1;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceModal.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceModal.data.length;
    return numSelected === numRows;
  }

  clearSelection() {
    this.selection = new SelectionModel<any[]>(true, []);
  }


  // ===================================================================================================================================
  ngAfterViewInit() {
    this.dataSourceModal.paginator = this.paginatorModal;
    this.dataSourceModal.sort = this.sortModal;
  }

  roleInterim() {
    return this.rolesService.isCandidat(this.currentUser.roles);
  }

  roleRecruteur() {
    return this.rolesService.isRecruteur(this.currentUser.roles);
  }

  roleSuperAdmin() {
    return this.rolesService.isCommercial(this.currentUser.roles);
  }


  valutElements() {


    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.getData();
    }


  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre(form: NgForm) {

    this.data.filter = form.value;

    // (form.value)


    this.getData();

  }

  pagination(event: any) {
    (this.pageSize);
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getData();


  }

  getData() {
    this.notifierService.blockUiStart();
    this.offreService.getJsonOffres(this.interim_id, this.data).subscribe(
      (response: any) => {
        this.notifierService.blockUiStop();
        if (!response.erreur) {
          // this.etat_dossier_interim = response[0].etat_dossier;
          this.offreService.listeDesOffresSubject.next(response.data);

          this.length = response.data.lenght;
          this.dataSource.paginator = this.paginator;
        } else {
          // this.notifierService.showNotificationEchec(response.erreur);
        }
      }
    );
  }

}
