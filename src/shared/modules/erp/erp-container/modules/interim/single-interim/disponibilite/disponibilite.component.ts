import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { from } from 'rxjs';
import { NotificationService } from '../../../../../../../services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RedirectionSercive } from '../../../../../../../services/redirection.sercive';
import { InterimService } from '../../../../../../../services/interim/interim.service';
import { DisponibiliteService } from '../../../../../../../services/interim/disponibilite.service';
import * as moment from 'moment';
import { ModalDisponibiliteGestionComponent } from '../../modal-interim/modal-disponibilite-gestion/modal-disponibilite-gestion.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../../../../../../../services/user.service';
import { NgDatePikerService } from '../../../../../../../services/ngDatePiker.service';
import { NgbCalendar, NgbDate, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from 'src/shared/services/utils.service';

moment.locale('fr');
moment.locale('fr-Fr');
declare var $: any;

@Component({
	selector: 'app-disponibilite',
	templateUrl: './disponibilite.component.html',
	styleUrls: [ './disponibilite.component.css' ]
})
export class DisponibiliteComponent implements OnInit {
	labelTab = 4;
	enableValider = false;
	showLoaderInit: boolean = true;
	showLoaderDispo_date_periode: boolean = true;
	showLoaderSend: boolean = false;
	initialCount = [];
	initialCount_fr = [];
	adminOrInterim = 'admin';
	minDate = new Date();
	public myForm: FormGroup;
	mois = [];
	month: any;
	loaderInterim;
	interim_id = 0;
	loaderValiderDispoPeriode = true;
	listeDisponiblites = [];
	listDateForMonth: any;
	highlightDays: any = [];
	listesOffresMatching = [];
	hoveredDate: NgbDate;
	message_notif = '';
	mail: boolean = true;
	dateDebut: NgbDate;
	dateFin: NgbDate;

	constructor(
		private disponibiliteservice: DisponibiliteService,
		private router: Router,
		public dialog: MatDialog,
		public interimService: InterimService,
		private userService: UserService,
		private formBuilder: FormBuilder,
		private utilService: UtilsService,
		private activatedRoute: ActivatedRoute,
		private notifierService: NotificationService,
		private calendar: NgbCalendar,
		public formatter: NgbDateParserFormatter
	) {
		this.dateDebut = calendar.getToday();
		this.dateFin = calendar.getNext(calendar.getToday(), 'd', 10);
	}

	ngOnInit() {
		this.message_notif = this.utilService.messageNotifMail();
		this.month = moment();
		if (!this.activatedRoute.snapshot.params.id) {
			this.adminOrInterim = 'interim';
		}
		this.interimService.labelTab_text.subscribe((val) => {
			if (val === 'disponibilite') {
				//  recuperation de l'id du linterimaire choisi
				this.interimService.currentInterimaire.subscribe((response) => {
					if (response) {
						// (response);
						this.interim_id = response.interim.id;
						this.getData(this.interim_id);
					}
				});
				this.disponibiliteservice.listeDisponibilitesForInterimSubject.subscribe((next) => {
					this.highlightDays = next;
				});
				if (this.activatedRoute.snapshot.params.id) {
					this.disponibiliteservice.interimIdDis = this.activatedRoute.snapshot.params.id;
				} else {
					const user = this.userService.getCurrentUser();
					this.disponibiliteservice.interimIdDis = user.interim.id;
				}
			}
		});
		this.initForm();
		this.myForm.get('periode_Date').valueChanges.subscribe((X) => {
			this.enableValider = true;
		});
	}

	getData(interim_id) {
		moment.locale('fr');
		moment.locale('fr-Fr');
		this.disponibiliteservice.getDisponibilite(interim_id).subscribe((disponibilite: any) => {
			if (disponibilite) {
				this.showLoaderInit = false;
				const tmp = [];
				this.interimService.listes_mois_disponibilite = [];
				this.initialCount = [];
				const tmpMois = [];
				disponibilite.forEach(function(elememt) {
					tmp.push({
						date: new Date(elememt.date),
						css: 'stay-dates',
						selectable: false,
						title: 'Holiday time !'
					});
					tmpMois.push(elememt);
				});
				const source = from(tmpMois);
				const data = source
					.pipe(
						groupBy((moisG) => moisG.mois),
						// return each item in group as array
						mergeMap((group) => group.pipe(toArray()))
					)
					.subscribe((val) => this.interimService.listes_mois_disponibilite.push(val));
				this.disponibiliteservice.listeDisponibilitesForInterimSubject.next(tmp);
			}
		});
	}

	initForm() {
		moment.locale('fr');
		moment.locale('fr-Fr');
		this.myForm = this.formBuilder.group({
			periode_Date: [ { start: '', end: '' }, Validators.required ]
		});
	}

	//permet de sauvergarder une date
	onSaveDisponibilite(content, interim_id, mail = true) {
		this.notifierService.onConfirm();
		this.notifierService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notifierService.blockUiStart();
				const tmp = [];
				content.forEach((x) => {
					const mois = x.format('MM-YYYY');
					tmp.push({ date: x.format('DD-MM-YYYY'), mois: mois, interim: interim_id, mail: mail });
				});
				this.disponibiliteservice.create(tmp).subscribe(
					(next: any) => {
						if (next.erreur) {
							this.notifierService.showNotificationEchecCopie('topCenter', next.erreur);
							this.showLoaderSend = false;
							this.notifierService.blockUiStop();
						} else {
							this.notifierService.showNotificationSuccessCopie('topCenter', next.success);
							this.highlightDays = [];
							this.initialCount = [];
							this.initForm();
							this.enableValider = false;
							this.getData(interim_id);
							this.notifierService.blockUiStop();
						}
					},
					(error) => {
						this.notifierService.showNotificationSuccessCopie('bottomRight', 'Erreur');
						this.highlightDays = [];
						this.initialCount = [];
						this.getData(interim_id);
						this.notifierService.blockUiStop();
					}
				);
			} else {
				this.highlightDays = [];
				this.initialCount = [];
				this.getData(interim_id);
			}
		});
	}

	onMailNotification(content, interim_id) {
		this.notifierService.onConfirm(
			// tslint:disable-next-line:max-line-length
			'Infos ! Souhaitez-vous envoyer un mail de notification ?'
		);

		this.notifierService.dialogRef.afterClosed().subscribe((response) => {
			this.onSaveDisponibilite(content, interim_id, response);
		});
	}

	transform_dates_fr() {
		this.initialCount_fr = [];
		moment.locale('fr-Fr');
		if (this.initialCount.length > 0) {
			const date_fr = moment(this.initialCount[0].format('L'));
			this.initialCount.forEach((element) => {
				this.initialCount_fr.push(moment(element));
			});
		}
	}

	onValiderDisponibiliteByPeriode() {
		const monthDebut: any =
			this.dateDebut.month.toString().length === 1 ? '0' + this.dateDebut.month.toString() : this.dateDebut.month;
		const monthFin: any =
			this.dateFin.month.toString().length === 1 ? '0' + this.dateFin.month.toString() : this.dateFin.month;
		const dateDebut: any = '' + this.dateDebut.day + '-' + monthDebut + '-' + this.dateDebut.year;
		const dateFin: any = '' + this.dateFin.day + '-' + monthFin + '-' + this.dateFin.year;
		this.notifierService.onConfirm();
		this.notifierService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notifierService.blockUiStart();
				moment.locale('fr-Fr');
				let interim_id = this.interim_id;
				const tmp: any = [];
				tmp.push({
					date: dateDebut,
					mois: monthDebut + '-' + this.dateDebut.year,
					interim: interim_id,
					mail: this.mail
				});
				tmp.push({
					date: dateFin,
					mois: monthFin + '-' + this.dateDebut.year,
					interim: interim_id,
					mail: this.mail
				});
				this.disponibiliteservice.create_periode(tmp).subscribe(() => {
					this.notifierService.showNotificationSuccessCopie(
						'topCenter',
						'vos disponibilités ont été bien enrégistrées !'
					);
					this.disponibiliteservice.disponibiliteSubject.next(null);
					this.loaderValiderDispoPeriode = true;
					this.showLoaderDispo_date_periode = true;
					this.highlightDays = [];
					this.showLoaderDispo_date_periode = true;
					this.initialCount = [];
					this.getData(interim_id);
					this.notifierService.blockUiStop();
				});
			}
		});
	}

	onShowDisponibiliteForMonth(DateForMonth) {
		this.disponibiliteservice.listeDisponibiliteChoisiSubject.next(DateForMonth);
		this.listDateForMonth = DateForMonth;
		this.disponibiliteservice.dialogRef = this.dialog.open(ModalDisponibiliteGestionComponent, {
			width: '700px',
			height: '500px',
			panelClass: 'myapp-no-padding-dialog',
			data: 1
		});

		// (DateForMonth);
	}

	onDeleteDisponibilite(item) {
		this.notifierService.onConfirm();
		this.notifierService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notifierService.blockUiStart();
				this.disponibiliteservice.deleteMoisDisponibilite(this.interim_id, JSON.stringify(item[0])).subscribe(
					(val: any) => {
						this.notifierService.blockUiStop();
						this.disponibiliteservice.disponibiliteSubject.next(null);
						this.highlightDays = [];
						this.initialCount = [];
						this.getData(this.interim_id);
						if (val.erreur) {
							this.notifierService.showNotificationEchecCopie('topCenter', val.erreur);
						} else {
							this.notifierService.showNotificationSuccessCopie('topCenter', val.success);
						}
					},
					(error1) => {
						this.notifierService.blockUiStop();
					}
				);
			} else {
			}
		});
	}

	onShowMatchingForMYRMS(item) {
		this.notifierService.blockUiStart();
		this.disponibiliteservice.dateDebut = item[0].mois;
		this.interimService.getNomSpecialitesForInterim(this.interim_id).subscribe((data) => {
			this.notifierService.blockUiStop();
			this.interimService.listesNomSpeciliatesForInterim.next(data);
			// moment.locale('fr-Fr');
			// $('#myModalDispobiliteMatching').modal({});
			this.disponibiliteservice.dialogRef = this.dialog.open(ModalDisponibiliteGestionComponent, {
				width: '900px',
				height: '500px',
				panelClass: 'myapp-no-padding-dialog',
				data: 2
			});
		});
		// const mois = moment(item[0].date).format('MMMM');
		// this.disponibiliteservice.getDisponibiliteMatchingMyRMS(this.interim_id, mois).subscribe(
		//   (data: any) => {
		//     this.listesOffresMatching = data;
		//     this.disponibiliteservice.listeDisponibiliteOffreMatchingSubject.next([]);
		//     this.disponibiliteservice.listeDisponibiliteOffreMatchingSubject.next(data);
		//
		//
		//   }
		// );
	}

	// fonction pour le ngDatepiker
	onDateSelection(date: NgbDate) {
		if (!this.dateDebut && !this.dateFin) {
			this.dateDebut = date;
		} else if (this.dateDebut && !this.dateFin && date.after(this.dateDebut)) {
			this.dateFin = date;
		} else {
			this.dateFin = null;
			this.dateDebut = date;
		}
	}

	isHovered(date: NgbDate) {
		return (
			this.dateDebut &&
			!this.dateFin &&
			this.hoveredDate &&
			date.after(this.dateDebut) &&
			date.before(this.hoveredDate)
		);
	}

	isInside(date: NgbDate) {
		return date.after(this.dateDebut) && date.before(this.dateFin);
	}

	isRange(date: NgbDate) {
		return date.equals(this.dateDebut) || date.equals(this.dateFin) || this.isInside(date) || this.isHovered(date);
	}

	validateInput(currentValue: NgbDate, input: string): NgbDate {
		const parsed = this.formatter.parse(input);
		return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
	}
}
