import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHebergementModalComponent } from './list-hebergement-modal.component';

describe('ListHebergementModalComponent', () => {
  let component: ListHebergementModalComponent;
  let fixture: ComponentFixture<ListHebergementModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHebergementModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHebergementModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
