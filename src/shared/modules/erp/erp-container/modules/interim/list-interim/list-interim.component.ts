import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogRef, MatDialog, PageEvent } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { from } from 'rxjs/internal/observable/from';
import { map, retry, startWith, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { EditInterimComponent } from '../edit-interim/edit-interim.component';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import { Interim } from '../../../../../../models/interim/Interim.model';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { InterimService } from '../../../../../../services/interim/interim.service';
import { UserService } from '../../../../../../services/user.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { UtilsService } from '../../../../../../services/utils.service';
import { FormEmailComponent } from '../single-interim/form-email/form-email.component';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { ListProspectionModalComponent } from '../../prospection/list-prospection-modal/list-prospection-modal.component';
import { FromEmailComponent } from '../../client-wrapper/client/modal-client/from-email/from-email.component';
import { TransactionModalComponent } from '../../sms/transaction/transaction-modal/transaction-modal.component';
import { SendinblueService } from 'src/shared/services/sendinblue.service';
import { DatePipe } from '@angular/common';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { GestionnaireInterneService } from '../../../../../../services/erp/gestionnaire-interne.service';
import { FilterService } from '../../../../../../services/filter.service';
import { PldService } from '../../../../../../services/pld.service';
import { ConstitutionDossierModalComponent } from '../modal-interim/constitution-dossier-modal/constitution-dossier-modal.component';
import { RolesService } from 'src/shared/services/roles.service';
import { ViewMessageProdpectionModalComponent } from '../../prospection/view-message-prodpection-modal/view-message-prodpection-modal.component';
import { RessourceService } from 'src/shared/services/erp/ressource.service';
import { HttpClient } from '@angular/common/http';
import { ClientService } from 'src/shared/services/recruteur/client.service';
import { Observable } from 'rxjs';

declare var $: any;

@Component({
	selector: 'app-list-interim',
	templateUrl: './list-interim.component.html',
	styleUrls: ['./list-interim.component.css']
})
export class ListInterimComponent implements OnInit, OnDestroy, AfterViewInit {
	formFilter: FormGroup;
	clientlist: Interim[];
	dialogRef: MatDialogRef<EditInterimComponent>;
	dialogRef_form_email: MatDialogRef<FormEmailComponent>;
	dialogRef_form_sms: MatDialogRef<TransactionModalComponent>;
	dataSourceInitial = [];
	interimPld = [];
	interimForm: FormGroup;

	view: boolean = false;
	actionInput: Boolean = false;
	action: string;
	dataSource: any = new MatTableDataSource();
	selection = new SelectionModel<any[]>(true, []);

	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;
	filteredOptions: Observable<any[]>;

	@ViewChild(SelectAutocompleteComponent, { static: false }) multiSelect: SelectAutocompleteComponent;
    
	
	

	pageSize = 12;
	currentUser: any;

	listActions: any[] = [
		{ value: 2, viewValue: 'Envoyer E-mail' },
		{ value: 7, viewValue: 'Envoyer SMS' },
		{ value: 3, viewValue: 'Activer MyRMS' },
		{ value: 4, viewValue: 'Desactiver MyRMS' },
		{ value: 5, viewValue: 'liste-rouge' },
		{ value: 6, viewValue: 'liste-vert' },
		{ value: 8, viewValue: 'Envoyer Identifiants' },
		{ value: 9, viewValue: 'Demande de dossier' },
		{ value: 10, viewValue: 'Demande de disponibilité' },
		{ value: 12, viewValue: 'Demande de coordonnees télephonque' },
		{ value: 11, viewValue: 'E-mail proposition collaboration' }
	];
	ordre: 'DESC' | 'ASC' = 'ASC';

	listSpecialites = [];
	listGestionnaires = [];

	pageEvent: PageEvent;

	range: Range = { fromDate: new Date(), toDate: new Date() };

	date_debut: number = null;
	date_fin: number = null;

	viewdatepiker: boolean = false;
	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];

	length: number = 0;
	pageSizeOptions: number[] = [12, 25, 50, 100, 150,300,500,1000];

	displayedColumns = [
		'select',
		'number',
		'nom',
		'email',
		'telephone',
		'statut',
		'contact',
		'ville',
		'actif',
		'inscription',
		'Etat',
		'date',
		'last_login',
		'Action'
	];

	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null,
		date_debut: null,
		date_fin: null,
		login_date_debut: null,
		login_date_fin: null,
		ordre: this.ordre
	};
	displayedColumnsFilter = [];

	profileForm = new FormGroup({
		selected: new FormControl([])
	});
	listsource: any[] = [];
	showLoadInterim: boolean;
	idInterim = 0;
	loader = 0;
	ModalUpdateInterim: any;
	interimPldToErp = false;
	size = 5001;
	limits = [];
	listClientFilter = [];


	constructor(
		private interimService: InterimService,
		private filterService: FilterService,
		private fb: FormBuilder,
		private acticatedRoute: ActivatedRoute,
		private userService: UserService,
		private notificationService: NotificationService,
		private router: Router,
		private notifierService: NotificationService,
		private utilService: UtilsService,
		private pldService: PldService,
		private dialog: MatDialog,
		private specialiteService: SpecialiteService,
		private sendInblueeService: SendinblueService,
		private datePipe: DatePipe,
		private rolesService: RolesService,
		private gestionnaireInterneService: GestionnaireInterneService,
		private ressourceService: RessourceService,
		private http: HttpClient,
		private clientService:ClientService
	) { }

	ngOnInit() {

		
		this.currentUser = this.userService.getCurrentUser();
		this.displayedColumnsFilter = this.interimService.displayedColumnsName();
		this.onGetLimit();
		this.formFilter = this.fb.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		if (this.elements.indexOf('date') === 1) {
			this.elements.splice(this.elements.indexOf('date'), 1);
		}
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
				this.getNewData();
			}
		}
		this.initInterimForm();
		this.initData();
		this.dataSource.sort = this.sort;
		this.specialiteService.list(null, true).subscribe((response: any) => {
			this.dataAutoCompleteFilter(response.data, 1);
		});

		this.gestionnaireInterneService.allGestionnaireInterne().subscribe((response) => {
			this.listGestionnaires = response;
		});



		this.allSources()


		this.datepickerConfig();

		if (this.roleSuperAdmin()) {
			this.listActions.push({
				value: 13,
				viewValue: 'Archiver'
			});
		}
	}

	voirInterimPld() {
		this.utilService.updateIdInterimPLD(this.interimPld).subscribe((resp) => {
			resp;
		});
	}
	allSources() {

		this.ressourceService.list({ filter: null }).subscribe((response: any) => {
			if (!response.erreur) {
				this.listsource = response.data;
			}
		});
	}
	roleSuperAdmin() {
		return this.rolesService.isCommercial(this.currentUser.roles);
	}
	initData() {
		this.notificationService.blockUiStop();
		if (this.acticatedRoute.snapshot.data['listInterims']['erreur']) {
			this.notificationService.showNotificationEchec(this.acticatedRoute.snapshot.data['listInterims']['erreur']);
			this.interimService.listInterim = this.acticatedRoute.snapshot.data['listInterims']['data'];
		} else {
			this.interimService.listInterim = this.acticatedRoute.snapshot.data['listInterims']['data'];
		}
		this.interimService.interimSubject.next(this.interimService.listInterim);
		// (this.interimService.listInterim);
		this.dataSourceInitial = this.interimService.listInterim;

		this.dataSource = new MatTableDataSource(this.interimService.listInterim);
		this.dataSource.sort = this.sort;
		this.length = this.acticatedRoute.snapshot.data['listInterims']['lenght'];


	}

	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.data.date_debut = null;
			this.data.date_fin = null;
			this.data.login_date_debut = null;
			this.data.login_date_fin = null;

			this.initData();
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} 
		
		else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
			this.elements.map((item) => {
				if (item === 'chAEviter') {
				    this.getClients()
					this.filteredOptions = this.formFilter.controls['chAEviter'].valueChanges.pipe(
						startWith(''),
						startWith(''),
						map(value => (typeof value === 'string' ? value : value.display)),
						map(name => (name ? this._filter(name) : this.listClientFilter.slice())),
					  );
					
				}
				
				
			
			});
		}
	}

	private _filter(name: string): any[] {
		const filterValue = name.toLowerCase();

	
		return this.listClientFilter.filter(option => option.display.toLowerCase().includes(filterValue));
	  }
getClients(){
	this.clientService.listClients(null, true).subscribe((response: any) => {
		if (response) {

			from(response.data).pipe(
				map(
				  (item: any) => {
					return {
					  display: item.nom_etablissement,
					  value: item.id,
		
					};
		
				  }),
				tap((el) => {
				  this.listClientFilter.push(el);
				})
			  ).subscribe();
			 
			}
		  });
}
	pagination(event: any) {
		this.pageSize = event.pageSize;
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = this.pageSize;
		this.getNewData();
	}

	dataAutoCompleteFilter(data: any, type: number) {
		from(data)
			.pipe(
				map((item: any) => {
					if (type == 1) {
						return {
							display: item.nomspecialite,
							value: item.id
						};
					}
				}),
				tap((el) => {
					if (type == 1) {
						this.listSpecialites.push(el);
					}
				})
			)
			.subscribe();
	}


	// fonction qui envoie le formulaire de filtre en base de donnee

	onSubmitFiltre() {

		//console.log(this.multiSelect2.selectedValue)
		if (this.formFilter.controls['specialite']) {
			this.formFilter.controls['specialite'].setValue(this.multiSelect.selectedValue);
		}
		// if (this.formFilter.controls['chAEviter']) {
		
		// 	this.formFilter.controls['chAEviter'].setValue(this.chaEviter);
		// }


		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = this.formFilter.value;

		// if (form.control.controls["date"]) {
		//   form.control.controls["date"].setValue(
		//     this.datePipe.transform(form.control.controls["date"].value, 'yyyy-MM-dd')
		//   );
		// }
		this.data.filter = this.formFilter.value;

		this.data.ordre = this.ordre;

		this.selection.clear()

		this.getNewData();
	}

	getNewData() {
		this.notificationService.blockUiStart();
		this.interimService.getAllInterimCopie(this.data).subscribe(
			(response: any) => {
				
				this.dataSource = new MatTableDataSource(response['data']);
				this.dataSource.sort = this.sort;
				this.length = response['lenght'];
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	onActifOuInactif(utilisateur, action) {
		this.notificationService.onConfirm('Êtes-vous sûr de vouloir continuer ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				utilisateur.actif = !utilisateur.actif;
				this.idInterim = utilisateur.id;
				this.interimService.actifOuInactif(utilisateur).subscribe(
					(response) => {
						this.notificationService.blockUiStop();
						if (!response['erreur']) {
							this.notificationService.showNotificationSuccess(response['success']);
						} else {
							this.notificationService.showNotificationEchec(response['erreur']);
						}
					},
					(error) => {
						this.notificationService.blockUiStop();
					},
					() => {
						this.utilService.redirectInterimListe();
					}
				);
			} else {
				this.utilService.redirectInterimListe();
			}
		});
	}

	initInterimForm() {
		this.interimForm = this.fb.group({
			nom: [],
			prenom: [],
			telephone: [],
			civilite: [],
			username: [],
			email: [],
			enabled: [],
			fixe: [],

			portable: [],
			num_r_p_p_s: [],
			inscription: [],
			statut: [],
			remarque: [],
			autre_remarques: [],
			description: [],
			coef_i_k: [],
			num_secu: [],
			rayon_intervention: [],
			actif: []
		});
	}

	OnUpdateInterimaire(idInterim: number = null) {
		const donnees = { id: idInterim, profil: false };
		this.dialogRef = this.dialog.open(EditInterimComponent, {
			width: '80%',
			height: '90%',
			panelClass: 'myapp-no-padding-dialog',
			data: donnees
		});

		this.dialogRef.afterClosed().subscribe((response) => {
			if (response) {
				if (!response.profil) {
					this.getNewData();
					this.selection.clear();
				}
			}
		});
	}

	filtrer(filtre: string) {
		filtre = filtre.trim();
		filtre = filtre.toLowerCase();
		this.dataSource.filter = filtre;
	}

	onActiveOuDesactiveUser(idUser, action, item) {
		const thiss = this;
		let donnees = { id: idUser, etat: action, inscription: null };
		if (item) {
			donnees = { id: idUser, etat: action, inscription: item };
		}

		this.notificationService.onConfirm('Êtes-vous sûr de vouloir continuer ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				action = JSON.parse(action);
				thiss.notificationService.blockUiStart();
				thiss.userService.activerOuDesactive(donnees).subscribe(
					(response) => {
						thiss.notificationService.blockUiStop();
						if (!response['erreur']) {
							thiss.notificationService.showNotificationSuccessCopie('bottomRight', response['success']);
						} else {
							thiss.notificationService.showNotificationEchecCopie('bottomRight', response['erreur']);
						}
					},
					(error) => {
						thiss.notificationService.blockUiStop();
					},
					() => {
						this.utilService.redirectInterimListe();
					}
				);
			} else {
				this.utilService.redirectInterimListe();
			}
		});
	}

	onEnregistreModificationInterim() {
		const fermeModal: HTMLElement = document.getElementsByName('fermeModal')[0] as HTMLElement;
		if (this.interimForm.touched) {
			let tableau = [];
			let keys = Object.keys(this.interimForm.value);
			for (let key of keys) {
				if (this.interimForm.value[key] !== null) {
					tableau.push(this.interimService.attributSansUnderscore(key));
					tableau.push(this.interimForm.value[key]);
				}
			}
			this.interimService.updateItem(tableau, this.ModalUpdateInterim.id).subscribe((response) => {
				if (!response['erreur']) {
					fermeModal.click();
					this.notificationService.showNotificationSuccess(response['success']);
					this.router.navigate(['/RMS-Admin/redirection-admin/', 'interim/list']);
				} else {
					this.notificationService.showNotificationEchec(response['erreur']);
				}
			});
		}
	}

	ngOnDestroy() {
		this.interimService.showLoadInterim.next(true);
	}

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	actionbtn(action: any, itemSelection: any) {
		let item = {
			etat: action,
			id: []
		};

		itemSelection.map((recruteur) => { });

		itemSelection.map((statut: any) => {
			item.id.push(statut.id);
		});
		if (action === 5 || action === 6 || action === 3 || action === 4) {
			this.notificationService.blockUiStart();

			if (item.id.length > 0) {
				this.interimService.updateEnable(item).subscribe(
					(response: any) => {
						if (response.erreur) {
							this.notificationService.showNotificationEchec(response.erreur);
							this.notificationService.blockUiStop();
						} else {
							this.getNewData();
						}
						this.notificationService.showNotificationSuccess(response.success);
						this.notificationService.blockUiStop();
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'erreur chargement veillez ressayer , consilter le service IT si le probleme persiste'
						);
						this.notificationService.blockUiStop();
					},
					() => { }
				);
			}
		} else if (action === 2) {
			let data = {
				type: 'mailDiffusion',
				selection: itemSelection
			};

			this.dialogRef_form_email = this.dialog.open(FromEmailComponent, {
				width: '800px',
				height: '600px',
				data: data,
				panelClass: 'myapp-no-padding-dialog'
			});
			this.dialogRef_form_email.afterClosed().subscribe((response) => {
				if (response) {
					this.selection.clear();
				}
			});
		} else if (action === 7) {
			let data = {
				type: 'mailDiffusion',
				selection: itemSelection
			};

			this.dialogRef_form_sms = this.dialog.open(TransactionModalComponent, {
				width: '600px',
				height: '350px',
				data: data,
				panelClass: 'myapp-no-padding-dialog'
			});
		} else if (action === 8) {
			let data = {
				type: false,
				data: item.id
			};

			this.notificationService.blockUiStart();
			this.sendInblueeService.sendMailIdentifiants(data).subscribe((response) => {
				this.notificationService.blockUiStop();
				if (!response.erreur) {
					this.notificationService.showNotificationSuccess(response.success);
				} else {
					this.notificationService.showNotificationEchec(response.erreur);
				}
			});
		} else if (action === 9) {
			if (itemSelection.length > 0) {
				const dialog = this.dialog.open(ConstitutionDossierModalComponent, {
					width: '950px',
					height: '470px',
					data: itemSelection,
					panelClass: 'myapp-no-padding-dialog'
				});
			} else {
				this.notificationService.showNotificationEchec('aucun interimaire sélectionné');
			}
		} else if (action === 10) {
			let id: any[] = [];

			itemSelection.map((i: any) => {
				id.push(i.id);
			});
			this.notifierService.blockUiStart();
			this.sendInblueeService.sendEmailDemandeDisponibilite(id).subscribe((response: any) => {
				if (response.erreur) {
					this.notifierService.showNotificationEchec(response.erreur);
				} else {

					this.notifierService.showNotificationSuccess(response.success);
				}


				this.notifierService.blockUiStop();
			});
		}
		else if (action === 11) {
			let id: any[] = [];

			itemSelection.map((i: any) => {
				id.push(i.id);
			});
			this.notifierService.blockUiStart();
			this.sendInblueeService.sendEmailPropositionCollaborartion(id).subscribe((response: any) => {
				if (response.erreur) {
					this.notifierService.showNotificationEchec(response.erreur);
				} else {

					this.notifierService.showNotificationSuccess(response.success);
				}


				this.notifierService.blockUiStop();
			});
		}

		else if (action === 12) {
			let id: any[] = [];

			itemSelection.map((i: any) => {
				id.push(i.id);
			});
			this.notifierService.blockUiStart();
			this.sendInblueeService.sendEmailDemandecoordonneeMobile(id).subscribe((response: any) => {
				if (response.erreur) {
					this.notifierService.showNotificationEchec(response.erreur);
				} else {

					this.notifierService.showNotificationSuccess(response.success);
				}


				this.notifierService.blockUiStop();
			});
		}

		else if (action === 13) {
			let data = {
				interims:[]
			}

			itemSelection.map((i: any) => {
				data.interims.push(i.id);
			});
			
			
			this.notifierService.onConfirm("Êtes-vous sûre d'effectuer cette action ?");
			this.notifierService.dialogRef.afterClosed().subscribe((response) => {
				if (response) {
					this.notifierService.blockUiStart();
					this.http.post<any>(this.interimService.resourceUrl + 'archived', data).pipe(retry(3)).subscribe(
						(rep) => {
							this.notifierService.blockUiStop();
							if (!rep.erreur) {
								this.notificationService.showNotificationSuccess("opération effectuée avec succès ")
								this.getNewData()
							}
							else {
								this.notificationService.showNotificationEchec(rep.erreur)
							}
						}
					)
					
				}
			});
		}

	}


	messageProspection(id: number) {
		const data = { id: id, modal: true, interim: true };

		const dialogRef_mssage: MatDialogRef<
			ListProspectionModalComponent
		> = this.dialog.open(ListProspectionModalComponent, {
			width: '900px',
			height: '400px',
			data: data,
			panelClass: 'myapp-no-padding-dialog'
		});

		dialogRef_mssage.afterClosed().subscribe((el) => {
			el;
		});
	}

	defaultValue(debut = 0, fin = 12) {
		const items = { offset: debut, limit: fin };
		return items;
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	getcivilite(civilite) {
		let c = '';
		if (civilite === 'Madame') {
			c = 'Mme';
		} else if (civilite === 'Monsieur') {
			c = 'M';
		}

		return c;
	}

	datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
		};
	}

	setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
	}

	updateRange(range: Range, login: boolean = false) {
		this.range = range;
		if (!login) {
			this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd') + ' 00:00:00';
			this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd') + ' 23:59:59';
		} else {
			this.data.login_date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd') + ' 00:00:00';
			this.data.login_date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd') + ' 23:59:59';
		}
	}

	updateRangeLastLogin(range: Range) { }

	onGetLimit() {
		for (let i = 1; i <= this.size; i += 100) {
			this.limits.push({ value: i, viewValue: i + '-' + (i + 100) });
		}
	}

	onLiePldToERp(form: NgForm) {
		this.notifierService.blockUiStart();
		this.pldService.idInterimPldToErp(form.value.offset).subscribe(
			(response) => {
				this.notifierService.blockUiStop();
			},
			(error) => {
				this.notifierService.blockUiStop();
			}
		);
	}

	updatepassword(interim) {
		this.notificationService.onConfirm(
			'Réinitialiser le mot de passe du Dr ' + interim.nom + ' ' + interim.prenom + ' ? '
		);
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notifierService.blockUiStart();
				this.userService.updatePasswordByAdmin(interim.user_id).subscribe(
					(rep: any) => {
						if (rep.success) {
							this.notificationService.showNotificationSuccessCopie('bottomRight', rep.success);
						} else {
							this.notificationService.showNotificationEchecCopie('bottomRight', rep.erreur);
						}
						this.notifierService.blockUiStop();
					},
					(error1) => {
						this.notifierService.blockUiStop();
					}
				);
			}
		});
	}

	remarques(message: any) {

		let dialogRef: MatDialogRef<ViewMessageProdpectionModalComponent>;
		dialogRef = this.dialog.open(ViewMessageProdpectionModalComponent, {
			width: '800px',
			height: '550px',
			panelClass: 'myapp-no-padding-dialog',
			data: message

		});
	}
}
