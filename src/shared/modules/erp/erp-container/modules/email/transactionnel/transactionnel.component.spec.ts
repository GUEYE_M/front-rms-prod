import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionnelComponent } from './transactionnel.component';

describe('TransactionnelComponent', () => {
  let component: TransactionnelComponent;
  let fixture: ComponentFixture<TransactionnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
