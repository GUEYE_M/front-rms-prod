import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SendinblueService} from '../../../../../../../services/sendinblue.service';
import {DatePipe} from '@angular/common';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {
  displayedColumns = ['Événements','Date','Objet','De','À','Tags'];
  reportData : any;
  nombre_page :number;

  nombre_total_element :number;
  num_page : number = 1 ;
  dataSource:MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  date_debut:any;
  date_fin:any;
  listsource:any[]=[];
  numeroPageForm: FormGroup;

  taillesPage: any[] = [
    {value: '12', viewValue: '12'},
    {value: '25', viewValue: '25'},
    {value: '50', viewValue: '50'},
    {value: '100', viewValue: '100'}
  ];

  evenementFilter: any[] = [

    {value: 'delivered', viewValue: 'Délivré'},
    {value: 'opened', viewValue: 'Ouvert'},
    {value: 'clicks', viewValue: 'Cliqué'},
    {value: 'hardBounces', viewValue: 'Hard bounce'},
    {value: 'softBounces', viewValue: 'Soft bounce'},
    {value: 'blocked', viewValue: 'Bloqué'},
    {value: 'unsubscribed', viewValue: 'Désinscrit'},
    {value: 'invalid', viewValue: 'Email invalide'},
    {value: 'spam', viewValue: 'Plainte'}
  ];

  expediteursItems: any[] = [

    {value: 'infoges@reseaumedical.fr', viewValue: 'infoges@reseaumedical.fr'},
    {value: 'contact@reseaumedical.fred', viewValue: 'contact@reseaumedical.fr'},
    {value: 'interim@reseaumedical.fr', viewValue: 'interim@reseaumedical.fr'},
    {value: 'oumar.diallo@reseaumedical.fr', viewValue: 'oumar.diallo@reseaumedical.fr'},

  ];
  evenements = new FormControl([],Validators.required);
  // evenementFilter: string[] = ['Envoyé', 'Délivré', '1ère ouverture', 'Ouvert', 'Cliqué', 'Hard bounce','Soft bounce','Bloqué','Plainte','Désinscrit','Email invalide','Différé','Error'];

  expediteurs = new FormControl([],Validators.required);
  //expediteursItems: string[] = ['infoges@reseaumedical.fr', 'contact@reseaumedical.fr', 'interim@reseaumedical.fr', 'oumar.diallo@reseaumedical.fr'];

  destinanaireMessageSerach = new FormControl([],Validators.required);

  btnexpediteur = false;
  btnevenements = false;
  btndestinanaireMessageSerach = false;

  @ViewChild(MatSort, {static: false})  sort:MatSort;
  @ViewChild('listPaginator', {static: false}) paginator:MatPaginator

  constructor(private sendInblueService:SendinblueService,private fb: FormBuilder,private datePipe:DatePipe) { }

  ngOnInit() {

   
    if(this.evenements.valid)
    {
      this.btnevenements = true
    }
    this.sendInblueService.nombre_email_envoyer_par_date.subscribe(
      (response)=>{
    
        this.nombre_page =  Math.round(response/+this.sendInblueService.limit)
      }
    )
    this.date_debut = this.datePipe.transform(new Date(),"yyyy-MM-dd");
    this.date_fin = this.datePipe.transform(new Date(),"yyyy-MM-dd");

   

    //this.reportsLogDay(this.date_debut,this.date_debut,1);





    this.numeroPageForm = this.fb.group(
      {
        numero_page: [Validators.required],

      }
    )

    this.numeroPageForm.patchValue(
      {
        numero_page : 1
      }
    )

    this.sendInblueService.logMaiParDateSubject.subscribe(
      (response)=>{
        this.dataSource = new MatTableDataSource(response);

      }
    )

  }

  reportsLogDay(date_debut:any,date_fin:any,offset,event:string=''){
    this.sendInblueService.reportsLogDay(date_debut,date_fin,offset).subscribe(
      (response:any)=>{
        this.sendInblueService.logMaiParDateSubject.next(response.events);


      }


    )


  }
  newPage()
  {
    if(this.numeroPageForm.valid)
    {
      let ofset = this.numeroPageForm.get('numero_page').value
      if(this.sendInblueService.event && this.evenements.valid || this.expediteurs.valid || this.destinanaireMessageSerach.valid)
      {
        this.sendInblueService.filterEvent(this.sendInblueService.date_debut,this.sendInblueService.date_debut,ofset * +this.sendInblueService.limit - 1,this.sendInblueService.event)
        this.num_page = ofset;

      }
      else
      {
        this.reportsLogDay(this.sendInblueService.date_debut,this.sendInblueService.date_debut,ofset * +this.sendInblueService.limit - 1)
        this.num_page = ofset;
      }



    }
  }
  refrech()
  {
    if(this.numeroPageForm.valid)
    {

      this.reportsLogDay(this.sendInblueService.date_debut,this.sendInblueService.date_debut,1)


    }


  }
  //fonction qui filtre par type d'evenent
  filterEvent(item:any){


    this.sendInblueService.event = item
    this.sendInblueService.filterEvent(this.sendInblueService.date_debut,this.sendInblueService.date_debut,this.sendInblueService.limit,this.sendInblueService.event).subscribe(
      (response:any)=>{
        this.sendInblueService.logMaiParDateSubject.next(response.events);
      }
    )

  }

  filterEmail(item:any){

    (item)
    this.sendInblueService.email = item
    this.sendInblueService.filterEmail(this.sendInblueService.date_debut,this.sendInblueService.date_debut,this.sendInblueService.limit,this.sendInblueService.email).subscribe(
      (response:any)=>{
        this.sendInblueService.logMaiParDateSubject.next(response.events);
      }
    )

  }
  taillePageFunc(taille:any)
  {
    this.sendInblueService.limit = taille;
    this.reportsLogDay(this.sendInblueService.date_debut,this.sendInblueService.date_debut,1)



  }
  evenementsearch(item)
  {
    if(this.evenements.valid){
      this.btnevenements = true;

      this.btndestinanaireMessageSerach = false
      this.btnexpediteur = false;

      this.expediteurs.reset();
      this.destinanaireMessageSerach.reset()
    }
    else{
      this.btnevenements = false;
    }

  }

  expediteursearch(item)
  {
    if(this.expediteurs.valid){

      this.btnexpediteur = true;
      this.btnevenements = false
      this.btndestinanaireMessageSerach = false

      this.destinanaireMessageSerach.reset()
      this.evenements.reset();

    }
    else{
      this.btnexpediteur = false;
    }

  }

  destinateurMessagesearch()
  {
    if(this.destinanaireMessageSerach.valid){

      this.btndestinanaireMessageSerach = true;
      this.btnevenements = false;
      this.btnexpediteur = false;

      this.evenements.reset();
      this.expediteurs.reset();


    }
    else{
      this.btndestinanaireMessageSerach = false;

    }

  }

}
