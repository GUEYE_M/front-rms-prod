import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { SendinblueService } from '../../../../../../services/sendinblue.service';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { NotificationService } from '../../../../../../services/notification.service';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup } from '@angular/forms';

import * as moment from 'moment';
import * as localization from 'moment/locale/fr';
import { LOCALE } from 'src/shared/utils/date_filter_config';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
moment.locale('fr', localization);
@Component({
  selector: 'app-transactionnel',
  templateUrl: './transactionnel.component.html',
  styleUrls: ['./transactionnel.component.css']
})
export class TransactionnelComponent implements OnInit {



  ligne_list: any[];
  email_campagns: any;
  email_contacts: any;
  showLoadClient: boolean;
  actionInput: Boolean = false;
  action: string;

  closeResult: string;

  public form: FormGroup;

  displayedColumns = ['ID', 'NOM', 'DESTINATAIRES', 'OUVREURS', 'CLIQUEURS', 'DÉSINSCRITS', 'DATE ENVOI'];

  reportData: any;
  reportsMailDay: any;
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource: any[] = [];
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('listPaginator', { static: false }) paginator: MatPaginator
  color = 'primary';
  color1 = 'warn';
  color2 = 'accent';
  mode = 'determinate';
  value = 50;
  bufferValue = 75;
  panelOpenState = false;
  campagnes: Observable<any[]>;

  locale = LOCALE


  start: string;
  // ADD CHART OPTIONS.
  chartOptions = {
    responsive: true    // THIS WILL MAKE THE CHART RESPONSIVE (VISIBLE IN ANY DEVICE).
  }

  labels = ['Delivré', 'Ouver', 'Cliquer', 'Bounces', 'Spam', 'Hard Bounces', 'Soft Bounces', 'Bloqué'];

  // STATIC DATA FOR THE CHART IN JSON FORMAT.
  chartData = [

    {
      label: 'statistique en %',
      data: []
    },


  ];

  // CHART COLOR.
  colors = [

    { // 2nd Year.
      backgroundColor: 'rgb(23,162,184)'

    }
  ]
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];
  @ViewChild('dateRangePicker', { static: false }) dateRangePicker;
  range: Range = { fromDate: new Date(), toDate: new Date() };
  constructor(private sendinblueService: SendinblueService,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private datePipe: DatePipe,
    private modalService: NgbModal) {

  }

  ngOnInit() {

    this.datepickerConfig()

    this.notificationService.blockUiStart();


    this.sendinblueService.allemailContacs().subscribe(
      (response) => {
        this.notificationService.blockUiStart();

        this.email_contacts = response;



      },
      () => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('une erreur est survenue lors du chargement des donées, veillez ressayer');
      },
      () => {
        this.notificationService.blockUiStop();
      }

    )
    this.sendinblueService.reportsMailDay().subscribe(
      (response: any) => {


        if (response) {
          this.reportsMailDay = response.reports;
          this.sendinblueService.nombre_email_envoyer_par_date.next(this.reportsMailDay[0].requests)
          this.initChartData();
          this.graphData(this.reportsMailDay)


        }



      },
      () => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('une erreur est survenue lors du chargement des donées, veillez ressayer');
      },
      () => {
        this.notificationService.blockUiStop();
      }


    )


    // this.sendinblueService.list().subscribe(
    //   (response)=>{

    //     if(response.length > 0)
    //     {
    //      (response)
    //       this.dataSource = new MatTableDataSource(response);

    //     }


    //   }
    // )




  }
  initChartData() {
    this.chartData = [

      {
        label: 'statistique en %',
        data: []
      },


    ];
  }
  updateRange(range: Range) {
    if (range.fromDate && range.toDate) {
      this.range.fromDate = range.fromDate
      this.range.toDate = range.toDate
      this.notificationService.blockUiStart();
      this.sendinblueService.reportsMailDay(this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd'), this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd')).subscribe(
        (response: any) => {
          if (response) {
            this.reportsMailDay = response.reports;
            this.sendinblueService.nombre_email_envoyer_par_date.next(this.reportsMailDay[0].requests)
            this.initChartData();
            this.graphData(response.reports)

          }

        },

        () => {
          this.notificationService.blockUiStop();
          this.notificationService.showNotificationEchec('une erreur est survenue lors du chargement des donées, veillez ressayer');
        },
        () => {
          this.notificationService.blockUiStop();
        }





      )

      this.sendinblueService.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd')
      this.sendinblueService.date_debut = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd')

      this.sendinblueService.reportsLogDay(this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd'), this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd')).subscribe(
        (response: any) => {
          this.sendinblueService.logMaiParDateSubject.next(response.events);

        }


      )


    }




  }

  // updateRange(event) {

  //   (event)


  //   this.notificationService.blockUiStart();
  //   this.sendinblueService.reportsMailDay(this.datePipe.transform(event.startDate, 'yyyy-MM-dd'), this.datePipe.transform(event.endDate, 'yyyy-MM-dd')).subscribe(
  //     (response: any) => {
  //       if (response) {
  //         this.reportsMailDay = response.reports;
  //         this.sendinblueService.nombre_email_envoyer_par_date.next(this.reportsMailDay[0].requests)
  //         this.initChartData();
  //         this.graphData(response.reports)


  //       }





  //     },

  //     () => {
  //       this.notificationService.blockUiStop();
  //       this.notificationService.showNotificationEchec('une erreur est survenue lors du chargement des donées, veillez ressayer');
  //     },
  //     () => {
  //       this.notificationService.blockUiStop();
  //     }





  //   )
  //   if (event.startDate != null && event.endDate != null) {
  //     this.sendinblueService.date_debut = this.datePipe.transform(event.startDate, 'yyyy-MM-dd')
  //     this.sendinblueService.date_fin = this.datePipe.transform(event.endDate, 'yyyy-MM-dd')

  //   }

  //   else {
  //     this.sendinblueService.date_debut = this.datePipe.transform(new Date(), "yyyy-MM-dd");
  //     this.sendinblueService.date_debut = this.datePipe.transform(new Date(), "yyyy-MM-dd");
  //   }

  //   this.sendinblueService.reportsLogDay(this.sendinblueService.date_debut, this.sendinblueService.date_fin).subscribe(
  //     (response: any) => {
  //       this.sendinblueService.logMaiParDateSubject.next(response.events);

  //     }
  //   )




  // }



  editLigneModal(id: number = null): void {


  }
  graphData(data: any) {


    this.chartData.map(
      (item) => {
        data.map(
          (ele) => {

            item.data.push(Math.round(ele.delivered * 100 / ele.requests).toFixed(2))
            item.data.push(Math.round(ele.uniqueOpens * 100 / ele.requests)).toFixed(2)
            item.data.push(Math.round(ele.clicks * 100 / ele.requests)).toFixed(2)
            item.data.push(Math.round(ele.hardBounces + ele.softBounces * 100 / ele.requests)).toFixed(2)
            item.data.push(Math.round(ele.spamReports * 100 / ele.requests)).toFixed(2)
            item.data.push(Math.round(ele.hardBounces * 100 / ele.requests)).toFixed(2)
            item.data.push(Math.round(ele.softBounces * 100 / ele.requests)).toFixed(2)
            item.data.push(Math.round(ele.blocked * 100 / ele.requests)).toFixed(2)
          }
        )

      }
    )
    // (this.chartData)
  }

  // CHART CLICK EVENT.
  onChartClick(event) {
    (event);
  }


  chartHovered(event) {
    (event);
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  datepickerConfig() {
    const today = new Date();
    const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
    const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

    const resetRange = { fromDate: today, toDate: today };
    if (this.dateRangePicker)
      this.dateRangePicker.resetDates(resetRange);

    this.setupPresets();
    this.options = {
      presets: this.presets,
      format: 'dd-MM-yyyy',
      range: { fromDate: null, toDate: null },
      applyLabel: "OK",
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: false,
        hasBackdrop: false
      },
      locale: 'de-DE',
      cancelLabel: "Annuler",
      startDatePrefix: "Debut",
      endDatePrefix: "Fin",
      placeholder: "Rechercher",
      animation: true
      // excludeWeekends:true,
      // fromMinMax: {fromDate:fromMin, toDate:fromMax},
      // toMinMax: {fromDate:toMin, toDate:toMax}
    };
  }


  setupPresets() {

    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    }

    const today = new Date()
    const yesterday = backDate(1);
    const minus7 = backDate(7)
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    this.presets = [
      { presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
      { presetLabel: "Les 7 derniers jours", range: { fromDate: minus7, toDate: today } },
      { presetLabel: "Les 30 derniers jours", range: { fromDate: minus30, toDate: today } },
      { presetLabel: "Ce mois", range: { fromDate: currMonthStart, toDate: currMonthEnd } },
      { presetLabel: "Le mois dernier", range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
    ]
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
