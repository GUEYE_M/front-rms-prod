import { Component, OnInit } from '@angular/core';
import {SendinblueService} from '../../../../../../services/sendinblue.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {UtilsService} from '../../../../../../services/utils.service';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})
export class WrapperComponent implements OnInit {

  reportData: any;
  constructor(
    private sendinblueService: SendinblueService,
    private notificationService: NotificationService,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {
    // f
    this.utilsService.onRevientEnHaut();
    this.notificationService.blockUiStart();
    this.sendinblueService.reportActivity().subscribe(
      (response) => {
        this.reportData = response;
      },
      () => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('une erreur est survenue lors du chargement des donées, veillez ressayer');
      },
      () =>
      {
        this.notificationService.blockUiStop();
      }

    );
  }


}
