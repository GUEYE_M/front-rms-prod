import {Route, RouterModule} from '@angular/router';
import {WrapperComponent} from './wrapper/wrapper.component';
import {TransactionnelComponent} from './transactionnel/transactionnel.component';
import {CampagneComponent} from './campagne/campagne.component';
import {CreateCampagneComponent} from './campagne/create-campagne/create-campagne.component';

const EMAILS_CONFIG: Route[] = [
  {
    path: 'boite-email',
    component: WrapperComponent,
  },
  {path: 'transactionnels', component: TransactionnelComponent},
  {path: 'contacts', component: TransactionnelComponent},
  {path: 'campagnes', component: CampagneComponent, children: [
      {path: 'create-campagne', component: CreateCampagneComponent}
    ]
  },
];

export const EmailsRouting = RouterModule.forChild(EMAILS_CONFIG);



