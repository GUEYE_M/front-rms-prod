import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { WrapperComponent } from './wrapper/wrapper.component';
import { CampagneComponent } from './campagne/campagne.component';
import { ContactsComponent } from './contacts/contacts.component';
import { TransactionnelComponent } from './transactionnel/transactionnel.component';
import {SharedModule} from '../../../../shared/shared.module';
import {EmailsRouting} from './email.routing';
import { CreateCampagneComponent } from './campagne/create-campagne/create-campagne.component';
import { LogsComponent } from './transactionnel/logs/logs.component';



@NgModule({
  declarations: [WrapperComponent, CampagneComponent, ContactsComponent, TransactionnelComponent, CreateCampagneComponent, LogsComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmailsRouting,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class EmailModule { }
