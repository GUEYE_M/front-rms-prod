import {Component, OnInit, ViewChild} from '@angular/core';
import {CreateCampagneComponent} from './create-campagne/create-campagne.component';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SendinblueService} from '../../../../../../services/sendinblue.service';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-campagne',
  templateUrl: './campagne.component.html',
  styleUrls: ['./campagne.component.css']
})
export class CampagneComponent implements OnInit {

  ligne_list: any[];
  email_campagns : any;
  dialogRef: MatDialogRef<CreateCampagneComponent>;

  showLoadClient: boolean;
  actionInput:Boolean= false;
  action:string;
  filterChoixCheckboxStorage: any;
  filterValuesStorage: any;
  filterchoixCheckBox = [];



  displayedColumns = ['ID','NOM','DESTINATAIRES','OUVER','CLIQUER','DÉSINSCRITS','DATE ENVOI'];

  reportData : any;
  dataSource:MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Archiver'];
  listsource:any[]=[];
  @ViewChild(MatSort, {static: false})  sort:MatSort;
  @ViewChild('listPaginator', {static: false}) paginator:MatPaginator

  constructor( private sendinblueService:SendinblueService,
               private dialog: MatDialog,) {

  }


  ngOnInit() {

    this.sendinblueService.allemailCampaigns().subscribe(
      (response:any)=>{

        this.dataSource = new MatTableDataSource(response.campaigns);

      }



    )
  }

  createCampagne(): void {


    this.dialogRef = this.dialog.open(CreateCampagneComponent, {
      width: '800px',
      height: '800px',


    })
  }


}
