import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratValiderComponent } from './contrat-valider.component';

describe('ContratValiderComponent', () => {
  let component: ContratValiderComponent;
  let fixture: ComponentFixture<ContratValiderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratValiderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratValiderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
