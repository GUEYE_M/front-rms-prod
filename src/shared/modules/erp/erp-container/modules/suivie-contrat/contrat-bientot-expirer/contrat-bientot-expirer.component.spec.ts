import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratBientotExpirerComponent } from './contrat-bientot-expirer.component';

describe('ContratBientotExpirerComponent', () => {
  let component: ContratBientotExpirerComponent;
  let fixture: ComponentFixture<ContratBientotExpirerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratBientotExpirerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratBientotExpirerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
