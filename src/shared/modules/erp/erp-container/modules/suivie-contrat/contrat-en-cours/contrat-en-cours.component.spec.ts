import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratEnCoursComponent } from './contrat-en-cours.component';

describe('ContratEnCoursComponent', () => {
  let component: ContratEnCoursComponent;
  let fixture: ComponentFixture<ContratEnCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratEnCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratEnCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
