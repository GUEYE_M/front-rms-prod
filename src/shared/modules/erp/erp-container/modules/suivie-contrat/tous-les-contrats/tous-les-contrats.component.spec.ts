import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TousLesContratsComponent } from './tous-les-contrats.component';

describe('TousLesContratsComponent', () => {
  let component: TousLesContratsComponent;
  let fixture: ComponentFixture<TousLesContratsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TousLesContratsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TousLesContratsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
