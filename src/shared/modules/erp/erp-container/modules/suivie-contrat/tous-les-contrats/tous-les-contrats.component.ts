import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, PageEvent, MatDialogRef, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { SellandsignService } from 'src/shared/services/sellandsign.service';
import { NotificationService } from 'src/shared/services/notification.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ApiVilleService } from 'src/shared/services/api_ville_service';
import { Commune } from 'src/shared/models/commune.model';
import { startWith, map, tap } from 'rxjs/operators';

import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { contractSelector } from 'src/shared/models/erp/ContratctSelector.model';
moment.locale('fr', localization);
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { from } from 'rxjs';
import { StatutContratSignComponent } from '../../interim/single-interim/mission-medecin/statut-contrat-sign/statut-contrat-sign.component';

declare var $: any;

@Component({
	selector: 'app-tous-les-contrats',
	templateUrl: './tous-les-contrats.component.html',
	styleUrls: [ './tous-les-contrats.component.css' ]
})
export class TousLesContratsComponent implements OnInit {
	dataSource: MatTableDataSource<any[]>;
	locale = LOCALE;
	listActions = [ 'Relancer', 'Annuler', 'Valider' ];
	action: string;
	columFilter: number;
	communes: any[];
	displayedColumns = [ 'select', 'number', 'id', 'date', 'etat', 'client', 'ville', 'mission', 'modele', 'action' ];
	listContrats: any[] = [];
	selection = new SelectionModel<any[]>(true, []);
	range: Range = { fromDate: new Date(), toDate: new Date() };

	date_debut: number = null;
	date_fin: number = null;

	viewdatepiker: boolean = false;
	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];
	dialogRefContratSign: MatDialogRef<StatutContratSignComponent>;

	data = {
		column: '0',
		statut: 0,
		date_debut: 0,
		date_fin: 0,
		offset: 0,
		limit: 12,
		client: '',
		company: ''
	};

	public form: FormGroup;

	statut_contrat = [
		{
			label: 'Contrats en cours',
			value: 1
		},
		{
			label: 'Contrats à valider',
			value: 2
		},

		{
			label: 'Contrats validés',
			value: 3
		},
		{
			label: 'Contrats archivés',
			value: 4
		},

		{
			label: 'Contrats abandonnés',
			value: 5
		}
	];
	filteredCommune: Observable<any[]>;
	displayedColumnsFilter = [
		{
			name: 'contractSelector',
			label: 'Statut'
		},
		{
			name: 'date',
			label: 'Date'
		},

		{
			name: 'company',
			label: 'Raison Social'
		},

		{
			name: 'customer',
			label: 'Client'
		}
	];

	size: number = 0;
	limit: number = 10;
	length: number = 0;
	pageSize = 12;

	elements = [];

	pageSizeOptions: number[] = [ 5, 10, 25, 100 ];

	// MatPaginator Output
	pageEvent: PageEvent;

	constructor(
		private sellandsignService: SellandsignService,
		private apiVilleService: ApiVilleService,
		private formBuilder: FormBuilder,
		private notificationService: NotificationService,
		private router: Router,
		private dialog: MatDialog
	) {}

	ngOnInit() {
		this.initData();
	}

	initData() {
		this.sellandsignService.getAllContratBd(this.data).subscribe((response) => {
			if (!response.erreur) {
				this.length = response.data.totalSize;
				this.sellandsignService.total_contrat.next(+response.data.totalSize);
				this.dataSource = new MatTableDataSource(this.sellandsignService.custumlist(response.data.elements));
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	initDataFilter() {
		this.data.column = '0';
		this.data.statut = 0;
		this.data.date_debut = 0;
		this.data.date_fin = 0;
		this.data.offset = 0;
		this.data.limit = 12;
		this.data.client = '';
		this.data.company = '';
	}

	valutElements() {
		//  (this.elements.length)
		if (this.elements.length === 0) {
			this.date_debut = null;
			this.date_fin = null;
			this.initDataFilter();
			this.initData();
		} else {
			this.elements.map((item) => {
				if (item == 'date') {
					this.datepickerConfig();
				} else {
					this.date_debut = null;
					this.date_fin = null;
				}
			});
		}
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;

		const numRows = this.dataSource.data.length;

		return numSelected === numRows;
	}
	pagination(event: any) {
		this.pageSize = event.pageSize;
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = this.pageSize;

		this.initData();
	}

	onSubmitFiltre(form: NgForm) {
		this.data.statut = 0;
		this.data.offset = this.size;
		this.data.limit = this.limit;
		this.initDataFilter();
		if (this.date_debut != null && this.date_fin != null) {
			this.data.column = '1';
			this.data.date_debut = this.date_debut;
			this.data.date_fin = this.date_fin;
		} else if (form.value['company']) {
			this.data.column = '3';
			this.data.company = form.value['company'];
		} else if (form.value['contractSelector']) {
			this.data.column = '0';
			this.data.statut = +form.value['contractSelector'];
		} else if (form.value['customer']) {
			this.data.column = '5';
			this.data.client = form.value['customer'];
		}

		this.initData();
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	actionbtn(action: string, items: any[]) {
		this.notificationService.blockUiStart();
		items.map((item) => {
			if (action === 'Valider') {
				if (item.status === 'SIGNED') {
					this.sellandsignService.validationContrat(item.transactionId).subscribe(
						(response) => {},
						(erreur) => {
							this.notificationService.showNotificationEchec(
								'erreur validation du contrat ' + item.id + ' veillez ressayer'
							);
							this.notificationService.blockUiStop();
						},
						() => {}
					);
				} else {
					this.notificationService.showNotificationEchec(
						'impossible de valider le  contrat N° ' + item.id + ' veillez verifier sont status'
					);
					this.notificationService.blockUiStop();
				}
			} else if (action === 'Relancer') {
				let data: any = {
					all: true,
					contract_id: '' + item.id,
					prevalidation_mode: false
				};
				this.sellandsignService.relancerContrat(data).subscribe(
					(response) => {
						if (response) {
							this.notificationService.showNotificationSuccess(
								'le  contrat ' + item.id + ' a bien été relancer'
							);
							this.notificationService.blockUiStop();
						}
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'erreur annulation contrat ' + item.id + ' veillez ressayer'
						);
						this.notificationService.blockUiStop();
					},
					() => {}
				);
			} else if (action === 'Annuler') {
				if (item.status === 'SIGNED' || item.status === 'OPEN') {
					this.sellandsignService.annulerContrat(item.id).subscribe(
						(response) => {},
						(erreur) => {
							this.notificationService.showNotificationEchec(
								'erreur annulation contrat ' + item.id + ' veillez ressayer'
							);
							this.notificationService.blockUiStop();
						},
						() => {
							this.initData();
							this.notificationService.showNotificationSuccess(
								'le  contrat ' + item.id + ' a bien été annuler'
							);
							this.notificationService.blockUiStop();
						}
					);
				} else {
					this.notificationService.showNotificationEchec(
						"impossible d'annuler le  contrat N° " + item.id + ' veillez verifier sont status'
					);
					this.notificationService.blockUiStop();
				}
			}
		});

		//this.router.navigate([ '/RMS-Admin/redirectionMed/', 'suivi-contrat', 0 ]);
	}

	datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
			// excludeWeekends:true,
			// fromMinMax: {fromDate:fromMin, toDate:fromMax},
			// toMinMax: {fromDate:toMin, toDate:toMax}
		};
	}

	setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
	}

	updateRange(range: Range) {
		//this.notificationService.blockUiStart();

		this.range = range;

		this.date_debut = new Date(this.range.fromDate).getTime();
		this.date_fin = new Date(this.range.toDate).getTime();
	}

	getStatutContrat(id: number) {
		this.notificationService.blockUiStart();
		this.sellandsignService.getStatutContrat(id).subscribe((response: any) => {
			this.notificationService.blockUiStop();
			if (!response.erreur) {
				this.statutContratSignModal(response.data);
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	statutContratSignModal(data: any): void {
		this.dialogRefContratSign = this.dialog.open(StatutContratSignComponent, {
			width: '65%',
			height: '65%',
			panelClass: 'myapp-no-padding-dialog',
			data: data
		});
		this.dialogRefContratSign.afterClosed().subscribe((response) => {
			if (response) {
				if (response === 'Annuler'.toLowerCase().trim()) {
				}
			}
		});
	}
}
