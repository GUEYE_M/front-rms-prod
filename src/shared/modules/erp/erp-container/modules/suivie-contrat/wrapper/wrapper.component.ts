import { Component, OnInit, ViewChild } from '@angular/core';
import { SellandsignService } from '../../../../../../services/sellandsign.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { BehaviorSubject } from 'rxjs';
import { ContratSellAndSignModel } from '../../../../../../models/erp/contratSellAndSign..model';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
	selector: 'app-wrapper',
	templateUrl: './wrapper.component.html',
	styleUrls: [ './wrapper.component.css' ]
})
export class WrapperComponent implements OnInit {
	actionInput: Boolean = false;
	selection = new SelectionModel<any[]>(true, []);
	action: string;
	listeAllContrats: ContratSellAndSignModel[];
	view_table_all_contrat: boolean = false;

	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;

	listContratSubjet: BehaviorSubject<ContratSellAndSignModel[]> = new BehaviorSubject([]);

	listActions = [ 'Valider', 'Relancer', 'Annuler' ];

	listsource: any[] = [];
	listcontrat: any[];
	showLoadClient: boolean = false;
	j: number = 0;

	total_contrat: any;
	contrat_encour: any;
	contrat_a_valider: any;
	contrat_valider: any;
	contrat_archiver: any;
	contrat_abandonnes: any;

	constructor(private sellandsignService: SellandsignService, private notificationService: NotificationService) {}

	ngOnInit() {
		this.sellandsignService.total_contrat.subscribe((el) => {
			this.total_contrat = el;
		});

		this.sellandsignService.contrat_valider.subscribe((el) => {
			this.contrat_valider = el;
		});

		this.sellandsignService.contrat_a_valider.subscribe((el) => {
			this.contrat_a_valider = el;
		});

		this.sellandsignService.contrat_encours.subscribe((el) => {
			this.contrat_encour = el;
		});

		this.sellandsignService.contrat_abandonnee.subscribe((el) => {
			this.contrat_abandonnes = el;
		});

		this.sellandsignService.contrat_archiver.subscribe((el) => {
			this.contrat_archiver = el;
		});
	}

	ngAfterViewInit() {}
	statutBtn(statut: number) {
		this.selection;
	}
}
