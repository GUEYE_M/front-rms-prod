import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratArchiverComponent } from './contrat-archiver.component';

describe('ContratArchiverComponent', () => {
  let component: ContratArchiverComponent;
  let fixture: ComponentFixture<ContratArchiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratArchiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratArchiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
