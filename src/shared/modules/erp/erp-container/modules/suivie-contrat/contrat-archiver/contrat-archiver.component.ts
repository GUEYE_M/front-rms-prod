import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { SellandsignService } from 'src/shared/services/sellandsign.service';
import { MatTableDataSource, MatDialogRef, MatDialog } from '@angular/material';
import { NotificationService } from 'src/shared/services/notification.service';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { StatutContratSignComponent } from '../../interim/single-interim/mission-medecin/statut-contrat-sign/statut-contrat-sign.component';

@Component({
	selector: 'app-contrat-archiver',
	templateUrl: './contrat-archiver.component.html',
	styleUrls: [ './contrat-archiver.component.css' ]
})
export class ContratArchiverComponent implements OnInit {
	dataSourceContratArchiver: MatTableDataSource<any[]>;
	displayedColumns = [ 'select', 'number', 'id', 'date', 'etat', 'client', 'ville', 'mission', 'modele', 'action' ];
	selection = new SelectionModel<any[]>(true, []);
	length: number = 0;
	pageSize = 10;
	pageSizeOptions: number[] = [ 5, 10, 25, 100 ];
	data = {
		column: '0',
		statut: 4,
		date_debut: 0,
		date_fin: 0,
		offset: 0,
		limit: 12,
		client: '',
		company: ''
	};
	dialogRefContratSign: MatDialogRef<StatutContratSignComponent>;

	constructor(
		private sellandsignService: SellandsignService,
		private notificationService: NotificationService,
		private dialog: MatDialog
	) {}

	ngOnInit() {
		this.initData();
	}

	initData() {
		this.sellandsignService.getAllContratBd(this.data).subscribe((response) => {
			if (!response.erreur) {
				this.length = response.data.totalSize;
				this.sellandsignService.contrat_archiver.next(+response.data.totalSize);

				this.dataSourceContratArchiver = new MatTableDataSource(
					this.sellandsignService.custumlist(response.data.elements)
				);
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;

		const numRows = this.dataSourceContratArchiver.data.length;

		return numSelected === numRows;
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSourceContratArchiver.data.forEach((row) => this.selection.select(row));
	}

	pagination(event: any) {
		this.pageSize = event.pageSize;
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = this.pageSize;

		this.initData();
	}

	getStatutContrat(id: number) {
		this.notificationService.blockUiStart();
		this.sellandsignService.getStatutContrat(id).subscribe((response: any) => {
			this.notificationService.blockUiStop();
			if (!response.erreur) {
				this.statutContratSignModal(response.data);
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	statutContratSignModal(data: any): void {
		this.dialogRefContratSign = this.dialog.open(StatutContratSignComponent, {
			width: '65%',
			height: '65%',
			panelClass: 'myapp-no-padding-dialog',
			data: data
		});
		this.dialogRefContratSign.afterClosed().subscribe((response) => {
			if (response) {
				if (response === 'Annuler'.toLowerCase().trim()) {
				}
			}
		});
	}
}
