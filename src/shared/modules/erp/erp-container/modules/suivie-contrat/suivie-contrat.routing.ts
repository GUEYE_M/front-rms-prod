import {Route, RouterModule} from '@angular/router';
import {WrapperComponent} from './wrapper/wrapper.component';
import { ProfilContratComponent } from './profil-contrat/profil-contrat.component';
import { ContraPldComponent } from './contra-pld/contra-pld.component';

const SUIVIE_CONTRAT_CONFIG: Route[] = [
  {
    path: '',
    component: WrapperComponent,
    
  },
  {
    path: ':id/profil-contrat',
    component: ProfilContratComponent,
  
  },
  {
    path: 'contrat-pld',
    component: ContraPldComponent,
  
  }


  
];

export const SuivieContratRouting = RouterModule.forChild(SUIVIE_CONTRAT_CONFIG);



