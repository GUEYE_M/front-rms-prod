import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratAbandonnerComponent } from './contrat-abandonner.component';

describe('ContratAbandonnerComponent', () => {
  let component: ContratAbandonnerComponent;
  let fixture: ComponentFixture<ContratAbandonnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratAbandonnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratAbandonnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
