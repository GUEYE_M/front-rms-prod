import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellandsignService } from 'src/shared/services/sellandsign.service';
import { MatTableDataSource } from '@angular/material';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
  selector: 'app-profil-contrat',
  templateUrl: './profil-contrat.component.html',
  styleUrls: ['./profil-contrat.component.css']
})
export class ProfilContratComponent implements OnInit {
  pdfSrc: string = '';
  dataSource: MatTableDataSource<any[]>;
  displayedColumns = ['number', 'Nom Complet', 'Email', 'Date', 'Télephone', 'Fonction', 'Mode Signature', 'Signature', 'Relancer'];

  action:any

  statut_contrat: string = '';
  signataire = {
    nomComplet: '',
    email: '',
    telephone: '',
    fonction: '',
    signatureStatus: '',
    signatureDate: null,
    signatureMode: null,

  }
  contratOption: string[] = [];
  signairesArray: any[] = [];
  client: any
  transaction_contrat: { id: any; transactionId: any; representant: any; status: any; subStatus: any; modele: any; date: any; closedDate: any; numero_client: any; };

  //signatureStatus: "SIGNED"

  constructor(private activateRoute: ActivatedRoute, private sellAndSignService: SellandsignService, private notificationService: NotificationService) {


    this.sellAndSignService.getDocumentPdfSellAndSign(this.activateRoute.snapshot.params.id).subscribe(
      (response: any) => {
        if (response) {

          this.pdfSrc = this.viewdocumentPdf(response)
        }
      }
    )

      ;
    // this.sellAndSignService.statutContrat(this.activateRoute.snapshot.params.id).subscribe(
    //   (response: any) => {
    //     (response)

    //     response['elements'].map(
    //       (item) => {
    //         this.signataire.signatureDate = item.signatureDate
    //         this.signataire.signatureMode = item.signatureMode
    //         this.signataire.signatureStatus = item.signatureStatus

    //         this.sellAndSignService.getSignataireByID(item.contractorId).subscribe(
    //           (el: any) => {


    //             this.signataire.nomComplet = el.firstname + ' ' + el.lastname
    //             this.signataire.email = el.email
    //             this.signataire.telephone = el.cell_phone
    //             this.signataire.fonction = el.jobTitle





    //           }
    //         )

    //         this.signairesArray.push(this.signataire)
    //       }
    //     )

    //   }
    // )


    // this.sellAndSignService.infosTransactionContrat(this.activateRoute.snapshot.params.id).subscribe(
    //   (response: any) => {
    //     //(response)

    //     this.transaction_contrat = {
    //       id: response.id,
    //       transactionId: response.transactionId,
    //       representant: response.vendorEmail,
    //       status: response.status,
    //       subStatus: response.subStatus,
    //       modele: response.filename,
    //       date: response.date,
    //       closedDate: response.closedDate,
    //       numero_client: response.customerNumber

    //     }

    //   }, (errur) => {
    //     // this.notificationService.showNotificationEchec("erreur chargement, veillez patienter un moment")
    //     // this.notificationService.blockUiStop();
    //   },
    //   () => {

    //     this.sellAndSignService.getClient(this.transaction_contrat.numero_client).subscribe(
    //       (response) => {

    //         this.client = response
    //       }
    //     )

    //     if (this.transaction_contrat.status === "ABANDONED") {
    //       this.statut_contrat = 'CONTRAT ABANDONNÉ'

    //     }
    //     else if (this.transaction_contrat.status === "SIGNED") {
    //       this.statut_contrat = "CONTRAT SIGNÉ"
    //       this.contratOption.push('Valider', 'Annuler', 'Telecharger le  document envoyé', 'Telecharger toutes les pieces')
    //     }
    //     else if (this.transaction_contrat.status === "CLOSED") {
    //       this.statut_contrat = "CONTRAT VALIDÉ"
    //       this.contratOption.push('Telecharger le  document signé', 'Telecharger le  dossier de preuve', 'Telecharger le certificat de preuve', 'Telecharger toutes les pieces')
    //     }
    //     else if (this.transaction_contrat.status === "OPEN") {
    //       this.statut_contrat = "CONTRAT  PRESENTER POUR SIGNATURE"
    //       this.contratOption.push('Relancer', 'Annuler', 'Telecharger le  document envoyé', 'Telecharger toutes les pieces')
    //     }

    //     else if (this.transaction_contrat.status === "ARCHIVED") {
    //       this.statut_contrat = "CONTRAT ARCHIVER"
    //       this.contratOption.push('Telecharger le  document signé', 'Telecharger le  dossier de preuve', 'Telecharger le certificat de preuve', 'Telecharger toutes les pieces')

    //     }

    //   }
    // )




  }

  ngOnInit() {

    this.dataSource = new MatTableDataSource(this.signairesArray)




  }
  viewdocumentPdf(file: any) {
    var newBlob = new Blob([file], { type: "application/pdf" });

    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(newBlob);
      return;
    }

    // For other browsers: 
    // Create a link pointing to the ObjectURL containing the blob.
    return window.URL.createObjectURL(newBlob);
  }

  relancerContrat() {
    this.notificationService.blockUiStart()
    let data: any = {
      all: true,
      contract_id: '' + this.transaction_contrat.id,
      prevalidation_mode: false

    }
    this.sellAndSignService.relancerContrat(data).subscribe(
      (response) => {
        if (response) {
          this.notificationService.showNotificationSuccess('le  contrat ' + this.transaction_contrat.id + ' a bien été relancer')
          this.notificationService.blockUiStop()
        }
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur annulation contrat ' + this.transaction_contrat.id + ' veillez ressayer')
        this.notificationService.blockUiStop()
      },
      () => {

      }
    )
  }

  actionbtn(action:string=null,contrat_id:number,transactionId:any)
  {
    if(action)
    {
      if(action.toLowerCase().trim() == "Valider".toLowerCase().trim())
      {
        this.notificationService.blockUiStart();
        this.sellAndSignService.validationContrat(transactionId).subscribe(
          (response)=>{
            (response);
          },
          (erreur)=>{
            this.notificationService.showNotificationEchec('une erreure est survenue lors de la validation du contrat, veillez ressayer')
          },
          ()=>{
                 this.notificationService.showNotificationSuccess("le contra a été bien valider")
                 this.notificationService.blockUiStop();
          }
        )
      }
      else if(action.toLowerCase().trim() == "Annuler".toLowerCase().trim())
      {
        this.notificationService.blockUiStart();
          this.sellAndSignService.annulerContrat(contrat_id).subscribe(
            (response)=>{
              (response);
            },
            (erreur)=>{
              this.notificationService.showNotificationEchec('echec annulation du contrat, veillez ressayer')
            },
            ()=>{
                   this.notificationService.showNotificationSuccess("le contra a été bien annuler")
                   this.notificationService.blockUiStop();
            }
          )
      }
     
      else if(action.toLowerCase().trim() == "Telecharger le  document signé".toLowerCase().trim())
      {
        this.notificationService.blockUiStart();
        
          this.sellAndSignService.getDocumentSigné(contrat_id).subscribe(
            (response)=>{
            
              this.dowloadFile(response,"document signer".toLowerCase().trim()+".pdf");
          
            },
            (erreur)=>{
              this.notificationService.showNotificationEchec('echec telechargement du contrat, veillez ressayer')
            },
            ()=>{
                  // this.notificationService.showNotificationSuccess("le contra a été bien annuler")
                   this.notificationService.blockUiStop();
            }
          )
      }
      else if(action.toLowerCase().trim() == "Telecharger le  dossier de preuve".toLowerCase().trim())
      {
        this.notificationService.blockUiStart();
        
        this.sellAndSignService.getDossierPreuve(contrat_id).subscribe(
          (response)=>{
           this.dowloadFile(response,"document preuve".toLowerCase().trim()+".zip");
         
          },
          (erreur)=>{
            this.notificationService.showNotificationEchec('echec telechargement du contrat, veillez ressayer')
          },
          ()=>{
                // this.notificationService.showNotificationSuccess("le contra a été bien annuler")
                 this.notificationService.blockUiStop();
          }
        )
  
  
      }
      else if (action.toLowerCase().trim() == "Telecharger le  document envoyé".toLowerCase().trim())
      {
        this.sellAndSignService.getDocumentPdfSellAndSign(contrat_id).subscribe(
          (response:any)=>{
            if(response)
  
            {this.dowloadFile(response,"document".toLowerCase().trim()+".pdf");
       
            }
  
       
          }
        )
      }
    }
    else {
      this.notificationService.showNotificationEchec('veillew selectionnez une action')
    }
  


  }
 dowloadFile(file:any,name:string)
 {

 // It is necessary to create a new blob object with mime-type explicitly set
          // otherwise only Chrome works like it should
          var newBlob = new Blob([file], { type: "application/pdf" });

          // IE doesn't allow using a blob object directly as link href
          // instead it is necessary to use msSaveOrOpenBlob
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }

          // For other browsers: 
          // Create a link pointing to the ObjectURL containing the blob.
          const data = window.URL.createObjectURL(newBlob);

          var link = document.createElement('a');
          link.href = data;
          link.download = name;
          // this is necessary as link.click() does not work on the latest firefox
          link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

          setTimeout(function () {
              // For Firefox it is necessary to delay revoking the ObjectURL
              window.URL.revokeObjectURL(data);
              link.remove();
          }, 100);
 }
}


