import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilContratComponent } from './profil-contrat.component';

describe('ProfilContratComponent', () => {
  let component: ProfilContratComponent;
  let fixture: ComponentFixture<ProfilContratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilContratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
