import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';

import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';

@Component({
  selector: 'app-contra-pld',
  templateUrl: './contra-pld.component.html',
  styleUrls: ['./contra-pld.component.css']
})
export class ContraPldComponent implements OnInit {
  listActions: any[] = [
		{ value: 1, viewValue: 'Exporter Contrat' },
	
  ];
  action: string;
	dataSource: any = new MatTableDataSource();
  selection = new SelectionModel<any[]>(true, []);

  range: Range = { fromDate: new Date(), toDate: new Date() };

  date_debut: number = null;
	date_fin: number = null;


	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];
  
  elements = [];
  
	displayedColumns = [
		'select',
    'number',
    'idPld',
    'date',
    'contrat'

  ];
  
  displayedColumnsFilter = [
		{
			name: 'idPld',
			label: 'ID PLD'
		},
		{
			name: 'type',
			label: 'Type'
		},

  ];
  
  data = {
		filter: null,
		date_debut: null,
    date_fin: null,
    offset: 0,
		limit: 12,
	
  };
  
 
  constructor() { }

  ngOnInit() {
  }



  masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	updateRange(range: Range) {
		//this.notificationService.blockUiStart();

		this.range = range;

		this.date_debut = new Date(this.range.fromDate).getTime();
		this.date_fin = new Date(this.range.toDate).getTime();
  }
  setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
  }
  
  datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
			// excludeWeekends:true,
			// fromMinMax: {fromDate:fromMin, toDate:fromMax},
			// toMinMax: {fromDate:toMin, toDate:toMax}
		};
	}


  valutElements() {
		//  (this.elements.length)
		if (this.elements.length === 0) {
			this.date_debut = null;
			this.date_fin = null;
		
			this.initData();
		} else {
			this.elements.map((item) => {
				if (item == 'date') {
					this.datepickerConfig();
				} else {
					this.date_debut = null;
					this.date_fin = null;
				}
			});
		}
  }
  

  initData() {

  }
  
  onSubmitFiltre(form: NgForm) {
    this.data.filter = form.value;

		this.initData();
	}


}
