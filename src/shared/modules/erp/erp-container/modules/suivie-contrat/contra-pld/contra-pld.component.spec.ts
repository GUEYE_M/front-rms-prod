import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContraPldComponent } from './contra-pld.component';

describe('ContraPldComponent', () => {
  let component: ContraPldComponent;
  let fixture: ComponentFixture<ContraPldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContraPldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContraPldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
