import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContratAValiderComponent } from './contrat-a-valider/contrat-a-valider.component';
import { ContratAbandonnerComponent } from './contrat-abandonner/contrat-abandonner.component';
import { ContratArchiverComponent } from './contrat-archiver/contrat-archiver.component';
import { ContratBientotExpirerComponent } from './contrat-bientot-expirer/contrat-bientot-expirer.component';
import { ContratEnCoursComponent } from './contrat-en-cours/contrat-en-cours.component';
import { ContratValiderComponent } from './contrat-valider/contrat-valider.component';
import { ProfilContratComponent } from './profil-contrat/profil-contrat.component';
import { TousLesContratsComponent } from './tous-les-contrats/tous-les-contrats.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import {SharedModule} from '../../../../shared/shared.module';
import {SuivieContratRouting} from './suivie-contrat.routing';
import { ContraPldComponent } from './contra-pld/contra-pld.component';



@NgModule({
  declarations: [
    ContratAValiderComponent,
    ContratAbandonnerComponent,
    ContratArchiverComponent,
    ContratBientotExpirerComponent,
    ContratEnCoursComponent,
    ContratValiderComponent,
    ProfilContratComponent,
    TousLesContratsComponent,
    WrapperComponent,
    ContraPldComponent],
  imports: [
    CommonModule,
    SharedModule,
    SuivieContratRouting

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SuivieContratModule { }
