import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratAValiderComponent } from './contrat-a-valider.component';

describe('ContratAValiderComponent', () => {
  let component: ContratAValiderComponent;
  let fixture: ComponentFixture<ContratAValiderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratAValiderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratAValiderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
