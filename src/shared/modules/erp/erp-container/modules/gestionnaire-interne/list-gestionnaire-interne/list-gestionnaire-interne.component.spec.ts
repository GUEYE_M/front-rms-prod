import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGestionnaireInterneComponent } from './list-gestionnaire-interne.component';

describe('ListGestionnaireInterneComponent', () => {
  let component: ListGestionnaireInterneComponent;
  let fixture: ComponentFixture<ListGestionnaireInterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGestionnaireInterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGestionnaireInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
