import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {from} from 'rxjs';
import {tap} from 'rxjs/operators';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {EditGestionnaireInterneComponent} from '../edit-gestionnaire-interne/edit-gestionnaire-interne.component';
import {NotificationService} from '../../../../../../services/notification.service';
import {UserService} from '../../../../../../services/user.service';
import {FilterService} from '../../../../../../services/filter.service';
import {RolesService} from '../../../../../../services/roles.service';
import {Router} from '@angular/router';
import {InterimService} from '../../../../../../services/interim/interim.service';
import {SelectionModel} from '@angular/cdk/collections';
import {GestionnaireInterneService} from '../../../../../../services/gestionnaire interne/gestionnaireInterne.service';
import {UtilsService} from '../../../../../../services/utils.service';

@Component({
  selector: 'app-list-gestionnaire-interne',
  templateUrl: './list-gestionnaire-interne.component.html',
  styleUrls: ['./list-gestionnaire-interne.component.css']
})

export class ListGestionnaireInterneComponent implements OnInit {
  formFilter: FormGroup;
  dataSourceInitial = [];
  displayedColumns = ['number', 'Nom', 'Prenom', 'Username', 'Email', 'Etat', 'IdReferentiel', 'Action'];
  displayedColumnsFilter = [];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  listActions = ['Supprimer', 'Desactiver'];
  listsource: any[] = [];
  colListeGestionnaireInterne = 'col-12';
  showLoadInterim: boolean;
  UserForm: FormGroup;
  rolesform: FormGroup;
  dialogRef: MatDialogRef<EditGestionnaireInterneComponent>;
  idGestionnaireInterne = 0;
  loader = 0;
  currentUser: any;


  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,


  };


  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];
  pageEvent: PageEvent;
  pageSize = 12;


  constructor(private interimService: InterimService,
              private gestionnaireInterneService: GestionnaireInterneService,
              private fb: FormBuilder,
              private router: Router,
              private formBuilder: FormBuilder,
              private roleService: RolesService,
              private dialog: MatDialog,
              private rolesService: RolesService,
              private filterService: FilterService,
              private userService: UserService,
              private utilsService: UtilsService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (filterValues) {
        if (this.filterService.sendFilters(this.formFilter)) {
          this.data.filter = this.formFilter.value;
          this.getNewData();
        }
      }
    } else {
      this.initData();
    }

    this.currentUser = this.userService.getCurrentUser();

    this.initUserForm();
    this.initRolesForm();

    this.displayedColumnsFilter = this.gestionnaireInterneService.displayFilterName();
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));


  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }


  onAddGestionnaireInterne() {
    this.colListeGestionnaireInterne = 'col-6';
  }

  initUserForm() {
    this.UserForm = this.fb.group({
      civilite: [''],
      nom: [''],
      prenom: [''],
      telephone: [''],
      email: [''],
      enabled: [true],
      username: [''],
      auteur: [''],
      roles: this.fb.array([this.initRolesForm()])
    });
  }

  initRolesForm() {
    this.rolesform = this.fb.group({
      ROLE_COMMERCIAL: [false],
      ROLE_SUPERVISEUR: [false],
      ROLE_CANDIDAT: [false],
      ROLE_RECRUTEUR: [false],
      ROLE_ADMIN: [true]
    });
    return this.rolesform;

  }

  onActiveOuDesactiveUser(idUser, action) {
    const thiss = this;
    const donnees = {id: idUser, etat: action};
    this.notificationService.onConfirm('Êtes-vous sûr de vouloir continuer ? ');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        action = JSON.parse(action);
        thiss.notificationService.blockUiStart();
        thiss.userService.activerOuDesactive(donnees).subscribe((response) => {
            thiss.notificationService.blockUiStop();
            thiss.initData();
            if (!response['erreur']) {
              // thiss.router.navigate(['/RMS-Admin/redirection-admin/', 'gestionnaire-interne/liste']);
              thiss.notificationService.showNotificationSuccessCopie('bottomRight', response['success']);
            } else {
              thiss.notificationService.showNotificationEchecCopie('bottomRight', response['erreur']);
            }
          },
          error => {
            thiss.initData();
            thiss.notificationService.blockUiStop();
          },
          () => {
            this.utilsService.redirectGIListe();
          });
      } else {
        this.utilsService.redirectGIListe();
      }
    });
  }

  onAddGestionnaireinterne() {
    const user = this.UserForm.value;
    const gestionnaireInterne = {id: null, user: user};
    this.gestionnaireInterneService.create(gestionnaireInterne).subscribe(response => {
      if (!response['erreur']) {
        this.utilsService.redirectGIListe();
        this.notificationService.showNotificationSuccess(response['success']);
      } else {
        this.notificationService.showNotificationEchec(response['erreur']);
      }
    });
  }

  editGestionnaireModal(id: number = null): void {
    this.dialogRef = this.dialog.open(EditGestionnaireInterneComponent, {
      width: '800px',
      height: '600px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {
          this.getNewData();
        }
      }
    );
  }


  initData() {


    this.listsource = [];

    this.notificationService.blockUiStart();
    this.gestionnaireInterneService.visibleList().subscribe(
      (response) => {
        const gI: any[] = [];
        this.length = response['lenght'];

        from(response['data']).pipe(
          tap((gestionnaireInterne: any) => {
            this.notificationService.blockUiStop();
            this.listsource.push(gestionnaireInterne);

            // if(this.rolesService.isSuperAdmin(this.currentUser.roles)) {
            // }
            // if(this.rolesService.isManagerUnique(this.currentUser.roles)) {
            //   gI.push(gestionnaireInterne);
            //   gI.forEach((element) => {
            //     if (!this.rolesService.isSuperAdmin(gestionnaireInterne.roles))
            //       this.listsource.push(gestionnaireInterne);
            //   });
            // }
          })
        ).subscribe();
        this.dataSource = new MatTableDataSource(this.listsource);
        this.dataSourceInitial = this.listsource;
        // this.dataSource.filterPredicate = this.createFilter();


      }
    );


  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();
  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getNewData();


  }

  getNewData() {
    this.listsource = [];
    this.gestionnaireInterneService.visibleList(this.data).subscribe(
      (response) => {
        this.notificationService.blockUiStart();

        this.length = response['lenght'];


        from(response['data']).pipe(
          tap((gestionnaireInterne: any) => {
            if (!this.rolesService.isSuperAdmin(this.currentUser.roles)) {
              const gI: any[] = [];
              gI.push(gestionnaireInterne);
              gI.forEach((element) => {
                if (!this.rolesService.isSuperAdmin(gestionnaireInterne.roles) && !this.rolesService.isComptable(gestionnaireInterne.roles)) {
                  this.listsource.push(gestionnaireInterne);
                }
              });
            } else {
              this.listsource.push(gestionnaireInterne);
            }
          })
        ).subscribe();
        this.dataSource = new MatTableDataSource(this.listsource);
        this.dataSourceInitial = this.listsource;
        // this.dataSource.filterPredicate = this.createFilter();


      },
      () => {
        this.notificationService.showNotificationEchec('erreur  !! veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );

  }

  // ===================================================================================================================================
  ngOnDestroy() {
    this.interimService.showLoadInterim.next(true);
  }
}
