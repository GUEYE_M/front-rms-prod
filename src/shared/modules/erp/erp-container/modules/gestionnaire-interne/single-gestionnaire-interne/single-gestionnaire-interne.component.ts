import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../../../../services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { GestionnaireInterneService } from '../../../../../../services/gestionnaire interne/gestionnaireInterne.service';
import { UtilsService } from 'src/shared/services/utils.service';
import { UserService } from 'src/shared/services/user.service';
import { RolesService } from 'src/shared/services/roles.service';
import { NgForm } from '@angular/forms';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import { DataModel, ChartModel, TypeChartModel } from 'src/shared/chart.model';

@Component({
  selector: 'app-single-gestionnaire-interne',
  templateUrl: './single-gestionnaire-interne.component.html',
  styleUrls: ['./single-gestionnaire-interne.component.css']
})
export class SingleGestionnaireInterneComponent implements OnInit {

  gestionnaireInterne: any;
  barChart: any;
  pie3dChart: any
  annees: any = []

  elements = [];
  data = {
    annee: 2020,
    id: null
  };
  labelPosition: 'p' | 'c' | 't' = 'p';

  listSpecialites = [];
  currentUser: any;
  displayedColumnsFilter = [

    {
      name: "specialite",
      label: "Specialite"
    },
    {
      name: "annee",
      label: "Annee"
    },


  ];

  client: any;
  response: any;
  constructor(
    private gestionnaireInterneService: GestionnaireInterneService,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private utiles: UtilsService,
    private userService: UserService,
    public roleService: RolesService,
    private specialiteService: SpecialiteService,
  ) { }
  ngOnInit() {
    this.chartBar(null)
    this.annees = this.utiles.yearForGraph();
    this.notificationService.blockUiStart();
    this.currentUser = this.userService.getCurrentUser();
    this.data.id = this.route.snapshot.params.id
    this.gestionnaireInterneService.find(this.route.snapshot.params.id).subscribe((response) => {
      this.notificationService.blockUiStop();
      this.gestionnaireInterneService.currentGestionnaireInterne.next(response);
      this.gestionnaireInterne = response;
      (this.gestionnaireInterne);
    },
      error => {
        this.notificationService.blockUiStop();
      });

    this.getData()

  }



  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }



  onSubmitFiltre(form: NgForm) {

    if (form.controls["annee"]) {
      this.data.annee = form.controls["annee"].value

    }

    this.getData();


  }

  newStat() {
    this.loadgrah(this.response)
  }


  valutElements() {

    if (this.elements.length === 0) {
      this.data.annee = 2020;
      this.loadgrah(this.response)

    }

  }

  getAllSpecialite() {
    this.specialiteService.list(null, true).subscribe((response: any) => {
      this.listSpecialites = response.data

    });
  }

  getData() {
    this.gestionnaireInterneService.getStatistique(this.data).subscribe(
      (response: any) => {
        if (!response.erreur) {

          this.loadgrah(response)
          this.response = response

        }
        else {
          this.notificationService.showNotificationEchec(response.erreur)
        }
      }
    );
  }




  chartBar(donnee) {
    let data = new DataModel()



    if (this.labelPosition === "p") {
      data.chart = {
        caption: "Evolution nombre de propection",
        subcaption: "Par mois",
        numberprefix: "",
        numbersuffix: "",
        plottooltext: "   <b>$dataValue</b> message de prospection  pour le mois de $label",
        theme: "fusion"
      }
    }
    else if (this.labelPosition == 'c') {
      data.chart = {
        caption: "Evolution nombre de candidature",
        subcaption: "Par mois",
        numberprefix: "",
        numbersuffix: "",
        plottooltext: "   <b>$dataValue</b> candidature  pour le mois de $label",
        theme: "fusion"
      }


    }

    else if (this.labelPosition == 't') {
      data.chart = {
        caption: "Evolution nombre de tache ",
        subcaption: "Par mois",
        numberprefix: "",
        numbersuffix: "",
        plottooltext: "   <b>$dataValue</b> message de prospection  pour le mois de $label",
        theme: "fusion"
      }
    }
    data.categories =
      [
        {
          category: [
            {
              label: "Janvier"
            },
            {
              label: "Fevrier"
            },
            {
              label: "Mars"
            },
            {
              label: "Avril"
            },
            {
              label: "Mai"
            },
            {
              label: "Juin"
            },
            {
              label: "Juillet"
            },
            {
              label: "Aout"
            },
            {
              label: "Septembre"
            },
            {
              label: "Octobre"
            },
            {
              label: "Novembre"
            },
            {
              label: "Décembre"
            },

          ]
        }
      ];

    if (this.labelPosition == "p") {
      data.dataset =
        [
          {
            seriesname: "Message",
            data: donnee
          },

        ]
    }

    else {
      data.dataset = donnee
    }








    this.barChart = new TypeChartModel('100%', 500, "msspline", "json", data)
  }



  loadgrah(response) {
    if (this.labelPosition === "p") {

      this.chartBar(response.prospection.data)
    }

    else if (this.labelPosition == "c") {
      this.chartBar(response.candidature)
    }

    else if (this.labelPosition == "t") {
      this.chartBar(response.taches)
    }


  }
}
