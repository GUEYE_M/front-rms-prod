import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleGestionnaireInterneComponent } from './single-gestionnaire-interne.component';

describe('SingleGestionnaireInterneComponent', () => {
  let component: SingleGestionnaireInterneComponent;
  let fixture: ComponentFixture<SingleGestionnaireInterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleGestionnaireInterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleGestionnaireInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
