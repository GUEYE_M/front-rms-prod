import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacheGestionnaireInterneComponent } from './tache-gestionnaire-interne.component';

describe('TacheGestionnaireInterneComponent', () => {
  let component: TacheGestionnaireInterneComponent;
  let fixture: ComponentFixture<TacheGestionnaireInterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacheGestionnaireInterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacheGestionnaireInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
