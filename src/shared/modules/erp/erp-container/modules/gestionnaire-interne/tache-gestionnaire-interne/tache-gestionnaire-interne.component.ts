import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatDialogRef, MatDialog} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {GestionnaireInterneService} from '../../../../../../services/gestionnaire interne/gestionnaireInterne.service';
import {TacheService} from '../../../../../../services/erp/tache.service';
import {RangeDateService} from '../../../../../../services/range.date.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {SelectionModel} from '@angular/cdk/collections';
import { ViewMessageProdpectionModalComponent } from '../../prospection/view-message-prodpection-modal/view-message-prodpection-modal.component';
import {UserService} from '../../../../../../services/user.service';

@Component({
  selector: 'app-tache-gestionnaire-interne',
  templateUrl: './tache-gestionnaire-interne.component.html',
  styleUrls: ['./tache-gestionnaire-interne.component.css']
})
export class TacheGestionnaireInterneComponent implements OnInit {

  dialogRef: MatDialogRef<ViewMessageProdpectionModalComponent>;

  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  panelOpenState = false;
  idGi: any;
  displayedColumns = ['select','number','Statut', 'Ligne' ,'Type','Heure Debut','Heure Fin','Date Debut','Date Fin','Description'];

  expandedElement: any | null;
  customRanges: any;
  ranges: any;
  constructor(private gestionnaireInterneService: GestionnaireInterneService,
              private tacheService: TacheService,
              private rangedateService: RangeDateService,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private userService: UserService,
              private notificationService: NotificationService) { }

  ngOnInit() {

    this.customRanges = this.rangedateService.customRanges;
    this.ranges = this.rangedateService.ranges
    if(this.route.snapshot.params.id)
      this.idGi = this.route.snapshot.params.id;
    else
      this.idGi =  this.userService.getCurrentUser().gestionnaire.id;
    this.tacheService.tacheGestionnaire(this.idGi);

    this.tacheService.tacheGestionnaireSubject.subscribe(
      (response:any) => {


        this.dataSource = new MatTableDataSource(response);
        if(this.dataSource.data) {
          this.dataSource.data.map(
            (data: any) => {
              if(data.etat === 0)
              {
                this.tacheService.tache_encours_gestionnaire.push(data);

              }
            }
          );
        }
        // this.tacheService.tacheEnCoursGestionnaireSubject.next(this.tacheService.tache_encours_gestionnaire)
        this.tacheService.emitTachesGestionnaireEncCours();
      },
      ()=>{

      },
      ()=>{

      }
    )


  }


  message(message:any)
  {

    this.dialogRef = this.dialog.open(ViewMessageProdpectionModalComponent, {
      width: '800px',
      height: '550px',

      panelClass: 'myapp-no-padding-dialog',
      data:message

    });
  }


  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  callback(event) {
    //this.notificationService.blockUiStart();

    (event.start,event.end)
    // event.start,event.end
    let data:any = {
      "date_debut":event.start,
      "date_fin":event.end,
      "gestionnaire":this.idGi

    }

    this.tacheService.tacheBydate(data).subscribe(
      (response:any)=>{
        if(response.data == null)
        {
          if(event.start === event.end )
          {
            this.notificationService.showNotificationSuccess("Aucune Tache disponible pour Aujourd'hui")
          }
          else{
            this.notificationService.showNotificationSuccess("Aucune Tache pour la date du " +event.start+ " au " +event.end)
          }

        }
        this.dataSource = new MatTableDataSource(response.data);

      },
      (erreur)=>{
        this.notificationService.showNotificationEchec('erreur chargement veillez ressayer , consilter le service IT si le probleme persiste')
        this.notificationService.blockUiStop();
      },
      ()=>{
        this.notificationService.blockUiStop();
      }
    )

    //(event.start,event.end)


  }

}
