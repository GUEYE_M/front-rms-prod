import {Route, RouterModule} from '@angular/router';
import {ListGestionnaireInterneComponent} from './list-gestionnaire-interne/list-gestionnaire-interne.component';
import {EditGestionnaireInterneComponent} from './edit-gestionnaire-interne/edit-gestionnaire-interne.component';
import {SingleGestionnaireInterneComponent} from './single-gestionnaire-interne/single-gestionnaire-interne.component';

const GI_CONFIG: Route[] = [
  {
    path: 'liste',
    component: ListGestionnaireInterneComponent,
  },
  {
    path: 'edit',
    component: EditGestionnaireInterneComponent,
  },
  {
    path: ':id/show',
    component: SingleGestionnaireInterneComponent
  },

];

export const GIRouting = RouterModule.forChild(GI_CONFIG);



