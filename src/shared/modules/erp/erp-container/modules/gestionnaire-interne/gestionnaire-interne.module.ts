import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListGestionnaireInterneComponent } from './list-gestionnaire-interne/list-gestionnaire-interne.component';
import {SharedModule} from '../../../../shared/shared.module';
import {GIRouting} from './gestionnaire-interne.routing';
import { EditGestionnaireInterneComponent } from './edit-gestionnaire-interne/edit-gestionnaire-interne.component';
import { SingleGestionnaireInterneComponent } from './single-gestionnaire-interne/single-gestionnaire-interne.component';
import { TacheEnCoursComponent } from './tache-en-cours/tache-en-cours.component';



@NgModule({
  declarations: [
    ListGestionnaireInterneComponent,
    EditGestionnaireInterneComponent,
    SingleGestionnaireInterneComponent,
    TacheEnCoursComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    GIRouting,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class GestionnaireInterneModule { }
