import { Component, OnInit } from '@angular/core';
import {TacheService} from '../../../../../../services/erp/tache.service';
import {NotificationService} from '../../../../../../services/notification.service';

@Component({
  selector: 'app-tache-en-cours',
  templateUrl: './tache-en-cours.component.html',
  styleUrls: ['./tache-en-cours.component.css']
})
export class TacheEnCoursComponent implements OnInit {
  step = 0;

  taches:any [] = [];
  listActions: any[];

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  constructor(private tacheService:TacheService,
              private notificationService:NotificationService) { }

  ngOnInit() {



    this.listActions = this.tacheService.etats;

    this.listActions.splice(0,1);
    this.tacheService.tacheEnCoursGestionnaireSubject.subscribe(
      (response)=>{
        if(response.length > 0)
        {
          this.taches = response
        }
        else {
          this.notificationService.showNotificationSuccess("Aucune  tache diponible pour vous ")
        }

      }
    )
  }


  actionbtn(action: any, id:any)
  {
    this.notificationService.blockUiStart();
    let item  = {
      etat : action,
      id : []
    }

    item.id.push(id);

    if(item.id.length > 0)
    {

      this.tacheService.updateAvancement(item).subscribe(
        (response)=>{
          (response)
        },
        (erreur)=>{
          this.notificationService.showNotificationEchec('erreur chargement veillez ressayer , consilter le service IT si le probleme persiste')
          this.notificationService.blockUiStop();
        },
        ()=>{

          this.tacheService.list_taches_gestionnaire.map(
            (item:any)=>{
              if(item.id == id)
              {
                // const index = this.tacheService.list_taches_gestionnaire.indexOf(item)
                // this.tacheService.list_taches_gestionnaire[index].etat = action ;
                // this.tacheService.emitTachesGestionnaire()




              }
            }
          )
          this.tacheService.tache_encours_gestionnaire.map(
            (item:any)=>{
              if(item.id == id)
              {
                (item)
                const index2 = this.tacheService.tache_encours_gestionnaire.indexOf(item)
                this.tacheService.tache_encours_gestionnaire.splice(index2,1)
                this.tacheService.emitTachesGestionnaireEncCours();


              }
            }
          )




          this.notificationService.showNotificationSuccess('Mise a jour effectuer avec success');
          this.notificationService.blockUiStop();
        }
      )

    }
  }

}
