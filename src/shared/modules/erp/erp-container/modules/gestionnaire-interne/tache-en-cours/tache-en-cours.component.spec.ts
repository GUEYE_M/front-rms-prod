import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacheEnCoursComponent } from './tache-en-cours.component';

describe('TacheEnCoursComponent', () => {
  let component: TacheEnCoursComponent;
  let fixture: ComponentFixture<TacheEnCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacheEnCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacheEnCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
