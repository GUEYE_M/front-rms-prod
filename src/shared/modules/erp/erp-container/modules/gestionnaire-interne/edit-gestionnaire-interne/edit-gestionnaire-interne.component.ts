import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material";
import { Client } from "../../../../../../models/recruteur/Client.model";
import { NotificationService } from "../../../../../../services/notification.service";
import { ApiVilleService } from "../../../../../../services/api_ville_service";
import { RecruteurService } from "../../../../../../services/recruteur/recruteur.service";
import { AuthNormalService } from "../../../../../../services/authNormal.service";
import { PldService } from "../../../../../../services/pld.service";
import { UserService } from "../../../../../../services/user.service";
import { RolesService } from "../../../../../../services/roles.service";
import { ActivatedRoute, Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { RolesModel } from "../../../../../../models/Roles.model";
import { GestionnaireInterneService } from "../../../../../../services/gestionnaire interne/gestionnaireInterne.service";
import { UtilsService } from "../../../../../../services/utils.service";

@Component({
  selector: "app-edit-gestionnaire-interne",
  templateUrl: "./edit-gestionnaire-interne.component.html",
  styleUrls: ["./edit-gestionnaire-interne.component.css"]
})
export class EditGestionnaireInterneComponent implements OnInit {
  giForm: FormGroup;
  commerciauxPld: any;
  edit: boolean = false;
  displayName: string;

  rolesArray = new RolesModel();
  rolesArraykeys = Object.keys(this.rolesArray);
  choixCheckBox = [];
  gI = [];
  isViewFormRecruteur: BehaviorSubject<boolean> = new BehaviorSubject(false);
  currentUser: any;
  filteredCommune: Observable<any[]>;
  newGi = {
    civilite: "",
    nom: "",
    prenom: "",
    telephone: "",
    email: "",
    enabled: true,
    username: "",
    auteur: "",
    roles: ""
  };

  idGI: number;
  constructor(
    private fb: FormBuilder,
    private gestionnaireInterneService: GestionnaireInterneService,
    public dialogRef: MatDialogRef<EditGestionnaireInterneComponent>,
    private router: Router,
    private route: ActivatedRoute,
    private rolesService: RolesService,
    private userService: UserService,
    private pldService: PldService,
    private utilsService: UtilsService,
    private authNormalService: AuthNormalService,
    private recruteurService: RecruteurService,
    private apiVilleService: ApiVilleService,
    private dialog: MatDialog,
    public notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: Client
  ) {}

  ngOnInit() {
    this.onGetIdReferentielPld();
    this.utilsService.onRevientEnHaut();
    this.currentUser = this.userService.getCurrentUser();
    if (!this.rolesService.isSuperAdmin(this.currentUser.roles)) {
      const rolesArraykeys: any[] = [];
      this.rolesArraykeys.forEach(element => {
        if (
          element !== "manager" &&
          element !== "comptable" &&
          element !== "superAdmin"
        )
          rolesArraykeys.push(element);
      });
      this.rolesArraykeys = rolesArraykeys;
    }
    if (this.data) {
      this.idGI = +this.data;
      this.gestionnaireInterneService.find(this.idGI).subscribe(response => {
        this.displayName =
          "Mise à jour du gestionnaire interne: " +
          response.prenom +
          " " +
          response.nom;
        this.gI = response;
        this.initform(response);
        this.notificationService.blockUiStop();
        this.edit = true;
        if (this.gI) {
          this.rolesArraykeys.forEach((element, index) => {
            if (Array.isArray(this.gI["roles"])) {
              this.gI["roles"].forEach(item => {
                if (this.rolesArray[element] === item) {
                  this.choixCheckBox.push(index);
                }
              });
            }
          });
        }
      });
    } else {
      this.displayName = "Nouvel enregistrement ";
      this.initform();
    }
  }
  initform(
    gi = {
      idGi: "",
      username: "",
      nom: "",
      prenom: "",
      IdReferentiel: "",
      email: "",
      roles: [],
      civilite: "",
      telephone: ""
    }
  ) {
    this.giForm = this.fb.group({
      idGi: [gi.idGi, Validators.required],
      nom: [gi.nom, Validators.required],
      prenom: [gi.prenom, Validators.required],
      email: [gi.email, Validators.required],
      roles: [],
      IdReferentiel: [gi["idReferentiel"]],
      username: [gi.username, Validators.required],
      civilite: [gi.civilite, Validators.required],
      telephone: [gi.telephone, Validators.required]
    });
  }
  save() {
    const rolesArrayNew = [];
    this.rolesArraykeys.forEach((element, index) => {
      this.choixCheckBox.forEach(item => {
        if (index === item) rolesArrayNew.push(this.rolesArray[element]);
      });
    });
    this.giForm.get("roles").setValue(rolesArrayNew);
    this.notificationService.blockUiStart();

    if (this.edit) {
      this.gestionnaireInterneService
        .update(+this.data, this.giForm.value)
        .subscribe(
          (response: any) => {
            if (!response.erreur) {
              this.notificationService.showNotificationSuccessCopie(
                "bottomRight",
                response.success
              );
              this.authNormalService.refreshUser();
              this.dialogRef.close(true);
            } else {
              this.notificationService.showNotificationEchecCopie(
                "bottomRight",
                response.erreur
              );
            }
          },
          erreur => {
            this.notificationService.blockUiStop();
            this.notificationService.showNotificationEchecCopie(
              "bottomRight",
              "erreur  de connexion , contacter le service IT si le probleme persiste"
            );
          },
          () => {
            this.notificationService.blockUiStop();
          }
        );
    } else {
      this.newGi.auteur = this.currentUser.username;
      this.newGi.civilite = this.giForm.value["civilite"];
      this.newGi.nom = this.giForm.value["nom"];
      this.newGi.prenom = this.giForm.value["prenom"];
      this.newGi.roles = this.giForm.value["roles"];
      this.newGi.email = this.giForm.value["email"];
      this.newGi.username = this.giForm.value["username"];
      this.newGi.telephone = this.giForm.value["telephone"];
      const gestionnaireInterne = { id: null, user: this.newGi };
      this.gestionnaireInterneService
        .create(gestionnaireInterne)
        .subscribe(response => {
          this.notificationService.blockUiStop();
          if (!response["erreur"]) {
            this.notificationService.showNotificationSuccessCopie(
              "bottomLeft",
              response["success"]
            );
            this.dialogRef.close(true);
          } else {
            this.notificationService.showNotificationEchecCopie(
              "bottomLeft",
              response["erreur"]
            );
          }
        });
    }
  }

  onGetIdReferentielPld() {
    this.notificationService.blockUiStart();
    this.pldService.listCommerciaux().subscribe(
      rep => {
        if (rep) {
          this.commerciauxPld = JSON.parse(rep);
          this.notificationService.blockUiStop();
        }
      },
      error => {
        this.notificationService.blockUiStop();
      }
    );
  }
}
