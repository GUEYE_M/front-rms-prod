import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGestionnaireInterneComponent } from './edit-gestionnaire-interne.component';

describe('EditGestionnaireInterneComponent', () => {
  let component: EditGestionnaireInterneComponent;
  let fixture: ComponentFixture<EditGestionnaireInterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGestionnaireInterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGestionnaireInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
