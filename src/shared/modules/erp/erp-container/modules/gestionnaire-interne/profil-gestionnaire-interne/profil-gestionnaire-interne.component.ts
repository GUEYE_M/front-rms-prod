import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GestionnaireInterneService } from '../../../../../../services/gestionnaire interne/gestionnaireInterne.service';
import { NotificationService } from 'src/shared/services/notification.service';
import { UserService } from '../../../../../../services/user.service';

@Component({
  selector: 'app-profil-gestionnaire-interne',
  templateUrl: './profil-gestionnaire-interne.component.html',
  styleUrls: ['./profil-gestionnaire-interne.component.css']
})
export class ProfilGestionnaireInterneComponent implements OnInit {

  keysUserItems = [];
  currentinterim: any;
  photo: string;

  color = 'primary';
  color1 = 'warn';
  color2 = 'accent';
  mode = 'determinate';
  value = 50;
  bufferValue = 75;
  panelOpenState = false;
  data = {
    id: null,
    date_debut: null,
    date_fin: null
  }
  statistique: any

  constructor(private gestionnaireInterneService: GestionnaireInterneService, private notificationService: NotificationService,
    private route: ActivatedRoute,
    private userService: UserService,
  ) { }

  ngOnInit() {
    if (this.route.snapshot.params.id)
      this.data.id = this.route.snapshot.params.id;
    else
      this.data.id = this.userService.getCurrentUser().gestionnaire.id;

    this.gestionnaireInterneService.find(this.data.id).subscribe((response) => {
      this.currentinterim = response;
      let keys = Object.keys(response);
      for (let key of keys) {
        if (
          key !== 'idGi'
          && key !== 'idUser'
          && key !== 'enabled'
          && key !== 'photo'
        ) {
          this.keysUserItems.push(key);
        }
        if (
          key !== 'idGi'
          && key !== 'idUser'
          && key !== 'enabled'
          && key !== 'photo'
        ) {
          this.photo = response.photo;
        }
        // this.loader = false;
        // this.gestionnaireInterneService.currentGestionnaireInterne.next(response);
        // this.gestionnaireInterne = response;
      }
    });



  }




}
