import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilGestionnaireInterneComponent } from './profil-gestionnaire-interne.component';

describe('ProfilGestionnaireInterneComponent', () => {
  let component: ProfilGestionnaireInterneComponent;
  let fixture: ComponentFixture<ProfilGestionnaireInterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilGestionnaireInterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilGestionnaireInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
