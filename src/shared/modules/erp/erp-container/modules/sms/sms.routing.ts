import {Route, RouterModule} from '@angular/router';
import { TransactionComponent } from './transaction/transaction.component';


const SMS_CONFIG: Route[] = [
 
  {path: 'transactionnels', component: TransactionComponent},
//   {path: 'contacts', component: TransactionnelComponent},
//   {path: 'campagnes', component: CampagneComponent, children: [
//       {path: 'create-campagne', component: CreateCampagneComponent}
//     ]
//   },
];

export const SMSRouting = RouterModule.forChild(SMS_CONFIG);



