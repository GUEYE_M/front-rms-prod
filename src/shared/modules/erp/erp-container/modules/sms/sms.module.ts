import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionComponent } from './transaction/transaction.component';
import { TransactionModalComponent } from './transaction/transaction-modal/transaction-modal.component';
import { SMSRouting } from './sms.routing';



@NgModule({
  declarations: [TransactionComponent],
  imports: [
    CommonModule,
    SMSRouting
  ],
  entryComponents:[]


})
export class SmsModule { }
