import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'src/shared/services/notification.service';
import { SendinblueService } from 'src/shared/services/sendinblue.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-transaction-modal',
  templateUrl: './transaction-modal.component.html',
  styleUrls: ['./transaction-modal.component.css']
})
export class TransactionModalComponent implements OnInit {


  editForm: FormGroup;
  displayName: string;
  edit: boolean = false;
  data_send: { 'sender': any; 'sender.name': string; 'sender.email': string; 'to': any[]; 'subject': string; 'message': string; 'templateId': number; 'replyTo.email': string; 'replyTo.name': string; };
  list_emails: any[];
  to = [];

  constructor(private fb: FormBuilder, private notificationService: NotificationService, private sendInblueService: SendinblueService,
    public dialogRef: MatDialogRef<TransactionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (this.data) {
      (data);


      let sender: any = {
        'name': 'RMS'
      };


      if (this.data.client) {
        this.data.selection.forEach(
          (item) => {
            item.recruteurs.forEach(element => {
              this.to.push(
                {

                  'telephone': element.telephone,

                }
              );
            });
          }
        );
      } else {
        (this.data.selection)
        this.data.selection.map(
          (el) => {
            this.to.push(
              {
                'telephone': el.telephone,


              }
            );
          }
        );
      }

      (this.to)


    }




  }


  ngOnInit() {

    this.editForm = this.fb.group(
      {

        recipient: [],
        type: [],
        sender: [null, [Validators.required]],
        content: [null, [Validators.required]],


      }
    );


  }


  sendSMS() {


    if (this.editForm.valid) {


      if (this.to.length > 0) {


        this.to.forEach(
          (item) => {
            this.notificationService.blockUiStart();

            this.editForm.get('recipient').setValue(item.telephone);
            this.editForm.get('type').setValue("transactional");


            this.sendInblueService.sendSMS(this.editForm.value).subscribe(
              (response) => {
                this.notificationService.blockUiStop();

                this.notificationService.showNotificationSuccess("sms envoyer  avec success");
                this.dialogRef.close()

              },
              (erreur) => {
                this.notificationService.showNotificationEchec('erreur server veillez ressayer a nouveau');
                this.notificationService.blockUiStop();
              },
              () => {
              }
            );

          }
        )



      }
    } else {
      this.notificationService.showNotificationEchec('Formulaire invalide');
    }
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }
}
