import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFicheClientComponent } from './create-fiche-client.component';

describe('CreateFicheClientComponent', () => {
  let component: CreateFicheClientComponent;
  let fixture: ComponentFixture<CreateFicheClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFicheClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFicheClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
