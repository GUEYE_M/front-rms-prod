import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../../../../services/notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from '../../../../../../../../../services/recruteur/client.service';
import {FicheClientService} from '../../../../../../../../../services/recruteur/fiche-client.service';

@Component({
  selector: 'app-create-fiche-client',
  templateUrl: './create-fiche-client.component.html',
  styleUrls: ['./create-fiche-client.component.css']
})
export class CreateFicheClientComponent implements OnInit, OnDestroy {

  @Input() ficheClient: any = [];
  ficheClientForm: FormGroup;
  edit: any;
  disabled: any = true;
  id_client = 0;
  hebergement_internat: any;
  repas: any;
  panelOpenState = false;
  showDiv = false;
  showLoadValide = false;

  constructor(private formBuilder: FormBuilder,
              private ficheClientService: FicheClientService,
              private clientService: ClientService,
              private router: Router,
              private notificationService: NotificationService,
              private activated: ActivatedRoute,
  ) {
    this.hebergement_internat = new FormControl();
    this.repas = new FormControl();
  }

  ngOnInit() {
    this.initFicheClientForm();
    this.id_client = this.activated.snapshot.params.id;
    this.ficheClientService.ficheClientSubject.next(this.ficheClient);
    this.ficheClientService.ficheClientSubject.subscribe(
      (next) => {
        if (next) {
          if (next.id) {
            this.edit = true;
          } else {
            this.edit = false;
          }
          this.ficheClient = next;
          this.showDiv = true;
        }
      }
    );
  }

  initFicheClientForm() {
    this.ficheClientForm = this.formBuilder.group({
      situation_de_la_structure: ['', Validators.required],
      distance_avec_gare: [''],
      distance_avec_aeroport: [''],
      info_pratique_ville: [''],
      hebergement_internat: [],
      remarque_hebergement: [''],
      remarque_repas: [''],
      taxi: [''],
      repas: ['']

    });
  }


  onCreateFicheClient() {
    this.showLoadValide = true;
    const ficheClientData = this.ficheClientForm.value;
    ficheClientData.client = this.id_client;
    ficheClientData.hebergement_internat = this.hebergement_internat.value.toString();
    ficheClientData.repas = this.repas.value.toString();
    this.ficheClientService.create(ficheClientData).subscribe(
      () => {
        // recuperation des fiches client
        this.ficheClientService.ficheClientSubject.next([]);
        this.notificationService.blockUiStart();
        this.ficheClientService.getFicheClient(this.id_client).subscribe(
          (next) => {
            this.notificationService.blockUiStop();
            this.ficheClient = next[0] ? next[0] : [];
            this.ficheClientService.ficheClientSubject.next(next[0]);
            this.notificationService.showNotificationSuccessCopie("bottomLeft","sauvegarde reussie")
          });
      },
      (error2) => {
      }
    );
  }

  onAnnuler() {
    this.edit = true;
    this.disabled = true;
  }

  onCreateFicheClientEdit(id) {
    this.notificationService.blockUiStart();
    this.showLoadValide = true;
    const ficheClientData = this.ficheClientForm.value;
    ficheClientData.id = this.ficheClient.id;
    ficheClientData.hebergement_internat = this.hebergement_internat.value.toString();
    ficheClientData.repas = this.repas.value.toString();
    this.ficheClientService.update(ficheClientData, id).subscribe(
      () => {
        this.ficheClientService.getFicheClient(this.id_client).subscribe(
          (next) => {
            this.notificationService.blockUiStop();
            this.ficheClient = next[0] ? next[0] : [];
            this.ficheClientService.ficheClientSubject.next(next[0]);

            this.notificationService.showNotificationSuccessCopie("bottomLeft","mis à jour reussie")
          });
      }
    );
  }

  ngOnDestroy() {
    this.ficheClientService.ficheClientSubject.next(null);
  }

  onEdit(ficheClient) {
    this.hebergement_internat = new FormControl(Array.of(ficheClient.hebergemen_internat));
    (this.hebergement_internat);
    this.repas = new FormControl(Array.of(ficheClient.repas));
    this.ficheClientForm.patchValue({
      situation_de_la_structure: ficheClient.situation_de_la_structure,
      distance_avec_gare: ficheClient.distance_avec_gare,
      distance_avec_aeroport: ficheClient.distance_avec_aeroport,
      info_pratique_ville: ficheClient.info_pratique_ville,
      hebergement_internat: ficheClient.hebergement_internat,
      remarque_hebergement: ficheClient.remarque_hebergement,
      remarque_repas: ficheClient.remarque_repas,
      taxi: ficheClient.taxi,
      repas: ficheClient.repas
    });
    this.edit = false;
    this.disabled = false;
    // 77
  }

}
