import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../../../../../../../services/notification.service';
import { UserService } from '../../../../../../../../../services/user.service';
import { InterimService } from '../../../../../../../../../services/interim/interim.service';
import { ActivatedRoute } from '@angular/router';
import { MessagaProspectionService } from '../../../../../../../../../services/messaga-prospection.service';

@Component({
	selector: 'app-edit-prospection',
	templateUrl: './edit-prospection.component.html',
	styleUrls: [ './edit-prospection.component.css' ]
})
export class EditProspectionComponent implements OnInit {
	messageForm: FormGroup;
	col = 'col-md-12';
	displayName: string;
	edit = false;
	date_rapel = false;
	list_type: any[];
	gestionnaire: any;
	user: number;
	client_prospect = false;

	recruteurs: any;

	constructor(
		private fb: FormBuilder,
		private notificationService: NotificationService,
		private userService: UserService,
		private interimService: InterimService,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private prospectionService: MessagaProspectionService,
		private route: ActivatedRoute,
		public dialogRef: MatDialogRef<EditProspectionComponent>
	) {
		if (this.data.recruteur) {
			this.recruteurs = data.recruteur;
			this.user = data.id;
			this.client_prospect = true;
		}
		if (this.data.id) {
			this.displayName = 'Mis a jour message prospection';
			this.notificationService.blockUiStart();

			this.prospectionService.find(+this.data.id).subscribe((response: any) => {
				response;
				this.initform(response.data);

				if (response.data.date_rapel) {
					this.messageForm.get('date_rapel').setValue(new Date(response.data.date_rapel.date));
				}

				this.edit = true;

				this.messageForm.patchValue({
					type: response.data.type,
					id: this.data.id
				});

				if (this.client_prospect) {
					response.data.user;
					this.recruteurs.map((item) => {
						if (+item.idUser === response.data.user) {
							this.messageForm.patchValue({
								recruteur: item
							});
						}
					});
				}
				this.typeChoisi();

				this.notificationService.blockUiStop();
			});
		} else {
			this.displayName = 'Nouveau Message de Prospection';
			this.initform();
		}
	}

	ngOnInit() {
		if (this.data.recruteur) {
		} else {
			this.interimService.currentInterimaire.subscribe((response) => {
				if (this.data.modal) {
					this.user = this.data.user_id;
				} else {
					this.user = response.interim.user.id;
				}
			});
		}
		this.gestionnaire = this.userService.getCurrentUser();

		this.prospectionService.listTypeProspection().subscribe((response: any) => {
			// (response);
			this.list_type = response.data;
		});
	}
	initform(
		message = {
			type: '',
			date_rapel: null,
			commentaire: '',
			a_rappeler: null,
			auteur: null,
			user: null,
			recruteur: ''
		}
	) {
		this.messageForm = this.fb.group({
			id: [],
			type: [ message.type, [ Validators.required ] ],
			date_rapel: [ message.date_rapel ],
			commentaire: [ message.commentaire, [ Validators.required ] ],
			a_rappeler: [ message.a_rappeler ],
			auteur: [],
			recruteur: [],
			user: [ message.user ],
			client: []
		});
	}
	compareWithFunc(a, b) {
		if (a && b) {
			return a.id === b.id;
		}
	}
	typeChoisi() {
		const str = 'A RAPPELER';

		if (str.trim().toUpperCase() === this.messageForm.get('type').value.trim().toUpperCase()) {
			this.col = 'col-md-6';
			this.date_rapel = true;
			this.messageForm.get('a_rappeler').setValue(true);
		} else {
			this.col = 'col-md-12';
			this.date_rapel = false;
			this.messageForm.get('a_rappeler').setValue(false);
		}
	}
	saveProspection() {
		//  (this.user);
		this.notificationService.blockUiStart();
		if (this.messageForm.valid) {
			this.messageForm.get('auteur').setValue(this.gestionnaire.idUser);

			if (this.client_prospect && this.messageForm.get('recruteur').value) {
				this.messageForm.get('user').setValue(this.messageForm.get('recruteur').value.idUser);
				this.messageForm.get('client').setValue(this.data.client);
			} else {
				this.messageForm.get('user').setValue(this.user);
			}

			const type = this.list_type.find((type) => type.designation === this.messageForm.get('type').value);

			this.messageForm.get('type').setValue(type.id);

			// tslint:disable-next-line:no-unused-expression
			this.messageForm.value;

			if (this.edit) {
				if (!this.date_rapel) {
					this.messageForm.get('date_rapel').setValue(null);
				}

				this.prospectionService.update(this.data, this.messageForm.value).subscribe(
					(response: any) => {
						if (response.erreur) {
							//  this.prospectionService.closemodale.next(false);

							this.notificationService.showNotificationEchec(response.erreur);
						} else {
							//  this.prospectionService.list_message_by_prospect.forEach(
							//    (item:any)=>{
							//      if(item.id == this.data.id)
							//      {
							//        const index = this.prospectionService.list_message_by_prospect.indexOf(item)
							//        this.prospectionService.list_message_by_prospect[index] = response.data

							//      }
							//    }
							//  )

							this.dialogRef.close(true);

							// this.prospectionService.emitMessageByProspect();
							this.notificationService.showNotificationSuccess(response.success);
							// this.prospectionService.closemodaleAddMsg.next(true);
							this.notificationService.blockUiStop();
						}
					},
					(erreur) => {
						this.notificationService.showNotificationEchec(
							'erreur  de connexion , contacter le service IT si le probleme persiste'
						);
					},
					() => {
						this.notificationService.blockUiStop();
					}
				);
			} else {
				this.prospectionService.create(this.messageForm.value).subscribe(
					(response: any) => {
						this.notificationService.blockUiStop();

						if (response.erreur) {
							this.prospectionService.closemodaleAddMsg.next(false);

							this.notificationService.showNotificationEchec(response.erreur);
						} else {
							this.dialogRef.close(true);
							this.notificationService.showNotificationSuccess(response.success);
							this.notificationService.blockUiStop();
						}
					},
					() => {},
					() => {
						this.notificationService.blockUiStop();
					}
				);
			}
		} else {
			this.notificationService.showNotificationEchecCopie('bottomLeft', 'formulaire invalide !');
			this.notificationService.blockUiStop();
		}
	}
}
