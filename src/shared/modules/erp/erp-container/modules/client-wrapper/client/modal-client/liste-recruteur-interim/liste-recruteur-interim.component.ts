import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-liste-recruteur-interim',
  templateUrl: './liste-recruteur-interim.component.html',
  styleUrls: ['./liste-recruteur-interim.component.css']
})
export class ListeRecruteurInterimComponent implements OnInit {
  listeRecruteur = [];
  constructor(
    public dialogRef: MatDialogRef<ListeRecruteurInterimComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.listeRecruteur = this.data.recruteur;
  }

}
