import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {FicheSpecialiteClientEditComponent} from '../fiche-specialite-client-edit/fiche-specialite-client-edit.component';
import {ActivatedRoute} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import {FicheSpeciliate} from '../../../../../../../../models/recruteur/FicheSpeciliate.model';
import {FicheSpecialiteService} from '../../../../../../../../services/recruteur/fiche-specialite.service';
import {FilterService} from '../../../../../../../../services/filter.service';
import {SpecialiteService} from '../../../../../../../../services/specialite.service';
import {NotificationService} from '../../../../../../../../services/notification.service';


@Component({
  selector: 'app-fiche-specialite-client',
  templateUrl: './fiche-specialite-client.component.html',
  styleUrls: ['./fiche-specialite-client.component.css']
})
export class FicheSpecialiteClientComponent implements OnInit, AfterViewInit {
  formFilter: FormGroup;
  pageSize: number = 12;
  listFicheSpecialite: FicheSpeciliate[];
  actionInput: Boolean = false;
  action: string;

  uri_listes_plannings = 'fiche-specialite/listes-filter';
  public form: FormGroup;
  displayedColumns = ['number', 'Nom Etablissement', 'Specialite', 'Nombre Lit',
    'Medecin Jour', 'Medecin Nuit', 'Passage Annee', 'Coeficient Saison',
    'Coeficient Tarification', 'Edit'];

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
  };
  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];

  displayedColumnsFilter = [];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Supprimer', 'Desactiver'];
  listsource: any[] = [];
  editView: boolean = false;
  listView: Boolean = true;
  dialogRef: MatDialogRef<FicheSpecialiteClientEditComponent>;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  constructor(private ficheSpeService: FicheSpecialiteService,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private ficheSpecialiteService: FicheSpecialiteService,
              private filterService: FilterService,
              private dialog: MatDialog,
              private specialiteService: SpecialiteService,
              private notificationService: NotificationService) {

    this.initData();


  }

  ngOnInit() {
    this.formFilter = this.formBuilder.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    }

    this.notificationService.blockUiStop();
    this.displayedColumnsFilter = this.ficheSpecialiteService.displayFilterName();
    this.ficheSpeService.closemodale.subscribe(
      (response: boolean) => {
        if (response) {
          this.dialogRef.close();
        }
      }
    );

  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  filtrer(filtre: string) {
    filtre = filtre.trim();
    filtre = filtre.toLowerCase();
    this.dataSource.filter = filtre;
  }

  add() {
    //this.openDialog();
    this.editView = true;
    this.listView = false;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(FicheSpecialiteClientEditComponent, {
      width: '600px',
      height: '550px',

      panelClass: 'myapp-no-padding-dialog',

    });

    dialogRef.afterClosed().subscribe(result => {
      this.editView = false;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  editFicheSpecilaiteModal(client = {id: null}, edit: boolean = false, id_fiche: number = null): void {
    this.notificationService.blockUiStart();
    this.specialiteService.list(null, true).subscribe(
      (spe: any) => {
        this.notificationService.blockUiStop();
        if (!spe.erreur) {
          if (spe.data.length > 0) {
            this.dialogRef = this.dialog.open(FicheSpecialiteClientEditComponent, {
              width: '900px',
              height: '700px',
              data: {
                'client': client,
                'edit': edit,
                'id_fiche': id_fiche,
                'allspecilite': spe.data
              }

            });

            this.dialogRef.afterClosed().subscribe(
              (resp) => {
                if (resp) {
                  this.getNewData();
                }
              }
            );

          } else {
            this.notificationService.showNotificationEchec('erreur chargement formulaire veillez ressayer');
          }


        }

      }
    );


  }

  initData() {

    this.ficheSpeService.listFicheSpe = this.activatedRoute.snapshot.data['listeficheSpecialites']['data'];

    this.ficheSpeService.ficheSpeSubject.next(this.ficheSpeService.listFicheSpe);
    // this.dataSource = new MatTableDataSource(this.ficheSpeService.listFicheSpe);

    this.ficheSpeService.ficheSpeSubject.subscribe(
      (response: any) => {


        this.dataSource = new MatTableDataSource(response);
      }
    );
    this.length = this.activatedRoute.snapshot.data['listeficheSpecialites']['lenght'];


  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();

  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getNewData();
  }

  getNewData() {
    this.notificationService.blockUiStart();
    this.ficheSpeService.allFicheSpecialite(this.data).subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource(response['data']);
        this.length = response['lenght'];
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }
}
