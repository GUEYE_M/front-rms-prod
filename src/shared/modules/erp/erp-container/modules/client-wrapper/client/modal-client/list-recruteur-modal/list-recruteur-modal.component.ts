import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { map, tap } from 'rxjs/operators';
import { BehaviorSubject, from } from 'rxjs';
import { NewClientModalComponent } from '../new-client-modal/new-client-modal.component';
import { RecruteurService } from '../../../../../../../../services/recruteur/recruteur.service';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-list-recruteur-modal',
  templateUrl: './list-recruteur-modal.component.html',
  styleUrls: ['./list-recruteur-modal.component.css']
})
export class ListRecruteurModalComponent implements OnInit {

  displayedColumns = ['select', 'number', 'Fonction', 'email', 'civilite', 'nom', 'prenom', 'telephone'];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  length: number = 0;
  elements = [];
  data = {
    offset: 0,
    limit: 10,
    filter: null,
  };
  pageSize: number = 10;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  displayedColumnsFilter = [];
  @ViewChild('listPaginator', { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) public sort: MatSort;
  listsource: any[] = [];
  listBehavior: BehaviorSubject<any[]> = new BehaviorSubject(null);
  recrutecurtSelectedTab: any[];

  constructor(
    private dialogRef: MatDialogRef<ListRecruteurModalComponent>,
    @Inject(MAT_DIALOG_DATA) private recruteurlist: any, private recruteurService: RecruteurService, private notificationService: NotificationService,
    private dialog: MatDialog
  ) {

    this.initData();


  }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    // this.sort.sortChange.subscribe((e) => (e));

  }

  ngOnInit() {
    this.displayedColumnsFilter = this.recruteurService.displayedColumnsName();
  }

  filtrer(filtre: string) {
    filtre = filtre.trim();
    filtre = filtre.toLowerCase();
    this.dataSource.filter = filtre;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ouvrirNewRecruteurModal() {
    const dialogRef: MatDialogRef<NewClientModalComponent> = this.dialog.open(NewClientModalComponent, {
      width: '900px',
      height: '600px',


    });

    dialogRef.afterClosed().subscribe(
      (resultat) => {

      }
    );
  }


  initData() {



    this.dataSource = new MatTableDataSource(this.recruteurlist.items);
    this.dataSource.paginator = this.paginator
    this.length = this.recruteurlist.length;

  }

  custumlist(list: any) {
    from(list).pipe(
      map(
        (recruteur: any) => {
          return {
            id: recruteur.id,
            fonction: recruteur.fonction,
            email: recruteur.user.email,
            civilite: recruteur.user.civilite,
            nom: recruteur.user.nom,
            prenom: recruteur.user.prenom,
            telephone: recruteur.user.telephone

          };

        }),
      tap((recruteur) => {
        this.listsource.push(recruteur);
      })
    ).subscribe();



    this.dataSource = new MatTableDataSource(this.listsource);
    this.dataSource.paginator = this.paginator;
    //this.length = this.recruteurlist.length;

  }

  valutElements() {


    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.listsource = [];
      this.data.filter = null;
      this.initData();
    }
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre(form: NgForm) {

    this.data.filter = form.value;

    //(form.value);


    this.getNewData();

  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getNewData();


  }

  getNewData() {
    this.listsource = [];
    this.notificationService.blockUiStart();
    this.recruteurService.allRecruteur(this.data).subscribe(
      (response: any) => {


        this.custumlist(response.data);
        this.length = response.length;


      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }

}
