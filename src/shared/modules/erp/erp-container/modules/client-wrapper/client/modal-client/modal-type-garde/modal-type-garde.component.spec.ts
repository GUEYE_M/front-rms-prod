import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTypeGardeComponent } from './modal-type-garde.component';

describe('ModalTypeGardeComponent', () => {
  let component: ModalTypeGardeComponent;
  let fixture: ComponentFixture<ModalTypeGardeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTypeGardeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTypeGardeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
