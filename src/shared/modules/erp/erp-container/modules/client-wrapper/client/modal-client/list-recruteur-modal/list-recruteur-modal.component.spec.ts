import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRecruteurModalComponent } from './list-recruteur-modal.component';

describe('ListRecruteurModalComponent', () => {
  let component: ListRecruteurModalComponent;
  let fixture: ComponentFixture<ListRecruteurModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRecruteurModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRecruteurModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
