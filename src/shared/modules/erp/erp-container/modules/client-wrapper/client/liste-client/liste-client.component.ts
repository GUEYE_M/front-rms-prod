import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ɵConsole } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ClientsInactifsComponent } from '../clients-inactifs/clients-inactifs.component';
import { EditListeClientComponent } from '../edit-liste-client/edit-liste-client.component';
import { FromEmailComponent } from '../modal-client/from-email/from-email.component';
import { ModalMailFichePosteComponent } from '../modal-client/modal-mail-fiche-poste/modal-mail-fiche-poste.component';
import { GestionnaireInterneService } from '../../../../../../../services/gestionnaire interne/gestionnaireInterne.service';
import { NotificationService } from '../../../../../../../services/notification.service';
import { SendinblueService } from '../../../../../../../services/sendinblue.service';
import { FilterService } from '../../../../../../../services/filter.service';
import { RecruteurService } from '../../../../../../../services/recruteur/recruteur.service';
import { RolesService } from '../../../../../../../services/roles.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PldService } from '../../../../../../../services/pld.service';
import { UtilsService } from '../../../../../../../services/utils.service';
import { UserService } from '../../../../../../../services/user.service';
import { ClientService } from '../../../../../../../services/recruteur/client.service';
import { SelectionModel } from '@angular/cdk/collections';
import { Client } from '../../../../../../../models/recruteur/Client.model';
import { ListeRecruteurInterimComponent } from '../modal-client/liste-recruteur-interim/liste-recruteur-interim.component';
import { ListProspectionModalComponent } from '../../../prospection/list-prospection-modal/list-prospection-modal.component';
import { TransactionModalComponent } from '../../../sms/transaction/transaction-modal/transaction-modal.component';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { DatePipe } from '@angular/common';

@Component({
	selector: 'app-liste-client',
	templateUrl: './liste-client.component.html',
	styleUrls: [ './liste-client.component.css' ]
})
export class ListeClientComponent implements OnInit, OnDestroy {
	formFilter: FormGroup;
	clientlist: Client[];
	clientsPld = [];
	uri_listes_plannings = 'client/listes-filter';
	currentUser: any;
	dialogClientsInactifs: MatDialogRef<ClientsInactifsComponent>;
	dialogRef_form_email: MatDialogRef<FromEmailComponent>;
	dialogRef_sms_modal: MatDialogRef<TransactionModalComponent>;
	liste_recruteur = [];
	list_gestionnaire = [];
	showLoadClient: boolean;
	view: boolean = false;
	actionInput: Boolean = false;
	action: string;
	public form: FormGroup;
	dialogRef: MatDialogRef<EditListeClientComponent>;
	range: Range = { fromDate: new Date(), toDate: new Date() };
	displayedColumns = [
		'select',
		'number',
		'raison',
		'recruteur',
		'telephone',
		'email',
		'prospection',
		'date',
		'activer',
		'edit'
	];
	length: number = 0;

	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null,
		date_debut: null,
		date_fin: null,
	};
	pageSize = 12;
	
	pageSizeOptions: number[] = [ 12, 25, 50, 100 ];
	displayedColumnsFilter = [
		{
			label: "nom etablissement",
		    name:	"etablissement",
		},
		{
			label: "Telephone",
		    name:	"telephone",
		},
		{
			label: "Adresse e-mail",
		    name:	"email",
		},
		{
			label: "Actif",
		    name:	"actif",
		},
		{
			label: "Type",
		    name:	"type",
		},
		{
			label: "Ville",
		    name:	"ville",
		},
		{
			label: "Region",
		    name:	"region",
		},
		{
			label: "Departement",
		    name:	"departement",
		},
		{
			label: "Adresse",
		    name:	"adresse",
		},
		{
			label: "Date Enregistrement",
		    name:	"date",
		},
		{
			label: "Gestionnaire",
		    name:	"gestionnaire",
		},
		{
			label: "Cible",
		    name:	"cible",
		},
		
	];

	selection = new SelectionModel<any[]>(true, []);
	loader = 0.1;
	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];
	dataSource: any = new MatTableDataSource();

	listActions: any[] = [
		{ value: 1, viewValue: 'E-Mail Demande Fiche Poste' },
		{ value: 2, viewValue: 'Envoyer E-mail' },
		{ value: 5, viewValue: 'Envoyer SMS' },
		{ value: 3, viewValue: 'Desactiver' },
		{ value: 4, viewValue: 'Activer' }
		
	];
	clientPldToErp = false;
	limits = [
		{ value: 1, viewValue: '1 - 100' },
		{ value: 101, viewValue: '101 - 200' },
		{ value: 201, viewValue: '201 - 300' },
		{ value: 301, viewValue: '301 - 400' },
		{ value: 401, viewValue: '401 - 500' },
		{ value: 501, viewValue: '501 - 600' },
		{ value: 601, viewValue: '601 - 700' },
		{ value: 701, viewValue: '701 - 800' }
	];
	listsource: any[] = [];
	@ViewChild(MatSort, { static: true })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;

	constructor(
		private clientService: ClientService,
		private router: Router,
		private userService: UserService,
		private utilService: UtilsService,
		private pldService: PldService,
		private activatedRoute: ActivatedRoute,
		private formBuilder: FormBuilder,
		private dialog: MatDialog,
		public rolesService: RolesService,
		private recruteurService: RecruteurService,
		private filterService: FilterService,
		private sendinblueService: SendinblueService,
		private notifierService: NotificationService,
		private gestionnaireInterneService: GestionnaireInterneService,
		private datePipe: DatePipe,
	) {
		// this.dataSource.data =;
	}

	ngOnInit() {
		this.notifierService.blockUiStop();
		localStorage.setItem('email', 'nom');
		this.formFilter = this.formBuilder.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);

		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
				this.getNewData();
			}
		} else {
		}
		this.initData();

		if (this.roleSuperAdmin()) {
			this.listActions.push({
				value: 6,
				viewValue: 'Archiver'
			});
		}
		// -----------------faire la liaisons avec les clients sur pld ----------------
		// const numbers = 500;
		// let id = '';
		// for (let i = 0; i < numbers; i++) {
		//   if (i < 10) {
		//     id = '000000' + i;
		//   } else if (i < 100) {
		//     id = '00000' + i;
		//     id = '0000' + i;
		//   }
		//   this.pldService.findClient(id).subscribe((response) => {
		//     if (response) {
		//       this.clientsPld.push(response);
		//     }
		//   });
		// }

		// a activer uniquement dans le filtre
		// this.gestionnaireInterneService.list().subscribe(
		//   (response) => {
		//     if (response.length > 0) {
		//       this.list_gestionnaire = response;
		//     }
		//   }
		//
		// );
		this.dataSource.sort = this.sort;
		this.datepickerConfig()
	}

	voirClientsPld() {
		const test = [ '1', '2' ];
		this.utilService.updateIdClientPLD(this.clientsPld).subscribe((resp) => {
			resp;
		});
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	actionbtn(action: any, clientSelection: any) {
		let item = {
			etat: action,
			id: []
		};
		clientSelection.map((recruteur) => {
			if (action === 3 || action === 4) {
				this.notifierService.blockUiStart();
				clientSelection.map((statut: any) => {
					item.id.push(statut.id);
				});

				if (item.id.length > 0) {
					this.clientService.updateEnable(item).subscribe(
						(response: any) => {
							if (response.erreur) {
								this.notifierService.showNotificationEchec(response.erreur);
								this.notifierService.blockUiStop();
							} else {
								this.dataSource.data.map((data: any) => {
									clientSelection.map((el) => {
										if (el.id === data.id) {
											if (action === 3) {
												data.activer = false;
											} else {
												data.activer = true;
											}
										}
									});
								});
							}
							this.notifierService.showNotificationSuccess(response.success);
							this.notifierService.blockUiStop();
						},
						(erreur) => {
							this.notifierService.showNotificationEchec(
								'erreur chargement veillez ressayer , consilter le service IT si le probleme persiste'
							);
							this.notifierService.blockUiStop();
						},
						() => {}
					);
				}
			}
		});

		if (action === 1) {
			let dialogRef: MatDialogRef<ModalMailFichePosteComponent>;

			dialogRef = this.dialog.open(ModalMailFichePosteComponent, {
				width: '80%',
				height: '470px',
				panelClass: 'myapp-no-padding-dialog'
			});
			dialogRef.afterClosed().subscribe((response) => {
				if (response) {
					this.notifierService.blockUiStart();
					let data = {
						clent: clientSelection,
						specialite: response
					};

					this.sendinblueService.sendEmailichePoste(data).subscribe(
						(response: any) => {
							if (response.erreur) {
								this.notifierService.showNotificationEchec(response.erreur);
								this.notifierService.blockUiStop();
							} else {
								this.notifierService.showNotificationSuccess(response.success);
							}
						},
						(erreur) => {
							this.notifierService.showNotificationEchec(
								'erreur chargement veillez ressayer , consilter le service IT si le probleme persiste'
							);
							this.notifierService.blockUiStop();
						},
						() => {
							this.notifierService.blockUiStop();
						}
					);
				}
			});
		}

		if (action === 2) {
			let data = {
				type: 'mailDiffusion',
				selection: clientSelection,
				client: true
			};

			this.dialogRef_form_email = this.dialog.open(FromEmailComponent, {
				width: '80%',
				height: '470px',
				data: data,
				panelClass: 'myapp-no-padding-dialog'
			});
		}

		if (action === 5) {
			let data = {
				selection: clientSelection,
				client: true
			};

			this.dialogRef_sms_modal = this.dialog.open(TransactionModalComponent, {
				width: '80%',
				height: '470px',
				data: data,
				panelClass: 'myapp-no-padding-dialog'
			});
		}

		if (action === 6) {
		
			let data = {
				clients:[]
			}

			clientSelection.map((i: any) => {
				data.clients.push(i.id);
			});

			this.notifierService.onConfirm("Êtes-vous sûre d'effectuer cette action ?");
			this.notifierService.dialogRef.afterClosed().subscribe((response) => {
				if (response) {
					this.notifierService.blockUiStart();
					this.clientService.archiverClient(data).subscribe((response: any) => {
						if (response.erreur) {
							this.notifierService.showNotificationEchec(response.erreur);
						} else {
							this.notifierService.showNotificationSuccess('les données ont été bien archivé');
						}

						this.getNewData();

						this.notifierService.blockUiStop();
					});
				}
			});
		}

		this.selection.clear();
	}

	editClientModal(id = null) {
		let data = {
			profil: false,
			id: id
		};
		this.dialogRef = this.dialog.open(EditListeClientComponent, {
			width: '80%',
			height: '470px',
			panelClass: 'myapp-no-padding-dialog',
			data: data
		});

		this.dialogRef.afterClosed().subscribe((response) => {
			if (response) {
				if (!response.profil) {
					this.getNewData();
				}
			}
		});
	}

	showClient(id) {}

	getClientNotGetionnaire() {}

	ngOnDestroy() {
		this.clientService.showLoadClient.next(true);
	}

	onActiveOuDesactiveUser(index_client, client, val) {
		this.loader = client.id;
		this.clientService.enableClient(client.id, val).subscribe((next) => {
			if (this.dataSource.data[index_client]) {
				this.utilService.redirectEspace({ espace: 'clientListe', id: '' });
				this.loader = 0.1;
			}
			if (val === true) {
				this.notifierService.showNotificationSuccessCopie('topCenter', 'le client a été activé !');
			} else {
				this.notifierService.showNotificationSuccessCopie('topCenter', 'le client a été desactivé !');
			}
			// this.router.navigate(['/RMS-Admin/redirection-admin/', 'client/list']);
		});
		// action = JSON.parse(action);
		// if (utilisateur.user.enabled !== action) {
		//     this.idItemUpdate = 0;
		//     this.keyItemUpdate = '';
		//     const afficherAlertSuccees: HTMLElement = document.getElementsByName('afficherAlertSuccees')[0] as HTMLElement;
		//     const afficherAlertEchec: HTMLElement = document.getElementsByName('afficherAlertEchec')[0] as HTMLElement;
		//     utilisateur.user.enabled = !utilisateur.user.enabled;
		//     this.idUtilisateur = utilisateur.idCurrent;
		//     this.loader = utilisateur.idCurrent;
		//     this.userService.activerOuDesactive(utilisateur).subscribe((response) => {
		//         this.loader = 0;
		//         if (!response['erreur'])
		//             this.notificationService.showNotificationSuccess(response['success']);
		//         else
		//             this.notificationService.showNotificationEchec(response['erreur']);
		//     });
		// }
	}

	onListeRecruteur(recruteurs): void {
		const dialogRef = this.dialog.open(ListeRecruteurInterimComponent, {
			width: '650px',
			height: '350px',
			panelClass: 'myapp-no-padding-dialog',
			data: { recruteur: recruteurs }
		});
	}

	listClientsInactifsModal() {
		this.dialogClientsInactifs = this.dialog.open(ClientsInactifsComponent, {
			width: '1000px',
			height: '550px',
			panelClass: 'myapp-no-padding-dialog'
		});
	}

	roleRecruteur() {
		return this.rolesService.isRecruteurUnique(this.currentUser.roles);
	}

	roleCommercial() {
		return this.rolesService.isCommercial(this.currentUser.roles);
	}
	roleSuperAdmin() {
		return this.rolesService.isCommercial(this.currentUser.roles);
	}

	initData() {
		this.currentUser = this.userService.getCurrentUser();
		const listeClients = this.activatedRoute.snapshot.data['listClients']['data'];

		if (this.roleCommercial()) {
			this.length = this.activatedRoute.snapshot.data['listClients']['lenght'];
			this.dataSource.data = listeClients;
			this.dataSource.sort = this.sort;
		}

		// this.clientService.clientSubject.next(listeClients);
	}

	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.initData();
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
		}
	}

	// fonction qui envoie le formulaire de filtre en base de donnee
	onSubmitFiltre() {
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = this.formFilter.value;
		this.getNewData();
	}

	pagination(event: any) {
		this.pageSize = event.pageSize;
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = this.pageSize;

		this.getNewData();
	}

	getNewData() {
		this.notifierService.blockUiStart();
		this.clientService.listClients(this.data).subscribe(
			(response: any) => {
				this.dataSource = new MatTableDataSource(response['data']);
				this.length = response['lenght'];
				this.dataSource.sort = this.sort;
			},
			(erreur) => {
				this.notifierService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
				this.notifierService.blockUiStop();
			},
			() => {
				this.notifierService.blockUiStop();
			}
		);
	}

	messageProspection(id, index: number) {
		let client: any = this.dataSource.data[index];

		const data = { id: id, modal: true, interim: false, recruteur: client.recruteurs };
		client.recruteurs;
		const dialogRef_mssage: MatDialogRef<
			ListProspectionModalComponent
		> = this.dialog.open(ListProspectionModalComponent, {
			width: '900px',
			height: '400px',
			data: data,
			panelClass: 'myapp-no-padding-dialog'
		});

		dialogRef_mssage.afterClosed().subscribe((el) => {
			el;
		});
	}

	onLiePldToERp(form: NgForm) {
		this.notifierService.blockUiStart();
		this.pldService.idClientPldToErp(form.value.offset).subscribe(
			(response) => {
				this.notifierService.blockUiStop();
			},
			(error) => {
				this.notifierService.blockUiStop();
			}
		);
	}
	
	updateRange(range: Range) {
	
		this.range = range;

			this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd') + ' 00:00:00';
			this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd') + ' 23:59:59';
		console.log(this.data)
	}

	
	datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
		};
	}

	setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
	}

}
