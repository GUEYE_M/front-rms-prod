import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListesPlanningsComponent } from './listes-plannings.component';

describe('ListesPlanningsComponent', () => {
  let component: ListesPlanningsComponent;
  let fixture: ComponentFixture<ListesPlanningsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListesPlanningsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListesPlanningsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
