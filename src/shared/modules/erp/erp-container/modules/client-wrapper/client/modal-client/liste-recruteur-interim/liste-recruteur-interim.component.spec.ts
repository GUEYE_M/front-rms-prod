import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeRecruteurInterimComponent } from './liste-recruteur-interim.component';

describe('ListeRecruteurInterimComponent', () => {
  let component: ListeRecruteurInterimComponent;
  let fixture: ComponentFixture<ListeRecruteurInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeRecruteurInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeRecruteurInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
