import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterimModalComponent } from './interim-modal.component';

describe('InterimModalComponent', () => {
  let component: InterimModalComponent;
  let fixture: ComponentFixture<InterimModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterimModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterimModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
