import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../../../../../../../services/notification.service';
import {UserService} from '../../../../../../../../../../services/user.service';
import {ClientService} from '../../../../../../../../../../services/recruteur/client.service';
import {MissionService} from '../../../../../../../../../../services/mission.service';
import {PlanningService} from '../../../../../../../../../../services/recruteur/planning.service';
import {TypeDeGardeService} from '../../../../../../../../../../services/recruteur/type-de-garde.service';
import * as moment from 'moment';
import {RolesService} from '../../../../../../../../../../services/roles.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-planning-by-date',
  templateUrl: './create-planning-by-date.component.html',
  styleUrls: ['./create-planning-by-date.component.css']
})
export class CreatePlanningByDateComponent implements OnInit, OnDestroy {
  formArrayPicker: FormGroup;
  qualifsData = [];


  currentUser: any;
  itemGardeChoisie: any;
  @Input() public initialCountCreate;
  @Input() public listesVacations;
  @Input() public typeDeGardeDefaut;
  @Input() public showAjout;
  @Input() public ficheSpecialiteDefaut;
  @Input() public ficheSpecialiteDefautChoix;
  @Input() public typeDeGardes;
  @Input() public ficheSpecialites;
  activerMission = false;
  majorer = false;
  statuts = [
    {statut: 'A Pourvoir'},
    {statut: 'Non Pourvue'},
    {statut: 'Perdue'},
    {statut: 'Autre'},
  ];
  loaderValider = false;
  fiche_specialite_idChoisie: number;
  ficheSpecialiteChoisie: any;
  id_client = 0;
  jtc: any = ' ';
  showAfterJtc = false;
  jtc_array = [
    {label: 'Jour travaillé', value: 'Jt'},
    {label: 'Heures travaillée', value: 'Ht'},
  ];
  month: any;
  monthNames = [
    'Janvier', 'Fevrier', 'Mars',
    'Avril', 'Mai', 'Juin', 'Juillet',
    'Août', 'Septembre', 'Octombre',
    'Novembre', 'Decembre'
  ];

  constructor(private typeGardeService: TypeDeGardeService,
              private planningService: PlanningService,
              private missionService: MissionService,
              private clientService: ClientService,
              private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService,
              public roleService: RolesService,
              private notificationService: NotificationService,
              private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.initFormData();
    this.currentUser = this.userService.getCurrentUser();
    moment.locale('fr-Fr');
    this.month = moment();
    this.id_client = this.activatedRoute.snapshot.params.id;
  }

  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }

  VerifieDateChoisieAfinDadapterlesAjoutDeVacations() {
    const data = [];
    if (this.initialCountCreate.length === 0) {
      this.ficheSpecialiteDefaut = [];
      this.ficheSpecialiteDefautChoix = [];
    }
    if (this.initialCountCreate.length > 0) {
      this.showAjout = false;
    } else {
      this.showAjout = true;
    }
    this.addGroupData(this.initialCountCreate);
  }

  onValider() {
    this.notificationService.blockUI.start();
    this.planningService.create(this.onValideFormData()).subscribe(
      (response: any) => {
        // on recupere l'ensemble des mission du client
        const dataFilter = {
          offset: 0,
          limit: 5,
          filter: null,
          id_client: this.clientService.idClient.getValue()
        };
        this.clientService.getAllMissionByClient(dataFilter).subscribe(
          (data_response: any) => {
            const listeMisions = data_response.data;
            this.clientService.allmissionsForClientSubject.next(data_response);
            this.planningService.title = 'Gestion des Missions';
            this.planningService.onResetPlanning();
            this.notificationService.blockUI.stop();
          });
        if (response.body.erreur) {
          let msg = response.body.erreur;
          this.notificationService.showNotificationEchecCopie('topCenter', msg);
        } else {
          let msg = 'La mission a été enregistrée avec success !';
          this.notificationService.showNotificationSuccessCopie('topCenter', msg);
        }
      },
      (error) => {
        const dataFilter = {
          offset: 0,
          limit: 5,
          filter: null,
          id_client: this.clientService.idClient.getValue()
        };
        this.clientService.getAllMissionByClient(dataFilter).subscribe(
          (data_response: any) => {
            const listeMisions = data_response.data;
            this.clientService.allmissionsForClientSubject.next(data_response);
            this.planningService.title = 'Gestion des Missions';
            this.planningService.onResetPlanning();
            this.notificationService.blockUI.stop();
          });
      }
    );
  }

  showJtc() {
    this.showAfterJtc = true;
    this.ficheSpecialiteChoisie = [];
    this.ficheSpecialiteDefaut = [];
    this.typeDeGardeDefaut = [];
    this.ficheSpecialiteDefautChoix = [];
  }

  choiceFicheSpecialite(index) {
    this.ficheSpecialiteChoisie = this.ficheSpecialites[index];
    this.typeDeGardeDefaut = [];
    this.ficheSpecialiteDefautChoix = [];
    this.ficheSpecialiteDefautChoix = this.ficheSpecialiteChoisie.typeGardes.filter((x) => {
      if (x.type_de_garde.toLowerCase().includes(this.jtc.toLowerCase()) && (x.annee === this.initialCountCreate[0].format('YYYY'))) {
        return x;
      }
    });
    this.ficheSpecialiteDefautChoix = this.ficheSpecialiteDefautChoix.map((x) => {
      x.qualifs = this.ficheSpecialiteChoisie.qualifications;
      return x;
    });
  }

  ngOnDestroy() {
    this.listesVacations = [];
  }

  // test form array
  initFormData() {
    this.formArrayPicker = this.formBuilder.group({
      groupData: this.formBuilder.array([])
    });
  }

  addGroupData(dates) {
    const groupData = this.formArrayPicker.controls.groupData as FormArray;
    // on recupere les valeurs du formulaire dans un tableau simple
    const formValues = this.formArrayPicker.value.groupData.map((x) => {
      if (x.date) {
        return x.date;
      } else {
        return [];
      }
    });
    // on verifie les donnee du formulaires nexiste plus dans le tableau on les supprime
    if (formValues.length > 0) {
      formValues.forEach((x) => {
        if (dates.indexOf(x) === -1) {
          groupData.controls.splice(formValues.indexOf(x), 1);
          this.formArrayPicker.value.groupData.splice(formValues.indexOf(x), 1);
        }
      });
    }
    // si le tableau contient de donnee on verifie sil existe pas dans le formulaire
    if (dates.length > 0) {
      dates.forEach((x) => {
        if (formValues.indexOf(x) === -1) {
          groupData.push(this.formBuilder.group({
            date: [x],
            occurences: this.formBuilder.array([])
          }));
        }
      });
    }
  }

  getOccurences(index) {
    const occurences = this.formArrayPicker.get(`groupData.${index}.occurences`) as FormArray;
    return occurences.controls;
  }

  addOccurence(index) {
    const occurences = this.formArrayPicker.get(`groupData.${index}.occurences`) as FormArray;
    occurences.push(this.formBuilder.group({
      vacation: [''],
      tarif: [''],
      salaire: [''],
      qualif: ['', Validators.required]
    }));
  }

  // conformiter des donnees aves la fonction existante
  onValideFormData() {
    const data = this.formArrayPicker.value.groupData;
    const tmp = [];
    data.forEach((x) => {
      x.occurences.forEach((y) => {
        const typeGardes = {
          heure_debut: y.vacation.heure_debut,
          heure_fin: y.vacation.heure_fin,
          salaire_net: y.salaire,
          tarif: y.tarif,
          qualif: y.qualif,
          statut: 'A Pourvoir',
          typeDeGarde_id: y.vacation.typeDeGarde_id,
          type_de_garde: y.vacation.type_de_garde
        };
        const tmpData = {
          typeGardes: null,
          mois_lettre: x.date.format('MMMM'),
          datePlan: x.date.format('YYYY-MM-DD'),
          fiche_specialite_id: this.ficheSpecialiteChoisie.fiche_specialite_id,
          nomSpecialite: this.ficheSpecialiteChoisie.nomspecialite,
          nomSpecialiteSplit: this.ficheSpecialiteChoisie.nomspecialite,
          enable: false,
          majorer: false,
          auteur: this.userService.getCurrentUser().username,
          jtc: this.jtc,
        };
        tmpData.typeGardes = typeGardes;
        tmp.push(tmpData);
      });
    });
    return tmp;
  }

  setDataOnForm(indexGroupData, indexOcurrence) {
    const occurence = this.getOccurences(indexGroupData)[indexOcurrence];
    const indexTmp = indexGroupData + indexOcurrence;
    this.qualifsData[indexTmp] = occurence.value.vacation.qualifs;
    occurence.patchValue({
      tarif: occurence.value.vacation.tarif,
      salaire: occurence.value.vacation.salaire_net,
    });
  }

  delOccurence(indexGroupData, indexOcurrence) {
    const occurence = this.formArrayPicker.get(`groupData.${indexGroupData}.occurences`) as FormArray;
    occurence.removeAt(indexOcurrence);
  }

//  fin de test
}
