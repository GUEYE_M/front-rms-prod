import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromEmailComponent } from './from-email.component';

describe('FromEmailComponent', () => {
  let component: FromEmailComponent;
  let fixture: ComponentFixture<FromEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
