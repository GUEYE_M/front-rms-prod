import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MissionService} from '../../../../../../../../services/mission.service';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {SelectionModel} from "@angular/cdk/collections";
import {RolesService} from "../../../../../../../../services/roles.service";
import {UserService} from "../../../../../../../../services/user.service";
import {ActivatedRoute} from "@angular/router";
import {CandidatureService} from "../../../../../../../../services/candidature.service";

@Component({
  selector: 'app-modal-maching-dispo',
  templateUrl: './modal-maching-dispo.component.html',
  styleUrls: ['./modal-maching-dispo.component.css']
})
export class ModalMachingDispoComponent implements OnInit {
  interims = [];
  selectionMatching = new SelectionModel<any[]>(true, []);
  @ViewChild(MatSort, {static: false}) sortMatching: MatSort;
  @ViewChild('listPaginator', {static: false}) paginatorMatching: MatPaginator;
  displayedColumns_interimaire_disponibile = ['select', 'date', 'libelle', 'debut', 'fin', 'salaire', 'etablissement', 'departement'];
  dataSource_interims_disponibile: MatTableDataSource<any[]> = new MatTableDataSource([]);
  date: any;
  adminOrInterim = 'admin';
  conditionMedecin = '';
  interim_id: any;
  load = true;
  listsource = [];
  isMissionEnabled = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private missionService: MissionService,
              public rolesService: RolesService,
              private activatedRoute: ActivatedRoute,
              private candidatureService: CandidatureService,
              public userService: UserService,
              private notificationService: NotificationService) {
    if (data) {
      let dateFormatFr = [
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
      ];
      let moisString: any = (dateFormatFr.lastIndexOf(data.mois) + 1);
      moisString = (moisString.toString().length > 1 ? moisString : 0 + '' + moisString) + '-' + data.annee;
      let contentSearch = {
        moisChiffre: moisString,
        mois: data.mois,
        annee: data.annee,
        nomSpecialite: data.nomSpecialite,
        missionId: data.mission_id
      };
      this.missionService.getMissionMachingByDispo(contentSearch).subscribe(
        (response) => {
          this.load = false;
          this.notificationService.blockUiStart();
          const interimCopie = [];
          if(response.data.length>0){
            response.data.forEach((element) => {
              const dataTmp = [];
              if(element.disponibilites){
                element.disponibilites.forEach((x) => {
                  element.plannings.filter((y) => {
                    if (x.date === y.date) {
                      dataTmp.push(y);
                    }
                  });
                });
                if(dataTmp.length > 0){
                  interimCopie.push(element);
                }
              }
            });
          }
          this.interims = interimCopie;
          this.notificationService.blockUiStop();
        },
        (erreur) => {
          this.notificationService.showNotificationEchec('erreur chargement!! veillez ressayer');
          this.notificationService.blockUiStop();
        },

        () => {
          this.notificationService.blockUiStop();
        }
      );
    }
  }

  ngOnInit() {

  }

  showCandidatures(interim) {
    this.clearSelectionMatching();
    this.interim_id = interim.interim;
    this.dataSource_interims_disponibile.data = [];
    const dataTmp = [];
    interim.disponibilites.forEach((x) => {
      interim.plannings.filter((y) => {
        if (x.date === y.date) {
          dataTmp.push(y);
        }
      });
    });
    this.dataSource_interims_disponibile.data = dataTmp;
    this.dataSource_interims_disponibile.paginator = this.paginatorMatching;
  }

  masterToggleMatching() {
    this.isAllSelectedMatching() ?
      this.clearSelectionMatching() :
      this.dataSource_interims_disponibile.data.forEach(
        row => this.selectionMatching.select(row));
  }

  isAllSelectedMatching() {
    const numSelected = this.selectionMatching.selected.length;
    const numRows = this.dataSource_interims_disponibile.data.length;
    return numSelected === numRows;
  }

  clearSelectionMatching() {
    this.selectionMatching = new SelectionModel<any[]>(true, []);
    // this.dataSource_interims_disponibile.data = [];
  }

  onPostuler() {
    if (this.selectionMatching.selected.length === 0) {
      this.clearSelectionMatching();
      this.notificationService.showNotificationEchecCopie('topCenter', 'Au moins une date est necessaire pour postuler !');
    } else {
      this.notificationService.blockUiStart();
      let parcours = 0;
      this.selectionMatching.selected.forEach(element => {
        this.selectionMatching.selected[parcours]['id_interim'] = this.interim_id;
        this.selectionMatching.selected[parcours]['condition_medecin'] = this.conditionMedecin;
        parcours = parcours + 1;
      });
      this.candidatureService.create_candidature_medecin_matching(JSON.stringify(this.selectionMatching.selected)).subscribe(
        (next: any) => {
          this.clearSelectionMatching();
          if (next.erreur) {
            this.notificationService.showNotificationEchec(next.erreur);
          } else {
            this.notificationService.showNotificationSuccessCopie('topCenter', next.success);
          }
          this.notificationService.blockUiStop();

        }, (error) => {
          this.notificationService.blockUiStop();
        }
      );
    }
  }


}
