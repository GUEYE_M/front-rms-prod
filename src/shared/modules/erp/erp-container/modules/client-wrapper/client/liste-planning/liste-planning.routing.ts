import {Route, RouterModule} from '@angular/router';
import {ListesPlanningsComponent} from './listes-plannings/listes-plannings.component';
import {ListesPlanningResolve} from '../../../../../../../resolver/listesPlanning.resolve';


const ROUTE_CONFIG: Route[] = [
  {
    path: 'liste',
    component: ListesPlanningsComponent,
    resolve: { listesPlannings: ListesPlanningResolve}
  },
];

export const ListePlanningRouting = RouterModule.forChild(ROUTE_CONFIG);
