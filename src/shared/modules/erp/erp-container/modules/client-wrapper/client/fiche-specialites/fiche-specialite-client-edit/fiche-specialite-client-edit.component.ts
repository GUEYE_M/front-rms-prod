import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {retry} from 'rxjs/operators';
import {FicheSpeciliate} from '../../../../../../../../models/recruteur/FicheSpeciliate.model';
import {Qualification} from '../../../../../../../../models/Qualification.model';
import {Specialite} from '../../../../../../../../models/Specialite.model';
import {Client} from '../../../../../../../../models/recruteur/Client.model';
import {FicheSpecialiteService} from '../../../../../../../../services/recruteur/fiche-specialite.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RolesService} from '../../../../../../../../services/roles.service';
import {UserService} from '../../../../../../../../services/user.service';
import {SpecialiteService} from '../../../../../../../../services/specialite.service';
import {ClientService} from '../../../../../../../../services/recruteur/client.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NotificationService} from '../../../../../../../../services/notification.service';

@Component({
  selector: 'app-fiche-specialite-client-edit',
  templateUrl: './fiche-specialite-client-edit.component.html',
  styleUrls: ['./fiche-specialite-client-edit.component.css']
})
export class FicheSpecialiteClientEditComponent implements OnInit, OnDestroy {

  FicheSpeForm: FormGroup;
  edit: boolean = false;
  ficheSpe: FicheSpeciliate;
  qualifSpecialite: Qualification[];
  listSpecialites: Specialite[] = [];
  clientList: Client[] = [];
  titre: string = '';
  currentUser: any;
  nom_etablissement: string = '';
  idFiche: number;
  specialite: boolean = false;
  hide_input_client = false;
  view_form: boolean = false;
  public content: string;

  constructor(private fb: FormBuilder, private ficheSpeService: FicheSpecialiteService,
              private route: ActivatedRoute, private router: Router,
              private roleService: RolesService,
              private userService: UserService,
              private specialiteService: SpecialiteService,
              private clientService: ClientService, public dialogRef: MatDialogRef<FicheSpecialiteClientEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private notificationService: NotificationService) {
    if (data.edit) {
      //this.notificationService.blockUiStart();
      this.idFiche = data.id_fiche;
    }
    if (this.data.client.id) {
      this.hide_input_client = true;
      //this.allSpecialites();
      //this.idFiche = null;
      //this.initdataform();
      //this.onClient(this.data.client.id);
    }
    this.listSpecialites = this.data.allspecilite;
  }

  ngOnInit() {
    // this.allSpecialites();
    this.allClient();
    this.currentUser = this.userService.getCurrentUser();
    //this.idFiche = this.route.snapshot.params['id'];
    this.initdataform();
  }

  initdataform() {

    if (this.idFiche) {

      this.notificationService.blockUiStart();
      this.edit = true;
      this.titre = 'Modification';

      this.ficheSpeService.find(this.idFiche).pipe(
        retry(3)
      ).subscribe(
        (response:any) => {
          if (response) {
            this.iniform(response);
            this.FicheSpeForm.patchValue(
              {
                // client : this.ficheSpe.client,
                specialite: response.qualifications[0].specialite.nomspecialite,
                qualifications: response.qualifications
              }
            );
            (this.FicheSpeForm.value);
            this.onSpecialite();

          }
          this.ficheSpe = response;


        },
        (error) => {
          this.notificationService.showNotificationEchec('erreur server, veillez ressayer');
          this.notificationService.blockUiStop();
          this.ficheSpeService.closemodale.next(false);
        },
        () => {

          this.notificationService.blockUiStop();

        }
      );


      //this.onclient(this.ficheSpe.client);


    } else {
      this.iniform();
    }
  }

  iniform(ficheSpe = {
    diplome: '', info_pratique: '', profil_rechercher: '', nombrepassage_par_an: null,
    nombre_lit: null, nombre_medecin_nuit: null, nombre_medecin_jour: null, coef_tarification: null,
    coef_saison: null, environemen_tech: null, qualifications: []
  }) {
    this.FicheSpeForm = this.fb.group(
      {
        specialite: [Validators.required],
        qualifications: [Validators.required],
        client: [Validators.required],
        diplome: [ficheSpe.diplome],
        info_pratique: [ficheSpe.info_pratique],
        profil_rechercher: [ficheSpe.profil_rechercher,Validators.required],
        nombrepassage_par_an: [ficheSpe.nombrepassage_par_an],
        nombre_lit: [ficheSpe.nombre_lit],
        nombre_medecin_nuit: [ficheSpe.nombre_medecin_nuit],
        nombre_medecin_jour: [ficheSpe.nombre_medecin_jour],
        coef_tarification: [ficheSpe.coef_tarification],
        coef_saison: [ficheSpe.coef_saison],
        environemen_tech: [ficheSpe.environemen_tech],

      }
    );

  }


  allClient() {


    this.clientService.listClients(null, true).subscribe(
      (client: any) => {
        this.clientList = client.data;
      }
    );


  }

  onClient(id: number) {

    // this.ficheSpeService.ficheSpecialiteByClient(id).unsubscribe();
    this.ficheSpeService.ficheSpeByClientSubject.subscribe(
      (response: any) => {

        response.forEach(
          (item) => {
            if (this.listSpecialites) {
              this.listSpecialites.forEach(
                (specialite, index) => {
                  if (specialite.nomspecialite == item.specialite) {
                    this.listSpecialites.splice(index, 1);
                  }
                }
              );
            }

          }
        );

      }
    );


    this.specialite = true;
  }

  onSpecialite() {
    const specialite = this.FicheSpeForm.get('specialite').value;
    (this.listSpecialites);
    if (this.listSpecialites) {
      if (this.listSpecialites.length > 0) {
        this.listSpecialites.filter((item) => {

          if (item.nomspecialite == specialite) {
            (this.qualifSpecialite);

            this.qualifSpecialite = item.qualifications;
          }
        });
      }

    }


  }


  compareWithFunc(a, b) {

    return a.qualification === b.qualification;
  }


  saveFicheSpe() {


    this.notificationService.blockUiStart();
    if (this.FicheSpeForm.valid) {

      if (this.edit) {
        if (this.data.client.id) {
          this.FicheSpeForm.get('client').setValue(this.data.client);
        } else {
          this.FicheSpeForm.get('client').setValue(this.ficheSpe.client);
        }


        this.ficheSpeService.update(this.idFiche, this.FicheSpeForm.value).subscribe(
          (response: any) => {

            if (response.erreur) {
              this.ficheSpeService.closemodale.next(false);
              this.notificationService.showNotificationEchec(response.erreur);

            } else {
             
              this.dialogRef.close(true);
              this.notificationService.showNotificationSuccess(response.success);

            }
          },
          (error) => {
            //   this.flashMessageService.flashMessage(error.message);
          },
          () => {
            this.notificationService.blockUiStop();
            // this.flashMessageService.flashMessage("Modification reussit avec success");
          }
        );
      } else {

        if (this.data.client.id) {
          this.FicheSpeForm.get('client').setValue(this.data.client);
        }
        this.ficheSpeService.create(this.FicheSpeForm.value).subscribe(
          (response: any) => {
            // (response.erreur);

            if (response.erreur) {

              this.ficheSpeService.closemodale.next(false);
              this.notificationService.showNotificationEchec(response.erreur);
              this.notificationService.blockUiStop();
            } else {

              if (this.data.client.id) {


                this.dialogRef.close(true);

              } else {
                this.ficheSpeService.listFicheSpe.push(response.data);

                this.ficheSpeService.emitsFicheSpecialite();
                (this.ficheSpeService.listFicheSpe);
              }
              this.ficheSpeService.closemodale.next(true);
              this.notificationService.showNotificationSuccess(response.success);
              this.notificationService.blockUiStop();
            }

          }, (error) => {

            // this.flashMessage(error.message)
          },
          () => {
            this.ficheSpeService.allFicheSpecialite();
            this.notificationService.blockUiStop();
          }
        );
      }
    } else {
      this.notificationService.blockUiStop();
      this.notificationService.showNotificationEchec('formulaire invalide, veillez verifier les données saisies');
    }
  }

  ngOnDestroy() {
    //this.ficheSpeService.closemodale.unsubscribe();
    this.notificationService.blockUiStop();
  }

  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }

}
