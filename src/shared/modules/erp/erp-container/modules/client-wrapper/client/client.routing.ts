import {Route, RouterModule} from '@angular/router';
import {ListeClientComponent} from './liste-client/liste-client.component';
import {EditListeClientComponent} from './edit-liste-client/edit-liste-client.component';
import {ClientResolveService} from '../../../../../../resolver/ClientResolve.resolve';
import {SingleClientComponent} from './single-client-container/single-client/single-client.component';
import {ClientWrapperComponent} from '../client-wrapper/client-wrapper.component';
import {ClientSingleResolve} from '../../../../../../resolver/ClientSingle.resolve';
import {EditProspectionComponent} from './single-client-container/prospection-interim/edit-prospection/edit-prospection.component';
import {InterimModalComponent} from './single-client-container/contrat-client/interim-modal/interim-modal.component';
import {CandidatureModalComponent} from './single-client-container/contrat-client/candidature-modal/candidature-modal.component';

const CLIENT_CONFIG: Route[] = [
  {
    path: '',
    component: ClientWrapperComponent,
    children: [
      {
        path: 'liste',
        component: ListeClientComponent,
        resolve: {listClients: ClientResolveService},
      },
      {
        path: 'edit',
        component: EditListeClientComponent,
      },
      {
        path: ':id/show',
        component: SingleClientComponent,
        resolve: {client: ClientSingleResolve}
      },
      {
        path: 'editModalProspection',
        component: EditProspectionComponent,
      },
      {
        path: 'showModalInterimForMission',
        component: InterimModalComponent,
      },
      {
        path: 'showModalCandidatureForMission',
        component: CandidatureModalComponent,
      }
    ]
  }
];
export const ClientRouting = RouterModule.forChild(CLIENT_CONFIG);
