import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditListeClientComponent } from './edit-liste-client.component';

describe('EditListeClientComponent', () => {
  let component: EditListeClientComponent;
  let fixture: ComponentFixture<EditListeClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditListeClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditListeClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
