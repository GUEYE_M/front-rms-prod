import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import * as moment from 'moment';
import {map, tap} from 'rxjs/operators';
import {BehaviorSubject, from} from 'rxjs';
import {SpecialiteService} from '../../../../../../../../services/specialite.service';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {FicheSpecialiteService} from '../../../../../../../../services/recruteur/fiche-specialite.service';
import {UtilsService} from '../../../../../../../../services/utils.service';
import {ClientService} from '../../../../../../../../services/recruteur/client.service';
import {TypeDeGardeService} from '../../../../../../../../services/recruteur/type-de-garde.service';
import {ActivatedRoute} from '@angular/router';
import {PlanningService} from '../../../../../../../../services/recruteur/planning.service';
import {MissionService} from '../../../../../../../../services/mission.service';
import {SelectAutocompleteComponent} from 'select-autocomplete';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit, OnDestroy {

  @ViewChild('idRep', {read: ElementRef, static: false}) idRef: ElementRef;
  displayedColumns: string[] = ['number', 'reference', 'mois', 'annee', 'specialite', 'etat', 'Show'];
  annees_moment = [
    2019, 2020, 2021, 2022, 2023, 2024, 2025
  ];
  selected: [];
  monthNames = [
    'Janvier', 'Février', 'Mars',
    'Avril', 'Mai', 'Juin', 'Juillet',
    'Août', 'Septembre', 'Octobre',
    'Novembre', 'Décembre'
  ];
  return_ = false;
  id_client: any;
  showAlertErp?: any = [];
  missionsErp?: MatTableDataSource<any[]> = new MatTableDataSource([]);
  @Input() fichespecialites?: any = [];
  planningsErp: any = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  typeDeGardes_default: any;
  hiddenCreateErp = true;
  missionChoisieErp: any = [];
  public planningsTmpErp: any = [];
  public planningsTmpErpCopie: any = [];
  highlightDaysErp: any = [];
  client: any;
  arrayForAddNewVacationErp = [];

  month?: any;
  year?: any;
  displayedColumnsFilter = [
    {
      name: 'reference',
      label: 'Reference'
    },
    {
      name: 'mois',
      label: 'Mois',
    },
    {
      name: 'annee',
      label: 'Annee',
    },
    {
      name: 'specialite',
      label: 'Specialite',
    },
  ];

  pageSize = 10;

  @Input() length: number = 0;

  elements = [];
  data = {
    offset: 0,
    limit: 10,
    filter: null,
    id_client: null

  };
  listSpecialites = [];
  pageSizeOptions: number[] = [10, 25, 50, 100];
  allmois = [];
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  @ViewChild(SelectAutocompleteComponent, {static: false}) multiSelect: SelectAutocompleteComponent;

  constructor(private missionService: MissionService,
              public planningService: PlanningService,
              private route: ActivatedRoute,
              private typeGardeService: TypeDeGardeService,
              private clientService: ClientService,
              private utilsService: UtilsService,
              private ficheSpecialiteService: FicheSpecialiteService,
              private notifierService: NotificationService,
              private specialiteService: SpecialiteService) {
  }

  ngOnInit() {

    // ffd
    moment.locale('fr');
    moment.locale('fr-Fr');
    this.month = moment(this.monthNames.indexOf(moment().format('MMMM')) + 1, 'MM');
    this.id_client = this.route.snapshot.params.id;
    this.clientService.idClient.next(this.id_client);
    this.data.id_client = this.route.snapshot.params.id;
    this.allmois = this.utilsService.getMois();
    this.planningService.showAlertSubject.subscribe(
      (showAlert) => {
        if (showAlert) {
          this.showAlertErp = showAlert;
        }
      }
    );
    this.clientService.labelTab_text.subscribe(
      (labeltext: any) => {
        if (labeltext === 'missionAndPlanning') {
          // this.planningService.AllplanningsForMissionChoiceSubject.next([]);
          // execution de la requette
          this.getData(this.id_client);
          this.clientService.allmissionsForClientSubject
            .subscribe(
              (next) => {
                // (next);
                this.missionsErp.data = next.data;
                this.length = next.lenght;
              }
            );
          this.specialiteService.list(null, true).subscribe(
            (response: any) => {
              from(response.data).pipe(
                map(
                  (item: any) => {
                    return {
                      display: item.nomspecialite,
                      value: item.id,
                    };
                  }),
                tap((el) => {
                  this.listSpecialites.push(el);
                })
              ).subscribe();
            }
          );
          this.ficheSpecialiteService.getFicheSpecialiteDistinct(this.id_client).subscribe(
            (ficheSpecialites) => {

              this.fichespecialites = ficheSpecialites;
            }
          );

          this.planningService.AllplanningsForMissionChoiceSubject.subscribe(
            (next) => {
              this.highlightDaysErp = next;
            }
          );
          this.planningService.planningsSubject.subscribe(
            (next) => {
              this.planningsTmpErp = next;
            }
          );
        } else {
          this.planningService.onResetPlanning();
        }
      }
    );
  }

  onChoisirMissionErp(missionChoisie) {
    moment.locale('fr-Fr');
    const result = this.fichespecialites.filter(element => {
      return +element.id === (missionChoisie.fiche_specialite_id);
    });
    this.typeDeGardes_default = result[0].typeGardes.filter(
      (x) => {
        return x ? +x.annee === +missionChoisie.annee : null;
      }
    );
    this.typeDeGardes_default = this.typeDeGardes_default.map((x) => {
      x.qualifs = result[0].qualifications;
      return x;
    });
    (this.typeDeGardes_default);
    //gerer le mois et lannee a afficher sur le multidapicker
    // this.month = moment().add({months: (this.monthNames.indexOf(missionChoisie.mois) + 1)});
    this.planningService.title = 'Mission : ' + missionChoisie.mois + ' ' + missionChoisie.nomSpecialite;
    this.planningService.showMission = true;
    this.planningService.hiddenListeMissions = false;
    // this.year = moment(missionChoisie.annee);
    // this.month = moment(this.monthNames.indexOf(missionChoisie.mois) + 1, 'MM');
    this.month = moment().month(this.monthNames.indexOf(missionChoisie.mois)).year(+missionChoisie.annee);
    (missionChoisie.annee);
    (this.monthNames.indexOf(missionChoisie.mois));
    this.missionChoisieErp = missionChoisie;
    this.planningService.getReformatPlanningForMission(missionChoisie.plannings);
    this.planningService.missionChoisieSubject.next(missionChoisie);
  }

  // verifier si le datepicker est cliquez ou pas
  onClickDatePicker() {
    if (this.planningService.newDatesOnMissionViaDatePiker.length > 0) {
      this.planningService.showCreateOnMissionOrList = true;
      this.planningService.newDatesOnMissionViaDatePikerSubject.next(this.planningService.newDatesOnMissionViaDatePiker);
    } else {
      this.planningService.showCreateOnMissionOrList = false;
      this.planningService.newDatesOnMissionViaDatePikerSubject.next([]);
    }
  }

  onContinuerMaj() {
    //this.hideListeMissionErp = true;
  }

  // ngAfterViewChecked() {
  //   //  this.missionsErp.paginator = this.paginator;
  //   // this.missionsErp.sort = this.sort;
  //   if (this.showAlertErp.length > 0) {
  //     if (this.showAlertErp[3]) {
  //       if (this.idRef.nativeElement.querySelector('#' + this.showAlertErp[3])) {
  //         this.idRef.nativeElement.querySelector('#' + this.showAlertErp[3]).click();
  //       }
  //     }
  //   }
  // }

  // onResetPlanning() {
  //   this.planningService.showNewCreate = false;
  //   this.planningService.newDatesOnMissionViaDatePiker = [];
  //   this.planningService.showCreateOnMissionOrList = false;
  //   this.planningService.hiddenListeMissions = true;
  //   this.planningService.showMission = false;
  //
  // }

  createNewMission() {
    this.planningService.hiddenListeMissions = false;
    this.planningService.showNewCreate = true;
  }

  ngOnDestroy() {
    this.planningService.onResetPlanning();
  }

  valutElements() {
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.data.limit = 5;
      this.data.offset = 0;

      this.getData(this.id_client);
    }
  }


  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;


    this.getData(this.id_client);


  }

  // fonction qui envoie le formulaire de filtre en base de donnee
  onSubmitFiltre(form: NgForm) {


    if (form.control.controls['specialite']) {

      form.control.controls['specialite'].setValue(this.selected);
    }
    (form.value);
    this.data.filter = form.value;
    this.getData(this.id_client);
  }

  getSelectedOptions(selected) {
    this.selected = selected;

  }

  getData(idclient) {
    this.clientService.allmissionsForClientSubject.next([]);
    this.notifierService.blockUiStart();
    this.data.id_client = idclient;
    this.clientService.getAllMissionByClient(this.data).subscribe(
      (response: any) => {
        this.clientService.allmissionsForClientSubject.next(response);
      },
      (erreur) => {
        this.notifierService.showNotificationEchec('erreur de chargement !!! veuillez ressayer à nouveau svp');
        this.notifierService.blockUiStop();
      },
      () => {
        this.notifierService.blockUiStop();
      }
    );
  }

}
