import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import * as moment from 'moment';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MissionService} from '../../../../../../../../services/mission.service';
import {CandidatureService} from '../../../../../../../../services/candidature.service';
import {RedirectionSercive} from '../../../../../../../../services/redirection.sercive';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from '../../../../../../../../services/recruteur/client.service';
import {RolesService} from '../../../../../../../../services/roles.service';
import {UserService} from '../../../../../../../../services/user.service';
import {NotificationService} from '../../../../../../../../services/notification.service';


declare var $: any;

@Component({
  selector: 'app-modal-liste-mission-for-candidatures-validation',
  templateUrl: './modal-liste-mission-for-candidatures-validation.component.html',
  styleUrls: ['./modal-liste-mission-for-candidatures-validation.component.css']
})
export class ModalListeMissionForCandidaturesValidationComponent implements OnInit, AfterViewInit {
  loaderTraitement = false;
  missionChoisie: any = [{
    medecins: null
  }];
  i: any;

  show_valider = true;
  erreur_coherence: any = '';


  mail: boolean = true;
  condition_med = '';

  hideListeCandidats = false;

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatSort, {static: false}) sortValidation: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  @ViewChild('listPaginatorValidation', {static: true}) paginatorValidation: MatPaginator;
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  dataSourceMedecins: MatTableDataSource<any[]> = new MatTableDataSource();
  tmp = [];
  displayedColumns = ['select', 'date', 'garde', 'debut', 'fin', 'tarif', 'salaire_net', 'action'];
  displayedColumnsClient = ['select', 'date', 'garde', 'debut', 'fin', 'tarif', 'action'];
  displayedColumnsMedecins = ['nom', 'prenom', 'dossier', 'action'];
  listsource: any[] = [];
  selection = new SelectionModel<any[]>(true, []);
  choixCandi = [];
  id_medecinChoisi: number;
  nomPrenomMedecin: string;
  id_client;
  select_data_source = 'En Attente';
  public choixCheckBox = [];
  message: any;
  currentUser: any;

  constructor(private missionService: MissionService,
              private candidatureService: CandidatureService,
              private redirectionService: RedirectionSercive,
              private router: Router,
              private dialog: MatDialog,
              private clientService: ClientService,
              private roleService: RolesService,
              private user_service: UserService,
              private notifierService: NotificationService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.currentUser = this.user_service.getCurrentUser();

    this.missionService.MissionsChoisieSubject.subscribe(
      (next) => {
        this.missionChoisie = next;
        if (next) {
          this.dataSourceMedecins.data = next.medecins;
          this.dataSourceMedecins.sort = this.sort;
          this.dataSourceMedecins.paginator = this.paginator;
          this.tmp = next.candidatures;
        }
      }
    );
  }

  onNoClick() {
    this.candidatureService.dialogRef.close();
  }

  onChoiceMedecin(medecin) {
    this.nomPrenomMedecin = medecin.nom + ' ' + medecin.prenom;
    this.id_medecinChoisi = medecin.interim_id;
    this.dataSource.data = this.tmp.filter((x) => {
      if (x.interim_id === this.id_medecinChoisi) {
        return x;
      }
    });
    this.dataSource.paginator = this.paginatorValidation;
    this.dataSource.sort = this.sortValidation;
  }

  getStatutColor(statut) {
    switch (statut) {
      case 'Validee':
        return 'green';
      case 'Non Retenue':
        return 'red';
      case 'En Attente':
        return '#d39e00';
    }
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  clearSelection() {
    this.selection = new SelectionModel<any[]>(true, []);
  }

  onTraitementCandidatures(choixCandiTmp) {
    this.candidatureService.dialogRef.close();
    this.notifierService.blockUiStart();
    this.id_client = this.candidatureService.idClient;
    const data_filter_array = {
      offset: 0,
      limit: 5,
      filter: null,
      id_client: this.id_client
    };
    this.candidatureService.candidaturesByMissionForClientSubject_.next([]);
    this.choixCandi = this.choixCandi.filter(function(el) {
      return el != null;
    });
    if (this.choixCandi.length > 0) {
      for (let i = 0; i < this.choixCandi.length; i++) {
        this.choixCandi[i]['auteurValidation'] = this.user_service.getCurrentUser().username;
      }
    }
    this.candidatureService.set_candidatures_for_clients(this.condition_med, this.mail, this.choixCandi).subscribe(
      (next: any) => {
        this.clientService.getCandidatureForMissionByClient(data_filter_array).subscribe(
          (liste_candidatures: any) => {
            this.candidatureService.candidaturesByMissionForClientSubject_.next(liste_candidatures);
            this.dataSource.data = liste_candidatures.candidatures ? liste_candidatures.candidatures.filter((x) => {
              return x.interim_id = this.id_medecinChoisi;
            }) : [];
            this.dataSource.sort = this.sortValidation;
            this.dataSource.paginator = this.paginatorValidation;
            this.notifierService.blockUiStop();
          }
        );
        this.selection.clear();
        if (next.erreur) {
          this.notifierService.showNotificationEchecCopie('bottomRight', next.erreur);
        } else {
          this.notifierService.showNotificationSuccessCopie('bottomRight', next.success || 'Veuillez réessayer !');
        }
      },
      (error2) => {
        this.notifierService.showNotificationEchecCopie('bottomRight', error2);
      },
    );
  }

  selectValueForCandidatureByPeriode(statut) {
    this.selection.selected.forEach(function(element: any) {
      element.statusChoisi = statut;
    });
    this.choixCandi = this.selection.selected;
    if (statut.toLowerCase() === 'validee') {
      this.CheckCoherence(this.choixCandi);
    } else {
      this.show_valider = true;
    }
  }

  // verifie si le couple choisi respectent lalgo de validation
  CheckCoherence(choixCandi) {
    // if (choixCandi.selected) {
    //   choixCandi = choixCandi.selected.map((x) => {
    //      x.statusChoisi = 'validee';
    //      return x;
    //   });
    // }
    // (choixCandi.selected[0].statusChoisi);
    this.erreur_coherence = '';
    // Nuit et Astreinte
    const array_nuit = [
      'nuit', 'nuit jt', 'nuit ht', 'astreinte',
      'astreinte jt', 'astreinte ht'
    ];
    // Nuit et Astreinte
    const array_deminuit = [
      'demi-nuit', 'demi-nuit jt', 'demi-nuit ht', 'astreinte',
      'astreinte jt', 'astreinte ht'
    ];
    const array_journee = [
      'journée', 'journée jt',
      'journée ht', 'journée + demi-nuit ht',
      'journée + astreinte', 'journée + astreinte jt', 'journée + astreinte ht'
    ];
    const array_demijournee = [
      'demi journée',
      'demi journée jt', 'demi journée ht',
      'journée + demi-nuit', 'journée + demi-nuit jt'
    ];
    const array_h24 = [
      '24 h', 'h24', '24 h jt', '24 h ht'
    ];
    const array_validee = [];
    choixCandi.forEach((item) => {
      const current_date = moment(item.date).format('DD-MM-YYYY');
      const date_plus_un = moment(item.date).add(1, 'days').format('DD-MM-YYYY');
      const date_moins_un = moment(item.date).add(-1, 'days').format('DD-MM-YYYY');
      if ((item.statusChoisi).toLowerCase() === 'validee') {
        choixCandi.forEach((element_) => {
          console.log(element_.date);
          const element_date = moment(element_.date).format('DD-MM-YYYY');
          // si le libelle de la date choisie est journee
          if (array_journee.concat(array_demijournee).indexOf(item.designation.toLowerCase())) {
            // date-1 en nuit ou 24h et status choisie a validee impossible
            if ((date_moins_un === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && (array_nuit.concat(array_h24).includes(element_.designation.toLowerCase()))
            ) {
              this.erreur_coherence = '';
              this.erreur_coherence = ' la veille en  \'' + element_.designation + ' : Incompatible avec la date du ' + date_moins_un;
            }
            // meme date pour plusieurs vacation en journee impossible
            if ((current_date === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && ((array_h24.concat(array_journee).includes(element_.designation.toLowerCase())))
              && ((item.candidature_id) !== (element_.candidature_id))
            ) {
              this.erreur_coherence = '';
              this.erreur_coherence = 'Impossibilité avec la date du : ' + current_date;
            }
          }
          // si le libelle de la date choisie est nuit
          if (array_nuit.includes(item.designation.toLowerCase())) {
            // date+1 en journee ou 24h et status choisie a validee impossible
            if ((date_plus_un === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && ((array_h24.concat(array_journee).includes(element_.designation.toLowerCase())))) {
              this.erreur_coherence = '';
              this.erreur_coherence = ' le lendemein en  \'' + element_.designation + ' : Incompatible avec la date du ' + date_moins_un;
            }
            // meme date pour plusieurs vacation en journee nuit ou 24 h
            ((array_h24.concat(array_nuit, array_deminuit).includes(element_.designation.toLowerCase())));
            if ((current_date === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && ((array_h24.concat(array_nuit, array_deminuit).includes(element_.designation.toLowerCase())))
              && (item.candidature_id) !== (element_.candidature_id)) {
              this.erreur_coherence = '';
              this.erreur_coherence = 'Nuit Impossibilité avec la date du : ' + current_date;
            }
          }
          // si le libelle de la date choisie est 24 h
          if (array_h24.includes(item.designation.toLowerCase())) {
            // date+1 en journee ou 24h et status choisie a validee impossible
            if ((date_plus_un === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && ((array_h24.concat(array_journee, array_demijournee).includes(element_.designation.toLowerCase())))) {
              this.erreur_coherence = '';
              this.erreur_coherence = ' le lendemein en  \'' + element_.designation + ' : Incompatible avec la date du ' + date_plus_un;
            }
            // date-1 en nuit ou 24h et status choisie a validee impossible
            if ((date_moins_un === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && ((array_h24.concat(array_h24, array_nuit).includes(element_.designation.toLowerCase())))) {
              this.erreur_coherence = '';
              this.erreur_coherence = 'la veille en  \'' + element_.designation + ' : Incompatible avec la date du ' + date_moins_un;
            }
            // meme date pour plusieurs vacation en journee nuit ou 24 h
            if ((current_date === element_date) && (element_.statusChoisi.toLowerCase() === 'validee')
              && (array_h24.concat(array_nuit, array_deminuit).includes(element_.designation.toLowerCase()))
              && (item.candidature_id) !== (element_.candidature_id)) {
              this.erreur_coherence = '';
              this.erreur_coherence = 'Impossibilité avec la date du : ' + current_date;
            }
          }
        });
      }
      if (this.erreur_coherence) {
        this.notifierService.showNotificationEchecCopie('bottomRight', this.erreur_coherence);
        this.show_valider = false;
        return this.erreur_coherence;
      } else {
        this.show_valider = true;
      }
    });
  }

  ngAfterViewInit() {
    this.dataSourceMedecins.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.paginator = this.paginatorValidation;
    this.dataSource.sort = this.sortValidation;
  }

  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }

  fermerModal() {
    $('#listeMissionForCandidatures_validation').modal('hide');
  }

}
