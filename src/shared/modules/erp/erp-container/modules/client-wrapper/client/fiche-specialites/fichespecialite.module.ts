import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FicheSpecialiteClientComponent} from './fiche-specialite-client/fiche-specialite-client.component';
import {SharedModule} from '../../../../../../shared/shared.module';
import {FicheSpecialiteRouting} from './fichespecialite.routing';

@NgModule({
  declarations: [
  FicheSpecialiteClientComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FicheSpecialiteRouting
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  entryComponents: [
  ]
})

export class FichespecialiteModule { }
