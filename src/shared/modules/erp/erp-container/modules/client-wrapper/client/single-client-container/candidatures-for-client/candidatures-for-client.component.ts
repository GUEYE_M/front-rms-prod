import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormGroup, NgForm} from '@angular/forms';
import {SelectAutocompleteComponent} from 'select-autocomplete';
import {ClientService} from '../../../../../../../../services/recruteur/client.service';
import {CandidatureService} from '../../../../../../../../services/candidature.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilsService} from '../../../../../../../../services/utils.service';
import {MissionService} from '../../../../../../../../services/mission.service';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {SpecialiteService} from '../../../../../../../../services/specialite.service';
import {RolesService} from "../../../../../../../../services/roles.service";
import {UserService} from "../../../../../../../../services/user.service";
import {ModalListeMissionForCandidaturesValidationComponent} from '../../modal-client/modal-liste-mission-for-candidatures-validation/modal-liste-mission-for-candidatures-validation.component';
import {from} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-candidatures-for-client',
  templateUrl: './candidatures-for-client.component.html',
  styleUrls: ['./candidatures-for-client.component.css']
})
export class CandidaturesForClientComponent implements OnInit, OnDestroy {

  dataSourceInitial = [];
  missionChoisie: any;
  choixCandi = [];
  listSpecialites = [];
  selected: [];

  @Input() dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
  displayedColumns: string[] = ['num', 'reference', 'mois', 'annee', 'specialite', 'etat', 'action'];
  displayedColumnsFilter = [
    {
      name: 'reference',
      label: 'Reference'
    },
    {
      name: 'mois',
      label: 'Mois',
    },
    {
      name: 'annee',
      label: 'Annee',
    },
    // {
    //   name: 'specialite',
    //   label: 'Specialite',
    // },
  ];

  pageSize = 5;

  @Input() length: number = 0;

  elements = [];
  data = {
    offset: 0,
    limit: 5,
    filter: null,
    id_client: null
  };
  //  displayedColumnsFilter =  []

  pageSizeOptions: number[] = [5, 12, 25, 50, 100];


  allmois = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  validationForm: FormGroup;
  id_medecinChoisi: number;
  id_client;
  currentUser: any;
  loaderTraitement = false;
  message: any;
  // items: FormArray;
  nomPrenomMedecin: string;
  @ViewChild(SelectAutocompleteComponent, {static: false}) multiSelect: SelectAutocompleteComponent;

  constructor(private clientService: ClientService,
              private candidatureService: CandidatureService,
              private router: Router,
              private utilsService: UtilsService,
              private activatedRoute: ActivatedRoute,
              private roleService: RolesService,
              private user_service: UserService,
              public dialog: MatDialog,
              private missionService: MissionService,
              private notifierService: NotificationService,
              private specialiteService: SpecialiteService) {
  }

  ngOnInit() {
    this.data.id_client = this.activatedRoute.snapshot.params['id'];
    this.candidatureService.idClient = this.data.id_client;
    this.allmois = this.utilsService.getMois();
    this.onSpecialite();
    this.candidatureService.loaderTraitementCandidatureServiceSubject.next(false);
    this.candidatureService.loaderTraitementCandidatureServiceSubject.subscribe(
      (next) => this.loaderTraitement = next
    );
    this.clientService.labelTab_text.subscribe(
      (label_text) => {
        if (label_text === 'candidature_client') {
          this.getData();
          this.candidatureService.candidaturesByMissionForClientSubject_.subscribe(
            (candidatures: any) => {
              this.dataSource = new MatTableDataSource(candidatures.data);
              this.length = candidatures.lenght;
            }
          );
        }
      }
    );
    this.id_client = this.activatedRoute.snapshot.params['id'];
  }
  onSpecialite(){
    this.specialiteService.list(null, true).subscribe(
      (response: any) => {
        from(response.data).pipe(
          map(
            (item: any) => {
              return {
                display: item.nomspecialite,
                value: item.id,
              };
            }),
          tap((el) => {
            this.listSpecialites.push(el);
          })
        ).subscribe();
      }
    );
  }
  getSelectedOptions(selected) {
    this.selected = selected;

  }
  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }

  onChoiceIdMission(index) {
    this.missionChoisie = this.dataSource.data[index];
    this.missionService.MissionsChoisieSubject.next(this.missionChoisie);
    this.candidatureService.dialogRef = this.dialog.open(ModalListeMissionForCandidaturesValidationComponent, {
      width: '900px',
      height: '600px',
      panelClass: 'myapp-no-padding-dialog',
      data: 1
    });
  }

  onChoiceMedecin(indexMed) {
    this.nomPrenomMedecin = this.missionChoisie.medecins[indexMed].nom + ' ' + this.missionChoisie.medecins[indexMed].prenom;
    this.id_medecinChoisi = this.missionChoisie.medecins[indexMed].interim_id;
  }

  getStatutColor(statut) {
    switch (statut) {
      case 'Validee':
        return 'green';
      case 'Non Retenue':
        return 'red';
      case 'En Attente':
        return '#d39e00';
    }
  }

  ngOnDestroy() {
    this.clientService.labelTab.next(0);
  }


  valutElements() {


    if (this.elements.length === 0) {
      this.data.filter = null;
      this.data.limit = 5;
      this.data.offset = 0;

      this.getData();
    }
  }


  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;


    this.getData();


  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre(form: NgForm) {

    if (form.control.controls['specialite']) {
      form.control.controls['specialite'].setValue(this.multiSelect.selectedValue);
    }

    this.data.filter = form.value;
    this.getData();

  }

  getData() {
    this.notifierService.blockUiStart();
    this.data.id_client = this.id_client;
    this.clientService.getCandidatureForMissionByClient(this.data).subscribe(
      (response: any) => {
        this.candidatureService.candidaturesByMissionForClientSubject_.next(response);
      },
      (erreur) => {
        this.notifierService.showNotificationEchec('erreur de chargement!! veuillez ressayer à nouveau svp');
        this.notifierService.blockUiStop();
      },
      () => {
        this.notifierService.blockUiStop();
      }
    );
  }

}
