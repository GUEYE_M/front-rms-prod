import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheSpecialiteClientComponent } from './fiche-specialite-client.component';

describe('FicheSpecialiteClientComponent', () => {
  let component: FicheSpecialiteClientComponent;
  let fixture: ComponentFixture<FicheSpecialiteClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheSpecialiteClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheSpecialiteClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
