import {Route, RouterModule} from '@angular/router';
import {FicheSpecialiteClientComponent} from './fiche-specialite-client/fiche-specialite-client.component';
import {FicheSpecialiteResolve} from '../../../../../../../resolver/ficheSpecialite.resolve';


const ROUTE_CONFIG: Route[] = [
  {
    path: 'liste',
    component: FicheSpecialiteClientComponent,
    resolve: { listeficheSpecialites: FicheSpecialiteResolve}
  },
];

export const FicheSpecialiteRouting = RouterModule.forChild(ROUTE_CONFIG);
