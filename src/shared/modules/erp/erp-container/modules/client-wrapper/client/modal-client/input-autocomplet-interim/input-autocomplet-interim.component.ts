import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatDialogRef} from '@angular/material';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {InterimService} from '../../../../../../../../services/interim/interim.service';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-input-autocomplet-interim',
  templateUrl: './input-autocomplet-interim.component.html',
  styleUrls: ['./input-autocomplet-interim.component.css']
})
export class InputAutocompletInterimComponent implements OnInit {

  viewform: boolean = false;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  interimCtrl = new FormControl();
  filteredInterims: Observable<string[]>;
  intems: any[] = [];
  allInterims: any[];

  @ViewChild('interimInput', {read: false, static: false}) interimInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {read: false, static: false}) matAutocomplete: MatAutocomplete;

  constructor(
    private interimService: InterimService, private notificationService: NotificationService,
    public dialogRef: MatDialogRef<InputAutocompletInterimComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

    this.getAllInterim();
    // this.filteredInterims = this.fruitCtrl.valueChanges.pipe(
    //   startWith(null),
    //   map((item: string | null) => item ? this._filter(item) : this.allInterims.slice()));


  }

  ngOnInit() {


  }

  getAllInterim() {
    this.notificationService.blockUiStart();
    this.interimService.getAllInterimByspecialiteAndInscription(this.data).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.notificationService.showNotificationEchec(response.erreur);
        } else {
          this.allInterims = response.data;
          this.filteredInterim();
        }
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur chargement, veillez ressayez');
        this.notificationService.blockUiStop();
      },
      () => {
        this.viewform = true;
        this.notificationService.blockUiStop();
      }
    );
  }

  add(event: MatChipInputEvent): void {

    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;


      // ajouter un interimaire
      if (value) {

        this.intems.push(value);
      }

      //  annuler interimaire
      if (input) {
        input.value = '';
      }

      this.interimCtrl.setValue(null);
    }
  }

  remove(item): void {


    const index = this.intems.indexOf(item);

    if (index >= 0) {
      this.intems.splice(index, 1);
      this.allInterims.push(item);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {


    this.intems.push(event.option.value);

    const index = this.allInterims.findIndex(item => +item.id === +event.option.value.id);
    this.allInterims.splice(index, 1);


    this.interimInput.nativeElement.value = '';
    this.interimCtrl.setValue(null);


    this.filteredInterim();
  }

  filteredInterim() {
    this.filteredInterims = this.interimCtrl.valueChanges
      .pipe(
        startWith(null),
        map(
          (item: string | null) => item ? this._filter(item) : this.allInterims
        ));
  }

  save() {

    this.dialogRef.close(this.intems);
  }

  private _filter(value: any) {

    return this.allInterims.filter(
      item => item.id === value.id
    );
  }

}
