import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListClientRecruteurComponent } from './list-client-recruteur.component';

describe('ListClientRecruteurComponent', () => {
  let component: ListClientRecruteurComponent;
  let fixture: ComponentFixture<ListClientRecruteurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListClientRecruteurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListClientRecruteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
