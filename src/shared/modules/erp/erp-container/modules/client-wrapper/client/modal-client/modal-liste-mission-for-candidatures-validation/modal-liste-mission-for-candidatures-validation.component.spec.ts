import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalListeMissionForCandidaturesValidationComponent } from './modal-liste-mission-for-candidatures-validation.component';

describe('ModalListeMissionForCandidaturesValidationComponent', () => {
  let component: ModalListeMissionForCandidaturesValidationComponent;
  let fixture: ComponentFixture<ModalListeMissionForCandidaturesValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListeMissionForCandidaturesValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListeMissionForCandidaturesValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
