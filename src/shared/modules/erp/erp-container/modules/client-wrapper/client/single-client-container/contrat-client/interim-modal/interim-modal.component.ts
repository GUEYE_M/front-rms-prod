import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';

@Component({
  selector: 'app-interim-modal',
  templateUrl: './interim-modal.component.html',
  styleUrls: ['./interim-modal.component.css']
})
export class InterimModalComponent implements OnInit {

  constructor(
    private bottomSheetRef: MatBottomSheetRef<InterimModalComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  ) {}

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
  ngOnInit() {
  }

}
