import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ClientPldModel } from '../../../../../../../../models/interim/ClientPld.model';
import { FormBuilder, FormControl, NgForm, Validators } from '@angular/forms';
import { ModalTypeGardeComponent } from '../../modal-client/modal-type-garde/modal-type-garde.component';
import { SelectAutocompleteComponent } from 'select-autocomplete';
import { BehaviorSubject, from, Subscription } from 'rxjs';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { ViewMessageProdpectionModalComponent } from '../../../../prospection/view-message-prodpection-modal/view-message-prodpection-modal.component';
import { Specialite } from '../../../../../../../../models/Specialite.model';
import { SelectionModel } from '@angular/cdk/collections';
import { ClientService } from '../../../../../../../../services/recruteur/client.service';
import { FilterService } from '../../../../../../../../services/filter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { FicheClientService } from '../../../../../../../../services/recruteur/fiche-client.service';
import { PldService } from '../../../../../../../../services/pld.service';
import { SpecialiteService } from '../../../../../../../../services/specialite.service';
import { MissionService } from '../../../../../../../../services/mission.service';
import { RolesService } from '../../../../../../../../services/roles.service';
import { UtilsService } from '../../../../../../../../services/utils.service';
import { UserService } from '../../../../../../../../services/user.service';
import { FicheSpecialiteService } from '../../../../../../../../services/recruteur/fiche-specialite.service';
import { ApiVilleService } from '../../../../../../../../services/api_ville_service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Client } from '../../../../../../../../models/recruteur/Client.model';
import { map, tap } from 'rxjs/operators';
import { FicheSpecialiteClientEditComponent } from '../../fiche-specialites/fiche-specialite-client-edit/fiche-specialite-client-edit.component';
import {EditListeClientComponent} from '../../edit-liste-client/edit-liste-client.component';
import {AddIdPldManuelComponent} from '../../../../missions-interimaires/add-id-pld-manuel/add-id-pld-manuel.component';

@Component({
  selector: 'app-single-client',
  templateUrl: './single-client.component.html',
  styleUrls: ['./single-client.component.css']
})
export class SingleClientComponent implements OnInit {

  @ViewChild('labelTab', { read: ElementRef, static: false }) labelTab: ElementRef;

  @ViewChild(SelectAutocompleteComponent, { static: false }) multiSelect: SelectAutocompleteComponent;
  id_client: any;
  singleClient: BehaviorSubject<any[]> = new BehaviorSubject([]);
  dataSourceInitial = [];
  dialogRefInfoPratique: MatDialogRef<ViewMessageProdpectionModalComponent>;
  dialogRefTypeGarde: MatDialogRef<ModalTypeGardeComponent>;
  dialogRefUpdateClient: MatDialogRef<EditListeClientComponent>;
  dialogRefAddidPldContrat: MatDialogRef<AddIdPldManuelComponent>;

  missions = [];
  count_tmp = 0;
  adresse: any;

  candidatureByMissionForClient = [];
  subsciprition_allmissionsForClientSubject: Subscription;
  client?: any = [];
  ficheClient: any;
  ficheSpecialite: any;
  index_tab = 0;
  elements?: any = [];
  elements_stat?: any = [];
  data = {
    filter: null,
    id_client: null
  };

  data_stat = {
    filter: null,
    id_client: null
  };

  specialite = new FormControl('', [Validators.required]);
  annee = new FormControl('', [Validators.required]);

  nom_etablissement: any;
  currentUser: any;

  pageEvent: PageEvent;
  pageSize = 5;
  colCard: any;
  length = 0;
  pageSizeOptions: number[] = [5, 12, 25, 50, 100];


  bufferValue = 75;
  panelOpenState = false;

  dialogRef: MatDialogRef<FicheSpecialiteClientEditComponent>;

  listSpecialites: Specialite[] = [];
  listSpeciliteFilter = [];

  annees: any[] = [
    { annee: new Date().getFullYear() },
    { annee: new Date().getFullYear() - 1 },
    { annee: new Date().getFullYear() - 2 },
    { annee: new Date().getFullYear() - 3 },
    { annee: new Date().getFullYear() - 4 },
    { annee: new Date().getFullYear() - 5 },
    { annee: new Date().getFullYear() - 6 },
    { annee: new Date().getFullYear() - 7 },
    { annee: new Date().getFullYear() - 8 },
    { annee: new Date().getFullYear() - 9 },
  ];

  displayedColumns: any[] =
    [
      'number', 'Specialite', 'Nombre Lit',
      'Medecin Jour', 'Medecin Nuit', 'Passage Annee', 'info', 'vacation'];
  displayedColumnsFilterStat = [
    {
      name: 'specialite',
      label: 'Specialite'
    },
    {
      name: 'annee',
      label: 'Annee'
    }
  ];


  data_of = {
    offset: 0,
    limit: 5,
    filter: null,
    id_client: null

  };
  displayedColumnsFilter = [
    {
      name: "specialite",
      label: "Specilaite"
    },
  ];
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource<any[]>([]);
  // mission du client
  missionsErp: MatTableDataSource<any[]> = new MatTableDataSource();
  length_missionPlanning = 0;
  // candidature du client
  dataSourceCandidatureForClient: MatTableDataSource<any[]> = new MatTableDataSource();
  length_candidatureForClient = 0;

  fichespe: any[];
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Supprimer', 'Desactiver'];
  listsource: any[] = [];
  allmois = [];
  selected: [];

  constructor(private clientService: ClientService,
    private notificationService: NotificationService,
    private ficheClientService: FicheClientService,
    private activatedRoute: ActivatedRoute,
    private pldService: PldService,
    private dialog: MatDialog,
    private specialiteService: SpecialiteService,
    private missionService: MissionService,
    public roleService: RolesService,
    private utilsService: UtilsService,
    private userService: UserService,
    private ficheSpecialiteService: FicheSpecialiteService,
    private apiVilleService: ApiVilleService) {
  }

  ngOnInit() {
    // on instancie afin de vider le behaviorsubject
    this.clientService.labelTab_text = new BehaviorSubject('');
    this.notificationService.blockUiStop();
    this.allmois = this.utilsService.getMois();
    if (localStorage.getItem('active_route')) {
      const active_route = localStorage.getItem('active_route');
      this.clientService.labelTab_text.next(active_route);
    }

    this.currentUser = this.userService.getCurrentUser();
    if (this.roleCommercial()) {
      this.colCard = 'col-md-6 col-sm-6 col-lg-3 mb-2';
      this.displayedColumns.push('Coeficient Saison');
      this.displayedColumns.push('Coeficient Tarification');
      this.displayedColumns.push('Edit');
    } else {
      this.displayedColumns.push('Edit');
      this.colCard = 'col-md-6 col-sm-6 col-lg-4 mb-2';

    }
    const client = this.activatedRoute.snapshot.data['client'].data;
    this.client = client;
    // let missionsForClient = client.missionsForClient;
    // missionsForClient.forEach(function (element) {
    // this.apiVilleService.findbyCommune(client.ville).subscribe(
    //     (response) => {
    //         this.adresse = response[0];
    //     }
    // );
    // this.ficheSpecialiteService.closemodale.subscribe(
    //   (response: boolean) => {
    //     if (response) {
    //       if (this.dialogRef.id) {
    //         this.dialogRef.close();
    //       }
    //     }
    //   }
    // );
    this.data.id_client = this.activatedRoute.snapshot.params.id;
    this.data_stat.id_client = this.activatedRoute.snapshot.params.id;


    this.clientService.currentClient.next(client);
    //   this.candidatureByMissionForClient = this.client.CandidatureByMissionForClient;
    this.ficheClient = this.client.ficheClient;
    // this.ficheClientService.ficheClientSubject.next(this.client.ficheClient);
    // this.missions = this.client.missionsForClient;
    this.clientService.currentClient.next(client);

    // this.index_tab = this.redirectionService.labelTab_client.getValue();
    // this.listSpecialite();
    this.clientService.labelTab_text.subscribe(
      (label_text) => {
        if (label_text === 'fiche_specialite') {
          this.getData(false);
          // this.ficheSpecialiteService.closemodale.subscribe(
          //   (response: boolean) => {
          //     if (response) {
          //       if (this.dialogRef.id) {
          //         this.dialogRef.close();
          //       }
          //     }
          //   }
          // );
          this.ficheSpecialiteService.ficheSpeByClientSubject.subscribe(
            (next) => {
              this.dataSource.data = next ? next : [];
            }
          );
        }
      }
    );

  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  drop(event: CdkDragDrop<any>) {
    moveItemInArray(this.client, event.previousIndex, event.currentIndex);
  }


  getbooleanString(value: boolean) {
    let v: string
    if (value == true) {
      v = "oui"
    }
    else {
      v = "non"

    }
    return v
  }

  editFicheSpecilaiteModal(client: Client, edit: boolean = false, id_fiche: number = null): void {
    this.notificationService.blockUiStart();

    this.specialiteService.list(null, true).subscribe(
      (spe: any) => {
        this.count_tmp = this.count_tmp + 1;
        if (!spe.erreur) {
          this.notificationService.blockUiStop();
          this.dialogRef = this.dialog.open(FicheSpecialiteClientEditComponent, {
            width: '900px',
            height: '700px',
            data: {
              'client': client,
              'edit': edit,
              'id_fiche': id_fiche,
              'allspecilite': spe.data
            }
          });

          this.dialogRef.afterClosed().subscribe(
            (item) => {
              if (item) {
                this.getData(false);
              }

            }
          );
        } else {
          this.notificationService.showNotificationEchec('erreur chargement formulaire veillez ressayer');
        }
      }
    );

  }


  getData(statistique: boolean) {
    this.notificationService.blockUiStart();

    if (!statistique) {

      // this.notificationService.blockUiStart();
      this.ficheSpecialiteService.ficheSpecialiteByClient(this.data).subscribe(
        (response: any) => {
          this.notificationService.blockUiStop();

          this.dataSource = new MatTableDataSource(response.data);
          if (response) {
            this.length = response.lenght;
          }


          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        (erreur) => {
          this.notificationService.showNotificationEchec('erreur chargement fiche specilite du centre hospitalier !! veillez ressayer a nouveau');
          this.notificationService.blockUiStop();
        },
        () => {
          this.listSpeciliteFilter = [];

          from(this.dataSource.data).pipe(
            map(
              (item: any) => {
                return {
                  display: item.specialite,
                  value: item.id_spe,

                };

              }),
            tap((el) => {
              this.listSpeciliteFilter.push(el);
            })
          ).subscribe();
          this.notificationService.blockUiStop();
        }
      );

    } else {
      this.notificationService.blockUiStart();
      this.clientService.find(this.data_stat).subscribe(
        (response: any) => {
          this.notificationService.blockUiStop();
          if (!response.erreur) {
            this.client = response.data;
          } else {
            this.notificationService.blockUiStop();
            this.notificationService.showNotificationEchec(response.erreur);
          }


        },
        (erreur) => {
          this.notificationService.showNotificationEchec('Erreur Chargement profil, Veillez contacter le service IT si le probleme persiste');
          this.notificationService.blockUiStop();
        },
        () => {
          this.notificationService.blockUiStop();
        }
      );
    }
  }

  // recuperation des fiches client
  getFicheClientForClient(id_client) {
    this.ficheClientService.ficheClientSubject.next([]);
    this.notificationService.blockUiStart();
    this.ficheClientService.getFicheClient(id_client).subscribe(
      (next) => {
        this.notificationService.blockUiStop();
        this.ficheClient = next[0] ? next[0] : [];
        this.ficheClientService.ficheClientSubject.next(next[0]);
      }
    );
  }

  // recuperation des prospections du client
  getProspectionsClient(id_client) {
  }


  changeItemTab(index) {
    const id_client = this.client.id;
    switch (index) {
      case 0:
        this.clientService.labelTab_text.next('general');
        // this.getStatistiquesClient(id_client);
        break;
      case 1:
        this.clientService.labelTab_text.next('profil');
        // this.listSpecialite();
        break;
      case 2:
        this.clientService.labelTab_text.next('fiche_specialite');
        // this.listSpecialite();
        break;
      case 3:
        this.clientService.labelTab_text.next('ficheClient');
        this.getFicheClientForClient(id_client);
        break;
      case 4:
        this.clientService.labelTab_text.next('missionAndPlanning');
        break;
      case 5:
        this.clientService.labelTab_text.next('candidature_client');
        break;
      case 6:
        if (this.roleCommercial())
          this.clientService.labelTab_text.next('message_de_prospections');
        break;
      case 7:
        this.clientService.labelTab_text.next('contrat');
        break;
      default:
      // code block
    }
  }

  infoPratique(message: any) {
    this.dialogRefInfoPratique = this.dialog.open(ViewMessageProdpectionModalComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: message

    });
  }

  vacation(id: number) {
    this.dialogRefTypeGarde = this.dialog.open(ModalTypeGardeComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: id
    });
  }

  // ngOnDestroy() {
  //   this.ficheSpecialiteService.closemodale.next(false);
  //   this.clientService.labelTab_text.next('general');
  //   // this.clientService.allmissionsForClientSubject.unsubscribe();
  // }

  roleCommercial() {
    return this.roleService.isCommercial(this.currentUser.roles);
  }



  onSubmitFiltre(form: NgForm, stat = false) {


    if (stat) {
      this.data_stat.filter = form.value;

    } else {

      if (this.selected.length > 0) {

        form.control.controls['specialite'].setValue(this.selected);
      }
      this.data.filter = form.value;
    }


    this.getData(stat);


  }

  getSelectedOptions(selected) {
    this.selected = selected;

  }

  valutElements(stat = false) {

    if (stat) {
      if (this.elements_stat) {
        if (this.elements_stat.length === 0) {
          this.data_stat.filter = null;
          this.getData(true);
        }
      }
    } else {
      if (this.elements) {
        if (this.elements.length === 0) {
          this.data.filter = null;
          this.getData(false);
        }
      }
    }
  }

  saveOnPLD() {
    this.notificationService.onConfirm('êtes-vous sûr de vouloir enregistrer ce client sur PLD ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        this.notificationService.blockUiStart();
        this.pldService.addClient(this.client.id).subscribe((resp) => {
          const resultat = JSON.parse(resp);
          if (resultat.Message === 'Ajout  avec succès.') {
            const clientUpdate = { id: this.client.id, idPld: resultat.ID };
            this.clientService.updateIdPld(clientUpdate).subscribe(response => {
              if (response === 'ok') {
                this.notificationService.showNotificationSuccessCopie('bottomRight',
                  'Le client est maintenant enregistré sur PLD !');
              } else {
                this.notificationService.showNotificationEchecCopie('bottomRight', 'Le client n\' a pas été enregistré sur PLD !');
              }
            },
              error => {
                this.notificationService.showNotificationEchecCopie('bottomRight', 'Le client n\' a pas été enregistré sur PLD !');
              },
              () => {
                this.notificationService.blockUiStop();
                this.utilsService.redirectEspace({ espace: 'client', id: this.client.id });
              }
            );
          }
        },
          error => {
            this.notificationService.showNotificationEchecCopie('bottomRight', error.error.message);
            this.notificationService.blockUiStop();
          });
      }
    });
  }
  editClientModal(idClient = null) {
    const items = {
      profil: false,
      id: idClient
    };
    this.dialogRefUpdateClient = this.dialog.open(EditListeClientComponent, {
      width: '80%',
      height: '470px',
      panelClass: 'myapp-no-padding-dialog',
      data: items

    });
  }
  addIdPldContrat() {
    this.dialogRefAddidPldContrat = this.dialog.open(AddIdPldManuelComponent, {
      panelClass: 'myapp-no-padding-dialog',
      data: { id: this.client.id, user: 'client' },
      width: '20%',
      height: '30%'
    });
  }
}
