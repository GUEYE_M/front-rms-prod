import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {TypeDeGardeService} from '../../../../../../../../../services/recruteur/type-de-garde.service';
import {PlanningService} from '../../../../../../../../../services/recruteur/planning.service';
import {MissionService} from '../../../../../../../../../services/mission.service';
import {ClientService} from '../../../../../../../../../services/recruteur/client.service';
import {FicheSpecialiteService} from '../../../../../../../../../services/recruteur/fiche-specialite.service';
import {ActivatedRoute} from '@angular/router';
import {RolesService} from '../../../../../../../../../services/roles.service';
import {UserService} from "../../../../../../../../../services/user.service";

@Component({
  selector: 'app-create-planning',
  templateUrl: './create-planning.component.html',
  styleUrls: ['./create-planning.component.css']
})
export class CreatePlanningComponent implements OnInit {
  currentUser: any;
  monthNames = [
    "Janvier", "Fevrier", "Mars",
    "Avril", "Mai", "Juin", "Juillet",
    "Août", "Septembre", "Octombre",
    "Novembre", "Decembre"
  ];
  //Listes des dates dans le multi datepicker
  initialCountCreate: any = [];
  //une copie de initialCount qui permet de savoir si la date est deselectionnee ou linverse
  listesVacations: any = [];
  // lensemble des types de gardes
  typeDeGardes: any = [];
  typeDeGardeDefaut?: any = [];
  //listes des fiches de Specialites
  ficheSpecialiteDefaut: any ;
  ficheSpecialiteDefautChoix: any = [];
  showAjout = true;
  //listes des fiches de missions
  @Input() missions: any = [];
  @Input() ficheSpecialites: any = [];
  constructor(
    private typeGardeService: TypeDeGardeService,
    private planningService: PlanningService,
    private missionService: MissionService,
    private clientService: ClientService,
    private userService: UserService,
    public roleService: RolesService,
    private ficheSpecialiteService: FicheSpecialiteService,
    private activatedRoute: ActivatedRoute

  ) { }
  bsRangeValue: Date[];
  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    moment().locale('fr-Fr');
    const idClient = this.activatedRoute.snapshot.params.id;
  }
  setDateIntoList(dateItem , indexVacation) {
    this.typeDeGardeDefaut[indexVacation]['datePlan'] = dateItem;
  }

}
