import {Component, Input, OnInit} from '@angular/core';
import {FicheClientService} from '../../../../../../../../services/recruteur/fiche-client.service';

@Component({
  selector: 'app-fiche-client',
  templateUrl: './fiche-client.component.html',
  styleUrls: ['./fiche-client.component.css']
})
export class FicheClientComponent implements OnInit {

  @Input() ficheClient: any[] = [];
  ficheClientCopie:any;
  showBtnEdit = true;
  edit: boolean = true;
  disabled: boolean = true;
  constructor(
    private ficheClientService: FicheClientService,
  ) { }
  ngOnInit() {
    this.ficheClientService.ficheClientSubject.next(this.ficheClient);
    this.ficheClientService.ficheClientSubject.subscribe(
      (next) => {
        if(next){
          if(next.id){
            this.showBtnEdit = false;
          }else{
            this.showBtnEdit = true;
          }
        }
      }
    );
  }
  onEdit(){
    this.showBtnEdit = false;
    this.edit = false;
    this.disabled = false;
  }

}
