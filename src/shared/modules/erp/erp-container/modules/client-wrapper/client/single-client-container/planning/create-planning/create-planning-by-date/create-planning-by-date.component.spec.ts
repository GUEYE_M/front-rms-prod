import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePlanningByDateComponent } from './create-planning-by-date.component';

describe('CreatePlanningByDateComponent', () => {
  let component: CreatePlanningByDateComponent;
  let fixture: ComponentFixture<CreatePlanningByDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePlanningByDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePlanningByDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
