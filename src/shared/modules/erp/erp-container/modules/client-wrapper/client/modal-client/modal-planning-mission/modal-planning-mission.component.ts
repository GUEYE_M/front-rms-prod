import {Component, OnInit, ViewChild, AfterViewInit, Inject} from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator, MAT_DIALOG_DATA} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {NgxDrpOptions, PresetItem, Range} from 'ngx-mat-daterange-picker';
import {DatePipe} from '@angular/common';
import {UtilsService} from 'src/shared/services/utils.service';
import {ActivatedRoute} from '@angular/router';
import {PlanningService} from 'src/shared/services/recruteur/planning.service';
import {NgForm} from '@angular/forms';
import {MissionService} from 'src/shared/services/mission.service';
import {NotificationService} from '../../../../../../../../services/notification.service';

@Component({
  selector: 'app-modal-planning-mission',
  templateUrl: './modal-planning-mission.component.html',
  styleUrls: ['./modal-planning-mission.component.css']
})
export class ModalPlanningMissionComponent implements OnInit, AfterViewInit {
  dataSourceInitial = [];
  uri_listes_plannings = 'plannings/listes-filter';
  dateDebutInitial: any;
  statuts = ['A Pourvoir', 'Pourvue', 'Annuler Ch'];
  displayedColumns = ['number', 'date', 'specialite', 'mois', 'annee', 'debut', 'fin', 'statut', 'tarif', 'salaire', 'etablissement'];
  dataSource: any = new MatTableDataSource();

  selection = new SelectionModel<any[]>(true, []);


  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];


  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  constructor(@Inject(MAT_DIALOG_DATA) public id: any,
              private notifierService: NotificationService,
              private missionService: MissionService
  ) {
    (id);

    if (id) {

      this.initData();
    }


  }

  ngOnInit() {


  }


  // ===================================================================================================================================
  ngAfterViewInit() {


  }

  initData() {


    this.notifierService.blockUiStart();
    this.missionService.getPlanningByMission(this.id).subscribe(
      (response: any) => {

        if (!response.erreur) {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.data.length;
          // this.dataSource.paginator = this.paginator;
          // this.dataSource.sort = this.sort;
        } else {
          this.notifierService.showNotificationEchec(response.erreur);
        }


      },
      (erreur) => {
        this.notifierService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notifierService.blockUiStop();
      },
      () => {
        this.notifierService.blockUiStop();
      }
    );


  }


}
