import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-candidature-modal',
  templateUrl: './candidature-modal.component.html',
  styleUrls: ['./candidature-modal.component.css']
})
export class CandidatureModalComponent implements OnInit {
  displayedColumns: string[] = [
    'position',
    'date',
    'vacation',
    'debut',
    'fin',
    'tarif',
    'statut'
  ];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  ngOnInit() {
    const candidatures = [];
    let cpt = 0;
    this.data.candidatures.forEach((item) => {
      cpt = cpt + 1;
      const candidature = {
        position: cpt,
        date: item.date.date,
        vacation: item.designation,
        debut: item.heureDebut.date,
        fin: item.heureFin.date,
        tarif: item.tarif,
        statut: item.statutCandidature,
      };
      candidatures.push(candidature);
    });
    if (candidatures.length > 0)
      this.dataSource = new MatTableDataSource(candidatures);
    this.dataSource.paginator = this.paginator;
  }

}
