import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatDialogRef} from '@angular/material';

import {NotificationService} from 'src/shared/services/notification.service';
import {FicheSpecialiteService} from 'src/shared/services/recruteur/fiche-specialite.service';
import {RolesService} from '../../../../../../../../services/roles.service';
import {UserService} from '../../../../../../../../services/user.service';

@Component({
  selector: 'app-modal-type-garde',
  templateUrl: './modal-type-garde.component.html',
  styleUrls: ['./modal-type-garde.component.css']
})
export class ModalTypeGardeComponent implements OnInit {
  length: number = 0;
  pageSizeOptions: number[] = [5, 10, 50, 100];
  displayedColumns = [
    'number',
    'Annee',
    'Libelle', 'Debut', 'Fin',
    'Tarif', 'Salaire Net', 'T.Reference'];
  pageSize: number = 12;
  currentUser: any;
  dataSource: MatTableDataSource<any[]> = new MatTableDataSource<any[]>();
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private ficheSpecialiteService: FicheSpecialiteService,
              private notificationService: NotificationService,
              public rolesService: RolesService,
              private userService: UserService,
              public dialogRef: MatDialogRef<ModalTypeGardeComponent>) {
    if (data) {

      this.ficheSpecialiteService.getTypeGardeByFicheSpecialiteId(data).subscribe(
        (response: any) => {
          this.notificationService.blockUiStart();
          if (!response.erreur) {
            this.dataSource = new MatTableDataSource(response.data);
            this.length = response.data.length;
            this.dataSource.paginator = this.paginator;
          } else {
            this.notificationService.showNotificationEchec(response.erreur);
          }
        },
        (erreur) => {
          this.notificationService.showNotificationEchec("erreur chargement!! veillez ressayer");
          this.notificationService.blockUiStop();
        },

        () => {
          this.notificationService.blockUiStop();
        }
      );
    }
  }


  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
  }
  roleCommercial() {
    return this.rolesService.isCommercial(this.currentUser.roles);
  }
  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }
}
