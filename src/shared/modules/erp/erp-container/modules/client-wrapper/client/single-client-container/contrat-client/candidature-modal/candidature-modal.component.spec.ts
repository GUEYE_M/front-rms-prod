import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatureModalComponent } from './candidature-modal.component';

describe('CandidatureModalComponent', () => {
  let component: CandidatureModalComponent;
  let fixture: ComponentFixture<CandidatureModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatureModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatureModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
