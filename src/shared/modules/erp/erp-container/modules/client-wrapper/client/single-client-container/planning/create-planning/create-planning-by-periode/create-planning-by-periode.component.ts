import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../../../../../../../services/notification.service';
import {UserService} from '../../../../../../../../../../services/user.service';
import {ClientService} from '../../../../../../../../../../services/recruteur/client.service';
import {MissionService} from '../../../../../../../../../../services/mission.service';
import {PlanningService} from '../../../../../../../../../../services/recruteur/planning.service';
import {TypeDeGardeService} from '../../../../../../../../../../services/recruteur/type-de-garde.service';
import * as moment from 'moment';
import {RolesService} from "../../../../../../../../../../services/roles.service";
import {NgbCalendar, NgbDate, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-planning-by-periode',
  templateUrl: './create-planning-by-periode.component.html',
  styleUrls: ['./create-planning-by-periode.component.css']
})
export class CreatePlanningByPeriodeComponent implements OnInit, OnChanges {
  hoveredDate: NgbDate;

  dateDebut: NgbDate;
  dateFin: NgbDate;

  showAfterJtc = false;
  jtc_array = [
    {label: 'Jour travaillé', value: 'Jt'},
    {label: 'Heures travaillée', value: 'Ht'},
  ];
  // castre le mois en format francais
  monthNames = [
    'Janvier', 'Fevrier', 'Mars',
    'Avril', 'Mai', 'Juin', 'Juillet',
    'Août', 'Septembre', 'Octombre',
    'Novembre', 'Decembre'
  ];
  statuts = [
    {statut: 'A Pourvoir'},
    {statut: 'Non Pourvue'},
    {statut: 'Perdue'},
    {statut: 'Autre'},
  ];
  monthNamesEn = [
    'January', 'February', 'March',
    'April', 'May', 'June', 'Jully',
    'August', 'September', 'October',
    'November', 'December'
  ];
  @Input() public bsRangeValue;
  @Input() public ficheSpecialiteDefaut;
  @Input() public ficheSpecialites;
  public typeDeGardeDefaut: any = [];
  public ficheSpecialiteDefautChoix = [];
  public typeDeGardeByFicheSpe: any = [];
  @Input() public typeDeGardes;
  @Input() public missions;
  enableValiderPeriode = false;
  public myForm: FormGroup;
  minDate = new Date();
  loaderValider = true;
  ficheSpecialitesTmp = [];
  actiyeN = true;
  currentUser: any;

  constructor(private typeGardeService: TypeDeGardeService,
              private planningService: PlanningService,
              private missionService: MissionService,
              private clientService: ClientService,
              private router: Router,
              public rolesService: RolesService,
              private userService: UserService,
              private notificationService: NotificationService,
              private activatedRoute: ActivatedRoute,
              private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,
              private formBuilder: FormBuilder) {
    this.dateDebut = calendar.getToday();
    this.dateFin = calendar.getNext(calendar.getToday(), 'd', 10);
  }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initForm();
    this.myForm.get('periode_Date').valueChanges.subscribe(
      (x) => {
        this.enableValiderPeriode = true;
      }
    );
    this.bsRangeValue = this.myForm.get('periode_Date').value;
    // this.typeGardeService.getTypeDeGarde().subscribe(
    //     (typedegardes) => {
    //         this.typeDeGardes = typedegardes;
    //     }
    // );
  }
  roleCommercial() {
    return this.rolesService.isCommercial(this.currentUser.roles);
  }

  onActiveBtnRadio(val) {
    if (val === 'y') {
      this.ficheSpecialitesTmp = this.ficheSpecialites;
      this.typeDeGardeDefaut = [];
      this.myForm.reset();
      this.actiyeN = true;
    } else if (val === 'n') {
      this.myForm.reset();
      this.actiyeN = false;
    }
  }

  initForm() {
    this.myForm = this.formBuilder.group({
      periode_Date: [{startDate: '', endDate: '' }, Validators.required],
      fiche_specialiteForm: [[], Validators.required],
      type_de_gardeForm: ['', Validators.required],
      statut: [this.statuts[0].statut, Validators.required],
      qualif: ['', Validators.required],
      activerMission: [false, Validators.required],
      jtc: ['', Validators.required],
      majorer: [true, Validators.required],
      auteur: [this.userService.getCurrentUser().username, Validators.required],
      missionForm: ['']
    });
  }

  getTypeDeGarde() {
    // this.typeGardeService.getTypeDeGarde().subscribe(
    //     (typedegardes) => {
    //         this.typeDeGardes = typedegardes;
    //     }
    // );
  }

  choiceMission() {
    const monthIndex = (this.monthNamesEn.indexOf(this.myForm.get('missionForm').value.mois));
    const annee = +this.myForm.get('missionForm').value.annee;
    this.minDate = new Date(annee, monthIndex);
    this.ficheSpecialitesTmp = [];
    const ficheSpecialiteId = this.myForm.get('missionForm').value.fiche_specialite_id;
    this.typeDeGardeDefaut = [];
    this.ficheSpecialiteDefautChoix = [];
    this.ficheSpecialites.filter((item) => {
      if (item.id === ficheSpecialiteId) {
        this.ficheSpecialitesTmp.push(item);
      }
    });
  }
  // on recupere les types de garde de la fiche de specialite suivant le Jtc
  choiceFicheSpecialite() {
    const annee = this.dateDebut.year;
    this.typeDeGardeByFicheSpe = [];
    const ficheSpecialite = this.myForm.get('fiche_specialiteForm').value;
    let typeGarde = ficheSpecialite.typeGardes;
    this.typeDeGardeDefaut = [];
    typeGarde = typeGarde.filter((x) => {
      if ((x.type_de_garde.toLowerCase().includes(this.myForm.get('jtc').value.toLowerCase())) && (+x.annee === annee)) {
        return x;
      }
    });
    typeGarde = typeGarde.map((x) => {
      x.qualifs = ficheSpecialite.qualifications;
      return x;
    });
    typeGarde.nomspecialite = ficheSpecialite.nomspecialite;
    typeGarde.fiche_specialite_id = ficheSpecialite.fiche_specialite_id;
    typeGarde.id = ficheSpecialite.fiche_specialite_id;
    this.typeDeGardeByFicheSpe = typeGarde;
  }

  getChoiceTypeDeGarde() {
    const id_client = this.activatedRoute.snapshot.params.id;
    const i = this.myForm.get('type_de_gardeForm').value;
    this.typeDeGardeDefaut[0] = this.typeDeGardeByFicheSpe[i];
    this.typeDeGardeDefaut[0].auteur = this.userService.getCurrentUser().username;
    this.typeDeGardeDefaut[0].client_id = id_client;
    this.typeDeGardeDefaut[0].nomspecialite = this.typeDeGardeByFicheSpe.nomspecialite;
    this.typeDeGardeDefaut[0].fiche_specialite_id = this.typeDeGardeByFicheSpe.fiche_specialite_id;
    this.typeDeGardeDefaut[0].jtc = this.myForm.get('jtc').value;
    this.typeDeGardeDefaut[0].activerMission = this.myForm.get('activerMission').value;
    this.myForm.patchValue({
      qualif: this.typeDeGardeDefaut[0].qualifs[0].qualif,
    });
    // this.typeDeGardeDefaut[indexSelect]['datePlan'] = (this.listesVacations[indexSelect]) ;
  }
  changeFiche(){
    this.myForm.patchValue({
      fiche_specialiteForm: [],
      type_de_gardeForm: [],
    });
    if(this.typeDeGardeDefaut[0])
      this.typeDeGardeDefaut[0].majorer = this.myForm.get('majorer').value;
  }
  enabledMission() {
    if (this.typeDeGardeDefaut[0]) {
      this.typeDeGardeDefaut[0].activerMission = this.myForm.get('activerMission').value;
      this.typeDeGardeDefaut[0].jtc = this.myForm.get('jtc').value;
      this.typeDeGardeDefaut[0].majorer = this.myForm.get('majorer').value;
    }
  }
  // choisir le Jtc a appliquer sur la mission
  showJtc() {
    this.showAfterJtc = true;
    this.typeDeGardeDefaut = [];
    this.typeDeGardeByFicheSpe = [];
    this.myForm.patchValue(
      {
        fiche_specialiteForm: []
      }
    );
  }
  onValiderPlanningByPeriode() {
    const monthDebut: any  = this.dateDebut.month.toString().length === 1 ? "0"+this.dateDebut.month.toString() : this.dateDebut.month;
    const monthFin: any  = this.dateFin.month.toString().length === 1 ? "0"+this.dateFin.month.toString() : this.dateFin.month;
    const dateDebut: any = ""+this.dateDebut.day+"-"+monthDebut+"-"+this.dateDebut.year;
    const dateFin: any = ""+this.dateFin.day+"-"+monthFin+"-"+this.dateFin.year;
    this.notificationService.blockUiStart();
    moment.locale('fr');
    moment.locale('fr-Fr');
    let date_fr = this.moisEnFrancais(this.dateDebut.month);
    this.typeDeGardeDefaut[1] = Object.values(this.myForm.get('periode_Date').value);
    if (this.myForm.get('missionForm').value) {
      this.typeDeGardeDefaut[2] = this.myForm.get('missionForm').value.reference;
    }
    this.typeDeGardeDefaut[0]['mois_lettre'] = date_fr;
    this.typeDeGardeDefaut[0]['majorer'] = this.myForm.get('majorer').value;
    this.typeDeGardeDefaut[0]['statut'] = this.myForm.get('statut').value;
    this.typeDeGardeDefaut[0]['qualif'] = this.myForm.get('qualif').value;
    const auteur = this.userService.getCurrentUser().username;
    this.typeDeGardeDefaut[0]['auteur'] = auteur;
    this.typeDeGardeDefaut[1] = [];
    this.typeDeGardeDefaut[1].push(dateDebut);
    this.typeDeGardeDefaut[1].push(dateFin);

    this.planningService.createMissionByPeriode(this.typeDeGardeDefaut).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.planningService.showNewCreate = false;
          this.planningService.hiddenListeMissions = true;
          this.planningService.onResetPlanning();
          this.notificationService.blockUiStop();
          this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
        } else {
          const dataFilter = {
            offset: 0,
            limit: 5,
            filter: null,
            id_client: this.clientService.idClient.getValue()
          };
          this.clientService.getAllMissionByClient(dataFilter).subscribe(
            (data_response: any) => {
              this.planningService.showNewCreate = false;
              this.planningService.hiddenListeMissions = true;
              this.planningService.onResetPlanning();
              const listeMisions = data_response.data;
              this.clientService.allmissionsForClientSubject.next(data_response);
              this.notificationService.blockUiStop();
            });
          this.notificationService.showNotificationSuccessCopie('bottomRight', 'La mission a été enregistrée avec success !');
        }
      },
      (error) => {
        this.planningService.showNewCreate = false;
        this.planningService.hiddenListeMissions = true;
        this.planningService.onResetPlanning();
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchecCopie('topCenter', error.error.message);
      }
    );
  }

  ngOnChanges() {
    this.ficheSpecialitesTmp = this.ficheSpecialites;
  }

  // fonction pour le ngDatepiker
  onDateSelection(date: NgbDate) {
    if (!this.dateDebut && !this.dateFin) {
      this.dateDebut = date;
    } else if (this.dateDebut && !this.dateFin && date.after(this.dateDebut)) {
      this.dateFin = date;
    } else {
      this.dateFin = null;
      this.dateDebut = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.dateDebut && !this.dateFin && this.hoveredDate && date.after(this.dateDebut) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.dateDebut) && date.before(this.dateFin);
  }

  isRange(date: NgbDate) {
    return date.equals(this.dateDebut) || date.equals(this.dateFin) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  moisEnFrancais(index){
    const arraryDate =  [
        'janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
    ];

    return arraryDate[(+index) - 1];
  }

}
