import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../../../../../shared/shared.module';
import {ListeMissionsRouting} from './liste-missions.routing';
import {ListeMissionsComponent} from './liste-missions/liste-missions.component';
import {ModalPlanningMissionComponent} from '../modal-client/modal-planning-mission/modal-planning-mission.component';
import {ModalMachingDispoComponent} from '../modal-client/modal-maching-dispo/modal-maching-dispo.component';


@NgModule({
  declarations: [
    ListeMissionsComponent,
    ModalPlanningMissionComponent,
    ModalMachingDispoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ListeMissionsRouting
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class ListeMissionsModule {
}
