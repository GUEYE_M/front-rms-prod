import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../../shared/shared.module';
import { ClientRouting } from './client.routing';
import { ClientsInactifsComponent } from './clients-inactifs/clients-inactifs.component';
import { ModalMailFichePosteComponent } from './modal-client/modal-mail-fiche-poste/modal-mail-fiche-poste.component';

import { NewClientModalComponent } from './modal-client/new-client-modal/new-client-modal.component';
import { SingleClientComponent } from './single-client-container/single-client/single-client.component';
import { FicheClientComponent } from './single-client-container/fiche-client/fiche-client.component';
import { CreateFicheClientComponent } from './single-client-container/fiche-client/create-fiche-client/create-fiche-client.component';
import { PlanningComponent } from './single-client-container/planning/planning.component';
import { CreatePlanningComponent } from './single-client-container/planning/create-planning/create-planning.component';
import { GestionPlanningComponent } from './single-client-container/planning/gestion-planning/gestion-planning.component';
import { CreatePlanningByDateComponent } from './single-client-container/planning/create-planning/create-planning-by-date/create-planning-by-date.component';
import { CreatePlanningByPeriodeComponent } from './single-client-container/planning/create-planning/create-planning-by-periode/create-planning-by-periode.component';
import { CandidaturesForClientComponent } from './single-client-container/candidatures-for-client/candidatures-for-client.component';
import { ProspectionInterimComponent } from './single-client-container/prospection-interim/prospection-interim.component';
import { ContratClientComponent } from './single-client-container/contrat-client/contrat-client.component';

import { ModalTypeGardeComponent } from './modal-client/modal-type-garde/modal-type-garde.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { InterimModalComponent } from './single-client-container/contrat-client/interim-modal/interim-modal.component';
import { CandidatureModalComponent } from './single-client-container/contrat-client/candidature-modal/candidature-modal.component';
import { StatistiqueComponent } from './single-client-container/single-client/statistique/statistique.component';
import { FusionChartsModule } from "angular-fusioncharts";
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);


@NgModule({
  declarations: [
    ClientsInactifsComponent,
    ModalMailFichePosteComponent,
    NewClientModalComponent,
    SingleClientComponent,
    FicheClientComponent,
    CreateFicheClientComponent,
    PlanningComponent,
    CreatePlanningComponent,
    GestionPlanningComponent,
    CreatePlanningByDateComponent,
    CreatePlanningByPeriodeComponent,
    CandidaturesForClientComponent,
    ProspectionInterimComponent,
    ContratClientComponent,
    InterimModalComponent,
    CandidatureModalComponent,
    StatistiqueComponent,



  ],
  imports: [
    CommonModule,
    SharedModule,
    ClientRouting,
    FusionChartsModule
  ], schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ], providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
  ],
  entryComponents: [
    ModalMailFichePosteComponent,


  ]
})

export class ClientModule {
}
