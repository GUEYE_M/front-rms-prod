import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidaturesForClientComponent } from './candidatures-for-client.component';

describe('CandidaturesForClientComponent', () => {
  let component: CandidaturesForClientComponent;
  let fixture: ComponentFixture<CandidaturesForClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidaturesForClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidaturesForClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
