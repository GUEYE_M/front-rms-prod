import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ListRecruteurModalComponent } from '../modal-client/list-recruteur-modal/list-recruteur-modal.component';
import { map, startWith, tap } from 'rxjs/operators';
import { Client } from '../../../../../../../models/recruteur/Client.model';
import { NotificationService } from '../../../../../../../services/notification.service';
import { ApiVilleService } from '../../../../../../../services/api_ville_service';
import { RolesService } from '../../../../../../../services/roles.service';
import { UserService } from '../../../../../../../services/user.service';
import { RecruteurService } from '../../../../../../../services/recruteur/recruteur.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GestionnaireInterneService } from '../../../../../../../services/gestionnaire interne/gestionnaireInterne.service';
import { ClientService } from '../../../../../../../services/recruteur/client.service';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { Recruteur } from '../../../../../../../models/recruteur/Recruteur.model';
import { Commune } from '../../../../../../../models/commune.model';
import { Departement } from '../../../../../../../models/departement.model';
import { ClientMajorationService } from 'src/shared/services/recruteur/client-majoration.service';
import { PldService } from '../../../../../../../services/pld.service';
import { UtilsService } from '../../../../../../../services/utils.service';
import { AddIdPldManuelComponent } from '../../../missions-interimaires/add-id-pld-manuel/add-id-pld-manuel.component';
@Component({
	selector: 'app-edit-liste-client',
	templateUrl: './edit-liste-client.component.html',
	styleUrls: ['./edit-liste-client.component.css']
})
export class EditListeClientComponent implements OnInit {
	clientForm: FormGroup;
	currentUser: any;
	viewlist_modale_recruteur = false;
	codedept: string;
	edit: boolean = false;
	departements: Departement[];
	inputcommune: boolean = true;
	communes: any[];
	displayName: string;
	listrecruteur: Recruteur[];
	recruteurs = [];
	gestionnaires_internes: any;
	groupe_majorations: any;
	isViewFormRecruteur: BehaviorSubject<boolean> = new BehaviorSubject(false);
	filteredCommune: Observable<any[]>;
	client: Client;
	response: any;
	idClient: number;
	dialogRefAddidPldContrat: MatDialogRef<AddIdPldManuelComponent>;
	actif_champ: boolean = true;

	constructor(
		private fb: FormBuilder,
		private clientService: ClientService,
		private giService: GestionnaireInterneService,
		private router: Router,
		private route: ActivatedRoute,
		private recruteurService: RecruteurService,
		private userService: UserService,
		private pldService: PldService,
		private utilsService: UtilsService,
		public roleService: RolesService,
		private apiVilleService: ApiVilleService,
		private dialog: MatDialog,
		private notificationService: NotificationService,
		private clientMajorationService: ClientMajorationService,
		private dialogRef: MatDialogRef<EditListeClientComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		if (this.data.id) {
			this.displayName = "Mise à jour de l'établissement de santé";
			this.notificationService.blockUiStart();
			this.idClient = +this.data.id;
			this.clientService.getRecruteurForClient(this.idClient).subscribe(
				(response) => {
					this.sendClientToPLD(response[0].idPld);
					this.notificationService.blockUiStart();
					this.recruteurs = response[0].recruteur;
					// (this.recruteurs);
					this.initform(response[0].client);
					this.response = response[0];

					this.notificationService.blockUiStop();
					this.edit = true;
					this.viewlist_modale_recruteur = true;
				},
				() => (erreur) => {
					this.notificationService.showNotificationEchec(
						'errur, echec chargement recruteur veillez ressayer'
					);
					this.notificationService.blockUiStop();
				},
				() => {
					this.notificationService.blockUiStop();
				}
			);
		} else {
			this.displayName = 'Ajouter un nouvel etablissement';
			this.initform();
		}
	}

	ngOnInit() {
		this.currentUser = this.userService.getCurrentUser();
		this.giService.list().subscribe((response: any) => {
			this.gestionnaires_internes = response;
		});

		this.clientMajorationService.list().subscribe((response: any) => {
			this.groupe_majorations = response.data;
		});
	}

	initform(
		client = {
			nom_etablissement: '',
			raison_sociale: '',
			type: '',
			tel: '',
			fax: '',
			siret: '',
			site_web: '',
			email: '',
			remarques: '',
			hopitalConnecte: false,
			majoration: null,
			taux_tva: null,
			coefficient_de_commission: null,
			numero_de_tva: null,
			rib: null,
			dateEnreg: null,
			mode_de_collaboration: '',
			auteur: '',
			periodesaison: '',
			adresse: '',
			complement_adresse: '',
			cp: '',
			ville: '',
			gestionnaire: '',
			client_majoration: '',
			region: '',
			departement: '',
			cible: false
		}
	) {
		this.clientForm = this.fb.group({
			id: [],
			enable: [],
			nom_etablissement: [client.nom_etablissement, Validators.required],
			raison_sociale: [client.raison_sociale, Validators.required],
			type: [client.type, Validators.required],
			tel: [client.tel, Validators.required],
			fax: [client.fax],
			email: [client.email, [Validators.required, Validators.email]],
			siret: [client.siret],
			site_web: [client.site_web],
			remarques: [client.remarques],
			hopitalConnecte: [client.hopitalConnecte],
			majoration: [client.majoration],
			taux_tva: [client.taux_tva],
			coefficient_de_commission: [client.coefficient_de_commission],
			numero_de_tva: [client.numero_de_tva],
			rib: [client.rib],
			mode_de_collaboration: [client.mode_de_collaboration],
			auteur: [],
			periodesaison: [client.periodesaison],
			adresse: [client.adresse, Validators.required],
			complement_adresse: [client.complement_adresse],
			cp: [client.cp, Validators.required],
			ville: [client.ville, Validators.required],
			region: [client.region],
			gestionnaire: [client.gestionnaire],
			client_majoration: [client.client_majoration],
			departement: [client.departement],
			numeroDepartement: [],
			cible: [client.cible],
			recruteurs: this.fb.array(
				this.recruteurs.map((recruteur) =>
					this.fb.group({
						id: recruteur.id,
						fonction: recruteur.fonction,
						email: recruteur.email,
						nom: recruteur.nom,
						prenom: recruteur.prenom
					})
				)
			),
			deleteRecruteur: this.fb.array([])
		});
	}

	onCommune(commune: string) {
		this.apiVilleService.findbyCommune(commune).subscribe((response: any) => {
			if (response) {
				this.communes = response.data;
				console.log(this.communes)

				this.filteredCommune = this.clientForm
					.get('ville')
					.valueChanges.pipe(
						startWith(''),
						map((value) =>

							this.communes.filter((commune) => commune.properties.label.toLowerCase().includes(value))
						)
					);
			}
		});
	}

	compareWithFunc(a, b) {
		a;
		if (a == b.id) {
			return b;
		}
	}
	compareWithFuncGest(a, b) { }

	cibleAction() {
		if (this.clientForm.get('cible').value) {
			this.actif_champ = false;
			this.clientForm.get('recruteurs').reset();
			(<FormArray>this.clientForm.get('recruteurs')).push(
				this.fb.group({
					id: 150,
					fonction: 'standart',
					email: 'cible@gmail.com',
					nom: 'standart',
					prenom: 'standart'
				})
			);
		} else {
			this.actif_champ = true;
		}
	}
	onblurCommune() {
		if (this.communes.length === 0) {
			this.clientForm.get('ville').reset();
			this.clientForm.get('cp').reset();
			// this.clientForm.get('adresse').get('ville').get('departement').reset();
			// this.clientForm.get('adresse').get('ville').get('region').reset();
			// this.clientForm.get('adresse').get('ville').get('codeRegion').reset();
			this.notificationService.showNotificationEchec('nom de commune incorecte');
		} else {


			this.communes.filter((c) => {
				//console.log(c)
				if (c.properties.postcode + "-" + c.properties.label === this.clientForm.get('ville').value) {
					let donne = c.properties.context.split(',');
					let dept = donne[0] + ' , ' + donne[1];
					this.clientForm.get('cp').setValue(c.properties.postcode);
					this.clientForm.get('region').setValue(donne[2]);
					this.clientForm.get('departement').setValue(dept);
					this.clientForm.get('ville').setValue(c.properties.label)
					this.clientForm.get('numeroDepartement').setValue(donne[0]);


					// this.clientForm.get('adresse').get('ville').get('departement').setValue(c.departement.nom)
					// this.clientForm.get('adresse').get('ville').get('region').setValue(c.region.nom)

					// this.clientForm.get('adresse').get('ville').get('codeRegion').setValue(c.region.code)
				}
			});
		}
	}

	ouvrirlistRecruteurModal() {
		this.notificationService.blockUiStart();
		this.recruteurService.allRecruteur().subscribe(
			(recruteur: any) => {
				if (recruteur.data.length > 0) {
					const listRecruteurModal = recruteur.data.filter(
						(item) => !this.clientForm.get('recruteurs').value.some((c) => c.id === item.id)
					);
					let data = {
						items: this.custumDataRecruteur(listRecruteurModal),
						length: recruteur.lenght
					};
					const dialogRef: MatDialogRef<
						ListRecruteurModalComponent
					> = this.dialog.open(ListRecruteurModalComponent, {
						width: '900px',
						height: '800px',
						data: data,
						panelClass: 'myapp-no-padding-dialog'
					});
					dialogRef.afterClosed().subscribe((resultat) => {
						if (resultat) {
							resultat.map((recruteur) => {
								(<FormArray>this.clientForm.get('recruteurs')).push(
									this.fb.group({
										id: recruteur.id,
										fonction: recruteur.fonction,
										email: recruteur.email,
										nom: recruteur.nom,
										prenom: recruteur.prenom
									})
								);
							});
						}
					});

					this.notificationService.blockUiStop();
				}
			},
			(error) => { },
			() => { }
		);
	}

	custumDataRecruteur(list: any) {
		let listsource: any[] = [];
		from(list)
			.pipe(
				map((recruteur: any) => {
					return {
						id: recruteur.id,
						fonction: recruteur.fonction,
						email: recruteur.user.email,
						civilite: recruteur.user.civilite,
						nom: recruteur.user.nom,
						prenom: recruteur.user.prenom,
						telephone: recruteur.user.telephone
					};
				}),
				tap((recruteur) => {
					listsource.push(recruteur);
				})
			)
			.subscribe();

		return listsource;
	}
	// this.notificationService.blockUiStart();
	// ('ok');
	retireRecruteur(index: number) {
		if (this.edit) {
			(<FormArray>this.clientForm.get('deleteRecruteur')).push(
				this.fb.group({
					id: (<FormArray>this.clientForm.get('recruteurs')).controls[index].value.id
				})
			);
		}
		(<FormArray>this.clientForm.get('recruteurs')).removeAt(index);
	}

	saveClient(profil_recruteur: boolean = false) {
		if (profil_recruteur) {
			(<FormArray>this.clientForm.get('recruteurs')).push(
				this.fb.group({
					id: this.currentUser.recruteur.id,
					fonction: this.currentUser.fonction,
					email: this.currentUser.email,
					nom: this.currentUser.nom,
					prenom: this.currentUser.prenom
				})
			);
		}

		this.notificationService.blockUiStart();
		if (this.edit) {
			this.clientForm.get('id').setValue(+this.data.id);
		}

		this.clientService.update(this.idClient, this.clientForm.value).subscribe(
			(response: any) => {
				this.notificationService.blockUiStop();
				if (response.erreur) {
					this.clientService.closemodale.next(false);
					this.notificationService.showNotificationEchec(response.erreur);
				} else {
					//     this.clientService.listClient.map(
					//       (t: any) => {
					//         if(t.id === this.data)
					//         {
					//           const index =  this.clientService.listClient.indexOf(t);
					//           this.clientService.listClient[index] = response.data;
					//         }
					//       }
					//     );
					//  this.clientService.emitClient();
					this.notificationService.showNotificationSuccess(response.success);
					location.reload();
					// this.clientService.closemodale.next(true);
					// ('ok')
				}
			},
			(erreur) => {
				this.notificationService.blockUiStop();

				this.notificationService.showNotificationEchec(
					'erreur  de connexion , contacter le service IT si le probleme persiste'
				);
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	enregistrerClient() {
		(<FormArray>this.clientForm.get('recruteurs')).push(
			this.fb.group({
				id: this.currentUser.recruteur.id,
				fonction: this.currentUser.fonction,
				email: this.currentUser.email,
				nom: this.currentUser.nom,
				prenom: this.currentUser.prenom
			})
		);

		this.clientService.create(this.clientForm.value).subscribe(
			(response: any) => {
				if (response.erreur) {
					this.clientService.closemodale.next(false);
					this.notificationService.showNotificationEchec(response.erreur);
				} else {
					this.clientService.listClient.push(response.data);
					this.clientService.emitClient();
					this.clientService.clientSubject.subscribe((next) => next);
					this.notificationService.showNotificationSuccess(response.success);
					this.clientService.closemodale.next(true);
				}
			},
			() => { },
			() => {
				this.notificationService.blockUiStop();
				this.dialog.closeAll();
				this.router.navigate(['/RMS-Admin/redirection-admin/', 'Etablissements']);
			}
		);
	}

	sendClientToPLD(idpld) {
		if (idpld == null) {
			this.notificationService.onConfirm('Voulez-vous enregistrer ce client sur PLD ? ');
			this.notificationService.dialogRef.afterClosed().subscribe((x) => {
				if (x) {
					this.notificationService.blockUiStart();
					this.pldService.addClient(this.idClient).subscribe(
						(resp) => {
							const resultat = JSON.parse(resp);
							if (resultat.Message === 'Ajout  avec succès.') {
								this.notificationService.showNotificationSuccessCopie(
									'bottomRight',
									'Le client est maintenant enregistré sur PLD !'
								);
								const clientUpdate = { id: this.idClient, idPld: resultat.ID };
								this.clientService.updateIdPld(clientUpdate).subscribe(
									(res) => {
										this.notificationService.blockUiStop();
										if (res === 'ok') {
											this.dialogRef.close(this.data);

											this.notificationService.showNotificationSuccessCopie(
												'bottomRight',
												'Le client est maintenant relié avec  Pld !'
											);
										} else {
											this.notificationService.showNotificationEchecCopie(
												'bottomRight',
												'Echec de la liaison du client avec PLD !'
											);
										}
									},
									(error) => {
										this.notificationService.showNotificationEchecCopie(
											'bottomRight',
											'Echec de la liaison du client avec PLD  !'
										);
									}
								);
							}
						},
						(error) => {
							this.notificationService.showNotificationEchecCopie('bottomRight', error.error.message);
							this.notificationService.blockUiStop();
						}
					);
				}
			});
		}
	}
	addIdPldContrat() {
		this.dialogRefAddidPldContrat = this.dialog.open(AddIdPldManuelComponent, {
			panelClass: 'myapp-no-padding-dialog',
			data: { id: this.data.id, user: 'client' },
			width: '20%',
			height: '30%'
		});
	}

	getValue(commune) {
		return commune.properties.postcode + "-" + commune.properties.label
	}
}
