import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheSpecialiteClientEditComponent } from './fiche-specialite-client-edit.component';

describe('FicheSpecialiteClientEditComponent', () => {
  let component: FicheSpecialiteClientEditComponent;
  let fixture: ComponentFixture<FicheSpecialiteClientEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheSpecialiteClientEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheSpecialiteClientEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
