import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TypeChartModel, DataModel, ChartModel } from 'src/shared/chart.model';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import { ClientService } from 'src/shared/services/recruteur/client.service';
import { NotificationService } from 'src/shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from 'src/shared/services/utils.service';
import { UserService } from 'src/shared/services/user.service';
import { RolesService } from 'src/shared/services/roles.service';

@Component({
	selector: 'app-statistique',
	templateUrl: './statistique.component.html',
	styleUrls: [ './statistique.component.css' ]
})
export class StatistiqueComponent implements OnInit {
	labelPosition: 'nbr' | 'ca' = 'nbr';
	checked = false;

	client: any;

	colorInterim = 'primary';
	modeInterim = 'determinate';
	valueInterim = 50;
	bufferValueInterim = 75;

	annees: any[];
	response: any = [];

	salaire: any = 0;
	heure_travailler: any = 0;
	nbr_mission: any = 0;
	nbr_specilaite: any = 0;
	currentUser: any;
	interim_id: any;

	barChart: any;
	pie3dChart: any;

	elements = [];
	data = {
		filter: null,
		annee: 2020,
		client: null
	};
	candiddatues = [];
	listSpecialites = [];

	displayedColumnsFilter = [
		{
			name: 'specialite',
			label: 'Specialite'
		},
		{
			name: 'annee',
			label: 'Annee'
		}
	];

	constructor(
		private specialiteService: SpecialiteService,
		private clientService: ClientService,
		private notificationService: NotificationService,
		private activateRoute: ActivatedRoute,
		private utiles: UtilsService,
		private userService: UserService,
		public roleService: RolesService
	) {}

	ngOnInit() {
		this.currentUser = this.userService.getCurrentUser();
		if (this.activateRoute.snapshot.params['id']) this.interim_id = this.activateRoute.snapshot.params['id'];
		else this.interim_id = this.currentUser.interim.id;

		this.annees = this.utiles.yearForGraph();

		this.data.client = +this.interim_id;

		this.getStatistique(this.data);

		this.getAllSpecialite();
	}

	getAllSpecialite() {
		this.specialiteService.list(null, true).subscribe((response: any) => {
			this.listSpecialites = response.data;
		});
	}
	newStat() {
		this.loadgrah(this.response);
	}

	roleCommercial() {
		return this.roleService.isCommercial(this.currentUser.roles);
	}

	getStatistique(data: any) {
		this.notificationService.blockUiStart();
		this.clientService.getStatistiqueTarifCh(data).subscribe((response: any) => {
			this.notificationService.blockUiStop();
			if (!response.erreur) {
				this.client = response.data_stat;
				this.response = response;
				this.loadgrah(response);
				//
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	convertInt(element) {
		let el = +element;

		return el;
	}

	loadgrah(response) {
		if (this.labelPosition === 'nbr') {
			this.pieChart(response.data.nbr);
			this.chartBar(response.data_mois_nbr);
		}
		if (this.labelPosition === 'ca') {
			this.pieChart(response.data.tarif);
			this.chartBar(response.data_mois_tarif);
		}
	}

	pieChart(donnee: any) {
		let data = new DataModel();
		let chartModel = new ChartModel();
		if (this.labelPosition === 'ca') {
			chartModel.caption = ' Total Tarif Commande';
			chartModel.numberprefix = '€';
			chartModel.yaxisname = 'Chiffre en euro';
		} else if (this.labelPosition === 'nbr') {
			chartModel.caption = 'Total nombre Commande';
			chartModel.numberprefix = '';
			chartModel.yaxisname = 'En Chiffre ';
		}

		chartModel.subcaption = 'Par statut';
		chartModel.theme = 'fusion';

		data.chart = chartModel;
		data.data = donnee;

		this.pie3dChart = new TypeChartModel('100%', 500, 'pie3d', 'json', data);
	}

	chartBar(donnee: any) {
		let data = new DataModel();

		if (this.labelPosition === 'ca') {
			data.chart = {
				caption: 'Evolution  tarif commande  pour chaque mois',
				subcaption: 'Par statut',
				numberprefix: '€',
				numbersuffix: '',
				plottooltext: '   <b>$dataValue</b>  commande $seriesName pour le mois de $label',
				theme: 'fusion'
			};
		} else {
			data.chart = {
				caption: 'Nombre   commande pour chaque mois',
				subcaption: 'Par statut',
				numberprefix: '',
				numbersuffix: '',
				plottooltext: '   <b>$dataValue</b>  commande $seriesName pour le mois de $label',
				theme: 'fusion'
			};
		}

		(data.categories = [
			{
				category: [
					{
						label: 'Janvier'
					},
					{
						label: 'Fevrier'
					},
					{
						label: 'Mars'
					},
					{
						label: 'Avril'
					},
					{
						label: 'Mai'
					},
					{
						label: 'Juin'
					},
					{
						label: 'Juillet'
					},
					{
						label: 'Aout'
					},
					{
						label: 'Septembre'
					},
					{
						label: 'Octobre'
					},
					{
						label: 'Novembre'
					},
					{
						label: 'Décembre'
					}
				]
			}
		]),
			(data.dataset = donnee);

		this.barChart = new TypeChartModel('100%', 500, 'mscolumn3d', 'json', data);
	}

	valutElements() {
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.data.annee = 2020;

			this.getStatistique(this.data);
		}
	}

	onSubmitFiltre(form: NgForm) {
		this.data.filter = form.value;

		if (form.controls['annee']) {
			this.data.annee = form.controls['annee'].value;
		}

		this.getStatistique(this.data);
	}
}
