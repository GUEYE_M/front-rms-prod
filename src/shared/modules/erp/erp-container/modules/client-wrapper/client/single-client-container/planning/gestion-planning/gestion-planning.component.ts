import { Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { UtilsService } from '../../../../../../../../../services/utils.service';
import { Form, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PlanningService } from '../../../../../../../../../services/recruteur/planning.service';
import { TypeDeGardeService } from '../../../../../../../../../services/recruteur/type-de-garde.service';
import { ClientService } from '../../../../../../../../../services/recruteur/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FicheSpecialiteService } from '../../../../../../../../../services/recruteur/fiche-specialite.service';
import { NotificationService } from '../../../../../../../../../services/notification.service';
import $ from 'jquery';
import * as moment from 'moment';
import { DialogConfirmationComponent } from '../../../../../../dialog-confirmation/dialog-confirmation.component';
import { RolesService } from '../../../../../../../../../services/roles.service';
import { UserService } from '../../../../../../../../../services/user.service';
import { NgbCalendar, NgbDate, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-gestion-planning',
	templateUrl: './gestion-planning.component.html',
	styleUrls: ['./gestion-planning.component.css']
})
export class GestionPlanningComponent implements OnInit, OnChanges, OnDestroy {
	formArrayPicker: FormGroup;
	qualifsData = [];

	itemGardeChoisie: any;
	@ViewChild('vacationsContent', { static: false })
	el: ElementRef;
	@ViewChild('parentContainer', { static: false })
	elContainer: ElementRef;
	supprimerDates = [];
	@ViewChild('vacationParent', { static: false })
	public vacationParent?: ElementRef[];
	@Input() public plannings: any = [];
	@Input() public hideListeMission: any;
	@Input() public showAlert: any;
	@Input() vacationParentArray = [];
	@Input()
	typeDeGardes?: any = [
		{
			typeDeGardes: {
				typeDeGardes: null
			}
		}
	];
	typeDeGardesTmp: any = [];
	@Input() public missionChoisie: any;
	typeDeGardeDefaut = [];
	data_intermediaire = [];
	@Input() typeDeGardeDefautAdd?: any = [];
	arrayForAddNewVacation: any = [];
	listesVacations: any = [];
	nmbreAjouter = 0;
	countNodeAddOnDate = 0;
	planning_id_add = 0;
	fs_choisi: any;
	supprimer_la_date = false;
	client_id = 0;
	p: number = 1;
	statuts = [{ statut: 'A Pourvoir' }, { statut: 'Non Pourvue' }, { statut: 'Perdue' }, { statut: 'Autre' }];
	boolNewPeriode = false;
	deleteVacation: any = [];
	deleteByDate: any = [];
	currentUser: any;
	public myFormNewPeriodeOnMission: FormGroup;
	// castre le mois en format francais
	monthNames = [
		'Janvier',
		'Février',
		'Mars',
		'Avril',
		'Mai',
		'Juin',
		'Juillet',
		'Août',
		'Septembre',
		'Octobre',
		'Novembre',
		'Décembre'
	];
	annees_moment = [2019, 2020, 2021, 2022, 2023, 2024, 2025];
	testI = 0;
	hoveredDate: NgbDate;

	dateDebut: NgbDate;
	dateFin: NgbDate;

	formGroup: FormGroup;
	skills;

	constructor(
		private renderer: Renderer2,
		public planningService: PlanningService,
		private typeGardeService: TypeDeGardeService,
		private clientService: ClientService,
		private router: Router,
		public rolesService: RolesService,
		private userService: UserService,
		private activatedRoute: ActivatedRoute,
		private formBuilder: FormBuilder,
		public dialog: MatDialog,
		private utilService: UtilsService,
		private ficheSpecialiteService: FicheSpecialiteService,
		public notificationService: NotificationService,
		private calendar: NgbCalendar,
		public formatter: NgbDateParserFormatter
	) {
		this.dateDebut = calendar.getToday();
		this.dateFin = calendar.getNext(calendar.getToday(), 'd', 10);
	}

	ngOnInit() {
		this.initFormData();
		this.initFormGroup();
		this.currentUser = this.userService.getCurrentUser();
		this.client_id = this.activatedRoute.snapshot.params.id;
		// this.typeGardeService.getTypeDeGarde().subscribe(
		//     (typedegardes) => {
		//         const tmp = [];
		//         typedegardes.filter((item: any) => {
		//             if ( item.fiche_specialite_id === this.missionChoisie.fiche_specialite_id){
		//                 tmp.push(item);
		//             }
		//         });
		//         this.typeDeGardes = tmp;
		//     }
		// );
		this.planningService.newDatesOnMissionViaDatePikerSubject.subscribe((x: []) => {
			if (x.length > 0) {
				this.addGroupData(x);
			} else {
				this.initFormData();
			}
		});
	}

	// new
	initFormGroup() {
		this.formGroup = this.formBuilder.group({
			published: true,
			vacations: this.formBuilder.array([])
		});

		this.skills = new FormArray([]);
	}

	addSkill() {
		const group = new FormGroup({
			vacation: new FormControl(''),
			salaire: new FormControl(''),
			qualifs: new FormControl(''),
			tarif: new FormControl('')
		});

		this.skills.push(group);
	}

	removeSkill(index: number) {
		this.skills.removeAt(index);
	}

	addCreds() {
		const creds = this.formGroup.controls.vacations as FormArray;
		creds.push(
			this.formBuilder.group({
				vacation: '',
				tarif: '',
				salaire: '',
				qualifs: ''
			})
		);
	}

	// fin
	setMonthOnDatePriode() {
		this.initForm();
		this.myFormNewPeriodeOnMission.patchValue({
			qualif: this.typeDeGardes[0].qualifs[0].qualif,
			statut: this.statuts[0].statut
		});
	}

	showOn() { }

	roleCommercial() {
		return this.rolesService.isCommercial(this.currentUser.roles);
	}

	initForm() {
		let month = this.monthNames.indexOf(this.missionChoisie.mois);
		let tmp_date = '01-' + this.missionChoisie.mois + '-' + this.missionChoisie.annee;
		this.myFormNewPeriodeOnMission = this.formBuilder.group({
			type_de_gardeForm: ['', Validators.required],
			qualif: [''],
			statut: [''],
			missionForm: [''],
			date: [{ start: null, end: null }]
		});
	}

	// monthNamesEn = [
	//   "January", "Febuary", "March",
	//   "April", "May", "June", "Jully",
	//   "August", "September", "October",
	//   "November", "December"
	// ];

	//afficher le tableau contenu dans le jumbotron au survol de la souris
	onMouseEntrer(index) {
		$('.hidden' + index).show();
	}

	//Cacher le tableau contenu dans le jumbotron quand il perd le focus de la souris
	onMouseLeave(index) {
		$('.hidden' + index).hide();
	}

	replaceSpanTarif(index, item) {
		if (item.statut !== 'Pourvue') {
			$('.showTarif' + index).show();
			$('.hideTarif' + index).hide();
		} else {
			// this.notificationService.showNotificationEchecCopie(
			// 	'topCenter',
			// 	"Impossible d'apporter de modification, le planning est pourvue"
			// );
			$('.showTarif' + index).show();
			$('.hideTarif' + index).hide();
		}
	}

	replaceInputTarif(index, item) {
		if (item.statut !== 'Pourvue') {
			$('.showTarif' + index).hide();
			$('.hideTarif' + index).show();
		} else {
			// this.notificationService.showNotificationEchecCopie(
			// 	'topCenter',
			// 	"Impossible d'apporter de modification, le planning est pourvue"
			// );
			$('.showTarif' + index).hide();
			$('.hideTarif' + index).show();
		}
	}

	replaceInputSalaireNet(index, item) {
		$('.showSalaireNet' + index).hide();
		$('.hideSalaireNet' + index).show();
	}

	replaceSpanSalaireNet(index, item) {
		if (item.statut !== 'Pourvue') {
			$('.showSalaireNet' + index).show();
			$('.hideSalaireNet' + index).hide();
		} else {
			// this.notificationService.showNotificationEchecCopie(
			// 	'topCenter',
			// 	"Impossible d'apporter de modification, le planning est pourvue"
			// );
			$('.showSalaireNet' + index).show();
			$('.hideSalaireNet' + index).hide();
		}
	}

	replaceStatutSelect(statut, index) {
		this.data_intermediaire[index].statut = statut;
		$('.showStatut' + index).hide();
		$('.hideStatut' + index).show();
	}

	replaceStatut(index) {
		$('.showStatut' + index).show();
		$('.hideStatut' + index).hide();
	}

	replaceStatutPourvue(index) {
		this.notificationService.showNotificationEchecCopie(
			'topCenter',
			"Impossible d'apporter de modification, le planning est pourvue"
		);
	}

	onAddVacation(index, countNodeAddOnDate) {
		this.arrayForAddNewVacation.push({ id: index, value: countNodeAddOnDate });
		this.getJTC();
	}

	getTypeDeGarde() {
		// (this.missionChoisie);
	}

	onAnnulerVacationInToNode(vacationsNode, indexVacation) {
		this.typeDeGardeDefaut.splice(indexVacation, 1);
		vacationsNode.remove(indexVacation);
	}

	react_typeDeGardes() {
		// (this.typeDeGardes);
		this.ficheSpecialiteService.getFicheSpecialiteDistinct(this.client_id).subscribe((ficheSpecialites: any) => {
			ficheSpecialites.forEach((element) => {
				if (+element.id === this.missionChoisie.fiche_specialite_id) {
					this.typeDeGardes = element.typeGardes;
				}
			});
		});
	}

	getJTC() {
		if (this.typeDeGardes.length > 0) {
			this.typeDeGardes = this.typeDeGardes.filter((x) => {
				if (x.type_de_garde.toLowerCase().includes(this.missionChoisie.jtc.toLowerCase())) {
					return x;
				}
			});
		}
	}

	getChoiceTypeDeGarde(index, indexVacation, itemDate) {
		const typeGarde = this.typeDeGardes.slice()[index];
		let type_garde_set: any;
		type_garde_set = {
			auteur: typeGarde.auteur,
			client_id: typeGarde.client_id,
			fiche_specialite_id: typeGarde.fiche_specialite_id,
			heure_debut: typeGarde.heure_debut,
			heure_fin: typeGarde.heure_fin,
			jtc: typeGarde.jtc,
			nomspecialite: typeGarde.nomspecialite,
			salaire_net: typeGarde.salaire_net,
			tarif: typeGarde.tarif,
			qualif: typeGarde.qualifs[0].qualif,
			qualifs: typeGarde.qualifs,
			statuts: this.statuts,
			statut: 'A Pourvoir',
			typeDeGarde_id: typeGarde.typeDeGarde_id,
			type_de_garde: typeGarde.type_de_garde
		};
		this.skills.at(indexVacation).patchValue({
			tarif: typeGarde.tarif,
			salaire: typeGarde.salaire_net,
			qualifs: typeGarde.qualifs[0].qualif
		});
		this.typeDeGardeDefaut[indexVacation] = [];
		this.typeDeGardeDefaut[indexVacation]['typeGardes'] = type_garde_set;
		this.typeDeGardeDefaut[indexVacation]['datePlan'] = itemDate.format('D-MM-Y');
		this.typeDeGardeDefaut[indexVacation]['mission_id'] = this.missionChoisie.mission_id;
	}

	getChoiceTypeDeGardeOnDate(value_garde, indexVacation, itemDate) {
		let typeGarde = this.typeDeGardes;
		typeGarde = typeGarde.filter((x) => {
			if (x.type_de_garde.includes(this.itemGardeChoisie.type_de_garde)) {
				return x;
			}
		});

		let type_garde_set: any;
		type_garde_set = {
			auteur: this.itemGardeChoisie.auteur,
			client_id: this.itemGardeChoisie.client_id,
			fiche_specialite_id: this.itemGardeChoisie.fiche_specialite_id,
			heure_debut: this.itemGardeChoisie.heure_debut,
			heure_fin: this.itemGardeChoisie.heure_fin,
			jtc: this.itemGardeChoisie.jtc,
			nomspecialite: this.itemGardeChoisie.nomspecialite,
			salaire_net: this.itemGardeChoisie.salaire_net,
			tarif: this.itemGardeChoisie.tarif,
			qualif: this.itemGardeChoisie.qualifs[0].qualif,
			qualifs: this.itemGardeChoisie.qualifs,
			statuts: this.statuts,
			statut: 'A Pourvoir',
			typeDeGarde_id: this.itemGardeChoisie.typeDeGarde_id,
			type_de_garde: this.itemGardeChoisie.type_de_garde
		};
		// this.typeDeGardeDefaut[indexVacation] = [];
		// this.typeDeGardeDefaut[indexVacation]['typeGardes'] = type_garde_set;
		// this.typeDeGardeDefaut[indexVacation]['datePlan'] = itemDate.format('Y-M-D');
		// this.typeDeGardeDefaut[indexVacation]['mission_id'] = this.missionChoisie.mission_id;
		//
		//
		//
		//
		// (this.missionChoisie);
		this.typeDeGardeDefautAdd[indexVacation] = [];
		this.typeDeGardeDefautAdd[indexVacation]['typeGardes'] = type_garde_set;
		this.typeDeGardeDefautAdd[indexVacation]['datePlan'] = itemDate;
		this.typeDeGardeDefautAdd[indexVacation]['mission_id'] = this.missionChoisie.mission_id;
	}

	choiceQualif(qualif, itemNodeNew) {
		if (this.typeDeGardeDefautAdd[itemNodeNew]) {
			this.typeDeGardeDefautAdd[itemNodeNew].typeGardes.qualif = qualif;
		}
	}

	choiceStatut(statut, itemNodeNew) {
		this.typeDeGardeDefautAdd[itemNodeNew].typeGardes.statut = statut;
	}

	choiceStatutNewOnMission(statut, itemNodeNew) {
		this.typeDeGardeDefaut[itemNodeNew].typeGardes.statut = statut;
	}

	// Effectue toute la mise a jour sur la page gestion du planning en une action
	//en gerant les alertes et le mail a envoyer au ch
	onUpdate(mail: boolean = false) {
		this.notificationService.onConfirm();
		this.notificationService.dialogRef.afterClosed().subscribe((result) => {
			if (result) {
				const id_client = this.clientService.idClient.getValue();
				const auteur = this.userService.getCurrentUser().username;
				const id_mission = this.missionChoisie.mission_id;
				this.planningService.AllplanningsForMissionChoiceSubject.next([]);
				this.notificationService.blockUI.start();
				const tmp: any = [];
				this.typeDeGardeDefaut = this.typeDeGardeDefaut.concat(this.typeDeGardeDefautAdd);
				const data = [];
				this.typeDeGardeDefaut.forEach((element) => {
					data.push(Object.assign({}, element));
				});
				this.typeDeGardeDefaut = data;
				tmp.push(this.typeDeGardeDefaut);
				//pour supprimer un ensemble de vacation suivant une date
				const data_deleteByDates = [];
				this.deleteByDate.forEach((element) => {
					data_deleteByDates.push(Object.assign({}, element));
				});
				tmp.push(this.deleteByDate);
				// pour supprimer des vacations suivant leurs dates respective
				tmp.push(this.deleteVacation);
				// l'ensemble de vacations a modifier
				this.plannings.forEach((element) => {
					element.forEach((elementTmp) => {
						tmp.push(elementTmp);
					});
				});
				this.planningService.update(tmp, id_client, auteur, mail, id_mission).subscribe(
					(next: any) => {
						const dataFilter = {
							offset: 0,
							limit: 500,
							filter: null,
							id_client: this.clientService.idClient.getValue()
						};
						if (next.erreur) {
							this.notificationService.showNotificationEchec(next.erreur);
							this.onFnGetListeMission(dataFilter);
						} else {
							this.onFnGetListeMission(dataFilter);
							this.notificationService.showNotificationSuccess(next.success);
						}
					},
					(error) => {
						const dataFilter = {
							offset: 0,
							limit: 500,
							filter: null,
							id_client: this.clientService.idClient.getValue()
						};
						this.onFnGetListeMission(dataFilter);
						this.notificationService.showNotificationSuccess(error);
					}
				);
			} else {
				const dataFilter = {
					offset: 0,
					limit: 5,
					filter: null,
					id_client: this.clientService.idClient.getValue()
				};
				this.onFnGetListeMission(dataFilter);
			}
		});
	}

	onAddElOnDataIntermediaire() {
		this.data_intermediaire = this.data_intermediaire.map((x) => {
			if (typeof x.del === 'undefined') {
				x.del = true;
			}
			return x;
		});
	}

	onFnGetListeMission(dataFilter) {
		this.clientService.getAllMissionByClient(dataFilter).subscribe((data_response: any) => {
			const listeMisions = data_response.data;
			this.clientService.allmissionsForClientSubject.next(data_response);
			listeMisions.forEach((element) => {
				if (element.mission_id === this.missionChoisie.mission_id) {
					this.planningService.getReformatPlanningForMission(element.plannings);
					this.clientService.allmissionsForClientSubject.next(data_response);
					this.planningService.showCreateOnMissionOrList = false;
					// on remet a vide tous les tableaux utiliser
					this.deleteVacation = [];
					this.typeDeGardeDefaut = [];
					this.typeDeGardeDefautAdd = [];
					this.listesVacations = [];
					this.planningService.newDatesOnMissionViaDatePiker = [];
					this.arrayForAddNewVacation = [];
					this.countNodeAddOnDate = 0;
					this.deleteByDate = [];
					this.notificationService.blockUiStop();
				} else {
					this.notificationService.blockUiStop();
				}
			});
		});
	}

	onDeleteVacation(vacationsContent, indexDatePlanning, i, itemPlanning) {
		// selection des differentes tr a supprimer
		this.elContainer.nativeElement.querySelector('#tr' + i + '' + indexDatePlanning).style.display = 'none';
		this.renderer.removeChild(
			this.elContainer.nativeElement,
			this.elContainer.nativeElement.querySelector('#tr' + i + '' + indexDatePlanning)
		);
		this.deleteVacation.push(itemPlanning.planning_id);
		this.data_intermediaire[indexDatePlanning].del = false;
	}

	onDeleteByDate(vacationParent, i, date) {
		vacationParent.panel._body.nativeElement.style.display = 'none';
		// vacationParent.panel._hideToggle = true;
		// this.vacationParentArray[i] = (vacationParent);
		// this.vacationParentArray[i].style.display = 'none';
		// (date);
		this.deleteByDate[0] = this.client_id;
		this.deleteByDate.push(date[0].dateSup);
	}

	//permet de recuperer la liste des types de gardes au demarage
	//tout en verifiant si elle nexiste sinon la recuperer
	checkIfDatepickerCheked() {
		// if ((this.planning_id_add === 0) && this.plannings.length > 0){
		//   this.planning_id_add = this.plannings[0][0].lastPlanning_id;
		// }
		// this.nmbreAjouter = this.nmbreAjouter + 1;
		// if (this.typeGardeService.typeDeGardeSubject.getValue().length > 0) {
		//   if (this.nmbreAjouter === 1) {
		//     const tmp = [];
		//     this.typeGardeService.typeDeGardeSubject.subscribe(
		//       (typedegardes) => {
		//         typedegardes.filter((item: any) => {
		//           if(item.fiche_specialite_id === this.missionChoisie.fiche_specialite_id){
		//             tmp.push(item);
		//           }
		//         });
		//       }
		//     );
		//     this.typeDeGardes = tmp;
		//   }
		// } else {
		//   if (this.nmbreAjouter === 1) {
		//     const tmp = [];
		//     this.typeGardeService.getTypeDeGarde().subscribe(
		//       (typedegardes) => {
		//         typedegardes.filter((item: any) => {
		//           if(item.fiche_specialite_id === this.missionChoisie.fiche_specialite_id){
		//             tmp.push(item);
		//           }
		//         });
		//       }
		//     );
		//     this.typeDeGardes = tmp;
		//   }
		// }
	}

	onNewPeriodeOnMission(mail: boolean = false) {
		const monthDebut: any =
			this.dateDebut.month.toString().length === 1 ? '0' + this.dateDebut.month.toString() : this.dateDebut.month;
		const monthFin: any =
			this.dateFin.month.toString().length === 1 ? '0' + this.dateFin.month.toString() : this.dateFin.month;
		const dateDebut: any = '' + this.dateDebut.day + '-' + monthDebut + '-' + this.dateDebut.year;
		const dateFin: any = '' + this.dateFin.day + '-' + monthFin + '-' + this.dateFin.year;
		this.notificationService.onConfirm();
		this.notificationService.dialogRef.afterClosed().subscribe((result) => {
			if (result) {
				const id_client = this.clientService.idClient.getValue();
				this.notificationService.blockUiStart();
				this.planningService.AllplanningsForMissionChoiceSubject.next([]);
				this.myFormNewPeriodeOnMission.patchValue({
					missionForm: this.missionChoisie.mission_id
				});
				// date: {start: Moment, end: Moment}
				this.myFormNewPeriodeOnMission.value.date.start = dateDebut;
				this.myFormNewPeriodeOnMission.value.date.end = dateFin;
				const auteur = this.userService.getCurrentUser().username;
				this.planningService
					.createPeiodeOnMission(this.myFormNewPeriodeOnMission.value, id_client, auteur, mail)
					.subscribe(
						(next: any) => {
							this.clientService.allmissionsForClientSubject.next([]);
							if (next.erreur) {
								this.initForm();
								this.notificationService.showNotificationEchec(next.erreur);
							} else {
								this.initForm();
								this.boolNewPeriode = false;
								const dataFilter = {
									offset: 0,
									limit: 5,
									filter: null,
									id_client: this.clientService.idClient.getValue()
								};
								this.notificationService.showNotificationSuccess(next.success);
								this.clientService.getAllMissionByClient(dataFilter).subscribe((data_response: any) => {
									const listeMisions = data_response.data;
									listeMisions.forEach((element) => {
										if (element.mission_id === this.missionChoisie.mission_id) {
											this.arrayForAddNewVacation = [];
											this.planningService.getReformatPlanningForMission(element.plannings);
											this.clientService.allmissionsForClientSubject.next(data_response);
											this.notificationService.blockUiStop();
										}
									});
								});
							}
						},
						() => {
							this.notificationService.showNotificationEchec(
								'erreur veuillez contacter le service informatique !'
							);
						},
						() => { }
					);
			} else {
				this.utilService.onRevientEnHaut();
				this.boolNewPeriode = !this.boolNewPeriode;
				this.setMonthOnDatePriode();
			}
		});
	}

	onNewEnregistrer() {
		this.notificationService.onConfirm();
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.utilService.onRevientEnHaut();
				this.planningService.AllplanningsForMissionChoiceSubject.next([]);
				this.notificationService.blockUiStart();
				this.typeDeGardeDefaut = this.onValideFormData();
				const auteur = this.userService.getCurrentUser().username;

				this.planningService.createByMission(this.typeDeGardeDefaut, auteur).subscribe(
					(next: any) => {
						if (next.erreur) {
							this.notificationService.showNotificationEchec(next.erreur);
							this.notificationService.blockUiStop();
						} else {
							this.notificationService.blockUiStop();
							const dataFilter = {
								offset: 0,
								limit: 5,
								filter: null,
								id_client: this.clientService.idClient.getValue()
							};
							this.clientService.getAllMissionByClient(dataFilter).subscribe((response: any) => {
								this.planningService.showCreateOnMissionOrList = false;
								const listeMisions = response.data;
								this.clientService.allmissionsForClientSubject.next(response);
								listeMisions.forEach((element) => {
									// on remet a vide tous les tableaux
									this.arrayForAddNewVacation = [];
									this.typeDeGardeDefaut = [];
									this.deleteVacation = [];
									this.arrayForAddNewVacation = [];
									this.planningService.newDatesOnMissionViaDatePiker = [];
									if (element.mission_id === this.missionChoisie.mission_id) {
										this.planningService.getReformatPlanningForMission(element.plannings);
										this.planningService.newDatesOnMissionViaDatePiker = [];
										this.notificationService.blockUiStop();
									}
								});
								this.notificationService.showNotificationSuccess(next.success);
							});
						}
					},
					(error) => {
						this.notificationService.blockUiStop();
					}
				);
			} else {
				this.utilService.onRevientEnHaut();
			}
		});
	}

	onContinuerMaj() {
		// if (this.typeDeGardeDefaut[0].datePlan) {
		this.planningService.showCreateOnMissionOrList = false;
		// }
	}

	ngOnChanges() {
		if (this.vacationParent) {
			this.vacationParentArray.forEach((element) => {
				element.hidden = false;
			});
		}
	}

	// fonction pour le ngDatepiker
	onDateSelection(date: NgbDate) {
		if (!this.dateDebut && !this.dateFin) {
			this.dateDebut = date;
		} else if (this.dateDebut && !this.dateFin && date.after(this.dateDebut)) {
			this.dateFin = date;
		} else {
			this.dateFin = null;
			this.dateDebut = date;
		}
	}

	isHovered(date: NgbDate) {
		return (
			this.dateDebut &&
			!this.dateFin &&
			this.hoveredDate &&
			date.after(this.dateDebut) &&
			date.before(this.hoveredDate)
		);
	}

	isInside(date: NgbDate) {
		return date.after(this.dateDebut) && date.before(this.dateFin);
	}

	isRange(date: NgbDate) {
		return date.equals(this.dateDebut) || date.equals(this.dateFin) || this.isInside(date) || this.isHovered(date);
	}

	validateInput(currentValue: NgbDate, input: string): NgbDate {
		const parsed = this.formatter.parse(input);
		return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
	}

	ngOnDestroy() {
		this.typeDeGardeDefaut = [];
		this.planningService.newDatesOnMissionViaDatePiker = [];
	}

	// test form array
	initFormData() {
		this.formArrayPicker = this.formBuilder.group({
			groupData: this.formBuilder.array([])
		});
	}

	addGroupData(dates) {
		const groupData = this.formArrayPicker.controls.groupData as FormArray;
		// on recupere les valeurs du formulaire dans un tableau simple
		const formValues = this.formArrayPicker.value.groupData.map((x) => {
			if (x.date) {
				return x.date;
			} else {
				return [];
			}
		});
		// on verifie les donnee du formulaires nexiste plus dans le tableau on les supprime
		if (formValues.length > 0) {
			formValues.forEach((x) => {
				if (dates.indexOf(x) === -1) {
					groupData.controls.splice(formValues.indexOf(x), 1);
					this.formArrayPicker.value.groupData.splice(formValues.indexOf(x), 1);
				}
			});
		}
		// si le tableau contient de donnee on verifie sil existe pas dans le formulaire
		if (dates.length > 0) {
			dates.forEach((x) => {
				if (formValues.indexOf(x) === -1) {
					groupData.push(
						this.formBuilder.group({
							date: [x],
							occurences: this.formBuilder.array([])
						})
					);
				}
			});
		}
	}

	getOccurences(index) {
		const occurences = this.formArrayPicker.get(`groupData.${index}.occurences`) as FormArray;
		return occurences.controls;
	}

	addOccurence(index) {
		const occurences = this.formArrayPicker.get(`groupData.${index}.occurences`) as FormArray;
		occurences.push(
			this.formBuilder.group({
				vacation: [''],
				tarif: [''],
				salaire: [''],
				qualif: ['', Validators.required]
			})
		);
	}

	// conformiter des donnees aves la fonction existante
	onValideFormData() {
		const data = this.formArrayPicker.value.groupData;
		const tmp = [];
		data.forEach((x) => {
			x.occurences.forEach((y) => {
				const typeGardes = {
					heure_debut: y.vacation.heure_debut,
					heure_fin: y.vacation.heure_fin,
					salaire_net: y.salaire,
					tarif: y.tarif,
					qualif: y.qualif,
					statut: 'A Pourvoir',
					typeDeGarde_id: y.vacation.typeDeGarde_id,
					type_de_garde: y.vacation.type_de_garde
				};
				const tmpData = {
					typeGardes: null,
					datePlan: x.date.format('DD-MM-YYYY'),
					mission_id: this.missionChoisie.mission_id
				};
				tmpData.typeGardes = typeGardes;
				tmp.push(tmpData);
			});
		});
		return tmp;
	}

	setDataOnForm(indexGroupData, indexOcurrence) {
		const occurence = this.getOccurences(indexGroupData)[indexOcurrence];
		const indexTmp = indexGroupData + indexOcurrence;
		this.qualifsData[indexTmp] = occurence.value.vacation.qualifs;
		occurence.patchValue({
			tarif: occurence.value.vacation.tarif,
			salaire: occurence.value.vacation.salaire_net
		});
	}

	delOccurence(indexGroupData, indexOcurrence) {
		const occurence = this.formArrayPicker.get(`groupData.${indexGroupData}.occurences`) as FormArray;
		occurence.removeAt(indexOcurrence);
	}

	//  fin de test
}
