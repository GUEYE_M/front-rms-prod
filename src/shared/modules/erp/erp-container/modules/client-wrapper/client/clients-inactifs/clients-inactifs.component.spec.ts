import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsInactifsComponent } from './clients-inactifs.component';

describe('ClientsInactifsComponent', () => {
  let component: ClientsInactifsComponent;
  let fixture: ComponentFixture<ClientsInactifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsInactifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsInactifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
