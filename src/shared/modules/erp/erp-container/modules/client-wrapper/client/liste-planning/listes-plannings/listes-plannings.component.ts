import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { UtilsService } from '../../../../../../../../services/utils.service';
import { DatePipe } from '@angular/common';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { PlanningService } from '../../../../../../../../services/recruteur/planning.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;
import { SelectionModel } from '@angular/cdk/collections';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { RolesService } from '../../../../../../../../services/roles.service';
import { UserService } from '../../../../../../../../services/user.service';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { FilterService } from '../../../../../../../../services/filter.service';
import set = Reflect.set;

moment.locale('fr', localization);

@Component({
	selector: 'app-listes-plannings',
	templateUrl: './listes-plannings.component.html',
	styleUrls: [ './listes-plannings.component.css' ]
})
export class ListesPlanningsComponent implements OnInit, AfterViewInit {
	listeStatus = [ 'Non Pourvue', 'Perdue', 'Annuler CH', 'A Pourvoir', 'Autre' ];
	formFilter: FormGroup;
	sendStatusFormGroup: FormGroup;
	popoverForm: FormGroup;
	uri_listes_plannings = 'plannings/listes-filter';
	dateDebutInitial: any;
	statuts = [
		'A Pourvoir',
		'Pourvue',
		'Non Pourvue',
		'Annule',
		'Annuler Ch',
		'Annuler Par le CH',
		'Annulee_Med',
		'Perdue',
		'Annulee_C_ApresValidation',
		'Annulee_C_AvantValidation',
		'Autre'
	];
	// displayedColumns = ['number', 'date', 'specialite', 'mois', 'annee', 'vacation', 'debut', 'fin', 'statut', 'tarif', 'salaire', 'ville', 'departement', 'etablissement'];
	displayedColumns = [
		'select',
		'number',
		'date',
		'specialite',
		'mois',
		'annee',
		'vacation',
		'debut',
		'fin',
		'statut',
		'salaire',
		'ville',
		'etablissement'
	];
	dataSource: any = new MatTableDataSource();

	selection = new SelectionModel<any[]>(true, []);
	listActions = [ 'Supprimer', 'Desactiver' ];

	elements: any = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null,
		date_debut: null,
		date_fin: null
	};
	currentUser: any;

	locale = LOCALE;
	pageSize: number = 12;
	allmois = [];

	range: Range = { fromDate: new Date(), toDate: new Date() };

	date_debut: number = null;
	date_fin: number = null;

	viewdatepiker: boolean = false;
	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];

	length: number = 0;
	pageSizeOptions: number[] = [ 12, 25, 50, 100, 200, 400 ];

	displayedColumnsFilter = [];
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;

	constructor(
		private activatedRoute: ActivatedRoute,
		private filterService: FilterService,
		private planningService: PlanningService,
		private notifierService: NotificationService,
		private datePipe: DatePipe,
		private fb: FormBuilder,
		private userService: UserService,
		public roleService: RolesService,
		private utilsService: UtilsService
	) {
		this.initData();
	}

	ngOnInit() {
		this.sendStatusFormGroup = this.fb.group({
			status: [ '' ]
		});
		this.formFilter = this.fb.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		this.elements.splice(this.elements.indexOf('date'), 1);
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		this.data.date_debut = null;
		this.data.date_fin = null;

		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
				this.getNewData();
			}
		}

		this.currentUser = this.userService.getCurrentUser();

		this.notifierService.blockUiStop();
		this.displayedColumnsFilter = this.planningService.displayFilterName();
		this.allmois = this.utilsService.getMois();

		this.initFormPopover();
	}

	initFormPopover() {
		this.popoverForm = this.fb.group({
			statut: [ 'Non Pourvue', [ Validators.required, Validators.minLength(6) ] ],
			idPlan: [ '', [ Validators.required, Validators.minLength(6) ] ],
			indexPlan: [ 0 ]
		});
	}

	// ===================================================================================================================================
	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		// permet de faire le  sort sur la date
		this.dataSource.sortingDataAccessor = (item, property) => {
			switch (property) {
				case 'date':
					return new Date(item.dateFormat);
				default:
					return item[property];
			}
		};
	}

	initData() {
		const listesPlannings = this.activatedRoute.snapshot.data['listesPlannings']['data'];
		this.length = this.activatedRoute.snapshot.data['listesPlannings']['lenght'];
		// on parcour la data afin de remettre la date en format date
		const tmp = listesPlannings.map((x) => {
			x.dateFormat = moment(x.dateFormat);
			return x;
		});
		this.dataSource.data = tmp;
	}

	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {

			this.data.filter = null;
			this.data.date_debut = null;
			this.data.date_fin = null;
			this.initData();

			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} else {
			this.elements.map((item) => {
				if (item === 'date') {
					this.datepickerConfig();
				}
				
				
			
			});
			localStorage.setItem('elements', JSON.stringify(this.elements));
		}
	}

	// fonction qui envoie le formulaire de filtre en base de donnee

	onSubmitFiltre() {
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = this.formFilter.value;
		this.getNewData();
	}

	pagination(event: any) {
		this.data.offset = event.pageIndex;
		this.data.limit = event.pageSize;

		this.getNewData();
	}

	getNewData() {
		this.notifierService.blockUiStart();
		this.planningService.getAllPlannings(this.data).subscribe(
			(response: any) => {
				if (response['data']) {
					const tmp = response['data'].map((x) => {
						x.dateFormat = new Date(x.dateFormat);
						return x;
					});
					this.dataSource = new MatTableDataSource(tmp);

					this.length = response['lenght'];
					this.dataSource.paginator = this.paginator;
					this.dataSource.sort = this.sort;
					this.dataSource.sortingDataAccessor = (item, property) => {
						switch (property) {
							case 'date':
								return new Date(item.dateFormat);
							default:
								return item[property];
						}
					};
				}
			},
			(erreur) => {
				this.notifierService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
				this.notifierService.blockUiStop();
			},
			() => {
				this.notifierService.blockUiStop();
			}
		);
	}

	onPopover(planning, index) {
		this.popoverForm.patchValue({
			idPlan: planning.planning_id,
			indexPlan: index
		});
	}

	onValidePopover() {
		this.notifierService.blockUiStart();

		this.planningService.changeStatutPlanningOnHaveAnyCandidature(this.popoverForm.value).subscribe((next: any) => {
			if (next.erreur) {
				this.notifierService.showNotificationEchecCopie('topCenter', next.erreur);
			} else {
				this.notifierService.showNotificationSuccessCopie('topCenter', next.success);
				this.dataSource.data[next.data.indexPlan].statut = next.data.statut;
			}
			this.notifierService.blockUiStop();
		});
	}

	datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
			// excludeWeekends:true,
			// fromMinMax: {fromDate:fromMin, toDate:fromMax},
			// toMinMax: {fromDate:toMin, toDate:toMax}
		};
	}

	setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
	}

	updateRange(range: Range) {
		//this.notificationService.blockUiStart();

		this.range = range;
		this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd');
		this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd');
		localStorage.setItem('date_debut', this.data.date_debut);
		localStorage.setItem('date_fin', this.data.date_fin);
	}

	onSendStatus() {
		this.notifierService.blockUiStart();
		const tmp = this.selection.selected.map((x: any) => {
			return x.planning_id;
		});
		const data: any = {};
		data.idPlan = tmp;
		data.statut = this.sendStatusFormGroup.value.status;
		this.planningService.changeStatutPlanningOnHaveAnyCandidature(data).subscribe((x) => {
			if (x.erreur) {
				this.notifierService.showNotificationEchecCopie('topCenter', x.erreur);
			} else {
				this.notifierService.showNotificationSuccessCopie('topCenter', x.success);
				this.selection.clear();
				this.getNewData();
			}
			this.notifierService.blockUiStop();
		});
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}
}
