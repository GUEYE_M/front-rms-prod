import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../../../shared/shared.module';
import { ListesPlanningsComponent } from './listes-plannings/listes-plannings.component';
import {ListePlanningRouting} from './liste-planning.routing';

@NgModule({
  declarations: [
  ListesPlanningsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ListePlanningRouting
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class ListePlanningModule { }
