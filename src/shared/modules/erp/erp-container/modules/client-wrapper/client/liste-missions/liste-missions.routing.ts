import {Route, RouterModule} from '@angular/router';
import {ListeMissionsComponent} from './liste-missions/liste-missions.component';
import {MissionsResolve} from '../../../../../../../resolver/missions.resolve';
import {ModalPlanningMissionComponent} from '../modal-client/modal-planning-mission/modal-planning-mission.component';
import {ModalMachingDispoComponent} from '../modal-client/modal-maching-dispo/modal-maching-dispo.component';


const ROUTE_CONFIG: Route[] = [
  {
    path: 'liste',
    component: ListeMissionsComponent,
    resolve: {missions: MissionsResolve}
  },
  {
    path: 'planningModal',
    component: ModalPlanningMissionComponent,
  },
  {
    path: 'disponibiliteModal',
    component: ModalMachingDispoComponent,
  }
];

export const ListeMissionsRouting = RouterModule.forChild(ROUTE_CONFIG);
