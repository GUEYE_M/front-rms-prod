import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMachingDispoComponent } from './modal-maching-dispo.component';

describe('ModalMachingDispoComponent', () => {
  let component: ModalMachingDispoComponent;
  let fixture: ComponentFixture<ModalMachingDispoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMachingDispoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMachingDispoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
