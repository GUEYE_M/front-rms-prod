import {Component, OnInit, ViewChild} from '@angular/core';
import {from} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {SpecialiteService} from '../../../../../../../../services/specialite.service';
import {MatDialogRef} from '@angular/material';
import {FormControl} from '@angular/forms';
import {SelectAutocompleteComponent} from 'select-autocomplete';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
  selector: 'app-modal-mail-fiche-poste',
  templateUrl: './modal-mail-fiche-poste.component.html',
  styleUrls: ['./modal-mail-fiche-poste.component.css']
})
export class ModalMailFichePosteComponent implements OnInit {

  toppings = new FormControl();

  listSpecialites = [];
  selected = [];
  @ViewChild(SelectAutocompleteComponent, {static: false}) multiSelect: SelectAutocompleteComponent;
  constructor(public dialogRef: MatDialogRef<ModalMailFichePosteComponent>,

              private specialiteService: SpecialiteService,private notificationService:NotificationService)
  {

  }
  

  ngOnInit() {
    this.specialiteService.list(null,true).subscribe(
      (response: any) => {
        from(response.data).pipe(
          map(
            (item: any) => {
              return{
                display: item.nomspecialite,
                value: item.nomspecialite,
              };
            }),
          tap((el) => {
            this.listSpecialites.push(el);
          })
        ).subscribe();
      }
    );
  }

  onConfirm(): void {
    // Close the dialog, return true
    if(this.selected.length > 0)
    {
      this.dialogRef.close(this.selected);
    }
    else
     {
       this.notificationService.showNotificationEchecCopie('bottomRight',"veillez selectionnez au moins une specialité");
     }
   
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }

  getSelectedOptions(selected) {
    this.selected = selected;
  }

}
