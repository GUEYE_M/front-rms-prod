import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputAutocompletInterimComponent } from './input-autocomplet-interim.component';

describe('InputAutocompletInterimComponent', () => {
  let component: InputAutocompletInterimComponent;
  let fixture: ComponentFixture<InputAutocompletInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputAutocompletInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputAutocompletInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
