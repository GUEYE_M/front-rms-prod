import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ClientService} from '../../../../../../../../services/recruteur/client.service';
import {NotificationService} from '../../../../../../../../services/notification.service';
import {ActivatedRoute} from '@angular/router';
import {MatBottomSheet, MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {InterimModalComponent} from './interim-modal/interim-modal.component';
import {CandidatureModalComponent} from './candidature-modal/candidature-modal.component';
import {RolesService} from '../../../../../../../../services/roles.service';
import {UserService} from '../../../../../../../../services/user.service';
import {SelectionModel} from '@angular/cdk/collections';
import {PldContratComponent} from '../../../../interim/single-interim/mission-medecin/pld-contrat/pld-contrat.component';
import {ListHebergementModalComponent} from '../../../../interim/single-interim/mission-medecin/list-hebergement-modal/list-hebergement-modal.component';


import {SellandsignService} from '../../../../../../../../services/sellandsign.service';
import {StatutContratSignComponent} from '../../../../interim/single-interim/mission-medecin/statut-contrat-sign/statut-contrat-sign.component';
import {ReleveHeureModalComponent} from '../../../../interim/single-interim/mission-medecin/releve-heure-modal/releve-heure-modal.component';
import { HebergementServiceService } from 'src/shared/services/recruteur/hebergement.service.service';
import { ListRecruteurModalComponent } from '../../modal-client/list-recruteur-modal/list-recruteur-modal.component';
import {URL_ANNEXE_CONTRAT} from '../../../../../../../../utils/server_api_url';

@Component({
  selector: 'app-contrat-client',
  templateUrl: './contrat-client.component.html',
  styleUrls: ['./contrat-client.component.css']
})
export class ContratClientComponent implements OnInit {
  idClient: any;
  selection = new SelectionModel<any[]>(true, []);
  detailsMMM = false;
  refDetailsMM = '';
  client: any;
  currentInterimaire: any;
  idpld: number;
  urlAnnexeContrat = URL_ANNEXE_CONTRAT;
  data = {
    id_ch : null,
    id_interim: null,
    id_recruteur: null,
    id_pld : null,
    apartenance : null,
    interim: false,
    reference_mission: null,
    hebergement: []
  };
  apartenance_contrat: number = null;

  carecteristiquePoste: any;
  currentUser: any;
  actionContrat = false;
  displayedColumns = [
    'position',
    'ref',
    'annee',
    'mois',
    'specialite',
    'renumeration',
    'action'
  ];
  displayedColumnsDetailsMM = [
    'select',
    'position',
    'date',
    'debut',
    'fin',
    'tarif',
    'contrat',
  ];
  displayedColumnslisteContrats = ['pld'];
  dialogRefRH: MatDialogRef<ReleveHeureModalComponent>;

  dialogRef: MatDialogRef<CandidatureModalComponent>;
  dataSourceCandidaturesDetailsMM: MatTableDataSource<any>;
  dialogueModalPldContrat: MatDialogRef<PldContratComponent>;
  dataSourceContrats: MatTableDataSource<any>;
  dialogRefContratSign: MatDialogRef<StatutContratSignComponent>;

  dataSource: any ;
  constructor(
    private clientService: ClientService,
    private activatedRoute: ActivatedRoute,
    private bottomSheet: MatBottomSheet,
    public roleService: RolesService,
    private userService: UserService,
    private dialog: MatDialog,
    private sellAndSign: SellandsignService,
    private hebergementService: HebergementServiceService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.idClient = this.activatedRoute.snapshot.params['id'];
    this.clientService.labelTab_text.subscribe(
      (label) => {
        this.clientService.contratsClient(this.idClient).subscribe((response) => {
            console.log(response);

            this.dataSource = [];
            if (response['data']) {
              let cpt = 0;
              response['data'].forEach(
                (item) => {
                  if (this.nbrCandidaturesValideesByRef(item.reference, item.candidatures) > 0) {
                    cpt += 1;
                    const contrat = {
                      position: cpt,
                      ref: item.reference,
                      annee: item.annee,
                      mois: item.mois,
                      specialite: item.nomspecialite,
                      renumeration: '',
                      candidatures: item.candidatures,
                    };
                    this.dataSource.push(contrat);
                  }
                });
            }
          },
          error => {
          });
      }
    );

  }
  onModalCandidature(reference, donnees) {
    const items = {ref: reference, candidatures: donnees};
    this.dialogRef = this.dialog.open(CandidatureModalComponent, {
      width: '800px',
      height: '500px',
      data: items,
      panelClass: 'myapp-no-padding-dialog'
    });
  }
  onModalIntmerim(donnees) {
    this.bottomSheet.open(InterimModalComponent, {
      data: donnees,
    });
  }
  onGetInterimsForMission(ref) {
    const response = {tarif: 0, data: []};
    let tarif = 0;
    this.dataSource.forEach(
      (element) => {
        if (element.ref === ref) {
          element.candidatures.forEach((el) => {
            if (el.statutCandidature === 'Validee') {
              const interimModel = {
                interim: el.prenom + ' ' + el.nom,
                id : el.interim_id,
              };
              response.data.push(interimModel);
              tarif = tarif + el.tarif;
            }
          });
        }
        response.data = this.supDoublons(response.data);
        response.tarif = tarif;
      });
    response.data = response.data.length <= 1 ? response.data[0] : response.data;
    return response;
  }
  nbrCandidaturesValidees(ref) {
    let nombre = 0;
    this.dataSource.forEach((item) => {
        if (item.ref === ref) {
          item.candidatures.forEach((el) => {
            if (el.statutCandidature === 'Validee') {
              nombre += 1;
            }
          });
        }
      });
    return nombre;
  }

  nbrCandidaturesValideesByRef(ref, candidatures) {
    let nombre = 0;
    candidatures.forEach((el) => {
      if (el.statutCandidature === 'Validee') {
        nombre += 1;
      }
    });
    return nombre;
  }

  supDoublons(array) {
    const len = array.length;
    const out = [];
    const obj = {};
    for (let i = 0; i < len; i++) {
      let existe = false;
      if (i === 0)
        out.push(array[i]);
      else {
        for (let j = 0; j < out.length; j++) {
          if (array[i].id === out[j].id)
            existe = true;
        }
        if (!existe)
          out.push(array[i]);
      }
    }
    return out;
  }
  OnAfficheReleveHr(item) {
    this.dialogRefRH = this.dialog.open(ReleveHeureModalComponent, {
      width: '70%',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: item
    });
  }
  onDetailMM(data) {
    if (data) {
      this.refDetailsMM = data.ref;
      let cpt = 0;
      this.notificationService.blockUiStart();
      this.clientService.candidatureValideeByRef(this.refDetailsMM).subscribe( response => {
        (response);
        const dataSourceContrat = [];
          this.notificationService.blockUiStop();
            if(response['data']) {
            this.dataSourceCandidaturesDetailsMM = new MatTableDataSource<any>(response['data']);
              response['data'].forEach(item => {
                if (item.pld) {
                  dataSourceContrat.push(item);
                }
              });
              if(dataSourceContrat)
                this.dataSourceContrats = new MatTableDataSource<any>(dataSourceContrat);
            }
          },
        error => {
          this.notificationService.blockUiStop();
      });
    }
    this.detailsMMM = !this.detailsMMM;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceCandidaturesDetailsMM.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceCandidaturesDetailsMM.data.forEach(row => this.selection.select(row));
    this.actionContrat = !this.actionContrat;
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onContratExiste(): boolean {
    let response = false;
    this.selection.selected.forEach((element) => {
      if (element['contrat'] && element['contrat'] !== null) {
        response = true;
      }
    });
    return response;
  }

  OnOuvreModalPld() {
    this.notificationService.blockUiStart();
    if (!this.onContratExiste() && (this.currentUser.gestionnaire.IdReferentiel)) {
      const donnees = {
        candidatures: this.selection.selected,
        carecteristiquePoste: this.carecteristiquePoste,
        interim: this.currentInterimaire,
        refMission: this.refDetailsMM,
        client: this.client
      };
      this.notificationService.blockUiStop();
      this.dialogueModalPldContrat = this.dialog.open(PldContratComponent, {
        width: '20%',
        height: '550px',

        panelClass: 'myapp-no-padding-dialog',
        data: donnees
      });
    } else if (!this.currentUser.gestionnaire.IdReferentiel) {
      this.notificationService.showNotificationEchecCopie('bottomRight', 'Vous pouvez pas créer de contrat si vous n\'êtes pas encore inscrit sur PLD!');
      this.notificationService.blockUiStop();
    } else {
      this.notificationService.showNotificationEchecCopie('bottomRight', 'Vous avez saisi une cadidature qui a déjà son contrat créer !');
      this.notificationService.blockUiStop();
    }
  }
  sendContratToSign() {

    this.notificationService.blockUiStart();
    this.sellAndSign.sendContratToSign(this.data).subscribe(
      (response:any)=>{
        if(!response.erreur)
        {
          this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
          this.updateActionContrat(this.apartenance_contrat);
          this.notificationService.blockUiStop();
        }
        else
        {
          this.notificationService.showNotificationSuccessCopie('bottomRight', response.erreur);
          this.notificationService.blockUiStop();
        }
      }
    );
  }
  updateActionContrat(apartenance_contrat: number, annuler: boolean = false) {
    this.dataSourceContrats.data.map(
      (el: any) => {
        if (el.idpld.id_pld === this.idpld) {
          if (apartenance_contrat === 1) {
            if (annuler) {
              el.interim_contrat = false;
            } else {
              el.interim_contrat = true;
            }
          } else {
            if (annuler) {
              el.ch_contrat = false;
            } else {
              el.ch_contrat = true;
            }
          }
        }
      }
    );
  }
  getStatutContrat(id_contrat_pld, appartenance) {
    this.notificationService.blockUiStart();

    this.sellAndSign.findContratIdbyIdPldAndApartenace(id_contrat_pld, appartenance).subscribe(
      (response: any) => {
        if (response.erreur) {
          this.notificationService.showNotificationEchec(response.erreur);
          this.notificationService.blockUiStop();
        } else {
          this.statutContratSignModal(response.data);

          this.idpld = id_contrat_pld;
          this.apartenance_contrat = appartenance;
          this.notificationService.blockUiStop();

        }


      },
      (erreur) => {

      },
      () => {

      }
    )
  }

  statutContratSignModal(data: any): void {
    this.dialogRefContratSign = this.dialog.open(StatutContratSignComponent, {
      width: '65%',
      height: '65%',
      panelClass: 'myapp-no-padding-dialog',
      data: data



    })
    this.dialogRefContratSign.afterClosed().subscribe(
      (response) => {
        if(response)
        {
          if(response === "Annuler".toLowerCase().trim())
          {
            this.updateActionContrat(this.apartenance_contrat,true)
          }
        }

      }
    );


  }

  onSignContrat(idpld, idcandidature, apartenancecontrat) {
    const thiss = this;
    const message = '';

    this.data.apartenance = apartenancecontrat;
    this.data.id_pld = idpld;
    this.apartenance_contrat = apartenancecontrat;
    this.idpld = idpld;
    this.data.id_interim = this.currentInterimaire.id;

    this.notificationService.confirmDialogue('Etes-vous sur de vouloir envoyer ce contrat en signature ?');
    document.addEventListener('iziToast-closing', function(data) {
      if (data['detail']['closedBy'] === 'Oui') {

        thiss.notificationService.blockUiStart();
        thiss.clientService.getClientRecruteurByCandidatureId(idcandidature).subscribe(
          (recruteur:any) => {

            if (recruteur) {
              const data = {
                items: recruteur.data,
                length: recruteur.data.length,
                edit: false
              };
              thiss.data.id_ch = recruteur.id_client;
              thiss.data.id_recruteur = recruteur.data[0].id_recruteur;
              thiss.data.reference_mission = recruteur.reference_mission;
              if(apartenancecontrat === 1)
              {
                thiss.data.interim = true
                thiss.notificationService.blockUiStart();
                thiss.hebergementService.getHebergementByClient(thiss.data.id_ch).subscribe(
                  (el: any) => {
                    thiss.notificationService.blockUiStop();
                    if(!el.erreur) {
                      let h = {
                        modal: true,
                        data:el.data
                      };
                      const dialogRefhebergement: MatDialogRef<ListHebergementModalComponent> = thiss.dialog.open(ListHebergementModalComponent, {
                        width: '900px',
                        height: '400px',
                        data: h,
                        panelClass: 'myapp-no-padding-dialog',
                      })

                      dialogRefhebergement.afterClosed().subscribe(
                        (el) => {
                          thiss.data.hebergement = el[0];
                          thiss.sendContratToSign();
                        }
                      );
                    }
                    else
                    {
                      thiss.notificationService.showNotificationEchecCopie("bottomRight",el.erreur)
                      thiss.notificationService.blockUiStop()
                    }
                  }
                );
              }

              else
              {
                const dialogRefrecruteur: MatDialogRef<ListRecruteurModalComponent> = thiss.dialog.open(ListRecruteurModalComponent, {
                  width: '900px',
                  height: '400px',
                  data: data,
                  panelClass: 'myapp-no-padding-dialog',
                });
                //this.notificationService.blockUiStop();
                dialogRefrecruteur.afterClosed().subscribe(
                  (recruteur) => {

                    if (recruteur.length > 0) {

                      thiss.data.id_recruteur = recruteur[0].id_recruteur;

                      thiss.data.apartenance = 0;
                      thiss.data.interim = false;

                      thiss.sendContratToSign();
                    }
                  }

                );
              }

            }

          },
          (error) => {
            thiss.notificationService.showNotificationEchec('erreur chargement veillez ressayer');
            thiss.notificationService.blockUiStop();
          },
          () => {
            thiss.notificationService.blockUiStop();

            // this.router.navigate(['/RMS-Admin/redirectionMed/', this.idIterim, 'interim']);
            // this.interimService.labelTab.next(this.labelTab);
          }
        );

      }
    });
  }

  removeDuplicates(items) {
    (items);
    const unique = [];
    const idPrimes = [];
    items.forEach(function(item, index) {
      if (index === 0) {
        unique.push(item);
        idPrimes.push(item.pld.idpld);
      } else {
        if (!idPrimes.includes(item.pld.idpld)) {
          unique.push(item);
          idPrimes.push(item.pld.idpld);
        }
      }
    });
    return unique;
  }
}
