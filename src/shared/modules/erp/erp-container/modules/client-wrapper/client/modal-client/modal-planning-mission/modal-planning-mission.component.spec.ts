import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPlanningMissionComponent } from './modal-planning-mission.component';

describe('ModalPlanningMissionComponent', () => {
  let component: ModalPlanningMissionComponent;
  let fixture: ComponentFixture<ModalPlanningMissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPlanningMissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPlanningMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
