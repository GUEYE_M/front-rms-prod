import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePlanningByPeriodeComponent } from './create-planning-by-periode.component';

describe('CreatePlanningByPeriodeComponent', () => {
  let component: CreatePlanningByPeriodeComponent;
  let fixture: ComponentFixture<CreatePlanningByPeriodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePlanningByPeriodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePlanningByPeriodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
