import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMailFichePosteComponent } from './modal-mail-fiche-poste.component';

describe('ModalMailFichePosteComponent', () => {
  let component: ModalMailFichePosteComponent;
  let fixture: ComponentFixture<ModalMailFichePosteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMailFichePosteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMailFichePosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
