import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProspectionComponent } from './edit-prospection.component';

describe('EditProspectionComponent', () => {
  let component: EditProspectionComponent;
  let fixture: ComponentFixture<EditProspectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProspectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProspectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
