import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatSort, MatPaginator, MatDialogRef, MatDialog } from '@angular/material';
import { RecruteurService } from 'src/shared/services/recruteur/recruteur.service';
import { UserService } from 'src/shared/services/user.service';
import { EditListeClientComponent } from '../edit-liste-client/edit-liste-client.component';
import { ClientService } from 'src/shared/services/recruteur/client.service';
import { NotificationService } from 'src/shared/services/notification.service';

@Component({
  selector: 'app-list-client-recruteur',
  templateUrl: './list-client-recruteur.component.html',
  styleUrls: ['./list-client-recruteur.component.css']
})
export class ListClientRecruteurComponent implements OnInit {
  
  displayedColumns = ['number', 'Raison Social','Telephone', 'Adresse','Commune', 'Email','Activer', 'Edit'];
  length: number = 0;

  pageSizeOptions: number[] = [12, 25, 50, 100];

  selection = new SelectionModel<any[]>(true, []);

  dialogRef: MatDialogRef<EditListeClientComponent>;

  dataSource: MatTableDataSource<any[]>;

  loader = 0.1;

  listsource: any[] = [];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  currentUser: any;
  constructor(private recruteurService:RecruteurService,private userService:UserService, private dialog: MatDialog,private clientService:ClientService,private notifierService:NotificationService) { }

  ngOnInit() {
   this.getData()

  }

  getData()
  {
    this.currentUser = this.userService.getCurrentUser();
      let clients: any;
      this.recruteurService.getClientForRecruteur(this.currentUser['recruteur']['id']).subscribe((rep) => {
        clients = rep['data'];
        this.length = clients.length
        this.dataSource = new MatTableDataSource(clients);
      });
    }


    editClientModal(id = null) {
       let data = {
           profil :true,
           id: id
       }
      this.dialogRef = this.dialog.open(EditListeClientComponent, {
        width: '800px',
          height: '550px',

          panelClass: 'myapp-no-padding-dialog',
        data: data
  
      });
      
      this.dialogRef.afterClosed().subscribe(
        (response)=>{
  
          if(response){
  
            if(response.profil)
            {
              this.getData()
            }
          }
        }
      )
  
  
    }
  

    onActiveOuDesactiveUser(index_client, client, val) {
      this.loader = client.id;
      this.clientService.enableClient(client.id, val).subscribe(
        (next) => {
          if (this.dataSource.data[index_client])
          {
            // this.dataSource.data[index_client].activer = val;
            this.loader = 0.1;
          }
          if (val === true) {
            this.notifierService.showNotificationSuccessCopie('topCenter', 'le client a été activé !');
          } else {
            this.notifierService.showNotificationSuccessCopie('topCenter', 'le client a été desactivé !');
          }
          // this.router.navigate(['/RMS-Admin/redirection-admin/', 'client/list']);
        }
      );
     
    }
}
