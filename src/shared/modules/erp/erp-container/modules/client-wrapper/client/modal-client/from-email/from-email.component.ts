import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BoiteEmailService } from '../../../../../../../../services/erp/boite-email.service';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { SendinblueService } from '../../../../../../../../services/sendinblue.service';

@Component({
	selector: 'app-from-email',
	templateUrl: './from-email.component.html',
	styleUrls: [ './from-email.component.css' ]
})
export class FromEmailComponent implements OnInit {
	editForm: FormGroup;
	displayName: string;
	edit: boolean = false;
	data_send: {
		sender: any;
		'sender.name': string;
		'sender.email': string;
		to: any[];
		subject: string;
		message: string;
		templateId: number;
		'replyTo.email': string;
		'replyTo.name': string;
	};
	list_emails: any[];
	to = [];

	constructor(
		private fb: FormBuilder,
		private notificationService: NotificationService,
		private sendInblueService: SendinblueService,
		private boiteEmailService: BoiteEmailService,
		public dialogRef: MatDialogRef<FromEmailComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		if (this.data) {
			this.to = [];

			data;

			let sender: any = {
				name: 'RMS',
				email: 'no-reply@reseaumedical.fr'
			};

			if (this.data.client) {
				this.data.selection.forEach((item) => {
					if (item.recruteurs.length > 0) {
						item.recruteurs.forEach((element) => {
							this.to.push({
								name: element.nom,
								email: element.email
							});
						});
					} else {
						this.notificationService.showNotificationEchec('aucun recruteur trouvé');
						this.dialogRef.close(true);
					}
				});
			} else {
				this.data.selection.map((el) => {
					this.to.push({
						name: el.nom,
						email: el.email
					});
				});
			}
		}

		this.boiteEmailService.list().subscribe((response: any) => {
			this.list_emails = response.data;
		});
	}

	ngOnInit() {
		this.editForm = this.fb.group({
			type: [],
			cc: [ null, [ Validators.required ] ],
			from: [ null, [ Validators.required ] ],
			to: [ Validators.required ],
			replyto: [ null, [ Validators.required ] ],
			objet: [ null, [ Validators.required ] ],
			message: [ null, [ Validators.required ] ]
		});
	}

	sendEmail() {
		if (this.editForm.valid) {
			this.notificationService.blockUiStart();
			if (this.to.length > 0) {
				this.editForm.get('to').setValue(this.to);
				if (this.data.client) {
					this.editForm.get('type').setValue(true);
				} else {
					this.editForm.get('type').setValue(false);
				}

				this.sendInblueService.sendEmail(this.editForm.value).subscribe(
					(response) => {
						this.notificationService.blockUiStop();
						if (response.erreur) {
							this.notificationService.showNotificationEchec(response.erreur);
						} else {
							this.notificationService.showNotificationSuccess(response.success);
							this.onDismiss();
							this.dialogRef.close(true);
						}
					},
					(erreur) => {
						this.notificationService.showNotificationEchec('erreur server veillez ressayer a nouveau');
						this.notificationService.blockUiStop();
					},
					() => {}
				);
			}
		} else {
			this.notificationService.showNotificationEchec('Formulaire invalide');
		}
	}

	onDismiss(): void {
		// Close the dialog, return false
		this.dialogRef.close(false);
	}
}
