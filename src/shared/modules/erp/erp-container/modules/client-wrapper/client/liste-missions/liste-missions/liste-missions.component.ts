import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalPlanningMissionComponent } from '../../modal-client/modal-planning-mission/modal-planning-mission.component';
import {
  MatDialog,
  MatDialogRef,
  MatPaginator,
  MatSort,
  MatTableDataSource
} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ModalMachingDispoComponent } from '../../modal-client/modal-maching-dispo/modal-maching-dispo.component';
import { ActivatedRoute } from '@angular/router';
import { MissionService } from '../../../../../../../../services/mission.service';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FilterService } from '../../../../../../../../services/filter.service';
import { SendinblueService } from '../../../../../../../../services/sendinblue.service';
import { NotificationService } from '../../../../../../../../services/notification.service';
import { UtilsService } from '../../../../../../../../services/utils.service';
import { ConfirmDialogModel } from '../../../../../../../../models/Confirm.dialog';
import { InputAutocompletInterimComponent } from '../../modal-client/input-autocomplet-interim/input-autocomplet-interim.component';
import { Range } from 'ngx-mat-daterange-picker';
import { ConfirmDialogComponent } from '../../../../confirm-dialog/confirm-dialog.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ModalInterimComponent } from '../../../../interim/modal-interim/modal-interim.component';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-liste-missions',
  templateUrl: './liste-missions.component.html',
  styleUrls: ['./liste-missions.component.css']
})
export class ListeMissionsComponent implements OnInit {
  formFilter: FormGroup;
  displayedColumns = [
    'select',
    'number',
    'nom_etablissement',
    'reference',
    'mois',
    'annee',
    'nomSpecialite',
    'activer',
    'date',
    'disponibilitéesInterim'
  ];
  dataSource: MatTableDataSource<any[]>;
  dataSource_interims_disponibile: MatTableDataSource<any[]>;
  isMissionEnabled = true;
  uri_listes_plannings = 'missions/listes-filter';
  action: number;
  listActions: any[] = [
    { value: 2, viewValue: 'Envoyer MDD' }
  ];
  activated = ['oui', 'non'];

  pageSize = 12;
  @Input() missions = [];
  selection = new SelectionModel<any[]>(true, []);
  listes_misions: any;
  loader = 0.1;

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null
  };
  listsource: any[] = [];
  allmois = [];
  range: Range = { fromDate: new Date(), toDate: new Date() };
  length = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];

  dialogRef: MatDialogRef<ModalMachingDispoComponent>;
  dialogRefPlanningMission: MatDialogRef<ModalPlanningMissionComponent>;

  displayedColumnsFilter = [];
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('listPaginator', { static: false }) paginator: MatPaginator;

  constructor(
    private activatedRoute: ActivatedRoute,
    private missionService: MissionService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    private filterService: FilterService,
    private sendinblueService: SendinblueService,
    private notificationService: NotificationService,
    private utilsService: UtilsService
  ) {
    this.initData();
  }

  ngOnInit() {
    this.formFilter = this.formBuilder.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    }

    this.notificationService.blockUiStop();
    this.displayedColumnsFilter = this.missionService.displayFilterName();
    this.allmois = this.utilsService.getMois();
  }

  onActiveBtnRadio(mission, choix) {
    this.loader = mission.mission_id;
    this.missionService
      .enableMission(mission.mission_id, choix)
      .subscribe(next => {
        this.dataSource.data.filter((item?: any) => {
          if (item.mission_id === mission.mission_id) {
            item.enable = choix;
            this.loader = 0.1;
          }
        });
        if (next === 'ok') {
          this.notificationService.showNotificationSuccess(
            'la mis à jour a été bien prise en compte !'
          );
        }
      });
  }

  masterToggle() {

    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  actionbtn(action: number, itemSelection: any) {
    const data = {
      mission: null,
      mdd_classic: true,
      interims: [],
      id_client: []
    };
    if (action === 2) {

      if (this.checkDublicateSpecialite(itemSelection).length > 1) {
        this.notificationService.showNotificationEchecCopie("bottomRight", "Invalide! vous avez selectionné des spécialités différentes")
      }
      else if (this.checkMissionEnable(itemSelection)) {

        this.notificationService.showNotificationEchecCopie("bottomRight", "echec envoi mdd  ! la mission  " + this.checkMissionEnable(itemSelection).reference + " n'est pas activé")
      }
      else {
        this.getInterim(data, this.getIDspecialiteAndCH(itemSelection))

      }
    }
  }

  getInterim(data: any, item) {

    let el = item;
    let dialogRefinterimlist: MatDialogRef<ModalInterimComponent>;
    dialogRefinterimlist = this.dialog.open(ModalInterimComponent, {
      width: '1000px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: item
    });

    dialogRefinterimlist.afterClosed().subscribe((response: any) => {
      if (response) {
        data.mission = item.missions;
        data.interims = response;
        data.id_client = item.id_client
        this.notificationService.onConfirm(
          // tslint:disable-next-line:max-line-length
          'Attention ! Voulez-vous vraiment effectuer cette action ?'
        );

        this.notificationService.dialogRef
          .afterClosed()
          .subscribe((response) => {
            if (response) {
              console.log(data.interims)
              this.sendMailDiffusion(data);
            }
          });
      }
    });
  }

  sendMailDiffusion(data: any) {
    //this.notificationService.blockUiStart();
    this.sendinblueService.sendMdd(data).subscribe(
      (response) => {
        this.notificationService.blockUiStop();
        if (!response.erreur) {

          this.notificationService.showNotificationSuccess(response.success);
          this.selection.clear()
        } else {
          this.notificationService.showNotificationEchec(response.erreur);
        }
      },

    );
  }

  show_interim_dispo(mission) {
    this.isMissionEnabled = mission.enable;
    this.dataSource_interims_disponibile = new MatTableDataSource(
      mission.interims_disponible
    );
  }

  initData() {
    this.listes_misions = this.activatedRoute.snapshot.data.missions.data;
    this.dataSource = new MatTableDataSource(this.listes_misions);
    this.length = this.activatedRoute.snapshot.data.missions.lenght;
  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }

  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();
  }

  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }

  getNewData() {
    this.notificationService.blockUiStart();
    this.missionService.getMissions(this.data).subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.length = response.lenght;
      },
      erreur => {
        this.notificationService.showNotificationEchec(
          'erreur recherche!! veillez ressayer a nouveau'
        );
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }

  machingDisponibilte(mission) {
    this.dialogRef = this.dialog.open(ModalMachingDispoComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: mission
    });
  }

  planningMission(mission) {
    this.dialogRefPlanningMission = this.dialog.open(
      ModalPlanningMissionComponent,
      {
        width: '70%',
        height: '60%',
        panelClass: 'myapp-no-padding-dialog',
        data: mission.mission_id
      }
    );
  }

  getIDspecialiteAndCH(data: []) {
    let items = {
      specialite: [],
      id_client: [],
      missions: []

    };

    data.map(
      (el: any) => {
        items.missions.push(el.mission_id);
        items.specialite.push(el.id_specialite);
        items.id_client.push(el.id_client);

      }
    );


    return items;
  }
  // cette fonction renvoi true si l'utilisateur a selectionnez deux specialité differentes 
  checkDublicateSpecialite(data: any[]) {


    let valueArr = data.map(
      (item) => {
        return item.nomSpecialite

      });

    return valueArr.filter(
      (item, index) => valueArr.indexOf(item) === index)

  }
  checkMissionEnable(mission: []) {
    let response = null;
    mission.forEach(
      (item: any) => {
        if (item.enable === false) {
          response = item
        }
      }
    )
    return response;
  }
}
