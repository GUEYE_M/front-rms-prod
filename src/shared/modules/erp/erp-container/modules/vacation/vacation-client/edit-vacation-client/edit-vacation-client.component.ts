import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Vacation} from '../../../../../../../models/recruteur/Vacation.model';
import {Client} from '../../../../../../../models/recruteur/Client.model';
import {Specialite} from '../../../../../../../models/Specialite.model';
import {ClientService} from '../../../../../../../services/recruteur/client.service';
import {FicheSpecialiteService} from '../../../../../../../services/recruteur/fiche-specialite.service';
import {VacationClientService} from '../../../../../../../services/recruteur/vacation-client.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {UserService} from '../../../../../../../services/user.service';
import {UtilsService} from '../../../../../../../services/utils.service';


@Component({
  selector: 'app-edit-vacation-client',
  templateUrl: './edit-vacation-client.component.html',
  styleUrls: ['./edit-vacation-client.component.css']
})
export class EditVacationClientComponent implements OnInit {

  vacationForm: FormGroup;
  @Input() vacation: Vacation;
  @Output() listEvent: EventEmitter<boolean> = new EventEmitter<false>();
  clientList: Client[];
  list_type: any[];
  //annee: number = null;
  edit: boolean = false;
  inputSpecialite: boolean = false;

  annee: any[] = [

    {value: '2020', viewValue: '2020'},
    {value: '2021', viewValue: '2021'},
    {value: '2022', viewValue: '2022'},
    {value: '2023', viewValue: '2023'},
    {value: '2024', viewValue: '2024'},
    {value: '2025', viewValue: '2025'},

  ];
  data = {
    filter: null,
    id_client: null

  };
  gestionnaire: any;

  listSpecialiteByFiche: Specialite[];
  displayName: string;

  constructor(private fb: FormBuilder, private clientService: ClientService,
              private ficheSpecialiteService: FicheSpecialiteService,
              private vacationService: VacationClientService,
              private  notificationService: NotificationService,
              private userService: UserService,
              private utilsService: UtilsService,
              @Inject(MAT_DIALOG_DATA) public id: any,
              private dialogRef: MatDialogRef<EditVacationClientComponent>,
              private router: Router) {
    this.vacationService.listtype();
    if (this.id) {
      this.displayName = 'Mis a jour Vacation ';
      this.notificationService.blockUiStart();

      this.vacationService.find(+this.id).subscribe
      (
        (response: any) => {
          if (response.erreur) {
            this.notificationService.showNotificationEchec(response.erreur);
          } else {

            this.initform(response.data);
            this.edit = true;

            this.vacationForm.patchValue(
              {
                id: +this.data,
                fiche_specialite: response.data.fiche_specialite_id
              }
            );
          }
          this.notificationService.blockUiStop();
        }
      );
    } else {
      this.displayName = 'Nouvelle Vacation';
      this.initform();
    }
  }
  ngOnInit() {
    this.allClient();
    this.allTypeVaction();
    this.gestionnaire = this.userService.getCurrentUser();
  }
  initform(vacation = {
    libelle: {},
    heure_debut: null,
    heure_fin: null,
    annee: '',
    tarif: null,
    salaire_net: null,
    salaire_brute: null,
    client: '',
    nomSpecialite: '',
    remarque: '',
    txtreference: null
  }) {
    this.vacationForm = this.fb.group(
      {
        id: [],
        client: [Validators.required],
        annee: [vacation.annee, Validators.required],
        fiche_specialite: [Validators.required],
        libelle: [vacation.libelle, Validators.required],
        heure_debut: [vacation.heure_debut, Validators.required],
        heure_fin: [vacation.heure_fin, Validators.required],
        tarif: [vacation.tarif, Validators.required],
        salaire_net: [vacation.salaire_net, Validators.required],
        salaire_brute: [vacation.salaire_brute, Validators.required],
        remarque: [vacation.remarque],
        txtreference: [vacation.txtreference, Validators.required],
        auteur: [],
      }
    );
  }
  compareWithFunc(a, b) {

    return a === b;
  }
  onClient() {
    this.notificationService.blockUiStart();
    this.data.id_client = this.vacationForm.get('client').value.id;
    this.ficheSpecialiteService.ficheSpecialiteByClient(this.data).subscribe(
      (response: any) => {
        this.notificationService.blockUiStop();
        if (response.data) {
          this.listSpecialiteByFiche = response.data;
          if (this.listSpecialiteByFiche.length > 0) {
            this.inputSpecialite = true;
          } else {
            this.inputSpecialite = false;
          }
        } else {

          this.notificationService.showNotificationEchec('ce centre hospitalier n\'a aucun specialité');
        }
      },
      () => {

        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }

  allClient() {
    this.clientService.listClients(null, true).subscribe(
      (client: any) => {
        this.clientList = client.data;
      }
    );
  }

  allTypeVaction() {
    this.vacationService.listtype().subscribe(
      (item: any) => {
        this.list_type = item.data;
      }
    );
  }

  saveVacation() {
    this.notificationService.blockUiStart();
    this.vacationForm.get('auteur').setValue(this.gestionnaire.gestionnaire.id);
    this.vacationForm.get('id').setValue(this.id);
    this.vacationService.create(this.vacationForm.value).subscribe(
      (response: any) => {
        if (response.erreur) {

          this.vacationService.closemodale.next(false);
          this.notificationService.showNotificationEchec(response.erreur);
          this.notificationService.blockUiStop();
        } else {
         // this.utilsService.redirectEspace({espace: 'vacation', id: null});
          // if(this.edit)
          // {

          //   this.vacationService.listVacation.map(
          //     (t:any)=>{

          //       if(t.typeDeGarde_id == this.data)
          //       {


          //         const index = this.vacationService.listVacation.indexOf(t)
          //         this.vacationService.listVacation[index] = response.data

          //       }
          //     }
          //   )
          // }
          // else{

          //   if(this.vacationService.listVacation != null)
          //   {

          //     this.vacationService.listVacation.push(response.data);

          //   }
          //   else{
          //     let item : Array<any> = [];
          //     item.push(response.data)

          //     this.vacationService.listVacation = item;
          //   }

          // }


          this.dialogRef.close(true)
          this.notificationService.showNotificationSuccess(response.success);
          this.notificationService.blockUiStop();
        }

      }, (error) => {
        this.notificationService.showNotificationSuccess('erreur de connexion au server, veillez ressayer');
        this.notificationService.blockUiStop();
      },
      () => {

      }
    );
  }
}
