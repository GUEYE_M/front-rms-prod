import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVacationClientComponent } from './edit-vacation-client.component';

describe('EditVacationClientComponent', () => {
  let component: EditVacationClientComponent;
  let fixture: ComponentFixture<EditVacationClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVacationClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVacationClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
