import {AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {EditVacationClientComponent} from './edit-vacation-client/edit-vacation-client.component';
import {Vacation} from '../../../../../../models/recruteur/Vacation.model';
import {VacationClientService} from '../../../../../../services/recruteur/vacation-client.service';
import {ActivatedRoute} from '@angular/router';
import {FilterService} from '../../../../../../services/filter.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {ClientPldModel} from '../../../../../../models/interim/ClientPld.model';
import {ClientService} from '../../../../../../services/recruteur/client.service';
import {PldService} from '../../../../../../services/pld.service';
import {UtilsService} from '../../../../../../services/utils.service';

@Component({
  selector: 'app-vacation-client',
  templateUrl: './vacation-client.component.html',
  styleUrls: ['./vacation-client.component.css']
})
export class VacationClientComponent implements OnInit, AfterViewInit {
  formFilter: FormGroup;
  action: any;
  libelle = ['Journee', 'Nuit', 'H24', 'Astreinte'];
  dialogRef: MatDialogRef<EditVacationClientComponent>;
  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,


  };
  pageSize: number = 12;
  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];
  displayedColumns = [
    'number',
    'Etablissement', 'specialite',
    'Annee',
    'Libelle', 'Debut', 'Fin',
    'Tarif', 'Salaire Net', 'Salaire Brute', 'T.Reference', 'auteur', 'Edit'];

  displayedColumnsFilter = [];
  @Output() vacationEvent: EventEmitter<Vacation> = new EventEmitter<null>();
  listVacations: any[];
  viewEdit: boolean = false;
  classlist: string = 'col-12';
  vacation: Vacation;

  edit: boolean = false;
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  constructor(private vacationService: VacationClientService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private filterService: FilterService,
              private dialog: MatDialog, private notifierService: NotificationService,
              private notificationService: NotificationService,
              private clientService: ClientService,
              private pldService: PldService,
              private utilsService: UtilsService,
  ) {
    // this.vacationService.allVacations();
    this.initData();
  }

  ngOnInit() {
    this.formFilter = this.formBuilder.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    }

    this.notifierService.blockUiStop();
    this.displayedColumnsFilter = this.vacationService.displayedColumnsName();
    this.vacationService.closemodale.subscribe(
      (response: boolean) => {
        if (response) {
          if (this.dialogRef != null) {
            this.dialogRef.close();
          }
        }
      }
    );

  }

  saveOnPLD(idClient) {
    this.notificationService.onConfirm('êtes-vous sûr de vouloir enregistrer ce client sur PLD ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        this.notificationService.blockUiStart();
        this.pldService.addClient(idClient).subscribe((resp) => {
            const resultat = JSON.parse(resp);
            if (resultat.Message === 'Ajout  avec succès.') {
              const clientUpdate = {id: idClient, idPld: resultat.ID};
              this.clientService.updateIdPld(clientUpdate).subscribe(response => {
                  if (response === 'ok') {
                    this.notificationService.showNotificationSuccessCopie('bottomRight',
                      'Le client est maintenant enregistré sur PLD !');
                  } else {
                    this.notificationService.showNotificationEchecCopie('bottomRight', 'Le client n\' a pas été enregistré sur PLD !');
                  }
                },
                error => {
                  this.notificationService.showNotificationEchecCopie('bottomRight', 'Le client n\' a pas été enregistré sur PLD !');
                },
                () => {
                  this.notificationService.blockUiStop();
                  this.utilsService.redirectEspace({espace: 'vacation', id: ''});
                }
              );
            }
          },
          error => {
            this.notificationService.showNotificationEchecCopie('bottomRight', error.error.message);
            this.notificationService.blockUiStop();
          });
      }
    });
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  add() {
    this.classlist = 'col-7';
    this.viewEdit = true;
    this.vacation = null;

  }

  editAction(vacation: Vacation) {

    this.vacation = vacation;
    this.classlist = 'col-7';
    this.viewEdit = true;
  }

  editVacationModal(id: number = null): void {


    this.dialogRef = this.dialog.open(EditVacationClientComponent, {
      width: '60%',
      height: '450px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {
          this.getNewData();
        }
      }
    );

  }


  initData() {

    const listeVacations = this.activatedRoute.snapshot.data['listesVacation']['data'];
    this.length = this.activatedRoute.snapshot.data['listesVacation']['lenght'];

    // (listeVacations)
    this.dataSource = new MatTableDataSource(listeVacations);

  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }

  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();

  }

  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }

  getNewData() {
    this.notifierService.blockUiStart();
    this.vacationService.allVacations(this.data).subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource(response['data']);
        this.length = response['lenght'];
      },
      (erreur) => {
        this.notifierService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notifierService.blockUiStop();
      },
      () => {
        this.notifierService.blockUiStop();
      }
    );
  }

}
