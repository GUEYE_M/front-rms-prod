import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacationClientComponent } from './vacation-client.component';

describe('VacationClientComponent', () => {
  let component: VacationClientComponent;
  let fixture: ComponentFixture<VacationClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacationClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacationClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
