import {Route, RouterModule} from '@angular/router';
import {VacationClientComponent} from './vacation-client/vacation-client.component';
import {VacationClientResolve} from '../../../../../resolver/vacationClient.resolve';
import {EditVacationClientComponent} from './vacation-client/edit-vacation-client/edit-vacation-client.component';

const ROUTE_CONFIG: Route[] = [
  {
    path: '',
    component: VacationClientComponent,
    resolve: { listesVacation: VacationClientResolve}
  },
  {
    path: 'edit',
    component: EditVacationClientComponent,
  }
];

export const VacationClientRouting = RouterModule.forChild(ROUTE_CONFIG);
