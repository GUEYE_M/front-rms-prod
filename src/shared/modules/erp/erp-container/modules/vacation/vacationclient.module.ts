import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import {VacationClientComponent} from './vacation-client/vacation-client.component';
import {EditVacationClientComponent} from './vacation-client/edit-vacation-client/edit-vacation-client.component';
import {VacationClientRouting} from './vacationclient.routing';

@NgModule({
  declarations: [
    VacationClientComponent,
    EditVacationClientComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    VacationClientRouting
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class VacationclientModule {}
