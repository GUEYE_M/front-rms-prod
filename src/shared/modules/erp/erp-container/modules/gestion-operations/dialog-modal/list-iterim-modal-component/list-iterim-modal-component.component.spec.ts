import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListIterimModalComponentComponent } from './list-iterim-modal-component.component';

describe('ListIterimModalComponentComponent', () => {
  let component: ListIterimModalComponentComponent;
  let fixture: ComponentFixture<ListIterimModalComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListIterimModalComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListIterimModalComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
