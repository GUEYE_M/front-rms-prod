import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOffresModalComponentComponent } from './list-offres-modal-component.component';

describe('ListOffresModalComponentComponent', () => {
  let component: ListOffresModalComponentComponent;
  let fixture: ComponentFixture<ListOffresModalComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOffresModalComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOffresModalComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
