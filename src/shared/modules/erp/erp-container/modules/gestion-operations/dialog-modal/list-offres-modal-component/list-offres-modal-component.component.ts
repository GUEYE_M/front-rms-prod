import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {map, tap} from "rxjs/operators";
import {from} from "rxjs";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {SelectionModel} from "@angular/cdk/collections";

@Component({
  selector: 'app-list-offres-modal-component',
  templateUrl: './list-offres-modal-component.component.html',
  styleUrls: ['./list-offres-modal-component.component.css']
})
export class ListOffresModalComponentComponent implements OnInit {

  displayedColumns = ['select', 'number', 'Annee', 'Mois', 'Reference'];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) public sort: MatSort;
  listsource: any[] = [];

  recrutecurtSelectedTab: any[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any[],
  ) {
    (data);
    from(data).pipe(
      map(
        (item) => {
          return {

            annee: item.annee,
            mois: item.mois,
            reference: item.reference,


          };

        }),
      tap((item) => {
        this.listsource.push(item);
      })
    ).subscribe();

    this.dataSource = new MatTableDataSource(this.listsource);


  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.sort.sortChange.subscribe((e) => (e));

  }

  ngOnInit() {
  }

  filtrer(filtre: string) {
    filtre = filtre.trim();
    filtre = filtre.toLowerCase();
    this.dataSource.filter = filtre;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

}
