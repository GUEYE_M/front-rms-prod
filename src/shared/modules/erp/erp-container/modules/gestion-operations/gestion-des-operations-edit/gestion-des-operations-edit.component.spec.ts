import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionDesOperationsEditComponent } from './gestion-des-operations-edit.component';

describe('GestionDesOperationsEditComponent', () => {
  let component: GestionDesOperationsEditComponent;
  let fixture: ComponentFixture<GestionDesOperationsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionDesOperationsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionDesOperationsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
