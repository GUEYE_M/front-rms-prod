import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../../../../services/notification.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { LigneTelephoniqueService } from '../../../../../../services/ligne.telephonique.service';
import { GestionnaireInterneService } from '../../../../../../services/erp/gestionnaire-interne.service';
import { Router } from '@angular/router';
import { TacheService } from '../../../../../../services/erp/tache.service';
import { BehaviorSubject } from 'rxjs';
import { ListOffresModalComponentComponent } from '../dialog-modal/list-offres-modal-component/list-offres-modal-component.component';
import { ListIterimModalComponentComponent } from '../dialog-modal/list-iterim-modal-component/list-iterim-modal-component.component';

import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { Moment } from 'moment';
moment.locale('fr', localization);

moment.locale('fr', localization);

@Component({
	selector: 'app-gestion-des-operations-edit',
	templateUrl: './gestion-des-operations-edit.component.html',
	styleUrls: [ './gestion-des-operations-edit.component.css' ]
})
export class GestionDesOperationsEditComponent implements OnInit {
	tacheForm: FormGroup;
	edit: boolean = false;
	displayName: string;
	list_types: any;
	lignes: any;
	data_lignes: any;

	etats: any;

	gestionnaire_internes: BehaviorSubject<any[]> = new BehaviorSubject([]);
	constructor(
		private fb: FormBuilder,
		private tacheService: TacheService,
		private ligneService: LigneTelephoniqueService,
		private gestionnaireInterneService: GestionnaireInterneService,
		private dialog: MatDialog,
		private router: Router,
		private dialogRef: MatDialogRef<GestionDesOperationsEditComponent>,
		private notificationService: NotificationService,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private datePipe: DatePipe
	) {
		if (this.data) {
			this.displayName = 'Mis a jour Camapagne';
			this.notificationService.blockUiStart();

			this.tacheService.find(+this.data).subscribe((response: any) => {
				this.initform(response);
				this.tacheForm.get('id').setValue(this.data);

				//   var event = new Date(Date.parse(response.heure_debut));
				//   (event)
				//  let  d = new Date(response.heure_debut) ; d.setTime( d.getTime() - new Date().getTimezoneOffset());
				//  (d)

				let gestionnaire_interne = [ {} ];

				response.tache_gestionnaire.map((response) => {
					gestionnaire_interne.push(response.gestionnaire_interne);
				});
				this.notificationService.blockUiStop();
				this.edit = true;

				this.tacheForm.patchValue({
					tache_gestionnaire: gestionnaire_interne
				});

				gestionnaire_interne.splice(0, 1);
			});
		} else {
			this.displayName = 'Nouvelle Tache';
			this.initform();
		}
	}

	ngOnInit() {
		this.etats = this.tacheService.etats;

		this.gestionnaireInterneService.allGestionnaireInterne().subscribe((response) => {
			this.gestionnaire_internes.next(response);
		});

		this.tacheService.listTypeTache().subscribe((response: any) => {
			this.list_types = response.data;
		});

		this.ligneService.list().subscribe((response: any) => {
			this.lignes = response.data;
		});
	}
	compareWithFunc(a, b) {
		return a.id === b.id;
	}
	//cette fonction permet d'obtenir les offres lier a une ligne telephonique
	// ligneChoisi(ligne:any)
	// {
	//   this.notificationService.blockUiStart();
	//   this.tacheForm.get('ligne').setValue(ligne.id);

	//     this.ligneService.getIntemLigneBySpecialite(ligne).subscribe(
	//       (response)=>{
	//         if(response.erreur)
	//         {
	//           this.notificationService.showNotificationEchec(response.erreur)
	//           this.notificationService.blockUiStop();

	//         }
	//       else{
	//         if(ligne.appartenance == 2)
	//         {
	//          const dialogRef: MatDialogRef<ListOffresModalComponent> = this.dialog.open(ListOffresModalComponent, {
	//            width: '900px',
	//            height: '800px',
	//            data: response.data ,
	//          })
	//          dialogRef.afterClosed().subscribe(
	//            (resultat) => {

	//              if (resultat) {
	//              this.tacheForm.get('data').setValue(resultat);
	//              }

	//            }

	//          )
	//         }
	//         else{
	//           const dialogRef: MatDialogRef<ListIterimModalComponent> = this.dialog.open(ListIterimModalComponent, {
	//             width: '900px',
	//             height: '800px',
	//             data: null,
	//           })
	//           dialogRef.afterClosed().subscribe(
	//             (resultat) => {

	//               if (resultat) {
	//                 this.tacheForm.get('data').setValue(resultat);
	//               }

	//             }

	//           )
	//          }

	//       }
	//       this.notificationService.blockUiStop();

	//       }

	//     )

	// }

	initform(
		tache = {
			nom: '',
			heure_debut: '',
			heure_fin: '',
			etat: null,
			gestionnaire_interne: [],
			date_debut: '',
			date_fin: '',
			description: '',
			type: [],
			ligne_telephonique: []
		}
	) {
		this.tacheForm = this.fb.group({
			type: [ tache.type, Validators.required ],
			nom: [ tache.nom, Validators.required ],
			heure_debut: [ tache.heure_debut, Validators.required ],
			heure_fin: [ tache.heure_fin, Validators.required ],
			tache_gestionnaire: [ tache.gestionnaire_interne, Validators.required ],
			date_debut: [ tache.date_debut, Validators.required ],
			date_fin: [ tache.date_fin, Validators.required ],
			description: [ tache.description ],
			statut: [ tache.etat, Validators.required ],
			id: [],
			ligne_telephonique: [ tache.ligne_telephonique, Validators.required ],
			data: []
		});
		console.log(new Date(tache.date_debut));
	}

	save() {
		// let type = this.list_types.find(
		//   (item)=>item.id == this.tacheForm.get('type').value.id
		// )

		// let ligne = this.lignes.find(
		//   (item)=>item.id == this.tacheForm.get('ligne').value.id
		// )

		if (this.tacheForm.valid) {
			// this.tacheForm.get('type').setValue(type.id)
			// this.tacheForm.get('ligne').setValue(ligne.id)

			this.notificationService.blockUiStart();

			this.tacheService.edit(this.tacheForm.value).subscribe(
				(response: any) => {
					if (response.erreur) {
						this.notificationService.showNotificationEchecCopie('bottomLeft', response.erreur);
						this.notificationService.blockUiStop();
					} else {
						this.notificationService.showNotificationSuccessCopie('bottomLeft', response.success);
						this.dialogRef.close(true);
					}
				},
				(erreur) => {
					this.notificationService.showNotificationEchec(
						'erreur  de connexion , contacter le service IT si le probleme persiste'
					);
					this.notificationService.blockUiStop();
				},
				() => {
					this.notificationService.blockUiStop();
				}
			);
		} else {
			this.notificationService.showNotificationEchec('le champ nom campagne est obligatoire');
		}
	}
}
