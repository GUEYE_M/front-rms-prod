import {Route, RouterModule} from '@angular/router';
import {GestionDesOperationsListeComponent} from './gestion-des-operations-liste/gestion-des-operations-liste.component';
import {GestionDesOperationsEditComponent} from './gestion-des-operations-edit/gestion-des-operations-edit.component';
// import {AdminConfigComponent} from './admin-config/admin-config.component';

const GESTION_OPERATIONS: Route[] = [
  {
    path: '',
    component: GestionDesOperationsListeComponent,
    children: [
      {
        path: 'liste',
        component: GestionDesOperationsListeComponent,
      }, {
        path: 'edit',
        component: GestionDesOperationsEditComponent,
      }
    ]
  }
];

export const GestionOperationsRouting = RouterModule.forChild(GESTION_OPERATIONS);
