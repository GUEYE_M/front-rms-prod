import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { NgForm } from "@angular/forms";
import { GestionDesOperationsEditComponent } from "../gestion-des-operations-edit/gestion-des-operations-edit.component";
import { UtilsService } from "../../../../../../services/utils.service";
import { DatePipe } from "@angular/common";
import { NotificationService } from "../../../../../../services/notification.service";
import { RangeDateService } from "../../../../../../services/range.date.service";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { TacheService } from "../../../../../../services/erp/tache.service";
import { SelectionModel } from "@angular/cdk/collections";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";

import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
moment.locale('fr', localization);
@Component({
  selector: 'app-gestion-des-operations-liste',
  templateUrl: './gestion-des-operations-liste.component.html',
  styleUrls: ['./gestion-des-operations-liste.component.css']
})
export class GestionDesOperationsListeComponent implements OnInit {

  public taches: any[];
  dialogRef: MatDialogRef<GestionDesOperationsEditComponent>;
  actionInput: Boolean = false;
  action: any
  range: any = { fromDate: new Date(), toDate: new Date() };

  date_debut: number = null;
  date_fin: number = null;



  viewdatepiker: boolean = false
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];



  displayedColumns = ['select', 'number', 'Nom', 'Statut', 'Ligne', 'Type', 'Gestionnaire', 'Heure Debut', 'Heure Fin', 'Date Debut', 'Date Fin', 'Edit'];
  listActions: any;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('listPaginator', { static: false }) paginator: MatPaginator;
  @ViewChild('dateRangePicker', { static: false }) dateRangePicker;


  dataSource: MatTableDataSource<any[]>;
  displayedColumnsFilter = [];
  pageEvent: PageEvent;
  pageSize = 12;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];
  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null

  };
  locale = LOCALE
  selection = new SelectionModel<any[]>(true, []);

  constructor(
    private tacheService: TacheService,
    private dialog: MatDialog,
    private rangedateService: RangeDateService,
    private notificationService: NotificationService,
    private datePipe: DatePipe,
    private utilsService: UtilsService
  ) {

  }



  ngOnInit() {
    this.utilsService.onRevientEnHaut();
    this.listActions = this.tacheService.etats;
    this.displayedColumnsFilter = this.tacheService.displayedColumnsName();
    this.initData();
    this.tacheService.closemodale.subscribe(
      (response: boolean) => {
        if (response === true) {
          if (this.dialogRef) {
            this.dialogRef.close();
          }

        }
      }
    );
  }

  editTacheModal(id: number = null): void {
    this.dialogRef = this.dialog.open(GestionDesOperationsEditComponent, {
      width: '800px',
      height: '660px',
      panelClass: 'myapp-no-padding-dialog',
      data: id

    });

    this.dialogRef.afterClosed().subscribe(
      (response) => {
        if (response) {

          this.initData()
        }
      }
    )
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  // handler function that receives the updated date range object
  updateRange(range: Range) {
    this.range = range;
    this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd');
    this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd');
  }



  actionbtn(action: any, operation: any) {
    (operation);
    if (operation.length > 0) {
      this.notificationService.blockUiStart();
      let item = {
        etat: action,
        id: []
      };
      operation.map(
        (statut: any) => {
          item.id.push(statut.id);
        }
      );
      if (item.id.length > 0) {
        this.tacheService.updateAvancement(item).subscribe(
          (response) => {
            (response);
          },
          (erreur) => {
            this.notificationService.showNotificationEchec('erreur chargement veillez ressayer , consilter le service IT si le probleme persiste');
            this.notificationService.blockUiStop();
          },
          () => {
            this.dataSource.data.map(
              (data: any) => {
                operation.map(
                  (el) => {
                    if (el.id === data.id) {
                      data.etat = item.etat;
                    }
                  }
                );
              }
            );
            this.notificationService.showNotificationSuccess('Mise a jour effectuer avec success');
            this.notificationService.blockUiStop();
          }
        );
      }
    } else {
      this.notificationService.showNotificationEchec('veillez choisir une action a éffectuer');
    }
  }
  initData() {
    this.notificationService.blockUiStart();
    this.tacheService.list().subscribe(
      (response: any) => {

        if (response) {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.sort = this.sort;
          this.length = response.lenght;
          if (response.length > 0) {
            this.actionInput = true;
          }
        }
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur !!, veillez ressayer');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );

  }
  valutElements() {
    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.data.date_debut = null;
      this.date_fin = null;
      this.initData();
    }
    else {
      this.elements.map(
        (item) => {
          if (item == "date") {


            this.datepickerConfig()


          }

        }
      )
    }
  }
  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }
  // fonction qui envoie le formulaire de filtre en base de donnee
  onSubmitFiltre(form: NgForm) {

    this.data.filter = form.value;
    this.getNewData();
  }
  getNewData() {
    this.notificationService.blockUiStart();
    this.tacheService.list(this.data).subscribe(
      (response: any) => {
        (this.data)
        this.dataSource = new MatTableDataSource(response.data);
        this.length = response.lenght;
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
  }


  datepickerConfig() {
    const today = new Date();
    const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
    const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

    //   const resetRange = {fromDate: today, toDate: today};
    //   this.dateRangePicker.resetDates(resetRange);

    this.setupPresets();
    this.options = {
      presets: this.presets,
      format: 'dd-MM-yyyy',
      range: { fromDate: null, toDate: null },
      applyLabel: "OK",
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: false,
        hasBackdrop: false
      },
      locale: 'de-DE',
      cancelLabel: "Annuler",
      startDatePrefix: "Debut",
      endDatePrefix: "Fin",
      placeholder: "Rechercher",
      animation: true
      // excludeWeekends:true,
      // fromMinMax: {fromDate:fromMin, toDate:fromMax},
      // toMinMax: {fromDate:toMin, toDate:toMax}
    };
  }


  setupPresets() {

    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    }

    const today = new Date()
    const yesterday = backDate(1);
    const minus7 = backDate(7)
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    this.presets = [
      { presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
      { presetLabel: "Les 7 derniers jours", range: { fromDate: minus7, toDate: today } },
      { presetLabel: "Les 30 derniers jours", range: { fromDate: minus30, toDate: today } },
      { presetLabel: "Ce mois", range: { fromDate: currMonthStart, toDate: currMonthEnd } },
      { presetLabel: "Le mois dernier", range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
    ]
  }




}
