import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionDesOperationsListeComponent } from './gestion-des-operations-liste.component';

describe('GestionDesOperationsListeComponent', () => {
  let component: GestionDesOperationsListeComponent;
  let fixture: ComponentFixture<GestionDesOperationsListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionDesOperationsListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionDesOperationsListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
