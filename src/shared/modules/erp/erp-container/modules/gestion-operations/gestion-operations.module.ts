import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import {ListOffresModalComponentComponent} from './dialog-modal/list-offres-modal-component/list-offres-modal-component.component';
import {ListIterimModalComponentComponent} from './dialog-modal/list-iterim-modal-component/list-iterim-modal-component.component';
import {GestionDesOperationsListeComponent} from './gestion-des-operations-liste/gestion-des-operations-liste.component';
import {GestionDesOperationsEditComponent} from './gestion-des-operations-edit/gestion-des-operations-edit.component';
import {GestionOperationsRouting} from './gestion-operations.routings';

@NgModule({
  declarations: [
    GestionDesOperationsListeComponent,
    GestionDesOperationsEditComponent,
    ListOffresModalComponentComponent,
    ListIterimModalComponentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    GestionOperationsRouting
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})

export class GestionOperationsModule {}
