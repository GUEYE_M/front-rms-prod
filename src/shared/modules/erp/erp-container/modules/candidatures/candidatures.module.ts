import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import { CandidaturesComponent } from './candidatures/candidatures.component';
import {CandidaturesRouting} from './candidatures.routing';



@NgModule({
  declarations: [CandidaturesComponent],
  imports: [
    CommonModule,
    SharedModule,
    CandidaturesRouting,
  ],
  providers : [
  ]
})
export class CandidaturesModule { }
