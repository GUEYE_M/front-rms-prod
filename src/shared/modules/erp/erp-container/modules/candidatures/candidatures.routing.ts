import {Route, RouterModule} from '@angular/router';
import {CandidaturesComponent} from './candidatures/candidatures.component';
import {AllCandidaturesResolver} from '../../../../../resolver/allCandidatures.resolver';

const CANDIDATURES: Route[] = [
  {
    path: '',
    component: CandidaturesComponent,
    resolve: {listesCandidtures: AllCandidaturesResolver}
  },

];

export const CandidaturesRouting = RouterModule.forChild(CANDIDATURES);




