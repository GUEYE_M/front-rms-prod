import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { UtilsService } from '../../../../../../services/utils.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../../../../../services/notification.service';
import { FilterService } from '../../../../../../services/filter.service';
import { CandidatureService } from '../../../../../../services/candidature.service';
import { SelectionModel } from '@angular/cdk/collections';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { GestionnaireInterneService } from 'src/shared/services/erp/gestionnaire-interne.service';
import { BehaviorSubject } from 'rxjs';

moment.locale('fr', localization);

@Component({
	selector: 'app-candidatures',
	templateUrl: './candidatures.component.html',
	styleUrls: [ './candidatures.component.css' ]
})
export class CandidaturesComponent implements OnInit, AfterViewInit {
	formFilter: FormGroup;
	listActions = [
		{
			type: 1,
			label: 'envoyer le(s) candidature(s)'
		},
		{
			type: 2,
			label: 'non pris en compte'
		}
	];
	action: number;

	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null,
		date_debut: null,
		date_fin: null
	};

	range: Range = { fromDate: new Date(), toDate: new Date() };

	date_debut: number = null;
	date_fin: number = null;

	viewdatepiker: boolean = false;
	options: NgxDrpOptions;
	presets: Array<PresetItem> = [];

	pageSize = 12;

	length: number = 0;
	nbr_interim: number = 0;
	nbr_client: number = 0;

	pageSizeOptions: number[] = [ 12, 25, 50, 100, 150 ];
	displayedColumnsFilter = [
		{
			name: 'nom',
			displayName: 'Nom'
		},
		{
			name: 'prenom',
			displayName: 'Prenom'
		},

		{
			name: 'email',
			displayName: 'Adresse E-mail'
		},

		{
			name: 'specialite',
			displayName: 'Specialite'
		},

		{
			name: 'date',
			displayName: 'Date'
		},
		{
			name: 'mois',
			displayName: 'Mois'
		},
		{
			name: 'annee',
			displayName: 'Annee'
		},
		{
			name: 'vacation',
			displayName: 'Vacation'
		},
		{
			name: 'salaire',
			displayName: 'Salaire'
		},
		{
			name: 'statut',
			displayName: 'Statut Candidature'
		},
		{
			name: 'reference',
			displayName: 'Reference Mission'
		},

		{
			name: 'etablissement',
			displayName: 'Centre Hospitalier'
		},
		{
			name: 'envoyer',
			displayName: 'Envoyer'
		},
		{
			name: 'auteur',
			displayName: 'Auteur Validation'
		}
	];

	displayedColumns = [
		'select',
		'num',
		'Medecin',
		'Specialite',
		'Date planning',
		'Mois',
		'Annee',
		'Vacation',
		'Salaire Net',
		'Statut',
		'Reference',
		'Etablissement',
		'Envoyer'
	];

	statuts = [
		'En Attente',
		'Validee',
		'Non Retenue',
		'Non pris en compte',
		'Annulee CH',
		'Annulee Med',
		'Non Effectuee',
		'Annulee_C_AvantValidation',
		'Annulee_C_AvantValidation'
	];

	dataSource: MatTableDataSource<any[]>;
	allmois = [];
	selection = new SelectionModel<any[]>(true, []);
	locale = LOCALE;
	listsource: any[] = [];
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;
	listesCandidatures = [];
	gestionnaire_internes: BehaviorSubject<any[]> = new BehaviorSubject([]);

	constructor(
		private activatedRoute: ActivatedRoute,
		private candidatureService: CandidatureService,
		private formBuilder: FormBuilder,
		private filterService: FilterService,
		private notificationService: NotificationService,
		private router: Router,
		private datePipe: DatePipe,
		private utilsService: UtilsService,
		private gestionnaireInterneService: GestionnaireInterneService
	) {}

	ngOnInit() {
		this.formFilter = this.formBuilder.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		this.elements.splice(this.elements.indexOf('date'), 1);
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
				this.getNewData();
			}
		}

		this.allmois = this.utilsService.getMois();
		this.gestionnaireInterneService.allGestionnaireInterne().subscribe((response) => {
			this.gestionnaire_internes.next(response);
		});

		this.initData();
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	actionbtn(action: number, recruteurSelection) {
		if (action === 1) {
			this.notificationService.blockUiStart();
			if (recruteurSelection.length > 0) {
				this.candidatureService.activerLesCandidaturesMedecins(JSON.stringify(recruteurSelection)).subscribe(
					(response: any) => {
						if (response.erreur) {
							this.notificationService.showNotificationEchec(response.erreur);
							// this.router.navigate(['/RMS-Admin/redirection/', 'candidatures', 1]);
							this.notificationService.blockUiStop();
						} else {
							this.notificationService.showNotificationSuccess(response.success);
							this.dataSource.data.map((data: any) => {
								recruteurSelection.map((el) => {
									if (el.id === data.id) {
										data.envoyer = true;
									}
								});
							});
							this.notificationService.blockUiStop();
						}
					},
					(error2) => {
						// this.router.navigate(['/RMS-Admin/redirection/', 'candidatures', 1]);
						this.notificationService.blockUiStop();
					}
				);
			} else {
				this.notificationService.showNotificationEchec('Aucun element selectionnez');
			}
		} else if (action === 2) {
			let isTrue = 1;
			const tmp = recruteurSelection.reduce((acum, currenValue) => {
				if (currenValue.satatut_candidature.toLowerCase() === 'en attente') {
					acum.push({
						statut: currenValue.satatut_candidature,
						id: currenValue.id,
						stat: 'Non pris en compte'
					});
				} else {
					isTrue = 0;
				}
				return acum;
			}, []);
			if (isTrue) {
				this.notificationService.blockUiStart();
				this.candidatureService.CandidaturesNonPrisesEnCharge(tmp).subscribe(
					(response: any) => {
						if (response.erreur) {
							this.notificationService.showNotificationEchec(response.erreur);
							// this.router.navigate(['/RMS-Admin/redirection/', 'candidatures', 1]);
							this.notificationService.blockUiStop();
						} else {
							this.notificationService.showNotificationSuccess(response.data);
							this.dataSource.data.map((data: any) => {
								recruteurSelection.map((el) => {
									if (el.id === data.id) {
										data.satatut_candidature = 'Non pris en compte';
									}
								});
							});
							this.notificationService.blockUiStop();
						}
					},
					(error2) => {
						// this.router.navigate(['/RMS-Admin/redirection/', 'candidatures', 1]);
						this.notificationService.blockUiStop();
					}
				);
			} else {
				this.notificationService.showNotificationEchec(
					"Veuillez choisir que les candidatures n'ont traitées ."
				);
			}
		} else {
		}
	}

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	initData() {
		const dataSnapshot = this.activatedRoute.snapshot.data['listesCandidtures'];
		this.listesCandidatures = dataSnapshot['data'];
		this.length = dataSnapshot['lenght'];
		this.nbr_client = dataSnapshot['nbrClient'];
		this.nbr_interim = dataSnapshot['nbrInterim'];
		this.notificationService.blockUiStop();
		// this.listesCandidatures = this.listesCandidatures.map((x) => {
		//   x.date = moment(x.date);
		//   return x;
		// });
		this.candidatureService.allCandidaturesSubject.next(this.listesCandidatures);
		this.candidatureService.allCandidaturesSubject.subscribe((response) => {
			this.dataSource = new MatTableDataSource(response);
		});
	}

	valutElements() {
		// creation du filtre depuis le storageonSubmitFiltre
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.data.date_debut = null;
			this.data.date_fin = null;
			this.initData();
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
			this.elements.map((item) => {
				if (item === 'date') {
					this.datepickerConfig();
				}
			});
		}
	}

	datepickerConfig() {
		const today = new Date();
		const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
		const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

		//   const resetRange = {fromDate: today, toDate: today};
		//   this.dateRangePicker.resetDates(resetRange);

		this.setupPresets();
		this.options = {
			presets: this.presets,
			format: 'dd-MM-yyyy',
			range: { fromDate: null, toDate: null },
			applyLabel: 'OK',
			calendarOverlayConfig: {
				shouldCloseOnBackdropClick: false,
				hasBackdrop: false
			},
			locale: 'de-DE',
			cancelLabel: 'Annuler',
			startDatePrefix: 'Debut',
			endDatePrefix: 'Fin',
			placeholder: 'Rechercher',
			animation: true
			// excludeWeekends:true,
			// fromMinMax: {fromDate:fromMin, toDate:fromMax},
			// toMinMax: {fromDate:toMin, toDate:toMax}
		};
	}

	setupPresets() {
		const backDate = (numOfDays) => {
			const today = new Date();
			return new Date(today.setDate(today.getDate() - numOfDays));
		};

		const today = new Date();
		const yesterday = backDate(1);
		const minus7 = backDate(7);
		const minus30 = backDate(30);
		const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
		const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
		const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

		this.presets = [
			{ presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
			{ presetLabel: 'Les 7 derniers jours', range: { fromDate: minus7, toDate: today } },
			{ presetLabel: 'Les 30 derniers jours', range: { fromDate: minus30, toDate: today } },
			{ presetLabel: 'Ce mois', range: { fromDate: currMonthStart, toDate: currMonthEnd } },
			{ presetLabel: 'Le mois dernier', range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
		];
	}

	updateRange(range: Range) {
		//this.notificationService.blockUiStart();

		this.range = range;

		this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd');
		this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd');
	}

	// fonction qui envoie le formulaire de filtre en base de donnee

	onSubmitFiltre() {
		this.data.filter = this.formFilter.value;

		console.log(this.elements)
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.getNewData();
	}

	pagination(event: any) {
		this.pageSize = event.pageSize;
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = this.pageSize;

		this.getNewData();
	}

	getNewData() {
		this.notificationService.blockUiStart();
		this.candidatureService.getAllCandidatures(this.data).subscribe(
			(response: any) => {
				this.candidatureService.allCandidaturesSubject.next(response['data']);
				// this.dataSource = new MatTableDataSource(response['data']);
				this.length = response['lenght'];
				this.nbr_client = response['nbrClient'];
				this.nbr_interim = response['nbrInterim'];
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur recherche!! veuillez réessayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}
}
