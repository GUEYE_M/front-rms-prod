import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import {ProspectionRouting} from './prospection.routing';
import { ListProspectionInterimComponent } from './list-prospection-interim/list-prospection-interim.component';
import { ListProspectionClientComponent } from './list-prospection-client/list-prospection-client.component';
import {ViewDateRapelProdpectionModalComponent} from './view-date-rapel-prodpection-modal/view-date-rapel-prodpection-modal.component';



@NgModule({
  declarations: [
    ListProspectionInterimComponent,
    ListProspectionClientComponent,
    ViewDateRapelProdpectionModalComponent,
   
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProspectionRouting,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  
})
export class ProspectionModule { }
