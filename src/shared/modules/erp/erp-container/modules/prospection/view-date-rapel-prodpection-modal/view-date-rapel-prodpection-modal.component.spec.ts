import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDateRapelProdpectionModalComponent } from './view-date-rapel-prodpection-modal.component';

describe('ViewDateRapelProdpectionModalComponent', () => {
  let component: ViewDateRapelProdpectionModalComponent;
  let fixture: ComponentFixture<ViewDateRapelProdpectionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDateRapelProdpectionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDateRapelProdpectionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
