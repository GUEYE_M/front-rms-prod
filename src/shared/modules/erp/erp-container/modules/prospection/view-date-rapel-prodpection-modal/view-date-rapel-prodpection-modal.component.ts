import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-view-date-rapel-prodpection-modal',
  templateUrl: './view-date-rapel-prodpection-modal.component.html',
  styleUrls: ['./view-date-rapel-prodpection-modal.component.css']
})
export class ViewDateRapelProdpectionModalComponent implements OnInit {

  date  : any
  constructor( @Inject(MAT_DIALOG_DATA) public data: any) {
    if(data)
    {
      this.date = data
    }

  }

  ngOnInit() {
  }

}
