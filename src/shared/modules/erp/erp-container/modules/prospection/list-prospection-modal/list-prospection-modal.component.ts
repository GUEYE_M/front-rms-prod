import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSort, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { MessagaProspectionService } from 'src/shared/services/messaga-prospection.service';
import { InterimService } from 'src/shared/services/interim/interim.service';
import { ClientService } from 'src/shared/services/recruteur/client.service';
import { UtilsService } from 'src/shared/services/utils.service';
import { NotificationService } from 'src/shared/services/notification.service';
import { FormGroup, NgForm } from '@angular/forms';
import { ViewMessageProdpectionModalComponent } from '../../prospection/view-message-prodpection-modal/view-message-prodpection-modal.component';
import { EditProspectionComponent } from '../../client-wrapper/client/single-client-container/prospection-interim/edit-prospection/edit-prospection.component';

@Component({
    selector: 'app-list-prospection-modal',
    templateUrl: './list-prospection-modal.component.html',
    styleUrls: ['./list-prospection-modal.component.css']
})
export class ListProspectionModalComponent implements OnInit {

    user: number;
    dialogRef: MatDialogRef<EditProspectionComponent>;
    message_list: any[];
    showLoadClient: boolean;
    actionInput: Boolean = false;
    action: string;
    list_message = [];
    public form: FormGroup;
    elements = [];
    datafiltre = {
        filter: null,
        date_debut: null,
        date_fin: null,
        interim: null,
        client: null
    };
    pageSize = 5;
    length: number = 0;
    pageSizeOptions: number[] = [5, 12, 25, 50];

    displayedColumnsFilter = [
        {
            name: "type",
            label: "Type"
        },
        {
            name: "rappeler",
            label: "A Rappeler"
        },

    ];

    displayedColumns = ['number', 'type', 'date rapel', 'date enregistrement', 'date mis a jour', 'auteur', 'commentaire', 'edit'];


    dataSource: MatTableDataSource<any[]> = new MatTableDataSource([]);
    selection = new SelectionModel<any[]>(true, []);

    listsource: any[] = [];
    listrecruteur: any[] = null;
    types = [];

    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild('listPaginator', { static: false }) paginator: MatPaginator;
    data: { id: number; recruteur: any[]; client: number };


    constructor(private prospectionService: MessagaProspectionService,
        private interimService: InterimService,
        private clientService: ClientService,
        private dialog: MatDialog,
        private utilsService: UtilsService,
        private notificationService: NotificationService,
        public dialogRef_list: MatDialogRef<ListProspectionModalComponent>,
        @Inject(MAT_DIALOG_DATA) public item: any
    ) {

        if (item.interim) {
            this.datafiltre.interim = item.id

        }
        else {
            this.datafiltre.client = item.id
            this.listrecruteur = item.recruteur
        }
        this.getData();
    }

    ngOnInit() {
        this.prospectionService.listTypeProspection().subscribe(
            (response: any) => {

                if (response) {

                    this.types = response.data;


                }
            }
        );

    }

    editMessageModal(id: number = null): void {
        let data = {
            id: id,
            recruteur: this.listrecruteur,
            client: +this.datafiltre.client,
            user_id: this.item.id,
            modal: true
        };
        this.dialogRef = this.dialog.open(EditProspectionComponent, {
            width: '800px',
            height: '580px',
            panelClass: 'myapp-no-padding-dialog',
            data: data

        });

        this.dialogRef.afterClosed().subscribe(
            (response) => {
                if (response) {
                    this.getData()
                }
            }
        )
    }

    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(
                row => this.selection.select(row));
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }


    // fonction qui envoie le formulaire de filtre en base de donnee

    onSubmitFiltre(form: NgForm) {

        this.datafiltre.filter = form.value;

        // (this.datafiltre);


        this.getData();
    }

    getData() {
          this.notificationService.blockUiStart()
        this.prospectionService.listMessageByProspect(this.datafiltre).subscribe(
            (response: any) => {
                this.notificationService.blockUiStop();
                this.prospectionService.list_message_by_prospect = response.data.data;
                this.dataSource = new MatTableDataSource(response.data.data);
              
                // this.prospectionService.emitMessageByProspect();
                this.length = response.data.data.length;

                this.dataSource.paginator = this.paginator;
                
                
              
            },
            (erreur) => {
                this.notificationService.showNotificationEchec('erreur recherche!! veuillez ressayer a nouveau');
                this.notificationService.blockUiStop();
            },
            () => {
                this.notificationService.blockUiStop();
            }
        );
    }

    valutElements() {
        //  (this.elements.length)
        if (this.elements.length === 0) {
            this.datafiltre.filter = null;
            this.datafiltre.date_debut = null;
            this.datafiltre.date_fin = null;
            this.getData()
        }
    }

    message(message: any) {
        let dialogRef: MatDialogRef<ViewMessageProdpectionModalComponent>;
        dialogRef = this.dialog.open(ViewMessageProdpectionModalComponent, {
            width: '800px',
            height: '550px',
            panelClass: 'myapp-no-padding-dialog',
            data: message

        });
    }
}
