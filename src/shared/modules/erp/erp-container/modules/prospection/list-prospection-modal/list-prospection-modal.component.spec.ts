import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProspectionModalComponent } from './list-prospection-modal.component';

describe('ListProspectionModalComponent', () => {
  let component: ListProspectionModalComponent;
  let fixture: ComponentFixture<ListProspectionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProspectionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProspectionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
