import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ViewDateRapelProdpectionModalComponent } from '../view-date-rapel-prodpection-modal/view-date-rapel-prodpection-modal.component';
import { ViewMessageProdpectionModalComponent } from '../view-message-prodpection-modal/view-message-prodpection-modal.component';
import { SelectionModel } from '@angular/cdk/collections';
import { MessagaProspectionService } from '../../../../../../services/messaga-prospection.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';

import { LOCALE } from 'src/shared/utils/date_filter_config';
moment.locale('fr', localization);
@Component({
  selector: 'app-list-prospection-client',
  templateUrl: './list-prospection-client.component.html',
  styleUrls: ['./list-prospection-client.component.css']
})
export class ListProspectionClientComponent implements OnInit {



  constructor(private prospectionService: MessagaProspectionService,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private datePipe: DatePipe) { }
  action: string;
  dataSource: MatTableDataSource<any[]>;
  dialogRef: MatDialogRef<ViewMessageProdpectionModalComponent>;
  dialogRefDate: MatDialogRef<ViewDateRapelProdpectionModalComponent>;

  selection = new SelectionModel<any[]>(true, []);
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('dateRangePicker', { static: false }) dateRangePicker;

  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 100];


  range: Range = { fromDate: new Date(), toDate: new Date() };

  date_debut: number = null
  date_fin: number = null

  viewdatepiker: boolean = false
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];


  types = [];
  elements = []
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null,
    interim: false


  }
  displayedColumnsFilter = []

  locale = LOCALE

  // MatPaginator Output
  pageEvent: PageEvent;
  pageSize = 12;
  displayedColumns = ['number', 'nom_etablisement', 'recruteur', 'message', 'type', 'a_rappeler', 'auteur', 'date'];
  ngOnInit() {

    this.prospectionService.listTypeProspection().subscribe(
      (response: any) => {

        this.types = response.data

      }
    );
    this.displayedColumnsFilter = this.prospectionService.displayFilterNameForClient();

    this.initData(this.data);




  }



  message(message: any) {

    this.dialogRef = this.dialog.open(ViewMessageProdpectionModalComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: message

    });
  }


  dateRapel(date: any) {

    this.dialogRefDate = this.dialog.open(ViewDateRapelProdpectionModalComponent, {
      width: '400px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: date

    });
  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize

    this.getNewData()



  }




  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre(form: NgForm) {



    this.data.filter = form.value

    this.getNewData()

  }

  getNewData() {
    this.notificationService.blockUiStart()
    this.prospectionService.allProspectionInterim(this.data).subscribe(
      (response: any) => {


        this.dataSource = new MatTableDataSource(response.data);
        this.length = +response.lenght



      },
      (erreur) => {
        this.notificationService.showNotificationEchec("erreur recherche!! veillez ressayer a nouveau")
        this.notificationService.blockUiStop()
      },
      () => {
        this.notificationService.blockUiStop()
      }
    )
  }


  initData(data: any = null) {
    this.notificationService.blockUiStart();
    this.prospectionService.allProspectionInterim(data).subscribe(
      (response) => {

        if (!response.erreur) {
          this.dataSource = new MatTableDataSource(response.data)
          this.length = +response.lenght
        }
        else {
          this.notificationService.showNotificationEchec(response.erreur);
        }


      },
      (erreur) => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec("erreur chargement, veillez ressayer");
      },
      () => {
        this.notificationService.blockUiStop();
      }
    )
  }


  valutElements() {



    //  (this.elements.length)
    if (this.elements.length === 0) {
      this.data.filter = null
      this.data.date_debut = null
      this.data.date_fin = null


      this.initData(this.data)
    }
    else {
      this.elements.map(
        (item) => {
          if (item == "date") {


            this.datepickerConfig()



          }

        }
      )
    }


  }

  datepickerConfig() {
    const today = new Date();
    const fromMin = new Date(today.getFullYear(), today.getMonth() - 2, 1);
    const fromMax = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const toMin = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const toMax = new Date(today.getFullYear(), today.getMonth() + 2, 0);

    //   const resetRange = {fromDate: today, toDate: today};
    //   this.dateRangePicker.resetDates(resetRange);

    this.setupPresets();
    this.options = {
      presets: this.presets,
      format: 'dd-MM-yyyy',
      range: { fromDate: null, toDate: null },
      applyLabel: "OK",
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: false,
        hasBackdrop: false
      },
      locale: 'de-DE',
      cancelLabel: "Annuler",
      startDatePrefix: "Debut",
      endDatePrefix: "Fin",
      placeholder: "Rechercher",
      animation: true
      // excludeWeekends:true,
      // fromMinMax: {fromDate:fromMin, toDate:fromMax},
      // toMinMax: {fromDate:toMin, toDate:toMax}
    };
  }


  setupPresets() {

    const backDate = (numOfDays) => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    }

    const today = new Date()
    const yesterday = backDate(1);
    const minus7 = backDate(7)
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    this.presets = [
      { presetLabel: "Aujourd'hui", range: { fromDate: today, toDate: today } },
      { presetLabel: "Les 7 derniers jours", range: { fromDate: minus7, toDate: today } },
      { presetLabel: "Les 30 derniers jours", range: { fromDate: minus30, toDate: today } },
      { presetLabel: "Ce mois", range: { fromDate: currMonthStart, toDate: currMonthEnd } },
      { presetLabel: "Le mois dernier", range: { fromDate: lastMonthStart, toDate: lastMonthEnd } }
    ]
  }


  updateRange(range: Range) {

    //this.notificationService.blockUiStart();

    this.range = range;


    this.data.date_debut = this.datePipe.transform(this.range.fromDate, 'yyyy-MM-dd')
    this.data.date_fin = this.datePipe.transform(this.range.toDate, 'yyyy-MM-dd')


  }


}
