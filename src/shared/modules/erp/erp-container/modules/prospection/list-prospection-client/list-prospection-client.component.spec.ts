import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProspectionClientComponent } from './list-prospection-client.component';

describe('ListProspectionClientComponent', () => {
  let component: ListProspectionClientComponent;
  let fixture: ComponentFixture<ListProspectionClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProspectionClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProspectionClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
