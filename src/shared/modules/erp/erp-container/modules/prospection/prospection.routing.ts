import {Route, RouterModule} from '@angular/router';
import {ListProspectionInterimComponent} from './list-prospection-interim/list-prospection-interim.component';
import {ViewMessageProdpectionModalComponent} from './view-message-prodpection-modal/view-message-prodpection-modal.component';
import {ViewDateRapelProdpectionModalComponent} from './view-date-rapel-prodpection-modal/view-date-rapel-prodpection-modal.component';
import {ListProspectionClientComponent} from './list-prospection-client/list-prospection-client.component';

const PROSPECTION_CONFIG: Route[] = [
  {
    path: 'interimaires',
    component: ListProspectionInterimComponent,
  },
  {
    path: 'clients',
    component: ListProspectionClientComponent,
  },
  {
    path: 'view-commentaire',
    component: ViewMessageProdpectionModalComponent,
  },
  {
    path: 'view-rappel',
    component: ViewDateRapelProdpectionModalComponent,
  },
];

export const ProspectionRouting = RouterModule.forChild(PROSPECTION_CONFIG);



