import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProspectionInterimComponent } from './list-prospection-interim.component';

describe('ListProspectionInterimComponent', () => {
  let component: ListProspectionInterimComponent;
  let fixture: ComponentFixture<ListProspectionInterimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProspectionInterimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProspectionInterimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
