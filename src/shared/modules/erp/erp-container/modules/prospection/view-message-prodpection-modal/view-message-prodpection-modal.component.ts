import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-view-message-prodpection-modal',
  templateUrl: './view-message-prodpection-modal.component.html',
  styleUrls: ['./view-message-prodpection-modal.component.css']
})
export class ViewMessageProdpectionModalComponent implements OnInit {

  message: any;
  constructor( @Inject(MAT_DIALOG_DATA) public data: any) {
    if(data)
    {
      this.message = data
    }

  }

  ngOnInit() {
  }

}
