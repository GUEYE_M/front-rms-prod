import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMessageProdpectionModalComponent } from './view-message-prodpection-modal.component';

describe('ViewMessageProdpectionModalComponent', () => {
  let component: ViewMessageProdpectionModalComponent;
  let fixture: ComponentFixture<ViewMessageProdpectionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMessageProdpectionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMessageProdpectionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
