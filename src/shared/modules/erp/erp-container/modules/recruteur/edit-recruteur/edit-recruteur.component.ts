import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, retry, startWith } from 'rxjs/operators';
import { ListClientModalComponent } from '../list-client-modal/list-client-modal.component';
import { RecruteurService } from '../../../../../../services/recruteur/recruteur.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiVilleService } from '../../../../../../services/api_ville_service';
import { ClientService } from '../../../../../../services/recruteur/client.service';
import { UserService } from '../../../../../../services/user.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { Client } from '../../../../../../models/recruteur/Client.model';
import { Recruteur } from '../../../../../../models/recruteur/Recruteur.model';
import { Commune } from '../../../../../../models/commune.model';
import { Observable } from 'rxjs';
import { UtilsService } from '../../../../../../services/utils.service';

@Component({
	selector: 'app-edit-recruteur',
	templateUrl: './edit-recruteur.component.html',
	styleUrls: [ './edit-recruteur.component.css' ]
})
export class EditRecruteurComponent implements OnInit {
	formControlValue = '';
	auteur: any;

	recruteurForm: FormGroup;
	clientForm: FormGroup;
	edit: boolean = false;
	recruteur: Recruteur;
	rc: {} = [ {} ];
	idReceuteur: number;
	communes: any[];
	recruteurClient = [];
	displayName: string;

	filteredCommune: Observable<any[]>;
	listClientSelected: Client[];
	listClient: Client[];
	listClientModal: Client[];
	dialogRef: MatDialogRef<ListClientModalComponent>;

	constructor(
		public dialogRefEdit: MatDialogRef<EditRecruteurComponent>,
		private fb: FormBuilder,
		private recruterService: RecruteurService,
		private route: ActivatedRoute,
		private router: Router,
		private apiVilleService: ApiVilleService,
		private clientService: ClientService,
		private dialog: MatDialog,
		private userService: UserService,
		private utilsService: UtilsService,
		private notificationService: NotificationService,
		@Inject(MAT_DIALOG_DATA) public data: Client
	) {
		if (this.data) {
			const id = this.data;
			this.displayName = 'Mise à jour du recruteur';
			this.notificationService.blockUiStart();
			this.recruterService.getClientForRecruteur(+id).pipe(retry(3)).subscribe((reponse: any) => {
				if (!reponse.erreur) {
					this.recruteurClient = reponse.data;

					this.recruterService.find(+id).pipe(retry(3)).subscribe(
						(response) => {
							this.iniform(response);
							this.recruteurForm.patchValue({
								id: +id
							});
						},
						(error) => {
							this.notificationService.showNotificationEchec('erreur du server, veillez ressayer');
							this.notificationService.blockUiStop();
						},
						() => {
							this.notificationService.blockUiStop();
						}
					);
				} else {
					this.notificationService.showNotificationEchec('erreur du server, veillez ressayer');
					this.notificationService.blockUiStop();
				}
			});
			this.edit = true;
			this.idReceuteur = +id;
			//
		} else {
			this.displayName = 'Nouveau Recruteur';
			this.iniform();
		}
	}

	ngOnInit() {
		this.auteur = this.userService.getCurrentUser()['username'];
		this.allClient();
	}
	//  validators(user:string){
	//       this.userService.findByUernameAndEmail(user).subscribe(
	//         (response)=>{
	//           if(response.body=== true)
	//           {
	//             ("identifiant existe");
	//           }
	//         }
	//       )
	//}
	iniform(
		recruteur = {
			user: {
				nom: '',
				prenom: '',
				telephone: '',
				description: '',
				remarque: '',
				enabled: false,
				username: '',
				email: '',
				civilite: '',
				auteur: '',
				adresse: '',
				complement_adresse: '',
				cp: '',
				ville: ''
			},
			id: '',
			fonction: '',
			recruteur_client: []
		}
	) {
		this.recruteurForm = this.fb.group({
			user: this.fb.group({
				civilite: [ recruteur.user.civilite, Validators.required ],
				auteur: [],
				nom: [ recruteur.user.nom, Validators.required ],
				prenom: [ recruteur.user.prenom, Validators.required ],
				telephone: [ recruteur.user.telephone, Validators.required ],
				remarque: [ recruteur.user.remarque ],
				description: [ recruteur.user.description ],
				enabled: [ recruteur.user.enabled ],
				username: [ recruteur.user.username, Validators.required ],
				email: [ recruteur.user.email, Validators.required ],
				adresse: [ recruteur.user.adresse, Validators.required ],
				complement_adresse: [ recruteur.user.complement_adresse ],
				cp: [ recruteur.user.cp, Validators.required ],
				ville: [ recruteur.user.ville, Validators.required ]
			}),
			id: [],
			fonction: [ recruteur.fonction, Validators.required ],
			recruteurClient: this.fb.array(
				this.recruteurClient.map((item) =>
					this.fb.group({
						id: item.id,
						nom_etablissement: item.nom_etablissement,
						raison_sociale: item.raison_sociale,
						telephone: item.telephone
					})
				)
			),
			deleteClient: this.fb.array([])
		});
	}

	onCommune(commune: string) {
		this.apiVilleService.findbyCommune(commune).subscribe((response: any) => {
			if (response) {
				this.communes = response.data;
				this.filteredCommune = this.recruteurForm
					.get('user')
					.get('ville')
					.valueChanges.pipe(
						startWith(''),
						map((value) =>
							this.communes.filter((commune) => commune.properties.label.toLowerCase().includes(value))
						)
					);
			}
		});
		// this.inputcommune = false
		// )
		// )
		//  this.apiVilleService.findbyCommune(this.recruteurForm.get("user").get("adresse").get("ville").get("commune").value).subscribe
		//  (
		//    (response)=>{
		//      if(response.status=== 200)
		//      {
		//       (response.body)
		//      } else{
		//        ("ville n'existe pas ");
		//      }
		//    }
		//  )
	}
	onblurCommune() {
		if (this.communes.length === 0) {
			this.recruteurForm.get('user').get('ville').reset();
			('ville incorecte');
		} else {
			this.communes.filter((c) => {
				if (c.properties.label == this.recruteurForm.get('user').get('ville').value) {
					this.recruteurForm.get('user').get('cp').setValue(c.properties.postcode);
				}
			});
		}
	}
	ouvrirlistClientModal(): void {
		const listClientModal = this.listClient.filter(
			(item) => !this.recruteurForm.get('recruteurClient').value.some((c) => c.id === item.id)
		);
		this.dialogRef = this.dialog.open(ListClientModalComponent, {
			width: '900px',
			height: '600px',
			data: listClientModal
		});
		this.dialogRef.afterClosed().subscribe((resultat) => {
			if (resultat) {
				resultat.map((client) => {
					(<FormArray>this.recruteurForm.get('recruteurClient')).push(
						this.fb.group({
							id: client.id,
							nom_etablissement: client.nom_etablissement,
							raison_sociale: client.raison_sociale,
							telephone: client.telephone,
							emaiCh: client.telephone
						})
					);
				});
			}
		});
	}

	retireClient(index: number) {
		if (this.edit) {
			(<FormArray>this.recruteurForm.get('deleteClient')).push(
				this.fb.group({
					id: (<FormArray>this.recruteurForm.get('recruteurClient')).controls[index].value.id
				})
			);
		}
		(<FormArray>this.recruteurForm.get('recruteurClient')).removeAt(index);
	}

	allClient() {
		this.clientService.clientSubject.pipe(retry(3)).subscribe((client) => {
			this.listClient = client;
		});
	}
	saveRecruteur() {
		if (!this.edit) this.recruteurForm.get('user').get('auteur').setValue(this.auteur);
		this.notificationService.blockUiStart();
		this.recruterService.create(this.recruteurForm.value).subscribe(
			(response: any) => {
				this.notificationService.blockUiStop();
				if (response.erreur) {
					this.recruterService.closemodale.next(false);
					this.notificationService.showNotificationEchec(response.erreur);
					this.notificationService.blockUiStop();
				} else {
					this.dialogRefEdit.close(true);
					this.notificationService.showNotificationSuccess(response.success);
				}
			},
			() => {
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}
}
