import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListClientModalComponent } from './list-client-modal.component';

describe('ListClientModalComponent', () => {
  let component: ListClientModalComponent;
  let fixture: ComponentFixture<ListClientModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListClientModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListClientModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
