import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { EditRecruteurComponent } from '../edit-recruteur/edit-recruteur.component';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../../../services/notification.service';
import { UserService } from '../../../../../../services/user.service';
import { ClientService } from '../../../../../../services/recruteur/client.service';
import { RecruteurService } from '../../../../../../services/recruteur/recruteur.service';
import { HttpClient } from '@angular/common/http';
import { SelectionModel } from '@angular/cdk/collections';
import { Recruteur } from '../../../../../../models/recruteur/Recruteur.model';
import { UtilsService } from '../../../../../../services/utils.service';
import { ListeRecruteurInterimComponent } from '../../client-wrapper/client/modal-client/liste-recruteur-interim/liste-recruteur-interim.component';
import { ListClientModalComponent } from '../list-client-modal/list-client-modal.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SendinblueService } from 'src/shared/services/sendinblue.service';
import { FilterService } from '../../../../../../services/filter.service';

@Component({
	selector: 'app-recruteur',
	templateUrl: './recruteur.component.html',
	styleUrls: [ './recruteur.component.css' ]
})
export class RecruteurComponent implements OnInit {
	formFilter: FormGroup;
	recruteurlist: Recruteur[];
	// tslint:disable-next-line:variable-name
	liste_client = [];
	selection = new SelectionModel<any[]>(true, []);
	rechercheForm: FormGroup;
	listsource: any[] = [];
	dialogRef: MatDialogRef<EditRecruteurComponent>;
	filtreEmailInput = false;
	filterexite = false;
	closeResult: string;
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;
	recrutecurtSelectedTab: any[];
	// tslint:disable-next-line:ban-types
	actionInput: Boolean = false;
	action: number;
	loader: any;
	displayedColumns = [
		'select',
		'number',
		'civilite',
		'nom',
		'prenom',
		'telephone',
		'email',
		'fonction',
		'date',
		'active',
		'etablissement',
		'Edit'
	];
	dataSource: MatTableDataSource<any[]>;
	length = 0;
	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null
	};
	// tslint:disable-next-line:no-inferrable-types
	pageSize: number = 12;

	pageSizeOptions: number[] = [ 12, 25, 50, 100 ];
	displayedColumnsFilter = [];

	listActions: any[] = [ { value: 1, viewValue: 'Envoyer Identifiants' } ];

	constructor(
		private recruteurService: RecruteurService,
		private filterService: FilterService,
		private fb: FormBuilder,
		private http: HttpClient,
		private clientService: ClientService,
		private userService: UserService,
		private notificationService: NotificationService,
		private router: Router,
		private utilsService: UtilsService,
		private dialog: MatDialog,
		private sendInblueeService: SendinblueService,
		private modalService: NgbModal
	) {}

	ngOnInit() {
		this.formFilter = this.fb.group({});
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		// recuperation du filtre du sauvegarde
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		if (filterValues) {
			if (filterValues) {
				if (this.filterService.sendFilters(this.formFilter)) {
					this.data.filter = this.formFilter.value;
					this.getNewData();
				}
			}
		} else {
			this.initData();
		}
		this.displayedColumnsFilter = this.recruteurService.displayedColumnsName();
		this.recruteurService.closemodale.subscribe((response: boolean) => {
			if (response) {
				this.dialogRef.close();
			}
		});
		this.rechercheForm = this.fb.group({
			recherche: this.fb.array([])
		});
	}

	addRcherche() {
		(this.rechercheForm.get('recherche') as FormArray).push(
			this.fb.group({
				name: []
			})
		);
	}

	actionbtn(action: number, recruteurSelection: any) {
		const item = {
			etat: action,
			id: []
		};
		recruteurSelection.map((statut: any) => {
			item.id.push(statut.user.id);
		});

		if (action === 1) {
			this.sendIdentifiant(item.id);
		}
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;

		const numRows = this.dataSource.data.length;

		return numSelected === numRows;
	}

	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	sendIdentifiant(id: any) {
		let data = {
			type: true,
			data: id
		};

		this.notificationService.blockUiStart();
		this.sendInblueeService.sendMailIdentifiants(data).subscribe((response) => {
			this.notificationService.blockUiStop();
			if (!response.erreur) {
				this.notificationService.showNotificationSuccess(response.success);
			} else {
				this.notificationService.showNotificationEchec(response.erreur);
			}
		});
	}

	editRecruteurModal(id: number = null): void {
		this.dialogRef = this.dialog.open(EditRecruteurComponent, {
			width: '800px',
			height: '550px',
			panelClass: 'myapp-no-padding-dialog',
			data: id
		});

		this.dialogRef.afterClosed().subscribe((res) => {
			if (res) {
				this.getNewData();
			}
		});
	}

	onActiveOuDesactiveUser(idUser, action) {
		const thiss = this;
		const donnees = { id: idUser, etat: action };
		this.notificationService.onConfirm('Êtes-vous sûr de vouloir continuer ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				action = JSON.parse(action);
				thiss.notificationService.blockUiStart();
				thiss.userService.activerOuDesactive(donnees).subscribe(
					(response: any) => {
						thiss.notificationService.blockUiStop();
						thiss.initData();
						if (!response.erreur) {
							thiss.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
							thiss.dialog.closeAll();
						} else {
							thiss.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
						}
					},
					(error) => {
						thiss.notificationService.showNotificationEchecCopie('bottomRight', error.message);
						thiss.utilsService.redirectRecruteursListe();
						thiss.notificationService.blockUiStop();
					}
				);
			} else {
				thiss.utilsService.redirectRecruteursListe();
			}
		});
	}

	initData() {
		this.notificationService.blockUiStart();
		this.recruteurService.allRecruteur(this.data).subscribe(
			(response: any) => {
				this.dataSource = new MatTableDataSource(response.data);
				this.length = response.lenght;
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.initData();
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
		}
	}

	// fonction qui envoie le formulaire de filtre en base de donnee

	onSubmitFiltre() {
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = this.formFilter.value;
		this.getNewData();
	}

	pagination(event: any) {
		this.data.offset = event.pageIndex;
		this.data.limit = event.pageSize;

		this.getNewData();
	}

	getNewData() {
		this.notificationService.blockUiStart();
		this.recruteurService.allRecruteur(this.data).subscribe(
			(response: any) => {
				this.dataSource = new MatTableDataSource(response.data);
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	showClients(clients) {
		const dialogRef = this.dialog.open(ListClientModalComponent, {
			width: '700px',
			height: '600px',
			panelClass: 'myapp-no-padding-dialog',
			data: clients
		});
	}

	open(content) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			}
		);
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
}
