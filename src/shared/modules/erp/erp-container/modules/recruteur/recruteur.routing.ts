import {Route, RouterModule} from '@angular/router';
import {RecruteurComponent} from './recruteur/recruteur.component';
import {EditRecruteurComponent} from './edit-recruteur/edit-recruteur.component';

const USERS_CONFIG: Route[] = [
  {
    path: 'liste',
    component: RecruteurComponent,
  },
  {
    path: 'edit',
    component: EditRecruteurComponent,
  }
];

export const RecruteurRouting = RouterModule.forChild(USERS_CONFIG);



