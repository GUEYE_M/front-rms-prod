import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecruteurComponent } from './recruteur/recruteur.component';
import {RecruteurRouting} from './recruteur.routing';
import { EditRecruteurComponent } from './edit-recruteur/edit-recruteur.component';
import {SharedModule} from '../../../../shared/shared.module';
import { ListClientModalComponent } from './list-client-modal/list-client-modal.component';



@NgModule({
  declarations: [
    RecruteurComponent,
    EditRecruteurComponent,
    ListClientModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RecruteurRouting,
  ],
  entryComponents: [
    ListClientModalComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class RecruteurModule { }
