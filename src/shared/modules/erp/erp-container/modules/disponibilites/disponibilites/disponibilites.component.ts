import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialogRef, MatDialog} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators, NgForm} from '@angular/forms';
import {NgxDrpOptions, PresetItem, Range} from 'ngx-mat-daterange-picker';
import {DatePipe} from '@angular/common';
import {UtilsService} from 'src/shared/services/utils.service';
import {DisponibiliteService} from 'src/shared/services/interim/disponibilite.service';
import {NotificationService} from 'src/shared/services/notification.service';
import {from} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {DateDisponibiliteInterimModalComponent} from '../date-disponibilite-interim-modal/date-disponibilite-interim-modal.component';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';
import {LocaleConfig} from 'ngx-daterangepicker-material';
import {LOCALE} from 'src/shared/utils/date_filter_config';
import {FilterService} from '../../../../../../services/filter.service';

moment.locale('fr', localization);

@Component({
  selector: 'app-disponibilites',
  templateUrl: './disponibilites.component.html',
  styleUrls: ['./disponibilites.component.css']
})
export class DisponibilitesComponent implements OnInit {
  formFilter: FormGroup;
  dataSourceInitial = [];
  uri_listes_plannings = 'plannings/listes-filter';
  dateDebutInitial: any;
  statuts = ['A Pourvoir', 'Pourvue', 'Annuler Ch'];
  displayedColumns = ['number', 'interim', 'email', 'date', 'mois', 'annee', 'ville', 'departement'];
  dialogRedate: MatDialogRef<DateDisponibiliteInterimModalComponent>;
  dataSource: any = new MatTableDataSource();

  selection = new SelectionModel<any[]>(true, []);
  listActions = ['Supprimer', 'Desactiver'];

  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,
    date_debut: null,
    date_fin: null


  };
  locale = LOCALE;
  listsource: any[] = [];
  pageSize: number = 12;

  range: Range = {fromDate: new Date(), toDate: new Date()};
  length: number = 0;
  pageSizeOptions: number[] = [12, 25, 50, 100];


  displayedColumnsFilter = [
    {
      name: 'date',
      label: 'Date'
    },
    {
      name: 'adresse',
      label: 'Adresse'
    },
    {
      name: 'nom',
      label: 'Nom'
    },
    {
      name: 'prenom',
      label: 'Prenom'
    },
    {
      name: 'email',
      label: 'E-Mail'
    },
    {
      name: 'specialite',
      label: 'Specialite'
    },
    {
      name: 'telephone',
      label: 'Telephone'
    },
    {
      name: 'ville',
      label: 'Ville'
    },
    {
      name: 'departement',
      label: 'Departement'
    }
  ];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;

  constructor(private activatedRoute: ActivatedRoute,
              private disponibiliteService: DisponibiliteService,
              private notifierService: NotificationService,
              private datePipe: DatePipe,
              private utilsService: UtilsService,
              private dialog: MatDialog,
              private fb: FormBuilder,
              private filterService: FilterService,
  ) {


  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getData();
      }
    }
    this.getData();

  }


  // ===================================================================================================================================
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.data.date_debut = null;
      this.data.date_fin = null;
      this.getData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }


  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {

    if (this.formFilter.value['date']) {
      this.data.date_debut = this.datePipe.transform(this.formFilter.value['date'].startDate, 'yyyy-MM-dd');
      this.data.date_fin = this.datePipe.transform(this.formFilter.value['date'].endDate, 'yyyy-MM-dd');
    }
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;


    this.getData();

  }

  pagination(event: any) {

    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;

    this.getData();


  }

  getData() {
    this.notifierService.blockUiStart();
    this.disponibiliteService.listAll(this.data).subscribe(
      (response: any) => {


        this.custumlist(response.data);

        this.length = response.lenght;


      },
      (erreur) => {
        this.notifierService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notifierService.blockUiStop();
      },
      () => {
        this.notifierService.blockUiStop();
      }
    );
  }


  custumlist(list: any) {

    this.listsource = [];
    let m = this.utilsService.convertNumnerMoisToString();
    from(list).pipe(
      map(
        (item: any) => {
          let t = item.mois.split('-');


          return {
            id: item.id,
            id_interim: item.id_interim,
            email: item.email,
            civilite: item.civilite,
            nom: item.nom,
            prenom: item.prenom,
            mois: m[t[0]],
            annee: t[1],
            ville: item.ville,
            departement: item.departement,
            telephone: item.telephone

          };

        }),
      tap((el) => {
        this.listsource.push(el);
      })
    ).subscribe();

    this.dataSource = new MatTableDataSource(this.listsource);

  }


  getDate(id_interim: number) {


    this.dialogRedate = this.dialog.open(DateDisponibiliteInterimModalComponent, {
      width: '1000px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: id_interim

    });

  }

}
