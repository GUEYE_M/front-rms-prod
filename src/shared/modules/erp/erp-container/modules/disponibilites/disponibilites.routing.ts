import {Route, RouterModule} from '@angular/router';
import {DisponibilitesComponent} from './disponibilites/disponibilites.component';

const DISPONIBILITES: Route[] = [
  {path: '', component: DisponibilitesComponent},
];

export const DisponibilitesRouting = RouterModule.forChild(DISPONIBILITES);



