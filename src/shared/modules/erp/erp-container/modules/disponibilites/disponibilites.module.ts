import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import { DisponibilitesComponent } from './disponibilites/disponibilites.component';
import {DisponibilitesRouting} from './disponibilites.routing';
import { DateDisponibiliteInterimModalComponent } from './date-disponibilite-interim-modal/date-disponibilite-interim-modal.component';



@NgModule({
  declarations: [
    DisponibilitesComponent,
    DateDisponibiliteInterimModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DisponibilitesRouting,
  ],
  entryComponents:[DateDisponibiliteInterimModalComponent],
  
})
export class DisponibilitesModule { }
