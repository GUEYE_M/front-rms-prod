import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateDisponibiliteInterimModalComponent } from './date-disponibilite-interim-modal.component';

describe('DateDisponibiliteInterimModalComponent', () => {
  let component: DateDisponibiliteInterimModalComponent;
  let fixture: ComponentFixture<DateDisponibiliteInterimModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateDisponibiliteInterimModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateDisponibiliteInterimModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
