import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatSort, MAT_DIALOG_DATA, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { NotificationService } from 'src/shared/services/notification.service';
import { DisponibiliteService } from 'src/shared/services/interim/disponibilite.service';

@Component({
  selector: 'app-date-disponibilite-interim-modal',
  templateUrl: './date-disponibilite-interim-modal.component.html',
  styleUrls: ['./date-disponibilite-interim-modal.component.css']
})
export class DateDisponibiliteInterimModalComponent implements OnInit {

  displayedColumns = ['number','date'];
  dataSource: any = new MatTableDataSource();
 
  selection = new SelectionModel<any[]>(true, []);
  

  length :number = 0;
  pageSizeOptions: number[] = [5, 12,25, 50, 100];
  pageSize:number = 5


  @ViewChild(MatSort,{static:false}) sort: MatSort;
  @ViewChild('listPaginator',{static:false}) paginator: MatPaginator;
  constructor(@Inject(MAT_DIALOG_DATA) public id: any,
  private notifierService: NotificationService,
 
  private disponibiliteService: DisponibiliteService

) {

   
  if(id)
  {
    
    this.initData()
    
  
  }


}

ngOnInit() {
 

}



// ===================================================================================================================================
ngAfterViewInit() {

 // this.dataSource.paginator = this.paginator;


}

initData()
{



this.notifierService.blockUiStart()
this.disponibiliteService.getDisponibilite(this.id).subscribe(
(response:any)=>{
  this.notifierService.blockUiStop()
if(response)
{
this.dataSource = new MatTableDataSource(response);
this.length = response.length
this.dataSource.paginator = this.paginator


}

else
{
this.notifierService.showNotificationEchec("echec chargement")
}





},
(erreur)=>{
this.notifierService.showNotificationEchec("erreur recherche!! veillez ressayer a nouveau")
this.notifierService.blockUiStop()
},
()=>{
this.notifierService.blockUiStop()
}
)


}

}



