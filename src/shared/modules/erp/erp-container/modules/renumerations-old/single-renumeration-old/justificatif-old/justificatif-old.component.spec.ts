import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JustificatifOldComponent } from './justificatif-old.component';

describe('JustificatifOldComponent', () => {
  let component: JustificatifOldComponent;
  let fixture: ComponentFixture<JustificatifOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JustificatifOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JustificatifOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
