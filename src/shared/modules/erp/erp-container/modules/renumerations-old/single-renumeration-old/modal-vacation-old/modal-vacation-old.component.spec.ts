import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVacationOldComponent } from './modal-vacation-old.component';

describe('ModalVacationOldComponent', () => {
  let component: ModalVacationOldComponent;
  let fixture: ComponentFixture<ModalVacationOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalVacationOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVacationOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
