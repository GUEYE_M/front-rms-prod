import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../../services/notification.service';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {UtilsService} from '../../../../../../../services/utils.service';
import {JustificatifService} from '../../../../../../../services/interim/justificatif.service';

@Component({
  selector: 'app-update-justificatif-old',
  templateUrl: './update-justificatif-old.component.html',
  styleUrls: ['./update-justificatif-old.component.css']
})
export class UpdateJustificatifOldComponent implements OnInit {


  form: FormGroup;
    idReanumation: any;
  justificatif: any;
  constructor(
      private notificationService: NotificationService,
      private dialog: MatDialog,
      private fb: FormBuilder,
      private router: Router,
      private utilsService: UtilsService,
      @Inject(MAT_DIALOG_DATA) public data: any,
      private justificatifService: JustificatifService,
  ) {}

  ngOnInit() {
    this.idReanumation = this.data.idReanumation;
    this.justificatif = this.data.justificatif;
    this.initform();
  }

  initform(params = {
    adresseInterim: this.justificatif.adresseInterim,
    adresseClient: this.justificatif.adresseClient,
    km: this.justificatif.km,
    trajet: this.justificatif.trajet,
    ik: this.justificatif.ik
  }) {
    this.form = this.fb.group(
        {
            adresseMedecin: [params.adresseInterim, [Validators.required]],
            adresseHopital: [params.adresseClient, [Validators.required]],
            nombreKm: [params.km],
            nombreTrajet: [params.trajet],
            coefIk: [params.ik]
        });
  }
  save() {
    const params = {id: this.idReanumation, data: this.form.value};
    this.justificatifService.new(params).subscribe((response: any) => {
          if (!response.erreur) {

            this.utilsService.redirectEspace({espace: 'renumerations-old', id: this.idReanumation});
            this.dialog.closeAll();
          } else {
            this.notificationService.showNotificationEchec(response.erreur);
          }

        },
        error => {
          this.notificationService.showNotificationEchec('Erreur lors du chargement de la requête.Veuillez réessayer !');
        });
  }
  udpate() {
    const params = {id: this.justificatif.id, data: this.form.value};
    this.justificatifService.update(params).subscribe((response) => {
          if (!response['erreur']) {
              this.utilsService.redirectEspace({espace: 'renumerations-old', id: this.idReanumation});
              this.dialog.closeAll();
          } else {
            this.notificationService.showNotificationEchec(response['erreur']);
          }

        },
        error => {
          this.notificationService.showNotificationEchec('Erreur lors du chargement de la requête.Veuillez réessayer !');
        });
  }
  mappy() {
    window.open('http://fr.mappy.com/#/5/M2/Ls/TItinerary/IFR' +  this.justificatif.adresseInterim + ' |TO ' +  this.justificatif.adresseClient + ', _blank,toolbar=0, location=0,directories=0, status=0, scrollbars=1, resizable=1, copyhistory=0, menuBar=0, width=1500, height=944, left=200, top=0');
  }

}
