import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRenumerationOldComponent } from './single-renumeration-old.component';

describe('SingleRenumerationOldComponent', () => {
  let component: SingleRenumerationOldComponent;
  let fixture: ComponentFixture<SingleRenumerationOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleRenumerationOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleRenumerationOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
