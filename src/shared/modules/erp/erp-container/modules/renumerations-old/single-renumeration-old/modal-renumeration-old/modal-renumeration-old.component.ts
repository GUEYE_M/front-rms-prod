import {Component, Inject, OnInit} from '@angular/core';
import {PaieService} from '../../../../../../../services/recruteur/paie.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {CandidatureService} from '../../../../../../../services/candidature.service';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-modal-renumeration-old',
  templateUrl: './modal-renumeration-old.component.html',
  styleUrls: ['./modal-renumeration-old.component.css']
})
export class ModalRenumerationOldComponent implements OnInit {
  renumerationPrestaKeys = [];
  data: any;
  renumerationPresta: any;
  idRenumerationPresta: number;
  renumerationFraisKeys = [];
  vacations: any;
  vacationsKeys: any;
  renumerationExiste = false;
  totalKeys = [];
  constructor(
      private paieService: PaieService,
      private notificationService: NotificationService,
      private candidatureService: CandidatureService,
      @Inject(MAT_DIALOG_DATA) public idRenum: any
  ) { }

  ngOnInit() {
    this.notificationService.blockUiStart();

    this.candidatureService.findCandidaturesByIdRenum(this.idRenum).subscribe((resp) => {
      this.notificationService.blockUiStop();
      this.vacationsKeys = Object.keys(resp['candidatures'][0]);
      this.vacations = resp['candidatures'];
      this.paieService.findByMM(resp['idMM']).subscribe((resultat) => {
        if (!resultat['erreur']) {
          let keys: any;
          this.renumerationPresta = resultat['data'];
          keys = Object.keys(this.renumerationPresta);
          keys.forEach((element) => {
            if(element !== 'datePaiement' && element !== 'modePaiement' && element !== 'totalPrestation' && element !== 'etat' && element !== 'total' && element !== 'remarque' && element !== 'id'  && element !== 'voiture' && element !== 'totalTarif') {
              this.renumerationFraisKeys.push(element);
            }
            if(element === 'datePaiement' || element === 'totalPrestation' || element === 'etat') {
              this.renumerationPrestaKeys.push(element);
            }
            if(element === 'total' || element === 'modePaiement' || element === 'remarque' || element === 'totalTarif') {
              this.totalKeys.push(element);
            }
          });
          this.renumerationExiste = true;
        }
      });
    });
  }

  stringToFloat(item) {
    item = item !== 'null' ? item : 0;
    return parseFloat(item);
  }

  isNullReturnZero(item) {
    if (item === null || item === '' || !item || item === 'null')
      return 0;
    return item;
  }

  formaterItem(item) {
    switch (item) {
      case 'datePaiementFrais':
        item = 'Date paiement frais';
        break;
      case 'totalFrais':
        item = 'Total frais';
        break;
      case 'etatFrais':
        item = 'Etat frais';
        break;
      case 'montantTrain':
        item = 'Montant train';
        break;
      case 'montantAvion':
        item = 'Montant avion';
        break;
      case 'montantTaxi':
        item = 'Montant taxi';
        break;
      case 'montantBus':
        item = 'Montant bus';
        break;
      case 'montantCovoiturage':
        item = 'Montant covoiturage';
        break;
      case 'montantParking':
        item = 'Montant parking';
        break;
      case 'montantLocationVoiture':
        item = 'Montant location voiture';
        break;
      case 'montantTicketPeage':
        item = 'Montant ticket peage';
        break;
      case 'montantHotel':
        item = 'Montant Hôtel';
        break;
      case 'montantRepas':
        item = 'Montant repas';
        break;
      case 'montantIk':
        item = 'Montant ik';
        break;
      case 'modePaiement':
        item = 'Mode paiement';
        break;
      case 'totalTarif':
        item = 'Total tarif';
        break;
      case 'datePaiement':
        item = 'Date paiement';
        break;
      case 'totalPrestation':
        item = 'Total prestation';
        break;
      case 'etat':
        item = 'État';
        break;


    }
    return item;
  }

}
