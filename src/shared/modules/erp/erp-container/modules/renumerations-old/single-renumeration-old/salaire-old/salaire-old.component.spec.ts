import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaireOldComponent } from './salaire-old.component';

describe('SalaireOldComponent', () => {
  let component: SalaireOldComponent;
  let fixture: ComponentFixture<SalaireOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaireOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaireOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
