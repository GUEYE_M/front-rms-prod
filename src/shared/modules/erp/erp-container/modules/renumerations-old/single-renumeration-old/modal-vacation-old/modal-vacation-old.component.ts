import {Component, Inject, OnInit} from '@angular/core';
import {PaieService} from '../../../../../../../services/recruteur/paie.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {CandidatureService} from '../../../../../../../services/candidature.service';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  selector: 'app-modal-vacation-old',
  templateUrl: './modal-vacation-old.component.html',
  styleUrls: ['./modal-vacation-old.component.css']
})
export class ModalVacationOldComponent implements OnInit {

  data: any;
  renumerationPresta: any;
  vacations: any;
  vacationsKeys: any;

  constructor(
      private paieService: PaieService,
      private notificationService: NotificationService,
      private candidatureService: CandidatureService,
      private activatedRoute: ActivatedRoute,
      private dialog: MatDialog,
      @Inject(MAT_DIALOG_DATA) public idRenum: any

  ) { }

  ngOnInit() {
    this.notificationService.blockUiStart();
    this.candidatureService.findCandidaturesByIdRenum(this.idRenum).subscribe((resp: any) => {
      this.notificationService.blockUiStop();
      this.vacationsKeys = Object.keys(resp['candidatures'][0]);
      this.vacations = resp['candidatures'];
    });
  }

}
