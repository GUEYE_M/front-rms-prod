import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRenumerationOldComponent } from './modal-renumeration-old.component';

describe('ModalRenumerationOldComponent', () => {
  let component: ModalRenumerationOldComponent;
  let fixture: ComponentFixture<ModalRenumerationOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRenumerationOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRenumerationOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
