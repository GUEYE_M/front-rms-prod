import { Component, OnInit } from '@angular/core';
import {IMAGES_API_URL} from '../../../../../../../utils/images_api_url';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilsService} from '../../../../../../../services/utils.service';
import {PaieService} from '../../../../../../../services/recruteur/paie.service';
import {JustificatifService} from '../../../../../../../services/interim/justificatif.service';
import {CandidatureService} from '../../../../../../../services/candidature.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmDialogModel} from '../../../../../../../models/Confirm.dialog';
import {UpdateJustificatifOldComponent} from '../update-justificatif-old/update-justificatif-old.component';
import {ModalVacationOldComponent} from '../modal-vacation-old/modal-vacation-old.component';
import {ModalRenumerationOldComponent} from '../modal-renumeration-old/modal-renumeration-old.component';

@Component({
  selector: 'app-justificatif-old',
  templateUrl: './justificatif-old.component.html',
  styleUrls: ['./justificatif-old.component.css']
})
export class JustificatifOldComponent implements OnInit {
  fileUrl = IMAGES_API_URL;

  data: any;
  renumerationPresta: any;
  vacations: any;
  fd: FormData = new FormData();
  files = [];
  fileDifferentImage = this.fileUrl + 'images/fichier.png';

  typefichierExiste = [];

  vacationsKeys: any;
  justificatifs: any;
  idContrat: any;
  idRenum: any;
  dialogRef: MatDialogRef<ModalVacationOldComponent>;
  dialogRef1: MatDialogRef<ModalRenumerationOldComponent>;
  dialogRef2: MatDialogRef<UpdateJustificatifOldComponent>;

  pieceForm: FormGroup;
  pieceItemsForm: FormGroup;


  constructor(
      private utilsService: UtilsService,
      private paieService: PaieService,
      private justificatifService: JustificatifService,
      private candidatureService: CandidatureService,
      private fb: FormBuilder,
      private notificationService: NotificationService,
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.initPieceForm();
    this.idRenum = this.activatedRoute.snapshot.params['id'];
    this.findJustificatif(this.idRenum);
  }

  onModalListeCandidatures() {
    this.dialogRef = this.dialog.open(ModalVacationOldComponent, {
      width: '40%',
      height: '50%',
      panelClass: 'myapp-no-padding-dialog',
      data: this.idRenum
    });
  }

  onModalRenumeration() {
    this.dialogRef1 = this.dialog.open(ModalRenumerationOldComponent, {
      width: '45%',
      height: '80%',
      panelClass: 'myapp-no-padding-dialog',
      data: this.idRenum
    });
  }

  onModalUpdateJustificatif() {
    const donnees = {idReanumation: this.idRenum, justificatif: this.justificatifs};
    this.dialogRef2 = this.dialog.open(UpdateJustificatifOldComponent, {
      width: '20%',
      height: '60%',
      panelClass: 'myapp-no-padding-dialog',
      data: donnees
    });
  }

  siNullRenderZero(item) {
    return this.utilsService.siNullRenderZero(item);
  }


  initPieceForm() {
    this.pieceForm = this.fb.group({
      items: this.fb.array([this.initItemRows()])
    });
  }

  get formArr() {
    return this.pieceForm.get('items') as FormArray;
  }

  initItemRows() {
    this.pieceItemsForm = this.fb.group({
      typefichier: ['', Validators.required],
    });
    return this.pieceItemsForm;
  }

  addNewRow() {
    this.formArr.push(this.initItemRows());
  }

  deleteRow(index: number) {
    this.formArr.removeAt(index);
  }

  getFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      const fileType = event.target.files[0].type.split('/');
      const file = event.target.files[0];
      reader.onload = (e: any) => {
        if (fileType.includes('video') === false) {
          this.pieceForm.value.items.forEach((element) => {
            this.fd.append(element.typefichier, file);
          });
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  save() {
    const tableau = {'id': this.justificatifs.id, 'files': this.fd};
    this.notificationService.blockUiStart();
    this.justificatifService.creerPiece(tableau).subscribe(resp => {
          this.notificationService.blockUiStop();
        },
        err => {
          this.notificationService.blockUiStop();
        },
        () => {
          this.utilsService.redirectEspace({espace: 'renumerations-old', id: this.idRenum});
        });
  }

  onDelete(item) {
    this.notificationService.onConfirm('Êtes-vous sûr de vouloir supprimer ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        this.notificationService.blockUiStart();
        this.justificatifService.delete(item).subscribe(resp => {
              this.notificationService.blockUiStop();
            },
            err => {
              this.notificationService.blockUiStop();
            },
            () => {
              this.findJustificatif(this.idRenum);
            });
      }});
  }

  actionJustificatif() {
    this.notificationService.onConfirm('Relancer le mail de demande de justificarif ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        this.notificationService.blockUiStart();
        const data = {
          id: this.justificatifs.id
        };
        this.justificatifService.relanceJistificatif(data).subscribe(
            (response) => {
              if (response.erreur) {
              } else {
                this.notificationService.showNotificationSuccess(response.success);
                this.justificatifs.nombreRelance = this.justificatifs.nombreRelance + 1;
              }
            },
            (erreur) => {
              this.notificationService.blockUiStop();
              this.notificationService.showNotificationEchec('erreur server veillez ressayer');
            },
            () => {
              this.notificationService.blockUiStop();
            }
        );
      }
    });
  }


  onActifOuInactif(justificatif, statut) {

    const message = '';
    let content = '';


    if (statut === true) {
      content = 'Êtes-vous sûr de valider les justificatifs ?? ';
    } else {
      content = 'Êtes-vous sûr d\'annuler les justificatifs ?? ';
    }
    const dialogData = new ConfirmDialogModel(content, message);

    this.notificationService.onConfirm(content);
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        this.notificationService.blockUiStart();

        const data = {
          id: justificatif.id,
          action: statut,

        };


        this.justificatifService.valideOrInvalide(data).subscribe(
            (response: any) => {
              if (response.erreur) {
                this.notificationService.showNotificationEchec(response.erreur);

              } else {
                this.justificatifs.actif = statut;
                this.notificationService.showNotificationSuccess(response.success);

              }
            },
            (erreur) => {

            },
            () => {
              this.notificationService.blockUiStop();
            }
        );
      }
    });
  }

  findJustificatif(idReanumeration) {
    this.justificatifService.findMMByIdRenumeration(idReanumeration).subscribe((response) => {
            this.justificatifs = response['data'];

        },
        error => {
        });
  }

}
