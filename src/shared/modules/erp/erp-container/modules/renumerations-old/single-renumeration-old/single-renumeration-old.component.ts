import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {JustificatifService} from '../../../../../../services/interim/justificatif.service';
import {PaieService} from '../../../../../../services/recruteur/paie.service';

@Component({
  selector: 'app-single-renumeration-old',
  templateUrl: './single-renumeration-old.component.html',
  styleUrls: ['./single-renumeration-old.component.css']
})
export class SingleRenumerationOldComponent implements OnInit {
  id: any;
  data: any;
  constructor(
      private activatedRoute: ActivatedRoute,
      private paieService: PaieService,
      private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.getInterimAndRef(this.id);
    }
  }
  getInterimAndRef(id) {
    this.notificationService.blockUiStart();
    this.paieService.findInterimAndRefByIdRenumeration(id).subscribe((response: any) => {
          this.notificationService.blockUiStop();
          this.data = response.data;
        },
        error => {
          this.notificationService.blockUiStop();
        });
  }
}
