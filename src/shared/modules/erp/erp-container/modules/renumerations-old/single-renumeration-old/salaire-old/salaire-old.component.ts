import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {PaieService} from '../../../../../../../services/recruteur/paie.service';
import {CandidatureService} from '../../../../../../../services/candidature.service';
import {ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {UpdateSalaireOldComponent} from '../update-salaire-old/update-salaire-old.component';

@Component({
  selector: 'app-salaire-old',
  templateUrl: './salaire-old.component.html',
  styleUrls: ['./salaire-old.component.css']
})
export class SalaireOldComponent implements OnInit {

  renumerationPrestaKeys = [];
  data: any;
  renumerationPresta: any;
  idRenumerationPresta: number;
  renumerationFraisKeys = [];
  vacations: any;
  vacationsKeys: any;
  renumerationExiste: boolean;
  totalKeys = [];
  dialogRef: MatDialogRef<UpdateSalaireOldComponent>;

  constructor(
      private paieService: PaieService,
      private candidatureService: CandidatureService,
      private activatedRoute: ActivatedRoute,
      private notificationService: NotificationService,
      private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.idRenumerationPresta = this.activatedRoute.snapshot.params['id'];
    if (this.idRenumerationPresta) {
      this.getCandidatureByRenumeration(this.idRenumerationPresta);
    }
  }

  updateSalaire(): void {
    this.dialogRef = this.dialog.open(UpdateSalaireOldComponent, {
      width: '50%',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
      data: this.data
    });
  }

  stringToFloat(item) {
    item = item !== 'null' ? item : 0;
    return parseFloat(item);
  }

  getCandidatureByRenumeration(id) {
    this.candidatureService.findCandidaturesByIdRenumeration(id).subscribe((resp) => {
      this.vacationsKeys = Object.keys(resp['candidatures'][0]);
      this.vacations = resp['candidatures'];
      this.notificationService.blockUiStart();
      this.paieService.findByMM(resp['idMM']).subscribe((resultat) => {
            this.notificationService.blockUiStop();
            if (!resultat['erreur']) {
              let keys: any;
              this.renumerationPresta = resultat['data'];
              this.data = {'idContrat': this.idRenumerationPresta, 'renumerationPresta': this.renumerationPresta};
              keys = Object.keys(this.renumerationPresta);
              keys.forEach((element) => {
                if (element !== 'datePaiement' && element !== 'totalTarif' && element !== 'modePaiement' && element !== 'totalPrestation' && element !== 'etat' && element !== 'total' && element !== 'remarque' && element !== 'id' && element !== 'voiture') {
                  this.renumerationFraisKeys.push(element);
                }
                if (element === 'datePaiement' || element === 'totalPrestation' || element === 'etat') {
                  this.renumerationPrestaKeys.push(element);
                }
                if (element === 'total' || element === 'modePaiement' || element === 'remarque' || element === 'totalTarif') {
                  this.totalKeys.push(element);
                }
              });
              this.renumerationExiste = true;
            } else {
              this.renumerationExiste = false;
            }
          },
          error1 => {
            this.notificationService.blockUiStop();
            this.renumerationExiste = false;
          });
    });
  }

  isNullReturnZero(item) {
    if (item === null || item === '' || !item || item === 'null')
      return 0;
    return item;
  }

  formaterItem(item) {
    switch (item) {
      case 'datePaiementFrais':
        item = 'Date paiement frais';
        break;
      case 'totalFrais':
        item = 'Total frais';
        break;
      case 'etatFrais':
        item = 'Etat frais';
        break;
      case 'montantTrain':
        item = 'Montant train';
        break;
      case 'montantAvion':
        item = 'Montant avion';
        break;
      case 'montantTaxi':
        item = 'Montant taxi';
        break;
      case 'montantBus':
        item = 'Montant bus';
        break;
      case 'montantCovoiturage':
        item = 'Montant covoiturage';
        break;
      case 'montantParking':
        item = 'Montant parking';
        break;
      case 'montantLocationVoiture':
        item = 'Montant location voiture';
        break;
      case 'montantTicketPeage':
        item = 'Montant ticket peage';
        break;
      case 'montantHotel':
        item = 'Montant Hôtel';
        break;
      case 'montantRepas':
        item = 'Montant repas';
        break;
      case 'montantIk':
        item = 'Montant ik';
        break;
      case 'modePaiement':
        item = 'Mode paiement';
        break;
      case 'totalTarif':
        item = 'Total tarif';
        break;
      case 'datePaiement':
        item = 'Date paiement';
        break;
      case 'totalPrestation':
        item = 'Total prestation';
        break;
      case 'etat':
        item = 'État';
        break;


    }
    return item;
  }
}
