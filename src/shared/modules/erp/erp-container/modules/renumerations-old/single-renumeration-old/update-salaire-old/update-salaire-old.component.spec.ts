import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSalaireOldComponent } from './update-salaire-old.component';

describe('UpdateSalaireOldComponent', () => {
  let component: UpdateSalaireOldComponent;
  let fixture: ComponentFixture<UpdateSalaireOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSalaireOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSalaireOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
