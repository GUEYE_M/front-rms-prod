import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateJustificatifOldComponent } from './update-justificatif-old.component';

describe('UpdateJustificatifOldComponent', () => {
  let component: UpdateJustificatifOldComponent;
  let fixture: ComponentFixture<UpdateJustificatifOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateJustificatifOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateJustificatifOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
