import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import { RenumerationsOldComponent } from './renumerations-old/renumerations-old.component';
import {RenumerationOldRouting} from './renumerations-old.routing';
import { SingleRenumerationOldComponent } from './single-renumeration-old/single-renumeration-old.component';
import { SalaireOldComponent } from './single-renumeration-old/salaire-old/salaire-old.component';
import { JustificatifOldComponent } from './single-renumeration-old/justificatif-old/justificatif-old.component';
import { UpdateJustificatifOldComponent } from './single-renumeration-old/update-justificatif-old/update-justificatif-old.component';
import { ModalRenumerationOldComponent } from './single-renumeration-old/modal-renumeration-old/modal-renumeration-old.component';
import { ModalVacationOldComponent } from './single-renumeration-old/modal-vacation-old/modal-vacation-old.component';
import { UpdateSalaireOldComponent } from './single-renumeration-old/update-salaire-old/update-salaire-old.component';



@NgModule({
  declarations: [
      RenumerationsOldComponent,
    SingleRenumerationOldComponent,
    SalaireOldComponent,
    JustificatifOldComponent,
    UpdateJustificatifOldComponent,
    ModalRenumerationOldComponent,
    ModalVacationOldComponent,
    UpdateSalaireOldComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RenumerationOldRouting,

  ],
  entryComponents: [
      UpdateJustificatifOldComponent,
      ModalRenumerationOldComponent,
      ModalVacationOldComponent,
      UpdateSalaireOldComponent
  ]
})
export class RenumerationsOldModule { }
