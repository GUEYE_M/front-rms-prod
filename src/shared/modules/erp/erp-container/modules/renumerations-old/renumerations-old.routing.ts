import {Route, RouterModule} from '@angular/router';
import {RenumerationsOldComponent} from './renumerations-old/renumerations-old.component';
import {SingleRenumerationOldComponent} from './single-renumeration-old/single-renumeration-old.component';

const RENUMERATION_OLD_CONFIG: Route[] = [
  {path: '', component: RenumerationsOldComponent},
  {path: ':id/show', component: SingleRenumerationOldComponent},

];

export const RenumerationOldRouting = RouterModule.forChild(RENUMERATION_OLD_CONFIG);



