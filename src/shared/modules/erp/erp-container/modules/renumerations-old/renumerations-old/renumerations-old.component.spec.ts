import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenumerationsOldComponent } from './renumerations-old.component';

describe('RenumerationsOldComponent', () => {
  let component: RenumerationsOldComponent;
  let fixture: ComponentFixture<RenumerationsOldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenumerationsOldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenumerationsOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
