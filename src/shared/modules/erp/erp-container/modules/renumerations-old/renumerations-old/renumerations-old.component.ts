import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { UtilsService } from '../../../../../../services/utils.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../../../services/notification.service';
import { PaieService } from '../../../../../../services/recruteur/paie.service';
import { SelectAutocompleteComponent } from 'select-autocomplete';
import { SelectionModel } from '@angular/cdk/collections';
import { EditInterimComponent } from '../../interim/edit-interim/edit-interim.component';
import { SpecialiteService } from 'src/shared/services/specialite.service';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { FilterService } from '../../../../../../services/filter.service';

@Component({
	selector: 'app-renumerations-old',
	templateUrl: './renumerations-old.component.html',
	styleUrls: [ './renumerations-old.component.css' ]
})
export class RenumerationsOldComponent implements OnInit {
	formFilter: FormGroup;
	dialogRef: MatDialogRef<EditInterimComponent>;
	dataSourceInitial = [];
	view: boolean = false;
	actionInput: Boolean = false;
	lengthList: any;
	action: string;
	pageIndex = 0;
	dataSource: any;
	selection = new SelectionModel<any[]>(true, []);
	listSpeciliteFilter = [];
	@ViewChild(SelectAutocompleteComponent, { static: false })
	multiSelect: SelectAutocompleteComponent;
	@ViewChild(MatSort, { static: true })
	sort: MatSort;
	@ViewChild('listPaginator', { static: false })
	paginator: MatPaginator;
	listActions: any[];
	displayedColumns = [
		'num',
		'interim',
		'etablissement',
		'annee',
		'mois',
		'total',
		'prestation',
		'frais',
		'totalTarif',
		'remarque',
		'action'
	];
	displayedColumnsFilter = [
		{
			name: 'reference',
			label: 'Reference'
		},
		{
			name: 'nom',
			label: 'Nom'
		},
		{
			name: 'prenom',
			label: 'Prenom'
		},
		{
			name: 'mois',
			label: 'Mois'
		},
		{
			name: 'annee',
			label: 'Annee'
		}
		// {
		//   name: 'etablissement',
		//   label: 'CH',
		// },
		// {
		//   name: 'specialite',
		//   label: 'Specialite',
		// },
	];
	pageSize = 12;
	length: number = 0;
	elements = [];
	data = {
		offset: 0,
		limit: 12,
		filter: null
	};
	selected: [];
	//  displayedColumnsFilter =  []

	pageSizeOptions: number[] = [ 12, 25, 50, 100, 150 ];

	allmois = [];

	constructor(
		private paieService: PaieService,
		private notificationService: NotificationService,
		private router: Router,
		private fb: FormBuilder,
		private filterService: FilterService,
		private dialog: MatDialog,
		private utilsService: UtilsService,
		private specialiteService: SpecialiteService
	) {}

	ngOnInit() {
		this.formFilter = this.fb.group({});
		// recuperation du filtre du sauvegarde
		this.elements = JSON.parse(localStorage.getItem('elements')) || [];
		const filterValues = JSON.parse(localStorage.getItem('filterValues'));
		this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
		if (filterValues) {
			if (this.filterService.sendFilters(this.formFilter)) {
				this.data.filter = this.formFilter.value;
				this.getData(this.data);
			}
		}

		this.allmois = this.utilsService.getMois();
		this.getData(this.data);
		this.dataSource.sort = this.sort;
	}

	siNullRenderZero(item) {
		return this.utilsService.siNullRenderZero(item);
	}

	totalPrestaAndFrais() {
		let resultat = 0;
		if (this.dataSource) {
			this.dataSource.data.map((element) => {
				const p = +element.prestation;
				resultat = resultat + p + this.siNullRenderZero(element.frais);
			});
		}
		return resultat;
	}

	RenderNumber(item) {
		return +item;
	}

	totalPresta() {
		let resultat = 0;
		if (this.dataSource) {
			this.dataSource.data.forEach((element) => {
				const p = +element.prestation;
				resultat = resultat + p;
			});
		}
		return resultat;
	}

	totalfrais() {
		let resultat = 0;
		if (this.dataSource) {
			this.dataSource.data.forEach((element) => {
				resultat = resultat + this.siNullRenderZero(element.frais);
			});
		}
		return resultat;
	}

	totalTarification() {
		let resultat = 0;
		if (this.dataSource) {
			this.dataSource.data.forEach((element) => {
				let t = +element.totalTarif;
				resultat = resultat + t;
			});
		}
		return resultat;
	}

	nombreInterimaire() {
		const tab = [];
		let resultat = 0;
		if (this.dataSource) {
			this.dataSource.data.forEach((value) => {
				if (!tab.some((x) => x.interim_id === value.interim_id)) {
					tab.push(value);
					resultat += 1;
				}
			});
		}
		return resultat;
	}

	getSelectedOptions(selected) {
		this.selected = selected;
	}

	nombreEtablissement() {
		const tab = [];
		let resultat = 0;
		if (this.dataSource) {
			this.dataSource.data.forEach((value) => {
				if (!tab.some((x) => x.hopital === value.hopital)) {
					tab.push(value);
					resultat += 1;
				}
			});
		}
		return resultat;
	}

	valutElements() {
		// creation du filtre depuis le storage
		this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
		if (this.elements.length === 0) {
			this.data.filter = null;
			this.data.limit = 12;
			this.data.offset = 0;
			this.getData(this.data);
			localStorage.removeItem('elements');
			localStorage.removeItem('filterValues');
		} else {
			localStorage.setItem('elements', JSON.stringify(this.elements));
		}
	}

	pagination(event: any) {
		this.pageSize = event.pageSize;
		// const data = this.defaultValue( * this.pageSize, this.pageSize);
		this.data.offset = event.pageIndex * this.pageSize;
		this.data.limit = event.pageSize;
		this.getData(this.data);
	}

	// fonction qui envoie le formulaire de filtre en base de donnee
	onSubmitFiltre() {
		// if (this.selected.length > 0) {

		//
		//   form.control.controls['specialite'].setValue(this.selected);
		// }
		localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
		this.data.filter = this.formFilter.value;
		this.getData(this.data);
	}

	getData(data) {
		this.notificationService.blockUiStart();
		this.paieService.listOld(data).subscribe(
			(response: any) => {
				if (!response.erreur) {
					this.dataSource = new MatTableDataSource(response.data);
					this.length = response.lenght;
					// this.getListLength(this.pageSize);
				} else {
					this.notificationService.showNotificationEchec(response.erreur);
					this.notificationService.blockUiStop();
				}
			},
			(erreur) => {
				this.notificationService.showNotificationEchec('erreur Chargement!! veillez ressayer a nouveau');
				this.notificationService.blockUiStop();
			},
			() => {
				this.notificationService.blockUiStop();
			}
		);
	}

	getListLength(size) {
		this.paieService.listLength(this.data).subscribe((x) => (this.length = Number(x)));
	}

	defaultValue(debut = 0, fin = 12) {
		const items = { offset: debut, limit: fin };
		return items;
	}
}
