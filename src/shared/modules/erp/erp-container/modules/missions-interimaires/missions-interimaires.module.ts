import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import { MissionsInterimairesComponent } from './missions-interimaires/missions-interimaires.component';
import {MissionInterimaireRouting} from './missions-interimaires.routing';
import { ListCandidatureValiderModalComponent } from './list-candidature-valider-modal/list-candidature-valider-modal.component';
import { RappelSecuriteComponent } from './rappel-securite/rappel-securite.component';



@NgModule({
  declarations: [
    MissionsInterimairesComponent,
    ListCandidatureValiderModalComponent,
    RappelSecuriteComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    MissionInterimaireRouting,
  ],
  entryComponents: [
    RappelSecuriteComponent,
  ]
})
export class MissionsInterimairesModule { }
