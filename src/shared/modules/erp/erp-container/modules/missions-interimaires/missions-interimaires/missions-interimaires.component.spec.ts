import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsInterimairesComponent } from './missions-interimaires.component';

describe('MissionsInterimairesComponent', () => {
  let component: MissionsInterimairesComponent;
  let fixture: ComponentFixture<MissionsInterimairesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsInterimairesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsInterimairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
