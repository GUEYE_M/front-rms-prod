import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPldContratComponent } from './modal-pld-contrat.component';

describe('PldContratComponent', () => {
  let component: ModalPldContratComponent;
  let fixture: ComponentFixture<ModalPldContratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPldContratComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPldContratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
