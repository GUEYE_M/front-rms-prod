import {Component, OnInit, ViewChild} from '@angular/core';
import {
    MatBottomSheet,
    MatDialog,
    MatDialogRef,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    PageEvent
} from '@angular/material';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {NotificationService} from '../../../../../../services/notification.service';
import {ActivatedRoute} from '@angular/router';
import {FilterService} from '../../../../../../services/filter.service';
import {UtilsService} from '../../../../../../services/utils.service';
import {CandidatureService} from '../../../../../../services/candidature.service';
import {MissionInterimService} from '../../../../../../services/interim/mission-interim.service';
import {NgxDrpOptions, PresetItem, Range} from 'ngx-mat-daterange-picker';
import {SelectionModel} from '@angular/cdk/collections';
import {ListCandidatureValiderModalComponent} from '../list-candidature-valider-modal/list-candidature-valider-modal.component';
import {MissionInterimaireService} from '../../../../../../services/missionInterimaire.service';
import {RolesService} from '../../../../../../services/roles.service';
import {UserService} from '../../../../../../services/user.service';
import {RappelSecuriteComponent} from '../rappel-securite/rappel-securite.component';
import {ModalPldContratComponent} from './modal-pld-contrat/modal-pld-contrat.component';

@Component({
    selector: 'app-missions-interimaires',
    templateUrl: './missions-interimaires.component.html',
    styleUrls: ['./missions-interimaires.component.css']
})
export class MissionsInterimairesComponent implements OnInit {
    formFilter: FormGroup;
    action: string;
    elements = [];
    loader = true;
    statsMissionMedecin: any;
    data = {
        offset: 0,
        limit: 12,
        filter: null,
        date_debut: null,
        date_fin: null
    };
    chartsGlobalCandidaturesStatut = true;
    itemschartGlobal = {
        width: '100%',
        height: '520',
        type: '',
        dataSource: {}
    };
    itemschartPeriode = {
        width: '100%',
        height: '520',
        type: '',
        dataSource: {}
    };
    itemschartContrat = {
        width: '100%',
        height: '260',
        type: '',
        dataSource: {}
    };
    itemschartCandidature = {
        width: '100%',
        height: '260',
        type: '',
        dataSource: {}
    };

    pageEvent: PageEvent;
    pageSize = 12;
    datestatsCandidature = '2020';
    length = 12;
    pageSizeOptions: number[] = [12, 25, 50, 100, 150];
    displayedColumnsFilter = ['date', 'auteur','nom', 'prenom', 'mois', 'annee', 'specialite', 'etablissement', 'reference'];
    dialogRef: MatDialogRef<ListCandidatureValiderModalComponent>;
    dialogRefContrat: MatDialogRef<ModalPldContratComponent>;
    dialogRefRappelSecu: MatDialogRef<RappelSecuriteComponent>;
    mission: any;
    displayedColumns = [
        'numero',
        'interim',
        'ref',
        'etablissement',
        'rapel_securiter',
        'vacation',
        'date',
        'auteur',
        'action'
    ];

    dataSource: MatTableDataSource<any[]> = new MatTableDataSource([]);
    selection = new SelectionModel<any[]>(true, []);
    @ViewChild(MatSort, {static: false})
    sort: MatSort;
    @ViewChild(MatPaginator, {static: false})
    paginator: MatPaginator;
    @ViewChild('dateRangePicker', {static: false})
    dateRangePicker;
    listActions = ['Supprimer', 'Desactiver'];
    listsource: any[] = [];
    allmois = [];
    showLoadInterim: boolean;
    missionsInterimaireData: any;
    range: Range = {fromDate: new Date(), toDate: new Date()};
    currentUser: any;
    date_debut: number = null;
    date_fin: number = null;

    viewdatepiker = false;
    options: NgxDrpOptions;
    presets: Array<PresetItem> = [];

    constructor(
        private missionnterimService: MissionInterimService,
        private candidatureService: CandidatureService,
        private formBuilder: FormBuilder,
        private utilsService: UtilsService,
        private missionInterimService: MissionInterimaireService,
        private filterService: FilterService,
        private activatedRoute: ActivatedRoute,
        private notificationService: NotificationService,
        private dialog: MatDialog,
        private roleService: RolesService,
        private userService: UserService,
        private bottomSheet: MatBottomSheet,
        private datePipe: DatePipe
    ) {
        this.chartsPeriode();
    }

    ngOnInit() {
        this.formFilter = this.formBuilder.group({});
        // recuperation du filtre du sauvegarde
        this.elements = JSON.parse(localStorage.getItem('elements')) || [];
        const filterValues = JSON.parse(localStorage.getItem('filterValues'));
        this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
        if (filterValues) {
            if (this.filterService.sendFilters(this.formFilter)) {
                this.data.filter = this.formFilter.value;
                this.onSubmitFiltre();
            }
        }
        this.currentUser = this.userService.getCurrentUser();
        this.allmois = this.utilsService.getMois();
        this.initMissionsInterimaire(this.data);
        this.initStatsBar(this.data);
    }

    initMissionsInterimaire(data) {
        this.notificationService.blockUiStart();
        this.missionInterimService.getMissionsInterims(data).subscribe(
            (response: any) => {
                this.utilsService.onRevientEnHaut();
                this.missionsInterimaireData = response;

                this.dataSource = new MatTableDataSource(this.missionsInterimaireData.data);
                this.length = this.missionsInterimaireData.length;
                setTimeout(() => {
                    this.dataSource.sort = this.sort;
                }, 2000);
                this.notificationService.blockUiStop();
            },
            (error1) => {
                this.notificationService.blockUiStop();
            }
        );
    }

    initStatsBar(data) {
      this.loader = true;
      this.missionInterimService.getStatsBarMissionsInterims(data).subscribe((response) => {
            this.loader = false;
            this.notificationService.blockUiStop();
            this.statsMissionMedecin = response;
            this.chartContrat(this.statsMissionMedecin.statsContrats);
            this.chartCandidatures(this.statsMissionMedecin.statsCandidatures);
            this.chartsGlobalCandidatures(this.statsMissionMedecin.statsCandidatures, this.statsMissionMedecin.annee);
        }, (error1) => {
            this.loader = false;
            this.notificationService.showNotificationEchec("Erreur du chargement des statistiques");
            this.notificationService.blockUiStop();
        });
    }

    // fonction qui envoie le formulaire de filtre en base de donnee

    onSubmitFiltre() {
        this.datestatsCandidature = 'pour toutes les années';
        if (this.formFilter.value.hasOwnProperty('annee'))
            this.datestatsCandidature = this.formFilter.value.annee;
        localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
        this.data.filter = JSON.stringify(this.formFilter.value);
        this.initMissionsInterimaire(this.data);
        this.initStatsBar(this.data);
    }

    pagination(event: any) {
        this.pageSize = event.pageSize;
        this.data.offset = event.pageIndex * this.pageSize;
        this.data.limit = this.pageSize;
        this.initMissionsInterimaire(this.data);
    }

    masterToggle() {
        this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach((row) => this.selection.select(row));
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    listDateCandidature(missionId, clientId, interimId, ref, spe) {
        const items = {
            idMission: missionId,
            refMM: ref,
            idClient: clientId,
            idInterim: interimId,
            specialite: spe
        };
        this.dialogRef = this.dialog.open(ListCandidatureValiderModalComponent, {
            panelClass: 'myapp-no-padding-dialog',
            data: items,
            width: '50%',
            height: '70%'
        });
    }

    modalContrat(missionId, clientId, interimId, ref, spe, nom, prenom, etablissement) {
        const items = {
            idMission: missionId,
            refMM: ref,
            idClient: clientId,
            idInterim: interimId,
            interim: prenom + ' ' + nom,
            client: etablissement,
            specialite: spe
        };
        this.dialogRefContrat = this.dialog.open(ModalPldContratComponent, {
            panelClass: 'myapp-no-padding-dialog',
            data: items,
            width: '70%',
            height: '70%'
        });
    }

    getNewData() {
        this.notificationService.blockUiStart();
        this.candidatureService.getAllCandidaturesValidees(this.data).subscribe(
            (response: any) => {
                this.dataSource = new MatTableDataSource(response['data']);
                this.length = response['lenght'];
            },
            (erreur) => {
                this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
                this.notificationService.blockUiStop();
            },
            () => {
                this.notificationService.blockUiStop();
            }
        );
    }

    onActifOuInactif(mission, action, rappel_secu, facturation) {
        this.notificationService.onConfirm('Êtes-vous sûre d\'effectuer cette action ?');
        this.notificationService.dialogRef.afterClosed().subscribe((response) => {
            if (response) {
                this.notificationService.blockUiStart();
                const data = {
                    mission: mission.mission_medecin_id,
                    action: action,
                    facturation: facturation,
                    rappel_secu: rappel_secu
                };

                this.missionnterimService.updateFacturationOrRapelSecu(data).subscribe(
                    (response: any) => {
                        this.notificationService.blockUiStop();
                        if (response.erreur) {
                            this.notificationService.showNotificationEchec(response.erreur);
                        } else {
                            this.notificationService.showNotificationSuccess(response.success);
                        }
                    },
                    (erreur) => {
                        this.notificationService.blockUiStop();
                    },
                    () => {
                        this.notificationService.blockUiStop();
                    }
                );
            }
            this.getNewData();
        });
    }

    chartsGlobalCandidatures(items, annee) {
        const V = [];
        const NR = [];
        const EA = [];
        const ACHApV = [];
        const ACHAvV = [];
        const NE = [];
        const ACH = [];
        const AM = [];
        const keyItems = Object.keys(items);
        const categorie = [];
        const dataset = [];
        keyItems.forEach((element) => {
          categorie.push({label: element});
          V.push({value: items[element].validee});
          EA.push({value: items[element]['en attente']});
          NR.push({value: items[element]['non retenue']});
          AM.push({value: items[element]['annulee med']});
          ACH.push({value: items[element]['annulee ch']});
          NE.push({value: items[element]['non effectuee']});
          ACHApV.push({value: items[element].annulee_c_apresvalidation});
          ACHAvV.push({value: items[element].annulee_c_avantvalidation});
        });
        const data = {
            chart: {
                caption: 'Le détail des candidatures ' + annee,
                snumbersuffix: '%',
                theme: 'fusion'
            },
            categories: [
                {
                    category: categorie
                }
            ],
            dataset: [
                {
                    seriesname: 'N.R',
                    data: NR
                },
                {
                    seriesname: 'A.CH.Ap.V',
                    color: '#B1D5E3',
                    data: ACHApV
                },
                {
                    seriesname: 'E.A',
                    color: '#E3B505',
                    data: EA
                },
                {
                    seriesname: 'A.CH.Av.V',
                    color: '#484851',
                    data: ACHAvV
                },
                {
                    seriesname: 'N.E',
                    color: '#EF7587',
                    data: NE
                },
                {
                    seriesname: 'A.CH',
                    color: '#610345',
                    data: ACH
                },
                {
                    seriesname: 'A.M',
                    color: '#E29795',
                    data: AM
                },
                {
                    seriesname: 'V',
                    color: '#039633',
                    data: V
                }
            ]
        };
        this.itemschartGlobal.dataSource = data;
        this.itemschartGlobal.type = 'mscolumn3dlinedy';
    }

    chartsPeriode() {
        const data = {
            chart: {
                caption: 'Support Tickets : Received vs Resolved',
                yaxisname: '# of Tickets',
                subcaption: 'Last week',
                numdivlines: '3',
                showvalues: '0',
                legenditemfontsize: '15',
                legenditemfontbold: '1',
                plottooltext: '<b>$dataValue</b> Tickets $seriesName on $label',
                theme: 'fusion'
            },
            categories: [
                {
                    category: [
                        {
                            label: 'Jan 1'
                        },
                        {
                            label: 'Jan 2'
                        },
                        {
                            label: 'Jan 3'
                        },
                        {
                            label: 'Jan 4'
                        },
                        {
                            label: 'Jan 5'
                        },
                        {
                            label: 'Jan 6'
                        },
                        {
                            label: 'Jan 7'
                        }
                    ]
                }
            ],
            dataset: [
                {
                    anchorbgcolor: '#5D62B5',
                    seriesname: 'Received',
                    data: [
                        {
                            value: '55'
                        },
                        {
                            value: '45'
                        },
                        {
                            value: '52'
                        },
                        {
                            value: '29'
                        },
                        {
                            value: '48'
                        },
                        {
                            value: '28'
                        },
                        {
                            value: '32'
                        }
                    ]
                },
                {
                    anchorbgcolor: '#29C3BE',
                    seriesname: 'Resolved',
                    data: [
                        {
                            value: '50'
                        },
                        {
                            value: '30'
                        },
                        {
                            value: '49'
                        },
                        {
                            value: '22'
                        },
                        {
                            value: '43'
                        },
                        {
                            value: '14'
                        },
                        {
                            value: '31'
                        }
                    ]
                }
            ]
        };
        this.itemschartPeriode.dataSource = data;
        this.itemschartPeriode.type = 'msspline';
    }

    chartsGlobalContrat(items, annee) {
        const SI = [];
        const SC = [];
        const NSI = [];
        const NSC = [];
        const categorie = [];
        const keyItems = Object.keys(items);
        keyItems.forEach((element) => {
            categorie.push({label: element});

            SI.push({value: items[element].SI});
            SC.push({value: items[element].SC});
            NSC.push({value: items[element].NSC});
            NSI.push({value: items[element].NSI});
        });
        const data = {
            chart: {
                caption: 'Le détail des contrats pour ' + annee,
                snumbersuffix: '%',
                theme: 'fusion'
            },
            categories: [
                {
                    category: categorie
                }
            ],
            dataset: [
                {
                    seriesname: 'S.I',
                    color: '#0ba88c',
                    data: SI
                },
                {
                    seriesname: 'S.C',
                    data: SC
                },
                {
                    seriesname: 'N.S.I',
                    data: NSI
                },
                {
                    seriesname: 'N.S.C',
                    color: '#e3005e',
                    data: NSC
                }
            ]
        };
        this.itemschartGlobal.dataSource = data;
        console.log(this.itemschartGlobal.dataSource);
        this.itemschartGlobal.type = 'mscolumn3dlinedy';
    }

    changeChartsGlobal(status) {
        status ?
          this.chartsGlobalCandidatures(this.statsMissionMedecin.statsCandidatures, this.statsMissionMedecin.annee) :
          this.chartsGlobalContrat(this.statsMissionMedecin.statsContrats, this.statsMissionMedecin);
    }

    chartContrat(items: any) {
        let SI = 0;
        let SC = 0;
        let NSI = 0;
        let NSC = 0;
        let NE = 0;
        const keyItems = Object.keys(items);
        keyItems.forEach((element) => {
          SI += items[element].SI;
          SC += items[element].SC;
          NSC += items[element].NSC;
          NSI +=  items[element].NSI;
          NE +=  items[element].NE;
        });
        const data = {
            chart: {
                caption: 'Contrats',
                theme: 'fusion'
            },
            data: [
                {
                    label: 'N.E',
                    value: NE
                },
                {
                    label: 'S.I',
                    value: SI
                },
                {
                    label: 'N.S.C',
                    value: NSC
                },
                {
                    label: 'N.S.I',
                    value: NSI
                },
                {
                    label: 'S.C',
                    value: SC
                }
            ]
        };
        this.itemschartContrat.dataSource = data;
        this.itemschartContrat.type = 'doughnut2d';
    }

    chartCandidatures(items) {
        let V = 0;
        let NR = 0;
        let EA = 0;
        let AM = 0;
        let ACH = 0;
        let ACHApV = 0;
        let ACHAvV = 0;
        let NE = 0;
        const keyItems = Object.keys(items);
        keyItems.forEach((element) => {
          V += items[element].validee;
          EA += items[element]['en attente'];
          NR += items[element]['non retenue'];
          AM +=  items[element]['annulee med'];
          ACH +=  items[element]['annulee ch'];
          NE +=  items[element]['non effectuee'];
          ACHApV +=  items[element].annulee_c_apresvalidation;
          ACHAvV +=  items[element].annulee_c_avantvalidation;
        });
        const data = {
            chart: {
                renderAt: 'chart-container',
                caption: 'Candidatures',
                theme: 'Umber'
            },
            data: [
                {
                    label: 'N.R',
                    value: NR
                },
                {
                    label: 'E.A',
                    value: EA
                },
                {
                    label: 'V',
                    value: V
                },
                {
                    label: 'A.M',
                    value: AM
                },
                {
                    label: 'A.CH',
                    value: ACH
                },
                {
                    label: 'A.CH.Ap.V',
                    value: ACHApV
                },
                {
                    label: 'A.CH.Av.V',
                    value: ACHAvV
                },
                {
                    label: 'N.E',
                    value: NE
                }
            ]
        };
        this.itemschartCandidature.dataSource = data;
        this.itemschartCandidature.type = 'pie3d';
    }

    valutElements() {
        // creation du filtre depuis le storage
        this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
        if (this.elements.length === 0) {
            this.data.filter = null;
            localStorage.removeItem('elements');
            localStorage.removeItem('filterValues');
            this.initMissionsInterimaire(this.data);
            this.initStatsBar(this.data);
        } else {
            localStorage.setItem('elements', JSON.stringify(this.elements));
            this.elements.map((item) => {
                if (item === 'date') {
                    // this.datepickerConfig();
                }
            });
        }
    }

    RappelSecuModal(idMM) {
        const items = {
            idmissionMed: idMM,
            user: this.currentUser.username
        };

        this.dialogRefRappelSecu = this.dialog.open(RappelSecuriteComponent, {
            panelClass: 'myapp-no-padding-dialog',
            data: items,
            width: '50%',
            height: '70%'
        });

        this.dialogRefRappelSecu.afterClosed().subscribe((response) => {
            if (response) {
                this.dataSource.data.map((x: any) => {
                    if (x.mission_medecin_id == idMM) {
                        x.rappel++;
                    }
                });
            }
        });
    }
}
