import {Route, RouterModule} from '@angular/router';
import {MissionsInterimairesComponent} from './missions-interimaires/missions-interimaires.component';
import {MissionMedecinResolve} from '../../../../../resolver/missionMedecin.resolve';
import {ListCandidatureValiderModalComponent} from './list-candidature-valider-modal/list-candidature-valider-modal.component';

const MISSION_INTERIMAIRES_CONFIG: Route[] = [
  {
    path: '',
    component: MissionsInterimairesComponent
  },
  {
    path: 'modal-date',
    component: ListCandidatureValiderModalComponent,
  },

];

export const MissionInterimaireRouting = RouterModule.forChild(MISSION_INTERIMAIRES_CONFIG);



