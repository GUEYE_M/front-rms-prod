import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIdPldManuelComponent } from './add-id-pld-manuel.component';

describe('AddIdPldManuelComponent', () => {
  let component: AddIdPldManuelComponent;
  let fixture: ComponentFixture<AddIdPldManuelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIdPldManuelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIdPldManuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
