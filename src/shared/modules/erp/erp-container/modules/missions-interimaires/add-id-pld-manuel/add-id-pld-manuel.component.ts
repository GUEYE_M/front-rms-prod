import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CandidatureService } from '../../../../../../services/candidature.service';
import { InterimService } from '../../../../../../services/interim/interim.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { ClientService } from '../../../../../../services/recruteur/client.service';

@Component({
	selector: 'app-add-id-pld-manuel',
	templateUrl: './add-id-pld-manuel.component.html',
	styleUrls: [ './add-id-pld-manuel.component.css' ]
})
export class AddIdPldManuelComponent implements OnInit {
	tabIdCandidatures = [];
	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private candidatureService: CandidatureService,
		private interimService: InterimService,
		private clientService: ClientService,
		private notificationService: NotificationService,
		public dialog: MatDialog
	) {}

	ngOnInit() {
		if (this.data && !this.data.id) {
			this.data.forEach((element: any) => {
				this.tabIdCandidatures.push(element.idCandidature);
			});
		}
	}

	onSubmit(f) {
		this.notificationService.blockUiStart();
		if (this.data && !this.data.id) {
			this.candidatureService
				.addIdContratPldToCandidature({ idContratPld: f.value.id, candidatureIds: this.tabIdCandidatures })
				.subscribe(
					(response: any) => { 
						if (!response.success) {
							this.notificationService.showNotificationEchecCopie('bottomRight', "une erreur est survenue lors de la liaison du contrat");
							
						} else {
							this.notificationService.showNotificationSuccessCopie('bottomRight',"La liaison a été effectuer avec succès");
							location.reload();
							this.dialog.closeAll();
						}
						this.notificationService.blockUiStop();
					},
					(error) => {
						this.notificationService.showNotificationEchecCopie('bottomRight', error.error.message);
						this.notificationService.blockUiStop();
					}
				);
		} else {
			if (this.data.user === 'interim') {
				this.interimService
					.addIdInterimPldToInterim({ idInterimPld: f.value.id, idInterim: this.data.id })
					.subscribe(
						(response: any) => {
							if (!response.erreur) {
								this.notificationService.showNotificationSuccessCopie('bottomRight', response.succees);
								this.dialog.closeAll();
								location.reload();
							} else {
								this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
							}
							this.notificationService.blockUiStop();
						},
						(error) => {
							this.notificationService.showNotificationEchecCopie('bottomRight', error.error.message);
							this.notificationService.blockUiStop();
						}
					);
			}
			if (this.data.user === 'client') {
				const data = { idPld: f.value.id, idClieent: this.data.id };

				this.clientService.addIdClientPLDToClientErp(data).subscribe(
					(response: any) => {
						if (!response.erreur) {
							this.notificationService.showNotificationSuccessCopie('bottomRight', response.succees);
							this.dialog.closeAll();
							location.reload();
						} else {
							this.notificationService.showNotificationEchecCopie('bottomRight', response.erreur);
						}
						this.notificationService.blockUiStop();
					},
					(error) => {
						this.notificationService.showNotificationEchecCopie('bottomRight', error.error.message);
						this.notificationService.blockUiStop();
					}
				);
			}
		}
	}
}
