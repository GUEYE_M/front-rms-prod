import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { CandidatureService } from '../../../../../../services/candidature.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { SelectionModel } from '@angular/cdk/collections';
import { PldContratComponent } from '../../interim/single-interim/mission-medecin/pld-contrat/pld-contrat.component';
import { UserService } from '../../../../../../services/user.service';
import { InterimService } from '../../../../../../services/interim/interim.service';
import { EditInterimComponent } from '../../interim/edit-interim/edit-interim.component';
import { EditListeClientComponent } from '../../client-wrapper/client/edit-liste-client/edit-liste-client.component';
import { UpdateContratPldComponent } from '../../../../../shared/update-contrat-pld/update-contrat-pld.component';
import { SellandsignService } from 'src/shared/services/sellandsign.service';
import { ListRecruteurModalComponent } from '../../client-wrapper/client/modal-client/list-recruteur-modal/list-recruteur-modal.component';
import { ListHebergementModalComponent } from '../../interim/single-interim/mission-medecin/list-hebergement-modal/list-hebergement-modal.component';
import { HebergementServiceService } from 'src/shared/services/recruteur/hebergement.service.service';
import { ClientService } from 'src/shared/services/recruteur/client.service';
import { StatutContratSignComponent } from '../../interim/single-interim/mission-medecin/statut-contrat-sign/statut-contrat-sign.component';

@Component({
	selector: 'app-list-candidature-valider-modal',
	templateUrl: './list-candidature-valider-modal.component.html',
	styleUrls: [ './list-candidature-valider-modal.component.css' ]
})
export class ListCandidatureValiderModalComponent implements OnInit {
	selection = new SelectionModel<any[]>(true, []);
	dataSource: MatTableDataSource<any[]> = new MatTableDataSource();
	dataSourceContrat = [];
	dataSourceDateCandidature: MatTableDataSource<any[]>;
	currentUser: any;
	currentInterimaire: any;
	client: any;
	idClientPld: any;
	idInterimPld: any;
	idInterim: any;
	idMM: any;
	idClient: any;
	idpld: number;
	specialite: any;
	designation: any;
	carecteristiquePoste: any;
	dialogRefEditInterim: MatDialogRef<EditInterimComponent>;
	dialogRefEditClient: MatDialogRef<EditListeClientComponent>;
	dialogueUpdateModalPldContrat: MatDialogRef<UpdateContratPldComponent>;
	apartenance_contrat: number = null;
	dialogRefContratSign: MatDialogRef<StatutContratSignComponent>;

	data_contrat_sign = {
		id_ch: null,
		id_interim: null,
		id_recruteur: null,
		id_pld: null,
		apartenance: null,
		interim: false,
		reference_mission: null,
		hebergement: []
	};

	dialogueModalPldContrat: MatDialogRef<PldContratComponent>;
	displayedColumns = [ 'select', 'number', 'date', 'vacation', 'debut', 'fin', 'salaire', 'tarif', 'contrat-pld' ];
	displaydColumnsList = [ 'date', 'vacation', 'debut', 'fin', 'salaire', 'tarif', 'auteur', 'statut' ];
	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private candidatureService: CandidatureService,
		private notificationService: NotificationService,
		public dialogRef: MatDialogRef<ListCandidatureValiderModalComponent>,
		private dialog: MatDialog,
		private userService: UserService,
		private interimService: InterimService,
		private sellAndSign: SellandsignService,
		private hebergementService: HebergementServiceService,
		private clientService: ClientService
	) {}

	ngOnInit() {
		if (this.data) {
			this.idMM = this.data.idMission;
			this.currentUser = this.userService.getCurrentUser();
			const dataSource = [];
			this.notificationService.blockUiStart();
			this.idClient = this.data.idClient;
			this.specialite = this.data.specialite;
			this.candidatureService.getListCandidatureByMissionMedecinID(+this.data.idMission).subscribe(
				(response: any) => {
					if (!response.erreur) {
						this.idClientPld = response.data[0].idClientPld;
						this.designation = response.data[0].designation;
						this.idInterimPld = response.data[0].idInterimPld;
						this.idInterim = response.data[0].idInterim;
						this.dataSourceDateCandidature = new MatTableDataSource(response.data);
						response.data.forEach((element) => {
							if (element.satatut_candidature === 'Validee') dataSource.push(element);
							if (element.contraPld) this.dataSourceContrat.push(element);
						});
						this.dataSource = new MatTableDataSource(dataSource);
					} else {
						this.notificationService.showNotificationEchec(response.erreur);
						this.dialogRef.close();
					}
				},
				(erreur) => {
					this.notificationService.showNotificationEchec('erreur server, veillez ressayer a nouveau');

					this.dialogRef.close();
					this.notificationService.blockUiStart();
				},
				() => {
					this.notificationService.blockUiStop();
				}
			);
		}
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSource.data.forEach((row) => this.selection.select(row));
	}

	/** The label for the checkbox on the passed row */
	checkboxLabel(row?: any): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
	}

	OnOuvreModalPld() {
		this.notificationService.blockUiStart();
		if (!this.onContratExiste() && this.currentUser.gestionnaire.IdReferentiel) {
			const items = {
				candidatures: this.selection.selected,
				carecteristiquePoste: 'SERVICE ' + this.data.specialite.toUpperCase(),
				interimId: this.data.idInterim,
				refMission: this.data.refMM,
				clientId: this.data.idClient
			};
			this.notificationService.blockUiStop();
			this.dialogueModalPldContrat = this.dialog.open(PldContratComponent, {
				width: '40%',
				height: '600px',
				panelClass: 'myapp-no-padding-dialog',
				data: items
			});
		} else if (!this.currentUser.gestionnaire.IdReferentiel) {
			this.notificationService.showNotificationEchecCopie(
				'bottomRight',
				"Vous pouvez pas créer de contrat si vous n'êtes pas encore inscrit sur PLD!"
			);
			this.notificationService.blockUiStop();
		} else {
			this.notificationService.showNotificationEchecCopie(
				'bottomRight',
				'Vous avez saisi une cadidature qui a déjà son contrat créer !'
			);
			this.notificationService.blockUiStop();
		}
	}
	initCurrentInterim() {
		const idInterim = this.currentUser.interim.id;
		this.interimService.labelTab_text.subscribe((index) => {
			if (index === 'missions') {
				this.notificationService.blockUiStart();
				this.interimService.missionByInterim(idInterim).subscribe(
					(response: any) => {
						// if (response) {
						//   this.currentInterimaire = response['interim'];
						//   this.idIterim = Number(response['interim'].id);
						//   this.idInterimPld = response.idPld;
						//   this.missionMed = response.missionMed;
						//   this.dataSourceMission = new MatTableDataSource(this.missionMed);
						//   this.itemMissionMed = Object.keys(this.missionMed);
						// }
						this.notificationService.blockUiStop();
					},
					(error) => {
						this.notificationService.blockUiStop();
					}
				);
			}
		});
	}

	OnUpdateInterimaire(idInterim: number = null) {
		const donnees = { id: idInterim, profil: 'pld' };
		this.dialogRefEditInterim = this.dialog.open(EditInterimComponent, {
			width: '800px',
      height: '90%',
      panelClass: 'myapp-no-padding-dialog',
			data: donnees
		});

		//   this.dialogRef.afterClosed().subscribe(response => {
		//     if (response) {
		//       if (!response.profil) {
		//         this.getNewData();
		//         this.selection.clear()
		//       }
		//     }
		//   });
	}
	editClientModal(idClient = null) {
		const donnees = {
			profil: 'pld',
			id: idClient
		};
		this.dialogRefEditClient = this.dialog.open(EditListeClientComponent, {
			width: '70%',
			height: '700px',
			panelClass: 'myapp-no-padding-dialog',
			data: donnees
		});
	}
	onContratExiste(): boolean {
		let response = false;
		this.selection.selected.forEach((element: any) => {
			if (element.contraPld !== null) {
				response = true;
			}
		});
		return response;
	}
	coontratUnique(data) {
		const response = [];
		const contratIdUnique = data
			.map((item) => item.contraPld.idPld)
			.filter((value, index, self) => self.indexOf(value) === index);
		if (contratIdUnique.length > 0) {
			contratIdUnique.forEach((element) => {
				response.push(this.returnDataByidContrat(data, element));
			});
		}
		return response;
	}

	returnDataByidContrat(data, idContrat) {
		let rep = null;
		data.forEach((item) => {
			if (item.contraPld.idPld === idContrat) rep = item;
		});
		return rep;
	}

	OnUpdateModalPld(item) {
		this.dialogueUpdateModalPldContrat = this.dialog.open(UpdateContratPldComponent, {
			width: '50%',
			height: '80%',
			panelClass: 'myapp-no-padding-dialog',
			data: item
		});
	}

	sendContratToSign() {
		this.notificationService.blockUiStart();
		this.sellAndSign.sendContratToSign(this.data_contrat_sign).subscribe((response: any) => {
			if (!response.erreur) {
				this.notificationService.showNotificationSuccessCopie('bottomRight', response.success);
				this.updateActionContrat(this.apartenance_contrat);
				this.notificationService.blockUiStop();
			} else {
				this.notificationService.showNotificationSuccessCopie('bottomRight', response.erreur);
				this.notificationService.blockUiStop();
			}
		});
	}

	updateActionContrat(apartenance_contrat: number, annuler: boolean = false) {
		let data = this.coontratUnique(this.dataSourceContrat);
		data.map((el: any) => {
			if (el.contraPld.idPld === this.idpld) {
				if (apartenance_contrat === 1) {
					if (annuler) {
						el.contraPld.interim_contrat = false;
					} else {
						el.contraPld.interim_contrat = true;
					}
				} else {
					if (annuler) {
						el.contraPld.ch_contrat = false;
					} else {
						el.contraPld.ch_contrat = true;
					}
				}
			}
		});
	}

	onSignContrat(id_pld, id_candidature, apartenance_contrat) {
		const thiss = this;
		const message = '';

		this.data_contrat_sign.apartenance = apartenance_contrat;
		this.data_contrat_sign.id_pld = id_pld;
		this.apartenance_contrat = apartenance_contrat;
		this.idpld = id_pld;
		this.data_contrat_sign.id_interim = +this.dataSourceContrat[0].idInterim;

		this.notificationService.onConfirm('Etes-vous sur de vouloir envoyer ce contrat en signature ?');

		this.notificationService.dialogRef.afterClosed().subscribe((response) => {
			if (response) {
				thiss.notificationService.blockUiStart();
				this.clientService.getClientRecruteurByCandidatureId(id_candidature).subscribe(
					(recruteur: any) => {
						if (recruteur) {
							let data = {
								items: recruteur.data,
								length: recruteur.data.length,
								edit: false
							};
							this.data_contrat_sign.id_ch = recruteur.id_client;
							this.data_contrat_sign.id_recruteur = recruteur.data[0].id_recruteur;
							thiss.data_contrat_sign.reference_mission = recruteur.reference_mission;

							if (apartenance_contrat == 1) {
								this.data_contrat_sign.interim = true;
								this.notificationService.blockUiStart();
								this.hebergementService
									.getHebergementByClient(this.data_contrat_sign.id_ch)
									.subscribe((el: any) => {
										this.notificationService.blockUiStop();
										if (!el.erreur) {
											let h = {
												modal: true,
												data: el.data
											};
											const dialogRefhebergement: MatDialogRef<
												ListHebergementModalComponent
											> = thiss.dialog.open(ListHebergementModalComponent, {
												width: '900px',
												height: '400px',
												data: h,
												panelClass: 'myapp-no-padding-dialog'
											});
											dialogRefhebergement.afterClosed().subscribe((el) => {
												if (el) {
													thiss.data_contrat_sign.hebergement = el[0];
													thiss.sendContratToSign();
												}
											});
										} else {
											this.notificationService.showNotificationEchecCopie(
												'bottomRight',
												el.erreur
											);
											this.notificationService.blockUiStop();
										}
									});
							} else {
								const dialogRefrecruteur: MatDialogRef<
									ListRecruteurModalComponent
								> = thiss.dialog.open(ListRecruteurModalComponent, {
									width: '900px',
									height: '400px',
									data: data,
									panelClass: 'myapp-no-padding-dialog'
								});
								//this.notificationService.blockUiStop();
								dialogRefrecruteur.afterClosed().subscribe((recruteur) => {
									if (recruteur.length > 0) {
										thiss.data_contrat_sign.id_recruteur = recruteur[0].id_recruteur;

										thiss.data_contrat_sign.apartenance = 0;
										thiss.data_contrat_sign.interim = false;

										thiss.sendContratToSign();
									}
								});
							}
						}
					},
					(error) => {
						this.notificationService.showNotificationEchec('erreur chargement veillez ressayer');
						this.notificationService.blockUiStop();
					},
					() => {
						this.notificationService.blockUiStop();

						// this.router.navigate(['/RMS-Admin/redirectionMed/', this.idIterim, 'interim']);
						// this.interimService.labelTab.next(this.labelTab);
					}
				);
			}
		});
	}

	getStatutContrat(id_contrat_pld, appartenance) {
		this.notificationService.blockUiStart();

		this.sellAndSign.findContratIdbyIdPldAndApartenace(id_contrat_pld, appartenance).subscribe(
			(response: any) => {
				if (response.erreur) {
					this.notificationService.showNotificationEchec(response.erreur);
					this.notificationService.blockUiStop();
				} else {
					this.statutContratSignModal(response.data);
					this.idpld = id_contrat_pld;
					this.apartenance_contrat = appartenance;
					this.notificationService.blockUiStop();
				}
			},
			(erreur) => {},
			() => {}
		);
	}

	statutContratSignModal(data: any): void {
		this.dialogRefContratSign = this.dialog.open(StatutContratSignComponent, {
			width: '65%',
			height: '65%',
			panelClass: 'myapp-no-padding-dialog',
			data: data
		});
		this.dialogRefContratSign.afterClosed().subscribe((response) => {
			if (response) {
				if (response === 'Annuler'.toLowerCase().trim()) {
					this.updateActionContrat(this.apartenance_contrat, true);
				}
			}
		});
	}
}
