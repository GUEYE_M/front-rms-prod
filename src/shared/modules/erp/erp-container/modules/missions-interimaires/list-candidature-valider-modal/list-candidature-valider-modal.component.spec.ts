import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCandidatureValiderModalComponent } from './list-candidature-valider-modal.component';

describe('ListCandidatureValiderModalComponent', () => {
  let component: ListCandidatureValiderModalComponent;
  let fixture: ComponentFixture<ListCandidatureValiderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCandidatureValiderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCandidatureValiderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
