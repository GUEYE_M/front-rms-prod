import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatTableDataSource, MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../../../../services/notification.service';
import { JustificatifService } from '../../../../../../services/interim/justificatif.service';
import { UtilsService } from '../../../../../../services/utils.service';

@Component({
	selector: 'app-rappel-securite',
	templateUrl: './rappel-securite.component.html',
	styleUrls: [ './rappel-securite.component.css' ]
})
export class RappelSecuriteComponent implements OnInit {
	dataSource: any = new MatTableDataSource();
	commentaire = false;
	displayedColumns = [ 'number', 'commentaire', 'auteur', 'date' ];

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private notificationService: NotificationService,
		private justificatifService: JustificatifService,
		private utilsService: UtilsService,
		private dialogRef: MatDialogRef<RappelSecuriteComponent>
	) {}
	ngOnInit() {
		this.relanceByIdMM();
	}
	relanceByIdMM() {
		this.justificatifService.relanceByIdMM(this.data.idmissionMed).subscribe((response: any) => {
			this.dataSource = new MatTableDataSource(response.data);
		});
	}

	onSubmitFiltre(form: NgForm) {
		const thiss = this;
		const data = {
			commentaire: '',
			idMissionMed: this.data.idmissionMed,
			user: this.data.user
		};
		if (form.value.commentaire) {
			data.commentaire = form.value.commentaire;
		}
		this.notificationService.onConfirm('Etes vous sur de vouloir continuer ? ');
		this.notificationService.dialogRef.afterClosed().subscribe((x) => {
			if (x) {
				this.notificationService.blockUiStart();
				thiss.justificatifService.rappelSecurite(data).subscribe((rep) => {
					this.notificationService.blockUiStop();
					if (rep.erreur) {
						this.notificationService.showNotificationEchec(rep.erreur);
					} else {
						this.notificationService.showNotificationSuccess(rep.success);
						this.dialogRef.close(true);
					}
				});
			}
		});
	}
	getData() {}
	addCommentaire() {
		this.commentaire = !this.commentaire;
	}
}
