import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RappelSecuriteComponent } from './rappel-securite.component';

describe('RappelSecuriteComponent', () => {
  let component: RappelSecuriteComponent;
  let fixture: ComponentFixture<RappelSecuriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RappelSecuriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RappelSecuriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
