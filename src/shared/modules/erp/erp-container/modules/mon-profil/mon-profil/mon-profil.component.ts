import { Component, OnInit } from '@angular/core';
import {EditMonProfilComponent} from '../edit-mon-profil/edit-mon-profil.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';
import {UserService} from '../../../../../../services/user.service';
import {NewAuthService} from '../../../../../../services/Auth/newAuth.service';
import {InterimService} from '../../../../../../services/interim/interim.service';
import {RolesService} from '../../../../../../services/roles.service';
import {UtilsService} from '../../../../../../services/utils.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../../../../../services/notification.service';
import {AuthNormalService} from '../../../../../../services/authNormal.service';
import $ from 'jquery';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mon-profil',
  templateUrl: './mon-profil.component.html',
  styleUrls: ['./mon-profil.component.css']
})
export class MonProfilComponent implements OnInit {

  dialogRef: MatDialogRef<EditMonProfilComponent>;
  currentUser: any;
  otherInfo: any;
  user: any;
  closeResult = '';
  fichier: any;
  utilisateur: any;
  public matchingPwd: boolean;
  public erreur: string;
  public  fd: FormData = new FormData();
  public updatePwdForm: FormGroup;
  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private newAuthService: NewAuthService,
    private interimService: InterimService,
    private rolesService: RolesService,
    private utilsService: UtilsService,
    private router: Router,
    private notificationService: NotificationService,
    private authNormalService: AuthNormalService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    // ff

    this.currentUser = this.userService.getCurrentUser();
    this.notificationService.blockUiStart();
    this.userService.monProfil().subscribe((rep) => {
        this.notificationService.blockUiStop();
        this.user = rep['user'];
        this.otherInfo = rep['other_info'];
      },
      error1 => {
        this.notificationService.blockUiStop();
      });
    this.initUpdatePwdForm();
  }

  initUpdatePwdForm() {
    this.updatePwdForm = this.fb.group({
      ancienPassword: ['', Validators.required],
      nouveauPassword: ['', [Validators.required, Validators.minLength(6)]],
      renouveauPassword: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  MatchingPassword(pwd) {
    if (pwd.name === 'nouveauPwd') {
      if (pwd.value !== this.updatePwdForm.value['renouveauPassword'])
        this.matchingPwd = true;
      else
        this.matchingPwd = false;
    }
    if(pwd.name === 'renouveauPwd') {
      if (pwd.value !== this.updatePwdForm.value['nouveauPassword'])
        this.matchingPwd = true;
      else
        this.matchingPwd = false;
    }
  }
  UpdatePasswordCompte() {
    this.notificationService.blockUiStart();
    const id = this.userService.getCurrentUser()['idUser'];
    const formValue = this.updatePwdForm.value;
    const password = formValue['ancienPassword'];
    const new_password = formValue['nouveauPassword'];
    const tableau = {'id': id, 'pwd': password, 'newPwd' : new_password };
    this.newAuthService.updatePasswordProfil(tableau).subscribe((response: any) => {
      if(response.success) {
        $('.close').click();
        this.initUpdatePwdForm();
      } else {
        this.erreur = response.erreur;
      }
      this.notificationService.blockUiStop();
    },
      error => {
      this.erreur = error.error.message;
        this.notificationService.blockUiStop();
      });
  }
  afficherPwd(pwd, glyphicon) {
    const eyeOpen = '<i class="fa fa-eye"  id="' + glyphicon.id + '" #' + glyphicon.id + '> </i>';
    const eyeClose = '<i class="fa fa-eye-slash" id="' + glyphicon.id + '" #' + glyphicon.id + '></i>';
    if (pwd.type === 'text') {
      pwd.type = 'password';
      $('#' + glyphicon.id).replaceWith(eyeOpen);
    }
    else {
      pwd.type = 'text';
      $('#' + glyphicon.id).replaceWith(eyeClose);
    }
  }



  roleInterim() {
    return this.rolesService.isCandidatUnique(this.currentUser.roles);
  }

  roleRecruteur() {
    return this.rolesService.isRecruteurUnique(this.currentUser.roles);
  }

  roleSuperAdmin() {
    return this.rolesService.isManager(this.currentUser.roles);
  }

  editProfilModal() {
    this.dialogRef = this.dialog.open(EditMonProfilComponent, {
      width: '800px',
      height: '550px',
      panelClass: 'myapp-no-padding-dialog',
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
