import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonProfilComponent } from './mon-profil/mon-profil.component';
import { EditMonProfilComponent } from './edit-mon-profil/edit-mon-profil.component';
import {SharedModule} from '../../../../shared/shared.module';



@NgModule({
  declarations: [
    MonProfilComponent,
    EditMonProfilComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class MonProfilModule { }
