import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMonProfilComponent } from './edit-mon-profil.component';

describe('EditMonProfilComponent', () => {
  let component: EditMonProfilComponent;
  let fixture: ComponentFixture<EditMonProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMonProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMonProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
