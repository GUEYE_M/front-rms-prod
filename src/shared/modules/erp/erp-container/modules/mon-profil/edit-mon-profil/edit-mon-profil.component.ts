import { Component, OnInit } from '@angular/core';
import {Commune} from '../../../../../../models/commune.model';
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiVilleService} from '../../../../../../services/api_ville_service';
import {UserService} from '../../../../../../services/user.service';
import {AuthNormalService} from '../../../../../../services/authNormal.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Router} from '@angular/router';
import {UtilsService} from '../../../../../../services/utils.service';
import {NewAuthService} from '../../../../../../services/Auth/newAuth.service';
import {RolesService} from '../../../../../../services/roles.service';
import {MatDialog} from '@angular/material';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-edit-mon-profil',
  templateUrl: './edit-mon-profil.component.html',
  styleUrls: ['./edit-mon-profil.component.css']
})
export class EditMonProfilComponent implements OnInit {

  currentUser: any;
  utilisateur: any;
  communes: Commune[];
  filteredCommune: Observable<any[]>;
  interimForm: FormGroup;
  recruteurForm: FormGroup;
  otherInfo: any;
  user: any;
  constructor(
    private fb: FormBuilder,
    private apiVilleService: ApiVilleService,
    private userService: UserService,
    private authNormalService: AuthNormalService,
    private  notificationService: NotificationService,
    private router: Router,
    private utilService: UtilsService,
    private newAuthService: NewAuthService,
    private dialog: MatDialog,
    private rolesService: RolesService

  ) { }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.notificationService.blockUiStart();
    this.userService.monProfil().subscribe((rep) => {
        this.notificationService.blockUiStop();
        this.user = rep['user'];
        this.otherInfo = rep['other_info'];
        if (this.roleInterim()) {
          this.initform(rep);
        }
        if (this.roleRecruteur()) {
          this.initRecruteurForm(rep);
        }
      },
      error1 => {
        this.notificationService.blockUiStop();
      });
  }
  initform(interim = {
    user: {
      civilite: '',
      nom: '',
      prenom: '',
      numSecu: '',
      username: '',
      idPaysNaissance: '',
      dateNaissance	: '',
      lieuNaissance	: '',
      telephone: '',
      adresse: '',
      complement_adresse: '',
      cp: '',
      ville: '',
      departement: '',
      region: '',
    },
    numRPPS: '',
    inscription: '',
    statut: '',
    rayonIntervention: '',
    preferenceDeplacement: '',
    preferenceContact: '',
  }) {
    this.interimForm = this.fb.group(
      {
        user: this.fb.group(
          {
            civilite: [interim['user'].civilite, Validators.required],
            nom: [interim['user'].nom, Validators.required],
            prenom: [interim['user'].prenom, Validators.required],
            numSecu: [interim['user'].numSecu, Validators.required],
            email: [this.user.email],
            username: [this.user.username],
            idPaysNaissance: [interim['user'].idPaysNaissance],
            dateNaissance: [interim['user'].dateNaissance],
            lieuNaissance	: [interim['user'].lieuNaissance	],
            telephone: [interim['user'].telephone, Validators.required],
            adresse: [interim['user'].adresse],
            complement_adresse: [interim['user'].complement_adresse],
            cp: [interim['user'].cp, Validators.required],
            ville: [interim['user'].ville, Validators.required],
            departement: [interim['user'].departement],
            region: [interim['user'].region],
          }
        ),
        id: [interim['other_info'].id],
        numRPPS: [interim['other_info'].numRPPS],
        inscription: [interim['other_info'].inscription],
        statut: [interim['other_info'].status],
        rayonIntervention: [interim['other_info'].rayonIntervention],
        preferenceDeplacement: [interim['other_info'].preferenceDeplacement],
        preferenceContact: [interim['other_info'].preferenceContact],
      });
  }
  initRecruteurForm(recruteur = {
    user: {
      civilite: '',
      nom: '',
      prenom: '',
      numSecu: '',
      username: '',
      idPaysNaissance: '',
      dateNaissance	: '',
      lieuNaissance	: '',
      telephone: '',
      adresse: '',
      complement_adresse: '',
      cp: '',
      ville: '',
      departement: '',
      region: '',
    },
    fonction: ''
  }) {
    this.recruteurForm = this.fb.group(
      {
        user: this.fb.group(
          {
            civilite: [recruteur['user'].civilite, Validators.required],
            nom: [recruteur['user'].nom, Validators.required],
            prenom: [recruteur['user'].prenom, Validators.required],
            numSecu: [recruteur['user'].numSecu, Validators.required],
            email: [this.user.email],
            username: [this.user.username],
            idPaysNaissance: [recruteur['user'].idPaysNaissance],
            dateNaissance: [recruteur['user'].dateNaissance],
            lieuNaissance	: [recruteur['user'].lieuNaissance	],
            telephone: [recruteur['user'].telephone, Validators.required],
            adresse: [recruteur['user'].adresse],
            complement_adresse: [recruteur['user'].complement_adresse],
            cp: [recruteur['user'].cp, Validators.required],
            ville: [recruteur['user'].ville, Validators.required],
            departement: [recruteur['user'].departement],
            region: [recruteur['user'].region],
          }
        ),
        fonction: [recruteur['other_info'].fonction],
      });
  }
  roleInterim() {
    return this.rolesService.isCandidatUnique(this.currentUser.roles);
  }

  roleRecruteur() {
    return this.rolesService.isRecruteurUnique(this.currentUser.roles);
  }

  roleSuperAdmin() {
    return this.rolesService.isCommercial(this.currentUser.roles);
  }

  onCommune(commune: string) {
    this.apiVilleService.findbyCommune(commune).subscribe(
      (response:any) => {
        if (response) {
          this.communes = response;
          this.filteredCommune = this.interimForm.get('user').get('ville').valueChanges
            .pipe(
              startWith(''),
              map(value =>

                this.communes.filter(commune =>
                  commune.nom.toLowerCase().includes(value)
                )
              ));
        }

      }
    );
  }
  onCommuneRecruteur(commune: string) {
    this.apiVilleService.findbyCommune(commune).subscribe(
      (response:any) => {
        if (response) {
          this.communes = response;
          this.filteredCommune = this.recruteurForm.get('user').get('ville').valueChanges
            .pipe(
              startWith(''),
              map(value =>

                this.communes.filter(commune =>
                  commune.nom.toLowerCase().includes(value)
                )
              ));
        }

      }
    );
  }
  onblurCommune() {
    if (this.communes.length === 0) {
      this.interimForm.get('user').get('ville').reset();
      this.interimForm.get('user').get('cp').reset();
      this.interimForm.get('user').get('departement').reset();
      this.interimForm.get('user').get('region').reset();
      ('ville incorecte');
    } else {
      this.communes.filter(
        (c) => {
          (c);
          if (c.nom === this.interimForm.get('user').get('ville').value) {
            this.interimForm.get('user').get('cp').setValue(c.code);
            this.interimForm.get('user').get('departement').setValue(c.departement.nom);
            this.interimForm.get('user').get('region').setValue(c.region.nom);
          }
        }
      );
    }
  }
  onblurCommuneRecruteur() {
    if (this.communes.length === 0) {
      this.recruteurForm.get('user').get('ville').reset();
      this.recruteurForm.get('user').get('cp').reset();
      this.recruteurForm.get('user').get('departement').reset();
      this.recruteurForm.get('user').get('region').reset();
      ('ville incorecte');
    } else {
      this.communes.filter(
        (c) => {
          if (c.nom === this.recruteurForm.get('user').get('ville').value) {
            this.recruteurForm.get('user').get('cp').setValue(c.code);
            this.recruteurForm.get('user').get('departement').setValue(c.departement.nom);
            this.recruteurForm.get('user').get('region').setValue(c.region.nom);
          }
        }
      );
    }
  }
  saveInterim() {
    const thiss = this;
    this.dialog.closeAll();
    this.notificationService.onConfirm('êtes-vous sûr (e) de vouloir continuer ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        thiss.utilService.onRevientEnHaut();
        thiss.authNormalService.signupInterimErp(thiss.interimForm.value).subscribe(response => {
            if (!response['erreur']) {
              thiss.notificationService.showNotificationSuccess(response['success']);
              thiss.authNormalService.refreshUser().subscribe(() => {
                // this.router.navigate(['RMS-Admin/redirection-admin/', 'Mon-Profil']);
                thiss.router.navigate(['RMS-Admin/redirection-admin/', 'Mon-Profil']);
              });
            } else {
              thiss.notificationService.showNotificationEchec(response['erreur']);
            }
          },
          (erreur) => {
            thiss.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
          });
      }});
  }
  saveRecruteur() {
    const thiss = this;
    this.dialog.closeAll();
    this.notificationService.onConfirm('êtes-vous sûr (e) de vouloir continuer ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        thiss.utilService.onRevientEnHaut();
        thiss.authNormalService.signupRecruteur(thiss.recruteurForm.value).subscribe(response => {
            if (!response['erreur']) {
              thiss.notificationService.showNotificationSuccess(response['success']);
              thiss.newAuthService.getUser().subscribe(() => {
                thiss.router.navigate(['RMS-Admin/redirection-admin/', 'Mon-Profil']);
              });
            } else {
              thiss.notificationService.showNotificationEchec(response['erreur']);
            }
          },
          (erreur) => {
            thiss.notificationService.showNotificationEchec('erreur  de connexion , contacter le service IT si le probleme persiste');
          });
      }
    });
  }

}
