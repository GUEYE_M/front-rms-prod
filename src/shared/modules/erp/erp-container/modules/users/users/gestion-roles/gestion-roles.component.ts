import { Component, Inject, OnInit } from '@angular/core';
import { UtilsService } from '../../../../../../../services/utils.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
	selector: 'app-gestion-roles',
	templateUrl: './gestion-roles.component.html',
	styleUrls: [ './gestion-roles.component.css' ]
})
export class GestionRolesComponent implements OnInit {
	tables: any;
	constructor(private utilsService: UtilsService, @Inject(MAT_DIALOG_DATA) public data: any) {}

	ngOnInit() {
		this.getAllTable();
	}

	getAllTable() {
		this.utilsService.getAllTables().subscribe(
			(tables) => {
				this.tables = tables;
			},
			(error) => {}
		);
	}
	onAddRoles(table, role) {
		const roleRenew = table + '_' + role;
	}
}
