import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {UserService} from '../../../../../../services/user.service';
import {FilterService} from '../../../../../../services/filter.service';
import {ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../../../../../services/notification.service';
import {SelectionModel} from '@angular/cdk/collections';
import {UtilsService} from '../../../../../../services/utils.service';
import {EditInterimComponent} from '../../interim/edit-interim/edit-interim.component';
import {GestionRolesComponent} from './gestion-roles/gestion-roles.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  formFilter: FormGroup;
  dataSourceInitial = [];
  dialogRefGestionRoles: MatDialogRef<GestionRolesComponent>;
  displayedColumns = ['select', 'number', 'Nom complet', 'Username', 'Email', 'Fonction', 'pwd', 'Etat', 'action'];
  action: number;
  listActions = [
    {
      type: 1,
      label: 'Réinitialiser mot de passe'
    }, {
      type: 2,
      label: 'Supprimer'
    }
    , {
      type: 3,
      label: 'Gestion de roles'
    }
  ];
  dataSource: MatTableDataSource<any[]>;
  selection = new SelectionModel<any[]>(true, []);
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  length: number = 0;
  elements = [];
  data = {
    offset: 0,
    limit: 12,
    filter: null,

  };
  pageSize: number = 12;

  pageSizeOptions: number[] = [12, 25, 50, 100];
  displayedColumnsFilter = [];

  @ViewChild('listPaginator', {static: false}) paginator: MatPaginator;
  listsource: any[] = [];
  idUtilisateur = 0;
  loader = 0;
  idItemUpdate = 0;
  keyItemUpdate = '';

  constructor(private  userService: UserService,
              private filterService: FilterService,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private utilsService: UtilsService,
              private activatedRoute: ActivatedRoute,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.formFilter = this.fb.group({});
    // recuperation du filtre du sauvegarde
    this.elements = JSON.parse(localStorage.getItem('elements')) || [];
    const filterValues = JSON.parse(localStorage.getItem('filterValues'));
    this.formFilter = this.filterService.setControlsOnFormFilter(this.elements, filterValues, this.formFilter);
    if (filterValues) {
      if (this.filterService.sendFilters(this.formFilter)) {
        this.data.filter = this.formFilter.value;
        this.getNewData();
      }
    } else {
      this.initData();
    }
    this.displayedColumnsFilter = this.userService.displayedColumnsName();
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  filtrer(filtre: string) {
    filtre = filtre.trim();
    filtre = filtre.toLowerCase();
    this.dataSource.filter = filtre;
  }

  onActiveOuDesactiveUser(idUser, action) {
    const thiss = this;
    const donnees = {id: idUser, etat: action};
    this.notificationService.onConfirm('Êtes-vous sûr de vouloir continuer ? ');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        action = JSON.parse(action);
        thiss.notificationService.blockUiStart();
        thiss.userService.activerOuDesactive(donnees).subscribe((response) => {
            thiss.notificationService.blockUiStop();
            thiss.initData();
            if (!response['erreur']) {
              // thiss.router.navigate(['/RMS-Admin/redirection-admin/', 'gestionnaire-interne/liste']);
              thiss.notificationService.showNotificationSuccessCopie('bottomRight', response['success']);
            } else {
              thiss.notificationService.showNotificationEchecCopie('bottomRight', response['erreur']);
            }
          },
          error => {
            thiss.initData();
            thiss.notificationService.blockUiStop();
          },
          () => {
            this.utilsService.redirectUserListe();
          });
      } else {
        this.utilsService.redirectUserListe();
      }
    });
  }


  onUpdateItem(utilisateur, keyItemUpdate) {
    this.idItemUpdate = utilisateur.idCurrent;
    this.keyItemUpdate = keyItemUpdate;
  }

  valutElements() {
    // creation du filtre depuis le storage
    this.formFilter = this.filterService.initFormFilter(this.elements, this.formFilter);
    if (this.elements.length === 0) {
      this.data.filter = null;
      this.initData();
      localStorage.removeItem('elements');
      localStorage.removeItem('filterValues');
    } else {
      localStorage.setItem('elements', JSON.stringify(this.elements));
    }
  }

  initData() {
    this.listsource = this.activatedRoute.snapshot.data['listesUsers'].data;
    if (this.listsource) {
      this.notificationService.blockUiStop();
    }
    this.dataSource = new MatTableDataSource(this.listsource);
    this.length = this.activatedRoute.snapshot.data['listesUsers'].lenght;
  }

  // fonction qui envoie le formulaire de filtre en base de donnee

  onSubmitFiltre() {
    localStorage.setItem('filterValues', JSON.stringify(this.formFilter.value));
    this.data.filter = this.formFilter.value;
    this.getNewData();

  }

  pagination(event: any) {
    this.data.offset = event.pageIndex;
    this.data.limit = event.pageSize;
    this.getNewData();
  }

  getNewData() {
    this.notificationService.blockUiStart();
    this.userService.listUsers(this.data).subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource(response['data']);
        this.length = response['lenght'];
        this.notificationService.blockUiStop();
      },
      (erreur) => {
        this.notificationService.showNotificationEchec('erreur recherche!! veillez ressayer a nouveau');
        this.notificationService.blockUiStop();
      },
      () => {
        this.notificationService.blockUiStop();
      }
    );
    this.notificationService.blockUiStop();
  }

  updatepassword(user) {
    this.notificationService.onConfirm('Réinitialiser le mot de passe de l\'utilisateur ' + user.nom + ' ' + user.prenom + ' ? ');
    this.notificationService.dialogRef.afterClosed().subscribe(x => {
      if (x) {
        this.notificationService.blockUiStart();
        this.userService.updatePasswordByAdmin(user.id).subscribe(
            (rep: any) => {
              if (rep.success) {
                this.notificationService.showNotificationSuccessCopie('bottomRight', rep.success);
              } else {
                this.notificationService.showNotificationEchecCopie('bottomRight', rep.erreur);
              }
              this.notificationService.blockUiStop();
            },
            error1 => {
              this.notificationService.blockUiStop();

            }
        );
      }
    });
  }
  actionbtn(action: number, selections) {
    const tab = [];
    if (selections && selections.length > 0) {
      if (action === 1) {
      } else if (action === 2 ) {
        selections.forEach((el) => {
          tab.push(el.id);
        });
        this.onSupprime(tab);
      } else if (action === 3 ) {
        selections.forEach((el) => {
          tab.push(el.id);
        });
        this.OnGestionRoles(tab);
      }
    }
  }
  onSupprimeOnUser(user: any) {
    this.onSupprime([user.id]);
  }
  onSupprime(ids) {
    this.notificationService.onConfirm('Voulez-vous vraiment supprimer ?');
    this.notificationService.dialogRef.afterClosed().subscribe((x) => {
      if (x) {
        this.notificationService.blockUiStart();
        this.userService.deleteUser(ids).subscribe(
            (rep: any) => {
              if (rep.success) {
                ids.forEach((id) => {
                  this.dataSource.data = this.dataSource.data.filter( (el: any) => { return el.id !== id; });
                });
                this.notificationService.showNotificationSuccessCopie('bottomRight', rep.success);
              } else {
                this.notificationService.showNotificationEchecCopie('bottomRight', 'echec');
              }
              this.notificationService.blockUiStop();
            },
            error1 => {
              this.notificationService.blockUiStop();
            });
      }
    });
  }

  OnGestionRoles(tab) {
    const donnees = { idInterims: tab };
    this.dialogRefGestionRoles = this.dialog.open(GestionRolesComponent, {
      width: '40%',
      height: '470px',
      panelClass: 'myapp-no-padding-dialog',
      data: donnees
    });
  }
}
