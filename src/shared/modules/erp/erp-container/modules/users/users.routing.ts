import {Route, RouterModule} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {UsersErpResolve} from '../../../../../resolver/usersErp.resolve';

const USERS_CONFIG: Route[] = [
  {
    path: 'liste',
    component: UsersComponent,
    resolve: {listesUsers: UsersErpResolve}
  }
];

export const UsersRouting = RouterModule.forChild(USERS_CONFIG);



