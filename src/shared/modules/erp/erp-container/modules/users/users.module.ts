import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import {UsersRouting} from './users.routing';
import {SharedModule} from '../../../../shared/shared.module';
import { GestionRolesComponent } from './users/gestion-roles/gestion-roles.component';



@NgModule({
  declarations: [
    UsersComponent,
    GestionRolesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersRouting
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  entryComponents: [
      GestionRolesComponent
  ]
})
export class UsersModule { }
