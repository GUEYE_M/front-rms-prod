import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { HistoriquesService } from '../../../../../services/erp/historiques.service';
import { type } from 'os';
import {
	ModalDismissReasons,
	NgbCalendar,
	NgbDate,
	NgbDateParserFormatter,
	NgbDateStruct,
	NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { NotificationService } from '../../../../../services/notification.service';

@Component({
	selector: 'app-historique',
	templateUrl: './historique.component.html',
	styleUrls: [ './historique.component.css' ]
})
export class HistoriqueComponent implements OnInit {
	allData: any;
	dateFiltre = false;
	tmpRangeDate: any;
	readonly DELIMITER = '-';
	displayedColumns = [ 'number', 'date', 'auteur', 'role', 'action', 'content', 'entities', 'reference', 'cible' ];
	displayedColumnsDest = [ 'nom', 'email' ];
	tmpDestinataires: any;
	hoveredDate: NgbDate;
	elements = [];
	displayedColumnsFilter = [
		{
			name: 'date',
			label: 'Date'
		},
		{
			name: 'auteur',
			label: 'Auteur'
		},
		{
			name: 'type',
			label: 'Type'
		},
		{
			name: 'action',
			label: 'Action'
		},
		{
			name: 'content',
			label: 'Content'
		},
		{
			name: 'entities',
			label: 'Entities'
		},
		{
			name: 'reference',
			label: 'Reference'
		},
		{
			name: 'cible',
			label: 'Cible'
		}
	];
	selection = new SelectionModel<any[]>(true, []);
	dataSource: MatTableDataSource<any[]> = new MatTableDataSource<any[]>();
	dataSourceDest: MatTableDataSource<any[]> = new MatTableDataSource<any[]>();
	@ViewChild(MatSort, { static: false })
	sort: MatSort;
	@ViewChild(MatSort, { static: false })
	sortDest: MatSort;
	@ViewChild('listPaginator', { static: true })
	paginator: MatPaginator;
	currentUser: any;
	closeResult: string;

	// MatPaginator Inputs
	length = 0;
	pageSize = 5;
	pageSizeOptions: number[] = [ 5, 10, 20, 100, 250, 400 ];

	// MatPaginator Output
	pageEvent: PageEvent;

	fromDate: NgbDate;
	toDate: NgbDate;
	dateDebut: any;
	dateFin: any;

	dataDefault: any;

	constructor(
		private modalService: NgbModal,
		private historiqueService: HistoriquesService,
		private calendar: NgbCalendar,
		public formatter: NgbDateParserFormatter,
		private notificationService: NotificationService,
		private historyService: HistoriquesService
	) {
		this.fromDate = calendar.getToday();
		this.toDate = calendar.getNext(calendar.getToday(), 'd', 0);
	}

	ngOnInit() {
		this.refreshData();
	}

	refreshData() {
		this.notificationService.blockUiStart();
		this.historiqueService.getHistory().subscribe((x: any) => {
			x = x.reverse();

			const tmp = x.slice(0, 5);
			this.dataSource.data = tmp;

			this.dataDefault = x;
			this.allData = x;
			this.length = x.length;
			this.OnSetTimeOut();
			this.notificationService.blockUiStop();
		});
	}

	isString(item) {
		if (item.cible) {
			return typeof item.cible;
		} else if (item.cibles) {
			return typeof item.cibles;
		}
	}

	open(content, items) {
		const tmp: BehaviorSubject<[]> = new BehaviorSubject([]);
		tmp.next(items);
		tmp.subscribe((x: any) => {
			this.dataSourceDest.data = Object.entries(x);
		});
		this.dataSourceDest.data = items;
		this.tmpDestinataires = items;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			}
		);
	}

	valutElements(item) {
		if (item.length === 0) {
			const tmpT = this.dataDefault.slice(0, 5);
			this.dataSource.data = tmpT;
			this.length = this.dataDefault.length;
			this.OnSetTimeOut();
		}
	}

	// fonction pour le submit
	onSubmitFiltre(form: NgForm) {
		this.notificationService.blockUiStart();
		if (this.tmpRangeDate) {
			this.historiqueService.getHistoryByPeriode(this.tmpRangeDate).subscribe((x: any) => {
				this.dataSource.data = [];
				let dataOnFiltre = x;
				for (const [ key, value ] of Object.entries(form.value)) {
					dataOnFiltre = this.filtreOnInputFormValueParcour(dataOnFiltre, key, value);
				}
				this.allData = dataOnFiltre;
				const tmp = dataOnFiltre.slice(0, 5);
				this.length = dataOnFiltre.length;
				this.dataSource.data = tmp;
				this.OnSetTimeOut();
			});
		} else {
			let dataOnFiltre = this.allData.slice();
			for (const [ key, value ] of Object.entries(form.value)) {
				dataOnFiltre = this.filtreOnInputFormValueParcour(dataOnFiltre, key, value);
			}
			this.dataSource.data = dataOnFiltre;
			this.length = this.dataSource.data.length;
			this.notificationService.blockUiStop();
			this.OnSetTimeOut();
		}
	}

	OnSetTimeOut() {
		setTimeout(() => {
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		}, 100);
		this.notificationService.blockUiStop();
	}

	filtreOnInputFormValueParcour(data, keyItem, valueItem) {
		const dataTmp = data.filter((x) => {
			valueItem = typeof valueItem === 'string' ? valueItem.toLowerCase() : valueItem;
			const xItem = typeof x[keyItem] === 'string' ? x[keyItem].toLowerCase() : x[keyItem];
			if (typeof xItem === 'string' && xItem.includes(valueItem)) {
				return x;
			} else {
				if (+xItem === +valueItem) {
					return x;
				}
			}
		});
		return dataTmp;
	}

	// datepiker Range
	onDateSelection(date: NgbDate) {
		if (!this.fromDate && !this.toDate) {
			this.fromDate = date;
		} else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
			this.toDate = date;
		} else {
			this.toDate = null;
			this.fromDate = date;
		}
		if (this.fromDate) {
			this.dateDebut = '' + this.fromDate.day + '-' + this.fromDate.month + '-' + this.fromDate.year;
		} else {
			this.dateDebut = '' + this.toDate.day + '-' + this.toDate.month + '-' + this.toDate.year;
		}
		if (this.toDate) {
			this.dateFin = '' + this.toDate.day + '-' + this.toDate.month + '-' + this.toDate.year;
		} else {
			this.dateFin = '' + this.fromDate.day + '-' + this.fromDate.month + '-' + this.fromDate.year;
		}
		this.tmpRangeDate = [ this.dateDebut, this.dateFin ];
	}

	isHovered(date: NgbDate) {
		return (
			this.fromDate &&
			!this.toDate &&
			this.hoveredDate &&
			date.after(this.fromDate) &&
			date.before(this.hoveredDate)
		);
	}

	isInside(date: NgbDate) {
		return date.after(this.fromDate) && date.before(this.toDate);
	}

	isRange(date: NgbDate) {
		return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
	}

	validateInput(currentValue: NgbDate, input: string): NgbDate {
		const parsed = this.formatter.parse(input);
		return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
	}

	format(date: NgbDateStruct): string {
		let result: string = null;
		if (date) {
			result = date.day + this.DELIMITER + date.month + this.DELIMITER + date.year;
		}
		return result;
	}

	pagination() {
		this.dataSource.data = [];
		const offset = this.pageEvent.pageIndex;
		const limit = this.pageEvent.pageSize;
		const tmp = this.allData.slice(offset * limit, limit * (offset + 1));
		this.dataSource.data = tmp;
		setTimeout(() => {
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		}, 100);
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
}
