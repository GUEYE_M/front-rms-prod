import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-erp-container',
  templateUrl: './erp-container.component.html',
  styleUrls: ['./erp-container.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ErpContainerComponent implements OnInit {

  myDate = new Date();
  constructor(private datePipe: DatePipe) {}

  ngOnInit() {

  }

}
