import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilsService} from '../../../../services/utils.service';
import {NotificationService} from '../../../../services/notification.service';

@Component({
  selector: 'app-redirection-admin',
  templateUrl: './redirection-admin.component.html',
  styleUrls: ['./redirection-admin.component.css']
})
export class RedirectionAdminComponent implements OnInit {
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.params = params;
      (params);
      if (this.params.espace === 'interimListe') {
        this.redirectlisteInterim();
      } else if (this.params.espace === 'GIListe') {
        this.redirectGIListe();
      } else if (this.params.espace === 'clientListe') {
        this.redirectlisteClient();
      } else if (this.params.espace === 'recruteursListe') {
        this.redirectlisteRecruteur();
      } else if (this.params.espace === 'usersListe') {
        this.redirectlisteUsers();
      } else if (this.params.espace === 'specialiteListe') {
        this.redirectSpecialitesListe();
      } else if (this.params.espace === 'missionMedecinListe') {
        this.redirectMMListe();
      } else if (this.params.espace === 'interim' && this.params.id) {
        this.redirectSingleInterim(this.params.id);
      } else if (this.params.espace === 'client'  && this.params.id) {
        this.redirectSingleClient(this.params.id);
      } else if (this.params.espace === 'renumerations'  && this.params.id) {
        this.redirectRenumerations(this.params.id);
      } else if (this.params.espace === 'renumerations-old'  && this.params.id) {
        this.redirectRenumerationsOld(this.params.id);
      } else if (this.params.espace === 'paie'  && this.params.id) {
        this.redirectPaie(this.params.id);
      } else if (this.params && !this.params.id)  {
        this.redirect(this.params.espace);
      } else {
        this.redirectAcceuil();
      }
    });
  }
  redirectSingleInterim(id) {
    this.router.navigate(['/RMS-Admin/interim/' + id + '/show']);
  }
  redirectGIListe() {
    this.router.navigate(['/RMS-Admin/gestionnaire-interne/liste']);
  }
  redirectSingleClient(id) {
    this.router.navigate(['/RMS-Admin/client/' + id + '/show']);
  }
  redirectlisteInterim() {
    this.router.navigate(['/RMS-Admin/interim/liste']);
  }
  redirectMMListe() {
    this.router.navigate(['/RMS-Admin/missions-interimaires']);
  }
  redirectlisteClient() {
    this.router.navigate(['/RMS-Admin/client/liste']);
  }
  redirectSpecialitesListe() {
    this.router.navigate(['/RMS-Admin/config/specialite/liste']);
  }
  redirectlisteUsers() {
    this.router.navigate(['/RMS-Admin/users/liste']);
  }
  redirectlisteRecruteur() {
    this.router.navigate(['/RMS-Admin/recruteurs/liste']);
  }
  redirectRenumerations(id) {
    this.router.navigate(['/RMS-Admin/renumerations/' + id + '/show']);
  }
  redirectRenumerationsOld(id) {
    this.router.navigate(['/RMS-Admin/renumerations-old/' + id + '/show']);
  }
  redirectPaie(id) {
    this.router.navigate(['/RMS-Admin/paie/' + id + '/show']);
  }
  redirectAcceuil() {
    this.router.navigate(['/RMS-Admin']);
  }
  redirect(params) {
    this.router.navigate(['/RMS-Admin/' + params]);
  }

}
