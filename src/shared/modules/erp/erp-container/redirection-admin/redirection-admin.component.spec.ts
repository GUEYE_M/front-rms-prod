import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectionAdminComponent } from './redirection-admin.component';

describe('RedirectionAdminComponent', () => {
  let component: RedirectionAdminComponent;
  let fixture: ComponentFixture<RedirectionAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectionAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectionAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
