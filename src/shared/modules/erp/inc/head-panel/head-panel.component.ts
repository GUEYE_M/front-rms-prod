import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {NotificationService} from '../../../../services/notification.service';
import {AuthNormalService} from '../../../../services/authNormal.service';
import {Router} from '@angular/router';
import {RolesService} from '../../../../services/roles.service';
import $ from 'jquery';
import {NewAuthService} from '../../../../services/Auth/newAuth.service';

@Component({
  selector: 'app-head-panel',
  templateUrl: './head-panel.component.html',
  styleUrls: ['./head-panel.component.css']
})
export class HeadPanelComponent implements OnInit {

  currentUser: any;
  erreur: string;
  fichier: any;
  fd: FormData = new FormData();
  constructor(private userService: UserService,
              private newAuthService: NewAuthService,
              private notificationService: NotificationService,
              private authNormalService: AuthNormalService,
              private router: Router,
              public roleService: RolesService) { }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    // This will hide sidebar when it's clicked outside of it
    $(document).on('click touchstart', function(e){
      e.stopPropagation();

      // closing left sidebar
      if($('body').hasClass('show-left')) {
        var targ = $(e.target).closest('.br-sideleft').length;
        if(!targ) {
          $('body').removeClass('show-left');
        }
      }

      // closing right sidebar
      if($('body').hasClass('show-right')) {
        var targ = $(e.target).closest('.br-sideright').length;
        if(!targ) {
          $('body').removeClass('show-right');
        }
      }
    });
  }

  OnchangephotoProfil(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      const img = '.imgphotoProfil';
      const fileType = event.target.files[0].type;
      let localUrl: string;
      const thiss = this;
      reader.onload = (e: any) => {
        if (fileType.includes('image')) {
          localUrl = e.target.result;
          $(img).attr('src', localUrl);
          this.notificationService.onConfirm('Êtes-vous sûr de vouloir continuer ?');
          this.notificationService.dialogRef.afterClosed().subscribe((x) => {
            if (x) {
              $(img).attr('src', localUrl);
              thiss.onUploadPP();
            } else {
              $(img).attr('src', thiss.currentUser.photo);
            }
          });
          this.fd.append('photoProfil', event.target.files[0]);
          this.fichier = event.target.files[0];
        } else {
          this.notificationService.showNotificationEchec('Cet type de fichier n\'est pas prise en compte !');
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  onCliqueinputfile() {
    const inputfile = '.photoProfil';
    $(inputfile).click();
  }
  // =======================================================================================================================================
  // Fonction pour uploder un element cote API
  onUploadPP() {
    const id = this.currentUser.idUser;
    const tableau = {'id': id, 'photo' : this.fd};
    this.notificationService.blockUiStart();
    this.userService.updatePhotoProfil(tableau).subscribe(
      (response) => {
        this.currentUser.photo = response;
        localStorage.setItem('rms-user', JSON.stringify(this.currentUser));
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationSuccess('Vous avez mise à jour avec succès votre photo de profil !');
      },
      err => {
        this.notificationService.blockUiStop();
        this.notificationService.showNotificationEchec('Echec de la mise à jour de votre photo de profil !');
      });
  }

  public logout(): void {
    this.newAuthService.logout();
  }

  public offerNotifications() {
    this.notificationService.offerNotifications();
  }
  displayDevice() {
    var menuText = $('.menu-item-label');

    if ($('body').hasClass('collapsed-menu')) {
      $('body').removeClass('collapsed-menu');

      // show current sub menu when reverting back from collapsed menu
      $('.show-sub + .br-menu-sub').slideDown();

      $('.br-sideleft').one('transitionend', function(e) {
        menuText.removeClass('op-lg-0-force');
        menuText.removeClass('d-lg-none');
      });

    } else {
      $('body').addClass('collapsed-menu');

      // hide toggled sub menu
      $('.show-sub + .br-menu-sub').slideUp();

      menuText.addClass('op-lg-0-force');
      $('.br-sideleft').one('transitionend', function(e) {
        menuText.addClass('d-lg-none');
      });
    }
  }
  displayDeviceMobile() {
    $('#btnLeftMenuMobile').on('click', function(){
      $('body').addClass('show-left');
      return false;
    });
  }
}
