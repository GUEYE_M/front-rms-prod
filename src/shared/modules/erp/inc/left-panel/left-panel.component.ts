import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {NotificationService} from '../../../../services/notification.service';
import {AuthNormalService} from '../../../../services/authNormal.service';
import {Router} from '@angular/router';
import {RolesService} from '../../../../services/roles.service';
import {ClientService} from "../../../../services/recruteur/client.service";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css']
})
export class LeftPanelComponent implements OnInit {
  currentUser: any;
  panelOpenState = false;
  docMyRMS = 'https://rmsapi-old.azurewebsites.net/docs/Manuel%20d\'utilisation%20MyRMS.pdf';
  docHopRMS = 'https://rmsapi-old.azurewebsites.net/docs/Manuel%20d\'utilisation%20Hop!RMS.pdf';
  constructor(
    private userService: UserService,
    public roleService: RolesService,
  private clientService: ClientService,
  ) { }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    (this.roleService.isSuperAdmin(this.currentUser.roles));
  }

  onClearLocalStorage(){
    localStorage.removeItem('elements');
    localStorage.removeItem('filterValues');
    localStorage.removeItem('date_debut');
    localStorage.removeItem('date_fin');
  }
}
