import {Route, RouterModule} from '@angular/router';
import {ErpContainerComponent} from './erp-container/erp-container.component';
import {WrapperComponent} from './erp-container/wrapper/wrapper.component';
import {MonProfilComponent} from './erp-container/modules/mon-profil/mon-profil/mon-profil.component';
import {EditMonProfilComponent} from './erp-container/modules/mon-profil/edit-mon-profil/edit-mon-profil.component';
import {RedirectionAdminComponent} from './erp-container/redirection-admin/redirection-admin.component';
import {ClientWrapperComponent} from './erp-container/modules/client-wrapper/client-wrapper/client-wrapper.component';
import {ListeClientComponent} from './erp-container/modules/client-wrapper/client/liste-client/liste-client.component';
import {ClientResolveService} from '../../resolver/ClientResolve.resolve';
import {ListClientRecruteurComponent} from './erp-container/modules/client-wrapper/client/list-client-recruteur/list-client-recruteur.component';
import {AuthCommercialGuard} from '../../guard/authCommercial.guard';
import {AuthSuperAdminGuard} from '../../guard/authSuperAdmin.guard';
import {AuthGuardManager} from '../../guard/auth.guardManager';
import {AuthGuardRecruteur} from '../../guard/auth.guardRecruteur';
import {SingleInterimComponent} from './erp-container/modules/interim/single-interim/single-interim.component';
import {BienvenueMyRMSComponentComponent} from './erp-container/modules/interim/bienvenue-my-rmscomponent/bienvenue-my-rmscomponent.component';
import {HistoriqueComponent} from './erp-container/modules/historique/historique.component';

const ERP_ROUTES: Route[] = [
  {
    path: '',
    component: ErpContainerComponent,
    children:
      [
        {path: '', component: WrapperComponent},
        {path: 'Mes-Rubriques', component: SingleInterimComponent},
        {
          path: 'bienvenue-my-rms', component: BienvenueMyRMSComponentComponent
        },
        {path: 'redirection-admin/:espace', component: RedirectionAdminComponent},
        {path: 'redirection-admin/:espace/:id', component: RedirectionAdminComponent},
        {
          path: 'Mon-Profil',
          component: MonProfilComponent,
          children:
            [
              {path: 'edit', component: EditMonProfilComponent}
            ]
        },
        {
          path: 'config',
          loadChildren: './erp-container/modules/admin-config/admin-config.module#AdminConfigModule',
          canActivate: [AuthGuardManager]
        },
        {
          path: 'users',
          loadChildren: './erp-container/modules/users/users.module#UsersModule',
          canActivate: [AuthSuperAdminGuard]
        },
        {
          path: 'recruteurs',
          loadChildren: './erp-container/modules/recruteur/recruteur.module#RecruteurModule',
          canActivate: [AuthCommercialGuard]
        }, {
        path: 'historiques',
        component: HistoriqueComponent,
        canActivate: [AuthCommercialGuard]
      },
        {
          path: 'gestionnaire-interne',
          loadChildren: './erp-container/modules/gestionnaire-interne/gestionnaire-interne.module#GestionnaireInterneModule',
          canActivate: [AuthSuperAdminGuard]
        },
        {
          path: 'prospection',
          loadChildren: './erp-container/modules/prospection/prospection.module#ProspectionModule',
          canActivate: [AuthGuardManager]
        },
        {
          path: 'email',
          loadChildren: './erp-container/modules/email/email.module#EmailModule',
          canActivate: [AuthGuardManager]
        },
        {
          path: 'taches',
          loadChildren: './erp-container/modules/gestion-operations/gestion-operations.module#GestionOperationsModule',
          canActivate: [AuthGuardManager]
        },
        {
          path: 'suivie-contrat',
          loadChildren: './erp-container/modules/suivie-contrat/suivie-contrat.module#SuivieContratModule',
          canActivate: [AuthGuardManager]
        },
        {
          path: 'interim',
          loadChildren: './erp-container/modules/interim/interim.module#InterimModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'client',
          loadChildren: './erp-container/modules/client-wrapper/client/client.module#ClientModule',
          canActivate: [AuthGuardRecruteur]
        },
        {
          path: 'candidatures',
          loadChildren: './erp-container/modules/candidatures/candidatures.module#CandidaturesModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'missions-interimaires',
          loadChildren: './erp-container/modules/missions-interimaires/missions-interimaires.module#MissionsInterimairesModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'renumerations',
          loadChildren: './erp-container/modules/renumerations/renumerations.module#RenumerationsModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'renumerations-old',
          loadChildren: './erp-container/modules/renumerations-old/renumerations-old.module#RenumerationsOldModule',
          canActivate: [AuthGuardManager]
        },
        {
          path: 'disponibilites',
          loadChildren: './erp-container/modules/disponibilites/disponibilites.module#DisponibilitesModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'vacation',
          loadChildren: './erp-container/modules/vacation/vacationclient.module#VacationclientModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'fiche-specialite',
          loadChildren: './erp-container/modules/client-wrapper/client/fiche-specialites/fichespecialite.module#FichespecialiteModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'liste-planning',
          loadChildren: './erp-container/modules/client-wrapper/client/liste-planning/liste-planning.module#ListePlanningModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'liste-missions',
          loadChildren: './erp-container/modules/client-wrapper/client/liste-missions/liste-missions.module#ListeMissionsModule',
          canActivate: [AuthCommercialGuard]
        },
        {
          path: 'Etablissements',
          component: ClientWrapperComponent, children: [
            {path: '', resolve: {listClients: ClientResolveService}, component: ListeClientComponent},
          ],
          canActivate: [AuthCommercialGuard]
        },

        {
          path: 'mes-etablissements',
          component: ClientWrapperComponent, children: [
            {path: '', component: ListClientRecruteurComponent},
          ],
          canActivate: [AuthGuardRecruteur]
        },

        {
          path: 'sms',
          loadChildren: './erp-container/modules/sms/sms.module#SmsModule',
          canActivate: [AuthGuardManager]
        },

      ],

  }
];

export const ErpRouting = RouterModule.forChild(ERP_ROUTES);
