import {Observable} from 'rxjs/Rx';
import {Injectable, OnInit} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
// import {InterimService} from "../services/interim/interim.service";
// import {PlanningService} from "../services/recruteur/planning.service";
import {SpecialiteService} from '../services/specialite.service';
import {NotificationService} from '../services/notification.service';

@Injectable()

export class SpecialiteListesResolver implements Resolve<{}> {
    constructor(
      private notificationService: NotificationService,
        private specialiteService: SpecialiteService
    ){}
    resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any{
        this.notificationService.blockUiStart();
        return this.specialiteService.list();
    }
}
