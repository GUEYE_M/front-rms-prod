import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {UserService} from '../services/user.service';
import {Observable} from 'rxjs';
import {NotificationService} from '../services/notification.service';

@Injectable()
export class UsersErpResolve implements Resolve<{}> {
    constructor(
        private usersService: UserService,
        private notificatiService: NotificationService
    ) {
    }
    resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return this.usersService.listUsers();
    }
}
