import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {ClientService} from '../services/recruteur/client.service';
import {Injectable} from '@angular/core';
import {PlanningService} from '../services/recruteur/planning.service';
import {MissionService} from '../services/mission.service';
import {NotificationService} from '../services/notification.service';

@Injectable()
export class ClientSingleResolve implements Resolve<any> {
  constructor(
    private notificationService: NotificationService,
    private clientService: ClientService,
  ) {
  }

  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{}> {

    const data_stat = {
      filter: null,
      id_client: parseInt(router.params.id)


    };
    this.notificationService.blockUiStart();
    return this.clientService.find(data_stat);
  };
}
