import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ClientService} from '../services/recruteur/client.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {NotificationService} from '../services/notification.service';

@Injectable()
export class ClientResolveService implements Resolve<any> {
  constructor(
    private notificationService: NotificationService,
    private clientService: ClientService
  ) {
  }

  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.notificationService.blockUiStart();
    const data = {
      offset: 0,
      limit: 12,
      date_debut: null,
      date_fin: null,
      filter: null,
    }
    return this.clientService.listClients(data);
  }
}
