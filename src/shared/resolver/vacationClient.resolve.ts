import { Injectable } from '@angular/core';
import { VacationClientService } from '../services/recruteur/vacation-client.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class VacationClientResolve implements Resolve<{}> {
	constructor(
		private notificationService: NotificationService,
		private vacationClientService: VacationClientService
	) {}

	resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
		const data = {
			offset: 0,
			limit: 12,
			filter: null
		};
		this.notificationService.blockUiStart();
		return this.vacationClientService.allVacations(data);
	}
}
