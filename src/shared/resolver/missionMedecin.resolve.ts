import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Rx";
import {CandidatureService} from "../services/candidature.service";
import {NotificationService} from '../services/notification.service';

@Injectable()
export class MissionMedecinResolve {
    constructor(
        private candidatureService: CandidatureService,
        private notificationservice: NotificationService
    ){}
    resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any{
      this.notificationservice.blockUiStart();
        return this.candidatureService.getAllCandidaturesValidees();
    }


}
