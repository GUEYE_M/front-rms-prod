import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {InterimService} from '../services/interim/interim.service';
import {Observable} from 'rxjs/Rx';
import {MissionService} from '../services/mission.service';
import {NotificationService} from '../services/notification.service';

@Injectable()
export class MissionsResolve implements Resolve<any> {
  constructor(
    private notificationService: NotificationService,
    private missionService: MissionService
  ) {
  }

  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.notificationService.blockUiStart();
    return this.missionService.getMissions();
  }
}
