import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Rx";
import {CandidatureService} from '../services/candidature.service';
import {NotificationService} from "../services/notification.service";

@Injectable()
export class AllCandidaturesResolver implements Resolve<any> {
  constructor(
    private candidaturesService: CandidatureService,
    private notificationService: NotificationService
  ) {
  }

  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.notificationService.blockUiStart();
    return this.candidaturesService.getAllCandidatures();
  }
}
