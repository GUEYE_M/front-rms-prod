import {Observable} from "rxjs/Rx";
import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {InterimService} from "../services/interim/interim.service";

@Injectable()
export class InterimSingleResolve implements Resolve<{}>{
    constructor(
        private interimService: InterimService
    ){};

    resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any{
        return this.interimService.find(router.params.id);
    }
}