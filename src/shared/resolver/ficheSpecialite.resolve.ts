import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {FicheSpecialiteService} from '../services/recruteur/fiche-specialite.service';
import {Observable} from 'rxjs/Rx';
import {NotificationService} from '../services/notification.service';

@Injectable()
export class FicheSpecialiteResolve implements Resolve<{}> {
  constructor(
    private notificationService: NotificationService,
    private ficheSpecialiteService: FicheSpecialiteService
  ) {
  }

  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.notificationService.blockUiStart();
    return this.ficheSpecialiteService.allFicheSpecialite();
  }
}
