import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {NotificationService} from '../services/notification.service';
import {OffresService} from '../services/recruteur/offres.service';
import {UserService} from '../services/user.service';

@Injectable()
export class OffreSiteResolver implements Resolve<any>{
  interimaire;
  constructor(
    private userService: UserService,
    private notificationService: NotificationService,
    private offresService: OffresService
  ){
    this.interimaire = this.userService.getCurrentUser();
    if(this.interimaire)
    {
      this.interimaire = this.interimaire.interim ? this.interimaire.interim.id : 0;
    }
    else
    {
      this.interimaire = 0;
    }
   
  }
  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.notificationService.blockUiStart();
    return this.offresService.getJsonOffresSite(this.interimaire, 0, 10);
  }
}
