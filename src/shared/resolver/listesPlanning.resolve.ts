import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {InterimService} from '../services/interim/interim.service';
import {PlanningService} from '../services/recruteur/planning.service';
import {NotificationService} from '../services/notification.service';

@Injectable()
export class ListesPlanningResolve implements Resolve<{}> {
  constructor(
    private notificationService: NotificationService,
    private planningService: PlanningService
  ) {
  }

  resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.notificationService.blockUiStart();
    return this.planningService.getAllPlannings();
  }
}
