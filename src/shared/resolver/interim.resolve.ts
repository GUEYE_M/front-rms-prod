import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { InterimService } from '../services/interim/interim.service';
import { Observable } from 'rxjs/Rx';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class InterimResolve implements Resolve<{}> {
	constructor(private interimService: InterimService, private notificationsSErvice: NotificationService) {}

	resolve(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
		const data = {
			filter: null,
			limit: 12,
			offset: 0,
			date_debut: null,
			date_fin: null,
			login_date_debut: null,
			login_date_fin: null,
			ordre: 'ASC'
		};
		this.notificationsSErvice.blockUiStart();
		return this.interimService.getAllInterimCopie(data);
	}
}
