import { Qualification } from "./Qualification.model";

export class Specialite {
    constructor(
        public id : number = null,
        public specialite:string = null,
        public nomspecialite: string = null,
        public abreviation:string = null,
        public qualifications :Qualification[]
    ){}
}

