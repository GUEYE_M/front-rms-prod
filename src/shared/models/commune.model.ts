import { Departement } from "./departement.model";
import { Region } from "./Region.model";

export class Commune {
    constructor(
        public id: number,
        public nom: string,
        public code: string,
        public departement: Departement,
        public region: Region,
        public 	population: string,
        public codesPostaux:any[],
    ){}
}

