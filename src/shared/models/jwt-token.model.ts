export interface JwtToken {
    isAuthenticated: boolean;
    roles: string;
    user: string;
}
