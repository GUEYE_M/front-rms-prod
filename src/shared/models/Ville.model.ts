export class Ville {
    constructor(
        public id: number,
        public cp:string,
        public numero_departement: string,
        public circonscription: string,
        public latitude: number,
        public longitude: number,
        public code_region: string,
        public region: string,
        public 	departement: string,
        public commune: string,
    ){}
}