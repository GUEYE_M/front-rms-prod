export class RolesModel {
    constructor(
        public commercial = 'ROLE_COMMERCIAL',
        public superviseur = 'ROLE_SUPERVISEUR',
        public manager= 'ROLE_MANAGER',
        public comptable= 'ROLE_COMPTABLE',
        public superAdmin = 'ROLE_SUPER_ADMIN',

) {}
}

