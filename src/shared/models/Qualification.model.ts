import { Specialite } from './Specialite.model';

export class Qualification {
    constructor(
        public id : number,
        public qualification: string,
        public specialite: Specialite
    ){}
}