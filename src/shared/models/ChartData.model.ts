
export class ChartDataModel {
    constructor(
        public label: string,
        public data: string,
    ) {}
}
