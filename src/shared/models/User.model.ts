import {Adresse} from './Adresse.model';

export class User {
    constructor(
        public username: string,
        public email: string,
        public password: string,
        public roles: any[],
        public civilite: string,
        public nom: string,
        public prenom: string,
        public telephone: string,
        public auteur: string,
        public remarque: string,
        public description: string,
        public dateEnreg: string,
        public enabled: boolean,
        public last_login: string,
        public confirmation_token: string,
        public photo: string,
        public adresse: string|null,
        public complement_adresse: string|null,
        public commune: string|null,
        public cp: string|null,
        public idPld: number|null,
        public numSecu: string|null,
    ) {}
}
