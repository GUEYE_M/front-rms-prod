import { Client } from "./Client.model";
import { Qualification } from "../Qualification.model";

export class FicheSpeciliate {
    constructor(
        public id:number,
        public qualifications:Qualification[],
        public info_pratique: string,
        public nombrepassage_par_an: number,
        public nombre_lit: number,
        public nombre_medecin_nuit: number,
        public nombre_medecin_jour: number, 
        public coef_saison: number,
        public environemen_tech: string,
        public client : Client,
        public diplome : string,
        public specialite:string,
        public coef_tarification: number
    ){}
}