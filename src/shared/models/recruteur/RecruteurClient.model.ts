import { Recruteur } from "./Recruteur.model";
import { Client } from "./Client.model";

export class RecruteurClient {
    constructor(
        public id: number,
        public recruteur: Recruteur[],
        public client: Client,
    
    ) {}
  } 
