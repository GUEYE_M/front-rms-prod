import { Offres } from "./Offres.model";

export class Planning {
    constructor(
        public offre: Offres,
        public tarif: number,
        public salaireNet: number,
        public date: Date,
        public statut: string,
        public garde: string,
        public qualification?: string,
    ){
        this.statut="A Pourvoir";
    }
}