
import { FicheSpeciliate } from "./FicheSpeciliate.model";
import { Time } from "@angular/common";

export class Vacation {
    constructor(
        public typeDeGarde_id:number,
        public libelle: string,
        public heure_debut: Time,
        public heure_fin: Time,
        public annee:number,
        public tarif: number,
        public salaire_net: number,
        public client:string,
        public nomSpecialite:string,
       
    ){}
}