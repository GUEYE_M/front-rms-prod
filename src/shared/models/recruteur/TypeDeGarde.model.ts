import { FicheSpeciliate } from "./FicheSpeciliate.model";
import { Time } from "@angular/common";

export class TypeDeGarde {
    constructor(
        public id: number,
        public fiche_specialite: FicheSpeciliate,
        public heure_debut: Time,
        public heure_fin: Time,
        public tarif_normal: number,
        public tarif_saison: number,
        public salaireNetNormal: string,
        public salaireNetSaison: string,
        public remarques?: number,
        
    ) {}
}