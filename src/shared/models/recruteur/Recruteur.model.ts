
import {User} from '../User.model';
import { RecruteurClient } from './RecruteurClient.model';

export class Recruteur {
    constructor(
        public fonction: string,
        public user: User,
        public recruteurClient: RecruteurClient[],
        public id?: number
    ) {}
  }
