import { Client } from "./Client.model";
import { Mission } from "../interim/Mission.model";

export class FicheClient {
    constructor(
        public id?:number,
        public info_pratique_ville?: string,
        public remarque_hebergement?: string,
        public repas?: string,
        public remarque_repas?: string,
        public taxi?: string,
        public client?:Client,
        public situation_de_la_structure?: string,
        public distance_avec_gare?: number,
        public distance_avec_aeroport?: number,
        public hebergemen_internat?: string,
        public mission?: Mission[]

    ){
        this.info_pratique_ville = "";
    }
}