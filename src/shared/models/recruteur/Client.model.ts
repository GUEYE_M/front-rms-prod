import { Adresse } from "../Adresse.model";
import { User } from "../User.model";
import { Recruteur } from "./Recruteur.model";

export class Client {
    constructor(
        public id: number,
        public recruteur: Recruteur,
        public nom_etablissement: string,
        public raison_sociale: string,
        public type: string,
        public telephone: string,
        public fax: string|null,
        public site_web: string|null,
        public remarques: string|null,
        public email:string|null,
        public hopitalConnecte: boolean|null,
        public majoration: number|null,
        public taux_tva: number|null,
        public coefficient_de_commision: number|null,
        public numero_de_tva: number|null,
        public 	rib: number|null,
        public 	dateEnreg: Date|null,
        public mode_de_collaboration: string|null,
        public auteur: string|null,
        public periodesaison: string|null,
        public  adresse: string,
        public complement_adresse:string,
        public commune: string,
        public cp : string
    ) {}
  } 
