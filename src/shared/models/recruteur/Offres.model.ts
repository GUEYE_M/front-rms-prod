import { FicheSpeciliate } from "./FicheSpeciliate.model";
import { Planning } from "./Planning.model";

export class Offres {
    constructor(
        public id: number,
        public fiche_specialite: FicheSpeciliate,
        public mois: string,
        public annee: string,
        public planning: Planning[],
        public dateenreg: Date,
        public auteur: string,
        public enable: boolean,
        public diplome_minimum_requis?: string,
        public requis_medecin_inscrit?: number,
        public remarques?: string,
        public reference?: string,
        public diplome?: string,
       
    ){}
}