

export class Tache {
    constructor(
        public id : number,
        public date_debut : Date,
        public date_fin : Date,
        public description : string,
        
        public type_tache :string,
        public heure_debut :Date,
        public heure_fin : Date

    ){}
}

