export class ContratSellAndSignModel {
    constructor(  
      public date: number, 
      public vendor_email : string, 
      public customer_number : string,
      public contract_definition_id : number,
      public closed : boolean,
      public message_title? : string,
      public message_body? : string,
      public filename? : string,
      public keep_on_move? : boolean,
      public transaction_id? : number,
      public customer_entity_id?: number,
    
  
    ) {}
}
