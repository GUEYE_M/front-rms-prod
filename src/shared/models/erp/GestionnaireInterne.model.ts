import {User} from '../User.model';

export class GestionnaireInterne {
    constructor(
      public id: number,
      public user: User,
    ) {}
}
