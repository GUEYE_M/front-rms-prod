

export class SignataireSellAndSignModel {
    constructor(  
      public customer_number : string, 
      public id : number, 
      public civility: string,
      public firstname: string,
      public lastname: string,
      public company_name: string,
      public registration_number: string,
      public address_1: string,
      public address_2: string,
      public postal_code: string,
      public city: string,
      public country: string,
      public cell_phone: string,
      public email: string,
      public phone: string,
      public job_title: string,
  
    ) {}
}



