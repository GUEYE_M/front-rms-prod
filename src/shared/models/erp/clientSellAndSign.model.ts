export class ClientSellAndSignModel {
    constructor(
      public number: string,
      public customer_code: string,
      public company_name: string,
      public registration_number: string,
      public address_1: string,
      public address_2: string,
      public postal_code: string,
      public city: string,
      public country: string,
      public civility: string,
      public firstname: string,
      public lastname: string,
      public cell_phone: string,
      public email: string,
      public phone: string,
      public job_title: string,

    ) {}
}



