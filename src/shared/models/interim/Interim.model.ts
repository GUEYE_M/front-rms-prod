import {User} from '../User.model';
import { Adresse } from '../Adresse.model';
import { Qualification } from '../Qualification.model';

export class Interim {
    constructor(
        public id: number,
        public user: User,
        public actif: boolean|false,
        public num_RPPS: string| null,
        public inscription: string| null,
        public statut: string| null,
        public remarque: string| null,
        public autre_remarques: string| null,
        public description: string| null,
        public coef_IK: number| null,
        public num_secu: string| null,
        public rayonIntervention: string,
        public preferenceDeplacement: string| null,
        public preferenceContact: string| null,
        public disponibiliteAppel: string| null,
        public qualifications: Qualification[]
    ) {
    }
}
