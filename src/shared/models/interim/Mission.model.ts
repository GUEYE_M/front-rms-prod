import {Interim} from './Interim.model';

export class Mission {
    constructor(
        public medecin: Interim,
        public auteur_candidature: string,
        public auteur_validation?: string,
        public date_validation?: string,
        public auteurcontratM?: string,
        public dateenvoiecontratM?: string,
        public auteurcontratH?: string,
        public dateenvoiecontratH?: string,
        public etatCM?: string,
        public dateCM?: string,
        public dateCH?: string,
        public auteur_retourCM?: string,
        public auteur_retourCH?: string,
        public etatCH?: string,
        public conditionmedecin?: string,
        public heure_travail?: number,
        public remarques?: string
    ) {}
}
