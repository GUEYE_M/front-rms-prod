export class ClientPldModel {
    constructor(
        public NAF: string,
        public SIRET: string,
        public TVAIntra: string,
        public RaisonSociale: string,
        public AdresseVille: string,
        public AdresseRue: string,
        public AdresseSuite: string,
        public AdresseCodePostal: string,
        public Telephone1: string,
        public Telephone2: string,
        public Fax: string,
        public Email: string,
        public IdFormeJuridique: number,
        public IdActiviteFact: number,
        public IdModeReglement: number,
        public IdEcheance: number,
        public IdDevise: number,
        public IdFactureCycle: number,
        public IdFactureGroupe1: number,
        public IdFactureGroupe2: number,
        public IdFactureGroupe3: number,
        public TabPrimes: any[],
        public TabQualifications: any[],
        public TabInterlocuteurs: any[]
    ) {}
}
