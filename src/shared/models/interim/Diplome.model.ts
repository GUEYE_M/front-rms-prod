import {Dossier} from './Dossier.model';

export class Diplome {
    constructor(
        public dossier: Dossier ,
        public diplome_file_name: string,
        public diplome_name: string,
        public updated_at: string
    ) {}
}