import {Mission} from './Mission.model';

export class Renumeration {
    constructor(
        public mission: Mission,
        public date_paiement: string,
        public date_paiementFrais?: string,
        public mode_paiement?: string,
        public totalPrestation?: number,
        public totalFrais?: number,
        public total?: number,
        public etat?: string,
        public etatFrais?: string,
        public remarque?: string,
        public voiture?: number,
        public montant_train?: number,
        public montant_avion?: number,
        public montant_taxi?: number,
        public montant_bus?: number,
        public montant_covoiturage?: number,
        public montant_parking?: number,
        public montant_location_voiture?: number,
        public autre?: number,
        public montant_ticket_peage?: number,
        public montant_hotel?: number,
        public montant_repas?: number,
        public montant_ik?: number
    ) {}
}