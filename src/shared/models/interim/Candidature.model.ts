import {Mission} from './Mission.model';
import {Planning} from '../recruteur/Planning.model';

export class Candidature {
    constructor(
        public mission: Mission,
        public planning: Planning,
        public statut_candidature: string
    ) {}
}