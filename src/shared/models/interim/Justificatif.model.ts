import {Mission} from './Mission.model';

export class Justificatif {
    constructor(
        public mission: Mission,
        public date: string,
        public adresseMedecin?: string,
        public adresseHopital?: string,
        public nombreTrajet?: string,
        public coefIk?: string,
        public nombre_km?: string,
        public enable?: number
    ) {}
}