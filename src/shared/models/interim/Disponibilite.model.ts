import {Interim} from './Interim.model';
import { UserService } from 'src/shared/services/user.service';

export class Disponibilite {
    constructor(
        public date: string,
        public mois: string,
    ) {
    }
}
