import {User} from '../User.model';
import { Adresse } from '../Adresse.model';
import { Qualification } from '../Qualification.model';

export class InterimPldModel {
    constructor(
        public TypeInterimaire: number,
        public Civilite: string,
        public Nom: string,
        public Prenom: string,
        public AdresseRue: string,
        public AdresseSuite: string,
        public AdresseVille: string,
        public AdresseCodePostal: string,
        public NumeroSecu: string,
        public DateNaissance: string,
        public LieuNaissance: string,
        public IdPaysNaissance: string,
        public idNationalite: string,
        public Telephone1: string,
        public Telephone2: string,
        public Email: string,
        public IdReglementAcompte: number,
        public IdReglementPaie: number,
        public IdMoyenLocomotion: number,
        public pcs: string,
        public TabQualifications: any[]
    ) {
    }
}
