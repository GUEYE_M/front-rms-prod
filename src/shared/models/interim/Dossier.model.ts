import {Interim} from './Interim.model';

export class Dossier {
    constructor(
        public enable: boolean,
        public etat: number,
        public medecin: Interim,
        public updated_at: Date,
        public validite_piece_identite?: Date,
        public piece_name?: string,
        public carte_vitale_name?: string,
        public carte_grise_name?: string,
        public cv_name?: string,
        public inscription_name?: string,
        public jdomicile_name?: string,
        public rib_name?: string,

    ) {}
}
