import { Ville } from './Ville.model';

export class Adresse {
    constructor(
        public adresse: string,
        public complement_adresse: string,
        public date_enreg: string,
        public ville: Ville

    ){}
}