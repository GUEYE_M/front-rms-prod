
export class ChartModel {
    [x: string]: string;
    showlabels: any;
    constructor(
        public caption?: string,
        public subcaption?: string,
        public showpercentintooltip?: string,
        public numberprefix?: string,
        public enablemultislicing?: string,
        public theme?: string,
        public xaxisname?: string,
        public yaxisname?: string,
        public formatnumberscale?: string,
        public plottooltext?: string,
        public showsum?: string,
        public valuefontcolor?: string,
        public showvalues?: string


    ) { }
}

export class TypeChartModel {
    constructor(
        public width?: string,
        public height?: number,
        public type?: string,
        public dataFormat: string = "json",
        public dataSource?: any
    ) { }
}

export class DataModel {
    constructor(
        public chart?: {},
        public data?: any,
        public categories?: any,
        public dataset?: any[],

    ) { }
}
export class SeriesModel {
    constructor(
        public seriesname: string,
        public data: [],


    ) { }
}

