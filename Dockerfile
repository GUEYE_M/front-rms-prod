FROM node:14.2


RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install libxss1 libnss3 libsecret-1-0 libxkbfile1 libnotify4 libsecret-1-0 libasound2 libgtk-3-0 git -f -y && \    
    apt --fix-broken install -y            
    
RUN curl -O https://packages.microsoft.com/repos/vscode/pool/main/c/code/code_1.45.1-1589445302_amd64.deb && \
    dpkg -i ./code_1.45.1-1589445302_amd64.deb && \
    rm ./code_1.45.1-1589445302_amd64.deb     
                



USER node

RUN mkdir /home/node/.npm-global
ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
RUN npm install -g @angular/cli@8.*

 

EXPOSE 4200

CMD ["/bin/bash"]